      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       ID DIVISION.                                                     00010000
       PROGRAM-ID. BUR0043.                                             00020002
      *********************************************************         00030000
      * CREATION SYSIN POUR REQUETE QMF Q014                  *         00041002
      *********************************************************         00050000
       ENVIRONMENT DIVISION.                                            00060000
       CONFIGURATION SECTION.                                           00060100
       SPECIAL-NAMES.   DECIMAL-POINT IS COMMA.                         00070000
       INPUT-OUTPUT  SECTION.                                           00080000
       FILE-CONTROL.                                                    00090000
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT QMF001A ASSIGN QMF001A.                              00100001
      *--                                                                       
            SELECT QMF001A ASSIGN QMF001A                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT QMF001B ASSIGN QMF001B.                              00100001
      *--                                                                       
            SELECT QMF001B ASSIGN QMF001B                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT QMF001C ASSIGN QMF001C.                              00100001
      *--                                                                       
            SELECT QMF001C ASSIGN QMF001C                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT QMF001D ASSIGN QMF001D.                              00100001
      *--                                                                       
            SELECT QMF001D ASSIGN QMF001D                                       
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      *{ Tr-Select-Sequential 1.3                                               
      *     SELECT FDATE   ASSIGN FDATE.                                00100002
      *--                                                                       
            SELECT FDATE   ASSIGN FDATE                                         
              ORGANIZATION LINE SEQUENTIAL.                                     
      *}                                                                        
      * -----------------------------------------------------------     00120000
       DATA DIVISION.                                                   00130000
       FILE SECTION.                                                    00140000
       FD   FDATE                                                       00141001
            RECORDING F                                                         
            RECORD CONTAINS 80                                          00142001
            BLOCK  CONTAINS 0                                           00143001
            LABEL RECORD STANDARD.                                      00144001
       01   EFDATE       PIC X(80).                                     00145001
       FD   QMF001A                                                     00141001
            RECORDING F                                                         
            RECORD CONTAINS 80                                          00142001
            BLOCK  CONTAINS 0                                           00143001
            LABEL RECORD STANDARD.                                      00144001
       01   EQMF001A     PIC X(80).                                     00145001
       FD   QMF001B                                                     00141001
            RECORDING F                                                         
            RECORD CONTAINS 80                                          00142001
            BLOCK  CONTAINS 0                                           00143001
            LABEL RECORD STANDARD.                                      00144001
       01   EQMF001B     PIC X(80).                                     00145001
       FD   QMF001C                                                     00141001
            RECORDING F                                                         
            RECORD CONTAINS 80                                          00142001
            BLOCK  CONTAINS 0                                           00143001
            LABEL RECORD STANDARD.                                      00144001
       01   EQMF001C     PIC X(80).                                     00145001
       FD   QMF001D                                                     00141001
            RECORDING F                                                         
            RECORD CONTAINS 80                                          00142001
            BLOCK  CONTAINS 0                                           00143001
            LABEL RECORD STANDARD.                                      00144001
       01   EQMF001D     PIC X(80).                                     00145001
      * *************************                                       00260000
       WORKING-STORAGE SECTION.                                         00270000
       77  COMPIL       PIC X(20)         VALUE ALL '?'.                00291000
      * ============================================================= * 02330000
      *    D E S C R I P T I O N      F I C H I E R      F D A T E    * 02340000
      * ============================================================= * 02350000
       01  WFDATE.                                                      02370000
           03  FDATE-JOUR          PIC  X(02).                          02380000
           03  FDATE-MOIS          PIC  X(02).                          02390000
           03  FDATE-SSAA.                                              02400000
               05  FDATE-SIECLE    PIC  X(02).                          02410000
               05  FDATE-ANNEE     PIC  X(02).                          02420000
           03  FILLER              PIC  X(72).                          02380000
       01  ZDATE.                                                       02370000
           03  ZDATE-SSAA.                                              02400000
               05  ZDATE-SIECLE    PIC  9(02) VALUE ZERO.               02410000
               05  ZDATE-ANNEE     PIC  9(02) VALUE ZERO.               02420000
           03  ZDATE-MOIS          PIC  9(02) VALUE ZERO.               02390000
           03  ZDATE-JOUR          PIC  9(02) VALUE ZERO.               02380000
      * ============================================================= * 02460000
      *        D E S C R I P T I O N      D E S      Z O N E S        * 02470000
      *           D ' A P P E L    M O D U L E    B E T D A T C       * 02480000
      * ============================================================= * 02490000
      *                                                                 02500000
           COPY  WORKDATC.                                              02510000
      *                                                                 02520000
      * ============================================================= * 02530000
      *        D E S C R I P T I O N      D E S      Z O N E S        * 02540000
      *           D ' A P P E L    M O D U L E    A B E N D           * 02550000
      * ============================================================= * 02560000
      *                                                                 02570000
           COPY  ABENDCOP.                                              02580000
      *                                                                 02590000
           EJECT                                                        02600000
      * ============================================================= * 02330000
      *    D E S C R I P T I O N      F I C H I E R      Q M F 0 1 A  * 02340000
      * ============================================================= * 02350000
       01  WQMF001A.                                                    00603001
           03  ENR01A.                                                  00603101
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10) VALUE 'RUN Q014 ('.          00603102
               05 FILLER         PIC X(7)  VALUE '&&JOUR='.             00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 JOURA          PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 FILLER         PIC X(10) VALUE ' FORM=F014'.          00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10)          VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(20)          VALUE  SPACE.       00603102
      * ============================================================= * 02330000
      *    D E S C R I P T I O N      F I C H I E R      Q M F 0 1 B  * 02340000
      * ============================================================= * 02350000
       01  WQMF001B.                                                    00603001
           03  ENR01B.                                                  00603101
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10) VALUE 'RUN Q014 ('.          00603102
               05 FILLER         PIC X(7)  VALUE '&&JOUR='.             00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 JOURB          PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 FILLER         PIC X(10) VALUE ' FORM=F014'.          00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10)          VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(20)          VALUE  SPACE.       00603102
      * ============================================================= * 02330000
      *    D E S C R I P T I O N      F I C H I E R      Q M F 0 1 C  * 02340000
      * ============================================================= * 02350000
       01  WQMF001C.                                                    00603001
           03  ENR01C.                                                  00603101
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10) VALUE 'RUN Q177 ('.          00603102
               05 FILLER         PIC X(8)  VALUE '&&FDATE='.            00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 JOURC          PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 FILLER         PIC X(10) VALUE ' FORM=F177'.          00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10)          VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(19)          VALUE  SPACE.       00603102
      * ============================================================= * 02330000
      *    D E S C R I P T I O N      F I C H I E R      Q M F 0 1 D  * 02340000
      * ============================================================= * 02350000
       01  WQMF001D.                                                    00603001
           03  ENR01D.                                                  00603101
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10) VALUE 'RUN Q177 ('.          00603102
               05 FILLER         PIC X(8)  VALUE '&&FDATE='.            00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 JOURD          PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE ''''.         00603102
               05 FILLER         PIC X(10) VALUE ' FORM=F177'.          00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(10)          VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(8)           VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X              VALUE  SPACE.       00603102
               05 FILLER         PIC X(19)          VALUE  SPACE.       00603102
      * *******************                                             00786002
       PROCEDURE DIVISION.                                              01100000
      * ============================================================= * 05120000
      *           I N I T I A L I S A T I O N      P R O G R A M M E  * 05130000
      * ============================================================= * 05140000
      * ============================================================= * 05120000
      *           I N I T I A L I S A T I O N      D A T E            * 05130000
      * ============================================================= * 05140000
      *                                                                 05150000
       INIT.                                                            05160000
           DISPLAY  '***DEBUT BUR0043******'.                           01133002
           MOVE  WHEN-COMPILED  TO  COMPIL.                             01134000
           DISPLAY  'COMPILATION DU : '  COMPIL.                        01135000
           OPEN INPUT  FDATE.                                           01136003
           OPEN OUTPUT QMF001A.                                         01136003
           OPEN OUTPUT QMF001B.                                         01136003
           OPEN OUTPUT QMF001C.                                         01136003
           OPEN OUTPUT QMF001D.                                         01136003
           MOVE 'BUR0043' TO ABEND-PROG.                                00040002
      *                                                                 05170000
           READ  FDATE  INTO  WFDATE AT  END                            05180000
             MOVE  '***  FICHIER  DATE  VIDE  ***'                      05190000
                                           TO  ABEND-MESS               05200000
             PERFORM  SORTIE-SECOURS                                    05210000
           END-READ.                                                    05220000
      *                                                                 05230000
           MOVE  FDATE-JOUR                TO  GFJOUR.                  05240000
           MOVE  FDATE-MOIS                TO  GFMOIS.                  05250000
           MOVE  FDATE-SIECLE              TO  GFSIECLE.                05260000
           MOVE  FDATE-ANNEE               TO  GFANNEE.                 05270000
           MOVE  '1'                       TO  GFDATA.                  05280000
      *                                                                 05290000
           CALL  'BETDATC'    USING  WORK-BETDATC.                      05300000
      *                                                                 05310000
           IF  GFVDAT                     =  '0'                        05320000
               MOVE  '***  DATE  LUE  DANS  FDATE  INCORRECTE  ***'     05330000
                                           TO  ABEND-MESS               05340000
             PERFORM  SORTIE-SECOURS                                    05350000
           END-IF.                                                      05360000
      * ============================================================= * 05120000
      *           C A L C U L  D A T E  D E  L I V R A J+2            * 05130000
      * ============================================================= * 05140000
      *                                                                 05150000
           COMPUTE GFQNT0 = GFQNT0 + 2.                                         
           MOVE  '3'   TO  GFDATA.                                      05280000
           CALL  'BETDATC'    USING  WORK-BETDATC.                      05300000
      *                                                                 05310000
           IF  GFVDAT                     =  '0'                        05320000
               MOVE  '***  DATE  LUE  DANS  FDATE  INCORRECTE  ***'     05330000
                                           TO  ABEND-MESS               05340000
             PERFORM  SORTIE-SECOURS                                    05350000
           END-IF.                                                      05360000
           DISPLAY  'DATE DE LIVRAISON J+2 ==> ' GFSAMJ-0.              01133002
           MOVE GFSAMJ-0 TO JOURA.                                              
           MOVE GFSAMJ-0 TO JOURC.                                              
      *                                                                 05230000
           MOVE  FDATE-JOUR                TO  GFJOUR.                  05240000
           MOVE  FDATE-MOIS                TO  GFMOIS.                  05250000
           MOVE  FDATE-SIECLE              TO  GFSIECLE.                05260000
           MOVE  FDATE-ANNEE               TO  GFANNEE.                 05270000
           MOVE  '1'                       TO  GFDATA.                  05280000
      *                                                                 05290000
           CALL  'BETDATC'    USING  WORK-BETDATC.                      05300000
      *                                                                 05310000
           IF  GFVDAT                     =  '0'                        05320000
               MOVE  '***  DATE  LUE  DANS  FDATE  INCORRECTE  ***'     05330000
                                           TO  ABEND-MESS               05340000
             PERFORM  SORTIE-SECOURS                                    05350000
           END-IF.                                                      05360000
      * ============================================================= * 05120000
      *           C A L C U L  D A T E  D E  L I V R A J+3            * 05130000
      * ============================================================= * 05140000
      *                                                                 05150000
           COMPUTE GFQNT0 = GFQNT0 + 3.                                         
           MOVE  '3'   TO  GFDATA.                                      05280000
           CALL  'BETDATC'    USING  WORK-BETDATC.                      05300000
      *                                                                 05310000
           IF  GFVDAT                     =  '0'                        05320000
               MOVE  '***  DATE  LUE  DANS  FDATE  INCORRECTE  ***'     05330000
                                           TO  ABEND-MESS               05340000
             PERFORM  SORTIE-SECOURS                                    05350000
           END-IF.                                                      05360000
           DISPLAY  'DATE DE LIVRAISON J+3 ====> ' GFSAMJ-0.            01133002
           MOVE GFSAMJ-0 TO JOURB.                                              
           MOVE GFSAMJ-0 TO JOURD.                                              
           WRITE EQMF001A FROM WQMF001A.                                        
           WRITE EQMF001B FROM WQMF001B.                                        
           WRITE EQMF001C FROM WQMF001C.                                        
           WRITE EQMF001D FROM WQMF001D.                                        
      * ============================================================= * 05120000
      *   F I N    B U R 0 0 4 3                                      * 05130000
      * ============================================================= * 05140000
       SORTIE.                                                          00110002
           DISPLAY  '***FIN   BUR0043******'.                           01240004
           CLOSE FDATE QMF001A QMF001B QMF001C QMF001D.                 01270004
      *{ Ba-Stop-Run-Statement 1.1                                              
      *    STOP RUN.                                                    01270005
      *--                                                                       
           EXIT PROGRAM.                                                        
      *}                                                                        
       SORTIE-SECOURS.                                                  00110002
           CALL 'ABEND' USING ABEND-PROG ABEND-MESS.                    00120002
