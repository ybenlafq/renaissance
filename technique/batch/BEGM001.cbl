      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 12/10/2016 1        
       IDENTIFICATION DIVISION.                                         00020017
       PROGRAM-ID.                  BEGM001.                            00030017
       AUTHOR.    DSA007.                                               00040017
      *------------------------------------------------------------*    00050017
      *      SOUS-PROGRAMME DE  MISE EN MASQUE LE CHAMPS           *    00060017
      *                               A EDITER                     *    00070042
      * DATE CREATION : 02/06/92                                   *    00100017
      *------------------------------------------------------------*    00110017
       ENVIRONMENT DIVISION.                                            00120017
      *{  Tr-Collating-Sequence-EBCDIC 1.0                                      
       CONFIGURATION SECTION.                                           00130017
       OBJECT-COMPUTER. UNIX-MF                                                 
           PROGRAM COLLATING SEQUENCE IS MWEBCDIC.                              
      *{  Tr-Collating-Sequence-EBCDIC 1.0 -Adding Section-                     
       SPECIAL-NAMES.                                                           
           COPY MW-COLSEQ.                                                      
      *}                                                                        
                                                                                
      *}                                                                        
       INPUT-OUTPUT SECTION.                                            00140017
       FILE-CONTROL.                                                    00150017
      *                                                                 00160017
      *                                                                 00190017
       DATA DIVISION.                                                   00200017
       FILE SECTION.                                                    00210017
      *                                                                 00871600
      *                                                                 00871600
      *---------------------------------------------------------------* 00875200
      *                 W O R K I N G  -  S T O R A G E               * 00875300
      *---------------------------------------------------------------* 00875400
      *                                                                 00875500
       WORKING-STORAGE SECTION.                                         00875600
      *                                                                 00875700
       01  WS.                                                                  
           05  W-NOMCALL      PIC X(8)     VALUE SPACES.                        
           COPY ABENDCOP.                                               00875801
      *                                                                 00875901
           COPY CEGM000W.                                               00875801
      *                                                                 00875901
      *---------------------------------------------------------------* 00875200
      *        L I N K A G E   S E C T I O N                          * 00875300
      *---------------------------------------------------------------* 00875400
      *                                                                 00875500
       LINKAGE SECTION.                                                         
           COPY ZWEGMASK.                                                       
      ***************************************************************** 21983010
      *     P R O C E D U R E                                         * 21990010
      ***************************************************************** 22000010
      *                                                                 22010010
       PROCEDURE DIVISION USING ZWORK-MASQUE.                           22020010
      *                                                                 22030010
           PERFORM TRAITEMENT-EGM001.                                   22033099
           GOBACK.                                                              
      *---------------------------------------------------------------  24815099
           COPY CEGM001P.                                                       
      *---------------------------------------------------------------  24820012
       FIN-ANORMALE  SECTION.                                           24830012
      *-----------------------------------------------------------------24840012
           MOVE 'BEGM001'   TO ABEND-PROG                               24860099
           MOVE 'ABEND'     TO W-NOMCALL                                        
           CALL W-NOMCALL USING  ABEND-PROG  ABEND-MESS.                24870099
