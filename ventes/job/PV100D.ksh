#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100D.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/02 AT 09.24.10 BY BURTECA                      
#    STANDARDS: P  JOBSET: PV100D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100DA
       ;;
(PV100DA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV100DAA
       ;;
(PV100DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER HISTORIQUE                                                   
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F91.PV100HD.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV100DAA.BPV108ED
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPV102                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAD
       ;;
(PV100DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV100DAA.BPV108ED
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100DAD.BPV108MD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100DAE
       ;;
(PV100DAE)
       m_CondExec 00,EQ,PV100DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV102  CALCUL DES NIVEAUX D AGREGATION                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAG
       ;;
(PV100DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE DES IMBRICATIONS DES  CODES MARKETING                                 
#    RTGA09D  : NAME=RSGA09D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09D /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGA11D  : NAME=RSGA11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11D /dev/null
#  TABLE DES EDITIONS                                                          
#    RTGA12D  : NAME=RSGA12D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA20D  : NAME=RSGA20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20D /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA25D  : NAME=RSGA25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25D /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA29D  : NAME=RSGA29D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29D /dev/null
#  TABLE CODES DESCRIPTIFS CODIC                                               
#    RTGA53D  : NAME=RSGA53D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53D /dev/null
#  TABLE HISTORIQUE PRMP                                                       
#    RTGG50D  : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50D /dev/null
#  TABLE HISTORIQUE PRMP DACEM                                                 
#    RTGG55D  : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55D /dev/null
#  TABLE DES MVTS  DE STOCK                                                    
#    RTGS40D  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
# ******* TABLE DES ANOS                                                       
#    RSAN00D  : NAME=RSAN00D,MODE=(U,U) - DYNAM=YES                            
# -X-RSAN00D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSAN00D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A2} FPV100 ${DATA}/PTEM/PV100DAD.BPV108MD
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00D  : NAME=RSAN00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00D /dev/null
#  SORTIE FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV102 ${DATA}/PTEM/PV100DAG.BPV102AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV102 
       JUMP_LABEL=PV100DAH
       ;;
(PV100DAH)
       m_CondExec 04,GE,PV100DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV103 MAJ DE LA TABLE HV32                   
#  POUR PGM BPV103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAJ
       ;;
(PV100DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PV100DAG.BPV102AD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100DAJ.BPV103AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_19_5 19 CH 5
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100DAK
       ;;
(PV100DAK)
       m_CondExec 00,EQ,PV100DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV103  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAM
       ;;
(PV100DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32D  : NAME=RSHV32D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A4} FPV103 ${DATA}/PTEM/PV100DAJ.BPV103AD
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  MAJ DE LA TABLE RTHV32                                                      
#    RSHV32D  : NAME=RSHV32D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV32D /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00D  : NAME=RSAN00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00D /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV103 
       JUMP_LABEL=PV100DAN
       ;;
(PV100DAN)
       m_CondExec 04,GE,PV100DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV104                                        
#  POUR PGM BPV104                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAQ
       ;;
(PV100DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV100DAA.BPV108ED
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100DAQ.BPV104AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_21 "     "
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 OR FLD_CH_171_5 EQ CST_3_21 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100DAR
       ;;
(PV100DAR)
       m_CondExec 00,EQ,PV100DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV104  MAJ DE  LA TABLE HV26                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAT
       ;;
(PV100DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE HISTORIQUE DES VENTES DE PSE                                          
#    RTGV26D  : NAME=RSGV26D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV26D /dev/null
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A6} FPV104 ${DATA}/PTEM/PV100DAQ.BPV104AD
#  MAJ DE LA TABLE RTHV26 HISTORIQUE VENTE PSE                                 
#    RSHV26D  : NAME=RSHV26D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV26D /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00D  : NAME=RSAN00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00D /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV104 
       JUMP_LABEL=PV100DAU
       ;;
(PV100DAU)
       m_CondExec 04,GE,PV100DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV107 MAJ DE LA TABLE HV33                   
#  POUR PGM BPV107                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV100DAX
       ;;
(PV100DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV100DAG.BPV102AD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100DAX.BPV107AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100DAY
       ;;
(PV100DAY)
       m_CondExec 00,EQ,PV100DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV107  MAJ DE  LA TABLE HV33                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV100DBA
       ;;
(PV100DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32D  : NAME=RSHV32D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A8} FPV107 ${DATA}/PTEM/PV100DAX.BPV107AD
#  MAJ DE LA TABLE RTHV33                                                      
#    RSHV33D  : NAME=RSHV33D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV33D /dev/null
#  MAJ DE LA TABLE ANOMALIE                                                    
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV107 
       JUMP_LABEL=PV100DBB
       ;;
(PV100DBB)
       m_CondExec 04,GE,PV100DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV100DBD
       ;;
(PV100DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#   TABLE DES CODES VENDEURS                                                   
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
       m_OutputAssign -c 9 -w IPV110 IMPRIM
#  FICHIER FORMATTER POUR EDITIONS GENERALISE (LRECL 222)                      
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG110 ${DATA}/PXX0/F91.BPV110AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100DBE
       ;;
(PV100DBE)
       m_CondExec 04,GE,PV100DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV100DBG
       ;;
(PV100DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE                                                                       
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
       m_OutputAssign -c 9 -w IPV111 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100DBH
       ;;
(PV100DBH)
       m_CondExec 04,GE,PV100DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DBJ PGM=BPV105     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV100DBJ
       ;;
(PV100DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F91.PV100HD.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F91.PV100HD.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  : PGM D EPURATION DE HV32 + HV33                                
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV100DBM
       ;;
(PV100DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTHV32D  : NAME=RSHV32D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV33D  : NAME=RSHV33D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV33D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01D /dev/null
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100DBN
       ;;
(PV100DBN)
       m_CondExec 04,GE,PV100DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV100DZA
       ;;
(PV100DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV100DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
