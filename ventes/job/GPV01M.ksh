#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV01M.ksh                       --- VERSION DU 17/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGPV01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/08/09 AT 15.54.51 BY BURTECB                      
#    STANDARDS: P  JOBSET: GPV01M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  BPV002 :  EXTRACTION DES MODIFICATIONS ET CREATIONS DES PRIX                
#                       EXCEPTIONNELS EN MAGASIN                               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV01MA
       ;;
(GPV01MA)
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV01MAA
       ;;
(GPV01MAA)
       m_CondExec ${EXAAF},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DB2                                                            
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA65   : NAME=RSGA65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE = 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** EXECUTION DU PROGRAMME BPV002 *******                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPV002 ${DATA}/PXX0/GPV01MAA.BPV02BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV002 
       JUMP_LABEL=GPV01MAB
       ;;
(GPV01MAB)
       m_CondExec 04,GE,GPV01MAA ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BPV002BM) + CREATION DU BPV002CM PRG=IPV002         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAD PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAD
       ;;
(GPV01MAD)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GPV01MAA.BPV02BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MAD.BPV02CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_18_5 18 CH 5
 /FIELDS FLD_BI_23_5 23 CH 5
 /FIELDS FLD_BI_15_3 15 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MAE
       ;;
(GPV01MAE)
       m_CondExec 00,EQ,GPV01MAD ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050 : CREATION D'UN FICHIER FCUMULS BPV002DM                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAG PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAG
       ;;
(GPV01MAG)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/GPV01MAD.BPV02CM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GPV01MAG.BPV02DM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV01MAH
       ;;
(GPV01MAH)
       m_CondExec 04,GE,GPV01MAG ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAJ PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAJ
       ;;
(GPV01MAJ)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GPV01MAG.BPV02DM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MAJ.BPV02EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MAK
       ;;
(GPV01MAK)
       m_CondExec 00,EQ,GPV01MAJ ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION IPV002                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAM PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAM
       ;;
(GPV01MAM)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/GPV01MAD.BPV02CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/GPV01MAJ.BPV02EM
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPV002 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV01MAN
       ;;
(GPV01MAN)
       m_CondExec 04,GE,GPV01MAM ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAQ PGM=BPV001     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAQ
       ;;
(GPV01MAQ)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** EXECUTION DU PROGRAMME IPV001 **************                          
       m_FileAssign -d SHR -g ${G_A6} IPV002 ${DATA}/PXX0/GPV01MAA.BPV02BM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPV001 ${DATA}/PXX0/GPV01MAQ.BPV02FM
       m_ProgramExec BPV001 
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BPV002FM) + CREATION DU BPV002GM PRG=IPV001         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAT PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAT
       ;;
(GPV01MAT)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/GPV01MAQ.BPV02FM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MAT.BPV02GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 7 CH 8
 /FIELDS FLD_BI_15_3 15 CH 3
 /FIELDS FLD_BI_21_4 21 CH 4
 /FIELDS FLD_BI_18_3 18 CH 3
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_3 ASCENDING,
   FLD_BI_21_4 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MAU
       ;;
(GPV01MAU)
       m_CondExec 00,EQ,GPV01MAT ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050 : CREATION D'UN FICHIER FCUMULS BPV002HM                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MAX PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MAX
       ;;
(GPV01MAX)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PXX0/GPV01MAT.BPV02GM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GPV01MAX.BPV02HM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV01MAY
       ;;
(GPV01MAY)
       m_CondExec 04,GE,GPV01MAX ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBA PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBA
       ;;
(GPV01MBA)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/GPV01MAX.BPV02HM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MBA.BPV02IM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MBB
       ;;
(GPV01MBB)
       m_CondExec 00,EQ,GPV01MBA ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION IPV001                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBD PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBD
       ;;
(GPV01MBD)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PXX0/GPV01MAT.BPV02GM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PXX0/GPV01MBA.BPV02IM
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPV001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV01MBE
       ;;
(GPV01MBE)
       m_CondExec 04,GE,GPV01MBD ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPV003 :  EXTRACTION DES MODIFICATIONS ET CREATIONS DES PRIX                
#                       EXCEPTIONNELS EN MAGASIN                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBG PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBG
       ;;
(GPV01MBG)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  PARAMETRE SOCIETE = 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DB2                                                            
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA65   : NAME=RSGA65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
#                                                                              
# ****** EXECUTION DU PROGRAMME BPV003 *******                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPV003 ${DATA}/PXX0/GPV01MBG.BPV02JM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV003 
       JUMP_LABEL=GPV01MBH
       ;;
(GPV01MBH)
       m_CondExec 04,GE,GPV01MBG ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BPV002JM) + CREATION DU BPV002KM PRG=IPV003         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBJ PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBJ
       ;;
(GPV01MBJ)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/GPV01MBG.BPV02JM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MBJ.BPV02KM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_5 10 CH 5
 /FIELDS FLD_BI_15_3 15 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_PD_18_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MBK
       ;;
(GPV01MBK)
       m_CondExec 00,EQ,GPV01MBJ ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050 : CREATION D'UN FICHIER FCUMULS BPV002DM                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBM PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBM
       ;;
(GPV01MBM)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PXX0/GPV01MBJ.BPV02KM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GPV01MBM.BPV02LM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV01MBN
       ;;
(GPV01MBN)
       m_CondExec 04,GE,GPV01MBM ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBQ PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBQ
       ;;
(GPV01MBQ)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PXX0/GPV01MBM.BPV02LM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV01MBQ.BPV02MM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV01MBR
       ;;
(GPV01MBR)
       m_CondExec 00,EQ,GPV01MBQ ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : EDITION IPV003                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV01MBT PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MBT
       ;;
(GPV01MBT)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PXX0/GPV01MBJ.BPV02KM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PXX0/GPV01MBQ.BPV02MM
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IPV003 FEDITION
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV01MBU
       ;;
(GPV01MBU)
       m_CondExec 04,GE,GPV01MBT ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV01MZA
       ;;
(GPV01MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV01MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
