#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VE900O.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVE900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/03/14 AT 16.33.00 BY BURTECA                      
#    STANDARDS: P  JOBSET: VE900O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTVE02-05-06-08-10-11-14                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VE900OA
       ;;
(VE900OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VE900OAA
       ;;
(VE900OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PARG/SEM.VE900OA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VE900OAA
       m_ProgramExec IEFBR14 "RDAR,VE900O.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900OAD
       ;;
(VE900OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VE900OA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VE900OAE
       ;;
(VE900OAE)
       m_CondExec 00,EQ,VE900OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAG
       ;;
(VE900OAG)
       m_CondExec ${EXAAK},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11O  : NAME=RSVE11O,MODE=I - DYNAM=YES                                
#    RSVE14O  : NAME=RSVE14O,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900OAG.VE900O01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900OAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAJ
       ;;
(VE900OAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11X  : NAME=RSVE11X,MODE=I - DYNAM=YES                                
#    RSVE14X  : NAME=RSVE14X,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900OAJ.UNVE11AO
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900OAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FIC SUR NLIEU,NVENTE 4,10,A                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAM
       ;;
(VE900OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VE900OAG.VE900O01
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SORTOUT ${DATA}/PTEM/VE900OAM.VE900O02
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900OAN
       ;;
(VE900OAN)
       m_CondExec 00,EQ,VE900OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9000                                                               
#   PREPARATION FICHIER PURGE ET FICHIER DE LOAD RTVE11                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAQ
       ;;
(VE900OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#         TABLE RTVE11                                                         
#    RSVE11O  : NAME=RSVE11O,MODE=(U,N) - DYNAM=YES                            
# -X-VE900OR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSVE11O /dev/null
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  RTVE11 TRI PAR LIEU ET NVENTE                                               
       m_FileAssign -d SHR -g ${G_A3} VE0011A ${DATA}/PTEM/VE900OAM.VE900O02
#  FICHIER POUR LE LOAD DE LA RTVE11                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011L ${DATA}/PARG/SEM.VE900OB
#  FICHIER DE PURGE DE LA RTVE11                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011P ${DATA}/PARG/SEM.VE900OC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9000 
       JUMP_LABEL=VE900OAR
       ;;
(VE900OAR)
       m_CondExec 04,GE,VE900OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVE11                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAT
       ;;
(VE900OAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSVE11O  : NAME=RSVE11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11O /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PARG/SEM.VE900OB
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/VE900OAJ.UNVE11AO
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900OAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VE900O_VE900OAT_RTVE11.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900OAU
       ;;
(VE900OAU)
       m_CondExec 04,GE,VE900OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE910                                                                
#  EPURATION DES TABLES RTVE10 RTVE11                                          
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VE900OAX
       ;;
(VE900OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ENTETES DE VENTES                                                 
#    RSVE10O  : NAME=RSVE10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE10O /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RSVE11O  : NAME=RSVE11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11O /dev/null
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 FVE910 ${DATA}/PARG/SEM.VE900OD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE910 
       JUMP_LABEL=VE900OAY
       ;;
(VE900OAY)
       m_CondExec 04,GE,VE900OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DE PURGE POUR LE PASSER A UN LRECL = 20                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBA
       ;;
(VE900OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PARG/SEM.VE900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/VE900OBA.VE900O03
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_52_3 52 PD 3
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_52_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900OBB
       ;;
(VE900OBB)
       m_CondExec 00,EQ,VE900OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BVE910 ET DU TRI PRECEDENT POUR CREER            
#  LE FICHIER VE900OE                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBD
       ;;
(VE900OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/VE900OBA.VE900O03
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PARG/SEM.VE900OD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PARG/SEM.VE900OE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900OBE
       ;;
(VE900OBE)
       m_CondExec 00,EQ,VE900OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9002                                                               
#  PURGE DE LA TABLE RTVE02 TABLE DES ADRESSES                                 
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBG
       ;;
(VE900OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENTES ADRESSES                                                   
#    RSVE02O  : NAME=RSVE02O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE02O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBG
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A9} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 FVE020 ${DATA}/PARG/SEM.VE900OF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9002 
       JUMP_LABEL=VE900OBH
       ;;
(VE900OBH)
       m_CondExec 04,GE,VE900OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9005                                                               
#  PURGE DE LA TABLE RTVE05                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBJ
       ;;
(VE900OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE05O  : NAME=RSVE05O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE05O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBJ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A10} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 FVE005 ${DATA}/PARG/SEM.VE900OG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9005 
       JUMP_LABEL=VE900OBK
       ;;
(VE900OBK)
       m_CondExec 04,GE,VE900OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9006                                                               
#  PURGE DE LA TABLE RTVE06                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBM
       ;;
(VE900OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE06O  : NAME=RSVE06O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE06O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBM
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A11} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 FVE006 ${DATA}/PARG/SEM.VE900OH
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9006 
       JUMP_LABEL=VE900OBN
       ;;
(VE900OBN)
       m_CondExec 04,GE,VE900OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9008                                                               
#  PURGE DE LA TABLE RTVE08                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBQ
       ;;
(VE900OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE08O  : NAME=RSVE08O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE08O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A12} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 FVE008 ${DATA}/PARG/SEM.VE900OI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9008 
       JUMP_LABEL=VE900OBR
       ;;
(VE900OBR)
       m_CondExec 04,GE,VE900OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9014                                                               
#  PURGE DE LA TABLE DES REGLEMENTS                                            
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBT
       ;;
(VE900OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES REGLEMENTS                                                        
#    RSVE14O  : NAME=RSVE14O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE14O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBT
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A13} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 FVE140 ${DATA}/PARG/SEM.VE900OJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9014 
       JUMP_LABEL=VE900OBU
       ;;
(VE900OBU)
       m_CondExec 04,GE,VE900OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9010                                                               
#  PURGE DE LA TABLE RTVE10                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VE900OBX
       ;;
(VE900OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSVE10O  : NAME=RSVE10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE10O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900OBX
#  FICHIER                                                                     
       m_FileAssign -d SHR -g ${G_A14} FVE900 ${DATA}/PARG/SEM.VE900OE
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 FVE100 ${DATA}/PARG/SEM.VE900OK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9010 
       JUMP_LABEL=VE900OBY
       ;;
(VE900OBY)
       m_CondExec 04,GE,VE900OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VE900OZA
       ;;
(VE900OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
