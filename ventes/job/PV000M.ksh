#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV000M.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPV000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/16 AT 15.19.05 BY BURTECA                      
#    STANDARDS: P  JOBSET: PV000M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BPV100  EXTRACTION DES PSE VENDUES                                 
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV000MA
       ;;
(PV000MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV000MAA
       ;;
(PV000MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00M  : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00M /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14M  : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14M /dev/null
#  TABLE DES MONTANTS DE GARANTIE COMPLEMENTAIRE                               
#    RTGA40M  : NAME=RSGA40M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA40M /dev/null
#  TABLE DES DE GARANTIE COMPLEMENTAIRE                                        
#    RTGA52M  : NAME=RSGA52M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52M /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGV11M  : NAME=RSGV11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11M /dev/null
#  TABLE DES CODES VENDEURS                                                    
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F89.FDPV100M
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  DATE DU JOUR JJMMSSAA                                                       
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
#  SORTIE FICHIER PRIMES VENDEURS DU JOUR                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100 ${DATA}/PTEM/PV000MAA.BPV100AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV100 
       JUMP_LABEL=PV000MAB
       ;;
(PV000MAB)
       m_CondExec 04,GE,PV000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV106  EXTRACTION DES VENTES DE PRODUITS GROUPE                   
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAD
       ;;
(PV000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00M  : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00M /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14M  : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14M /dev/null
#  TABLE DES PARAMETRES ASSOCIES AUX FAMILLES                                  
#    RTGA30M  : NAME=RSGA30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30M /dev/null
#  TABLE DES GARANTIES COMPLEMENTAIRES                                         
#    RTGA52M  : NAME=RSGA52M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52M /dev/null
#  TABLE DES LIENS                                                             
#    RTGA58M  : NAME=RSGA58M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA58M /dev/null
#  TABLE DES ZONES DE PRIX STANDARDS                                           
#    RTGA59M  : NAME=RSGA59M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59M /dev/null
#  TABLE DES COMMISSIONS PAR ZONES DE PRIX                                     
#    RTGA62M  : NAME=RSGA62M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA62M /dev/null
#  TABLE DES HISTORIQUES STATUTS                                               
#    RTGA65M  : NAME=RSGA65M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65M /dev/null
#  TABLE DES HISTORIQUES SENSIBILITES                                          
#    RTGA66M  : NAME=RSGA66M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA66M /dev/null
#  TABLE COMMISSIONS BBTE                                                      
#    RTGA75M  : NAME=RSGA75M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA75M /dev/null
#  TABLE HISTORIQUE DES PRMP                                                   
#    RTGG50M  : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50M /dev/null
#  TABLE HISTORIQUE DES PRMP DACEM                                             
#    RTGG55M  : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55M /dev/null
#  TABLE DES MVTS DE STOCK                                                     
#    RTGS40M  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40M /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F89.FDPV100M
#  SORTIE FICHIER VENDEURS SUR CODICS GROUPE                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV106 ${DATA}/PTEM/PV000MAD.BPV106AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV106 
       JUMP_LABEL=PV000MAE
       ;;
(PV000MAE)
       m_CondExec 04,GE,PV000MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV101  PRIMES VENDEURS SUR VENTES DE CODICS ELEMENTS              
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAG
       ;;
(PV000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00M  : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00M /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14M  : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14M /dev/null
#  TABLE DES PARAMETRES ASSOCIES AUX FAMILLES                                  
#    RTGA30M  : NAME=RSGA30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30M /dev/null
#  TABLE DES GARANTIES COMPLEMENTAIRES                                         
#    RTGA52M  : NAME=RSGA52M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52M /dev/null
#  TABLE DES PRIMES JAUNES                                                     
#    RTGA62M  : NAME=RSGA62M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA62M /dev/null
#  TABLE DES HISTORIQUES STATUTS                                               
#    RTGA65M  : NAME=RSGA65M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65M /dev/null
#  TABLE DES HISTORIQUES SENSIBILITES                                          
#    RTGA66M  : NAME=RSGA66M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA66M /dev/null
#  TABLE COMMISSION BBTE                                                       
#    RTGA75M  : NAME=RSGA75M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA75M /dev/null
#  TABLE DES PRMP                                                              
#    RTGG50M  : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50M /dev/null
#  TABLE HISTORIQUE DES PRMP DACEM                                             
#    RTGG55M  : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55M /dev/null
#  TABLE DES MVTS DE STOCK                                                     
#    RTGS40M  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40M /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F89.FDPV100M
#  SORTIE FICHIER VENDEURS SUR CODICS ELEMENTS                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV101 ${DATA}/PTEM/PV000MAG.BPV101AM
#  DATE DU DERNIER TRAITEMENT GENERATION +1 DU FDPV100                         
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDPV100O ${DATA}/PXX0/F89.FDPV100M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV101 
       JUMP_LABEL=PV000MAH
       ;;
(PV000MAH)
       m_CondExec 04,GE,PV000MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL DES FICHIERS DU JOUR SUR UN FICHIER HISTORIQUE DU JOUR                
#  TRI  SUR FICHIER 32,10                                                      
#  SORT FIELDS=(1,3,A,4,3,A,32,10,A,7,6,A,19,5,A,49,5,A AVANT MOFIF FA         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAJ
       ;;
(PV000MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV000MAA.BPV100AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PV000MAG.BPV101AM
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/PV000MAD.BPV106AM
# * FICHIER HISTO DU JOUR                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F89.BPV100JM.JOUR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_49_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000MAK
       ;;
(PV000MAK)
       m_CondExec 00,EQ,PV000MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAM PGM=BPV108     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAM
       ;;
(PV000MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F89.PV100HM.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV000MAM.BPV108AM
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI SUR FICHIER ISSU PGM BPV108                                             
#  REPRISE: OUI.                                                               
#  ATTENTION SI NCGFC (BPV11 PARAMETRE = OUI) TRI SUR CODE VENDEUR             
#  SORT FIELDS=(1,3,A,4,3,A,7,6,A,32,10,A,66,1,A),FORMAT=CH                    
#  SINON                                      TRI SUR LIBELLE VENDEUR          
#  SORT FIELDS=(1,3,A,4,3,A,32,10,A,7,6,A,66,1,A),FORMAT=CH                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAQ
       ;;
(PV000MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PV000MAM.BPV108AM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV000MAQ.BPV109AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "G"
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_7_6 07 CH 06
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000MAR
       ;;
(PV000MAR)
       m_CondExec 00,EQ,PV000MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV109  EDITION DU DETAIL DES VENTES ET DU CUMUL MENSUEL           
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAT
       ;;
(PV000MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE DES ARTICLES GENERALISE                                               
#    RTGA01D  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  TABLE DES ANOMALIES                                                         
#    RTAN00D  : NAME=RSAN00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00D /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$VDATEJ__JJMMANN
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
       m_FileAssign -d SHR -g ${G_A5} FPV109J ${DATA}/PEX0/F89.BPV100JM.JOUR
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FPV109M ${DATA}/PTEM/PV000MAQ.BPV109AM
#  SORTIE FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 IPV109 ${DATA}/PXX0/F89.BPV109MM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV109 
       JUMP_LABEL=PV000MAU
       ;;
(PV000MAU)
       m_CondExec 04,GE,PV000MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV109 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV000MAX
       ;;
(PV000MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F89.BPV109MM
       m_OutputAssign -c 9 -w IPV109 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PV000MAY
       ;;
(PV000MAY)
       m_CondExec 00,EQ,PV000MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR FICHIERS ISSU DU REGROUPEMENT JOURNALIER + HISTO                    
#  POUR CONSTITUTION FICHIER HISTORIQUE                                        
#  REPRISE: OUI MAIS ATTENTION AUX GENERATIONS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000MBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV000MBA
       ;;
(PV000MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/PV000MAA.BPV100AM
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/PV000MAG.BPV101AM
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/PV000MAD.BPV106AM
# DOIT ETRE INITIALISE POUR LE 1ER PASSAGE                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F89.PV100HM.HISTO
# FICHIER HISTORIQUE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F89.PV100HM.HISTO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000MBB
       ;;
(PV000MBB)
       m_CondExec 00,EQ,PV000MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV000MZA
       ;;
(PV000MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV000MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
