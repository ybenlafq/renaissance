#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD1D.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGNMD1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/10 AT 09.53.28 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD1D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF866 : CREATION DU FICHIER DE COMPARAISON PRIX DE VENTE                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD1DA
       ;;
(GNMD1DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       RUN=${RUN}
       JUMP_LABEL=GNMD1DAA
       ;;
(GNMD1DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF866 ${DATA}/P991/SEM.BTF866AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF866 
       JUMP_LABEL=GNMD1DAB
       ;;
(GNMD1DAB)
       m_CondExec 04,GE,GNMD1DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF867 : CREATION DU FICHIER DE COMPARAISON HISTO PRIX DE VENTE             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD1DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD1DAD
       ;;
(GNMD1DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF867 ${DATA}/P991/SEM.BTF867AD
# ******* NOUVEAU FICHIER POUR HADOOP ADO02P                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 HAD867 ${DATA}/PXX0/F91.BTF867BD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF867 
       JUMP_LABEL=GNMD1DAE
       ;;
(GNMD1DAE)
       m_CondExec 04,GE,GNMD1DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF868 : CREATION DU FICHIER DE COMPARAISON HISTO STATUT                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD1DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD1DAG
       ;;
(GNMD1DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
#    RSGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGN68 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF868 ${DATA}/P991/SEM.BTF868AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF868 
       JUMP_LABEL=GNMD1DAH
       ;;
(GNMD1DAH)
       m_CondExec 04,GE,GNMD1DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF869 : CREATION DU FICHIER DE COMPARAISON PRIX DE CESSION/PRMP            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD1DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD1DAJ
       ;;
(GNMD1DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA59   : NAME=RSGA56D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGA75   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF869 ${DATA}/P991/SEM.BTF869AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF869 
       JUMP_LABEL=GNMD1DAK
       ;;
(GNMD1DAK)
       m_CondExec 04,GE,GNMD1DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF873 : EXTRACTION GG20 DU JOUR                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD1DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD1DAM
       ;;
(GNMD1DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION ALLANT VERS GNMD3                               
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF873 ${DATA}/PXX0/F91.BTF873AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF873 
       JUMP_LABEL=GNMD1DAN
       ;;
(GNMD1DAN)
       m_CondExec 04,GE,GNMD1DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF876 : EXTRACTION TH12 / FR50 POUR FRANCHISE.                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD1DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD1DAQ
       ;;
(GNMD1DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTTH12 /dev/null
#    RTFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFR50 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION ALLANT VERS GNMD3                               
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF876 ${DATA}/PXX0/F91.BTF876AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF876 
       JUMP_LABEL=GNMD1DAR
       ;;
(GNMD1DAR)
       m_CondExec 04,GE,GNMD1DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
