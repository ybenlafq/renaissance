#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS052P.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFS052 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/24 AT 12.36.18 BY BURTECA                      
#    STANDARDS: P  JOBSET: FS052P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BCX085                                                                
# ********************************************************************         
#  EXTRACTION DES LIGNES DE REGLEMENTS                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS052PA
       ;;
(FS052PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FS052PAA
       ;;
(FS052PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ************** TABLE DES ENCAISSEMENTS                                       
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ************** TABLE DES ADRESSES                                            
#    RTGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV02 /dev/null
# ************** FICHIER D'EXTRACTION                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 69 -t LSEQ -g +1 FCX85 ${DATA}/PTEM/FS052PAA.BFX085AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCX085 
       JUMP_LABEL=FS052PAB
       ;;
(FS052PAB)
       m_CondExec 04,GE,FS052PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DE L'EXTRACTION                                         
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FS052PAD
       ;;
(FS052PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FS052PAA.BFX085AP
       m_FileAssign -d NEW,CATLG,DELETE -r 69 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FS052PAD.BFX090AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_29 1 CH 29
 /KEYS
   FLD_CH_1_29 ASCENDING
 /* Record Type = F  Record Length = 69 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FS052PAE
       ;;
(FS052PAE)
       m_CondExec 00,EQ,FS052PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ENCAISSEMENTS EFFECTUES PAR LES LIVREURS                        
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052PAG PGM=BCX090     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FS052PAG
       ;;
(FS052PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ************** FICHIER ISSU DU TRI                                           
       m_FileAssign -d SHR -g ${G_A2} FCX90T ${DATA}/PTEM/FS052PAD.BFX090AP
#    SUPRESSION DE L'EDITION LE 25 04 2001 CGA                                 
       m_OutputAssign -c "*" ICX090
       m_ProgramExec BCX090 
# ********************************************************************         
#  BFS052 *   COBOL2/DB2/DL1                                                   
# *********                                                                    
#       COMPTABILISATION DES VENTES TOPEES DU JOUR                             
#       VERIFIE QUE LE FICHIER FFV001 PRECEDENT A BIEN ETE DEVERSE             
#       DANS GFV.(NUTIL = 'O' OU 'N' DANS RTFV19)                              
#       SI VALEUR = 'N' ABANDON DU PROGRAMME                                   
#       SI VALEUR = 'O' MISE A JOUR EFFECTUEES + NUTIL A 'N'                   
#                                                                              
#       CALL AU SOUS PROG BFX053 : MAJ DES FICHIER PSE                         
#       CALL AU SOUS PROG MCS005 : EVITE ACCES TL0102AP                        
#                                                                              
# *********                                                                    
#   REPRISE OUI APRES EXECUTION DU BACKOUT CORTRANS                            
#       ATTENTION SEULS LES VSAM SONT REMIS EN ETAT AVANT M.A.J                
#       LE PROGRAMME PREND EN CHARGE LES M.A.J DEJA EFFECTUEES SUR             
#       LES TABLES DB2 EN CAS DE REPRISE.                                      
#                                                                              
#   LA REPRISE DOIT OBLIGATOIREMENT SE FAIRE :                                 
#   1. AVEC LA FDATE DU PLANTAGE                                               
#   2. AVEC LE PARAM�TRE DCOMPTA EGAL _A FDATE (FORMAT JJMMSSAA)                
#      (DATE DE PREP)                                                          
#   ET                                                                         
#   3. SAUVEGARDER LE FICHIER FFV001 (BFS052A*) POUR LA COMPTA (FV001*         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FS052PAJ
       ;;
(FS052PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE DES PARAMETRES FAMILLES                                        
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE DES RUBRIQUES BUDGETAIRE                                       
#    RSGA90   : NAME=RSGA90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA90 /dev/null
# ******* TABLE ADRESSE CLIENTS                                                
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ******* TABLE ENTETE DE VENTES                                               
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE DES COMPTES                                                    
#    RSFX00   : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE D'ATTRIBUTION DES N�DE PIECES                                  
#    RSFX05   : NAME=RSFX05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX05 /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE VENTE PRESTATION                                               
#    RSGV13   : NAME=RSGV13,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV13 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES SEQUENCES D'INTERFACES                                     
#    RSFT29   : NAME=RSFT29,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMANTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPS00 /dev/null
# ******* FICHIER DES VENTES VERS GFV                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FFV001 ${DATA}/PNCGP/F07.BFS052AP
# ******* FICHIER DES ANNULATIONS POUR PGM BPS020 HEBDO                        
       m_FileAssign -d NEW,CATLG,DELETE -r 56 -t LSEQ -g +1 FX053 ${DATA}/PTEM/FS052PAJ.BFX053AP
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PXX0/F07.BFS052CP
# ******* FICHIER DES PRETATIONS                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052PAJ.BFS052EP
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IFX052 IFX052
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FX052COM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS052P
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS052 
       JUMP_LABEL=FS052PAK
       ;;
(FS052PAK)
       m_CondExec 04,GE,FS052PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BBS052 : DUPLICATION DU BFS052 POUR BIENS ET SERVICES                   
#  -------                                                                     
#  REPRISE : OUI (VOIR EXPLICATION EN D�BUT DE PCL)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FS052PAM
       ;;
(FS052PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN ENTREE                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
#    RSBC12   : NAME=RSBC12P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC12 /dev/null
#    RSBC15   : NAME=RSBC15P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC15 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR30   : NAME=RSPR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR30 /dev/null
#    RSPR55   : NAME=RSPR55P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR55 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSFT23   : NAME=RSFT23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT23 /dev/null
#                                                                              
# ******* TABLES EN MAJ                                                        
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMANTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPS00 /dev/null
#                                                                              
# ******* FICHIERS EN SORTIE                                                   
# ******* FICHIER DES VENTES VERS FTICSP                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PNCGP/F07.BBS052AP
# ******* FICHIER DES ANNULATIONS                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 56 -t LSEQ -g +1 FX053 ${DATA}/PXX0/F07.BBS053AP
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PNCGP/F07.BBS052CP
# ******* FICHIER DES PRETATIONS                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052PAM.BBS052EP
#                                                                              
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IBS052 IFX052
#                                                                              
# ******* FICHIERS PARAM�TRE                                                   
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/BS052POM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/BS052P
#                                                                              
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.FDATMGDP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS052 
       JUMP_LABEL=FS052PAN
       ;;
(FS052PAN)
       m_CondExec 04,GE,FS052PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL DU FICHIER DES PRESTATIONS                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FS052PAQ
       ;;
(FS052PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/FS052PAJ.BFS052EP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BFS052FP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/FS052PAM.BBS052EP
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGP/F07.BFS052FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_76_6 76 CH 6
 /FIELDS FLD_CH_82_5 82 CH 5
 /KEYS
   FLD_CH_82_5 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_52_8 ASCENDING,
   FLD_CH_76_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FS052PAR
       ;;
(FS052PAR)
       m_CondExec 00,EQ,FS052PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FS052PZA
       ;;
(FS052PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FS052PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
