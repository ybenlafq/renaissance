#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VI000P.ksh                       --- VERSION DU 08/10/2016 14:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVI000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/17 AT 09.48.28 BY BURTECA                      
#    STANDARDS: P  JOBSET: VI000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DE LA TABLE RTVI01                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VI000PA
       ;;
(VI000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/11/17 AT 09.48.28 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: VI000P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'VTE EXP NON TOPEES'                    
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VI000PAA
       ;;
(VI000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
#  TABLE DES RESERVATION SUR COMMANDES FOURNISSEURS                            
#    RTVI01   : NAME=RSVI01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTVI01 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VI000PAA.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VI000PAB
       ;;
(VI000PAB)
       m_CondExec 04,GE,VI000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   INSERT DANS LA TABLE RTVI01                                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VI000PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VI000PAD
       ;;
(VI000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  TABLE DES RESERVATION SUR COMMANDES FOURNISSEURS                            
#    RTVI01   : NAME=RSVI01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTVI01 /dev/null
#    RTVI00   : NAME=RSVI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVI00 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11Y,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11L,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VI000PAD.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VI000PAE
       ;;
(VI000PAE)
       m_CondExec 04,GE,VI000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
