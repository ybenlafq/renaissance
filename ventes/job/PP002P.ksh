#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PP002P.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPP002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/01 AT 09.08.56 BY BURTECC                      
#    STANDARDS: P  JOBSET: PP002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  FUSION DES FICHIERS SIG001B                                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PP002PA
       ;;
(PP002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/08/01 AT 09.08.56 BY BURTECC                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: PP002P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'INTERFACE NCG/SIGA'                    
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PP002PAA
       ;;
(PP002PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.SIG001BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.SIG001BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.SIG001BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.SIG001BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.SIG001BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.SIG001BD
       m_FileAssign -d NEW,CATLG,DELETE -r 913 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.SIG001CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 0
 /FIELDS FLD_CH_1_24 1 CH 24
 /FIELDS FLD_ZD_138_12 138 ZD 12
 /FIELDS FLD_CH_95_3 95 CH 3
 /CONDITION CND_1 FLD_ZD_138_12 EQ CST_1_5 
 /KEYS
   FLD_CH_1_24 ASCENDING,
   FLD_CH_95_3 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP002PAB
       ;;
(PP002PAB)
       m_CondExec 00,EQ,PP002PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER SIG001CP VERS UNIX                                         
# ********************************************************************         
# *AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                      
# *CFTIN    DATA  *                                                            
# *SEND PART=HRPROD,                                                           
# *     IDF=SIG001CP,                                                          
# *     FNAME=":SIG001CP""(0)                                                  
# *         DATAEND                                                            
# ********************************************************************         
#  ENVOI DU FICHIER SIG001CP VERS GATEWAY                                      
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SIG001CP,                                                           
#      FNAME=":SIG001CP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  PGM WAITSTOP POUR PLAN POUR AVOIR LA DUREE EXACTE DE LA RECUP               
#  LIBER� PAR LA PROC DE FIN DE TRANSFERT CFT (=> TRANSFERT OK)                
# ********************************************************************         
# AAP      STEP  PGM=WAITSTOP,RSTRT=SAME,TIME=1440                             
# //SYSPRINT DD SYSOUT=*                                                       
# //SYSOUT   DD SYSOUT=*                                                       
#                                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPP002P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP002PAD PGM=FTP        ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PP002PAD
       ;;
(PP002PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PP002PAD.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PP002PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PP002PAG
       ;;
(PP002PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PP002PAG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
