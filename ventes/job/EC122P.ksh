#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC122P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC122 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/20 AT 09.35.31 BY BURTECA                      
#    STANDARDS: P  JOBSET: EC122P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *****************************************************************            
#  DELETE DU FICHIER SEQUENTIEL PUR PXX0.F07.FEC122ER                          
#  REPRISE : OUI                                                               
# *****************************************************************            
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC122PA
       ;;
(EC122PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC122PAA
       ;;
(EC122PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=EC122PAB
       ;;
(EC122PAB)
       m_CondExec 16,NE,EC122PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DE LA TABLE RTCM00                                       
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAD
       ;;
(EC122PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
#    RSCM00   : NAME=RSCM00,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/EC122PAD.FEC122AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHIER DU JOUR DE LA TABLE RTCM00                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAG
       ;;
(EC122PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EC122PAD.FEC122AP
# ******* FICHIER FEC106 DU JOUR                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FEC122BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_4_7 1" "
 /DERIVEDFIELD CST_7_10 1" "
 /DERIVEDFIELD PAD_7_9 230" "
 /DERIVEDFIELD PAD_1_3 3" "
 /DERIVEDFIELD CST_1_4 1" "
 /DERIVEDFIELD PAD_4_6 73" "
 /FIELDS FLD_CH_9_22 9 CH 22
 /FIELDS FLD_CH_3_6 3 CH 6
 /FIELDS FLD_CH_3_28 3 CH 28
 /KEYS
   FLD_CH_3_28 ASCENDING
 /* Record Type = F  Record Length = 385 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_6,PAD_1_3,CST_1_4,FLD_CH_9_22,PAD_4_6,CST_4_7,FLD_CH_9_22,PAD_7_9,CST_7_10
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=EC122PAH
       ;;
(EC122PAH)
       m_CondExec 00,EQ,EC122PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC900  -  COMPARAISON FICHIERS J ET J-1                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAJ
       ;;
(EC122PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A2} FEC90001 ${DATA}/PXX0/F07.FEC122BP
# FEC90002 FILE  NAME=FEC122BP,MODE=I,GEN=0                                    
       m_FileAssign -d SHR FEC90002 /dev/null
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC90003 ${DATA}/PTEM/EC122PAJ.FEC122CP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC900 
       JUMP_LABEL=EC122PAK
       ;;
(EC122PAK)
       m_CondExec 04,GE,EC122PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC110  -  MISE EN FORME XML ET ENVOI MQ                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAM
       ;;
(EC122PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE - CONTIENT PARAM POUR MQ                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A3} FEC11001 ${DATA}/PTEM/EC122PAJ.FEC122CP
# ******* FICHIER FEC106X - FICHIER DES ENREGISTREMENTS XML PRODUITS           
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ -g +1 FEC11002 ${DATA}/PTEM/EC122PAM.FEC122DP
# ******  LES PARAM POUR MQ SONT DANS                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC110 
       JUMP_LABEL=EC122PAN
       ;;
(EC122PAN)
       m_CondExec 04,GE,EC122PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCM801 SUPPRESSION DES HEADER XML                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAQ PGM=BCM801     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAQ
       ;;
(EC122PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ENTRE  :                                                             
       m_FileAssign -d SHR -g ${G_A4} FCM801I ${DATA}/PTEM/EC122PAM.FEC122DP
# ******  ENTRE  :                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ FCM801O ${DATA}/PXX0.F07.FEC122GP
       m_ProgramExec BCM801 
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=*,PATTERN=JVZIP                                           
# ****** FICHIER EN ENTREE DE ZIP                                              
# DD1      FILE  NAME=FEC122GP,MODE=I                                          
# ****** FICHIER EN SORTIE DE ZIP                                              
# FICZIP   FILE  NAME=FEC122EP,MODE=O                                          
#                                                                              
# SYSIN    DATA  *                                                             
# -x CP1252                                                                    
# //DD:FICZIP                                                                  
# //DD:DD1 -n FEC122EP.TXT                                                     
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAT PGM=JVMLDM76   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAT
       ;;
(EC122PAT)
       m_CondExec ${EXABE},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR DD1 ${DATA}/PXX0.F07.FEC122GP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 1800 -t LSEQ FICZIP ${DATA}/PXX0.F07.FEC122EP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PAT.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  REPRO DU FICHIER                                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EC122PAX
       ;;
(EC122PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR INPUT ${DATA}/PXX0.F07.FEC122GP
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ -g +1 OUTPUT ${DATA}/PXX0/F07.FEC122X
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PAX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=EC122PAY
       ;;
(EC122PAY)
       m_CondExec 16,NE,EC122PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FTP SUR LA GATEWAY DU FTEC122P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC122PBA PGM=FTP        ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EC122PBA
       ;;
(EC122PBA)
       m_CondExec ${EXABO},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PBA.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC122PZA
       ;;
(EC122PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC122PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
