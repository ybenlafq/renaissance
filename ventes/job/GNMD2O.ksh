#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD2O.ksh                       --- VERSION DU 08/10/2016 22:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGNMD2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/21 AT 14.32.50 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD2O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF870 : MISE � JOUR DE CERTAINES SOUS TABLES DE LA RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD2OA
       ;;
(GNMD2OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A86=${G_A86:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GNMD2OAA
       ;;
(GNMD2OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF870 
       JUMP_LABEL=GNMD2OAB
       ;;
(GNMD2OAB)
       m_CondExec 04,GE,GNMD2OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF872 :  MAJ FLAG RTLI00 DES LIEUX MIGRES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAD
       ;;
(GNMD2OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF872 
       JUMP_LABEL=GNMD2OAE
       ;;
(GNMD2OAE)
       m_CondExec 04,GE,GNMD2OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF500 : EXTRACTION DE LA TABLE DES DEVISE RTFM04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAG
       ;;
(GNMD2OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE DES DEVISE                                                     
#    RSFM04D  : NAME=RSFM04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF500 ${DATA}/PTEM/GNMD2OAG.BTF500AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF500 
       JUMP_LABEL=GNMD2OAH
       ;;
(GNMD2OAH)
       m_CondExec 04,GE,GNMD2OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF505 : EXTRACTION DE LA TABLE RTFM05                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAJ
       ;;
(GNMD2OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSFM05   : NAME=RSFM05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF505 ${DATA}/PTEM/GNMD2OAJ.BTF505AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF505 
       JUMP_LABEL=GNMD2OAK
       ;;
(GNMD2OAK)
       m_CondExec 04,GE,GNMD2OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF598 : EXTRACTION DE LA TABLE RTGA00                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAM
       ;;
(GNMD2OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIS�E                                                    
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALIS�E                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA58   : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA67   : NAME=RSGA67O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE ANOMALIES                                                      
#    RSAN00   : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF598 ${DATA}/PTEM/GNMD2OAM.BTF598AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF598 
       JUMP_LABEL=GNMD2OAN
       ;;
(GNMD2OAN)
       m_CondExec 04,GE,GNMD2OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF606 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAQ
       ;;
(GNMD2OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA06   : NAME=RSGA06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF606 ${DATA}/PTEM/GNMD2OAQ.BTF606AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF606 
       JUMP_LABEL=GNMD2OAR
       ;;
(GNMD2OAR)
       m_CondExec 04,GE,GNMD2OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF611 : CREATION DU FICHIER DE COMPARAISON POUR LES TABLES                 
#                    RTGA09, RTGA11, RTGA12 ET RTGA29                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAT
       ;;
(GNMD2OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA11   : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA29   : NAME=RSGA29O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS D'EXTRACTION DES ARTICLES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611A ${DATA}/PTEM/GNMD2OAT.BTF611AO
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611B ${DATA}/PTEM/GNMD2OAT.BTF611BO
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611C ${DATA}/PTEM/GNMD2OAT.BTF611CO
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611D ${DATA}/PTEM/GNMD2OAT.BTF611DO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF611 
       JUMP_LABEL=GNMD2OAU
       ;;
(GNMD2OAU)
       m_CondExec 04,GE,GNMD2OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF616 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OAX
       ;;
(GNMD2OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF616 ${DATA}/PTEM/GNMD2OAX.BTF616AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF616 
       JUMP_LABEL=GNMD2OAY
       ;;
(GNMD2OAY)
       m_CondExec 04,GE,GNMD2OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF620 : EXTRACTION DE LA SOUS TABLE NMDAT                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBA
       ;;
(GNMD2OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIE                                                      
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF620 ${DATA}/PTEM/GNMD2OBA.BTF620AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF620 
       JUMP_LABEL=GNMD2OBB
       ;;
(GNMD2OBB)
       m_CondExec 04,GE,GNMD2OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF625 : CREATION DU FICHIER DE POUR LA TABLE RTGA30                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBD
       ;;
(GNMD2OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF625 ${DATA}/PTEM/GNMD2OBD.BTF625AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF625 
       JUMP_LABEL=GNMD2OBE
       ;;
(GNMD2OBE)
       m_CondExec 04,GE,GNMD2OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF631 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA14            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBG
       ;;
(GNMD2OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF631 ${DATA}/PTEM/GNMD2OBG.BTF631AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF631 
       JUMP_LABEL=GNMD2OBH
       ;;
(GNMD2OBH)
       m_CondExec 04,GE,GNMD2OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF636 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBJ
       ;;
(GNMD2OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA20   : NAME=RSGA20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF636 ${DATA}/PTEM/GNMD2OBJ.BTF636AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF636 
       JUMP_LABEL=GNMD2OBK
       ;;
(GNMD2OBK)
       m_CondExec 04,GE,GNMD2OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF641 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBM
       ;;
(GNMD2OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA21   : NAME=RSGA21O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF641 ${DATA}/PTEM/GNMD2OBM.BTF641AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF641 
       JUMP_LABEL=GNMD2OBN
       ;;
(GNMD2OBN)
       m_CondExec 04,GE,GNMD2OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF646 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA22            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBQ
       ;;
(GNMD2OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA22   : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF646 ${DATA}/PTEM/GNMD2OBQ.BTF646AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF646 
       JUMP_LABEL=GNMD2OBR
       ;;
(GNMD2OBR)
       m_CondExec 04,GE,GNMD2OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF651 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA23            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBT
       ;;
(GNMD2OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA23   : NAME=RSGA23O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA23 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF651 ${DATA}/PTEM/GNMD2OBT.BTF651AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF651 
       JUMP_LABEL=GNMD2OBU
       ;;
(GNMD2OBU)
       m_CondExec 04,GE,GNMD2OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF656 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA24            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OBX
       ;;
(GNMD2OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA24   : NAME=RSGA24O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA24 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF656 ${DATA}/PTEM/GNMD2OBX.BTF656AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF656 
       JUMP_LABEL=GNMD2OBY
       ;;
(GNMD2OBY)
       m_CondExec 04,GE,GNMD2OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF662 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCA
       ;;
(GNMD2OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF662 ${DATA}/PTEM/GNMD2OCA.BTF662AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF662 
       JUMP_LABEL=GNMD2OCB
       ;;
(GNMD2OCB)
       m_CondExec 04,GE,GNMD2OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF666 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCD
       ;;
(GNMD2OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA26   : NAME=RSGA26O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF666 ${DATA}/PTEM/GNMD2OCD.BTF666AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF666 
       JUMP_LABEL=GNMD2OCE
       ;;
(GNMD2OCE)
       m_CondExec 04,GE,GNMD2OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF671 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA27            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCG
       ;;
(GNMD2OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA27   : NAME=RSGA27O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA27 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF671 ${DATA}/PTEM/GNMD2OCG.BTF671AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF671 
       JUMP_LABEL=GNMD2OCH
       ;;
(GNMD2OCH)
       m_CondExec 04,GE,GNMD2OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF682 : EXTRACTION DE LA TABLE DES GARANTIES RTGA40                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCJ
       ;;
(GNMD2OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLE                                                        
#    RSGA00D  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******* TABLE GENERALISE                                                     
#    RSGA01D  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******* TABLE                                                                
#    RSGA14D  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14D /dev/null
# ******* TABLE                                                                
#    RSGA30D  : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30D /dev/null
# ******* TABLE CODICS LIE                                                     
#    RSGA58D  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58D /dev/null
# ******* TABLE                                                                
#    RSGA67D  : NAME=RSGA67O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67D /dev/null
# ******* TABLE MONTANTS DES GARANTIES COMPLEMENTAIRES                         
#    RSGA40D  : NAME=RSGA40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA40D /dev/null
# ******* TABLE ANO                                                            
#    RSAN00D  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF682 ${DATA}/PTEM/GNMD2OCJ.BTF682AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF682 
       JUMP_LABEL=GNMD2OCK
       ;;
(GNMD2OCK)
       m_CondExec 04,GE,GNMD2OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF686 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCM
       ;;
(GNMD2OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA41   : NAME=RSGA41O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF686 ${DATA}/PTEM/GNMD2OCM.BTF686AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF686 
       JUMP_LABEL=GNMD2OCN
       ;;
(GNMD2OCN)
       m_CondExec 04,GE,GNMD2OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF687 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA51            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCQ
       ;;
(GNMD2OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA51   : NAME=RSGA51O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF687 ${DATA}/PTEM/GNMD2OCQ.BTF687AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF687 
       JUMP_LABEL=GNMD2OCR
       ;;
(GNMD2OCR)
       m_CondExec 04,GE,GNMD2OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF692 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA52            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCT
       ;;
(GNMD2OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA52   : NAME=RSGA52O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF692 ${DATA}/PTEM/GNMD2OCT.BTF692AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF692 
       JUMP_LABEL=GNMD2OCU
       ;;
(GNMD2OCU)
       m_CondExec 04,GE,GNMD2OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF702 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA58            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OCX
       ;;
(GNMD2OCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES CODICS GROUPES                                             
#    RSGA58   : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF702 ${DATA}/PTEM/GNMD2OCX.BTF702AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF702 
       JUMP_LABEL=GNMD2OCY
       ;;
(GNMD2OCY)
       m_CondExec 04,GE,GNMD2OCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF707 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA64            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODA
       ;;
(GNMD2ODA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA64   : NAME=RSGA64O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF707 ${DATA}/PTEM/GNMD2ODA.BTF707AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF707 
       JUMP_LABEL=GNMD2ODB
       ;;
(GNMD2ODB)
       m_CondExec 04,GE,GNMD2ODA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF710 : EXTRACTION DE LA TABLE GQ12                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODD
       ;;
(GNMD2ODD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE MODE DE DELIVRANCE                                             
#    RSGQ12   : NAME=RSGQ12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF710 ${DATA}/PTEM/GNMD2ODD.BTF710AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF710 
       JUMP_LABEL=GNMD2ODE
       ;;
(GNMD2ODE)
       m_CondExec 04,GE,GNMD2ODD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF715 : EXTRACTION TABLE DES CODES VENDEURS RTGV31                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODG
       ;;
(GNMD2ODG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE CODES VENDEURS                                                 
#    RSGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF715 ${DATA}/PTEM/GNMD2ODG.BTF715AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF715 
       JUMP_LABEL=GNMD2ODH
       ;;
(GNMD2ODH)
       m_CondExec 04,GE,GNMD2ODG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF718 : EXTRACTION TABLE COMPTEUR SAV RTPA70                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODJ
       ;;
(GNMD2ODJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPA70   : NAME=RSPA70O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPA70 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF718 ${DATA}/PTEM/GNMD2ODJ.BTF718AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF718 
       JUMP_LABEL=GNMD2ODK
       ;;
(GNMD2ODK)
       m_CondExec 04,GE,GNMD2ODJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF720 : EXTRACTION TABLE MODE PMT / RBMT RTPM06                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODM
       ;;
(GNMD2ODM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPM06   : NAME=RSPM06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF720 ${DATA}/PTEM/GNMD2ODM.BTF720AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF720 
       JUMP_LABEL=GNMD2ODN
       ;;
(GNMD2ODN)
       m_CondExec 04,GE,GNMD2ODM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF725 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPM35            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODQ
       ;;
(GNMD2ODQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PARAMETRES MQSERIES                                            
#    RSPM35   : NAME=RSPM35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM35 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF725 ${DATA}/PTEM/GNMD2ODQ.BTF725AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF725 
       JUMP_LABEL=GNMD2ODR
       ;;
(GNMD2ODR)
       m_CondExec 04,GE,GNMD2ODQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF751 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODT
       ;;
(GNMD2ODT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PRESTATION                                                     
#    RSPR00D  : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00D /dev/null
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF751 ${DATA}/PTEM/GNMD2ODT.BTF751AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF751 
       JUMP_LABEL=GNMD2ODU
       ;;
(GNMD2ODU)
       m_CondExec 04,GE,GNMD2ODT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF755 : EXTRACTION TABLE LIEN FAMILLE/CODE PRESTATION RTPR01               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2ODX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2ODX
       ;;
(GNMD2ODX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN FAMILLE / CODE PRESTATION                                 
#    RSPR01   : NAME=RSPR01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF755 ${DATA}/PTEM/GNMD2ODX.BTF755AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF755 
       JUMP_LABEL=GNMD2ODY
       ;;
(GNMD2ODY)
       m_CondExec 04,GE,GNMD2ODX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF761 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEA
       ;;
(GNMD2OEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#    RSPR02D  : NAME=RSPR02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR02D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF761 ${DATA}/PTEM/GNMD2OEA.BTF761AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF761 
       JUMP_LABEL=GNMD2OEB
       ;;
(GNMD2OEB)
       m_CondExec 04,GE,GNMD2OEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF766 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR03  *         
#  REPRISE: OUI  *                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OED
       ;;
(GNMD2OED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#    RSPR03D  : NAME=RSPR03O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR03D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF766 ${DATA}/PTEM/GNMD2OED.BTF766AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF766 
       JUMP_LABEL=GNMD2OEE
       ;;
(GNMD2OEE)
       m_CondExec 04,GE,GNMD2OED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF770 : EXTRACTION TABLE LIEN PRESTATIONS RTPR04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEG
       ;;
(GNMD2OEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN PRESTATION                                                
#    RSPR04   : NAME=RSPR04O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR04 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF770 ${DATA}/PTEM/GNMD2OEG.BTF770AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF770 
       JUMP_LABEL=GNMD2OEH
       ;;
(GNMD2OEH)
       m_CondExec 04,GE,GNMD2OEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF777 : EXTRACT TABLE PRIX PRESTATION LIE A LA ZONE DE PRIX RTPR10         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEJ
       ;;
(GNMD2OEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06D  : NAME=RSPR06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06D /dev/null
# ******* TABLE PRIX PRESTATION LIE A ZONE DE PRIX                             
#    RSPR10D  : NAME=RSPR10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR10D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF777 ${DATA}/PTEM/GNMD2OEJ.BTF777AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF777 
       JUMP_LABEL=GNMD2OEK
       ;;
(GNMD2OEK)
       m_CondExec 04,GE,GNMD2OEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF782 : EXTRACTION TABLE PRIX PRESTATION LIE AU CODIC RTPR14               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEM
       ;;
(GNMD2OEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06D  : NAME=RSPR06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06D /dev/null
# ******* TABLE PRIX PRESTATION LIE A ARTICLE                                  
#    RSPR14D  : NAME=RSPR14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR14D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF782 ${DATA}/PTEM/GNMD2OEM.BTF782AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF782 
       JUMP_LABEL=GNMD2OEN
       ;;
(GNMD2OEN)
       m_CondExec 04,GE,GNMD2OEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF783 : EXTRACTION DE LA TABLE RTPR53                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEQ
       ;;
(GNMD2OEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR53 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF783 ${DATA}/PTEM/GNMD2OEQ.BTF783AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF783 
       JUMP_LABEL=GNMD2OER
       ;;
(GNMD2OER)
       m_CondExec 04,GE,GNMD2OEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF784 : EXTRACTION DE LA TABLE RTGA31                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OET
       ;;
(GNMD2OET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA31   : NAME=RSGA31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF784 ${DATA}/PTEM/GNMD2OET.BTF784AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF784 
       JUMP_LABEL=GNMD2OEU
       ;;
(GNMD2OEU)
       m_CondExec 04,GE,GNMD2OET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFT785 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OEX
       ;;
(GNMD2OEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF785 ${DATA}/PTEM/GNMD2OEX.BTF785AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF785 
       JUMP_LABEL=GNMD2OEY
       ;;
(GNMD2OEY)
       m_CondExec 04,GE,GNMD2OEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF786 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFA
       ;;
(GNMD2OFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF786 ${DATA}/PTEM/GNMD2OFA.BTF786AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF786 
       JUMP_LABEL=GNMD2OFB
       ;;
(GNMD2OFB)
       m_CondExec 04,GE,GNMD2OFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF787 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFD
       ;;
(GNMD2OFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF787 ${DATA}/PTEM/GNMD2OFD.BTF787AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF787 
       JUMP_LABEL=GNMD2OFE
       ;;
(GNMD2OFE)
       m_CondExec 04,GE,GNMD2OFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF788 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFG PGM=IKJEFT01   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFG
       ;;
(GNMD2OFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF788 ${DATA}/PTEM/GNMD2OFG.BTF788AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF788 
       JUMP_LABEL=GNMD2OFH
       ;;
(GNMD2OFH)
       m_CondExec 04,GE,GNMD2OFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF794 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFJ
       ;;
(GNMD2OFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF794 ${DATA}/PTEM/GNMD2OFJ.BTF794AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF794 
       JUMP_LABEL=GNMD2OFK
       ;;
(GNMD2OFK)
       m_CondExec 04,GE,GNMD2OFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF795 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFM
       ;;
(GNMD2OFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF795 ${DATA}/PTEM/GNMD2OFM.BTF795AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF795 
       JUMP_LABEL=GNMD2OFN
       ;;
(GNMD2OFN)
       m_CondExec 04,GE,GNMD2OFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF796 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFQ
       ;;
(GNMD2OFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF796 ${DATA}/PTEM/GNMD2OFQ.BTF796AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF796 
       JUMP_LABEL=GNMD2OFR
       ;;
(GNMD2OFR)
       m_CondExec 04,GE,GNMD2OFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF797 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFT
       ;;
(GNMD2OFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF797 ${DATA}/PTEM/GNMD2OFT.BTF797AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF797 
       JUMP_LABEL=GNMD2OFU
       ;;
(GNMD2OFU)
       m_CondExec 04,GE,GNMD2OFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF798 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OFX
       ;;
(GNMD2OFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS25   : NAME=RSBS25P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS25 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF798 ${DATA}/PTEM/GNMD2OFX.BTF798AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF798 
       JUMP_LABEL=GNMD2OFY
       ;;
(GNMD2OFY)
       m_CondExec 04,GE,GNMD2OFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF799 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGA PGM=IKJEFT01   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGA
       ;;
(GNMD2OGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS26   : NAME=RSBS26P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS26 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF799 ${DATA}/PTEM/GNMD2OGA.BTF799AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF799 
       JUMP_LABEL=GNMD2OGB
       ;;
(GNMD2OGB)
       m_CondExec 04,GE,GNMD2OGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF802 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTIP02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGD
       ;;
(GNMD2OGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FPV212 ${DATA}/PXX0/F16.BPV212AO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF802 ${DATA}/PXX0/F16.BTF802AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF802 
       JUMP_LABEL=GNMD2OGE
       ;;
(GNMD2OGE)
       m_CondExec 04,GE,GNMD2OGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF805 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA91            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGG
       ;;
(GNMD2OGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES                                                            
#    RSGA91   : NAME=RSGA91O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA91 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF805 ${DATA}/PTEM/GNMD2OGG.BTF805AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF805 
       JUMP_LABEL=GNMD2OGH
       ;;
(GNMD2OGH)
       m_CondExec 04,GE,GNMD2OGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF810 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA92            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGJ PGM=IKJEFT01   ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGJ
       ;;
(GNMD2OGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA92   : NAME=RSGA92O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA92 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF810 ${DATA}/PTEM/GNMD2OGJ.BTF810AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF810 
       JUMP_LABEL=GNMD2OGK
       ;;
(GNMD2OGK)
       m_CondExec 04,GE,GNMD2OGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF815 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA93            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGM PGM=IKJEFT01   ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGM
       ;;
(GNMD2OGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA93   : NAME=RSGA93O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA93 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF815 ${DATA}/PTEM/GNMD2OGM.BTF815AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF815 
       JUMP_LABEL=GNMD2OGN
       ;;
(GNMD2OGN)
       m_CondExec 04,GE,GNMD2OGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF820 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA12            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGQ
       ;;
(GNMD2OGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR12   : NAME=RSPR12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF820 ${DATA}/PTEM/GNMD2OGQ.BTF820AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF820 
       JUMP_LABEL=GNMD2OGR
       ;;
(GNMD2OGR)
       m_CondExec 04,GE,GNMD2OGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF825 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGT PGM=IKJEFT01   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGT
       ;;
(GNMD2OGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR16   : NAME=RSPR16O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF825 ${DATA}/PTEM/GNMD2OGT.BTF825AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF825 
       JUMP_LABEL=GNMD2OGU
       ;;
(GNMD2OGU)
       m_CondExec 04,GE,GNMD2OGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF835 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA56            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OGX
       ;;
(GNMD2OGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA56   : NAME=RSGA56O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA56 /dev/null
# ******* TABLE                                                                
#    RSGA59   : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* TABLE                                                                
#    RSGA66   : NAME=RSGA66O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
# ******* TABLE                                                                
#    RSGA68   : NAME=RSGA68O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
# ******* TABLE                                                                
#    RSGA75   : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* TABLE                                                                
#    RSGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF835 ${DATA}/PTEM/GNMD2OGX.BTF835AO
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2OGX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF835 
       JUMP_LABEL=GNMD2OGY
       ;;
(GNMD2OGY)
       m_CondExec 04,GE,GNMD2OGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF836 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHA PGM=IKJEFT01   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHA
       ;;
(GNMD2OHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE                                                                
#    RSGQ01   : NAME=RSGQ01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF836 ${DATA}/PTEM/GNMD2OHA.BTF836AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF836 
       JUMP_LABEL=GNMD2OHB
       ;;
(GNMD2OHB)
       m_CondExec 04,GE,GNMD2OHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF837 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHD PGM=IKJEFT01   ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHD
       ;;
(GNMD2OHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGQ06   : NAME=RSGQ06O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF837 ${DATA}/PTEM/GNMD2OHD.BTF837AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF837 
       JUMP_LABEL=GNMD2OHE
       ;;
(GNMD2OHE)
       m_CondExec 04,GE,GNMD2OHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF838 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHG PGM=IKJEFT01   ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHG
       ;;
(GNMD2OHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE STOCK LOCAUX                                                   
#    RSSL01   : NAME=RSSL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF838 ${DATA}/PTEM/GNMD2OHG.BTF838AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF838 
       JUMP_LABEL=GNMD2OHH
       ;;
(GNMD2OHH)
       m_CondExec 04,GE,GNMD2OHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF839 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHJ PGM=IKJEFT01   ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHJ
       ;;
(GNMD2OHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSSL16   : NAME=RSSL16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF839 ${DATA}/PTEM/GNMD2OHJ.BTF839AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF839 
       JUMP_LABEL=GNMD2OHK
       ;;
(GNMD2OHK)
       m_CondExec 04,GE,GNMD2OHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF840 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHM PGM=IKJEFT01   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHM
       ;;
(GNMD2OHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGG40   : NAME=RSGG40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF840 ${DATA}/PTEM/GNMD2OHM.BTF840AO
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 HAD840 ${DATA}/PXX0/F16.BTF840BO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF840 
       JUMP_LABEL=GNMD2OHN
       ;;
(GNMD2OHN)
       m_CondExec 04,GE,GNMD2OHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BTF841 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTEE03           
# * REPRISE: OUI                                                               
# ********************************************************************         
# ALT      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** TABLE CODES POSTAUX                                                 
# RSEE03   FILE  DYNAM=YES,NAME=RSEE03,MODE=I                                  
# ******** PARAMETRE SOCIETE                                                   
# FNSOC    DATA  CLASS=FIX1,MBR=SOCMGIO                                        
# ******** PARAMETRE FDATE                                                     
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** FICHIER D'EXTRACTION                                                
# FTF841   FILE  NAME=BTF841AO,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BTF841) PLAN(BTDARO)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BTF842 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGB05            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHQ PGM=IKJEFT01   ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHQ
       ;;
(GNMD2OHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF842 ${DATA}/PTEM/GNMD2OHQ.BTF842AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF842 
       JUMP_LABEL=GNMD2OHR
       ;;
(GNMD2OHR)
       m_CondExec 04,GE,GNMD2OHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF843 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA54            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHT PGM=IKJEFT01   ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHT
       ;;
(GNMD2OHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF843 ${DATA}/PTEM/GNMD2OHT.BTF843AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF843 
       JUMP_LABEL=GNMD2OHU
       ;;
(GNMD2OHU)
       m_CondExec 04,GE,GNMD2OHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF844 : CREATION DU FICHIER DE COMPARAISON POUR GV21/22/35/11 TL02         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OHX PGM=IKJEFT01   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OHX
       ;;
(GNMD2OHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#    RSGV35   : NAME=RSGV35O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF844 ${DATA}/PTEM/GNMD2OHX.BTF844AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF844 
       JUMP_LABEL=GNMD2OHY
       ;;
(GNMD2OHY)
       m_CondExec 04,GE,GNMD2OHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF845 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA53            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIA PGM=IKJEFT01   ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIA
       ;;
(GNMD2OIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA53   : NAME=RSGA53O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF845 ${DATA}/PTEM/GNMD2OIA.BTF845AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF845 
       JUMP_LABEL=GNMD2OIB
       ;;
(GNMD2OIB)
       m_CondExec 04,GE,GNMD2OIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF846 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OID PGM=IKJEFT01   ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OID
       ;;
(GNMD2OID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL40   : NAME=RSSL40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF846 ${DATA}/PTEM/GNMD2OID.BTF846AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF846 
       JUMP_LABEL=GNMD2OIE
       ;;
(GNMD2OIE)
       m_CondExec 04,GE,GNMD2OID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF847 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIG PGM=IKJEFT01   ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIG
       ;;
(GNMD2OIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL41   : NAME=RSSL41O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL41 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF847 ${DATA}/PTEM/GNMD2OIG.BTF847AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF847 
       JUMP_LABEL=GNMD2OIH
       ;;
(GNMD2OIH)
       m_CondExec 04,GE,GNMD2OIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF848 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIJ PGM=IKJEFT01   ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIJ
       ;;
(GNMD2OIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE STOCK LOCAL - MODE I (POUR EVITER SERIALIS.DES FILIALE         
#    RSSL10   : NAME=RSSL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF848 ${DATA}/PTEM/GNMD2OIJ.BTF848AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF848 
       JUMP_LABEL=GNMD2OIK
       ;;
(GNMD2OIK)
       m_CondExec 04,GE,GNMD2OIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF849 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTYF00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIM PGM=IKJEFT01   ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIM
       ;;
(GNMD2OIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE PARIS                                                          
#    RSYF00   : NAME=RSYF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSYF00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES CODES VENDEURS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF849 ${DATA}/PTEM/GNMD2OIM.BTF849AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF849 
       JUMP_LABEL=GNMD2OIN
       ;;
(GNMD2OIN)
       m_CondExec 04,GE,GNMD2OIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF860 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLI00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIQ PGM=IKJEFT01   ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIQ
       ;;
(GNMD2OIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF860 ${DATA}/PTEM/GNMD2OIQ.BTF860AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF860 
       JUMP_LABEL=GNMD2OIR
       ;;
(GNMD2OIR)
       m_CondExec 04,GE,GNMD2OIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF862 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLG09            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIT PGM=IKJEFT01   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIT
       ;;
(GNMD2OIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLL09   : NAME=RSLG09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLL09 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF862 ${DATA}/PTEM/GNMD2OIT.BTF862AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF862 
       JUMP_LABEL=GNMD2OIU
       ;;
(GNMD2OIU)
       m_CondExec 04,GE,GNMD2OIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF863 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA59            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OIX PGM=IKJEFT01   ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OIX
       ;;
(GNMD2OIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF863 ${DATA}/PTEM/GNMD2OIX.BTF863AO
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2OIX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF863 
       JUMP_LABEL=GNMD2OIY
       ;;
(GNMD2OIY)
       m_CondExec 04,GE,GNMD2OIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF864 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJA PGM=IKJEFT01   ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJA
       ;;
(GNMD2OJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGG20   : NAME=RSGG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF864 ${DATA}/PTEM/GNMD2OJA.BTF864AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF864 
       JUMP_LABEL=GNMD2OJB
       ;;
(GNMD2OJB)
       m_CondExec 04,GE,GNMD2OJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF865 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJD PGM=IKJEFT01   ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJD
       ;;
(GNMD2OJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE POUR LA CONTRIBUTION RECYCLAGE D3E                             
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF865 ${DATA}/PTEM/GNMD2OJD.BTF865AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF865 
       JUMP_LABEL=GNMD2OJE
       ;;
(GNMD2OJE)
       m_CondExec 04,GE,GNMD2OJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF875 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA82            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJG PGM=IKJEFT01   ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJG
       ;;
(GNMD2OJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA82   : NAME=RSGA82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA82 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF875 ${DATA}/PTEM/GNMD2OJG.BTF875AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF875 
       JUMP_LABEL=GNMD2OJH
       ;;
(GNMD2OJH)
       m_CondExec 04,GE,GNMD2OJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF877 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJJ PGM=IKJEFT01   ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJJ
       ;;
(GNMD2OJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877A ${DATA}/PTEM/GNMD2OJJ.BTF877AO
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877B ${DATA}/PTEM/GNMD2OJJ.BTF877BO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF877 
       JUMP_LABEL=GNMD2OJK
       ;;
(GNMD2OJK)
       m_CondExec 04,GE,GNMD2OJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF878 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL17            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJM PGM=IKJEFT01   ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJM
       ;;
(GNMD2OJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSSL17   : NAME=RSSL17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL17 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF878 ${DATA}/PTEM/GNMD2OJM.BTF878AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF878 
       JUMP_LABEL=GNMD2OJN
       ;;
(GNMD2OJN)
       m_CondExec 04,GE,GNMD2OJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJQ
       ;;
(GNMD2OJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GNMD2OAG.BTF500AO
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GNMD2OAJ.BTF505AO
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GNMD2OAM.BTF598AO
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GNMD2OAQ.BTF606AO
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GNMD2OAX.BTF616AO
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GNMD2OAT.BTF611AO
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GNMD2OAT.BTF611BO
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GNMD2OAT.BTF611CO
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GNMD2OAT.BTF611DO
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GNMD2OBA.BTF620AO
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GNMD2OBD.BTF625AO
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GNMD2OBG.BTF631AO
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GNMD2OBJ.BTF636AO
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GNMD2OBM.BTF641AO
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GNMD2OBQ.BTF646AO
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/GNMD2OBT.BTF651AO
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GNMD2OBX.BTF656AO
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GNMD2OCA.BTF662AO
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GNMD2OCD.BTF666AO
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GNMD2OCG.BTF671AO
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GNMD2OCJ.BTF682AO
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GNMD2OCM.BTF686AO
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GNMD2OCQ.BTF687AO
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PTEM/GNMD2OCT.BTF692AO
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GNMD2OCX.BTF702AO
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GNMD2ODA.BTF707AO
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GNMD2ODD.BTF710AO
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GNMD2ODG.BTF715AO
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GNMD2ODJ.BTF718AO
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GNMD2ODM.BTF720AO
       m_FileAssign -d SHR -g ${G_A31} -C ${DATA}/PTEM/GNMD2ODQ.BTF725AO
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GNMD2ODT.BTF751AO
       m_FileAssign -d SHR -g ${G_A33} -C ${DATA}/PTEM/GNMD2ODX.BTF755AO
       m_FileAssign -d SHR -g ${G_A34} -C ${DATA}/PTEM/GNMD2OEA.BTF761AO
       m_FileAssign -d SHR -g ${G_A35} -C ${DATA}/PTEM/GNMD2OED.BTF766AO
       m_FileAssign -d SHR -g ${G_A36} -C ${DATA}/PTEM/GNMD2OEG.BTF770AO
       m_FileAssign -d SHR -g ${G_A37} -C ${DATA}/PTEM/GNMD2OEJ.BTF777AO
       m_FileAssign -d SHR -g ${G_A38} -C ${DATA}/PTEM/GNMD2OEM.BTF782AO
       m_FileAssign -d SHR -g ${G_A39} -C ${DATA}/PTEM/GNMD2OEQ.BTF783AO
       m_FileAssign -d SHR -g ${G_A40} -C ${DATA}/PTEM/GNMD2OET.BTF784AO
       m_FileAssign -d SHR -g ${G_A41} -C ${DATA}/PTEM/GNMD2OEX.BTF785AO
       m_FileAssign -d SHR -g ${G_A42} -C ${DATA}/PTEM/GNMD2OFA.BTF786AO
       m_FileAssign -d SHR -g ${G_A43} -C ${DATA}/PTEM/GNMD2OFD.BTF787AO
       m_FileAssign -d SHR -g ${G_A44} -C ${DATA}/PTEM/GNMD2OFG.BTF788AO
       m_FileAssign -d SHR -g ${G_A45} -C ${DATA}/PTEM/GNMD2OFJ.BTF794AO
       m_FileAssign -d SHR -g ${G_A46} -C ${DATA}/PTEM/GNMD2OFM.BTF795AO
       m_FileAssign -d SHR -g ${G_A47} -C ${DATA}/PTEM/GNMD2OFQ.BTF796AO
       m_FileAssign -d SHR -g ${G_A48} -C ${DATA}/PTEM/GNMD2OFT.BTF797AO
       m_FileAssign -d SHR -g ${G_A49} -C ${DATA}/PTEM/GNMD2OFX.BTF798AO
       m_FileAssign -d SHR -g ${G_A50} -C ${DATA}/PTEM/GNMD2OGA.BTF799AO
       m_FileAssign -d SHR -g ${G_A51} -C ${DATA}/PTEM/GNMD2OGG.BTF805AO
       m_FileAssign -d SHR -g ${G_A52} -C ${DATA}/PTEM/GNMD2OGJ.BTF810AO
       m_FileAssign -d SHR -g ${G_A53} -C ${DATA}/PTEM/GNMD2OGM.BTF815AO
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/GNMD2OGQ.BTF820AO
       m_FileAssign -d SHR -g ${G_A55} -C ${DATA}/PTEM/GNMD2OGT.BTF825AO
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/GNMD2OGX.BTF835AO
       m_FileAssign -d SHR -g ${G_A57} -C ${DATA}/PTEM/GNMD2OHA.BTF836AO
       m_FileAssign -d SHR -g ${G_A58} -C ${DATA}/PTEM/GNMD2OHD.BTF837AO
       m_FileAssign -d SHR -g ${G_A59} -C ${DATA}/PTEM/GNMD2OHG.BTF838AO
       m_FileAssign -d SHR -g ${G_A60} -C ${DATA}/PTEM/GNMD2OHJ.BTF839AO
       m_FileAssign -d SHR -g ${G_A61} -C ${DATA}/PTEM/GNMD2OHM.BTF840AO
#         FILE  NAME=BTF841AO,MODE=I                                           
       m_FileAssign -d SHR -g ${G_A62} -C ${DATA}/PTEM/GNMD2OHQ.BTF842AO
       m_FileAssign -d SHR -g ${G_A63} -C ${DATA}/PTEM/GNMD2OHT.BTF843AO
       m_FileAssign -d SHR -g ${G_A64} -C ${DATA}/PTEM/GNMD2OHX.BTF844AO
       m_FileAssign -d SHR -g ${G_A65} -C ${DATA}/PTEM/GNMD2OIA.BTF845AO
       m_FileAssign -d SHR -g ${G_A66} -C ${DATA}/PTEM/GNMD2OID.BTF846AO
       m_FileAssign -d SHR -g ${G_A67} -C ${DATA}/PTEM/GNMD2OIG.BTF847AO
       m_FileAssign -d SHR -g ${G_A68} -C ${DATA}/PTEM/GNMD2OIJ.BTF848AO
       m_FileAssign -d SHR -g ${G_A69} -C ${DATA}/PTEM/GNMD2OIM.BTF849AO
       m_FileAssign -d SHR -g ${G_A70} -C ${DATA}/PTEM/GNMD2OIQ.BTF860AO
       m_FileAssign -d SHR -g ${G_A71} -C ${DATA}/PTEM/GNMD2OIT.BTF862AO
       m_FileAssign -d SHR -g ${G_A72} -C ${DATA}/PTEM/GNMD2OIX.BTF863AO
       m_FileAssign -d SHR -g ${G_A73} -C ${DATA}/PTEM/GNMD2OJA.BTF864AO
       m_FileAssign -d SHR -g ${G_A74} -C ${DATA}/PTEM/GNMD2OJD.BTF865AO
       m_FileAssign -d SHR -g ${G_A75} -C ${DATA}/PTEM/GNMD2OJG.BTF875AO
       m_FileAssign -d SHR -g ${G_A76} -C ${DATA}/PTEM/GNMD2OJJ.BTF877AO
       m_FileAssign -d SHR -g ${G_A77} -C ${DATA}/PTEM/GNMD2OJJ.BTF877BO
       m_FileAssign -d SHR -g ${G_A78} -C ${DATA}/PTEM/GNMD2OJM.BTF878AO
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2OJQ.BTF001XO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2OJR
       ;;
(GNMD2OJR)
       m_CondExec 00,EQ,GNMD2OJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF001AO     NOUVEAU POUR EXCLURE LES ENRS RTXM0            
#   SUR LUI-MEME                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJT
       ;;
(GNMD2OJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P916/SEM.BTF001AO
# ****    MODIF                                                                
       m_FileAssign -d SHR -g +0 SORTOUT ${DATA}/P916/SEM.BTF001AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "X-RTGA59"
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /FIELDS FLD_CH_1_385 1 CH 385
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_8 EQ CST_3_8 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2OJU
       ;;
(GNMD2OJU)
       m_CondExec 00,EQ,GNMD2OJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OJX PGM=BTF900     ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OJX
       ;;
(GNMD2OJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A79} FTF600 ${DATA}/PTEM/GNMD2OJQ.BTF001XO
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/P916/SEM.BTF001AO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PXX0/F16.BTF002AO
       m_ProgramExec BTF900 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER BTF001X      NOUVEAUTE                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKA
       ;;
(GNMD2OKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A80} SORTIN ${DATA}/PTEM/GNMD2OJQ.BTF001XO
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2OKA.BTF901AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_13 "RTPR14"
 /DERIVEDFIELD CST_3_9 "RTPR12"
 /DERIVEDFIELD CST_1_5 "RTPR10"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_25_8 25 CH 8
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_5 OR FLD_CH_1_6 EQ CST_3_9 OR FLD_CH_1_6 EQ CST_5_13 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_25_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2OKB
       ;;
(GNMD2OKB)
       m_CondExec 00,EQ,GNMD2OKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF901 :                                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKD PGM=IKJEFT01   ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKD
       ;;
(GNMD2OKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A81} FTF600 ${DATA}/PTEM/GNMD2OKA.BTF901AO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/GNMD2OKD.BTF901BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF901 
       JUMP_LABEL=GNMD2OKE
       ;;
(GNMD2OKE)
       m_CondExec 04,GE,GNMD2OKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF901                                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKG
       ;;
(GNMD2OKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A82} SORTIN ${DATA}/PTEM/GNMD2OKD.BTF901BO
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.BTF901CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2OKH
       ;;
(GNMD2OKH)
       m_CondExec 00,EQ,GNMD2OKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ910 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKJ PGM=IKJEFT01   ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKJ
       ;;
(GNMD2OKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A83} FMQ910 ${DATA}/PXX0/F16.BTF802AO
       m_FileAssign -d SHR -g ${G_A84} -C ${DATA}/PXX0/F16.BTF002AO
       m_FileAssign -d SHR -g ${G_A85} -C ${DATA}/PXX0/F16.BTF901CO
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/GNMD2O1N
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2OKJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ910 
       JUMP_LABEL=GNMD2OKK
       ;;
(GNMD2OKK)
       m_CondExec 04,GE,GNMD2OKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DU FICHIER SEQUENTIEL POUR LIBERER DE LA PLACE                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKM PGM=IDCAMS     ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKM
       ;;
(GNMD2OKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2OKM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2OKN
       ;;
(GNMD2OKN)
       m_CondExec 16,NE,GNMD2OKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER BTF001XD POUR COMPARAISON DU LENDEMAIN                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2OKQ PGM=IEBGENER   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OKQ
       ;;
(GNMD2OKQ)
       m_CondExec ${EXAQJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A86} SYSUT1 ${DATA}/PTEM/GNMD2OJQ.BTF001XO
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SYSUT2 ${DATA}/P916/SEM.BTF001AO
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2OKR
       ;;
(GNMD2OKR)
       m_CondExec 00,EQ,GNMD2OKQ ${EXAQJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2OZA
       ;;
(GNMD2OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
