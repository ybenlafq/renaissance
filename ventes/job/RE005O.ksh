#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE005O.ksh                       --- VERSION DU 20/10/2016 10:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PORE005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/04/01 AT 10.33.37 BY BURTECA                      
#    STANDARDS: P  JOBSET: RE005O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE005OA
       ;;
(RE005OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE005OAA
       ;;
(RE005OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# ********************************* FICHIER FRE002 MENSUEL                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BRE002MO.MENSUEL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE005OAA.BRE002AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_PD_12_3 12 PD 3
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE005OAB
       ;;
(RE005OAB)
       m_CondExec 00,EQ,RE005OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BRE002 (SUR FICHIER ISSU CHAINE JOURNALIERE RE000P:PGM BRE001)          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAD PGM=BRE002     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAD
       ;;
(RE005OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  FMOIS                                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
#  FRE002  : FICHIER TRIE                                                      
       m_FileAssign -d SHR -g ${G_A1} FRE002 ${DATA}/PTEM/RE005OAA.BRE002AO
#  SORTIE :512 DE LONG                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE002S ${DATA}/PXX0/F16.BRE002MO.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/RE005OAD.BRE002BO
       m_ProgramExec BRE002 
# ********************************************************************         
#   PROG    BRE035  : REMISE A NIVEAU DES CODES GROUPES DE MAG                 
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAG
       ;;
(RE005OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTGA10   : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  ENTREE :512 DE LONG  .FEXTRAC : PRIS EN I/O                                 
# -M-BRE002BO - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g ${G_A2} FEXTRAC ${DATA}/PTEM/RE005OAD.BRE002BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE035 
       JUMP_LABEL=RE005OAH
       ;;
(RE005OAH)
       m_CondExec 04,GE,RE005OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RE005OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAJ
       ;;
(RE005OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE005OAD.BRE002BO
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE005OAJ.BRE002CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_7_2 7 CH 2
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE005OAK
       ;;
(RE005OAK)
       m_CondExec 00,EQ,RE005OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRE002CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAM
       ;;
(RE005OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/RE005OAJ.BRE002CO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE005OAM.BRE002DO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE005OAN
       ;;
(RE005OAN)
       m_CondExec 04,GE,RE005OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAQ
       ;;
(RE005OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/RE005OAM.BRE002DO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE005OAQ.FRE002AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE005OAR
       ;;
(RE005OAR)
       m_CondExec 00,EQ,RE005OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT MENSUEL : IRE002                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAT
       ;;
(RE005OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/RE005OAJ.BRE002CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FCUMULS ${DATA}/PTEM/RE005OAQ.FRE002AO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG132 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE002 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE005OAU
       ;;
(RE005OAU)
       m_CondExec 04,GE,RE005OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE: OUI : SORT DU FICHIER DU MOIS PRECEDENT AVEC MOIS ACTUEL           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE005OAX
       ;;
(RE005OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BRE010MO.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 SORTOUT ${DATA}/PTEM/RE005OAX.BRE010AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_PD_45_2 45 PD 2
 /FIELDS FLD_PD_28_4 28 PD 4
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_36_5 36 PD 5
 /FIELDS FLD_PD_47_2 47 PD 2
 /FIELDS FLD_PD_41_2 41 PD 2
 /FIELDS FLD_PD_24_4 24 PD 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_PD_32_4 32 PD 4
 /FIELDS FLD_PD_43_2 43 PD 2
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_24_4,
    TOTAL FLD_PD_28_4,
    TOTAL FLD_PD_32_4,
    TOTAL FLD_PD_36_5,
    TOTAL FLD_PD_41_2,
    TOTAL FLD_PD_43_2,
    TOTAL FLD_PD_45_2,
    TOTAL FLD_PD_47_2
 /* Record Type = F  Record Length = 048 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE005OAY
       ;;
(RE005OAY)
       m_CondExec 00,EQ,RE005OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE030  PREPARATION DU FICHIER POUR LOAD DE TABLE                  
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OBA PGM=BRE030     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE005OBA
       ;;
(RE005OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  PARAMETRE  MOIS                                                             
       m_FileAssign -i FMOIS
$FMOIS
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  RTRE00  : FICHIER                                                           
       m_FileAssign -d SHR -g ${G_A8} RTRE00 ${DATA}/PTEM/RE005OAX.BRE010AO
#  SORTIE :048 DE LONG                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 FRE030 ${DATA}/PTEM/RE005OBA.BRE010CO
#  SORTIE :048 DE LONG: MOIS PRECEDENT                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 FRE031 ${DATA}/PXX0/F16.BRE10AMO.MOISPRED
       m_ProgramExec BRE030 
# ********************************************************************         
#  LOAD DE LA TABLE RTRE00                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OBD PGM=DSNUTILB   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE005OBD
       ;;
(RE005OBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
# ******  FIC DE LOAD DE LA RTRE00                                             
       m_FileAssign -d SHR -g ${G_A9} SYSREC ${DATA}/PTEM/RE005OBA.BRE010CO
# ******  HISTO VENDEURS                                                       
#    RSRE00O  : NAME=RSRE00O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSRE00O /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_ProgramExec -b LRTRE0001 RESUME YES
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE005OBE
       ;;
(RE005OBE)
       m_CondExec 04,GE,RE005OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   IMAGECOPY DU TABLESPACE  RSRE00O DE LA D BASE PODRE00                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE005OBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE005OBG
       ;;
(RE005OBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 4096 -g +1 SYSCOPY ${DATA}/PFIC/F16.RE00IO
       m_OutputAssign -c "*" SYSPRINT
#    TABLE    : NAME=RSRE00O,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR TABLE /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE005OBG.sysin
       m_UtilityExec
# **                                                                           
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE005OBH
       ;;
(RE005OBH)
       m_CondExec 04,GE,RE005OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE005OZA
       ;;
(RE005OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE005OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
