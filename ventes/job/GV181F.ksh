#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV181F.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGV181 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/10/27 AT 16.25.00 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV181F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ****************************************************************             
#                                                                              
#  PASSAGE DU BGV180 POUR PARIS                                                
#                                                                              
# ****************************************************************             
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV181FA
       ;;
(GV181FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV181FAA
       ;;
(GV181FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAA.BGV180BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAA.BDA189BP
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MGI OUEST                                            
#                                                                              
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV181FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAD
       ;;
(GV181FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAD.BGV180BO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAD.BDA189BO
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR LUXEMBOURG                                           
#                                                                              
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV181FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAG
       ;;
(GV181FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAG.BGV180BX
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAG.BDA189BX
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR LYON                                                 
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV181FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAJ
       ;;
(GV181FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAJ.BGV180BY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAJ.BDA189BY
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR LILLE                                                
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV181FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAM
       ;;
(GV181FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAM.BGV180BL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAM.BDA189BL
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR METZ                                                 
#                                                                              
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP GV181FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAQ
       ;;
(GV181FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAQ.BGV180BM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAQ.BDA189BM
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MARSEILLE                                            
#                                                                              
# ************************************************************                 
#                                                                              
# ***********************************                                          
# *   STEP GV181FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAT
       ;;
(GV181FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV181FAT.BGV180BD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV181FAT.BDA189BD
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180H
# ******                                                                       
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV180                                                    
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV181FAX
       ;;
(GV181FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV181FAT.BGV180BD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GV181FAM.BGV180BL
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GV181FAQ.BGV180BM
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GV181FAD.BGV180BO
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GV181FAA.BGV180BP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GV181FAJ.BGV180BY
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GV181FAG.BGV180BX
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FAX.BGV183CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FAY
       ;;
(GV181FAY)
       m_CondExec 00,EQ,GV181FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#  PASSAGE DU BGV183 POUR LE CUMUL                                             
#                                                                              
# ********************************************************************         
# BJ      STEP  PGM=IKJEFT01                                                   
# YSABOUT REPORT SYSOUT=*                                                      
# YSPRINT REPORT SYSOUT=*                                                      
# YSOUT   REPORT SYSOUT=*                                                      
# YSTSPRT REPORT SYSOUT=*                                                      
# ****** FICHIER EN ENTREE                                                     
# GV180   FILE  NAME=BGV183CP,MODE=I                                           
# ****** FICHIER EN SORTIE                                                     
# GV183   FILE  NAME=BGV183DP,MODE=O                                           
# YSTSIN  DATA  *,CLASS=FIX2                                                   
# DSN SYSTEM(RDAR)                                                             
# RUN PROGRAM(BGV183) PLAN(BGV183)                                             
# END                                                                          
#         DATAEND                                                              
# *********************************************************                    
# *********************************************************                    
# ****                                              *******                    
# ****                                              *******                    
# *********************************************************                    
# *********************************************************                    
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER CREDITF                                                   
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV181FBA
       ;;
(GV181FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.CREDITF
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FBA.FGV184AP
# *********************************************************                    
# *                                                                            
# *     EXTRACT DU CA - PGM = BGV184                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV181FBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "CA "
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_6 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FBD
       ;;
(GV181FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** TABLE ARTICLE EN ENTREE                                               
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A8} FDA189 ${DATA}/PTEM/GV181FAA.BDA189BP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GV181FAD.BDA189BO
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GV181FAJ.BDA189BY
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GV181FAM.BDA189BL
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GV181FAQ.BDA189BM
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GV181FAT.BDA189BD
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GV181FAG.BDA189BX
       m_FileAssign -d SHR -g ${G_A15} FGV184 ${DATA}/PTEM/GV181FBA.FGV184AP
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FCR003 ${DATA}/PTEM/GV181FBD.FGV184BP
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER CREDITF                                                   
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          

       m_ProgramExec -b BGV184 
       JUMP_LABEL=GV181FBG
       ;;
(GV181FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.CREDITF
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FBG.FGV182AP
# *********************************************************                    
# *                                                                            
# *     EXTRACT DU CA - PGM = BGV182                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV181FBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "CRE"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_6 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FBJ
       ;;
(GV181FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A16} FDA189 ${DATA}/PTEM/GV181FAA.BDA189BP
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GV181FAJ.BDA189BY
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GV181FAM.BDA189BL
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GV181FAQ.BDA189BM
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GV181FAD.BDA189BO
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GV181FAT.BDA189BD
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GV181FAG.BDA189BX
       m_FileAssign -d SHR -g ${G_A23} FGV182 ${DATA}/PTEM/GV181FBG.FGV182AP
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FCR004 ${DATA}/PTEM/GV181FBJ.FGV182BP
# *********************************************************                    
# *                                                                            
# *     EXTRACT DU CA - PGM = BGV189                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV181FBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          

       m_ProgramExec -b BGV182 
       JUMP_LABEL=GV181FBM
       ;;
(GV181FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A24} FDA189 ${DATA}/PTEM/GV181FAA.BDA189BP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GV181FAD.BDA189BO
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GV181FAJ.BDA189BY
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GV181FAM.BDA189BL
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GV181FAQ.BDA189BM
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GV181FAT.BDA189BD
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GV181FAG.BDA189BX
# ****** TABLE EN ENTREE                                                       
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FGV189 ${DATA}/PTEM/GV181FBM.BGV189CP
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV189CP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          

       m_ProgramExec -b BGV189 
       JUMP_LABEL=GV181FBQ
       ;;
(GV181FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GV181FBM.BGV189CP
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GV181FBJ.FGV182BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FBQ.BGV189FP
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183CP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_13_7 13 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FBT
       ;;
(GV181FBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/GV181FAX.BGV183CP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FBT.BGV183JP
# **********************************************************                   
#  PGM : BGV186                                                                
#  ------------                                                                
#  EDITION                                                                     
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_7_1 EQ CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FBU
       ;;
(GV181FBU)
       m_CondExec 00,EQ,GV181FBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV181FBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GV181FBX
       ;;
(GV181FBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A34} FGV189 ${DATA}/PTEM/GV181FBQ.BGV189FP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A35} FGV183 ${DATA}/PTEM/GV181FBT.BGV183JP
# *********** 1 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186H IGV186
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183CP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          

       m_ProgramExec -b BGV186 
       JUMP_LABEL=GV181FCA
       ;;
(GV181FCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/GV181FAX.BGV183CP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FCA.BGV183KP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_19_7 19 CH 7
 /CONDITION CND_1 FLD_CH_7_1 NE CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FCB
       ;;
(GV181FCB)
       m_CondExec 00,EQ,GV181FCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV196  NEW                                                           
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV181FCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GV181FCD
       ;;
(GV181FCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A37} FGV189 ${DATA}/PTEM/GV181FBQ.BGV189FP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A38} FGV183 ${DATA}/PTEM/GV181FCA.BGV183KP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186H IGV186
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV183CP ET FGV184BP                                      
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          

       m_ProgramExec -b BGV196 
       JUMP_LABEL=GV181FCG
       ;;
(GV181FCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A39} SORTIN ${DATA}/PTEM/GV181FAX.BGV183CP
       m_FileAssign -d SHR -g ${G_A40} -C ${DATA}/PTEM/GV181FBD.FGV184BP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FCG.FGV184CP
# **********************************************************                   
#                                                                              
#     TRI DES FICHIER BGV189CP ET FGV182BP                                     
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_14_5 14 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FCJ
       ;;
(GV181FCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/GV181FBM.BGV189CP
       m_FileAssign -d SHR -g ${G_A42} -C ${DATA}/PTEM/GV181FBJ.FGV182BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FCJ.BGV187AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_13_7 13 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FCK
       ;;
(GV181FCK)
       m_CondExec 00,EQ,GV181FCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV187                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV181FCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GV181FCM
       ;;
(GV181FCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A43} FGV189 ${DATA}/PTEM/GV181FCJ.BGV187AP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A44} FGV183 ${DATA}/PTEM/GV181FCG.FGV184CP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV187 IGV187
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER FGV184CP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV181FCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          

       m_ProgramExec -b BGV187 
       JUMP_LABEL=GV181FCQ
       ;;
(GV181FCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/GV181FCG.FGV184CP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV181FCQ.FGV184FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_1 7 CH 1
 /CONDITION CND_1 FLD_CH_7_1 NE CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV181FCR
       ;;
(GV181FCR)
       m_CondExec 00,EQ,GV181FCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV197  NEW                                                           
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV181FCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GV181FCT
       ;;
(GV181FCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A46} FGV189 ${DATA}/PTEM/GV181FCJ.BGV187AP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A47} FGV183 ${DATA}/PTEM/GV181FCQ.FGV184FP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV187 IGV187
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV197 
       JUMP_LABEL=GV181FCU
       ;;
(GV181FCU)
       m_CondExec 04,GE,GV181FCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV181FZA
       ;;
(GV181FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV181FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
