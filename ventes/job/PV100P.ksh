#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100P.ksh                       --- VERSION DU 08/10/2016 13:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 16.19.24 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV100P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100PA
       ;;
(PV100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       PV100PA=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=PV100PAA
       ;;
(PV100PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# ********************************************                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F07.PV100HP.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F07.PV100HP.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  :         EPURATION DE HV32 + HV33                              
#  -------------------------------------------------------------------         
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100PAD
       ;;
(PV100PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE DES VENDEURS                                                   
#    RTHV32   : NAME=RSHV32,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV32 /dev/null
#    RTHV33   : NAME=RSHV33,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV33 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *****   FICHIER PARAMETRE                                                    
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100PAE
       ;;
(PV100PAE)
       m_CondExec 04,GE,PV100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH REQUETE Q181                                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100PAG
       ;;
(PV100PAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[PV100PA] 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QMF181 DSQPRINT
       m_FileAssign -i QMFPARM
RUN Q181 (&&FMOIS='$FMOIS_3_4$FMOIS_1_2' &&FNSOC='$DIFDEP_1_3' FORM=F181
PRINT REPORT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p PV100P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=PV100PAH
       ;;
(PV100PAH)
       m_CondExec 04,GE,PV100PAG ${EXAAK},NE,YES 1,EQ,$[PV100PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH REQUETE QALARM (SUIVI DES VENTES DE TELESURVEILLANCE)              
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100PAJ
       ;;
(PV100PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QALARM DSQPRINT
       m_FileAssign -i QMFPARM
RUN QALARM (&&DMOIS='$FMOIS_3_4$FMOIS_1_2' FORM=FALARM
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p PV100P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=PV100PAK
       ;;
(PV100PAK)
       m_CondExec 04,GE,PV100PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH REQUETE QBVPA  (SUIVI DES VENTES)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100PAM
       ;;
(PV100PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QBVPA DSQPRINT
       m_FileAssign -i QMFPARM
RUN QBVPA (&&DMOIS='$FMOIS_3_4$FMOIS_1_2' FORM=FBVPA
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p PV100P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=PV100PAN
       ;;
(PV100PAN)
       m_CondExec 04,GE,PV100PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH REQUETE QBV039B (SUIVI DES VENTES)                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IKJEFT01,PATTERN=EO4,PATKW=(L=QBV039B,S=RDAR,             
#                M=PV100P4)                                                    
# QMFPARM  DATA  CLASS=VAR,PARMS=QBV039B,MBR=PV100P4                           
# ********************************************************************         
#  QMFBATCH REQUETE Q329509 (SUIVI MAGASIN DE L'OPERATION LE 9 TELECOM         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100PAQ
       ;;
(PV100PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w Q329509 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.Q329509 (&&FDATE='$VDATEJ__ANNMMDD' &&SOC='$DIFDEP_1_3' FORM=ADMFIL.F329509
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p PV100P5 -a QMFPARM DSQPRINT
       JUMP_LABEL=PV100PAR
       ;;
(PV100PAR)
       m_CondExec 04,GE,PV100PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
