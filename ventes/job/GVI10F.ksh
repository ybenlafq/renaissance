#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GVI10F.ksh                       --- VERSION DU 09/10/2016 05:48
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGVI10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/06/23 AT 15.43.00 BY BURTECM                      
#    STANDARDS: P  JOBSET: GVI10F                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  STEP DE TRI FUSION DES FICHIERS CONTENANT LES DONNEES NMD ISSUES            
#  DES CHAINES GV005* ET MG925* -> FIC BGV005L*                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GVI10FA
       ;;
(GVI10FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GVI10FAA
       ;;
(GVI10FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BGV005LD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.BGV005LL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BGV005LM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BGV005LO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BGV005LP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGR/F94.BGV005LR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.BGV005LY
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BGV005LX
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GVI10FAA.BGV005LF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_2  "999"
 /FIELDS FLD_CH_72_6 72 CH 6
 /FIELDS FLD_CH_50_6 50 CH 6
 /FIELDS FLD_CH_44_6 44 CH 6
 /FIELDS FLD_CH_4_177 4 CH 177
 /FIELDS FLD_CH_66_6 66 CH 6
 /FIELDS FLD_CH_62_4 62 CH 4
 /FIELDS FLD_CH_56_6 56 CH 6
 /FIELDS FLD_CH_40_4 40 CH 4
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_CH_82_6 82 CH 6
 /FIELDS FLD_CH_78_4 78 CH 4
 /KEYS
   FLD_CH_1_39 ASCENDING
 /MT_INFILE_SORT SORTIN
 /REFORMAT CST_0_2,FLD_CH_4_177
 /SUMMARIZE 
    TOTAL FLD_CH_40_4,
    TOTAL FLD_CH_44_6,
    TOTAL FLD_CH_50_6,
    TOTAL FLD_CH_56_6,
    TOTAL FLD_CH_62_4,
    TOTAL FLD_CH_66_6,
    TOTAL FLD_CH_72_6,
    TOTAL FLD_CH_78_4,
    TOTAL FLD_CH_82_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GVI10FAB
       ;;
(GVI10FAB)
       m_CondExec 00,EQ,GVI10FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  STEP DE TRI FUSION DES FICHIERS CONTENANT LES DONNEES NGI ISSUES            
#  DES CHAINES MG925* -> FIC BGVI06B*                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GVI10FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GVI10FAD
       ;;
(GVI10FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.BGVI06BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.BGVI06BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BGVI06BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BGVI06BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GVI10FAD.BGVI06BF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_0_2  "999"
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_72_6 72 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_158_6 158 PD 6
 /FIELDS FLD_CH_4_177 4 CH 177
 /FIELDS FLD_PD_132_4 132 PD 4
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_56_6 56 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_78_4 78 PD 4
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_148_4 148 PD 4
 /FIELDS FLD_PD_136_6 136 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_174_6 174 PD 6
 /FIELDS FLD_PD_168_6 168 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_152_6 152 PD 6
 /FIELDS FLD_PD_164_4 164 PD 4
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_PD_62_4 62 PD 4
 /FIELDS FLD_PD_50_6 50 PD 6
 /KEYS
   FLD_CH_1_39 ASCENDING
 /MT_INFILE_SORT SORTIN
 /REFORMAT CST_0_2,FLD_CH_4_177
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_6,
    TOTAL FLD_PD_62_4,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_6,
    TOTAL FLD_PD_78_4,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_4,
    TOTAL FLD_PD_136_6,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_4,
    TOTAL FLD_PD_152_6,
    TOTAL FLD_PD_158_6,
    TOTAL FLD_PD_164_4,
    TOTAL FLD_PD_168_6,
    TOTAL FLD_PD_174_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=GVI10FAE
       ;;
(GVI10FAE)
       m_CondExec 00,EQ,GVI10FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGVI10                                                                
#  ------------                                                                
#  EDITION ETAT SUIVI DES VENTES HEBDO POUR GROUPE (ETAT IGVI10)               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GVI10FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GVI10FAG
       ;;
(GVI10FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ENTREE : TABLE EN LECTURE                                            
#    RSGA11   : NAME=RSGA11B,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******* ENTREE : FICHIER FUSION DES DONNEES NMD                              
       m_FileAssign -d SHR -g ${G_A1} FICINNMD ${DATA}/PTEM/GVI10FAA.BGV005LF
# ******* ENTREE : FICHIER FUSION DES DONNEES MGI                              
       m_FileAssign -d SHR -g ${G_A2} FICINMGI ${DATA}/PTEM/GVI10FAD.BGVI06BF
# ******* ENTREE : PARAMETRES DATES                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
# ******* SORTIE : EDITION ETAT IGVI10                                         
       m_OutputAssign -c 9 -w IGVI10 IGVI10
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGVI10 
       JUMP_LABEL=GVI10FAH
       ;;
(GVI10FAH)
       m_CondExec 04,GE,GVI10FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES FICHIERS BGV005L*                                         
#  CREE UNE GENERATION A VIDE DES FICHIERS BGV005L*                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GVI10FAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GVI10FAJ
       ;;
(GVI10FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT1 ${DATA}/PNCGB/F96.BGV005LB
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT2 ${DATA}/PNCGD/F91.BGV005LD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT3 ${DATA}/PNCGL/F61.BGV005LL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT4 ${DATA}/PNCGM/F89.BGV005LM
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT5 ${DATA}/PNCGO/F16.BGV005LO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT6 ${DATA}/PNCGP/F07.BGV005LP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT7 ${DATA}/PNCGR/F94.BGV005LR
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT8 ${DATA}/PNCGY/F45.BGV005LY
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT9 ${DATA}/P908/SEM.BGV005LX
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GVI10FAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GVI10FAK
       ;;
(GVI10FAK)
       m_CondExec 16,NE,GVI10FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES FICHIERS BGVI06B*                                         
#  CREE UNE GENERATION A VIDE DES FICHIERS BGVI06B*                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GVI10FAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GVI10FAM
       ;;
(GVI10FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT1 ${DATA}/PNCGL/F61.BGVI06BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT2 ${DATA}/PNCGY/F45.BGVI06BY
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT3 ${DATA}/PNCGD/F91.BGVI06BD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 OUT4 ${DATA}/PNCGO/F16.BGVI06BO
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GVI10FAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GVI10FAN
       ;;
(GVI10FAN)
       m_CondExec 16,NE,GVI10FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GVI10FZA
       ;;
(GVI10FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GVI10FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
