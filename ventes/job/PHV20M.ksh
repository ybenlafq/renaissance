#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV20M.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPHV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/12/30 AT 15.33.54 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PHV20M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#     PURGE DES HISTO DETAILS VENTES PAR MAG/ MOIS (RTHV20)                    
#               ET    HISTO DES PSE PAR MAG/MOIS   (RTHV26)                    
#                                                                              
#           GARDER 2 ANS + L'HISTO EN COURS                                    
#           ==> ON FORCE DANS BETDATC LE MOIS LU DANS FEXERCIC                 
#                  ET ON SOUSTRAIT NJOURS     LU DANS FCARTE                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV20MA
       ;;
(PHV20MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PHV20MAA
       ;;
(PHV20MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   HISTO DETAILS  VENTES  MAG/MOIS                                      
#    RSHV20M  : NAME=RSHV20M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV20M /dev/null
# *****   HISTO VENTES PSE MAG/MOIS                                            
#    RSHV26M  : NAME=RSHV26M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV26M /dev/null
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 0731 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV20MAA
# *****   N� DU MOIS DE L'EXERCICE DARTY : 02                                  
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV200 
       JUMP_LABEL=PHV20MAB
       ;;
(PHV20MAB)
       m_CondExec 04,GE,PHV20MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTHV20                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20MAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PHV20MAD
       ;;
(PHV20MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 51 -g +1 SYSREC01 ${DATA}/PXX0/F89.HV20UNM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20MAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSHV20                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PHV20MR0                                     
#                  VERIFIER LE BACKOUT RPHV20M ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20MAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV20MAJ
       ;;
(PHV20MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SYSREC01 ${DATA}/PXX0/F89.HV26UNM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20MAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSHV26                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PHV20MR1                                     
#                  VERIFIER LE BACKOUT RPHV20M ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20MAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV20MZA
       ;;
(PHV20MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
