#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PYGSM00.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGSM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/30 AT 11.18.37 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM00Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#       TRI DU FICHIER D'EXTRACTION DES STOCKS MAGASINS                        
#       ***********************************************                        
#  (VERSION/SOCIETE/LIEU/NO SEQUENCE/LIBELLE MARKETING 1 2 3/MAYQUE/           
#   MONTANT POUR TRI/CODIC1/CODIC2)                                            
# OMIT COND=(1,3,EQ,C'TM4',OR,1,3,EQ,C'EM1',OR,1,3,EQ,C'TM2'),FORMAT=C         
#   REPRISE OUI                                                                
# ********************************************************************         
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU DIMANCHE                    
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM00YA
       ;;
(GSM00YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       GSM00YA=${LUNDI}
       GSM00YB=${MARDI}
       GSM00YC=${MERCRED}
       GSM00YD=${JEUDI}
       GSM00YE=${VENDRED}
       GSM00YF=${SAMEDI}
       GSM00YG=${DIMANCH}
       JUMP_LABEL=GSM00YAA
       ;;
(GSM00YAA)
       m_CondExec ${EXAAA},NE,YES 1,EQ,$[GSM00YF] 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_6_25 FORMA
 /DERIVEDFIELD CST_5_24 "TM2")
 /DERIVEDFIELD CST_1_16 "TM4"
 /DERIVEDFIELD CST_3_20 "EM1"
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 CST_6_25 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAB
       ;;
(GSM00YAB)
       m_CondExec 00,EQ,GSM00YAA ${EXAAA},NE,YES 1,EQ,$[GSM00YF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU LUNDI                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAD
       ;;
(GSM00YAD)
       m_CondExec ${EXAAF},NE,YES 1,EQ,$[GSM00YG] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A1} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAE
       ;;
(GSM00YAE)
       m_CondExec 00,EQ,GSM00YAD ${EXAAF},NE,YES 1,EQ,$[GSM00YG] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MARDI                       
#                   EDITION TOTAL DES STOCKS                                   
#    OMIT COND=(1,3,EQ,C'EM1'),FORMAT=CH                                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAG
       ;;
(GSM00YAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[GSM00YA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A2} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_BI_92_5 92 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /KEYS
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAH
       ;;
(GSM00YAH)
       m_CondExec 00,EQ,GSM00YAG ${EXAAK},NE,YES 1,EQ,$[GSM00YA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MERCREDI                    
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAJ
       ;;
(GSM00YAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[GSM00YB] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A3} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAK
       ;;
(GSM00YAK)
       m_CondExec 00,EQ,GSM00YAJ ${EXAAP},NE,YES 1,EQ,$[GSM00YB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU JEUDI                       
#    OMIT COND=(1,3,EQ,C'EM1'),FORMAT=CH                                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAM
       ;;
(GSM00YAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[GSM00YC] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A4} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /KEYS
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAN
       ;;
(GSM00YAN)
       m_CondExec 00,EQ,GSM00YAM ${EXAAU},NE,YES 1,EQ,$[GSM00YC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU VENDREDI                    
#    OMIT COND=(1,3,EQ,C'EM1'),FORMAT=CH                                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAQ
       ;;
(GSM00YAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[GSM00YD] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A5} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /KEYS
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAR
       ;;
(GSM00YAR)
       m_CondExec 00,EQ,GSM00YAQ ${EXAAZ},NE,YES 1,EQ,$[GSM00YD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU SAMEDI                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM00YAT
       ;;
(GSM00YAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[GSM00YE] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGM075DY
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A6} SORTOUT ${DATA}/PXX0/F45.BSM025BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_207_5 207 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00YAU
       ;;
(GSM00YAU)
       m_CondExec 00,EQ,GSM00YAT ${EXABE},NE,YES 1,EQ,$[GSM00YE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
