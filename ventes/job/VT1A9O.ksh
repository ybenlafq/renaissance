#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VT1A9O.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVT1A9 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/09/24 AT 18.03.42 BY PREPA2                       
#    STANDARDS: P  JOBSET: VT1A9O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REMISE A ZERO DES GDG                                                       
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ****************************************************************             
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VT1A9OA
       ;;
(VT1A9OA)
#
#VT1A9OAJ
#VT1A9OAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#VT1A9OAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2010/09/24 AT 18.03.42 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: VT1A9O                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'ALT-VT900O'                            
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VT1A9OAA
       ;;
(VT1A9OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT1 ${DATA}/PARG/SEM.VT900OF
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT2 ${DATA}/PARG/SEM.VT900OG
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT3 ${DATA}/PARG/SEM.VT900OH
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT4 ${DATA}/PARG/SEM.VT900OI
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT5 ${DATA}/PARG/SEM.VT900OL
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT6 ${DATA}/PARG/SEM.VT900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT7 ${DATA}/PARG/SEM.VT900OJ
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT1A9OAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VT1A9OAB
       ;;
(VT1A9OAB)
       m_CondExec 16,NE,VT1A9OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN : VT900O                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT1A9OAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
