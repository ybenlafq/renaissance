#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV21L.ksh                       --- VERSION DU 19/10/2016 16:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGPV21 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/11/15 AT 18.20.34 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GPV21L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV21LA
       ;;
(GPV21LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV21LAA
       ;;
(GPV21LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************
#  DEPENDANCES POUR PLAN :             *
# **************************************
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# 
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )
# 
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F61.DATPV21L
# 
# ------  TABLES EN LECTURE
# 
#    RTGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA66   : NAME=RSGA66L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA73   : NAME=RSGA73L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA73 /dev/null
#    RTGA75   : NAME=RSGA75L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGS30 /dev/null
# 
# ------  FICHIERS D'EXTRACTION
# 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PTEM/GPV21LAA.HPV200AL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FPV220 ${DATA}/PTEM/GPV21LAA.HPV200BL
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV21LAB
       ;;
(GPV21LAB)
       m_CondExec 04,GE,GPV21LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER HPV200AR (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)
#  ET CREATION DU FICHIER HPV211AR ENTRANT DS LE PGM BPV211
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAD PGM=SORT       ** ID=AAF
# ***********************************
       JUMP_LABEL=GPV21LAD
       ;;
(GPV21LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GPV21LAA.HPV200AL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LAD.HPV211AL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LAE
       ;;
(GPV21LAE)
       m_CondExec 00,EQ,GPV21LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER HPV200BR (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,
#  ET CREATION DU FICHIER HPV211BR ENTRANT DS LE PGM BPV211
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAG PGM=SORT       ** ID=AAK
# ***********************************
       JUMP_LABEL=GPV21LAG
       ;;
(GPV21LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GPV21LAA.HPV200BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LAG.HPV211BL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LAH
       ;;
(GPV21LAH)
       m_CondExec 00,EQ,GPV21LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BPV210
# ********************************************************************
#  EXTRACTION DES DONNEES EN VUE DE CREER UN FICHIER D'EXTRACT
#  ENTRANT DS LE GENERATEUR D'ETAT (INTERESSEMENT HEBDO)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAJ PGM=IKJEFT01   ** ID=AAP
# ***********************************
       JUMP_LABEL=GPV21LAJ
       ;;
(GPV21LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# 
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )
# 
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/R961SOC
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F61.DATPV21L
# 
# ------  TABLES EN LECTURE
# 
#    RTGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA66   : NAME=RSGA66L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA73   : NAME=RSGA73L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA73 /dev/null
#    RTGA75   : NAME=RSGA75L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGS30 /dev/null
# 
# ------  FICHIER D'EXTRACTION
# 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTRAC ${DATA}/PTEM/GPV21LAJ.HPV210AL
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BPV210 
       JUMP_LABEL=GPV21LAK
       ;;
(GPV21LAK)
       m_CondExec 04,GE,GPV21LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER HPV210AR (FEXTRAC) ISSU DE BPV210 ET CREATION DU FIC
#  HPV210BR ENTRANT DANS LE PGM BPV211
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAM PGM=SORT       ** ID=AAU
# ***********************************
       JUMP_LABEL=GPV21LAM
       ;;
(GPV21LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GPV21LAJ.HPV210AL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LAM.HPV210BL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_62_7 62 CH 7
 /KEYS
   FLD_CH_62_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LAN
       ;;
(GPV21LAN)
       m_CondExec 00,EQ,GPV21LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BPV211
# ********************************************************************
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAQ PGM=IKJEFT01   ** ID=AAZ
# ***********************************
       JUMP_LABEL=GPV21LAQ
       ;;
(GPV21LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# 
# ------- TABLES EN LECTURE
# 
#    RTGA11   : NAME=RSGA11L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA12   : NAME=RSGA12L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA25   : NAME=RSGA25L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA26   : NAME=RSGA26L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA26 /dev/null
#    RTGA53   : NAME=RSGA53L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA53 /dev/null
# 
# ------- FICHIER D'EXTRACTION ISSU DES TRIS PRECEDENTS
# 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC1 ${DATA}/PTEM/GPV21LAD.HPV211AL
       m_FileAssign -d SHR -g ${G_A5} FPV220 ${DATA}/PTEM/GPV21LAG.HPV211BL
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC2 ${DATA}/PTEM/GPV21LAM.HPV210BL
# 
# ------- FICHIERS D'EDITION ENTRANT DANS LE GENERATEUR D'ETAT
# 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTR1B ${DATA}/PTEM/GPV21LAQ.IPV200AL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FEXTR2B ${DATA}/PTEM/GPV21LAQ.IPV210AL
# 
# ------- FICHIER FPV220B EN DUMMY
# 
       m_FileAssign -d SHR FPV220B /dev/null
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV21LAR
       ;;
(GPV21LAR)
       m_CondExec 04,GE,GPV21LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER IPV210AR POUR CREATION DU FICHIER (FEXTRAC1)
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV210)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAT PGM=SORT       ** ID=ABE
# ***********************************
       JUMP_LABEL=GPV21LAT
       ;;
(GPV21LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GPV21LAQ.IPV210AL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LAT.IPV210BL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_185_5 185 PD 5
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_PD_190_5 190 PD 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_30_3 30 CH 3
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_43_5 ASCENDING,
   FLD_CH_48_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_185_5,
    TOTAL FLD_PD_190_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LAU
       ;;
(GPV21LAU)
       m_CondExec 00,EQ,GPV21LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BEG050
# ********************************************************************
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV210
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LAX PGM=IKJEFT01   ** ID=ABJ
# ***********************************
       JUMP_LABEL=GPV21LAX
       ;;
(GPV21LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# 
# ------  TABLE DU GENERATEUR D'ETATS
# 
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG30 /dev/null
# 
# ------  PREMIER FICHIER FEXTRAC1
# 
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/GPV21LAT.IPV210BL
# 
# ------  FICHIER FCUMULS
# 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GPV21LAX.IPV210CL
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21LAY
       ;;
(GPV21LAY)
       m_CondExec 04,GE,GPV21LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER FCUMULS (IPV210CR) ISSU DU BEG050 POUR CREATION DU
#  FICHIER IPV210DR
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBA PGM=SORT       ** ID=ABO
# ***********************************
       JUMP_LABEL=GPV21LBA
       ;;
(GPV21LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GPV21LAX.IPV210CL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LBA.IPV210DL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LBB
       ;;
(GPV21LBB)
       m_CondExec 00,EQ,GPV21LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BEG060
# ********************************************************************
#  CREATION DU PREMIER ETAT IPV210
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBD PGM=IKJEFT01   ** ID=ABT
# ***********************************
       JUMP_LABEL=GPV21LBD
       ;;
(GPV21LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# 
# ------  TABLES EN LECTURE
# 
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG30 /dev/null
# 
# ------  FICHIER FEXTRAC ET FCUMUL
# 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/GPV21LAT.IPV210BL
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/GPV21LBA.IPV210DL
# 
# ------  PARAMETRE DATE , FMOIS , SOCIETE
# 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$FMOIS
_end
# 
# ------  CREATION DE L'ETAT IPV210
# 
       m_OutputAssign -c 9 -w IPV210 FEDITION
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21LBE
       ;;
(GPV21LBE)
       m_CondExec 04,GE,GPV21LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER IPV210A POUR CREATION DU FICHIER (FEXTRAC1)
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV210)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBG PGM=SORT       ** ID=ABY
# ***********************************
       JUMP_LABEL=GPV21LBG
       ;;
(GPV21LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GPV21LAQ.IPV210AL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LBG.IPV210EL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "E"
 /DERIVEDFIELD CST_3_15 "S"
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_PD_190_5 190 PD 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_185_5 185 PD 5
 /FIELDS FLD_CH_181_1 181 CH 1
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_10_20 10 CH 20
 /CONDITION CND_2 FLD_CH_181_1 EQ CST_1_11 OR FLD_CH_181_1 EQ CST_3_15 
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_43_5 ASCENDING,
   FLD_CH_48_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_185_5,
    TOTAL FLD_PD_190_5
 /INCLUDE CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LBH
       ;;
(GPV21LBH)
       m_CondExec 00,EQ,GPV21LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BEG050
# ********************************************************************
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV210
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBJ PGM=IKJEFT01   ** ID=ACD
# ***********************************
       JUMP_LABEL=GPV21LBJ
       ;;
(GPV21LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# 
# ------  TABLE DU GENERATEUR D'ETATS
# 
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG30 /dev/null
# 
# ------  PREMIER FICHIER FEXTRAC1
# 
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/GPV21LBG.IPV210EL
# 
# ------  FICHIER FCUMULS
# 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/GPV21LBJ.IPV210FL
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21LBK
       ;;
(GPV21LBK)
       m_CondExec 04,GE,GPV21LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER FCUMULS (IPV210FL) ISSU DU BEG050 POUR CREATION DU
#  FICHIER IPV210GR
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBM PGM=SORT       ** ID=ACI
# ***********************************
       JUMP_LABEL=GPV21LBM
       ;;
(GPV21LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GPV21LBJ.IPV210FL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GPV21LBM.IPV210GL
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LBN
       ;;
(GPV21LBN)
       m_CondExec 00,EQ,GPV21LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : BEG060
# ********************************************************************
#  CREATION DU SECOND  ETAT IPV210
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBQ PGM=IKJEFT01   ** ID=ACN
# ***********************************
       JUMP_LABEL=GPV21LBQ
       ;;
(GPV21LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# 
# ------  TABLES EN LECTURE
# 
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES
       m_FileAssign -d SHR RTEG30 /dev/null
# 
# ------  FICHIER FEXTRAC ET FCUMUL
# 
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/GPV21LBG.IPV210EL
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/GPV21LBM.IPV210GL
# 
# ------  PARAMETRE DATE , FMOIS , SOCIETE
# 
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$FMOIS
_end
# 
# ------  CREATION DE L'ETAT IPV210M
# 
       m_OutputAssign -c 9 -w IPV210M FEDITION
# 
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***
# ************************************************

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21LBR
       ;;
(GPV21LBR)
       m_CondExec 04,GE,GPV21LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER IPV200A  POUR CREATION DU FICHIER (FEXTRAC1)
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV200)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
#  ACS      SORT
#  SORTIN   FILE  NAME=IPV200AL,MODE=I
#  SORTOUT  FILE  NAME=IPV200BL,MODE=O
#  SYSIN    DATA  *
#   SORT  FIELDS=(7,3,A,10,3,A,13,3,A,16,10,A,26,5,A,31,5,A,
#                36,4,A,40,5,A,45,7,A,52,2,A),FORMAT=BI
#          DATAEND
# ********************************************************************
#  PGM : BEG050
# ********************************************************************
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV200
# ********************************************************************
#  ACX      STEP  PGM=IKJEFT01
#  SYSOUT   REPORT SYSOUT=*
#  SYSTSPRT REPORT SYSOUT=*
# 
# ------  TABLE DU GENERATEUR D'ETATS
# 
#  RTEG00   FILE  DYNAM=YES,NAME=RSEG00L,MODE=I
#  RTEG05   FILE  DYNAM=YES,NAME=RSEG05L,MODE=I
#  RTEG10   FILE  DYNAM=YES,NAME=RSEG10L,MODE=I
#  RTEG15   FILE  DYNAM=YES,NAME=RSEG15L,MODE=I
#  RTEG25   FILE  DYNAM=YES,NAME=RSEG25L,MODE=I
#  RTEG30   FILE  DYNAM=YES,NAME=RSEG30L,MODE=I
# 
# ------  PREMIER FICHIER FEXTRAC1
# 
#  FEXTRAC  FILE  NAME=IPV200BL,MODE=I
# 
# ------  FICHIER FCUMULS
# 
#  FCUMULS  FILE  NAME=IPV200CL,MODE=O
# 
#  SYSTSIN  DATA  *,CLASS=FIX2
#   DSN SYSTEM(RDAR)
#   RUN PROGRAM(BEG050) PLAN(BEG050L)
#   END
#          DATAEND
# ********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER FCUMULS (IPV200C) ISSU DU BEG050 POUR CREATION DU
#  FICHIER IPV200D
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
#  ADC      SORT
#  SORTIN   FILE  NAME=IPV200CL,MODE=I
#  SORTOUT  FILE  NAME=IPV200DL,MODE=O
#  SYSIN    DATA  *
#   SORT FIELDS=(1,512,A),FORMAT=BI
#           DATAEND
# ********************************************************************
#  PGM : BEG060
# ********************************************************************
#  CREATION DU PREMIER ETAT IPV200 (RECAP OFFRE ACTIVE HEBDO)
# ********************************************************************
#  ADH      STEP  PGM=IKJEFT01
#  SYSOUT   REPORT SYSOUT=*
#  SYSTSPRT REPORT SYSOUT=*
# 
# ------  TABLES EN LECTURE
# 
#  RTGA01   FILE  DYNAM=YES,NAME=RSGA01L,MODE=I
#  RTGA71   FILE  DYNAM=YES,NAME=RSGA71L,MODE=I
#  RTEG00   FILE  DYNAM=YES,NAME=RSEG00L,MODE=I
#  RTEG05   FILE  DYNAM=YES,NAME=RSEG05L,MODE=I
#  RTEG10   FILE  DYNAM=YES,NAME=RSEG10L,MODE=I
#  RTEG11   FILE  DYNAM=YES,NAME=RSEG11L,MODE=I
#  RTEG15   FILE  DYNAM=YES,NAME=RSEG15L,MODE=I
#  RTEG20   FILE  DYNAM=YES,NAME=RSEG20L,MODE=I
#  RTEG25   FILE  DYNAM=YES,NAME=RSEG25L,MODE=I
#  RTEG30   FILE  DYNAM=YES,NAME=RSEG30L,MODE=I
# 
# ------  FICHIER FEXTRAC ET FCUMUL
# 
#  FEXTRAC  FILE  NAME=IPV200BL,MODE=I
#  FCUMULS  FILE  NAME=IPV200DL,MODE=I
# 
# ------  PARAMETRE DATE , FMOIS , SOCIETE
# 
#  FDATE    DATA  CLASS=VAR,PARMS=FDATE
#  FNSOC    DATA  CLASS=FIX1,MBR=SOCDNPC
#  FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=GPV21L3
# 
# ------  CREATION DE L'ETAT IPV200
# 
#  //FEDITION DD SYSOUT=(9,IPV200),RECFM=VA,SPIN=UNALLOC
# 
#  SYSTSIN  DATA  *,CLASS=FIX2
#   DSN SYSTEM(RDAR)
#   RUN PROGRAM(BEG060) PLAN(BEG060L)
#   END
#           DATAEND
# ********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER IPV200A  POUR CREATION DU FICHIER (FEXTRAC1)
#  ENTRANT DANS LE GENERATEUR D'ETAT (ETAT IPV200)
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
#  ADM      SORT
#  SORTIN   FILE  NAME=IPV200AL,MODE=I
#  SORTOUT  FILE  NAME=IPV200EL,MODE=O
#  SYSIN    DATA  *
#   SORT  FIELDS=(7,3,A,10,3,A,13,3,A,16,10,A,26,5,A,31,5,A,
#                 36,4,A,40,5,A,45,7,A,52,2,A),FORMAT=BI
#   INCLUDE COND=(164,1,CH,EQ,C'E',OR,164,1,CH,EQ,C'S')
#           DATAEND
# ********************************************************************
#  PGM : BEG050
# ********************************************************************
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE SECOND ETAT IPV200
# ********************************************************************
#  ADR      STEP  PGM=IKJEFT01
#  SYSOUT   REPORT SYSOUT=*
#  SYSTSPRT REPORT SYSOUT=*
# 
# ------  TABLE DU GENERATEUR D'ETATS
# 
#  RTEG00   FILE  DYNAM=YES,NAME=RSEG00L,MODE=I
#  RTEG05   FILE  DYNAM=YES,NAME=RSEG05L,MODE=I
#  RTEG10   FILE  DYNAM=YES,NAME=RSEG10L,MODE=I
#  RTEG15   FILE  DYNAM=YES,NAME=RSEG15L,MODE=I
#  RTEG25   FILE  DYNAM=YES,NAME=RSEG25L,MODE=I
#  RTEG30   FILE  DYNAM=YES,NAME=RSEG30L,MODE=I
# 
# ------  PREMIER FICHIER FEXTRAC1
# 
#  FEXTRAC  FILE  NAME=IPV200EL,MODE=I
# 
# ------  FICHIER FCUMULS
# 
#  FCUMULS  FILE  NAME=IPV200FL,MODE=O
# 
#  SYSTSIN  DATA  *,CLASS=FIX2
#   DSN SYSTEM(RDAR)
#   RUN PROGRAM(BEG050) PLAN(BEG050L)
#   END
#           DATAEND
# ********************************************************************
#  PGM : SORT
# ********************************************************************
#  TRI DU FICHIER FCUMULS (IPV200F) ISSU DU BEG050 POUR CREATION DU
#  FICHIER IPV200G
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
#  ADW      SORT
#  SORTIN   FILE  NAME=IPV200FL,MODE=I
#  SORTOUT  FILE  NAME=IPV200GL,MODE=O
#  SYSIN    DATA  *
#   SORT FIELDS=(1,512,A),FORMAT=BI
#           DATAEND
# ********************************************************************
#  PGM : BEG060
# ********************************************************************
#  CREATION DU DEUXIEME ETAT IPV200  (CODIC EN MAJ SUR UNE SEMAINE)
# ********************************************************************
#  AEB      STEP  PGM=IKJEFT01
#  SYSOUT   REPORT SYSOUT=*
#  SYSTSPRT REPORT SYSOUT=*
# 
# ------  TABLES EN LECTURE
# 
#  RTGA01   FILE  DYNAM=YES,NAME=RSGA01L,MODE=I
#  RTGA71   FILE  DYNAM=YES,NAME=RSGA71L,MODE=I
#  RTEG00   FILE  DYNAM=YES,NAME=RSEG00L,MODE=I
#  RTEG05   FILE  DYNAM=YES,NAME=RSEG05L,MODE=I
#  RTEG10   FILE  DYNAM=YES,NAME=RSEG10L,MODE=I
#  RTEG11   FILE  DYNAM=YES,NAME=RSEG11L,MODE=I
#  RTEG15   FILE  DYNAM=YES,NAME=RSEG15L,MODE=I
#  RTEG20   FILE  DYNAM=YES,NAME=RSEG20L,MODE=I
#  RTEG25   FILE  DYNAM=YES,NAME=RSEG25L,MODE=I
#  RTEG30   FILE  DYNAM=YES,NAME=RSEG30L,MODE=I
# 
# ------  FICHIER FEXTRAC ET FCUMUL
# 
#  FEXTRAC  FILE  NAME=IPV200EL,MODE=I
#  FCUMULS  FILE  NAME=IPV200GL,MODE=I
# 
# ------  PARAMETRE DATE , FMOIS , SOCIETE
# 
#  FDATE    DATA  CLASS=VAR,PARMS=FDATE
#  FNSOC    DATA  CLASS=FIX1,MBR=SOCDNPC
#  FMOIS    DATA  CLASS=VAR,PARMS=FMOISP,MBR=GPV21L4
# 
# ------  CREATION DE L'ETAT IPV200M
# 
#  //FEDITION DD SYSOUT=(9,IPV200M),RECFM=VA,SPIN=UNALLOC
# 
#  SYSTSIN  DATA  *,CLASS=FIX2
#   DSN SYSTEM(RDAR)
#   RUN PROGRAM(BEG060) PLAN(BEG060L)
#   END
#           DATAEND
# ********************************************************************
#  PGM : SORT
# ********************************************************************
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE
# ********************************************************************
#  REPRISE: OUI
# ********************************************************************
# 
# ***********************************
# *   STEP GPV21LBT PGM=SORT       ** ID=AEG
# ***********************************
       JUMP_LABEL=GPV21LBT
       ;;
(GPV21LBT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F61.DATPV21L
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************************
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***
# ************************************************
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_16_10 16 CH 10
 /FIELDS FLD_CH_26_5 26 CH 5
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_10 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_31_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21LBU
       ;;
(GPV21LBU)
       m_CondExec 00,EQ,GPV21LBT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************
# ********************************************************************
# 
# ***********************************
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98
# ***********************************
       JUMP_LABEL=GPV21LZA
       ;;
(GPV21LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV21LZA.sysin
       m_UtilityExec
# *********************************************************************
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
# ************************************
# *      STEP ZABCOND/ZABEND      ****
# ************************************
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
