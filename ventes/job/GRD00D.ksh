#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRD00D.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGRD00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/05/16 AT 10.27.38 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GRD00D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DU TABLESPACE RSRD00D                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRD00DA
       ;;
(GRD00DA)
#
#GRD00DAA
#GRD00DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GRD00DAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRD00DAD
       ;;
(GRD00DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BHV030AD
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00DAD.BRD005FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_11 "VEN"
 /DERIVEDFIELD CST_1_7 "VEN"
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_33_7 33 CH 7
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_7 OR FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_7 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DAE
       ;;
(GRD00DAE)
       m_CondExec 00,EQ,GRD00DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD005                                                                
# ********************************************************************         
#   TRAITEMENT DE LA REDEVANCE AUDIOVISUELLE                                   
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAG
       ;;
(GRD00DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A1} FGS41 ${DATA}/PTEM/GRD00DAD.BRD005FD
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA01   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES FAMILLES/TYPE DE DECLARATION                               
#    RSGA15   : NAME=RSGA15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15 /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22   : NAME=RSGA22D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02   : NAME=RSGV02D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV02 /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10   : NAME=RSGV10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10 /dev/null
# ******  TABLE DES LIGNES DE VENTES                                           
#    RSGV11   : NAME=RSGV11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00   : NAME=RSRD00D,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00 /dev/null
# ******  FICHIER DES VENTES A DECLARER                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 FRD005 ${DATA}/PXX0/F91.BRD005HD
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 FRD006 ${DATA}/PTEM/GRD00DAG.BRD006AD
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 278 -g +1 FRD007 ${DATA}/PTEM/GRD00DAG.BRD007AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD005 
       JUMP_LABEL=GRD00DAH
       ;;
(GRD00DAH)
       m_CondExec 04,GE,GRD00DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD020                                                                
# ********************************************************************         
#  EDITION DES VENTES SOUMISES A LA REDEVANCE AUDIOVISUELLE                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#  AAK      STEP  PGM=BRD020,LANG=CBL                                          
#  FICHIER DATE                                                                
# FDATE    DATA CLASS=VAR,PARMS=FDATE                                          
#  FDATE    FILE  NAME=FDATE,MODE=I                                            
#  FICHIER DES VENTES A DECLARER                                               
#  FRD005   FILE  NAME=BRD005HD,MODE=I                                         
#  EDITION DES VENTES A DECLARER                                               
#  IRD020   REPORT SYSOUT=(9,IRD020)                                           
# ********************************************************************         
#  PGM : BRD015                                                                
# ********************************************************************         
#  EDITION DES ANOMALIES                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAJ PGM=BRD015     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAJ
       ;;
(GRD00DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d SHR -g ${G_A2} FRD007 ${DATA}/PTEM/GRD00DAG.BRD007AD
# ******  EDITION DES ANOMALIES                                                
# IRD015   REPORT SYSOUT=(9,IRD015)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD015 ${DATA}/PXX0/F91.BIRD015D
       m_ProgramExec BRD015 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAM PGM=IEBGENER   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAM
       ;;
(GRD00DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PXX0/F91.BIRD015D
       m_OutputAssign -c 9 -w IRD015 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00DAN
       ;;
(GRD00DAN)
       m_CondExec 00,EQ,GRD00DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD006 AVANT EDITION DE CONTROLE                           
#   MAG + N� DE VENTE  17,10,A                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAQ
       ;;
(GRD00DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GRD00DAG.BRD006AD
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 SORTOUT ${DATA}/PEX0/F91.BRD006BD.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_10 17 CH 10
 /KEYS
   FLD_CH_17_10 ASCENDING
 /* Record Type = F  Record Length = 191 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DAR
       ;;
(GRD00DAR)
       m_CondExec 00,EQ,GRD00DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD010                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAT PGM=BRD010     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAT
       ;;
(GRD00DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A5} FRD006 ${DATA}/PEX0/F91.BRD006BD.ETAT
# ******  EDITION LISTE CONTROLE                                               
# IRD010   REPORT SYSOUT=(9,IRD010)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD010 ${DATA}/PXX0/F91.BIRD010D
       m_ProgramExec BRD010 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DAX PGM=IEBGENER   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DAX
       ;;
(GRD00DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A6} SYSUT1 ${DATA}/PXX0/F91.BIRD010D
       m_OutputAssign -c 9 -w IRD010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00DAY
       ;;
(GRD00DAY)
       m_CondExec 00,EQ,GRD00DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER STOCK (DD FGS41 DU PGM BRD005)                             
#   CODIC = 19,7                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBA
       ;;
(GRD00DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GRD00DAD.BRD005FD
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00DBA.BRD005GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 07
 /KEYS
   FLD_CH_19_7 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DBB
       ;;
(GRD00DBB)
       m_CondExec 00,EQ,GRD00DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * AJOUT PGM BRD040  ************************************************         
# ********************************************************************         
#  PGM : BRD040                                                                
# ********************************************************************         
#   CONSTITUTION DES FICHIERS EDDITION DE CONTROLE DES DECLARATION             
#   REPRISE: OUI SI ABEND                                                      
#            CE PGM NE FAIT AUCUNE MAJ SUR LES TABLES DB2                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBD
       ;;
(GRD00DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#         FICHIERS EN ENTREE                                                   
#                                                                              
# ****  FICHIER RESULTAT DES STOCKS TRIES                                      
       m_FileAssign -d SHR -g ${G_A8} FGS41 ${DATA}/PTEM/GRD00DBA.BRD005GD
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER MOIS                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******  CODIC SOUMIS A DECLARATION                                           
#    RSGA51M  : NAME=RSGA51D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA51M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01M  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10M  : NAME=RSGV10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10M /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00R  : NAME=RSRD00D,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00R /dev/null
#                                                                              
#         FICHIERS EN SORTIES                                                  
#                                                                              
# ******  FICHIER DES CODICS A DECLARER ET DECLARES : LRECL 63                 
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 FRD040 ${DATA}/PEX0/F91.BRD040AD.ETAT
# ******  FICHIER DES CODICS NON DECLARABLES:         LRECL 75                 
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 FRD050 ${DATA}/PEX0/F91.BRD050AD.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD040 
       JUMP_LABEL=GRD00DBE
       ;;
(GRD00DBE)
       m_CondExec 04,GE,GRD00DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD040 DU PGM BRD041            
#   POUR EDITION ETAT IRD040                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBG
       ;;
(GRD00DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PEX0/F91.BRD040AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00DBG.BRD040BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_27_6 27 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DBH
       ;;
(GRD00DBH)
       m_CondExec 00,EQ,GRD00DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD041 DU PGM BRD041            
#   POUR EDITION ETAT IRD041                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBJ
       ;;
(GRD00DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PEX0/F91.BRD040AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00DBJ.BRD040CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DBK
       ;;
(GRD00DBK)
       m_CondExec 00,EQ,GRD00DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD042 DU PGM BRD041            
#   POUR EDITION ETAT IRD042                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBM
       ;;
(GRD00DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PEX0/F91.BRD040AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00DBM.BRD040DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_PD_21_6 21 PD 6
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DBN
       ;;
(GRD00DBN)
       m_CondExec 00,EQ,GRD00DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD041                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD040 + IRD041 + IRD042                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBQ PGM=BRD041     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBQ
       ;;
(GRD00DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A12} FRD040 ${DATA}/PTEM/GRD00DBG.BRD040BD
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A13} FRD041 ${DATA}/PTEM/GRD00DBJ.BRD040CD
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A14} FRD042 ${DATA}/PTEM/GRD00DBM.BRD040DD
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD040 IRD040
# ******  EDITION LISTE CONTROLE                                               
# IRD041   REPORT SYSOUT=(9,IRD041),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD041 ${DATA}/PXX0/F91.BIRD041D
# ******  EDITION LISTE CONTROLE                                               
# IRD042   REPORT SYSOUT=(9,IRD042),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_OutputAssign -c "*" IRD042
       m_ProgramExec BRD041 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBT
       ;;
(GRD00DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A15} SYSUT1 ${DATA}/PXX0/F91.BIRD041D
       m_OutputAssign -c 9 -w IRD041 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00DBU
       ;;
(GRD00DBU)
       m_CondExec 00,EQ,GRD00DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD050 DU PGM BRD050            
#   POUR EDITION ETAT IRD050                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DBX
       ;;
(GRD00DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PEX0/F91.BRD050AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00DBX.BRD050BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_53_6 53 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DBY
       ;;
(GRD00DBY)
       m_CondExec 00,EQ,GRD00DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD051 DU PGM BRD050            
#   POUR EDITION ETAT IRD051                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DCA
       ;;
(GRD00DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PEX0/F91.BRD050AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00DCA.BRD050CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DCB
       ;;
(GRD00DCB)
       m_CondExec 00,EQ,GRD00DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD052 DU PGM BRD050            
#   POUR EDITION ETAT IRD052                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DCD
       ;;
(GRD00DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PEX0/F91.BRD050AD.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00DCD.BRD050DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00DCE
       ;;
(GRD00DCE)
       m_CondExec 00,EQ,GRD00DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD050                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD050 + IRD051 + IRD052                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCG PGM=BRD050     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DCG
       ;;
(GRD00DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A19} FRD050 ${DATA}/PTEM/GRD00DBX.BRD050BD
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A20} FRD051 ${DATA}/PTEM/GRD00DCA.BRD050CD
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A21} FRD052 ${DATA}/PTEM/GRD00DCD.BRD050DD
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD050 IRD050
# ******  EDITION LISTE CONTROLE                                               
# IRD051   REPORT SYSOUT=(9,IRD051),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD051 ${DATA}/PXX0/F91.BIRD051D
# ******  EDITION LISTE CONTROLE                                               
# IRD052   REPORT SYSOUT=(9,IRD052),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_OutputAssign -c "*" IRD052
       m_ProgramExec BRD050 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCJ PGM=IEBGENER   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DCJ
       ;;
(GRD00DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A22} SYSUT1 ${DATA}/PXX0/F91.BIRD051D
       m_OutputAssign -c 9 -w IRD051 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00DCK
       ;;
(GRD00DCK)
       m_CondExec 00,EQ,GRD00DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BUR0075                                                               
# ********************************************************************         
#   PURGE DE LA RTRD00 TOUS CE QUI EST INFERIEUR A 90 JOURS                    
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DCM
       ;;
(GRD00DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00D  : NAME=RSRD00D,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00D  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00D /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BUR0075 
       JUMP_LABEL=GRD00DCN
       ;;
(GRD00DCN)
       m_CondExec 04,GE,GRD00DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSRD00D                                       
# ********************************************************************         
#   REPRISE : OUI APRES UN TERM UTILITY                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00DCQ PGM=DSNUTILB   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GRD00DZA
       ;;
(GRD00DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRD00DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
