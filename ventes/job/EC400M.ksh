#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC400M.ksh                       --- VERSION DU 08/10/2016 14:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMEC400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/13 AT 15.31.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: EC400M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM = BEC400  -                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC400MA
       ;;
(EC400MA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=EC400MAA
       ;;
(EC400MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  PRED     LINK  NAME=$X010PZ,MODE=I                                          
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLES EN ECRITURE                                                   
#    RSEC02   : NAME=RSEC02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC02 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC400 
       JUMP_LABEL=EC400MAB
       ;;
(EC400MAB)
       m_CondExec 04,GE,EC400MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
