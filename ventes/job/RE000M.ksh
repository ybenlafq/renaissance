#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE000M.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMRE000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/11/25 AT 08.43.19 BY BURTECR                      
#    STANDARDS: P  JOBSET: RE000M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE000MA
       ;;
(RE000MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE000MAA
       ;;
(RE000MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGS105AM
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -g +1 SORTOUT ${DATA}/PTEM/RE000MAA.BRE200AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_9 81 CH 9
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING,
   FLD_CH_81_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MAB
       ;;
(RE000MAB)
       m_CondExec 00,EQ,RE000MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRE200   EXTRACTION DES VENTES DU FICHIER CAISSES 36 VENTE-EMPORTEE         
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAD PGM=BRE200     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAD
       ;;
(RE000MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  PARAMETRE PGM: SI 'N' TRAITEMENT NCG                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE000MAD
#                                                                              
# ******  FICHIER VENTES EMPORTEES TRI�                                        
       m_FileAssign -d SHR -g ${G_A1} FEMPORT ${DATA}/PTEM/RE000MAA.BRE200AM
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000MAD.BRE200BM
       m_ProgramExec BRE200 
# ********************************************************************         
#  BRE205   EXTRACTION DES VENTES DU FICHIER CAISSES AS400 NEM                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAG PGM=BRE205     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAG
       ;;
(RE000MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ******  FICHIER VENTES AS400 LG 107 ISSU DE NM009M                           
       m_FileAssign -d SHR -g +0 FTS057 ${DATA}/PXX0/F89.BNM157AM
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000MAG.BRE205AM
       m_ProgramExec BRE205 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAJ
       ;;
(RE000MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE000MAD.BRE200BM
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/RE000MAG.BRE205AM
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/RE000MAJ.BRE100AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_21_6 21 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_4 10 CH 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_4 ASCENDING,
   FLD_CH_21_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MAK
       ;;
(RE000MAK)
       m_CondExec 00,EQ,RE000MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE001 : PROG D EXTRACTION (DEPENDANCE AVEC BGV135=>DSTAT)         
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAM
       ;;
(RE000MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE FAMILLES                                                              
#    RTGA14   : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14 /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES VENTES                                                            
#    RTGV11   : NAME=RSGV11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES VENTES : ADRESSES                                                 
#    RTGV02   : NAME=RSGV02M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES GAMMES : PRIX EXCEPTIONNELS                                       
#    RTGG20   : NAME=RSGG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#                                                                              
#  ENTREE: FICHIER SOCIETE                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  ENTREE: TRI DU FICHIER PRECEDEND                                            
       m_FileAssign -d SHR -g ${G_A4} FRE000 ${DATA}/PTEM/RE000MAJ.BRE100AM
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/RE000MAM.BRE001AM
#  FICHIER EN SORTIE : FRE002  DE 512 DE LONG MENSUEL                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE002 ${DATA}/PTEM/RE000MAM.BRE002JM
#  FICHIER EN SORTIE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE003 ${DATA}/PTEM/RE000MAM.BRE003JM
#  FICHIER EN SORTIE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 RTRE00 ${DATA}/PTEM/RE000MAM.BRE010JM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE001 
       JUMP_LABEL=RE000MAN
       ;;
(RE000MAN)
       m_CondExec 04,GE,RE000MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE001AD)                                           
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAQ
       ;;
(RE000MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/RE000MAM.BRE001AM
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000MAQ.BRE001BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_13_5 13 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_BI_13_5 ASCENDING,
   FLD_PD_18_3 DESCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MAR
       ;;
(RE000MAR)
       m_CondExec 00,EQ,RE000MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRE001CD                                      
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAT
       ;;
(RE000MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/RE000MAQ.BRE001BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE000MAT.BRE001CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE000MAU
       ;;
(RE000MAU)
       m_CondExec 04,GE,RE000MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE000MAX
       ;;
(RE000MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE000MAT.BRE001CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000MAX.FRE001AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MAY
       ;;
(RE000MAY)
       m_CondExec 00,EQ,RE000MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRE001                                                 
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE000MBA
       ;;
(RE000MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/RE000MAQ.BRE001BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FCUMULS ${DATA}/PTEM/RE000MAX.FRE001AM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F89.BRE065AM
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE000MBB
       ;;
(RE000MBB)
       m_CondExec 04,GE,RE000MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL DU FRE002                                 
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE000MBD
       ;;
(RE000MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE002  DE 512 DE LONG (JOURNALIER)                     
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/RE000MAM.BRE002JM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BRE002MM.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F89.BRE002MM.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_BI_15_5 15 CH 5
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MBE
       ;;
(RE000MBE)
       m_CondExec 00,EQ,RE000MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL POUR HISTO VENDEUR (RTRE00)               
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE000MBG
       ;;
(RE000MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/RE000MAM.BRE010JM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BRE010MM.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 SORTOUT ${DATA}/PXX0/F89.BRE010MM.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_13_6 13 CH 6
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_7_6 7 CH 6
 /FIELDS FLD_BI_1_3 1 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_6 ASCENDING
 /* Record Type = F  Record Length = 048 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MBH
       ;;
(RE000MBH)
       m_CondExec 00,EQ,RE000MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL FICHIER JOURNALIER FRE003 SUR L HEBDO                                
#  REPRISE: NON BACKOUT                                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE000MBJ
       ;;
(RE000MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/RE000MAM.BRE003JM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BRE003HM.HEBDO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F89.BRE003HM.HEBDO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_15_1 15 CH 1
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_1 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000MBK
       ;;
(RE000MBK)
       m_CondExec 00,EQ,RE000MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DU FICHIER DE REPRISE CREE EN CAS DE PLANTAGE                        
#  REPRISE FORCEE AU DEBUT                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000MBM PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=RE000MBM
       ;;
(RE000MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -g +1 OUT1 ${DATA}/PXX0/F89.BNM157AM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000MBM.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RE000MBN
       ;;
(RE000MBN)
       m_CondExec 16,NE,RE000MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE000MZA
       ;;
(RE000MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
