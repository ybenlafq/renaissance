#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV610Y.ksh                       --- VERSION DU 09/10/2016 05:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGV610 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/05/07 AT 09.24.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV610Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV600 : EXTRACTION DES VENTES DE LA RTGV11 NON TOPEE LIVREES               
#           DONT LA DATE DE LIVRAISON EST INFERIEURE AU SAMEDI PRECEDE         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GV610YA
       ;;
(GV610YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2005/05/07 AT 09.24.25 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: GV610Y                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'EDIT� NON TOPLIVRE'                    
# *                           APPL...: REPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GV610YAA
       ;;
(GV610YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -d SHR -g +0 FDATE ${DATA}/PEX0/F07.FDATE
#                                                                              
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FGV600 ${DATA}/PTEM/GV610YAA.BGV600AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV600 
       JUMP_LABEL=GV610YAB
       ;;
(GV610YAB)
       m_CondExec 04,GE,GV610YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   EDITION DES VENTES NON TOPEE LIVREES DEPUIS LE SAMEDI PRECEDENT            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV610YAD PGM=BGV610     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV610YAD
       ;;
(GV610YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d SHR -g ${G_A1} FGV600 ${DATA}/PTEM/GV610YAA.BGV600AY
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -d SHR -g +0 FDATE ${DATA}/PEX0/F07.FDATE
#                                                                              
       m_OutputAssign -c 9 -w BGV610 FIMP
       m_ProgramExec BGV610 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV610YZA
       ;;
(GV610YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV610YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
