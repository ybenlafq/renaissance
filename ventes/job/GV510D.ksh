#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV510D.ksh                       --- VERSION DU 08/10/2016 13:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGV510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/17 AT 08.55.46 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV510D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV514  POSITIONNE UN FLAG SUR LES ENTETES DE VENTE NON NEM A EPURE         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV510DA
       ;;
(GV510DA)
#
#GV510DAD
#GV510DAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV510DAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV510DAA
       ;;
(GV510DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510DAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV514 
       JUMP_LABEL=GV510DAB
       ;;
(GV510DAB)
       m_CondExec 04,GE,GV510DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU  TABLESPACE : RSMQ15D                                            
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510DAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV510DAG
       ;;
(GV510DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510DAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GV510DAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15D  : NAME=RSMQ15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15D /dev/null
# ******  FICHIER LG 19 EPURATION DES RESERVATIONS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FTS053 ${DATA}/PXX0/F91.BNM153AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GV510DAH
       ;;
(GV510DAH)
       m_CondExec 04,GE,GV510DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV517  SUPPRESSION DANS LA TABLE RTGV10                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV510DAJ
       ;;
(GV510DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSNV20   : NAME=RSNV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV20 /dev/null
# ******  FICHIER MAGASIN NEM                                                  
       m_FileAssign -d SHR -g ${G_A1} FVTENP ${DATA}/PXX0/F91.BNM153AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV517 
       JUMP_LABEL=GV510DAK
       ;;
(GV510DAK)
       m_CondExec 04,GE,GV510DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV516                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV510DAM
       ;;
(GV510DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510DAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV516 
       JUMP_LABEL=GV510DAN
       ;;
(GV510DAN)
       m_CondExec 04,GE,GV510DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV515  DERESERVATION STOCKS ET QUOTAS + SUPPRESSION SUR TABLES             
#          MOUCHARD DES VENTES NON VALIDEES DONT LA DATE DE VENTE EST          
#          < OU = A LA DATE DU TRAITEMENT MOINS N JOURS                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV510DAQ
       ;;
(GV510DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE MOUCHARD DES RESERVATIONS STOCKS                               
#    RSGV40D  : NAME=RSGV40D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV40D /dev/null
# ******  TABLE MOUCHARD DES RESERVATIONS QUOTAS                               
#    RSGV41D  : NAME=RSGV41D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV41D /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  TABLE DES EN-TETES DE MUTATION                                       
#    RSGB05D  : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05D /dev/null
# ******  TABLE DES MUTATIONS DETAIL                                           
#    RSGB15D  : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15D /dev/null
# ******  TABLE DES LIGNES COMMANDES SPECIFIQUES                               
#    RSGF00D  : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00D /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (EMPORTE)                             
#    RSGQ02D  : NAME=RSGQ02D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ02D /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (LIVRE)                               
#    RSGQ03D  : NAME=RSGQ03D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ03D /dev/null
# ******  TABLE DES STOCKS ENTREPOT                                            
#    RSGS10D  : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10D /dev/null
# ******  TABLE DU JUSTIFICATIF DU LIEU DE TRANSIT                             
#    RSGS20D  : NAME=RSGS20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS20D /dev/null
# ******  TABLE DES STOCKS MAGASINS                                            
#    RSGS30D  : NAME=RSGS30D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30D /dev/null
# ******  TABLE DES STOCKS                                                     
#    RSGS43D  : NAME=RSGS43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43D /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS ENTREPOT                           
#    RSGV21D  : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21D /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS MAGASINS                           
#    RSGV22D  : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22D /dev/null
# ******  TABLE VENTES                                                         
#    RSGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******  TABLE DES LIAISONS ARTICLES                                          
#    RSGA58D  : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58D /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE FNSOC                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  DELAI DE PURGE                                                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510DAQ
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV515 
       JUMP_LABEL=GV510DAR
       ;;
(GV510DAR)
       m_CondExec 04,GE,GV510DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV518  SUPPRIME LES VENTES NON VALIDEES (NON ENCAISSEES)                   
#          DONT LA DATE DE VENTE EST                                           
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
#          EX : LE CLIENT N EST PAS PASSE EN CAISSE CAR TROP DE MONDE          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV510DAT
       ;;
(GV510DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02D  : NAME=RSGV02D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02D /dev/null
# ******  TABLE GV03                                                           
#    RSGV03D  : NAME=RSGV03D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03D /dev/null
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10D  : NAME=RSGV10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11D  : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
# ******  TABLE DES PRESTATIONS DE VENTES                                      
#    RSGV13D  : NAME=RSGV13D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13D /dev/null
# ******  TABLE DES CARACTERISTIQUES ARTICLES DES VENTES                       
#    RSGV15D  : NAME=RSGV15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15D /dev/null
# ******  TABLE VENTES                                                         
#    RSGV20D  : NAME=RSGV20D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV20D /dev/null
# ******  TABLE DES VENTES : AUTORISATIONS PREL STOCK PAR ENTREPOT             
#    RSGV27D  : NAME=RSGV27D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27D /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSGV23D  : NAME=RSGV23D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23D /dev/null
# ******  TABLE PSE                                                            
#    RSPS00D  : NAME=RSPS00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00D /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510DAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV518 
       JUMP_LABEL=GV510DAU
       ;;
(GV510DAU)
       m_CondExec 04,GE,GV510DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
