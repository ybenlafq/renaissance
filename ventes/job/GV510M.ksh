#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV510M.ksh                       --- VERSION DU 08/10/2016 12:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGV510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/17 AT 08.56.13 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV510M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV514  POSITIONNE UN FLAG SUR LES ENTETES DE VENTE NON NEM A EPURE         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV510MA
       ;;
(GV510MA)
#
#GV510MAD
#GV510MAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV510MAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV510MAA
       ;;
(GV510MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV10M  : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510MAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV514 
       JUMP_LABEL=GV510MAB
       ;;
(GV510MAB)
       m_CondExec 04,GE,GV510MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU  TABLESPACE : RSMQ15M                                            
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510MAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV510MAG
       ;;
(GV510MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510MAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GV510MAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15M  : NAME=RSMQ15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15M /dev/null
# ******  FICHIER LG 19 EPURATION DES RESERVATIONS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FTS053 ${DATA}/PXX0/F89.BNM153AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GV510MAH
       ;;
(GV510MAH)
       m_CondExec 04,GE,GV510MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV517  SUPPRESSION DANS LA TABLE RTGV10                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV510MAJ
       ;;
(GV510MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10M  : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSNV20   : NAME=RSNV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV20 /dev/null
# ******  FICHIER MAGASIN NEM                                                  
       m_FileAssign -d SHR -g ${G_A1} FVTENP ${DATA}/PXX0/F89.BNM153AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV517 
       JUMP_LABEL=GV510MAK
       ;;
(GV510MAK)
       m_CondExec 04,GE,GV510MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV516   ENVOI MESSAGE MQSERIES DANS QUEUE LOCALE HOST                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV510MAM
       ;;
(GV510MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10M  : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510MAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV516 
       JUMP_LABEL=GV510MAN
       ;;
(GV510MAN)
       m_CondExec 04,GE,GV510MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV515  DERESERVATION STOCKS ET QUOTAS + SUPPRESSION SUR TABLES             
#          MOUCHARD DES VENTES NON VALIDEES DONT LA DATE DE VENTE EST          
#          < OU = A LA DATE DU TRAITEMENT MOINS N JOURS                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV510MAQ
       ;;
(GV510MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE MOUCHARD DES RESERVATIONS STOCKS                               
#    RSGV40M  : NAME=RSGV40M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV40M /dev/null
# ******  TABLE MOUCHARD DES RESERVATIONS QUOTAS                               
#    RSGV41M  : NAME=RSGV41M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV41M /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ******  TABLE DES EN-TETES DE MUTATION                                       
#    RSGB05M  : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05M /dev/null
# ******  TABLE DES MUTATIONS DETAIL                                           
#    RSGB15M  : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15M /dev/null
# ******  TABLE DES LIGNES COMMANDES SPECIFIQUES                               
#    RSGF00M  : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00M /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (EMPORTE)                             
#    RSGQ02M  : NAME=RSGQ02M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ02M /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (LIVRE)                               
#    RSGQ03M  : NAME=RSGQ03M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ03M /dev/null
# ******  TABLE DES STOCKS ENTREPOT                                            
#    RSGS10M  : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10M /dev/null
# ******  TABLE DU JUSTIFICATIF DU LIEU DE TRANSIT                             
#    RSGS20M  : NAME=RSGS20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS20M /dev/null
# ******  TABLE DES STOCKS MAGASINS                                            
#    RSGS30M  : NAME=RSGS30M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30M /dev/null
# ******  TABLE DES                                                            
#    RSGS43M  : NAME=RSGS43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43M /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS ENTREPOT                           
#    RSGV21M  : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21M /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS MAGASINS                           
#    RSGV22M  : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22M /dev/null
# ******  TABLE VENTES                                                         
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  TABLE DES LIAISONS ARTICLES                                          
#    RSGA58M  : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58M /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE FNSOC                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  DELAI DE PURGE                                                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510MAQ
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV515 
       JUMP_LABEL=GV510MAR
       ;;
(GV510MAR)
       m_CondExec 04,GE,GV510MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV518  SUPPRIME LES VENTES NON VALIDEES (NON ENCAISSEES)                   
#          DONT LA DATE DE VENTE EST                                           
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
#          EX : LE CLIENT N EST PAS PASSE EN CAISSE CAR TROP DE MONDE          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV510MAT
       ;;
(GV510MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02M  : NAME=RSGV02M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02M /dev/null
# ******  TABLE GV03                                                           
#    RSGV03M  : NAME=RSGV03M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03M /dev/null
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10M  : NAME=RSGV10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11M  : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
# ******  TABLE DES PRESTATIONS DE VENTES                                      
#    RSGV13M  : NAME=RSGV13M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13M /dev/null
# ******  TABLE DES CARACTERISTIQUES ARTICLES DES VENTES                       
#    RSGV15M  : NAME=RSGV15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15M /dev/null
# ******  TABLE DES VENTES                                                     
#    RSGV20M  : NAME=RSGV20M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV20M /dev/null
# ******  TABLE DES VENTES                                                     
#    RSGV23M  : NAME=RSGV23M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23M /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSGV27M  : NAME=RSGV27M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27M /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSPS00M  : NAME=RSPS00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00M /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 989                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510MAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV518 
       JUMP_LABEL=GV510MAU
       ;;
(GV510MAU)
       m_CondExec 04,GE,GV510MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
