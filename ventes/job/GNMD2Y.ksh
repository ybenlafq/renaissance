#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD2Y.ksh                       --- VERSION DU 08/10/2016 22:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGNMD2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/21 AT 14.38.02 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD2Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF870 : MISE � JOUR DE CERTAINES SOUS TABLES DE LA RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD2YA
       ;;
(GNMD2YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GNMD2YAA
       ;;
(GNMD2YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF870 
       JUMP_LABEL=GNMD2YAB
       ;;
(GNMD2YAB)
       m_CondExec 04,GE,GNMD2YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF872 :  MAJ FLAG RTLI00 DES LIEUX MIGRES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAD
       ;;
(GNMD2YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF872 
       JUMP_LABEL=GNMD2YAE
       ;;
(GNMD2YAE)
       m_CondExec 04,GE,GNMD2YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF500 : EXTRACTION DE LA TABLE DES DEVISE RTFM04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAG
       ;;
(GNMD2YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE DES DEVISE                                                     
#    RSFM04Y  : NAME=RSFM04Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF500 ${DATA}/PTEM/GNMD2YAG.BTF500AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF500 
       JUMP_LABEL=GNMD2YAH
       ;;
(GNMD2YAH)
       m_CondExec 04,GE,GNMD2YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF505 : EXTRACTION DE LA TABLE RTFM05                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAJ
       ;;
(GNMD2YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSFM05   : NAME=RSFM05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF505 ${DATA}/PTEM/GNMD2YAJ.BTF505AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF505 
       JUMP_LABEL=GNMD2YAK
       ;;
(GNMD2YAK)
       m_CondExec 04,GE,GNMD2YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF598 : EXTRACTION DE LA TABLE RTGA00                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAM
       ;;
(GNMD2YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIS�E                                                    
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALIS�E                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA30   : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA58   : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA67   : NAME=RSGA67Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE ANOMALIES                                                      
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF598 ${DATA}/PTEM/GNMD2YAM.BTF598AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF598 
       JUMP_LABEL=GNMD2YAN
       ;;
(GNMD2YAN)
       m_CondExec 04,GE,GNMD2YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF606 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAQ
       ;;
(GNMD2YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA06   : NAME=RSGA06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF606 ${DATA}/PTEM/GNMD2YAQ.BTF606AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF606 
       JUMP_LABEL=GNMD2YAR
       ;;
(GNMD2YAR)
       m_CondExec 04,GE,GNMD2YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF611 : CREATION DU FICHIER DE COMPARAISON POUR LES TABLES                 
#                    RTGA09, RTGA11, RTGA12 ET RTGA29                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAT
       ;;
(GNMD2YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA29   : NAME=RSGA29Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS D'EXTRACTION DES ARTICLES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611A ${DATA}/PTEM/GNMD2YAT.BTF611AY
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611B ${DATA}/PTEM/GNMD2YAT.BTF611BY
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611C ${DATA}/PTEM/GNMD2YAT.BTF611CY
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611D ${DATA}/PTEM/GNMD2YAT.BTF611DY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF611 
       JUMP_LABEL=GNMD2YAU
       ;;
(GNMD2YAU)
       m_CondExec 04,GE,GNMD2YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF616 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YAX
       ;;
(GNMD2YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF616 ${DATA}/PTEM/GNMD2YAX.BTF616AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF616 
       JUMP_LABEL=GNMD2YAY
       ;;
(GNMD2YAY)
       m_CondExec 04,GE,GNMD2YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF620 : EXTRACTION DE LA SOUS TABLE NMDAT                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBA
       ;;
(GNMD2YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIE                                                      
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF620 ${DATA}/PTEM/GNMD2YBA.BTF620AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF620 
       JUMP_LABEL=GNMD2YBB
       ;;
(GNMD2YBB)
       m_CondExec 04,GE,GNMD2YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF625 : CREATION DU FICHIER DE POUR LA TABLE RTGA30                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBD
       ;;
(GNMD2YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF625 ${DATA}/PTEM/GNMD2YBD.BTF625AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF625 
       JUMP_LABEL=GNMD2YBE
       ;;
(GNMD2YBE)
       m_CondExec 04,GE,GNMD2YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF631 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA14            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBG
       ;;
(GNMD2YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF631 ${DATA}/PTEM/GNMD2YBG.BTF631AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF631 
       JUMP_LABEL=GNMD2YBH
       ;;
(GNMD2YBH)
       m_CondExec 04,GE,GNMD2YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF636 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBJ
       ;;
(GNMD2YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA20   : NAME=RSGA20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF636 ${DATA}/PTEM/GNMD2YBJ.BTF636AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF636 
       JUMP_LABEL=GNMD2YBK
       ;;
(GNMD2YBK)
       m_CondExec 04,GE,GNMD2YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF641 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBM
       ;;
(GNMD2YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA21   : NAME=RSGA21Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF641 ${DATA}/PTEM/GNMD2YBM.BTF641AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF641 
       JUMP_LABEL=GNMD2YBN
       ;;
(GNMD2YBN)
       m_CondExec 04,GE,GNMD2YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF646 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA22            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBQ
       ;;
(GNMD2YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA22   : NAME=RSGA22Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF646 ${DATA}/PTEM/GNMD2YBQ.BTF646AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF646 
       JUMP_LABEL=GNMD2YBR
       ;;
(GNMD2YBR)
       m_CondExec 04,GE,GNMD2YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF651 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA23            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBT
       ;;
(GNMD2YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA23   : NAME=RSGA23Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA23 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF651 ${DATA}/PTEM/GNMD2YBT.BTF651AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF651 
       JUMP_LABEL=GNMD2YBU
       ;;
(GNMD2YBU)
       m_CondExec 04,GE,GNMD2YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF656 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA24            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YBX
       ;;
(GNMD2YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA24   : NAME=RSGA24Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA24 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF656 ${DATA}/PTEM/GNMD2YBX.BTF656AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF656 
       JUMP_LABEL=GNMD2YBY
       ;;
(GNMD2YBY)
       m_CondExec 04,GE,GNMD2YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF662 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCA
       ;;
(GNMD2YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF662 ${DATA}/PTEM/GNMD2YCA.BTF662AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF662 
       JUMP_LABEL=GNMD2YCB
       ;;
(GNMD2YCB)
       m_CondExec 04,GE,GNMD2YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF666 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCD
       ;;
(GNMD2YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA26   : NAME=RSGA26Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF666 ${DATA}/PTEM/GNMD2YCD.BTF666AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF666 
       JUMP_LABEL=GNMD2YCE
       ;;
(GNMD2YCE)
       m_CondExec 04,GE,GNMD2YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF671 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA27            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCG
       ;;
(GNMD2YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA27   : NAME=RSGA27Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA27 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF671 ${DATA}/PTEM/GNMD2YCG.BTF671AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF671 
       JUMP_LABEL=GNMD2YCH
       ;;
(GNMD2YCH)
       m_CondExec 04,GE,GNMD2YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF682 : EXTRACTION DE LA TABLE DES GARANTIES RTGA40                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCJ
       ;;
(GNMD2YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLE                                                        
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******* TABLE GENERALISE                                                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* TABLE                                                                
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******* TABLE                                                                
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30Y /dev/null
# ******* TABLE CODICS LIE                                                     
#    RSGA58Y  : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58Y /dev/null
# ******* TABLE                                                                
#    RSGA67Y  : NAME=RSGA67Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67Y /dev/null
# ******* TABLE MONTANTS DES GARANTIES COMPLEMENTAIRES                         
#    RSGA40Y  : NAME=RSGA40Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA40Y /dev/null
# ******* TABLE ANO                                                            
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF682 ${DATA}/PTEM/GNMD2YCJ.BTF682AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF682 
       JUMP_LABEL=GNMD2YCK
       ;;
(GNMD2YCK)
       m_CondExec 04,GE,GNMD2YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF686 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCM
       ;;
(GNMD2YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA41   : NAME=RSGA41Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF686 ${DATA}/PTEM/GNMD2YCM.BTF686AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF686 
       JUMP_LABEL=GNMD2YCN
       ;;
(GNMD2YCN)
       m_CondExec 04,GE,GNMD2YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF687 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA51            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCQ
       ;;
(GNMD2YCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA51   : NAME=RSGA51Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF687 ${DATA}/PTEM/GNMD2YCQ.BTF687AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF687 
       JUMP_LABEL=GNMD2YCR
       ;;
(GNMD2YCR)
       m_CondExec 04,GE,GNMD2YCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF692 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA52            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCT
       ;;
(GNMD2YCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA52   : NAME=RSGA52Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF692 ${DATA}/PTEM/GNMD2YCT.BTF692AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF692 
       JUMP_LABEL=GNMD2YCU
       ;;
(GNMD2YCU)
       m_CondExec 04,GE,GNMD2YCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF702 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA58            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YCX
       ;;
(GNMD2YCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES CODICS GROUPES                                             
#    RSGA58   : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF702 ${DATA}/PTEM/GNMD2YCX.BTF702AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF702 
       JUMP_LABEL=GNMD2YCY
       ;;
(GNMD2YCY)
       m_CondExec 04,GE,GNMD2YCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF707 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA64            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDA
       ;;
(GNMD2YDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA64   : NAME=RSGA64Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF707 ${DATA}/PTEM/GNMD2YDA.BTF707AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF707 
       JUMP_LABEL=GNMD2YDB
       ;;
(GNMD2YDB)
       m_CondExec 04,GE,GNMD2YDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF710 : EXTRACTION DE LA TABLE GQ12                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDD
       ;;
(GNMD2YDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE MODE DE DELIVRANCE                                             
#    RSGQ12   : NAME=RSGQ12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF710 ${DATA}/PTEM/GNMD2YDD.BTF710AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF710 
       JUMP_LABEL=GNMD2YDE
       ;;
(GNMD2YDE)
       m_CondExec 04,GE,GNMD2YDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF715 : EXTRACTION TABLE DES CODES VENDEURS RTGV31                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDG
       ;;
(GNMD2YDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE CODES VENDEURS                                                 
#    RSGV31   : NAME=RSGV31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF715 ${DATA}/PTEM/GNMD2YDG.BTF715AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF715 
       JUMP_LABEL=GNMD2YDH
       ;;
(GNMD2YDH)
       m_CondExec 04,GE,GNMD2YDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF718 : EXTRACTION TABLE COMPTEUR SAV RTPA70                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDJ
       ;;
(GNMD2YDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPA70   : NAME=RSPA70Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPA70 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF718 ${DATA}/PTEM/GNMD2YDJ.BTF718AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF718 
       JUMP_LABEL=GNMD2YDK
       ;;
(GNMD2YDK)
       m_CondExec 04,GE,GNMD2YDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF720 : EXTRACTION TABLE MODE PMT / RBMT RTPM06                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDM
       ;;
(GNMD2YDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPM06   : NAME=RSPM06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF720 ${DATA}/PTEM/GNMD2YDM.BTF720AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF720 
       JUMP_LABEL=GNMD2YDN
       ;;
(GNMD2YDN)
       m_CondExec 04,GE,GNMD2YDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF725 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPM35            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDQ
       ;;
(GNMD2YDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PARAMETRES MQSERIES                                            
#    RSPM35   : NAME=RSPM35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM35 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF725 ${DATA}/PTEM/GNMD2YDQ.BTF725AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF725 
       JUMP_LABEL=GNMD2YDR
       ;;
(GNMD2YDR)
       m_CondExec 04,GE,GNMD2YDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF751 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDT
       ;;
(GNMD2YDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PRESTATION                                                     
#    RSPR00Y  : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00Y /dev/null
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF751 ${DATA}/PTEM/GNMD2YDT.BTF751AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF751 
       JUMP_LABEL=GNMD2YDU
       ;;
(GNMD2YDU)
       m_CondExec 04,GE,GNMD2YDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF755 : EXTRACTION TABLE LIEN FAMILLE/CODE PRESTATION RTPR01               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YDX
       ;;
(GNMD2YDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN FAMILLE / CODE PRESTATION                                 
#    RSPR01   : NAME=RSPR01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF755 ${DATA}/PTEM/GNMD2YDX.BTF755AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF755 
       JUMP_LABEL=GNMD2YDY
       ;;
(GNMD2YDY)
       m_CondExec 04,GE,GNMD2YDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF761 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEA
       ;;
(GNMD2YEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
#    RSPR02Y  : NAME=RSPR02Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR02Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF761 ${DATA}/PTEM/GNMD2YEA.BTF761AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF761 
       JUMP_LABEL=GNMD2YEB
       ;;
(GNMD2YEB)
       m_CondExec 04,GE,GNMD2YEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF766 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR03  *         
#  REPRISE: OUI  *                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YED
       ;;
(GNMD2YED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
#    RSPR03Y  : NAME=RSPR03Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR03Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF766 ${DATA}/PTEM/GNMD2YED.BTF766AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF766 
       JUMP_LABEL=GNMD2YEE
       ;;
(GNMD2YEE)
       m_CondExec 04,GE,GNMD2YED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF770 : EXTRACTION TABLE LIEN PRESTATIONS RTPR04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEG
       ;;
(GNMD2YEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN PRESTATION                                                
#    RSPR04   : NAME=RSPR04Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR04 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF770 ${DATA}/PTEM/GNMD2YEG.BTF770AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF770 
       JUMP_LABEL=GNMD2YEH
       ;;
(GNMD2YEH)
       m_CondExec 04,GE,GNMD2YEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF777 : EXTRACT TABLE PRIX PRESTATION LIE A LA ZONE DE PRIX RTPR10         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEJ
       ;;
(GNMD2YEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06Y  : NAME=RSPR06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06Y /dev/null
# ******* TABLE PRIX PRESTATION LIE A ZONE DE PRIX                             
#    RSPR10Y  : NAME=RSPR10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR10Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF777 ${DATA}/PTEM/GNMD2YEJ.BTF777AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF777 
       JUMP_LABEL=GNMD2YEK
       ;;
(GNMD2YEK)
       m_CondExec 04,GE,GNMD2YEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF782 : EXTRACTION TABLE PRIX PRESTATION LIE AU CODIC RTPR14               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEM
       ;;
(GNMD2YEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06Y  : NAME=RSPR06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06Y /dev/null
# ******* TABLE PRIX PRESTATION LIE A ARTICLE                                  
#    RSPR14Y  : NAME=RSPR14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR14Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF782 ${DATA}/PTEM/GNMD2YEM.BTF782AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF782 
       JUMP_LABEL=GNMD2YEN
       ;;
(GNMD2YEN)
       m_CondExec 04,GE,GNMD2YEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF783 : EXTRACTION DE LA TABLE RTPR53                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEQ
       ;;
(GNMD2YEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR53 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF783 ${DATA}/PTEM/GNMD2YEQ.BTF783AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF783 
       JUMP_LABEL=GNMD2YER
       ;;
(GNMD2YER)
       m_CondExec 04,GE,GNMD2YEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF784 : EXTRACTION DE LA TABLE RTGA31                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YET
       ;;
(GNMD2YET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA31   : NAME=RSGA31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF784 ${DATA}/PTEM/GNMD2YET.BTF784AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF784 
       JUMP_LABEL=GNMD2YEU
       ;;
(GNMD2YEU)
       m_CondExec 04,GE,GNMD2YET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFT785 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YEX
       ;;
(GNMD2YEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF785 ${DATA}/PTEM/GNMD2YEX.BTF785AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF785 
       JUMP_LABEL=GNMD2YEY
       ;;
(GNMD2YEY)
       m_CondExec 04,GE,GNMD2YEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF786 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFA
       ;;
(GNMD2YFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF786 ${DATA}/PTEM/GNMD2YFA.BTF786AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF786 
       JUMP_LABEL=GNMD2YFB
       ;;
(GNMD2YFB)
       m_CondExec 04,GE,GNMD2YFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF787 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFD
       ;;
(GNMD2YFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF787 ${DATA}/PTEM/GNMD2YFD.BTF787AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF787 
       JUMP_LABEL=GNMD2YFE
       ;;
(GNMD2YFE)
       m_CondExec 04,GE,GNMD2YFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF788 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFG PGM=IKJEFT01   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFG
       ;;
(GNMD2YFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF788 ${DATA}/PTEM/GNMD2YFG.BTF788AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF788 
       JUMP_LABEL=GNMD2YFH
       ;;
(GNMD2YFH)
       m_CondExec 04,GE,GNMD2YFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF794 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFJ
       ;;
(GNMD2YFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF794 ${DATA}/PTEM/GNMD2YFJ.BTF794AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF794 
       JUMP_LABEL=GNMD2YFK
       ;;
(GNMD2YFK)
       m_CondExec 04,GE,GNMD2YFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF795 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFM
       ;;
(GNMD2YFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF795 ${DATA}/PTEM/GNMD2YFM.BTF795AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF795 
       JUMP_LABEL=GNMD2YFN
       ;;
(GNMD2YFN)
       m_CondExec 04,GE,GNMD2YFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF796 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFQ
       ;;
(GNMD2YFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF796 ${DATA}/PTEM/GNMD2YFQ.BTF796AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF796 
       JUMP_LABEL=GNMD2YFR
       ;;
(GNMD2YFR)
       m_CondExec 04,GE,GNMD2YFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF797 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFT
       ;;
(GNMD2YFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF797 ${DATA}/PTEM/GNMD2YFT.BTF797AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF797 
       JUMP_LABEL=GNMD2YFU
       ;;
(GNMD2YFU)
       m_CondExec 04,GE,GNMD2YFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF798 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YFX
       ;;
(GNMD2YFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS25   : NAME=RSBS25P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS25 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF798 ${DATA}/PTEM/GNMD2YFX.BTF798AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF798 
       JUMP_LABEL=GNMD2YFY
       ;;
(GNMD2YFY)
       m_CondExec 04,GE,GNMD2YFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF799 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGA PGM=IKJEFT01   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGA
       ;;
(GNMD2YGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS26   : NAME=RSBS26P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS26 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF799 ${DATA}/PTEM/GNMD2YGA.BTF799AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF799 
       JUMP_LABEL=GNMD2YGB
       ;;
(GNMD2YGB)
       m_CondExec 04,GE,GNMD2YGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF802 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTIP02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGD
       ;;
(GNMD2YGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FPV212 ${DATA}/PXX0/F45.BPV212AY
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF802 ${DATA}/PXX0/F45.BTF802AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF802 
       JUMP_LABEL=GNMD2YGE
       ;;
(GNMD2YGE)
       m_CondExec 04,GE,GNMD2YGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF805 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA91            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGG
       ;;
(GNMD2YGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE DES                                                            
#    RSGA91Y  : NAME=RSGA91Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA91Y /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF805 ${DATA}/PTEM/GNMD2YGG.BTF805AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF805 
       JUMP_LABEL=GNMD2YGH
       ;;
(GNMD2YGH)
       m_CondExec 04,GE,GNMD2YGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF810 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA92            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGJ PGM=IKJEFT01   ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGJ
       ;;
(GNMD2YGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE                                                                
#    RSGA92Y  : NAME=RSGA92Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA92Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF810 ${DATA}/PTEM/GNMD2YGJ.BTF810AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF810 
       JUMP_LABEL=GNMD2YGK
       ;;
(GNMD2YGK)
       m_CondExec 04,GE,GNMD2YGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF815 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA93            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGM PGM=IKJEFT01   ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGM
       ;;
(GNMD2YGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE                                                                
#    RSGA93Y  : NAME=RSGA93Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA93Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF815 ${DATA}/PTEM/GNMD2YGM.BTF815AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF815 
       JUMP_LABEL=GNMD2YGN
       ;;
(GNMD2YGN)
       m_CondExec 04,GE,GNMD2YGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF820 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA12            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGQ
       ;;
(GNMD2YGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR12   : NAME=RSPR12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF820 ${DATA}/PTEM/GNMD2YGQ.BTF820AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF820 
       JUMP_LABEL=GNMD2YGR
       ;;
(GNMD2YGR)
       m_CondExec 04,GE,GNMD2YGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF825 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGT PGM=IKJEFT01   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGT
       ;;
(GNMD2YGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******* TABLE                                                                
#    RSPR16Y  : NAME=RSPR16Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR16Y /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF825 ${DATA}/PTEM/GNMD2YGT.BTF825AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF825 
       JUMP_LABEL=GNMD2YGU
       ;;
(GNMD2YGU)
       m_CondExec 04,GE,GNMD2YGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF835 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA56            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YGX
       ;;
(GNMD2YGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA56 /dev/null
# ******* TABLE                                                                
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA65   : NAME=RSGA65Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* TABLE                                                                
#    RSGA66   : NAME=RSGA66Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
# ******* TABLE                                                                
#    RSGA68   : NAME=RSGA68Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
# ******* TABLE                                                                
#    RSGA75   : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* TABLE                                                                
#    RSGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF835 ${DATA}/PTEM/GNMD2YGX.BTF835AY
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2YGX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF835 
       JUMP_LABEL=GNMD2YGY
       ;;
(GNMD2YGY)
       m_CondExec 04,GE,GNMD2YGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF836 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHA PGM=IKJEFT01   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHA
       ;;
(GNMD2YHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE                                                                
#    RSGQ01   : NAME=RSGQ01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF836 ${DATA}/PTEM/GNMD2YHA.BTF836AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF836 
       JUMP_LABEL=GNMD2YHB
       ;;
(GNMD2YHB)
       m_CondExec 04,GE,GNMD2YHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF837 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHD PGM=IKJEFT01   ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHD
       ;;
(GNMD2YHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGQ06   : NAME=RSGQ06Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF837 ${DATA}/PTEM/GNMD2YHD.BTF837AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF837 
       JUMP_LABEL=GNMD2YHE
       ;;
(GNMD2YHE)
       m_CondExec 04,GE,GNMD2YHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF838 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHG PGM=IKJEFT01   ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHG
       ;;
(GNMD2YHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE STOCK LOCAUX                                                   
#    RSSL01   : NAME=RSSL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF838 ${DATA}/PTEM/GNMD2YHG.BTF838AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF838 
       JUMP_LABEL=GNMD2YHH
       ;;
(GNMD2YHH)
       m_CondExec 04,GE,GNMD2YHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF839 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHJ PGM=IKJEFT01   ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHJ
       ;;
(GNMD2YHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSSL16   : NAME=RSSL16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF839 ${DATA}/PTEM/GNMD2YHJ.BTF839AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF839 
       JUMP_LABEL=GNMD2YHK
       ;;
(GNMD2YHK)
       m_CondExec 04,GE,GNMD2YHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF840 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHM PGM=IKJEFT01   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHM
       ;;
(GNMD2YHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGG40   : NAME=RSGG40Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF840 ${DATA}/PTEM/GNMD2YHM.BTF840AY
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 HAD840 ${DATA}/PXX0/F45.BTF840BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF840 
       JUMP_LABEL=GNMD2YHN
       ;;
(GNMD2YHN)
       m_CondExec 04,GE,GNMD2YHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BTF841 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTEE03           
# * REPRISE: OUI                                                               
# ********************************************************************         
# ALT      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** TABLE CODES POSTAUX                                                 
# RSEE03   FILE  DYNAM=YES,NAME=RSEE03,MODE=I                                  
# ******** PARAMETRE SOCIETE                                                   
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDRA                                         
# ******** PARAMETRE FDATE                                                     
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** FICHIER D'EXTRACTION                                                
# FTF841   FILE  NAME=BTF841AY,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BTF841) PLAN(BTF841Y)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BTF842 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGB05            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHQ PGM=IKJEFT01   ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHQ
       ;;
(GNMD2YHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF842 ${DATA}/PTEM/GNMD2YHQ.BTF842AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF842 
       JUMP_LABEL=GNMD2YHR
       ;;
(GNMD2YHR)
       m_CondExec 04,GE,GNMD2YHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF843 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA54            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHT PGM=IKJEFT01   ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHT
       ;;
(GNMD2YHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF843 ${DATA}/PTEM/GNMD2YHT.BTF843AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF843 
       JUMP_LABEL=GNMD2YHU
       ;;
(GNMD2YHU)
       m_CondExec 04,GE,GNMD2YHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF844 : CREATION DU FICHIER DE COMPARAISON POUR GV21/22/35/11 TL02         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YHX PGM=IKJEFT01   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YHX
       ;;
(GNMD2YHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#    RSGV35   : NAME=RSGV35Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF844 ${DATA}/PTEM/GNMD2YHX.BTF844AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF844 
       JUMP_LABEL=GNMD2YHY
       ;;
(GNMD2YHY)
       m_CondExec 04,GE,GNMD2YHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF845 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA53            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIA PGM=IKJEFT01   ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIA
       ;;
(GNMD2YIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA53   : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF845 ${DATA}/PTEM/GNMD2YIA.BTF845AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF845 
       JUMP_LABEL=GNMD2YIB
       ;;
(GNMD2YIB)
       m_CondExec 04,GE,GNMD2YIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF846 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YID PGM=IKJEFT01   ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YID
       ;;
(GNMD2YID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL40   : NAME=RSSL40Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF846 ${DATA}/PTEM/GNMD2YID.BTF846AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF846 
       JUMP_LABEL=GNMD2YIE
       ;;
(GNMD2YIE)
       m_CondExec 04,GE,GNMD2YID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF847 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIG PGM=IKJEFT01   ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIG
       ;;
(GNMD2YIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL41   : NAME=RSSL41Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL41 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF847 ${DATA}/PTEM/GNMD2YIG.BTF847AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF847 
       JUMP_LABEL=GNMD2YIH
       ;;
(GNMD2YIH)
       m_CondExec 04,GE,GNMD2YIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF848 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIJ PGM=IKJEFT01   ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIJ
       ;;
(GNMD2YIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE STOCK LOCAL - MODE I (POUR EVITER SERIALIS.DES FILIALE         
#    RSSL10   : NAME=RSSL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF848 ${DATA}/PTEM/GNMD2YIJ.BTF848AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF848 
       JUMP_LABEL=GNMD2YIK
       ;;
(GNMD2YIK)
       m_CondExec 04,GE,GNMD2YIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF849 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTYF00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIM PGM=IKJEFT01   ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIM
       ;;
(GNMD2YIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE PARIS                                                          
#    RSYF00   : NAME=RSYF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSYF00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES CODES VENDEURS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF849 ${DATA}/PTEM/GNMD2YIM.BTF849AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF849 
       JUMP_LABEL=GNMD2YIN
       ;;
(GNMD2YIN)
       m_CondExec 04,GE,GNMD2YIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF860 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLI00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIQ PGM=IKJEFT01   ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIQ
       ;;
(GNMD2YIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF860 ${DATA}/PTEM/GNMD2YIQ.BTF860AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF860 
       JUMP_LABEL=GNMD2YIR
       ;;
(GNMD2YIR)
       m_CondExec 04,GE,GNMD2YIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF862 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLG09            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIT PGM=IKJEFT01   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIT
       ;;
(GNMD2YIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLG09   : NAME=RSLG09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLG09 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF862 ${DATA}/PTEM/GNMD2YIT.BTF862AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF862 
       JUMP_LABEL=GNMD2YIU
       ;;
(GNMD2YIU)
       m_CondExec 04,GE,GNMD2YIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF863 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA59            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YIX PGM=IKJEFT01   ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YIX
       ;;
(GNMD2YIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF863 ${DATA}/PTEM/GNMD2YIX.BTF863AY
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2YIX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF863 
       JUMP_LABEL=GNMD2YIY
       ;;
(GNMD2YIY)
       m_CondExec 04,GE,GNMD2YIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF864 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJA PGM=IKJEFT01   ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJA
       ;;
(GNMD2YJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGG20   : NAME=RSGG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF864 ${DATA}/PTEM/GNMD2YJA.BTF864AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF864 
       JUMP_LABEL=GNMD2YJB
       ;;
(GNMD2YJB)
       m_CondExec 04,GE,GNMD2YJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF865 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJD PGM=IKJEFT01   ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJD
       ;;
(GNMD2YJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE POUR LA CONTRIBUTION RECYCLAGE D3E                             
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF865 ${DATA}/PTEM/GNMD2YJD.BTF865AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF865 
       JUMP_LABEL=GNMD2YJE
       ;;
(GNMD2YJE)
       m_CondExec 04,GE,GNMD2YJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF875 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA82            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJG PGM=IKJEFT01   ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJG
       ;;
(GNMD2YJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA82   : NAME=RSGA82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA82 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF875 ${DATA}/PTEM/GNMD2YJG.BTF875AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF875 
       JUMP_LABEL=GNMD2YJH
       ;;
(GNMD2YJH)
       m_CondExec 04,GE,GNMD2YJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF877 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJJ PGM=IKJEFT01   ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJJ
       ;;
(GNMD2YJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877A ${DATA}/PTEM/GNMD2YJJ.BTF877AY
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877B ${DATA}/PTEM/GNMD2YJJ.BTF877BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF877 
       JUMP_LABEL=GNMD2YJK
       ;;
(GNMD2YJK)
       m_CondExec 04,GE,GNMD2YJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF878 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL17            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJM PGM=IKJEFT01   ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJM
       ;;
(GNMD2YJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSSL17   : NAME=RSSL17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL17 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF878 ${DATA}/PTEM/GNMD2YJM.BTF878AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF878 
       JUMP_LABEL=GNMD2YJN
       ;;
(GNMD2YJN)
       m_CondExec 04,GE,GNMD2YJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AS400 LOCAUX                                                                
# ********************************************************************         
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJQ
       ;;
(GNMD2YJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GNMD2YAG.BTF500AY
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GNMD2YAJ.BTF505AY
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GNMD2YAM.BTF598AY
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GNMD2YAQ.BTF606AY
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GNMD2YAT.BTF611AY
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GNMD2YAT.BTF611BY
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GNMD2YAT.BTF611CY
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GNMD2YAT.BTF611DY
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GNMD2YAX.BTF616AY
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GNMD2YBA.BTF620AY
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GNMD2YBD.BTF625AY
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GNMD2YBG.BTF631AY
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GNMD2YBJ.BTF636AY
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GNMD2YBM.BTF641AY
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GNMD2YBQ.BTF646AY
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/GNMD2YBT.BTF651AY
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GNMD2YBX.BTF656AY
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GNMD2YCA.BTF662AY
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GNMD2YCD.BTF666AY
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GNMD2YCG.BTF671AY
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GNMD2YCJ.BTF682AY
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GNMD2YCM.BTF686AY
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GNMD2YCQ.BTF687AY
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PTEM/GNMD2YCT.BTF692AY
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GNMD2YCX.BTF702AY
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GNMD2YDA.BTF707AY
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GNMD2YDD.BTF710AY
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GNMD2YDG.BTF715AY
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GNMD2YDJ.BTF718AY
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GNMD2YDM.BTF720AY
       m_FileAssign -d SHR -g ${G_A31} -C ${DATA}/PTEM/GNMD2YDQ.BTF725AY
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GNMD2YDT.BTF751AY
       m_FileAssign -d SHR -g ${G_A33} -C ${DATA}/PTEM/GNMD2YDX.BTF755AY
       m_FileAssign -d SHR -g ${G_A34} -C ${DATA}/PTEM/GNMD2YEA.BTF761AY
       m_FileAssign -d SHR -g ${G_A35} -C ${DATA}/PTEM/GNMD2YED.BTF766AY
       m_FileAssign -d SHR -g ${G_A36} -C ${DATA}/PTEM/GNMD2YEG.BTF770AY
       m_FileAssign -d SHR -g ${G_A37} -C ${DATA}/PTEM/GNMD2YEJ.BTF777AY
       m_FileAssign -d SHR -g ${G_A38} -C ${DATA}/PTEM/GNMD2YEM.BTF782AY
       m_FileAssign -d SHR -g ${G_A39} -C ${DATA}/PTEM/GNMD2YEQ.BTF783AY
       m_FileAssign -d SHR -g ${G_A40} -C ${DATA}/PTEM/GNMD2YET.BTF784AY
       m_FileAssign -d SHR -g ${G_A41} -C ${DATA}/PTEM/GNMD2YEX.BTF785AY
       m_FileAssign -d SHR -g ${G_A42} -C ${DATA}/PTEM/GNMD2YFA.BTF786AY
       m_FileAssign -d SHR -g ${G_A43} -C ${DATA}/PTEM/GNMD2YFD.BTF787AY
       m_FileAssign -d SHR -g ${G_A44} -C ${DATA}/PTEM/GNMD2YFG.BTF788AY
       m_FileAssign -d SHR -g ${G_A45} -C ${DATA}/PTEM/GNMD2YFJ.BTF794AY
       m_FileAssign -d SHR -g ${G_A46} -C ${DATA}/PTEM/GNMD2YFM.BTF795AY
       m_FileAssign -d SHR -g ${G_A47} -C ${DATA}/PTEM/GNMD2YFQ.BTF796AY
       m_FileAssign -d SHR -g ${G_A48} -C ${DATA}/PTEM/GNMD2YFT.BTF797AY
       m_FileAssign -d SHR -g ${G_A49} -C ${DATA}/PTEM/GNMD2YFX.BTF798AY
       m_FileAssign -d SHR -g ${G_A50} -C ${DATA}/PTEM/GNMD2YGA.BTF799AY
       m_FileAssign -d SHR -g ${G_A51} -C ${DATA}/PTEM/GNMD2YGG.BTF805AY
       m_FileAssign -d SHR -g ${G_A52} -C ${DATA}/PTEM/GNMD2YGJ.BTF810AY
       m_FileAssign -d SHR -g ${G_A53} -C ${DATA}/PTEM/GNMD2YGM.BTF815AY
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/GNMD2YGQ.BTF820AY
       m_FileAssign -d SHR -g ${G_A55} -C ${DATA}/PTEM/GNMD2YGT.BTF825AY
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/GNMD2YGX.BTF835AY
       m_FileAssign -d SHR -g ${G_A57} -C ${DATA}/PTEM/GNMD2YHA.BTF836AY
       m_FileAssign -d SHR -g ${G_A58} -C ${DATA}/PTEM/GNMD2YHD.BTF837AY
       m_FileAssign -d SHR -g ${G_A59} -C ${DATA}/PTEM/GNMD2YHG.BTF838AY
       m_FileAssign -d SHR -g ${G_A60} -C ${DATA}/PTEM/GNMD2YHJ.BTF839AY
       m_FileAssign -d SHR -g ${G_A61} -C ${DATA}/PTEM/GNMD2YHM.BTF840AY
#         FILE  NAME=BTF841AY,MODE=I                                           
       m_FileAssign -d SHR -g ${G_A62} -C ${DATA}/PTEM/GNMD2YHQ.BTF842AY
       m_FileAssign -d SHR -g ${G_A63} -C ${DATA}/PTEM/GNMD2YHT.BTF843AY
       m_FileAssign -d SHR -g ${G_A64} -C ${DATA}/PTEM/GNMD2YHX.BTF844AY
       m_FileAssign -d SHR -g ${G_A65} -C ${DATA}/PTEM/GNMD2YIA.BTF845AY
       m_FileAssign -d SHR -g ${G_A66} -C ${DATA}/PTEM/GNMD2YID.BTF846AY
       m_FileAssign -d SHR -g ${G_A67} -C ${DATA}/PTEM/GNMD2YIG.BTF847AY
       m_FileAssign -d SHR -g ${G_A68} -C ${DATA}/PTEM/GNMD2YIJ.BTF848AY
       m_FileAssign -d SHR -g ${G_A69} -C ${DATA}/PTEM/GNMD2YIM.BTF849AY
       m_FileAssign -d SHR -g ${G_A70} -C ${DATA}/PTEM/GNMD2YIQ.BTF860AY
       m_FileAssign -d SHR -g ${G_A71} -C ${DATA}/PTEM/GNMD2YIT.BTF862AY
       m_FileAssign -d SHR -g ${G_A72} -C ${DATA}/PTEM/GNMD2YIX.BTF863AY
       m_FileAssign -d SHR -g ${G_A73} -C ${DATA}/PTEM/GNMD2YJA.BTF864AY
       m_FileAssign -d SHR -g ${G_A74} -C ${DATA}/PTEM/GNMD2YJD.BTF865AY
       m_FileAssign -d SHR -g ${G_A75} -C ${DATA}/PTEM/GNMD2YJG.BTF875AY
       m_FileAssign -d SHR -g ${G_A76} -C ${DATA}/PTEM/GNMD2YJJ.BTF877AY
       m_FileAssign -d SHR -g ${G_A77} -C ${DATA}/PTEM/GNMD2YJJ.BTF877BY
       m_FileAssign -d SHR -g ${G_A78} -C ${DATA}/PTEM/GNMD2YJM.BTF878AY
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2YJQ.BTF01AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2YJR
       ;;
(GNMD2YJR)
       m_CondExec 00,EQ,GNMD2YJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   AS400 LOCAUX                                                               
# ********************************************************************         
#   TRI DU FICHIER BTF001AY     NOUVEAU POUR EXCLURE LES ENRS RTXM0            
#   SUR LUI-MEME                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJT
       ;;
(GNMD2YJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P945/SEM.BTF001AY
# ****    MODIF                                                                
       m_FileAssign -d SHR -g +0 SORTOUT ${DATA}/P945/SEM.BTF001AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /DERIVEDFIELD CST_3_8 "X-RTGA59"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_1_385 1 CH 385
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_8 EQ CST_3_8 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2YJU
       ;;
(GNMD2YJU)
       m_CondExec 00,EQ,GNMD2YJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   AS400 LOCAUX                                                               
# ********************************************************************         
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YJX PGM=BTF900     ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YJX
       ;;
(GNMD2YJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A79} FTF600 ${DATA}/PTEM/GNMD2YJQ.BTF01AY
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/P945/SEM.BTF001AY
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/EUMD2YCA.BTF002AY
       m_ProgramExec BTF900 
# ********************************************************************         
#   AS400 LOCAUX                                                               
# ********************************************************************         
#   TRI DU FICHIER BTF001X      NOUVEAUTE                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKA
       ;;
(GNMD2YKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P945/SEM.BTF001AY
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2YKA.BTF901AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_13 "RTPR14"
 /DERIVEDFIELD CST_3_9 "RTPR12"
 /DERIVEDFIELD CST_1_5 "RTPR10"
 /FIELDS FLD_CH_25_8 25 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_5 OR FLD_CH_1_6 EQ CST_3_9 OR FLD_CH_1_6 EQ CST_5_13 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_25_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2YKB
       ;;
(GNMD2YKB)
       m_CondExec 00,EQ,GNMD2YKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   AS400 LOCAUX                                                               
# ********************************************************************         
#  BTF901 : MISE A JOUR DU FICHIER BTF901AY                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKD PGM=IKJEFT01   ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKD
       ;;
(GNMD2YKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A80} FTF600 ${DATA}/PTEM/GNMD2YKA.BTF901AY
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/GNMD2YKD.BTF901BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF901 
       JUMP_LABEL=GNMD2YKE
       ;;
(GNMD2YKE)
       m_CondExec 04,GE,GNMD2YKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   AS400 LOCAUX                                                               
# ********************************************************************         
#   TRI DU FICHIER BTF901                                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKG
       ;;
(GNMD2YKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A81} SORTIN ${DATA}/PTEM/GNMD2YKD.BTF901BY
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.BTF901CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2YKH
       ;;
(GNMD2YKH)
       m_CondExec 00,EQ,GNMD2YKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   AS400 LOCAUX                                                               
# ********************************************************************         
#  BMQ910 : ENVOI DES MAJ PAR MQSERIES POUR LES AS400 LOCAUX                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKJ PGM=IKJEFT01   ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKJ
       ;;
(GNMD2YKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A82} FMQ910 ${DATA}/PXX0/F45.BTF802AY
       m_FileAssign -d SHR -g ${G_A83} -C ${DATA}/PTEM/EUMD2YCA.BTF002AY
       m_FileAssign -d SHR -g ${G_A84} -C ${DATA}/PXX0/F45.BTF901CY
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/GNMD2Y1N
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2YKJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ910 
       JUMP_LABEL=GNMD2YKK
       ;;
(GNMD2YKK)
       m_CondExec 04,GE,GNMD2YKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DU FICHIER SEQUENTIEL POUR LIBERER DE LA PLACE                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKM PGM=IDCAMS     ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKM
       ;;
(GNMD2YKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2YKM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2YKN
       ;;
(GNMD2YKN)
       m_CondExec 16,NE,GNMD2YKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER BTF001AY POUR COMPARAISON DU LENDEMAIN                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2YKQ PGM=IEBGENER   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YKQ
       ;;
(GNMD2YKQ)
       m_CondExec ${EXAQJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A85} SYSUT1 ${DATA}/PTEM/GNMD2YJQ.BTF01AY
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SYSUT2 ${DATA}/P945/SEM.BTF001AY
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2YKR
       ;;
(GNMD2YKR)
       m_CondExec 00,EQ,GNMD2YKQ ${EXAQJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2YZA
       ;;
(GNMD2YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
