#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ST030F.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFST030 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/16 AT 17.11.32 BY BURTEC6                      
#    STANDARDS: P  JOBSET: ST030F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR PARIS                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=ST030FA
       ;;
(ST030FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       RUN=${RUN}
       JUMP_LABEL=ST030FAA
       ;;
(ST030FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F07.ST030FA
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F07.ST030FA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAB
       ;;
(ST030FAB)
       m_CondExec 04,GE,ST030FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR LUX                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAD
       ;;
(ST030FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/P908/SEM.ST030FB
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/P908/SEM.ST030FB
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAE
       ;;
(ST030FAE)
       m_CondExec 04,GE,ST030FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR MGI OUEST                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAG
       ;;
(ST030FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F16.ST030FC
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F16.ST030FC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAH
       ;;
(ST030FAH)
       m_CondExec 04,GE,ST030FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR LYON                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAJ
       ;;
(ST030FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F45.ST030FD
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F45.ST030FD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAK
       ;;
(ST030FAK)
       m_CondExec 04,GE,ST030FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR LILLE                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAM
       ;;
(ST030FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F61.ST030FE
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F61.ST030FE
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAN
       ;;
(ST030FAN)
       m_CondExec 04,GE,ST030FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR DAL                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAQ
       ;;
(ST030FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F89.ST030FF
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F89.ST030FF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAR
       ;;
(ST030FAR)
       m_CondExec 04,GE,ST030FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BST030                                                                      
#  ALIMENTATION DES STATISTIQUES VENTES LI�ES POUR DPM                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ST030FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ST030FAT
       ;;
(ST030FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS03 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE10   : NAME=RSVE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTVE11   : NAME=RSVE11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
# ******  TABLE EN LECTURE                                                     
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  TABLE EN MAJ                                                         
#    RTST30   : NAME=RSST30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTST30 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d SHR -g +0 FDST030 ${DATA}/PXX0/F91.ST030FG
# ************ FICHIER FEXTRAC TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDST030O ${DATA}/PXX0/F91.ST030FG
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BST030 
       JUMP_LABEL=ST030FAU
       ;;
(ST030FAU)
       m_CondExec 04,GE,ST030FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
