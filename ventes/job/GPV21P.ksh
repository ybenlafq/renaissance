#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV21P.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGPV21 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/11/15 AT 18.28.37 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GPV21P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV21PA
       ;;
(GPV21PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV21PAA
       ;;
(GPV21PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )                
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F07.DATPV21P
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA73   : NAME=RSGA73,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA73 /dev/null
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS30 /dev/null
#                                                                              
# ------  FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PXX0/GPV21PAA.HPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220 ${DATA}/PXX0/GPV21PAA.HPV200BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV21PAB
       ;;
(GPV21PAB)
       m_CondExec 04,GE,GPV21PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER HPV200AP (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)         
#  ET CREATION DU FICHIER HPV211AP ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAD
       ;;
(GPV21PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GPV21PAA.HPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV21PAD.HPV211AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PAE
       ;;
(GPV21PAE)
       m_CondExec 00,EQ,GPV21PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER HPV200BP (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,         
#  ET CREATION DU FICHIER HPV211BP ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAG
       ;;
(GPV21PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GPV21PAA.HPV200BP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV21PAG.HPV211BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PAH
       ;;
(GPV21PAH)
       m_CondExec 00,EQ,GPV21PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV210                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES EN VUE DE CREER UN FICHIER D'EXTRACT                 
#  ENTRANT DS LE GENERATEUR D'ETAT (INTERESSEMENT HEBDO)                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAJ
       ;;
(GPV21PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )                
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FSOCLIEU ${DATA}/CORTEX4.P.MTXTFIX1/RDARSOC
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F07.DATPV21P
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA73   : NAME=RSGA73,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA73 /dev/null
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS30 /dev/null
#                                                                              
# ------  FICHIER D'EXTRACTION                                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PXX0/GPV21PAJ.HPV210AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV210 
       JUMP_LABEL=GPV21PAK
       ;;
(GPV21PAK)
       m_CondExec 04,GE,GPV21PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER HPV210AP (FEXTRAC) ISSU DE BPV210 ET CREATION DU FIC         
#  HPV210BP ENTRANT DANS LE PGM BPV211                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAM
       ;;
(GPV21PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GPV21PAJ.HPV210AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GPV21PAM.HPV210BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_62_7 62 CH 7
 /KEYS
   FLD_CH_62_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PAN
       ;;
(GPV21PAN)
       m_CondExec 00,EQ,GPV21PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV211                                                                
# ********************************************************************         
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAQ
       ;;
(GPV21PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- TABLES EN LECTURE                                                    
#                                                                              
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA26 /dev/null
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
#                                                                              
# ------- FICHIER D'EXTRACTION ISSU DES TRIS PRECEDENTS                        
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC1 ${DATA}/PXX0/GPV21PAD.HPV211AP
       m_FileAssign -d SHR -g ${G_A5} FPV220 ${DATA}/PXX0/GPV21PAG.HPV211BP
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC2 ${DATA}/PXX0/GPV21PAM.HPV210BP
#                                                                              
# ------- FICHIERS D'EDITION ENTRANT DANS LE GENERATEUR D'ETAT                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTR1B ${DATA}/PIP0/GPV21PAQ.IPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTR2B ${DATA}/PIP0/GPV21PAQ.IPV210AP
#                                                                              
# ------- FICHIER FPV220B EN DUMMY                                             
#                                                                              
       m_FileAssign -d SHR FPV220B /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV21PAR
       ;;
(GPV21PAR)
       m_CondExec 04,GE,GPV21PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IPV210AP POUR CREATION DU FICHIER (FEXTRAC1)                 
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV210)                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAT
       ;;
(GPV21PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PIP0/GPV21PAQ.IPV210AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PAT.IPV210BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_PD_185_5 185 PD 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_PD_180_5 180 PD 5
 /FIELDS FLD_CH_43_5 43 CH 5
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_43_5 ASCENDING,
   FLD_CH_48_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_185_5,
    TOTAL FLD_PD_180_5
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PAU
       ;;
(GPV21PAU)
       m_CondExec 00,EQ,GPV21PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV210           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PAX
       ;;
(GPV21PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLE DU GENERATEUR D'ETATS                                          
#                                                                              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  PREMIER FICHIER FEXTRAC1                                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PIP0/GPV21PAT.IPV210BP
#                                                                              
# ------  FICHIER FCUMULS                                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PIP0/GPV21PAX.IPV210CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21PAY
       ;;
(GPV21PAY)
       m_CondExec 04,GE,GPV21PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS (IPV210CP) ISSU DU BEG050 POUR CREATION DU           
#  FICHIER IPV210DP                                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBA
       ;;
(GPV21PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PIP0/GPV21PAX.IPV210CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PBA.IPV210DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PBB
       ;;
(GPV21PBB)
       m_CondExec 00,EQ,GPV21PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DU PREMIER ETAT IPV210                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBD
       ;;
(GPV21PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  FICHIER FEXTRAC ET FCUMUL                                            
#                                                                              
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PIP0/GPV21PAT.IPV210BP
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PIP0/GPV21PBA.IPV210DP
#                                                                              
# ------  PARAMETRE DATE , FMOIS , SOCIETE                                     
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ------  CREATION DE L'ETAT IPV210                                            
#                                                                              
       m_OutputAssign -c 9 -w IPV210 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21PBE
       ;;
(GPV21PBE)
       m_CondExec 04,GE,GPV21PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IPV210AP POUR CREATION DU FICHIER (FEXTRAC1)                 
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV210M)                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBG
       ;;
(GPV21PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PIP0/GPV21PAQ.IPV210AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PBG.IPV210EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "E"
 /DERIVEDFIELD CST_3_15 "S"
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_PD_190_5 190 PD 5
 /FIELDS FLD_CH_10_20 10 CH 20
 /FIELDS FLD_CH_33_10 33 CH 10
 /FIELDS FLD_CH_181_1 181 CH 1
 /FIELDS FLD_PD_185_5 185 PD 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /CONDITION CND_2 FLD_CH_181_1 EQ CST_1_11 OR FLD_CH_181_1 EQ CST_3_15 
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_20 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_10 ASCENDING,
   FLD_CH_43_5 ASCENDING,
   FLD_CH_48_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_185_5,
    TOTAL FLD_PD_190_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PBH
       ;;
(GPV21PBH)
       m_CondExec 00,EQ,GPV21PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV210           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBJ
       ;;
(GPV21PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLE DU GENERATEUR D'ETATS                                          
#                                                                              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  PREMIER FICHIER FEXTRAC1                                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PIP0/GPV21PBG.IPV210EP
#                                                                              
# ------  FICHIER FCUMULS                                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PIP0/GPV21PBJ.IPV210FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21PBK
       ;;
(GPV21PBK)
       m_CondExec 04,GE,GPV21PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS (IPV210FP) ISSU DU BEG050 POUR CREATION DU           
#  FICHIER IPV210GP                                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBM
       ;;
(GPV21PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PIP0/GPV21PBJ.IPV210FP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PBM.IPV210GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PBN
       ;;
(GPV21PBN)
       m_CondExec 00,EQ,GPV21PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DU SECOND  ETAT IPV210M                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBQ
       ;;
(GPV21PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  FICHIER FEXTRAC ET FCUMUL                                            
#                                                                              
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PIP0/GPV21PBG.IPV210EP
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PIP0/GPV21PBM.IPV210GP
#                                                                              
# ------  PARAMETRE DATE , FMOIS , SOCIETE                                     
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ------  CREATION DE L'ETAT IPV210M                                           
#                                                                              
       m_OutputAssign -c 9 -w IPV210M FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21PBR
       ;;
(GPV21PBR)
       m_CondExec 04,GE,GPV21PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IPV200AP POUR CREATION DU FICHIER (FEXTRAC1)                 
#  ENTRANT DANS LE GENERATEUR  D'ETAT (ETAT IPV200)                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBT
       ;;
(GPV21PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PIP0/GPV21PAQ.IPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PBT.IPV200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_16_10 16 CH 10
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_CH_7_3 7 CH 3
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_10 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_31_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PBU
       ;;
(GPV21PBU)
       m_CondExec 00,EQ,GPV21PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE PREMIER ETAT IPV200           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PBX
       ;;
(GPV21PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLE DU GENERATEUR D'ETATS                                          
#                                                                              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  PREMIER FICHIER FEXTRAC1                                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PIP0/GPV21PBT.IPV200BP
#                                                                              
# ------  FICHIER FCUMULS                                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PIP0/GPV21PBX.IPV200CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21PBY
       ;;
(GPV21PBY)
       m_CondExec 04,GE,GPV21PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS (IPV200CP) ISSU DU BEG050 POUR CREATION DU           
#  FICHIER IPV200DP                                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCA
       ;;
(GPV21PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PIP0/GPV21PBX.IPV200CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PCA.IPV200DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PCB
       ;;
(GPV21PCB)
       m_CondExec 00,EQ,GPV21PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DU PREMIER ETAT IPV200 (RECAP OFFRE ACTIVE HEBDO)                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCD
       ;;
(GPV21PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  FICHIER FEXTRAC ET FCUMUL                                            
#                                                                              
       m_FileAssign -d SHR -g ${G_A20} FEXTRAC ${DATA}/PIP0/GPV21PBT.IPV200BP
       m_FileAssign -d SHR -g ${G_A21} FCUMULS ${DATA}/PIP0/GPV21PCA.IPV200DP
#                                                                              
# ------  PARAMETRE DATE , FMOIS , SOCIETE                                     
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ------  CREATION DE L'ETAT IPV200                                            
#                                                                              
       m_OutputAssign -c 9 -w IPV200 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21PCE
       ;;
(GPV21PCE)
       m_CondExec 04,GE,GPV21PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER IPV200AP POUR CREATION DU FICHIER (FEXTRAC1)                 
#  ENTRANT DANS LE GENERATEUR D'ETAT (ETAT IPV200M)                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCG
       ;;
(GPV21PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PIP0/GPV21PAQ.IPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PCG.IPV200EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "E"
 /DERIVEDFIELD CST_3_13 "S"
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_31_5 31 CH 5
 /FIELDS FLD_CH_26_5 26 CH 5
 /FIELDS FLD_CH_16_10 16 CH 10
 /FIELDS FLD_CH_164_1 164 CH 1
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_CH_164_1 EQ CST_1_9 OR FLD_CH_164_1 EQ CST_3_13 
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_16_10 ASCENDING,
   FLD_CH_26_5 ASCENDING,
   FLD_CH_31_5 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PCH
       ;;
(GPV21PCH)
       m_CondExec 00,EQ,GPV21PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN PREMIER FICHIER FCUMULS POUR LE SECOND ETAT IPV200M           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCJ
       ;;
(GPV21PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLE DU GENERATEUR D'ETATS                                          
#                                                                              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  PREMIER FICHIER FEXTRAC1                                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PIP0/GPV21PCG.IPV200EP
#                                                                              
# ------  FICHIER FCUMULS                                                      
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PIP0/GPV21PCJ.IPV200FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GPV21PCK
       ;;
(GPV21PCK)
       m_CondExec 04,GE,GPV21PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FCUMULS (IPV200FP) ISSU DU BEG050 POUR CREATION DU           
#  FICHIER IPV200GP                                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCM
       ;;
(GPV21PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PIP0/GPV21PCJ.IPV200FP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PIP0/GPV21PCM.IPV200GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PCN
       ;;
(GPV21PCN)
       m_CondExec 00,EQ,GPV21PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DU DEUXIEME ETAT IPV200M (CODIC EN MAJ SUR UNE SEMAINE)            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCQ
       ;;
(GPV21PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  TABLES EN LECTURE                                                    
#                                                                              
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
#                                                                              
# ------  FICHIER FEXTRAC ET FCUMUL                                            
#                                                                              
       m_FileAssign -d SHR -g ${G_A25} FEXTRAC ${DATA}/PIP0/GPV21PCG.IPV200EP
       m_FileAssign -d SHR -g ${G_A26} FCUMULS ${DATA}/PIP0/GPV21PCM.IPV200GP
#                                                                              
# ------  PARAMETRE DATE , FMOIS , SOCIETE                                     
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ------  CREATION DE L'ETAT IPV200M                                           
#                                                                              
       m_OutputAssign -c 9 -w IPV200M FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GPV21PCR
       ;;
(GPV21PCR)
       m_CondExec 04,GE,GPV21PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV21PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PCT
       ;;
(GPV21PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA                                                       
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F07.DATPV21P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV21PCU
       ;;
(GPV21PCU)
       m_CondExec 00,EQ,GPV21PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV21PZA
       ;;
(GPV21PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV21PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
