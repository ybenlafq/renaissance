#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CA006P.ksh                       --- VERSION DU 08/10/2016 22:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCA006 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/12/09 AT 14.29.09 BY BURTEC6                      
#    STANDARDS: P  JOBSET: CA006P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   BMU980 : EXTRACTION DES MUTATIONS DES MGI DE DIF NLLE VERSION              
#   REPRISE : OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CA006PA
       ;;
(CA006PA)
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       C_A3=${C_A3:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=CA006PAA
       ;;
(CA006PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE                                                                
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
# ******  TABLE DU CALENDRIER DES MUTATIONS                                    
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  TABLE DETAIL DES MUTATIONS                                           
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******  TABLE DES PRMP                                                       
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  TABLE DES PRMP DACEM                                                 
#    RSGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE PARAMETRES ASSOCIES AUX FAMILLES                               
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
# ******  FICHIER D'EXTRACTION DES MUTS  PARIS                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMU980 ${DATA}/PTEM/CA006PAA.CA6MUTAP
#                                                                              
# ******  DATE PARAMETRE SOUS JJMMSSAA                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE LIEU :                                             
       m_FileAssign -d SHR FNLIEU ${DATA}/CORTEX4.P.MTXTFIX1/CA006PAA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMU980 
       JUMP_LABEL=CA006PAB
       ;;
(CA006PAB)
       m_CondExec 04,GE,CA006PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECREATION DU FICHIER EXTRACTION POUR CAPRO                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CA006PAD PGM=EZTPA00    ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CA006PAD
       ;;
(CA006PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} FILEA ${DATA}/PTEM/CA006PAA.CA6MUTAP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FILES ${DATA}/PTEM/CA006PAD.CA6MUTBP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/CA006PAD
       m_ProgramExec CA006PAD
# ********************************************************************         
#  CREATION DU FICHIER MUT TLM ET ELA DE CAPRO                                 
#              INCLUDE COND=(35,6,EQ,C'997000')                                
#  REPRISE: OUI.MAIS VOIR COMMENTAIRES DU PREMIER STEP                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CA006PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CA006PAG
       ;;
(CA006PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/CA006PAD.CA6MUTBP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PKA0/F44.CA0600A0
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "997000"
 /FIELDS FLD_CH_35_6 35 CH 6
 /CONDITION CND_1 FLD_CH_35_6 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CA006PAH
       ;;
(CA006PAH)
       m_CondExec 00,EQ,CA006PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CA006PZA
       ;;
(CA006PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CA006PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
