#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS080O.ksh                       --- VERSION DU 08/10/2016 13:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POFS080 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 15.12.09 BY BURTECN                      
#    STANDARDS: P  JOBSET: FS080O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BFS080 :                                                                    
#  REPRISE: NON BACKOUT JOBSET                                                 
#  MODIF LE 291100 CGA ETAT PLUS UTIL A MGIO                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS080OA
       ;;
(FS080OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=FS080OAA
       ;;
(FS080OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#  DEPENDANCE FORTE SUR FS052 CAR BFX080 NE TRAITE QUE LES VENTES              
#  AYANT DCOMPTA A LA DATE DU JOUR                                             
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************ TABLE EN MISE A JOUR ********************                       
# ******  MAJ DU NUMERO DE SEQ ET DU STATUS                                    
#    RSFT29O  : NAME=RSFT29O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFT29O /dev/null
# ******  MAJ DE LA DCOMPTA SUR TABLE DES LIGNES DE REGLEMENTS                 
#    RSGV14O  : NAME=RSGV14O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14O /dev/null
# ************ TABLE EN LECTURE *************************                      
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGV02O  : NAME=RSGV02O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  TABLE DES ECS                                                        
#    RSFX00O  : NAME=RSFX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00O /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# * A BLANC POUR TRAITEMENT NORMAL                                             
# * CODER LA DATE JJMMSSAA SI VOUS VOULEZ REPRENDRE UNE JOURNNEE               
       m_FileAssign -d SHR FCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FS080OAA
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS080O
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MGIO090
# ******  REPORT DES ANOMALIES                                                 
#  MODIF LE 291100 CGA ETAT PLUS UTIL A MGIO                                   
#  BCX0803  REPORT SYSOUT=(9,IFX080)                                           
       m_OutputAssign -c "*" BCX0803
# ******  FICHIER DES ENCAISSEMENTS LIVRAISON POUR GCT : PCL FV001O            
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FFV001 ${DATA}/PNCGO/F16.BFS080AO
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS080 
       JUMP_LABEL=FS080OAB
       ;;
(FS080OAB)
       m_CondExec 04,GE,FS080OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
