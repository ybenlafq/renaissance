#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  POGV135.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGV135 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/10/30 AT 11.20.37 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV135O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGV135 : 1. POUR VENTE 1 => EXTRACTION DES VENTES TLM DU JOUR              
#            2. MAJ DE LA DSTAT = FDATE DANS RTGV10 RTGV11                     
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV135OA
       ;;
(GV135OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV135OAA
       ;;
(GV135OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******* GENERALISEE                                                          
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* LIEUX                                                                
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* MARQUES                                                              
#    RSGA22O  : NAME=RSGA22O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22O /dev/null
# ******* ARTICLES COMMISSIONS PAR ZONE DE PRIX                                
#    RSGA62O  : NAME=RSGA62O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62O /dev/null
# ******* BBTE                                                                 
#    RSGA75O  : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75O /dev/null
# ******* PRMP DU JOUR                                                         
#    RSGG50O  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50O /dev/null
# ******* PRMP DACEM                                                           
#    RSGG50O  : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50O /dev/null
#                                                                              
# ******* VENTES (ENTETES DE VENTES)                                           
#    RSGV10O  : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
# ******* VENTES (LIGNES DE VENTES )                                           
#    RSGV11O  : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******* TABLE MAGASINS DU GROUPE  LGT                                        
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******  TABLE FAMILLES TRAITES  LGT                                          
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EXTRACTION DES VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -t LSEQ -g +1 FGV135 ${DATA}/PGV0/F16.BGV135AO
# ******  DETAILS DES VENTES EXTRAITES                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 216 -t LSEQ -g +1 FRTGV11 ${DATA}/PGV0/F16.BGV135BO
# ******  VENTES PAR GROUPE DE PRODUITS DE 80 A 93                             
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -t LSEQ -g +1 FHV105 ${DATA}/PGV0/F16.BGV135CO
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV135 
       JUMP_LABEL=GV135OAB
       ;;
(GV135OAB)
       m_CondExec 04,GE,GV135OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
