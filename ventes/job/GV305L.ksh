#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV305L.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV305 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/01/02 AT 15.33.36 BY PREPA2                       
#    STANDARDS: P  JOBSET: GV305L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='DNPC'                                                              
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV305LA
       ;;
(GV305LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV305LAA
       ;;
(GV305LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00L /dev/null
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
#    RSGA09L  : NAME=RSGA09L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09L /dev/null
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#    RSGA12L  : NAME=RSGA12L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12L /dev/null
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
#    RSGA20L  : NAME=RSGA20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20L /dev/null
#    RSGA22L  : NAME=RSGA22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22L /dev/null
#    RSGA23L  : NAME=RSGA23L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA23L /dev/null
#    RSGA25L  : NAME=RSGA25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25L /dev/null
#    RSGA26L  : NAME=RSGA26L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26L /dev/null
#    RSGA29L  : NAME=RSGA29L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29L /dev/null
#    RSGA53L  : NAME=RSGA53L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53L /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV305LAA
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/GV305LAA.BEX111GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV305LAB
       ;;
(GV305LAB)
       m_CondExec 04,GE,GV305LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAD
       ;;
(GV305LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV305LAA.BEX111GL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/GV305LAD.BEX111HL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305LAE
       ;;
(GV305LAE)
       m_CondExec 00,EQ,GV305LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BGV305  EXTRACTION DES VENTES DES 4 DERNIERES SEMAINES               
#       RTHV04 SELON LA SEGMENTATION PRESENTE DANS LE FICHIER FEX001           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAG
       ;;
(GV305LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
#                                                                              
#    RSHV04L  : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04L /dev/null
#                                                                              
# ***CARTE SOCIETE                                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PTEM/GV305LAD.BEX111HL
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 FGV305 ${DATA}/PTEM/GV305LAG.BGV305GL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV305 
       JUMP_LABEL=GV305LAH
       ;;
(GV305LAH)
       m_CondExec 04,GE,GV305LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR MAGASIN                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAJ
       ;;
(GV305LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GV305LAG.BGV305GL
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305LAJ.BGV305HL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305LAK
       ;;
(GV305LAK)
       m_CondExec 00,EQ,GV305LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR ZONE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAM
       ;;
(GV305LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GV305LAG.BGV305GL
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305LAM.BGV305JL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_7_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305LAN
       ;;
(GV305LAN)
       m_CondExec 00,EQ,GV305LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR GROUPE                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAQ
       ;;
(GV305LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GV305LAG.BGV305GL
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305LAQ.BGV305KL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_CH_9_2 9 CH 2
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_9_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305LAR
       ;;
(GV305LAR)
       m_CondExec 00,EQ,GV305LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DES ETATS IGV015 ET IGV018                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAT
       ;;
(GV305LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#                                                                              
# ******* FICHIER TRI� PAR MAGASIN                                             
       m_FileAssign -d SHR -g ${G_A6} FGV305 ${DATA}/PTEM/GV305LAJ.BGV305HL
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305LAT
#  FICHIERS EDITION                                                            
       m_OutputAssign -c 9 -w IGV015 IGV015S
#  IGV015M  REPORT SYSOUT=(9,IGV018),SPIN=UNALLOC                              
       m_OutputAssign -c Z IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305LAU
       ;;
(GV305LAU)
       m_CondExec 04,GE,GV305LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV016                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV305LAX
       ;;
(GV305LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#                                                                              
# ******* FICHIER TRI� PAR ZONE                                                
       m_FileAssign -d SHR -g ${G_A7} FGV305 ${DATA}/PTEM/GV305LAM.BGV305JL
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305LAX
#  FICHIERS EDITION                                                            
       m_OutputAssign -c 9 -w IGV016 IGV015S
       m_OutputAssign -c 9 -w IGV016 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305LAY
       ;;
(GV305LAY)
       m_CondExec 04,GE,GV305LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV017                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV305LBA
       ;;
(GV305LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#                                                                              
# ******* FICHIER TRI� PAR GRP DE MAGASINS                                     
       m_FileAssign -d SHR -g ${G_A8} FGV305 ${DATA}/PTEM/GV305LAQ.BGV305KL
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305LBA
#  FICHIERS EDITION                                                            
       m_OutputAssign -c 9 -w IGV017 IGV015S
       m_OutputAssign -c 9 -w IGV017 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305LBB
       ;;
(GV305LBB)
       m_CondExec 04,GE,GV305LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV305LZA
       ;;
(GV305LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV305LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
