#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV305M.ksh                       --- VERSION DU 08/10/2016 17:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMAV305 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/11/27 AT 09.11.18 BY BURTECA                      
#    STANDARDS: P  JOBSET: AV305M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='CONTROLE'                                                          
# **                                                                           
# ********************************************************************         
#  PGM BAV305 (BATCH COBOL2 DB2)                                               
#  CHARGEMENT DES RTAV01 ET RTAV02 A PARTIR DE LA RTAV10                       
#  MAJ DE LA RTAV10                                                            
#  SOUS-SYSTEME : RDAR                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=AV305MA
       ;;
(AV305MA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2008/11/27 AT 09.11.18 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: AV305M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALIM BASE AVOIRS'                      
# *                           APPL...: REPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=AV305MAA
       ;;
(AV305MAA)
       m_CondExec ${EXAAA},NE,YES 
#                                                                              
# *********************************                                            
# **    DEPENDANCE PLAN        ****                                            
# *********************************                                            
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE GENERALISEE RTAV10                                             
#    RSAV10   : NAME=RSAV10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSAV10 /dev/null
#                                                                              
# *****   PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *****   TABLES EN MAJ                                                        
#    RSAV01M  : NAME=RSAV01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV01M /dev/null
#    RSAV02M  : NAME=RSAV02M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV02M /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV305 
       JUMP_LABEL=AV305MAB
       ;;
(AV305MAB)
       m_CondExec 04,GE,AV305MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
