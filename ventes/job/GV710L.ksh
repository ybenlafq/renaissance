#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV710L.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV710 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/15 AT 14.59.56 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GV710L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='VENTES'                                                            
# ********************************************************************         
#  BGV705 : EXTRACTION DES VENTES "CODIC-GROUPES"                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV710LA
       ;;
(GV710LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV710LAA
       ;;
(GV710LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******* LIEUX                                                                
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******* RELATION CODE ETAT FAMILLE / RAYON                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
# ******* CODE RAYON DES FAMILLE / RAYON                                       
#    RSGA21L  : NAME=RSGA21L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21L /dev/null
# ******* RELATION ENTRE ARTICLE ET COMMISSIONS                                
#    RSGA62L  : NAME=RSGA62L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62L /dev/null
# ******* HISTO DES VENTES DES GROUPE DE PRODUITS                              
#    RSHV12L  : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12L /dev/null
#                                                                              
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******* FIC D'EXTRACTION DES VENTES "CODIC GROUPES"                          
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 FGV705 ${DATA}/PXX0/GV710LAA.BGV705AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV705 
       JUMP_LABEL=GV710LAB
       ;;
(GV710LAB)
       m_CondExec 04,GE,GV710LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER FGV705 : 1,11,A  :  SOC , SEQUENCE EDITION , FAMILL         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV710LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV710LAD
       ;;
(GV710LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GV710LAA.BGV705AL
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 SORTOUT ${DATA}/PXX0/GV710LAD.BGV710AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_90_6 90 PD 6
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_PD_52_6 52 PD 6
 /FIELDS FLD_PD_62_6 62 PD 6
 /FIELDS FLD_PD_80_4 80 PD 4
 /FIELDS FLD_PD_30_6 30 PD 6
 /FIELDS FLD_PD_96_6 96 PD 6
 /FIELDS FLD_PD_40_6 40 PD 6
 /FIELDS FLD_PD_46_6 46 PD 6
 /FIELDS FLD_PD_74_6 74 PD 6
 /FIELDS FLD_PD_18_6 18 PD 6
 /FIELDS FLD_PD_58_4 58 PD 4
 /FIELDS FLD_PD_84_6 84 PD 6
 /FIELDS FLD_PD_68_6 68 PD 6
 /FIELDS FLD_PD_36_4 36 PD 4
 /FIELDS FLD_PD_14_4 14 PD 4
 /FIELDS FLD_PD_24_6 24 PD 6
 /KEYS
   FLD_CH_1_11 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_14_4,
    TOTAL FLD_PD_18_6,
    TOTAL FLD_PD_24_6,
    TOTAL FLD_PD_30_6,
    TOTAL FLD_PD_36_4,
    TOTAL FLD_PD_40_6,
    TOTAL FLD_PD_46_6,
    TOTAL FLD_PD_52_6,
    TOTAL FLD_PD_58_4,
    TOTAL FLD_PD_62_6,
    TOTAL FLD_PD_68_6,
    TOTAL FLD_PD_74_6,
    TOTAL FLD_PD_80_4,
    TOTAL FLD_PD_84_6,
    TOTAL FLD_PD_90_6,
    TOTAL FLD_PD_96_6
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV710LAE
       ;;
(GV710LAE)
       m_CondExec 00,EQ,GV710LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV710 : EDITION DU SUIVI DES VENTES "GROUPES DE CODICS" IGV710             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV710LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV710LAG
       ;;
(GV710LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FAMILLES                                                             
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******* LIBELL�S DES RAYONS                                                  
#    RSGA20L  : NAME=RSGA20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20L /dev/null
#                                                                              
# ******* FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES VENTES DE BGV705 (TRI�)                     
       m_FileAssign -d SHR -g ${G_A2} FGV705 ${DATA}/PXX0/GV710LAD.BGV710AL
#                                                                              
# ******* EDITION ANALYSE COMMERCIALE DES VENTES "GROUPE DE CODICS"            
       m_OutputAssign -c 9 -w IGV710 IGV710
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV710 
       JUMP_LABEL=GV710LAH
       ;;
(GV710LAH)
       m_CondExec 04,GE,GV710LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV710LZA
       ;;
(GV710LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV710LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
