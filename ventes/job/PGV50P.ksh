#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PGV50P.ksh                       --- VERSION DU 08/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPGV50 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/11/13 AT 13.10.12 BY PREPA2                       
#    STANDARDS: P  JOBSET: PGV50P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BPGV500 : PURGE DES TABLES RTGV50 RTGV51 RTGV52 RTGV53 RTGV55 RTGV5         
#                  DELETE DES LIGNES OU DMOISVENTE   <  FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  NOTE  : A L'APPEL DE BETDATC ON FORCE : AA DE FDATE + MM DE FEXERCI         
#                                             ET ON SOUSTRAIT 731 JOUR         
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PGV50PA
       ;;
(PGV50PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PGV50PAA
       ;;
(PGV50PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   HISTO DES REMONTEES DES VENTES MGI                                   
#    RSGV50   : NAME=RSGV50,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV50 /dev/null
# *****   HISTO VENTES MGI DARTY OUEST                                         
#    RSGV51   : NAME=RSGV51,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV51 /dev/null
# *****   HISTO VENTES MGI DARTY OUEST                                         
#    RSGV52   : NAME=RSGV52,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV52 /dev/null
# *****   HISTO VENTES MGI DARTY OUEST                                         
#    RSGV53   : NAME=RSGV53,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV53 /dev/null
# *****   HISTO VENTES MGI DARTY OUEST                                         
#    RSGV55   : NAME=RSGV55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV55 /dev/null
# *****   HISTO VENTES MGI DARTY OUEST                                         
#    RSGV56   : NAME=RSGV56,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV56 /dev/null
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 0731 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PGV50PAA
# *****   N� DU MOIS DE L'EXERCICE DARTY : 02                                  
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPGV500 
       JUMP_LABEL=PGV50PAB
       ;;
(PGV50PAB)
       m_CondExec 04,GE,PGV50PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTGV50                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PAD
       ;;
(PGV50PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 20 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV50UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV50                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PAJ
       ;;
(PGV50PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 57 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV51UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV51                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PAQ
       ;;
(PGV50PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 57 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV52UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PAQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV52                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PAX
       ;;
(PGV50PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 57 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV53UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV53                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PBD
       ;;
(PGV50PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 57 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV55UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PBD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV55                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PBJ
       ;;
(PGV50PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 53 -g +1 SYSREC01 ${DATA}/PXX0/F07.GV56UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PBJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSGV56                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX RPGV50P                                      
#                  VERIFIER LE BACKOUT RPGV50P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PGV50PBM PGM=DSNUTILB   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PGV50PZA
       ;;
(PGV50PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PGV50PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
