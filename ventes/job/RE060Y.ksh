#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE060Y.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYRE060 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/02 AT 10.35.41 BY PREPA2                       
#    STANDARDS: P  JOBSET: RE060Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BRE060  CONSTITUTION DE LA SYSIN POUR FAST UNLOAD .                
#   WARNING : LE FICHIER FPARAM EST EN DUR DANS LE PCL .                       
#                   M = MENSUEL OU H = HEBDO                                   
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE060YA
       ;;
(RE060YA)
#
#RE060YBJ
#RE060YBJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#RE060YBJ
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RAAF=${RAAF:-RE060YAD}
       RUN=${RUN}
       JUMP_LABEL=RE060YAA
       ;;
(RE060YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#  FPARAM M = MENSUEL OU H = HEBDO                                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE060YAA
       m_FileAssign -d SHR FLOGIS ${DATA}/CORTEX4.P.MTXTFIX1/FLOGISY
#                                                                              
#  CONSTITUTION DE LA SYSIN POUR UNLOAD                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX2/RE60HUNL.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRE060 ${DATA}/PTEM/RE060YAA.BRE060AY
       m_ProgramExec BRE060 
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAD
       ;;
(RE060YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SYSREC01 ${DATA}/PTEM/RE060YAD.UNLGS40Y
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SYSREC02 ${DATA}/PTEM/RE060YAD.UNLGV10Y
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SYSREC03 ${DATA}/PTEM/RE060YAD.UNLGV11Y
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/RE060YAA.BRE060AY
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SI CODE RETOUR > 04  ABEND                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAG PGM=ZUTABEND   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAG
       ;;
(RE060YAG)
       m_CondExec ${EXAAK},NE,YES 04,GE,$[RAAF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAJ
       ;;
(RE060YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE060YAD.UNLGS40Y
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PTEM/RE060YAJ.GS40AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 1 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /* Record Type = F  Record Length = 073 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060YAK
       ;;
(RE060YAK)
       m_CondExec 00,EQ,RE060YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAM
       ;;
(RE060YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE060YAD.UNLGV11Y
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PTEM/RE060YAM.GV11AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 094 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060YAN
       ;;
(RE060YAN)
       m_CondExec 00,EQ,RE060YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   LIEU  SUR 4            N� DE VENTE SUR 7 :                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAQ
       ;;
(RE060YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RE060YAD.UNLGV10Y
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/RE060YAQ.GV10AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 011 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060YAR
       ;;
(RE060YAR)
       m_CondExec 00,EQ,RE060YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE061 .CE PGM COMPARE LES DEUX FICHIERS D UNLOAD                      
#   IL CHARGE LE FIC FRE061 AVEC LES DONNEES ABSENTES DE RTGS40                
#   ET CHARGE SUR FREMIS TOUS LES CODES REMISES RENCONTRES                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAT PGM=BRE061     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAT
       ;;
(RE060YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
#  FICHIER D UNLOAD RTGV10 TRIE                                                
       m_FileAssign -d SHR -g ${G_A5} RTGV10 ${DATA}/PTEM/RE060YAQ.GV10AY
#                                                                              
#  FICHIER D UNLOAD RTGV11 TRIE                                                
       m_FileAssign -d SHR -g ${G_A6} RTGV11 ${DATA}/PTEM/RE060YAM.GV11AY
#                                                                              
#  FICHIER DE SORTIE DE LRECL 110                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 FRE061 ${DATA}/PTEM/RE060YAT.BRE061AY
       m_ProgramExec BRE061 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER BRE061A*                                                   
#   CODIC + LIEU + VENTE + VENDEUR                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE060YAX
       ;;
(RE060YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE060YAT.BRE061AY
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 SORTOUT ${DATA}/PTEM/RE060YAX.BRE061BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_24_6 24 CH 6
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060YAY
       ;;
(RE060YAY)
       m_CondExec 00,EQ,RE060YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE062 .CE PGM ALIGNE LES REMISES DE TOUTES LES VENTES A PARTI         
#   DE FREMIS ET PREPARE UN FRE062 :                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE060YBA
       ;;
(RE060YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA52   : NAME=RSGA52Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52 /dev/null
#  TABLE                                                                       
#    RTGA59   : NAME=RSGA59Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59 /dev/null
#  TABLE                                                                       
#    RTGA65   : NAME=RSGA65Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65 /dev/null
#  TABLE                                                                       
#    RTGG20   : NAME=RSGG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#  TABLE                                                                       
#    RTGG50   : NAME=RSGG50Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#  TABLE                                                                       
#    RTPR00   : NAME=RSPR00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPR00 /dev/null
#  TABLE                                                                       
#    RTPR10   : NAME=RSPR10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPR10 /dev/null
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRE061 ${DATA}/PTEM/RE060YAX.BRE061BY
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/RE060YAJ.GS40AY
#                                                                              
#    FICHIER EN SORTIE DE LRECL 140                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FRE062 ${DATA}/PXX0/F45.RESULTAT.HISTO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE062 
       JUMP_LABEL=RE060YBB
       ;;
(RE060YBB)
       m_CondExec 04,GE,RE060YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RE060YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE060YBD
       ;;
(RE060YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F45.RESULTAT.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PTEM/RE060YBD.BRE062BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_99_5 99 CH 5
 /FIELDS FLD_CH_112_8 112 CH 8
 /FIELDS FLD_CH_88_1 88 CH 1
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_CH_73_5 73 CH 5
 /FIELDS FLD_CH_58_5 58 CH 5
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_CH_94_2 94 CH 2
 /FIELDS FLD_PD_104_8 104 PD 8
 /FIELDS FLD_CH_68_5 68 CH 5
 /FIELDS FLD_CH_21_6 21 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_63_5 63 CH 5
 /FIELDS FLD_CH_89_5 89 CH 5
 /FIELDS FLD_CH_78_5 78 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_21_6 ASCENDING,
   FLD_CH_58_5 ASCENDING,
   FLD_CH_63_5 ASCENDING,
   FLD_CH_68_5 ASCENDING,
   FLD_CH_73_5 ASCENDING,
   FLD_CH_78_5 ASCENDING,
   FLD_CH_83_5 ASCENDING,
   FLD_CH_88_1 ASCENDING,
   FLD_CH_89_5 ASCENDING,
   FLD_CH_94_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_99_5,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8
 /* Record Type = F  Record Length = 071 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_11_3,FLD_CH_21_6,FLD_CH_58_5,FLD_CH_63_5,FLD_CH_68_5,FLD_CH_73_5,FLD_CH_78_5,FLD_CH_83_5,FLD_CH_88_1,FLD_CH_89_5,FLD_CH_94_2,FLD_CH_99_5,FLD_CH_104_8,FLD_CH_112_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE060YBE
       ;;
(RE060YBE)
       m_CondExec 00,EQ,RE060YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DSNUTILB                                                                   
#   LOAD DE LA TABLE RTRE60                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE060YBG
       ;;
(RE060YBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ****** FICHIER DES DONNEES.                                                  
       m_FileAssign -d SHR -g ${G_A11} SYSREC ${DATA}/PTEM/RE060YBD.BRE062BY
# ****** TABLE RESULTAT                                                        
#    RSRE60   : NAME=RSRE60Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSRE60Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSRE60 /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060YBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/RE060Y_RE060YBG_RTRE60.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE060YBH
       ;;
(RE060YBH)
       m_CondExec 04,GE,RE060YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPAIR NOCOPY DU TABLESPACE RSRE060                               *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060YBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE060YBM
       ;;
(RE060YBM)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060Y1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060Y1 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060YBN
       ;;
(RE060YBN)
       m_CondExec 04,GE,RE060YBM ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE060YZA
       ;;
(RE060YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
