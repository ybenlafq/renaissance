#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPGV135.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV135 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/03/16 AT 11.09.17 BY BURTECC                      
#    STANDARDS: P  JOBSET: GV135P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGV135 : 1. POUR VENTE 1 => EXTRACTION DES VENTES TLM DU JOUR              
#            2. MAJ DE LA DSTAT = FDATE DANS RTGV10 RTGV11                     
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV135PA
       ;;
(GV135PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV135PAA
       ;;
(GV135PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#                                                                              
#    ATTENTION 2 JOBS DONC 2 PAVES DE PRED                                     
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* GENERALISEE                                                          
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* COMMISSION BBTE                                                      
#    RSGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* MARQUES                                                              
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* ARTICLES COMMISSIONS PAR ZONE DE PRIX                                
#    RSGA62   : NAME=RSGA62,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA62 /dev/null
# ******* PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG55 /dev/null
# ******* VENTES (ENTETES DE VENTES)                                           
#    RSGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* VENTES (LIGNES DE VENTES )                                           
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE MAGASINS DU GROUPE                                             
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******  TABLE FAMILLES TRAITES                                               
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EXTRACTION DES VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 FGV135 ${DATA}/PGV0/F07.BGV135AP
# ******  DETAILS DES VENTES EXTRAITES                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 216 -g +1 FRTGV11 ${DATA}/PGV0/F07.BGV135BP
# ******  VENTES PAR GROUPE DE PRODUITS DE 80 A 93                             
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV105 ${DATA}/PGV0/F07.BGV135CP
# ******  VENTES DES PRESTATIONS                                               
# ******  EN DUMMY NE SERT PLUS POUR LE MOMENT                                 
       m_FileAssign -d SHR FFS052 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV135 
       JUMP_LABEL=GV135PAB
       ;;
(GV135PAB)
       m_CondExec 04,GE,GV135PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
