#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PRE00P.ksh                       --- VERSION DU 08/10/2016 12:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPRE00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/12/30 AT 16.30.29 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PRE00P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#     PURGE DES HISTO REMISES COMMERCIALES PAR VENDEURS                        
#               RTRE00 RTRE10                                                  
#                                                                              
#           GARDER 1 ANS + L'HISTO EN COURS                                    
#           ==> ON FORCE DANS BETDATC LE MOIS LU DANS FEXERCIC                 
#                  ET ON SOUSTRAIT NJOURS     LU DANS FCARTE                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PRE00PA
       ;;
(PRE00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PRE00PAA
       ;;
(PRE00PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   HISTO DETAILS REMISES COMMERCIALES                                   
#    RSRE00   : NAME=RSRE00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRE00 /dev/null
# *****   HISTO REMISES COMMERCIALES PAR VENDEUR                               
#    RSRE10   : NAME=RSRE10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSRE10 /dev/null
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 0365 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PRE00PAA
# *****   N� DU MOIS DE L'EXERCICE DARTY : 02                                  
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPRE000 
       JUMP_LABEL=PRE00PAB
       ;;
(PRE00PAB)
       m_CondExec 04,GE,PRE00PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTRE00                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PRE00PAD
       ;;
(PRE00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 48 -g +1 SYSREC01 ${DATA}/PXX0/F07.RE00UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSRE00                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PRE00PR0                                     
#                  VERIFIER LE BACKOUT RPRE00P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PRE00PAJ
       ;;
(PRE00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 97 -g +1 SYSREC01 ${DATA}/PXX0/F07.RE10UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSRE10                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PRE00PR1                                     
#                  VERIFIER LE BACKOUT RPRE00P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PRE00PZA
       ;;
(PRE00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
