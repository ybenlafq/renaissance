#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EM927P.ksh                       --- VERSION DU 08/10/2016 22:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEM927 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/01/13 AT 10.49.00 BY BURTECA                      
#    STANDARDS: P  JOBSET: EM927P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  MERGE DU FICHIER BNM155AP AVEC EVENTUELLEMENT LES MVTS DE LA VEILLE         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EM927PA
       ;;
(EM927PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2004/01/13 AT 10.49.00 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: EM927P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ARCHIV. CAISSES'                       
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EM927PAA
       ;;
(EM927PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ************************                                                     
#  DEPENDANCE POUR PLAN                                                        
# ************************                                                     
# *****   FIC VENANT DU PCL NM001P                                             
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM155AP
# *****   FICHIER UTILISE DANS LA CHAINE                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/EM927PAA.BIC927AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_9_8 09 CH 8
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_CH_43_1 43 CH 1
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_17_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EM927PAB
       ;;
(EM927PAB)
       m_CondExec 00,EQ,EM927PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ARCHIVAGE DES TRANSACTIONS DE CAISSE                                        
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EM927PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EM927PAD
       ;;
(EM927PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PTEM/EM927PAA.BIC927AP
#                                                                              
#   TABLE DES MODES DE PAIEMENT NEM                                            
#    RTPM06   : NAME=RSPM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPM06 /dev/null
#   TABLE DES ARTICLES                                                         
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
#   TABLE                                                                      
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#   TABLE DES VENTES                                                           
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#   TABLE DES ENTETES DE CAISSE                                                
#    RTEM51   : NAME=RSEM51,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM51 /dev/null
#   TABLE DES LIGNES DETAIL TRANSACTIONS CAISSE                                
#    RTEM52   : NAME=RSEM52,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM52 /dev/null
#   TABLE DES REGLEMENTS DE TRANSACTIONS CAISSE                                
#    RTEM53   : NAME=RSEM53,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM53 /dev/null
#   TABLE DES TRANSACTIONS ADMINISTRATIVES                                     
#    RTEM54   : NAME=RSEM54,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM54 /dev/null
#   TABLE DES DECOMPTES CAISSE                                                 
#    RTEM55   : NAME=RSEM55,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM55 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM927 
       JUMP_LABEL=EM927PAE
       ;;
(EM927PAE)
       m_CondExec 04,GE,EM927PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EM927PZA
       ;;
(EM927PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EM927PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
