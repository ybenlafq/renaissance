#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV519L.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV519 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/07/13 AT 16.58.23 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GV519L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#    REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE           
#    -=-=-=-=                                                                  
# ********************************************************************         
#  QUIESCE DE RSGV27L                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV519LA
       ;;
(GV519LA)
#
#GV519LAA
#GV519LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV519LAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=GV519LAD
       ;;
(GV519LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DATE :JJMMSSAA + 1                                           
       m_FileAssign -i FDATE1
$VDATEJ1_JJMMANN
_end
# ******  PARAMETRE C SOC,NBRES D'AUTORISATIONS,DATEJ1 SSAAMMJJ                
# ******  SOIT        961300SSAAMMJJ                                           
       m_FileAssign -i FCARTE
$DNPCDEP_1_3900$VDATEJ1_ANNMMJJ
_end
# ******  TABLE DES AUTORISATIONS                                              
#    RSGV27L  : NAME=RSGV27L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27L /dev/null
# ******  LISTE DES AUTORISATIONS ENTREPOT                                     
       m_OutputAssign -c 9 -w IGV519 IGV519
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV519 
       JUMP_LABEL=GV519LAE
       ;;
(GV519LAE)
       m_CondExec 04,GE,GV519LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
