#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC116P.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC116 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/23 AT 13.45.39 BY OPERATB                      
#    STANDARDS: P  JOBSET: EC116P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS DU JOUR VENANT DE BEC105 DE CHAQUE FILIALE                 
#  + CREATION DU FEC116 DU JOUR                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC116PA
       ;;
(EC116PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'0'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC116PAA
       ;;
(EC116PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PTEM/F07.BEC105AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PTEM/F07.BEC105AY
# ******* FICHIER EXTRAIT TTES FILIALES                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/EC116PAA.BEC116AP
# ANCIEN TRI D.COM                                                             
# SORT FIELDS=(1,47,CH,A)                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_52 1 CH 52
 /KEYS
   FLD_CH_1_52 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC116PAB
       ;;
(EC116PAB)
       m_CondExec 00,EQ,EC116PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC116  -  ELIMINATION DOUBLONS SUITE BEC105 SUR TTES FILIALE         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAD
       ;;
(EC116PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EXTRAIT                                                      
       m_FileAssign -d SHR -g ${G_A1} FEC105 ${DATA}/PTEM/EC116PAA.BEC116AP
#                                                                              
# ******* FICHIER EXTRAIT VERS GATEWAY POUR M5I351                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC116 ${DATA}/PTEM/EC116PAD.BEC116EP
# ******* FIC DE LOAD RTEC06                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 FEC116F ${DATA}/PTEM/EC116PAD.BEC116FP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC116 
       JUMP_LABEL=EC116PAE
       ;;
(EC116PAE)
       m_CondExec 04,GE,EC116PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DE LOAD RTEC06                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAG
       ;;
(EC116PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/EC116PAD.BEC116FP
       m_FileAssign -d NEW,CATLG,DELETE -r 24 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.EC06RP
# SORT FIELDS=(6,5,CH,A,3,3,CH,A,1,2,CH,A)                                     
# SORT FIELDS=(6,5,CH,A,14,5,CH,A,3,3,CH,A,1,2,CH,A)                           
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_24 1 CH 24
 /KEYS
   FLD_CH_1_24 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC116PAH
       ;;
(EC116PAH)
       m_CondExec 00,EQ,EC116PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU FICHIER DDNAME FEC116  (BEC116EP)                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAJ
       ;;
(EC116PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/EC116PAD.BEC116EP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FEC116
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_385 1 CH 385
 /KEYS
   FLD_CH_1_385 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC116PAK
       ;;
(EC116PAK)
       m_CondExec 00,EQ,EC116PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC900  -  COMPARAISON FICHIERS J ET J-1                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAM
       ;;
(EC116PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A4} FEC90001 ${DATA}/PXX0/F07.FEC116
       m_FileAssign -d SHR -g ${G_A5} FEC90002 ${DATA}/PXX0/F07.FEC116
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC90003 ${DATA}/PTEM/EC116PAM.BEC116CP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC900 
       JUMP_LABEL=EC116PAN
       ;;
(EC116PAN)
       m_CondExec 04,GE,EC116PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC110  -  MISE EN FORME XML ET ENVOI MQ                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAQ
       ;;
(EC116PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE - CONTIENT PARAM POUR MQ                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A6} FEC11001 ${DATA}/PTEM/EC116PAM.BEC116CP
# ******* FICHIER FEC116X - FICHIER DES ENREGISTREMENTS XML PRODUITS           
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ -g +1 FEC11002 ${DATA}/PXX0/F07.FEC116X
# ******  LES PARAM POUR MQ SONT DANS TABLES DB2                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC110 
       JUMP_LABEL=EC116PAR
       ;;
(EC116PAR)
       m_CondExec 04,GE,EC116PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#           LOAD DE LA TABLE RTEC06                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAT
       ;;
(EC116PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* ZONES GEOGRAPHIQUES VALIDES                                          
#    RSEC06   : NAME=RSEC06,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEC06 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A7} SYSREC ${DATA}/PXX0/F07.RELOAD.EC06RP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC116PAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/EC116P_EC116PAT_RTEC06.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EC116PAU
       ;;
(EC116PAU)
       m_CondExec 04,GE,EC116PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DES FICHIERS VERS M5I351                                              
#  REPRISE : OUI                                                               
# *******************************************************************          
# ABJ      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BEC116BP,                                                           
#      FNAME=":BEC116BP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  CHANGEMENT PCL => EC116P <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTEC116P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC116PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EC116PAX
       ;;
(EC116PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EC116PAX.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP EC116PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EC116PBA
       ;;
(EC116PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC116PBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC116PZA
       ;;
(EC116PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC116PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
