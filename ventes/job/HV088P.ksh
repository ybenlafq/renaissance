#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV088P.ksh                       --- VERSION DU 08/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPHV088 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 10.23.25 BY BURTEC6                      
#    STANDARDS: P  JOBSET: HV088P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   FUSION DES FICHIERS HV08R* ISSU DE GV135*B                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=HV088PA
       ;;
(HV088PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2016/02/24 AT 10.23.25 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: HV088P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD RTHV88'                           
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=HV088PAA
       ;;
(HV088PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV991/F91.HV08RD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV961/F61.HV08RL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV989/F89.HV08RM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV916/F16.HV08RO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.HV08RP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV945/F45.HV08RY
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HV88RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV088PAB
       ;;
(HV088PAB)
       m_CondExec 00,EQ,HV088PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV88                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV088PAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HV088PAD
       ;;
(HV088PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV88                                                
       m_FileAssign -d SHR -g ${G_A1} SYSREC ${DATA}/PXX0/F07.HV88RP
#    RSHV88   : NAME=RSHV88,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV88 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV088PAD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/HV088P_HV088PAD_RTHV88.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=HV088PAE
       ;;
(HV088PAE)
       m_CondExec 04,GE,HV088PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FICHIERS HV08R*                                                  
# ********************************************************************         
#  SI AJOUT D'UNE NOUVELLE PRESENTATION, NE PAS OUBLIER D'AJOUTER CE           
#  FICHIER DANS L'ETAPE DE DELETE                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV088PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HV088PAG
       ;;
(HV088PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# -M-HV08RD   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN1 ${DATA}/PGV991/F91.HV08RD
# -M-HV08RL   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN2 ${DATA}/PGV961/F61.HV08RL
# -M-HV08RM   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN3 ${DATA}/PGV989/F89.HV08RM
# -M-HV08RO   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN4 ${DATA}/PGV916/F16.HV08RO
# -M-HV08RP   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN5 ${DATA}/PXX0/F07.HV08RP
# -M-HV08RY   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d OLD,KEEP,DELETE -g +0 IN6 ${DATA}/PGV945/F45.HV08RY
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV088PAG.sysin
       m_UtilityExec
# **************                                                               
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=HV088PAH
       ;;
(HV088PAH)
       m_CondExec 16,NE,HV088PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
