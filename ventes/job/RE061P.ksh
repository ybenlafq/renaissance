#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE061P.ksh                       --- VERSION DU 17/10/2016 18:34
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRE061 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 06/04/18 AT 09.39.34 BY BURTECR                      
#    STANDARDS: P  JOBSET: RE061P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BRE060  CONSTITUTION DE LA SYSIN POUR FAST UNLOAD                  
#              PARAMETRE Q POUR HEBDO                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE061PA
       ;;
(RE061PA)
#
#RE061PBJ
#RE061PBJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#RE061PBJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RAAF=${RAAF:-RE061PAD}
       RUN=${RUN}
       JUMP_LABEL=RE061PAA
       ;;
(RE061PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  FPARAM Q = TRAITEMENT HEBDO                                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE061PAA
       m_FileAssign -d SHR FLOGIS ${DATA}/CORTEX4.P.MTXTFIX1/FLOGISP
#  CONSTITUTION DE LA SYSIN POUR UNLOAD                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX2/RE61HUNL.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRE060 ${DATA}/PTEM/RE061PAA.BRE060XP
       m_ProgramExec BRE060 
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAD
       ;;
(RE061PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SYSREC01 ${DATA}/PTEM/RE060XAD.UNLGS40X
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SYSREC02 ${DATA}/PTEM/RE060XAD.UNLGV10X
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SYSREC03 ${DATA}/PTEM/RE060XAD.UNLGV11X
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/RE061PAA.BRE060XP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SI CODE RETOUR > 04  ABEND                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAG PGM=ZUTABEND   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAG
       ;;
(RE061PAG)
       m_CondExec ${EXAAK},NE,YES 04,GE,$[RAAF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAJ
       ;;
(RE061PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE060XAD.UNLGS40X
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PTEM/RE061PAJ.GS40XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 1 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /* Record Type = F  Record Length = 073 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE061PAK
       ;;
(RE061PAK)
       m_CondExec 00,EQ,RE061PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAM
       ;;
(RE061PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE060XAD.UNLGV11X
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PTEM/RE061PAM.GV11XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 094 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE061PAN
       ;;
(RE061PAN)
       m_CondExec 00,EQ,RE061PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   LIEU  SUR 4            N� DE VENTE SUR 7 :                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAQ
       ;;
(RE061PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RE060XAD.UNLGV10X
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/RE061PAQ.GV10XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 011 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE061PAR
       ;;
(RE061PAR)
       m_CondExec 00,EQ,RE061PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE061 .CE PGM COMPARE LES DEUX FICHIERS D UNLOAD                      
#   IL CHARGE LE FIC FRE061 AVEC LES DONNEES ABSENTES DE RTGS40                
#   ET CHARGE SUR FREMIS TOUS LES CODES REMISES RENCONTRES                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAT PGM=BRE061     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAT
       ;;
(RE061PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
#  FICHIER D UNLOAD RTGV10 TRIE                                                
       m_FileAssign -d SHR -g ${G_A5} RTGV10 ${DATA}/PTEM/RE061PAQ.GV10XP
#                                                                              
#  FICHIER D UNLOAD RTGV11 TRIE                                                
       m_FileAssign -d SHR -g ${G_A6} RTGV11 ${DATA}/PTEM/RE061PAM.GV11XP
#                                                                              
#  FICHIER DE SORTIE DE LRECL 82                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 FRE061 ${DATA}/PTEM/RE061PAT.BRE061XP
       m_ProgramExec BRE061 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER BRE061XP                                                   
#   CODIC + LIEU + VENTE + VENDEUR                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE061PAX
       ;;
(RE061PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE061PAT.BRE061XP
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 SORTOUT ${DATA}/PTEM/RE061PAX.BRE061YP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_24_6 24 CH 6
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_6 ASCENDING
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE061PAY
       ;;
(RE061PAY)
       m_CondExec 00,EQ,RE061PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE062 .CE PGM ALIGNE LES REMISES DE TOUTES LES VENTES A PARTI         
#   DE FREMIS ET PREPARE UN FRE062 :                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBA
       ;;
(RE061PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA52   : NAME=RSGA52,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA52 /dev/null
#  TABLE                                                                       
#    RTGA59   : NAME=RSGA59,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA59 /dev/null
#  TABLE                                                                       
#    RTGA65   : NAME=RSGA65,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA65 /dev/null
#  TABLE                                                                       
#    RTGG20   : NAME=RSGG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG20 /dev/null
#  TABLE                                                                       
#    RTGG50   : NAME=RSGG50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
#  TABLE                                                                       
#    RTPR00   : NAME=RSPR00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPR00 /dev/null
#  TABLE                                                                       
#    RTPR10   : NAME=RSPR10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPR10 /dev/null
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRE061 ${DATA}/PTEM/RE061PAX.BRE061YP
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/RE061PAJ.GS40XP
#                                                                              
#    FICHIER EN SORTIE DE LRECL 140                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FRE062 ${DATA}/PXX0/F07.BRE062XP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE062 
       JUMP_LABEL=RE061PBB
       ;;
(RE061PBB)
       m_CondExec 04,GE,RE061PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BRE062                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBD
       ;;
(RE061PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F07.BRE062XP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PTEM/RE061PBD.BRE062YP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_104_8 104 PD 8
 /FIELDS FLD_CH_63_5 63 CH 5
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_CH_78_5 78 CH 5
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_CH_68_5 68 CH 5
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_112_8 112 CH 8
 /FIELDS FLD_CH_99_5 99 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_58_5 58 CH 5
 /FIELDS FLD_CH_88_1 88 CH 1
 /FIELDS FLD_CH_73_5 73 CH 5
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_CH_94_2 94 CH 2
 /FIELDS FLD_CH_89_5 89 CH 5
 /FIELDS FLD_CH_21_6 21 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_21_6 ASCENDING,
   FLD_CH_58_5 ASCENDING,
   FLD_CH_63_5 ASCENDING,
   FLD_CH_68_5 ASCENDING,
   FLD_CH_73_5 ASCENDING,
   FLD_CH_78_5 ASCENDING,
   FLD_CH_83_5 ASCENDING,
   FLD_CH_88_1 ASCENDING,
   FLD_CH_89_5 ASCENDING,
   FLD_CH_94_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_99_5,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8
 /* Record Type = F  Record Length = 071 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_11_3,FLD_CH_21_6,FLD_CH_58_5,FLD_CH_63_5,FLD_CH_68_5,FLD_CH_73_5,FLD_CH_78_5,FLD_CH_83_5,FLD_CH_88_1,FLD_CH_89_5,FLD_CH_94_2,FLD_CH_99_5,FLD_CH_104_8,FLD_CH_112_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE061PBE
       ;;
(RE061PBE)
       m_CondExec 00,EQ,RE061PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DSNUTILB                                                                   
#   LOAD DE LA TABLE RTRE61                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBG
       ;;
(RE061PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ****** FICHIER DES DONNEES.                                                  
       m_FileAssign -d SHR -g ${G_A11} SYSREC ${DATA}/PTEM/RE061PBD.BRE062YP
# ****** TABLE RESULTAT                                                        
#    RSRE61   : NAME=RSRE61,MODE=(U,N) - DYNAM=YES                             
# -X-RSRE61   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSRE61 /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE061PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/RE061P_RE061PBG_RTRE61.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE061PBH
       ;;
(RE061PBH)
       m_CondExec 04,GE,RE061PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPAIR NOCOPY DU TABLESPACE RSRE060                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBM
       ;;
(RE061PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603H DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE061P1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE061P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE061PBN
       ;;
(RE061PBN)
       m_CondExec 04,GE,RE061PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE604 (DETAIL DU CA ET DES REMISES)                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBQ
       ;;
(RE061PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE604H DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE061P2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE061P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE061PBR
       ;;
(RE061PBR)
       m_CondExec 04,GE,RE061PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE605 (FICHIER DES VENDEURS)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBT
       ;;
(RE061PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE605H DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE061P3
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE061P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE061PBU
       ;;
(RE061PBU)
       m_CondExec 04,GE,RE061PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE603HD (ETAT DES REMISES PAR TYPE DE REMISE)           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE061PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=RE061PBX
       ;;
(RE061PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603HD DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE061P5
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE061P5 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE061PBY
       ;;
(RE061PBY)
       m_CondExec 04,GE,RE061PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE061PZA
       ;;
(RE061PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE061PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
