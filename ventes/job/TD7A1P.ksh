#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD7A1P.ksh                       --- VERSION DU 08/10/2016 14:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD7A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/08 AT 17.55.57 BY BURTECC                      
#    STANDARDS: P  JOBSET: TD7A1P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#         MISE � Z�RO DES FICHIERS NON GDG POUR L'ENVOI VERS DAFTEL            
#                                                                              
# ********************************************************************         
#         *                                                                    
#  IDCAMS *                                                                    
# *********                                                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=TD7A1PA
       ;;
(TD7A1PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2004/03/08 AT 17.55.57 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: TD7A1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-TD7A1P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=TD7A1PAA
       ;;
(TD7A1PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
# ******* FIC REMIS � Z�RO                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT1 ${DATA}/PXX0.F07.FTD070AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT2 ${DATA}/PXX0.F07.FTD070BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT3 ${DATA}/PXX0.F07.FTD070CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT4 ${DATA}/PXX0.F07.FTD070DP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT5 ${DATA}/PXX0.F07.FTD070EP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT6 ${DATA}/PXX0.F07.FTD070FP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT7 ${DATA}/PXX0.F07.FTD070GP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT8 ${DATA}/PXX0.F07.FTD070HP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT9 ${DATA}/PXX0.F07.FTD070IP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT10 ${DATA}/PXX0.F07.FTD070JP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT11 ${DATA}/PXX0.F07.FTD070KP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT12 ${DATA}/PXX0.F07.FTD070LP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 OUT13 ${DATA}/PXX0.F07.FTD070MP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD7A1PAA.sysin
       m_UtilityExec
#                                                                              
#   CREATION DU LIEN VIRTUEL POUR FV001P                                       
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD7A1PAB
       ;;
(TD7A1PAB)
       m_CondExec 16,NE,TD7A1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
