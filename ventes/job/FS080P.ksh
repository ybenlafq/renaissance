#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS080P.ksh                       --- VERSION DU 08/10/2016 17:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFS080 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/08/05 AT 08.10.56 BY PREPA2                       
#    STANDARDS: P  JOBSET: FS080P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BFS080 :                                                                    
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS080PA
       ;;
(FS080PA)
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=FS080PAA
       ;;
(FS080PAA)
       m_CondExec ${EXAAK},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#  DEPENDANCE FORTE SUR FS052 CAR BFX080 NE TRAITE QUE LES VENTES              
#  AYANT DCOMPTA A LA DATE DU JOUR                                             
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************ TABLE EN MISE A JOUR ********************                       
# ******  MAJ DU NUMERO DE SEQ ET DU STATUS                                    
#    RSFT29   : NAME=RSFT29,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSFT29 /dev/null
# ******  MAJ DE LA DCOMPTA SUR TABLE DES LIGNES DE REGLEMENTS                 
#    RSGV14   : NAME=RSGV14,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
# ************ TABLE EN LECTURE *************************                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  TABLE DES ECS                                                        
#    RSFX00   : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# * A BLANC POUR TRAITEMENT NORMAL                                             
# * CODER LA DATE JJMMSSAA SI VOUS VOULEZ REPRENDRE UNE JOURNNEE               
       m_FileAssign -d SHR FCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FS080PAA
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS080P
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DIF090
# ******  REPORT DES ANOMALIES                                                 
       m_OutputAssign -c 9 -w IFX080 BCX0803
# ******  FICHIER DES ENCAISSEMENTS LIVRAISON POUR GCT : PCL FV001P            
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FFV001 ${DATA}/PNCGP/F07.BFS080AP
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS080 
       JUMP_LABEL=FS080PAB
       ;;
(FS080PAB)
       m_CondExec 04,GE,FS080PAA ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
