#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PP000O.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPP000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/02/22 AT 16.17.22 BY PREPA2                       
#    STANDARDS: P  JOBSET: PP000O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPP000                                                                
#  ------------                                                                
#  CREATION DU FICHIER DES PAMMS INDIVIDUELLES                                 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PP000OA
       ;;
(PP000OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PP000OAA
       ;;
(PP000OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
#  CREATION DES FICHIERS                                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPP00   : NAME=RSPP00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPP00 /dev/null
# ------  FICHIERS PARAMETRE (FMOIS FNSOC)                                     
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ------  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 125 -g +1 FPP000 ${DATA}/PTEM/PP000OAA.BPP000AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP000 
       JUMP_LABEL=PP000OAB
       ;;
(PP000OAB)
       m_CondExec 04,GE,PP000OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS :  BPP000AO ISSU DU BPP000                                 
#                      BPP440AO ISSU DU BGV440 DE LA CHAINE PR440O             
#                      BPP120AO ISSU DU BPV120 DE LA CHAINE PV001O             
# ********************************************************************         
#  REPRISE: OUI                                                      *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAD
       ;;
(PP000OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PP000OAA.BPP000AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BPP440AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BPP120AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BPV410FO
       m_FileAssign -d NEW,CATLG,DELETE -r 125 -g +1 SORTOUT ${DATA}/PTEM/PP000OAD.BPP000BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_6 18 CH 6
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_18_6 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /* Record Type = F  Record Length = 125 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OAE
       ;;
(PP000OAE)
       m_CondExec 00,EQ,PP000OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPP001                                                                
#  ------------                                                                
#  CREATION DU FICHIER SIGA DES PRIMES INDIVIDUELLES                           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAG
       ;;
(PP000OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPP00   : NAME=RSPP00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPP00 /dev/null
# ------  FICHIERS PARAMETRE (FMOIS FNSOC)                                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A2} FPP000 ${DATA}/PTEM/PP000OAD.BPP000BO
# ------  FICHIER REPRIS PAA SIGA DANS LA CHAINE CGI10M                        
       m_FileAssign -d NEW,CATLG,DELETE -r 913 -g +1 FPP001 ${DATA}/PTEM/PP000OAG.SIG001AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP001 
       JUMP_LABEL=PP000OAH
       ;;
(PP000OAH)
       m_CondExec 04,GE,PP000OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUM DU FICHIER SIG001AM POUR ELIMINER LES DOUBLONS                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAJ
       ;;
(PP000OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PP000OAG.SIG001AO
       m_FileAssign -d NEW,CATLG,DELETE -r 913 -g +1 SORTOUT ${DATA}/PXX0/F16.SIG001BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_ZD_138_12 138 ZD 12
 /FIELDS FLD_CH_95_3 95 CH 3
 /FIELDS FLD_CH_13_12 13 CH 12
 /KEYS
   FLD_CH_13_12 ASCENDING,
   FLD_CH_95_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_138_12
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OAK
       ;;
(PP000OAK)
       m_CondExec 00,EQ,PP000OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BPP000BM POUR LE PGM BPP010                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAM
       ;;
(PP000OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PP000OAD.BPP000BO
       m_FileAssign -d NEW,CATLG,DELETE -r 95 -g +1 SORTOUT ${DATA}/PTEM/PP000OAM.BPP010AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_6 18 CH 6
 /FIELDS FLD_CH_12_6 12 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_18_6 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_12_6 ASCENDING,
   FLD_CH_7_5 ASCENDING
 /* Record Type = F  Record Length = 95 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OAN
       ;;
(PP000OAN)
       m_CondExec 00,EQ,PP000OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPP010                                                                
#  ------------                                                                
#  CREATIONS DES FICHIERS POUR LE PGM BPP001                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAQ
       ;;
(PP000OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ------  FICHIERS PARAMETRE (FMOIS FNSOC)                                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A5} FPP000 ${DATA}/PTEM/PP000OAM.BPP010AO
# ------  FICHIERS D'EXTRACTIONS                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 IPP010A ${DATA}/PTEM/PP000OAQ.FPP010AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 IPP015A ${DATA}/PTEM/PP000OAQ.FPP015AO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPP010B ${DATA}/PTEM/PP000OAQ.FPP010BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPP015B ${DATA}/PTEM/PP000OAQ.FPP015BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP010 
       JUMP_LABEL=PP000OAR
       ;;
(PP000OAR)
       m_CondExec 04,GE,PP000OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPP010AM POUR LE PGM BPP011                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAT
       ;;
(PP000OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/PP000OAQ.FPP010AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/PP000OAT.BPP011AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 1 CH 6
 /KEYS
   FLD_BI_1_6 ASCENDING
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OAU
       ;;
(PP000OAU)
       m_CondExec 00,EQ,PP000OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPP010BM POUR LE PGM BPP011                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PP000OAX
       ;;
(PP000OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PP000OAQ.FPP010BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PP000OAX.BPP011BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_39 1 CH 39
 /KEYS
   FLD_BI_1_39 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OAY
       ;;
(PP000OAY)
       m_CondExec 00,EQ,PP000OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPP011                                                                
#  ------------                                                                
#  EXTRACTION DES DONNEES POUR EDITER L'ETAT IPP010                            
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBA PGM=BPP011     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBA
       ;;
(PP000OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS ISSU DES TRIS PRECEDENTS                                    
       m_FileAssign -d SHR -g ${G_A8} IPP01XA ${DATA}/PTEM/PP000OAT.BPP011AO
       m_FileAssign -d SHR -g ${G_A9} IPP01XB ${DATA}/PTEM/PP000OAX.BPP011BO
# ------  FICHIER D'EXTRACTION POUR LE GENERATEUR D'ETAT                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPP01X ${DATA}/PTEM/PP000OBA.IPP010BO
       m_ProgramExec BPP011 
# ********************************************************************         
#  PGM : BEG050                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBD
       ;;
(PP000OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/PP000OBA.IPP010BO
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PP000OBD.IPP010CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PP000OBE
       ;;
(PP000OBE)
       m_CondExec 04,GE,PP000OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBG
       ;;
(PP000OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/PP000OBD.IPP010CO
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PP000OBG.IPP010DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OBH
       ;;
(PP000OBH)
       m_CondExec 00,EQ,PP000OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  ------------                                                                
#  CREATION DE L'ETAT IPP010 ETAT MENSUEL DE SYNTHESE DES PRIMES IND.          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBJ
       ;;
(PP000OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/PP000OBA.IPP010BO
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A13} FCUMULS ${DATA}/PTEM/PP000OBG.IPP010DO
# ------  FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER DUMMY                                                        
       m_FileAssign -d SHR FEG132 /dev/null
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IPP010                                                          
#  FEDITION REPORT SYSOUT=(9,IPP010),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_OutputAssign -c "*" FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PP000OBK
       ;;
(PP000OBK)
       m_CondExec 04,GE,PP000OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPP015AM POUR LE PGM BPP011                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBM
       ;;
(PP000OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/PP000OAQ.FPP015AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/PP000OBM.BPP011CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 1 CH 6
 /KEYS
   FLD_BI_1_6 ASCENDING
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OBN
       ;;
(PP000OBN)
       m_CondExec 00,EQ,PP000OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FPP015BM POUR LE PGM BPP011                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBQ
       ;;
(PP000OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/PP000OAQ.FPP015BO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PP000OBQ.BPP011DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_39 1 CH 39
 /KEYS
   FLD_BI_1_39 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OBR
       ;;
(PP000OBR)
       m_CondExec 00,EQ,PP000OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPP011                                                                
#  ------------                                                                
#  EXTRACTION DES DONNEES POUR EDITER L'ETAT IPP015                            
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBT PGM=BPP011     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBT
       ;;
(PP000OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS ISSU DES TRIS PRECEDENTS                                    
       m_FileAssign -d SHR -g ${G_A16} IPP01XA ${DATA}/PTEM/PP000OBM.BPP011CO
       m_FileAssign -d SHR -g ${G_A17} IPP01XB ${DATA}/PTEM/PP000OBQ.BPP011DO
# ------  FICHIER D'EXTRACTION POUR LE GENERATEUR D'ETAT                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPP01X ${DATA}/PTEM/PP000OBT.IPP015BO
       m_ProgramExec BPP011 
# ********************************************************************         
#  PGM : BEG050                                                                
#  ------------                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PP000OBX
       ;;
(PP000OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PTEM/PP000OBT.IPP015BO
# ------  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/PP000OBX.IPP015CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PP000OBY
       ;;
(PP000OBY)
       m_CondExec 04,GE,PP000OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PP000OCA
       ;;
(PP000OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/PP000OBX.IPP015CO
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PP000OCA.IPP015DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP000OCB
       ;;
(PP000OCB)
       m_CondExec 00,EQ,PP000OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  ------------                                                                
#  CREATION DE L'ETAT IPP015 ETAT MENSUEL DE SYNTHESE DES PRIMES PAMM          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP000OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PP000OCD
       ;;
(PP000OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ------  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ------  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ------  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A20} FEXTRAC ${DATA}/PTEM/PP000OBT.IPP015BO
# ------  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A21} FCUMULS ${DATA}/PTEM/PP000OCA.IPP015DO
# ------  FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------  FICHIER DUMMY                                                        
       m_FileAssign -d SHR FEG132 /dev/null
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IPP015                                                          
#  FEDITION REPORT SYSOUT=(9,IPP015),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_OutputAssign -c "*" FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PP000OCE
       ;;
(PP000OCE)
       m_CondExec 04,GE,PP000OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PP000OZA
       ;;
(PP000OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PP000OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
