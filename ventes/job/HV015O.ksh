#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015O.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 11.07.09 BY BURTECN                      
#    STANDARDS: P  JOBSET: HV015O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DE TABLESPACES RSHV20O RSHV25O AVANT MAJ                            
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015OA
       ;;
(HV015OA)
#
#HV015OAA
#HV015OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV015OAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=HV015OAD
       ;;
(HV015OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F16.BHV000AO
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/HV015OAD.BHV015AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_34_5 34 CH 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015OAE
       ;;
(HV015OAE)
       m_CondExec 00,EQ,HV015OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV015  : GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE                  
#            DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                      
#  REPRISE :OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HV015OAG
       ;;
(HV015OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES (GENERALITES ARTICLES)                                      
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  FAMILLE (EDITION DES ETATS)                                          
#    RSGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
# ******  RAYONS (COMPOSANTS DU RAYON)                                         
#    RSGA21O  : NAME=RSGA21O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21O /dev/null
# ******  QUOTAS DE LIVRAISON (POPULATION MARCHE/DONNEES DE BASE INSEE         
#    RSGQ01O  : NAME=RSGQ01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01O /dev/null
#                                                                              
# ******  FIC DES VENTES DU MOIS TRI�                                          
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PTEM/HV015OAD.BHV015AO
# ******  FIC DES STATS PAR MAGS/GRPE DE STAT/COMMUNE                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PTEM/HV015OAG.BHV015BO
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015OAH
       ;;
(HV015OAH)
       m_CondExec 04,GE,HV015OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SEQUENTIEL BHV015BO                                          
#  CODE RAMON  1,11 ; CODE COMMUNE  32,5                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HV015OAJ
       ;;
(HV015OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/HV015OAG.BHV015BO
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/HV015OAJ.BHV015CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_20_5 20 PD 5
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_CH_32_5 32 CH 05
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015OAK
       ;;
(HV015OAK)
       m_CondExec 00,EQ,HV015OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV020 : MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOIS             
#           ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                       
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE.FAIRE UN RECOVER DE RSHV20O RSHV25O AVE         
#           LA VALEUR DU RBA DU PREMIER STEP.UTILISEZ LA CHAINE DB2RBO         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=HV015OAM
       ;;
(HV015OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES STATS PAR MAGS/GROUPE DE STATS/COMMUNE                       
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PTEM/HV015OAJ.BHV015CO
#                                                                              
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                              
#    RSHV20O  : NAME=RSHV20O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV20O /dev/null
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                       
#    RSHV25O  : NAME=RSHV25O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV25O /dev/null
#                                                                              
# ******  DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015OAN
       ;;
(HV015OAN)
       m_CondExec 04,GE,HV015OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=HV015OZA
       ;;
(HV015OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
