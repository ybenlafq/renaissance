#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV000Y.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYHV000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/08/04 AT 09.34.57 BY BURTEC6                      
#    STANDARDS: P  JOBSET: HV000Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='LYON'                                                              
# ********************************************************************         
#   TRI DU FICHIER SUR CODIC                                                   
#   INCLUDE SUR LIEU ORIGINE ET LIEU DESTINATION                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV000YA
       ;;
(HV000YA)
#
#HV000YBA
#HV000YBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV000YBA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=HV000YAA
       ;;
(HV000YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BHV030AY
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -g +1 SORTOUT ${DATA}/PTEM/HV000YAA.BHV000CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "VEN"
 /DERIVEDFIELD CST_1_4 "VEN"
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_4 OR FLD_CH_13_3 EQ CST_3_8 
 /KEYS
   FLD_CH_19_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 127 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV000YAB
       ;;
(HV000YAB)
       m_CondExec 00,EQ,HV000YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  BHV000 : EXTRACTION DES VENTES DU MOIS                                    
# ********************************************************************         
#  BHV000 : EXTRACTION DES VENTES DU MOIS                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAD
       ;;
(HV000YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  MVTS DE STOCKS (MVTS STOCKS LIEU)                                    
       m_FileAssign -d SHR -g ${G_A1} FRTGS40 ${DATA}/PTEM/HV000YAA.BHV000CY
# ******  ARTICLES                                                             
#    RSGA00R  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00R /dev/null
# ******  LIEUX                                                                
#    RSGA10R  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10R /dev/null
# ******  ARTICLES ZONE DE PRIX                                                
#    RSGA59R  : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59R /dev/null
#                                                                              
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  MOIS TRAIT�                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *****   FIC D'EXTRACTION DES VENTES DU MOIS                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV000 ${DATA}/PGV0/F45.BHV000AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV000 
       JUMP_LABEL=HV000YAE
       ;;
(HV000YAE)
       m_CondExec 04,GE,HV000YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CODIC 7,7 ; MODE DE DELIVR 14,3 ; SOCIETE 1,3 ; MAGASIN 4,3                 
#  REPRISE:OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAG
       ;;
(HV000YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PGV0/F45.BHV000AY
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/HV000YAG.BHV000BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_29_6 29 PD 6
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_25_4 25 PD 4
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_25_4,
    TOTAL FLD_PD_29_6
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV000YAH
       ;;
(HV000YAH)
       m_CondExec 00,EQ,HV000YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV005  : ENRICHISSEMENT DU FICHIER DES EXTACTIONS DES VENTES               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAJ
       ;;
(HV000YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FAMILLES (EDITION DES ETATS)                                         
#    RSGA11R  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11R /dev/null
# ******  EDITION DES ETATS (TABLES MARKETING)                                 
#    RSGA12R  : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12R /dev/null
# ******  ASSOCIATIONS CODES VALEURS MARKETING CODES VALEURS DESCRP            
#    RSGA25R  : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25R /dev/null
# ******  ARTICLES (CODES DESCRIPTIFS VALEURS)                                 
#    RSGA53R  : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53R /dev/null
#                                                                              
# ******  FICHIER DES VENTES DU MOIS TRI�                                      
       m_FileAssign -d SHR -g ${G_A3} FHV005 ${DATA}/PTEM/HV000YAG.BHV000BY
# ******  VENTES DU MOIS ENRICHIES                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV010 ${DATA}/PTEM/HV000YAJ.BHV005AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV005 
       JUMP_LABEL=HV000YAK
       ;;
(HV000YAK)
       m_CondExec 04,GE,HV000YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER SEQUENTIEL BHV005AY                                        
#    SOCIETE 1,3 ; MAG 4,3 ; MODE DE DELI 7,3 ; SEQ DE FAMILLE 35,3            
#    CODE MARKETING 38,5 ; CODE VALEUR MARKETING 43,5                          
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAM
       ;;
(HV000YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/HV000YAJ.BHV005AY
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PTEM/HV000YAM.BHV005BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_43_5 43 CH 5
 /FIELDS FLD_PD_22_8 22 PD 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_35_3 35 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_16_6 16 PD 6
 /FIELDS FLD_CH_38_5 38 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_35_3 ASCENDING,
   FLD_CH_38_5 ASCENDING,
   FLD_CH_43_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_16_6,
    TOTAL FLD_PD_22_8
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV000YAN
       ;;
(HV000YAN)
       m_CondExec 00,EQ,HV000YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    BHV010 : CREATION DU FICHIER D'ALIMENTATION DE LA TABLE RTHV00            
#    ON NE GARDE QUE CE QUI DOIT ETRE CHARGE DEPUIS LES 12 DERNIERS MO         
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAQ
       ;;
(HV000YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  VENTES DU MOIS                                                       
       m_FileAssign -d SHR -g ${G_A5} FHV015 ${DATA}/PTEM/HV000YAM.BHV005BY
# ******  TABLE HISTO VENTES                                                   
#    RSHV00R  : NAME=RSHV00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV00R /dev/null
#                                                                              
# ******  FICHIER DES MOUVEMENTS DES 12 DERNIERS MOIS _A CHARGER                
       m_FileAssign -d NEW,CATLG,DELETE -r 56 -g +1 FHV020 ${DATA}/PTEM/HV000YAQ.BHV010AY
#                                                                              
# ******  MOIS MMSSAA                                                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV010 
       JUMP_LABEL=HV000YAR
       ;;
(HV000YAR)
       m_CondExec 04,GE,HV000YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    CREATION DU FICHIER DE LOAD DE LA TABLE HISTO DES VENTES                  
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAT
       ;;
(HV000YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/HV000YAQ.BHV010AY
       m_FileAssign -d NEW,CATLG,DELETE -r 56 -g +1 SORTOUT ${DATA}/PGV994/F45.RELOAD.HV00RR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_35_13 35 CH 13
 /FIELDS FLD_CH_1_15 1 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_35_13 ASCENDING
 /* Record Type = F  Record Length = 56 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV000YAU
       ;;
(HV000YAU)
       m_CondExec 00,EQ,HV000YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTHV00                                                    
#    REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=HV000YAX
       ;;
(HV000YAX)
       m_CondExec ${EXABJ},NE,YES 
#  FICHIERS DE TRAVAIL                                                         
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD DE RTHV00 DE LYON                                    
       m_FileAssign -d SHR -g ${G_A7} SYSREC ${DATA}/PGV994/F45.RELOAD.HV00RR
# ******  HISTO DES VENTES PAR MODE DE DELIVRANCE                              
#    RSHV00R  : NAME=RSHV00Y,MODE=(U,N) - DYNAM=YES                            
# -X-RSHV00Y  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV00R /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV000YAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/HV000Y_HV000YAX_RTHV00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=HV000YAY
       ;;
(HV000YAY)
       m_CondExec 04,GE,HV000YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR DU TS                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV000YBA PGM=DSNUTILB   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=HV000YZA
       ;;
(HV000YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV000YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
