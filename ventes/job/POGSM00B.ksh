#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  POGSM00B.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGSM00B -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: GSM00O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
#  BSM030 :   EDITION DE L'ETAT STOCKS MAGASINS                                
#  REPRISE:   OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GSM00OB
       ;;
(GSM00OB)
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXC98=${EXC98:-0}
       EX9999=${EX9999:-YES}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON MONDAY    2009/03/30 AT 11.17.34 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: GSM00O                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'EDITION STOCK'                         
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GSM00OBA
       ;;
(GSM00OBA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **************                       NUMERO DE SOCIETE                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FDATE JJMMSSAA + 1                                                   
       m_FileAssign -i FDATE
$FDATE1
_end
# ******  FICHIER EXTRACTION DES STOCKS MAGASINS TRIE                          
       m_FileAssign -d SHR -g +0 FSM025 ${DATA}/PXX0/F16.BSM025BO
# ******  TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  FAMILLES                                                             
#    RSGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  PRIX                                                                 
#    RSGA59O  : NAME=RSGA59O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA59O /dev/null
# ******  COMMISSIONS VENDEURS                                                 
#    RSGA62O  : NAME=RSGA62O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA62O /dev/null
# ******  FICHIER REPRIS DANS GEG00O                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG100 ${DATA}/PTEM/GSM00OBA.BSM030AO
# ******  IMPRESSION                                                           
       m_OutputAssign -c 9 -w ISM030 ISM030
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM030 
       JUMP_LABEL=GSM00OBB
       ;;
(GSM00OBB)
       m_CondExec 04,GE,GSM00OBA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   WAIT DE 10 MINUTES POUR L'ETAT CREE PRECEDAMENT ET LE TEMPS D'ETRE         
#   OFFLOADER DANS EOS ET AINSI SOULAGER LE SPOOL.                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00OBD PGM=HWAIT      ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00OBD
       ;;
(GSM00OBD)
       m_CondExec ${EXCAF},NE,YES 
       m_ProgramExec HWAIT "600"
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=GSM00OZB
       ;;
(GSM00OZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GSM00OZB.sysin
       m_UtilityExec
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=9999                                  
# ***********************************                                          
       JUMP_LABEL=GSM00OZC
       ;;
(GSM00OZC)
       m_CondExec 1,EQ,$[EX9999] 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GSM00OZC.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
