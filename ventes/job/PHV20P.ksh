#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV20P.ksh                       --- VERSION DU 09/10/2016 00:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPHV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/12/30 AT 15.34.56 BY BURTEC5                      
#    STANDARDS: P  JOBSET: PHV20P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#     PURGE DES HISTO DETAILS VENTES PAR MAG/ MOIS (RTHV20)                    
#               ET    HISTO DES PSE PAR MAG/MOIS   (RTHV26)                    
#                                                                              
#           GARDER 2 ANS + L'HISTO EN COURS                                    
#           ==> ON FORCE DANS BETDATC LE MOIS LU DANS FEXERCIC                 
#                  ET ON SOUSTRAIT NJOURS     LU DANS FCARTE                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV20PA
       ;;
(PHV20PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PHV20PAA
       ;;
(PHV20PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   HISTO DETAILS  VENTES  MAG/MOIS                                      
#    RSHV20   : NAME=RSHV20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV20 /dev/null
# *****   HISTO VENTES PSE MAG/MOIS                                            
#    RSHV26   : NAME=RSHV26,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV26 /dev/null
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 0731 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV20PAA
# *****   N� DU MOIS DE L'EXERCICE DARTY : 02                                  
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV200 
       JUMP_LABEL=PHV20PAB
       ;;
(PHV20PAB)
       m_CondExec 04,GE,PHV20PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTHV20                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PHV20PAD
       ;;
(PHV20PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 51 -g +1 SYSREC01 ${DATA}/PXX0/F07.HV20UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSHV20                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PHV20PR0                                     
#                  VERIFIER LE BACKOUT RPHV20P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV20PAJ
       ;;
(PHV20PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SYSREC01 ${DATA}/PXX0/F07.HV26UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSHV26                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PHV20PR1                                     
#                  VERIFIER LE BACKOUT RPHV20P ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV20PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV20PZA
       ;;
(PHV20PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV20PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
