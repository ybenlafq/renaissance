#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VE1A9O.ksh                       --- VERSION DU 08/10/2016 13:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVE1A9 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/11/16 AT 16.39.33 BY BURTECA                      
#    STANDARDS: P  JOBSET: VE1A9O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REMISE A ZERO DES GDG                                                       
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=VE1A9OA
       ;;
(VE1A9OA)
#
#VE1A9OAJ
#VE1A9OAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#VE1A9OAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON MONDAY    2009/11/16 AT 16.39.33 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: VE1A9O                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'ALT-VE900O'                            
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=VE1A9OAA
       ;;
(VE1A9OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT1 ${DATA}/PARG/SEM.VE900OF
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT2 ${DATA}/PARG/SEM.VE900OG
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT3 ${DATA}/PARG/SEM.VE900OH
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT4 ${DATA}/PARG/SEM.VE900OI
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT5 ${DATA}/PARG/SEM.VE900OK
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT6 ${DATA}/PARG/SEM.VE900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT7 ${DATA}/PARG/SEM.VE900OJ
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE1A9OAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VE1A9OAB
       ;;
(VE1A9OAB)
       m_CondExec 16,NE,VE1A9OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN : VE900O                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE1A9OAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
