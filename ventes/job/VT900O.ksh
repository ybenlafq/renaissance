#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VT900O.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POVT900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/05/28 AT 18.07.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: VT900O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGV02-05-06-08-10-11-14-15-20-23-27-41                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VT900OA
       ;;
(VT900OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VT900OAA
       ;;
(VT900OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PARG/SEM.VT900OA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VT900OAA
       m_ProgramExec IEFBR14 "RDAR,VT900O.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT900OAD
       ;;
(VT900OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VT900OA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VT900OAE
       ;;
(VT900OAE)
       m_CondExec 00,EQ,VT900OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTGV11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAG
       ;;
(VT900OAG)
       m_CondExec ${EXAAK},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
#    RSGV14O  : NAME=RSGV14O,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/VT900OAG.VT900O01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900OAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTGV11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAJ
       ;;
(VT900OAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/VT900OAJ.VT900O0
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900OAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FIC SUR NLIEU,NVENTE 4,10,A                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAM
       ;;
(VT900OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VT900OAG.VT900O01
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900OAM.VT900O02
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900OAN
       ;;
(VT900OAN)
       m_CondExec 00,EQ,VT900OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9000                                                               
#   PREPARATION FICHIER PURGE ET FICHIER DE LOAD RTGV11                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAQ
       ;;
(VT900OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#         TABLE RTGV11                                                         
#    RSGV11O  : NAME=RSGV11O,MODE=(U,N) - DYNAM=YES                            
# -X-VT900OR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGV11O /dev/null
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  RTGV11 TRI PAR LIEU ET NVENTE                                               
       m_FileAssign -d SHR -g ${G_A3} VT0011A ${DATA}/PTEM/VT900OAM.VT900O02
#  FICHIER POUR LE LOAD DE LA RTGV11                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 VT0011L ${DATA}/PARG/SEM.VT900OB
#  FICHIER DE PURGE DE LA RTGV11                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 VT0011P ${DATA}/PARG/SEM.VT900OC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9000 
       JUMP_LABEL=VT900OAR
       ;;
(VT900OAR)
       m_CondExec 04,GE,VT900OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTGV11                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAT
       ;;
(VT900OAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSGV11O  : NAME=RSGV11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PARG/SEM.VT900OB
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/VT900OAJ.VT900O0
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900OAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VT900O_VT900OAT_RTGV11.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT900OAU
       ;;
(VT900OAU)
       m_CondExec 04,GE,VT900OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT910                                                                
#  EPURATION DES TABLES RTGV10 RTGV11                                          
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VT900OAX
       ;;
(VT900OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ENTETES DE VENTES                                                 
#    RSGV10O  : NAME=RSGV10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RSGV11O  : NAME=RSGV11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FVT910 ${DATA}/PARG/SEM.VT900OD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT910 
       JUMP_LABEL=VT900OAY
       ;;
(VT900OAY)
       m_CondExec 04,GE,VT900OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DE PURGE POUR LE PASSER A UN LRECL = 20                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBA
       ;;
(VT900OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PARG/SEM.VT900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900OBA.VT900O03
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_52_3 52 PD 3
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_52_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900OBB
       ;;
(VT900OBB)
       m_CondExec 00,EQ,VT900OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BVT910 ET DU TRI PRECEDENT POUR CREER            
#  LE FICHIER VT900OE                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBD
       ;;
(VT900OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/VT900OBA.VT900O03
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PARG/SEM.VT900OD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PARG/SEM.VT900OE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900OBE
       ;;
(VT900OBE)
       m_CondExec 00,EQ,VT900OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9002                                                               
#  PURGE DE LA TABLE RTGV02 TABLE DES ADRESSES                                 
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBG
       ;;
(VT900OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENTES ADRESSES                                                   
#    RSGV02O  : NAME=RSGV02O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV02O /dev/null
#                                                                              
#    RSTL11O  : NAME=RSTL11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL11O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBG
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A9} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -t LSEQ -g +1 FVT020 ${DATA}/PARG/SEM.VT900OF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9002 
       JUMP_LABEL=VT900OBH
       ;;
(VT900OBH)
       m_CondExec 04,GE,VT900OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9005                                                               
#  PURGE DE LA TABLE RTGV05                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBJ
       ;;
(VT900OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV05O  : NAME=RSGV05O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV05O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBJ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A10} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -t LSEQ -g +1 FVT005 ${DATA}/PARG/SEM.VT900OG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9005 
       JUMP_LABEL=VT900OBK
       ;;
(VT900OBK)
       m_CondExec 04,GE,VT900OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9006                                                               
#  PURGE DE LA TABLE RTGV06                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBM
       ;;
(VT900OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV06O  : NAME=RSGV06O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV06O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBM
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A11} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 FVT006 ${DATA}/PARG/SEM.VT900OH
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9006 
       JUMP_LABEL=VT900OBN
       ;;
(VT900OBN)
       m_CondExec 04,GE,VT900OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9008                                                               
#  PURGE DE LA TABLE RTGV08                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBQ
       ;;
(VT900OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV08O  : NAME=RSGV08O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV08O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A12} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -t LSEQ -g +1 FVT008 ${DATA}/PARG/SEM.VT900OI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9008 
       JUMP_LABEL=VT900OBR
       ;;
(VT900OBR)
       m_CondExec 04,GE,VT900OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9014                                                               
#  PURGE DE LA TABLE DES REGLEMENTS                                            
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBT
       ;;
(VT900OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES REGLEMENTS                                                        
#    RSGV14O  : NAME=RSGV14O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBT
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A13} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -t LSEQ -g +1 FVT140 ${DATA}/PARG/SEM.VT900OJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9014 
       JUMP_LABEL=VT900OBU
       ;;
(VT900OBU)
       m_CondExec 04,GE,VT900OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9015                                                               
#  PURGE DE LA TABLE RTGV15                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VT900OBX
       ;;
(VT900OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES CARACTERISTIQUES SPECIFIQUES                                      
#    RSGV15O  : NAME=RSGV15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV15O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OBX
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A14} FVT900 ${DATA}/PARG/SEM.VT900OE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9015 
       JUMP_LABEL=VT900OBY
       ;;
(VT900OBY)
       m_CondExec 04,GE,VT900OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9023                                                               
#  PURGE DE LA TABLE RTGV23                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCA
       ;;
(VT900OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV23O  : NAME=RSGV23O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV23O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OCA
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A15} FVT900 ${DATA}/PARG/SEM.VT900OE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9023 
       JUMP_LABEL=VT900OCB
       ;;
(VT900OCB)
       m_CondExec 04,GE,VT900OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9010                                                               
#  PURGE DE LA TABLE RTGV23                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCD
       ;;
(VT900OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV10O  : NAME=RSGV10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OCD
#  FICHIER                                                                     
       m_FileAssign -d SHR -g ${G_A16} FVT900 ${DATA}/PARG/SEM.VT900OE
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FVT901 ${DATA}/PARG/SEM.VT900OK
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -t LSEQ -g +1 FVT100 ${DATA}/PARG/SEM.VT900OL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9010 
       JUMP_LABEL=VT900OCE
       ;;
(VT900OCE)
       m_CondExec 04,GE,VT900OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9027                                                               
#  PURGE DE LA TABLE DES AUTORISATIONS ENTREPOT                                
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCG
       ;;
(VT900OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES AUTORISATIONS ENTREPOT                                            
#    RSGV27O  : NAME=RSGV27O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV27O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OCG
#  FICHIER POUR LES PURGES DE LA TABLE RTGV27                                  
       m_FileAssign -d SHR -g ${G_A17} FVT901 ${DATA}/PARG/SEM.VT900OK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9027 
       JUMP_LABEL=VT900OCH
       ;;
(VT900OCH)
       m_CondExec 04,GE,VT900OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9041                                                               
#  PURGE DE LA TABLE RTGV14                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCJ
       ;;
(VT900OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES MOUCHARDS DE QUOTAS                                               
#    RSGV41O  : NAME=RSGV41O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV41O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OCJ
#  FICHIER FEPURE                                                              
       m_FileAssign -d SHR -g ${G_A18} FVT901 ${DATA}/PARG/SEM.VT900OK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9041 
       JUMP_LABEL=VT900OCK
       ;;
(VT900OCK)
       m_CondExec 04,GE,VT900OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER VT900OE                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCM
       ;;
(VT900OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PARG/SEM.VT900OE
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900OCM.VT900O04
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900OCN
       ;;
(VT900OCN)
       m_CondExec 00,EQ,VT900OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9020                                                               
#  PURGE DE LA TABLES HISTORIQUE ENTETE DE VENTES                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCQ
       ;;
(VT900OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV20O  : NAME=RSGV20O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV20O /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900OCQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A20} FVT900 ${DATA}/PTEM/VT900OCM.VT900O04
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9020 
       JUMP_LABEL=VT900OCR
       ;;
(VT900OCR)
       m_CondExec 04,GE,VT900OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE VT900OB POUR LOAD RTGV11 CAR PAS DE PLACE SUR VOLUMES S1P*           
#  REPRISE AU STEP : OUI                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900OCT PGM=IDCAMS     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=VT900OCT
       ;;
(VT900OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900OCT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VT900OCU
       ;;
(VT900OCU)
       m_CondExec 16,NE,VT900OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VT900OZA
       ;;
(VT900OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
