#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT10D.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDIVT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/06/14 AT 14.50.28 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT10D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    QUIESCE DU TABLESPACE RSIT15                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT10DA
       ;;
(IVT10DA)
#
#IVT10DAA
#IVT10DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT10DAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVT10DAD
       ;;
(IVT10DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLE / GENERALITES ARTICLES                                 
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLES DES FAMILLES                                                  
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK SOUS LIEU MAGASIN                                        
#    RSGS30   : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE LIEUX INVENTORIES                                              
#    RSIT05   : NAME=RSIT05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT05 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSIT10   : NAME=RSIT10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT10 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSFL90   : NAME=RSFL90D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  PARAMETRE SOCIETE 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES CODICS A INVENTORIER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 FIT010 ${DATA}/PXX0/F91.BIT010AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT010 
       JUMP_LABEL=IVT10DAE
       ;;
(IVT10DAE)
       m_CondExec 04,GE,IVT10DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTIT15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAG
       ;;
(IVT10DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -g +1 SYSREC01 ${DATA}/PGI991/F91.UNLOAD.IT15UD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10DAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR N�INVENTAIRE ; NLIEU ; NSOCIETE ; NSSLIEU ; NCODIC                  
#  OUTREC 1 ; 35                                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAJ
       ;;
(IVT10DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F91.BIT010AD
# ******  FICHIER DES CODICS POUR LOAD RTIT15                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PTEM/IVT10DAJ.BIT010GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_35 1 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_35
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10DAK
       ;;
(IVT10DAK)
       m_CondExec 00,EQ,IVT10DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI N�2                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAM
       ;;
(IVT10DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVT10DAJ.BIT010GD
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PGI991/F91.UNLOAD.IT15UD
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PGI991/F91.RELOAD.IT15RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_3 6 CH 3
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DAN
       ;;
(IVT10DAN)
       m_CondExec 00,EQ,IVT10DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTIT15                                                     
# ********************************************************************         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAQ
       ;;
(IVT10DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTIT15                                                
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGI991/F91.RELOAD.IT15RD
#                                                                              
#    RSIT15   : NAME=RSIT15D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10DAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVT10D_IVT10DAQ_RTIT15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVT10DAR
       ;;
(IVT10DAR)
       m_CondExec 04,GE,IVT10DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAT
       ;;
(IVT10DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F91.BIT010AD
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10DAT.BIT010BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_85_1 85 CH 1
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DAU
       ;;
(IVT10DAU)
       m_CondExec 00,EQ,IVT10DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT011 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DAX PGM=BIT011     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DAX
       ;;
(IVT10DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A6} FIT011 ${DATA}/PTEM/IVT10DAT.BIT010BD
       m_OutputAssign -c 9 -w BIT011 IIT011
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F91.BIT010CD
       m_ProgramExec BIT011 
# ********************************************************************         
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBA
       ;;
(IVT10DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F91.BIT010AD
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10DBA.BIT010DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "2"
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DBB
       ;;
(IVT10DBB)
       m_CondExec 00,EQ,IVT10DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT031 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBD PGM=BIT031     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBD
       ;;
(IVT10DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A8} FIT011 ${DATA}/PTEM/IVT10DBA.BIT010DD
       m_OutputAssign -c 9 -w BIT031 IIT031
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F91.BIT010ED
       m_ProgramExec BIT031 
# ********************************************************************         
#  BIT015  POUR TOUS LES CODICS A INVENTORIER DONT LA DATE DE                  
#          CONSTITUTUION DU STOCK THEORIQUE EST EGALE A LA DATE DE             
#          TRAITEMENT IL Y A CALCUL DU STOCK ET INSCRIPTION DANS RTIT1         
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBG
       ;;
(IVT10DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLES DES LIEUX                                                     
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  TABLE STOCK SOUS LIEU / MAGASINS                                     
#    RSGS30   : NAME=RSGS30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS60   : NAME=RSGS60D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT015 
       JUMP_LABEL=IVT10DBH
       ;;
(IVT10DBH)
       m_CondExec 04,GE,IVT10DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT035                                                                      
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBJ
       ;;
(IVT10DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE STOCKS                                                         
#    RSGS36   : NAME=RSGS36D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 991                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT035 
       JUMP_LABEL=IVT10DBK
       ;;
(IVT10DBK)
       m_CondExec 04,GE,IVT10DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT100  BUT: PRAPARATION DU FICHIER POUR EDITION DES ETIQUETTES             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBM
       ;;
(IVT10DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PTEM/IVT10DBM.BIT100AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT100 
       JUMP_LABEL=IVT10DBN
       ;;
(IVT10DBN)
       m_CondExec 04,GE,IVT10DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBQ
       ;;
(IVT10DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/IVT10DBM.BIT100AD
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/IVT10DBQ.BIT100BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_16_15 16 CH 15
 /FIELDS FLD_CH_1_1 01 CH 01
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DBR
       ;;
(IVT10DBR)
       m_CondExec 00,EQ,IVT10DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBT PGM=BIV105     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBT
       ;;
(IVT10DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES                                               
       m_FileAssign -d SHR -g ${G_A10} FIN105 ${DATA}/PTEM/IVT10DBQ.BIT100BD
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIT040  BUT: EDITION D'UN ETAT D INVENTAIRE COMLPLET                        
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DBX
       ;;
(IVT10DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA67   : NAME=RSGA67D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSEG90   : NAME=RSEG90D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG90 /dev/null
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IIT004 ${DATA}/PTEM/IVT10DBX.BIT040AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT040 
       JUMP_LABEL=IVT10DBY
       ;;
(IVT10DBY)
       m_CondExec 04,GE,IVT10DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIT040AD)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DCA
       ;;
(IVT10DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IVT10DBX.BIT040AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10DCA.BIT040BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_3 15 CH 03
 /FIELDS FLD_BI_44_2 44 CH 02
 /FIELDS FLD_BI_37_7 37 CH 07
 /FIELDS FLD_BI_7_5 7 CH 05
 /FIELDS FLD_BI_18_5 18 CH 05
 /FIELDS FLD_BI_23_5 23 CH 05
 /FIELDS FLD_BI_28_9 28 CH 09
 /FIELDS FLD_BI_12_3 12 CH 03
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING,
   FLD_BI_28_9 ASCENDING,
   FLD_BI_37_7 ASCENDING,
   FLD_BI_44_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DCB
       ;;
(IVT10DCB)
       m_CondExec 00,EQ,IVT10DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIT040CD                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DCD
       ;;
(IVT10DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/IVT10DCA.BIT040BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/IVT10DCD.BIT040CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IVT10DCE
       ;;
(IVT10DCE)
       m_CondExec 04,GE,IVT10DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DCG
       ;;
(IVT10DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/IVT10DCD.BIT040CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10DCG.BIT040DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10DCH
       ;;
(IVT10DCH)
       m_CondExec 00,EQ,IVT10DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIT004                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DCJ
       ;;
(IVT10DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/IVT10DCA.BIT040BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/IVT10DCG.BIT040DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIT004 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IVT10DCK
       ;;
(IVT10DCK)
       m_CondExec 04,GE,IVT10DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVT10DZA
       ;;
(IVT10DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
