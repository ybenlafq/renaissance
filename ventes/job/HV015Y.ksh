#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015Y.ksh                       --- VERSION DU 08/10/2016 17:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/01/25 AT 09.03.07 BY BURTEC2                      
#    STANDARDS: P  JOBSET: HV015Y                                              
# --------------------------------------------------------------------         
# **--USER='LYON'                                                              
# ********************************************************************         
#  QUIESCE DE TABLESPACES RSHV20Y RSHV25Y AVANT MAJ                            
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015YA
       ;;
(HV015YA)
#
#HV015YAA
#HV015YAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV015YAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       EXAAP=${EXAAP:-0}
       G_A2=${G_A2:-'+1'}
       EXAAU=${EXAAU:-0}
       G_A3=${G_A3:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=HV015YAD
       ;;
(HV015YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F45.BHV000AY
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/HV015YAD.BHV015AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_34_5 34 CH 5
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_48_5 48 CH 5
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015YAE
       ;;
(HV015YAE)
       m_CondExec 00,EQ,HV015YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV015  : GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE                  
#            DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                      
#  REPRISE :OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015YAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015YAG
       ;;
(HV015YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES (GENERALITES ARTICLES)                                      
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  FAMILLE (EDITION DES ETATS)                                          
#    RSGA11Y  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11Y /dev/null
# ******  RAYONS (COMPOSANTS DU RAYON)                                         
#    RSGA21Y  : NAME=RSGA21Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21Y /dev/null
# ******  QUOTAS DE LIVRAISON (POPULATION MARCHE/DONNEES DE BASE INSEE         
#    RSGQ01Y  : NAME=RSGQ01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01Y /dev/null
#                                                                              
# ******  FIC DES VENTES DU MOIS TRI�                                          
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PXX0/HV015YAD.BHV015AY
# ******  FIC DES STATS PAR MAGS/GRPE DE STAT/COMMUNE                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PXX0/HV015YAG.BHV015BY
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015YAH
       ;;
(HV015YAH)
       m_CondExec 04,GE,HV015YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SEQUENTIEL BHV015BY                                          
#  CODE RAYON  1,11 ; CODE COMMUNE  32,5                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015YAJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=HV015YAJ
       ;;
(HV015YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/HV015YAG.BHV015BY
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PXX0/HV015YAJ.BHV015CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_CH_32_5 32 CH 05
 /FIELDS FLD_PD_20_5 20 PD 5
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015YAK
       ;;
(HV015YAK)
       m_CondExec 00,EQ,HV015YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV020 : MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOIS             
#           ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                       
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE.FAIRE UN RECOVER DE RSHV20YRSHV25Y AVEC         
#           LA VALEUR DU RBA DU PREMIER STEP.UTILISEZ LA CHAINE DB2RBP         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015YAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015YAM
       ;;
(HV015YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES STATS PAR MAGS/GROUPE DE STATS/COMMUNE                       
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PXX0/HV015YAJ.BHV015CY
#                                                                              
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                              
#    RSHV20Y  : NAME=RSHV20Y,MODE=U - DYNAM=YES                                
# -X-RSHV20Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV20Y /dev/null
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                       
#    RSHV25Y  : NAME=RSHV25Y,MODE=U - DYNAM=YES                                
# -X-RSHV25Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV25Y /dev/null
#                                                                              
# ******  DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015YAN
       ;;
(HV015YAN)
       m_CondExec 04,GE,HV015YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=HV015YZA
       ;;
(HV015YZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
