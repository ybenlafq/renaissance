#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PP100P.ksh                       --- VERSION DU 17/10/2016 18:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPP100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/05/04 AT 16.39.51 BY BURTECA                      
#    STANDARDS: P  JOBSET: PP100P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM BPP100                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL REEL DES PIECES TLM DE LA NCG                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PP100PA
       ;;
(PP100PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PP100PAA
       ;;
(PP100PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ TABLE RTGA00                                                          
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTEX30   EXTRACTEUR                                             
#    RTEX30   : NAME=RSEX30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEX30 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP100 
       JUMP_LABEL=PP100PAB
       ;;
(PP100PAB)
       m_CondExec 04,GE,PP100PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP101                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL REEL DES PIECES ELA DE LA NCG                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAD
       ;;
(PP100PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTEX30   EXTRACTEUR                                             
#    RTEX30   : NAME=RSEX30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEX30 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP101 
       JUMP_LABEL=PP100PAE
       ;;
(PP100PAE)
       m_CondExec 04,GE,PP100PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AP ISSU DU BHV030 CHAINE STAT0P                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAG
       ;;
(PP100PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/PP100PAG.BPP102AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907"
 /DERIVEDFIELD CST_3_11 "VEN"
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_27_3 27 CH 3
 /CONDITION CND_2 FLD_CH_10_3 EQ CST_1_7 AND FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3
 /INCLUDE CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP100PAH
       ;;
(PP100PAH)
       m_CondExec 00,EQ,PP100PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP102                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL DES B.E TLM                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAJ
       ;;
(PP100PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ FICHIER FGS40 (ISSU DU TRI PRECEDANT)                                 
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PXX0/PP100PAG.BPP102AP
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP102 
       JUMP_LABEL=PP100PAK
       ;;
(PP100PAK)
       m_CondExec 04,GE,PP100PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AP ISSU DU BHV030 CHAINE STAT0P                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAM PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAM
       ;;
(PP100PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/PP100PAM.BPP103AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907"
 /DERIVEDFIELD CST_3_11 "VEN"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /CONDITION CND_2 FLD_CH_10_3 EQ CST_1_7 AND FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3
 /INCLUDE CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP100PAN
       ;;
(PP100PAN)
       m_CondExec 00,EQ,PP100PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP103                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL EN EMPORTE MAGASIN                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAQ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAQ
       ;;
(PP100PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ FICHIER FGS40 (ISSU DU TRI PRECEDANT)                                 
       m_FileAssign -d SHR -g ${G_A2} FGS40 ${DATA}/PXX0/PP100PAM.BPP103AP
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP103 
       JUMP_LABEL=PP100PAR
       ;;
(PP100PAR)
       m_CondExec 04,GE,PP100PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP030                                                                  
#  ----------                                                                  
#  MISE A JOUR DE LA PERIODE DE STAT TRAITEE                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100PAT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PAT
       ;;
(PP100PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE RTGA01                                                                
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP030 
       JUMP_LABEL=PP100PAU
       ;;
(PP100PAU)
       m_CondExec 04,GE,PP100PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=PP100PZA
       ;;
(PP100PZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PP100PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
