#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD2P.ksh                       --- VERSION DU 08/10/2016 13:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGNMD2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/21 AT 14.35.27 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD2P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF870 : MISE � JOUR DE CERTAINES SOUS TABLES DE LA RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD2PA
       ;;
(GNMD2PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A86=${G_A86:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GNMD2PAA
       ;;
(GNMD2PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF870 
       JUMP_LABEL=GNMD2PAB
       ;;
(GNMD2PAB)
       m_CondExec 04,GE,GNMD2PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF872 :  MAJ FLAG RTLI00 DES LIEUX MIGRES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAD
       ;;
(GNMD2PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF872 
       JUMP_LABEL=GNMD2PAE
       ;;
(GNMD2PAE)
       m_CondExec 04,GE,GNMD2PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF500 : EXTRACTION DE LA TABLE DES DEVISE RTFM04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAG
       ;;
(GNMD2PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES DEVISE                                                     
#    RSFM04   : NAME=RSFM04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM04 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF500 ${DATA}/PTEM/GNMD2PAG.BTF500AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF500 
       JUMP_LABEL=GNMD2PAH
       ;;
(GNMD2PAH)
       m_CondExec 04,GE,GNMD2PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF505 : EXTRACTION DE LA TABLE RTFM05                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAJ
       ;;
(GNMD2PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSFM05   : NAME=RSFM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF505 ${DATA}/PTEM/GNMD2PAJ.BTF505AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF505 
       JUMP_LABEL=GNMD2PAK
       ;;
(GNMD2PAK)
       m_CondExec 04,GE,GNMD2PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF598 : EXTRACTION DE LA TABLE RTGA00                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAM
       ;;
(GNMD2PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIS�E                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALIS�E                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE ANOMALIES                                                      
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF598 ${DATA}/PTEM/GNMD2PAM.BTF598AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF598 
       JUMP_LABEL=GNMD2PAN
       ;;
(GNMD2PAN)
       m_CondExec 04,GE,GNMD2PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF606 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAQ
       ;;
(GNMD2PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA06   : NAME=RSGA06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF606 ${DATA}/PTEM/GNMD2PAQ.BTF606AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF606 
       JUMP_LABEL=GNMD2PAR
       ;;
(GNMD2PAR)
       m_CondExec 04,GE,GNMD2PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF611 : CREATION DU FICHIER DE COMPARAISON POUR LES TABLES                 
#                    RTGA09, RTGA11, RTGA12 ET RTGA29                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAT
       ;;
(GNMD2PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA29 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS D'EXTRACTION DES ARTICLES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611A ${DATA}/PTEM/GNMD2PAT.BTF611AP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611B ${DATA}/PTEM/GNMD2PAT.BTF611BP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611C ${DATA}/PTEM/GNMD2PAT.BTF611CP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611D ${DATA}/PTEM/GNMD2PAT.BTF611DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF611 
       JUMP_LABEL=GNMD2PAU
       ;;
(GNMD2PAU)
       m_CondExec 04,GE,GNMD2PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF616 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PAX
       ;;
(GNMD2PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF616 ${DATA}/PTEM/GNMD2PAX.BTF616AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF616 
       JUMP_LABEL=GNMD2PAY
       ;;
(GNMD2PAY)
       m_CondExec 04,GE,GNMD2PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF620 : EXTRACTION DE LA SOUS TABLE NMDAT                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBA
       ;;
(GNMD2PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIE                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF620 ${DATA}/PTEM/GNMD2PBA.BTF620AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF620 
       JUMP_LABEL=GNMD2PBB
       ;;
(GNMD2PBB)
       m_CondExec 04,GE,GNMD2PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF625 : CREATION DU FICHIER DE POUR LA TABLE RTGA30                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBD
       ;;
(GNMD2PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF625 ${DATA}/PTEM/GNMD2PBD.BTF625AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF625 
       JUMP_LABEL=GNMD2PBE
       ;;
(GNMD2PBE)
       m_CondExec 04,GE,GNMD2PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF631 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA14            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBG
       ;;
(GNMD2PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF631 ${DATA}/PTEM/GNMD2PBG.BTF631AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF631 
       JUMP_LABEL=GNMD2PBH
       ;;
(GNMD2PBH)
       m_CondExec 04,GE,GNMD2PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF636 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBJ
       ;;
(GNMD2PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF636 ${DATA}/PTEM/GNMD2PBJ.BTF636AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF636 
       JUMP_LABEL=GNMD2PBK
       ;;
(GNMD2PBK)
       m_CondExec 04,GE,GNMD2PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF641 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBM
       ;;
(GNMD2PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF641 ${DATA}/PTEM/GNMD2PBM.BTF641AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF641 
       JUMP_LABEL=GNMD2PBN
       ;;
(GNMD2PBN)
       m_CondExec 04,GE,GNMD2PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF646 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA22            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBQ
       ;;
(GNMD2PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF646 ${DATA}/PTEM/GNMD2PBQ.BTF646AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF646 
       JUMP_LABEL=GNMD2PBR
       ;;
(GNMD2PBR)
       m_CondExec 04,GE,GNMD2PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF651 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA23            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBT
       ;;
(GNMD2PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA23   : NAME=RSGA23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA23 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF651 ${DATA}/PTEM/GNMD2PBT.BTF651AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF651 
       JUMP_LABEL=GNMD2PBU
       ;;
(GNMD2PBU)
       m_CondExec 04,GE,GNMD2PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF656 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA24            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PBX
       ;;
(GNMD2PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA24   : NAME=RSGA24,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA24 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF656 ${DATA}/PTEM/GNMD2PBX.BTF656AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF656 
       JUMP_LABEL=GNMD2PBY
       ;;
(GNMD2PBY)
       m_CondExec 04,GE,GNMD2PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF662 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCA
       ;;
(GNMD2PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF662 ${DATA}/PTEM/GNMD2PCA.BTF662AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF662 
       JUMP_LABEL=GNMD2PCB
       ;;
(GNMD2PCB)
       m_CondExec 04,GE,GNMD2PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF666 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCD
       ;;
(GNMD2PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA26 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF666 ${DATA}/PTEM/GNMD2PCD.BTF666AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF666 
       JUMP_LABEL=GNMD2PCE
       ;;
(GNMD2PCE)
       m_CondExec 04,GE,GNMD2PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF671 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA27            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCG
       ;;
(GNMD2PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA27   : NAME=RSGA27,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA27 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF671 ${DATA}/PTEM/GNMD2PCG.BTF671AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF671 
       JUMP_LABEL=GNMD2PCH
       ;;
(GNMD2PCH)
       m_CondExec 04,GE,GNMD2PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF682 : EXTRACTION DE LA TABLE DES GARANTIES RTGA40                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCJ
       ;;
(GNMD2PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE                                                                
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE                                                                
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE CODICS LIE                                                     
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE MONTANTS DES GARANTIES COMPLEMENTAIRES                         
#    RSGA40   : NAME=RSGA40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA40 /dev/null
# ******* TABLE ANO                                                            
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF682 ${DATA}/PTEM/GNMD2PCJ.BTF682AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF682 
       JUMP_LABEL=GNMD2PCK
       ;;
(GNMD2PCK)
       m_CondExec 04,GE,GNMD2PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF686 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCM
       ;;
(GNMD2PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA41   : NAME=RSGA41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF686 ${DATA}/PTEM/GNMD2PCM.BTF686AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF686 
       JUMP_LABEL=GNMD2PCN
       ;;
(GNMD2PCN)
       m_CondExec 04,GE,GNMD2PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF687 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA51            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCQ
       ;;
(GNMD2PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA51   : NAME=RSGA51,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF687 ${DATA}/PTEM/GNMD2PCQ.BTF687AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF687 
       JUMP_LABEL=GNMD2PCR
       ;;
(GNMD2PCR)
       m_CondExec 04,GE,GNMD2PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF692 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA52            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCT
       ;;
(GNMD2PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA52   : NAME=RSGA52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF692 ${DATA}/PTEM/GNMD2PCT.BTF692AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF692 
       JUMP_LABEL=GNMD2PCU
       ;;
(GNMD2PCU)
       m_CondExec 04,GE,GNMD2PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF702 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA58            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PCX
       ;;
(GNMD2PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES CODICS GROUPES                                             
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF702 ${DATA}/PTEM/GNMD2PCX.BTF702AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF702 
       JUMP_LABEL=GNMD2PCY
       ;;
(GNMD2PCY)
       m_CondExec 04,GE,GNMD2PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF707 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA64            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDA
       ;;
(GNMD2PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA64   : NAME=RSGA64,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF707 ${DATA}/PTEM/GNMD2PDA.BTF707AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF707 
       JUMP_LABEL=GNMD2PDB
       ;;
(GNMD2PDB)
       m_CondExec 04,GE,GNMD2PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF710 : EXTRACTION DE LA TABLE GQ12                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDD
       ;;
(GNMD2PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE MODE DE DELIVRANCE                                             
#    RSGQ12   : NAME=RSGQ12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF710 ${DATA}/PTEM/GNMD2PDD.BTF710AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF710 
       JUMP_LABEL=GNMD2PDE
       ;;
(GNMD2PDE)
       m_CondExec 04,GE,GNMD2PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF715 : EXTRACTION TABLE DES CODES VENDEURS RTGV31                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDG
       ;;
(GNMD2PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE CODES VENDEURS                                                 
#    RSGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF715 ${DATA}/PTEM/GNMD2PDG.BTF715AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF715 
       JUMP_LABEL=GNMD2PDH
       ;;
(GNMD2PDH)
       m_CondExec 04,GE,GNMD2PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF718 : EXTRACTION TABLE COMPTEUR SAV RTPA70                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDJ
       ;;
(GNMD2PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPA70   : NAME=RSPA70,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPA70 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF718 ${DATA}/PTEM/GNMD2PDJ.BTF718AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF718 
       JUMP_LABEL=GNMD2PDK
       ;;
(GNMD2PDK)
       m_CondExec 04,GE,GNMD2PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF720 : EXTRACTION TABLE MODE PMT / RBMT RTPM06                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDM
       ;;
(GNMD2PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPM06   : NAME=RSPM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF720 ${DATA}/PTEM/GNMD2PDM.BTF720AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF720 
       JUMP_LABEL=GNMD2PDN
       ;;
(GNMD2PDN)
       m_CondExec 04,GE,GNMD2PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF725 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPM35            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDQ
       ;;
(GNMD2PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PARAMETRES MQSERIES                                            
#    RSPM35   : NAME=RSPM35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM35 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF725 ${DATA}/PTEM/GNMD2PDQ.BTF725AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF725 
       JUMP_LABEL=GNMD2PDR
       ;;
(GNMD2PDR)
       m_CondExec 04,GE,GNMD2PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF751 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDT
       ;;
(GNMD2PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PRESTATION                                                     
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF751 ${DATA}/PTEM/GNMD2PDT.BTF751AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF751 
       JUMP_LABEL=GNMD2PDU
       ;;
(GNMD2PDU)
       m_CondExec 04,GE,GNMD2PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF755 : EXTRACTION TABLE LIEN FAMILLE/CODE PRESTATION RTPR01               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PDX
       ;;
(GNMD2PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN FAMILLE / CODE PRESTATION                                 
#    RSPR01   : NAME=RSPR01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF755 ${DATA}/PTEM/GNMD2PDX.BTF755AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF755 
       JUMP_LABEL=GNMD2PDY
       ;;
(GNMD2PDY)
       m_CondExec 04,GE,GNMD2PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF761 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEA
       ;;
(GNMD2PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR02   : NAME=RSPR02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF761 ${DATA}/PTEM/GNMD2PEA.BTF761AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF761 
       JUMP_LABEL=GNMD2PEB
       ;;
(GNMD2PEB)
       m_CondExec 04,GE,GNMD2PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF766 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PED
       ;;
(GNMD2PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSPR03   : NAME=RSPR03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR03 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF766 ${DATA}/PTEM/GNMD2PED.BTF766AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF766 
       JUMP_LABEL=GNMD2PEE
       ;;
(GNMD2PEE)
       m_CondExec 04,GE,GNMD2PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF770 : EXTRACTION TABLE LIEN PRESTATIONS RTPR04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEG
       ;;
(GNMD2PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN PRESTATION                                                
#    RSPR04   : NAME=RSPR04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR04 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF770 ${DATA}/PTEM/GNMD2PEG.BTF770AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF770 
       JUMP_LABEL=GNMD2PEH
       ;;
(GNMD2PEH)
       m_CondExec 04,GE,GNMD2PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF777 : EXTRACT TABLE PRIX PRESTATION LIE A LA ZONE DE PRIX RTPR10         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEJ
       ;;
(GNMD2PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR06 /dev/null
# ******* TABLE PRIX PRESTATION LIE A ZONE DE PRIX                             
#    RSPR10   : NAME=RSPR10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF777 ${DATA}/PTEM/GNMD2PEJ.BTF777AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF777 
       JUMP_LABEL=GNMD2PEK
       ;;
(GNMD2PEK)
       m_CondExec 04,GE,GNMD2PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF782 : EXTRACTION TABLE PRIX PRESTATION LIE AU CODIC RTPR14               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEM
       ;;
(GNMD2PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR06 /dev/null
# ******* TABLE PRIX PRESTATION LIE A ARTICLE                                  
#    RSPR14   : NAME=RSPR14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR14 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF782 ${DATA}/PTEM/GNMD2PEM.BTF782AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF782 
       JUMP_LABEL=GNMD2PEN
       ;;
(GNMD2PEN)
       m_CondExec 04,GE,GNMD2PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF783 : EXTRACTION DE LA TABLE RTPR53                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEQ
       ;;
(GNMD2PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR53 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF783 ${DATA}/PTEM/GNMD2PEQ.BTF783AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF783 
       JUMP_LABEL=GNMD2PER
       ;;
(GNMD2PER)
       m_CondExec 04,GE,GNMD2PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF784 : EXTRACTION DE LA TABLE RTGA31                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PET
       ;;
(GNMD2PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA31   : NAME=RSGA31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA31 /dev/null
# ******** PARAMETRE SOCIETE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******** FICHIER EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF784 ${DATA}/PTEM/GNMD2PET.BTF784AP
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF784 
       JUMP_LABEL=GNMD2PEU
       ;;
(GNMD2PEU)
       m_CondExec 04,GE,GNMD2PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF785 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PEX
       ;;
(GNMD2PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF785 ${DATA}/PTEM/GNMD2PEX.BTF785AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF785 
       JUMP_LABEL=GNMD2PEY
       ;;
(GNMD2PEY)
       m_CondExec 04,GE,GNMD2PEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF786 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFA
       ;;
(GNMD2PFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF786 ${DATA}/PTEM/GNMD2PFA.BTF786AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF786 
       JUMP_LABEL=GNMD2PFB
       ;;
(GNMD2PFB)
       m_CondExec 04,GE,GNMD2PFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF787 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFD
       ;;
(GNMD2PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ****** FICHIER D'EXTRACTION DES ARTICLES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF787 ${DATA}/PTEM/GNMD2PFD.BTF787AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF787 
       JUMP_LABEL=GNMD2PFE
       ;;
(GNMD2PFE)
       m_CondExec 04,GE,GNMD2PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF788 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFG PGM=IKJEFT01   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFG
       ;;
(GNMD2PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF788 ${DATA}/PTEM/GNMD2PFG.BTF788AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF788 
       JUMP_LABEL=GNMD2PFH
       ;;
(GNMD2PFH)
       m_CondExec 04,GE,GNMD2PFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF802 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTIP02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFJ
       ;;
(GNMD2PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FPV212 ${DATA}/PXX0/F07.BPV212AP
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF802 ${DATA}/PXX0/F07.BTF802AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF802 
       JUMP_LABEL=GNMD2PFK
       ;;
(GNMD2PFK)
       m_CondExec 04,GE,GNMD2PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF805 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA91            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFM
       ;;
(GNMD2PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES                                                            
#    RSGA91   : NAME=RSGA91,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA91 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF805 ${DATA}/PTEM/GNMD2PFM.BTF805AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF805 
       JUMP_LABEL=GNMD2PFN
       ;;
(GNMD2PFN)
       m_CondExec 04,GE,GNMD2PFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF810 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA92            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFQ
       ;;
(GNMD2PFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA92   : NAME=RSGA92,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA92 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF810 ${DATA}/PTEM/GNMD2PFQ.BTF810AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF810 
       JUMP_LABEL=GNMD2PFR
       ;;
(GNMD2PFR)
       m_CondExec 04,GE,GNMD2PFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF815 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA93            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFT
       ;;
(GNMD2PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA93   : NAME=RSGA93,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA93 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF815 ${DATA}/PTEM/GNMD2PFT.BTF815AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF815 
       JUMP_LABEL=GNMD2PFU
       ;;
(GNMD2PFU)
       m_CondExec 04,GE,GNMD2PFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#  BTF794 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PFX
       ;;
(GNMD2PFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF794 ${DATA}/PTEM/GNMD2PFX.BTF794AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF794 
       JUMP_LABEL=GNMD2PFY
       ;;
(GNMD2PFY)
       m_CondExec 04,GE,GNMD2PFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF795 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGA PGM=IKJEFT01   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGA
       ;;
(GNMD2PGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF795 ${DATA}/PTEM/GNMD2PGA.BTF795AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF795 
       JUMP_LABEL=GNMD2PGB
       ;;
(GNMD2PGB)
       m_CondExec 04,GE,GNMD2PGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF796 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGD
       ;;
(GNMD2PGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF796 ${DATA}/PTEM/GNMD2PGD.BTF796AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF796 
       JUMP_LABEL=GNMD2PGE
       ;;
(GNMD2PGE)
       m_CondExec 04,GE,GNMD2PGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF797 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGG
       ;;
(GNMD2PGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF797 ${DATA}/PTEM/GNMD2PGG.BTF797AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF797 
       JUMP_LABEL=GNMD2PGH
       ;;
(GNMD2PGH)
       m_CondExec 04,GE,GNMD2PGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF798 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGJ PGM=IKJEFT01   ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGJ
       ;;
(GNMD2PGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS25   : NAME=RSBS25P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS25 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF798 ${DATA}/PTEM/GNMD2PGJ.BTF798AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF798 
       JUMP_LABEL=GNMD2PGK
       ;;
(GNMD2PGK)
       m_CondExec 04,GE,GNMD2PGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF799 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGM PGM=IKJEFT01   ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGM
       ;;
(GNMD2PGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS26   : NAME=RSBS26P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS26 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF799 ${DATA}/PTEM/GNMD2PGM.BTF799AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF799 
       JUMP_LABEL=GNMD2PGN
       ;;
(GNMD2PGN)
       m_CondExec 04,GE,GNMD2PGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF820 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA12            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGQ
       ;;
(GNMD2PGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR12   : NAME=RSPR12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF820 ${DATA}/PTEM/GNMD2PGQ.BTF820AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF820 
       JUMP_LABEL=GNMD2PGR
       ;;
(GNMD2PGR)
       m_CondExec 04,GE,GNMD2PGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF825 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGT PGM=IKJEFT01   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGT
       ;;
(GNMD2PGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR16   : NAME=RSPR16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF825 ${DATA}/PTEM/GNMD2PGT.BTF825AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF825 
       JUMP_LABEL=GNMD2PGU
       ;;
(GNMD2PGU)
       m_CondExec 04,GE,GNMD2PGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF835 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA56            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PGX
       ;;
(GNMD2PGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA56   : NAME=RSGA56,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA56 /dev/null
# ******* TABLE                                                                
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* TABLE                                                                
#    RSGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA66 /dev/null
# ******* TABLE                                                                
#    RSGA68   : NAME=RSGA68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA68 /dev/null
# ******* TABLE                                                                
#    RSGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* TABLE                                                                
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF835 ${DATA}/PTEM/GNMD2PGX.BTF835AP
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2PGX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF835 
       JUMP_LABEL=GNMD2PGY
       ;;
(GNMD2PGY)
       m_CondExec 04,GE,GNMD2PGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF836 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHA PGM=IKJEFT01   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHA
       ;;
(GNMD2PHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE                                                                
#    RSGQ01   : NAME=RSGQ01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF836 ${DATA}/PTEM/GNMD2PHA.BTF836AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF836 
       JUMP_LABEL=GNMD2PHB
       ;;
(GNMD2PHB)
       m_CondExec 04,GE,GNMD2PHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF837 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHD PGM=IKJEFT01   ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHD
       ;;
(GNMD2PHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGQ06   : NAME=RSGQ06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF837 ${DATA}/PTEM/GNMD2PHD.BTF837AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF837 
       JUMP_LABEL=GNMD2PHE
       ;;
(GNMD2PHE)
       m_CondExec 04,GE,GNMD2PHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF838 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHG PGM=IKJEFT01   ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHG
       ;;
(GNMD2PHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE STOCK LOCAUX                                                   
#    RSSL01   : NAME=RSSL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF838 ${DATA}/PTEM/GNMD2PHG.BTF838AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF838 
       JUMP_LABEL=GNMD2PHH
       ;;
(GNMD2PHH)
       m_CondExec 04,GE,GNMD2PHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF839 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHJ PGM=IKJEFT01   ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHJ
       ;;
(GNMD2PHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSSL16   : NAME=RSSL16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF839 ${DATA}/PTEM/GNMD2PHJ.BTF839AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF839 
       JUMP_LABEL=GNMD2PHK
       ;;
(GNMD2PHK)
       m_CondExec 04,GE,GNMD2PHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF840 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHM PGM=IKJEFT01   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHM
       ;;
(GNMD2PHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGG40   : NAME=RSGG40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF840 ${DATA}/PTEM/GNMD2PHM.BTF840AP
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 HAD840 ${DATA}/PXX0/F07.BTF840BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF840 
       JUMP_LABEL=GNMD2PHN
       ;;
(GNMD2PHN)
       m_CondExec 04,GE,GNMD2PHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BTF841 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTEE03           
# * REPRISE: OUI                                                               
# ********************************************************************         
# ALT      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** TABLE CODES POSTAUX                                                 
# RSEE03   FILE  DYNAM=YES,NAME=RSEE03,MODE=I                                  
# ******** PARAMETRE SOCIETE                                                   
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ******** PARAMETRE FDATE                                                     
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** FICHIER D'EXTRACTION                                                
# FTF841   FILE  NAME=BTF841AP,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BTF841) PLAN(BTF841)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BTF842 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGB05            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHQ PGM=IKJEFT01   ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHQ
       ;;
(GNMD2PHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF842 ${DATA}/PTEM/GNMD2PHQ.BTF842AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF842 
       JUMP_LABEL=GNMD2PHR
       ;;
(GNMD2PHR)
       m_CondExec 04,GE,GNMD2PHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF843 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA54            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHT PGM=IKJEFT01   ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHT
       ;;
(GNMD2PHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF843 ${DATA}/PTEM/GNMD2PHT.BTF843AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF843 
       JUMP_LABEL=GNMD2PHU
       ;;
(GNMD2PHU)
       m_CondExec 04,GE,GNMD2PHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF844 : CREATION DU FICHIER DE COMPARAISON POUR GV21/22/35/11 TL02         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PHX PGM=IKJEFT01   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PHX
       ;;
(GNMD2PHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#    RSGV35   : NAME=RSGV35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF844 ${DATA}/PTEM/GNMD2PHX.BTF844AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF844 
       JUMP_LABEL=GNMD2PHY
       ;;
(GNMD2PHY)
       m_CondExec 04,GE,GNMD2PHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF845 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA53            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIA PGM=IKJEFT01   ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIA
       ;;
(GNMD2PIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF845 ${DATA}/PTEM/GNMD2PIA.BTF845AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF845 
       JUMP_LABEL=GNMD2PIB
       ;;
(GNMD2PIB)
       m_CondExec 04,GE,GNMD2PIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF846 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PID PGM=IKJEFT01   ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PID
       ;;
(GNMD2PID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL40   : NAME=RSSL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF846 ${DATA}/PTEM/GNMD2PID.BTF846AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF846 
       JUMP_LABEL=GNMD2PIE
       ;;
(GNMD2PIE)
       m_CondExec 04,GE,GNMD2PID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF847 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIG PGM=IKJEFT01   ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIG
       ;;
(GNMD2PIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL41   : NAME=RSSL41,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL41 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF847 ${DATA}/PTEM/GNMD2PIG.BTF847AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF847 
       JUMP_LABEL=GNMD2PIH
       ;;
(GNMD2PIH)
       m_CondExec 04,GE,GNMD2PIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF848 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIJ PGM=IKJEFT01   ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIJ
       ;;
(GNMD2PIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE STOCK LOCAL - MODE I (POUR EVITER SERIALIS.DES FILIALE         
#    RSSL10   : NAME=RSSL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF848 ${DATA}/PTEM/GNMD2PIJ.BTF848AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF848 
       JUMP_LABEL=GNMD2PIK
       ;;
(GNMD2PIK)
       m_CondExec 04,GE,GNMD2PIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF849 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTYF00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIM PGM=IKJEFT01   ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIM
       ;;
(GNMD2PIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE PARIS                                                          
#    RSYF00   : NAME=RSYF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSYF00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES CODES VENDEURS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF849 ${DATA}/PTEM/GNMD2PIM.BTF849AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF849 
       JUMP_LABEL=GNMD2PIN
       ;;
(GNMD2PIN)
       m_CondExec 04,GE,GNMD2PIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF860 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLI00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIQ PGM=IKJEFT01   ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIQ
       ;;
(GNMD2PIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF860 ${DATA}/PTEM/GNMD2PIQ.BTF860AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF860 
       JUMP_LABEL=GNMD2PIR
       ;;
(GNMD2PIR)
       m_CondExec 04,GE,GNMD2PIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF862 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLG09            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIT PGM=IKJEFT01   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIT
       ;;
(GNMD2PIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLG09   : NAME=RSLG09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLG09 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF862 ${DATA}/PTEM/GNMD2PIT.BTF862AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF862 
       JUMP_LABEL=GNMD2PIU
       ;;
(GNMD2PIU)
       m_CondExec 04,GE,GNMD2PIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COLLER : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA59            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PIX PGM=IKJEFT01   ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PIX
       ;;
(GNMD2PIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF863 ${DATA}/PTEM/GNMD2PIX.BTF863AP
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2PIX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF863 
       JUMP_LABEL=GNMD2PIY
       ;;
(GNMD2PIY)
       m_CondExec 04,GE,GNMD2PIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF864 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJA PGM=IKJEFT01   ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJA
       ;;
(GNMD2PJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGG20   : NAME=RSGG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF864 ${DATA}/PTEM/GNMD2PJA.BTF864AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF864 
       JUMP_LABEL=GNMD2PJB
       ;;
(GNMD2PJB)
       m_CondExec 04,GE,GNMD2PJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF865 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJD PGM=IKJEFT01   ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJD
       ;;
(GNMD2PJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE POUR LA CONTRIBUTION RECYCLAGE D3E                             
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF865 ${DATA}/PTEM/GNMD2PJD.BTF865AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF865 
       JUMP_LABEL=GNMD2PJE
       ;;
(GNMD2PJE)
       m_CondExec 04,GE,GNMD2PJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF875 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA82            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJG PGM=IKJEFT01   ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJG
       ;;
(GNMD2PJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA82   : NAME=RSGA82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA82 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF875 ${DATA}/PTEM/GNMD2PJG.BTF875AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF875 
       JUMP_LABEL=GNMD2PJH
       ;;
(GNMD2PJH)
       m_CondExec 04,GE,GNMD2PJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF877 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJJ PGM=IKJEFT01   ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJJ
       ;;
(GNMD2PJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE SOCIETE                                                    
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877A ${DATA}/PTEM/GNMD2PJJ.BTF877AP
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877B ${DATA}/PTEM/GNMD2PJJ.BTF877BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF877 
       JUMP_LABEL=GNMD2PJK
       ;;
(GNMD2PJK)
       m_CondExec 04,GE,GNMD2PJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF878 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL17            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJM PGM=IKJEFT01   ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJM
       ;;
(GNMD2PJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE SOCIETE                                                    
#    RSSL17   : NAME=RSSL17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL17 /dev/null
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF878 ${DATA}/PTEM/GNMD2PJM.BTF878AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF878 
       JUMP_LABEL=GNMD2PJN
       ;;
(GNMD2PJN)
       m_CondExec 04,GE,GNMD2PJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJQ
       ;;
(GNMD2PJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GNMD2PAG.BTF500AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GNMD2PAJ.BTF505AP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GNMD2PAM.BTF598AP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GNMD2PAQ.BTF606AP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GNMD2PAT.BTF611AP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GNMD2PAT.BTF611BP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GNMD2PAT.BTF611CP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GNMD2PAT.BTF611DP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GNMD2PAX.BTF616AP
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GNMD2PBA.BTF620AP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GNMD2PBD.BTF625AP
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GNMD2PBG.BTF631AP
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GNMD2PBJ.BTF636AP
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GNMD2PBM.BTF641AP
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GNMD2PBQ.BTF646AP
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/GNMD2PBT.BTF651AP
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GNMD2PBX.BTF656AP
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GNMD2PCA.BTF662AP
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GNMD2PCD.BTF666AP
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GNMD2PCG.BTF671AP
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GNMD2PCJ.BTF682AP
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GNMD2PCM.BTF686AP
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GNMD2PCQ.BTF687AP
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PTEM/GNMD2PCT.BTF692AP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GNMD2PCX.BTF702AP
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GNMD2PDA.BTF707AP
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GNMD2PDD.BTF710AP
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GNMD2PDG.BTF715AP
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GNMD2PDJ.BTF718AP
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GNMD2PDM.BTF720AP
       m_FileAssign -d SHR -g ${G_A31} -C ${DATA}/PTEM/GNMD2PDQ.BTF725AP
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GNMD2PDT.BTF751AP
       m_FileAssign -d SHR -g ${G_A33} -C ${DATA}/PTEM/GNMD2PDX.BTF755AP
       m_FileAssign -d SHR -g ${G_A34} -C ${DATA}/PTEM/GNMD2PEA.BTF761AP
       m_FileAssign -d SHR -g ${G_A35} -C ${DATA}/PTEM/GNMD2PED.BTF766AP
       m_FileAssign -d SHR -g ${G_A36} -C ${DATA}/PTEM/GNMD2PEG.BTF770AP
       m_FileAssign -d SHR -g ${G_A37} -C ${DATA}/PTEM/GNMD2PEJ.BTF777AP
       m_FileAssign -d SHR -g ${G_A38} -C ${DATA}/PTEM/GNMD2PEM.BTF782AP
       m_FileAssign -d SHR -g ${G_A39} -C ${DATA}/PTEM/GNMD2PEQ.BTF783AP
       m_FileAssign -d SHR -g ${G_A40} -C ${DATA}/PTEM/GNMD2PET.BTF784AP
       m_FileAssign -d SHR -g ${G_A41} -C ${DATA}/PTEM/GNMD2PEX.BTF785AP
       m_FileAssign -d SHR -g ${G_A42} -C ${DATA}/PTEM/GNMD2PFA.BTF786AP
       m_FileAssign -d SHR -g ${G_A43} -C ${DATA}/PTEM/GNMD2PFD.BTF787AP
       m_FileAssign -d SHR -g ${G_A44} -C ${DATA}/PTEM/GNMD2PFG.BTF788AP
       m_FileAssign -d SHR -g ${G_A45} -C ${DATA}/PTEM/GNMD2PFX.BTF794AP
       m_FileAssign -d SHR -g ${G_A46} -C ${DATA}/PTEM/GNMD2PGA.BTF795AP
       m_FileAssign -d SHR -g ${G_A47} -C ${DATA}/PTEM/GNMD2PGD.BTF796AP
       m_FileAssign -d SHR -g ${G_A48} -C ${DATA}/PTEM/GNMD2PGG.BTF797AP
       m_FileAssign -d SHR -g ${G_A49} -C ${DATA}/PTEM/GNMD2PGJ.BTF798AP
       m_FileAssign -d SHR -g ${G_A50} -C ${DATA}/PTEM/GNMD2PGM.BTF799AP
       m_FileAssign -d SHR -g ${G_A51} -C ${DATA}/PTEM/GNMD2PFM.BTF805AP
       m_FileAssign -d SHR -g ${G_A52} -C ${DATA}/PTEM/GNMD2PFQ.BTF810AP
       m_FileAssign -d SHR -g ${G_A53} -C ${DATA}/PTEM/GNMD2PFT.BTF815AP
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/GNMD2PGQ.BTF820AP
       m_FileAssign -d SHR -g ${G_A55} -C ${DATA}/PTEM/GNMD2PGT.BTF825AP
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/GNMD2PGX.BTF835AP
       m_FileAssign -d SHR -g ${G_A57} -C ${DATA}/PTEM/GNMD2PHA.BTF836AP
       m_FileAssign -d SHR -g ${G_A58} -C ${DATA}/PTEM/GNMD2PHD.BTF837AP
       m_FileAssign -d SHR -g ${G_A59} -C ${DATA}/PTEM/GNMD2PHG.BTF838AP
       m_FileAssign -d SHR -g ${G_A60} -C ${DATA}/PTEM/GNMD2PHJ.BTF839AP
       m_FileAssign -d SHR -g ${G_A61} -C ${DATA}/PTEM/GNMD2PHM.BTF840AP
#         FILE  NAME=BTF841AP,MODE=I                                           
       m_FileAssign -d SHR -g ${G_A62} -C ${DATA}/PTEM/GNMD2PHQ.BTF842AP
       m_FileAssign -d SHR -g ${G_A63} -C ${DATA}/PTEM/GNMD2PHT.BTF843AP
       m_FileAssign -d SHR -g ${G_A64} -C ${DATA}/PTEM/GNMD2PHX.BTF844AP
       m_FileAssign -d SHR -g ${G_A65} -C ${DATA}/PTEM/GNMD2PIA.BTF845AP
       m_FileAssign -d SHR -g ${G_A66} -C ${DATA}/PTEM/GNMD2PID.BTF846AP
       m_FileAssign -d SHR -g ${G_A67} -C ${DATA}/PTEM/GNMD2PIG.BTF847AP
       m_FileAssign -d SHR -g ${G_A68} -C ${DATA}/PTEM/GNMD2PIJ.BTF848AP
       m_FileAssign -d SHR -g ${G_A69} -C ${DATA}/PTEM/GNMD2PIM.BTF849AP
       m_FileAssign -d SHR -g ${G_A70} -C ${DATA}/PTEM/GNMD2PIQ.BTF860AP
       m_FileAssign -d SHR -g ${G_A71} -C ${DATA}/PTEM/GNMD2PIT.BTF862AP
       m_FileAssign -d SHR -g ${G_A72} -C ${DATA}/PTEM/GNMD2PIX.BTF863AP
       m_FileAssign -d SHR -g ${G_A73} -C ${DATA}/PTEM/GNMD2PJA.BTF864AP
       m_FileAssign -d SHR -g ${G_A74} -C ${DATA}/PTEM/GNMD2PJD.BTF865AP
       m_FileAssign -d SHR -g ${G_A75} -C ${DATA}/PTEM/GNMD2PJG.BTF875AP
       m_FileAssign -d SHR -g ${G_A76} -C ${DATA}/PTEM/GNMD2PJJ.BTF877AP
       m_FileAssign -d SHR -g ${G_A77} -C ${DATA}/PTEM/GNMD2PJJ.BTF877BP
       m_FileAssign -d SHR -g ${G_A78} -C ${DATA}/PTEM/GNMD2PJM.BTF878AP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2PJQ.BTF001JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2PJR
       ;;
(GNMD2PJR)
       m_CondExec 00,EQ,GNMD2PJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF001AP     NOUVEAU POUR EXCLURE LES ENRS RTXM0            
#   SUR LUI-MEME                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJT
       ;;
(GNMD2PJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P907/SEM.BTF001AP
# ****    MODIF                                                                
       m_FileAssign -d SHR -g +0 SORTOUT ${DATA}/P907/SEM.BTF001AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "X-RTGA59"
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /FIELDS FLD_CH_1_385 1 CH 385
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_1_8 1 CH 8
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_8 EQ CST_3_8 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2PJU
       ;;
(GNMD2PJU)
       m_CondExec 00,EQ,GNMD2PJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PJX PGM=BTF900     ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PJX
       ;;
(GNMD2PJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A79} FTF600 ${DATA}/PTEM/GNMD2PJQ.BTF001JP
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/P907/SEM.BTF001AP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PXX0/F07.BTF002AP
       m_ProgramExec BTF900 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER BTF001X      NOUVEAUTE                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKA
       ;;
(GNMD2PKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A80} SORTIN ${DATA}/PTEM/GNMD2PJQ.BTF001JP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2PKA.BTF901AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "RTPR12"
 /DERIVEDFIELD CST_1_5 "RTPR10"
 /DERIVEDFIELD CST_5_13 "RTPR14"
 /FIELDS FLD_CH_25_8 25 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_5 OR FLD_CH_1_6 EQ CST_3_9 OR FLD_CH_1_6 EQ CST_5_13 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_25_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2PKB
       ;;
(GNMD2PKB)
       m_CondExec 00,EQ,GNMD2PKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF901 :                                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKD PGM=IKJEFT01   ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKD
       ;;
(GNMD2PKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A81} FTF600 ${DATA}/PTEM/GNMD2PKA.BTF901AP
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/GNMD2PKD.BTF901BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF901 
       JUMP_LABEL=GNMD2PKE
       ;;
(GNMD2PKE)
       m_CondExec 04,GE,GNMD2PKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF901                                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKG
       ;;
(GNMD2PKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A82} SORTIN ${DATA}/PTEM/GNMD2PKD.BTF901BP
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BTF901CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2PKH
       ;;
(GNMD2PKH)
       m_CondExec 00,EQ,GNMD2PKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ910 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKJ PGM=IKJEFT01   ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKJ
       ;;
(GNMD2PKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******                                                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******                                                                       
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A83} FMQ910 ${DATA}/PXX0/F07.BTF802AP
       m_FileAssign -d SHR -g ${G_A84} -C ${DATA}/PXX0/F07.BTF002AP
       m_FileAssign -d SHR -g ${G_A85} -C ${DATA}/PXX0/F07.BTF901CP
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/GNMD2P1N
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2PKJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ910 
       JUMP_LABEL=GNMD2PKK
       ;;
(GNMD2PKK)
       m_CondExec 04,GE,GNMD2PKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DU FICHIER SEQUENTIEL POUR LIBERER DE LA PLACE                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKM PGM=IDCAMS     ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKM
       ;;
(GNMD2PKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2PKM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2PKN
       ;;
(GNMD2PKN)
       m_CondExec 16,NE,GNMD2PKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER BTF001JP POUR COMPARAISON DU LENDEMAIN                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2PKQ PGM=IEBGENER   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PKQ
       ;;
(GNMD2PKQ)
       m_CondExec ${EXAQJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A86} SYSUT1 ${DATA}/PTEM/GNMD2PJQ.BTF001JP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SYSUT2 ${DATA}/P907/SEM.BTF001AP
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2PKR
       ;;
(GNMD2PKR)
       m_CondExec 00,EQ,GNMD2PKQ ${EXAQJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2PZA
       ;;
(GNMD2PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
