#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV201Y.ksh                       --- VERSION DU 08/10/2016 13:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYAV201 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/11/03 AT 14.19.55 BY BURTECA                      
#    STANDARDS: P  JOBSET: AV201Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BAV201 **  COBOL2/DB2  �PURATION DES AVOIRS P�RIM�S                     
#  REPRISE: OUI               ETAT IAV006                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AV201YA
       ;;
(AV201YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=AV201YAA
       ;;
(AV201YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******* SOUS TABLES G�N�RALIS�ES                                             
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES AVOIRS EN M.A.J                                            
#    RSAV01   : NAME=RSAV01Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV01 /dev/null
# ******* TABLE DES AVOIRS EN LECTURE                                          
#    RSAV02   : NAME=RSAV02Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV02 /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* TABLE GCT                                                            
#    RSFT60   : NAME=RSFT60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT60 /dev/null
# ******* TABLE GCT                                                            
#    RSFM20   : NAME=RSFM20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20 /dev/null
# ******* TABLE FX                                                             
#    RSFX00   : NAME=RSFX00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* FIC POUR GENERATEUR D'ETAT (ETAT IAV006)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IAV006 ${DATA}/PNCGY/F45.BAV201AY
# ******* FIC AVOIRS COMMERCIAUX PERIM�S POUR COMPTA GCT                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PXX0/F45.AV201Y
# ***********  PARAMETRE DATE                                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *                                                                            
# **************W*A*R*N*I*N*G*****************************************         
# *C EST ICI QUE L ON INDIQUE SI LE TRAITEMANT EST EN SIMUL OU NON             
# *SI SIMUL METTRE SIMU . SI REEL METTRE REEL                                  
# **************W*A*R*N*I*N*G*****************************************         
# *                                                                            
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/AV201YAA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV201 
       JUMP_LABEL=AV201YAB
       ;;
(AV201YAB)
       m_CondExec 04,GE,AV201YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
