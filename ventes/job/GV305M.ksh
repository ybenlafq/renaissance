#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV305M.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGV305 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/01/02 AT 16.39.47 BY PREPA2                       
#    STANDARDS: P  JOBSET: GV305M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='METZ'                                                              
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV305MA
       ;;
(GV305MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV305MAA
       ;;
(GV305MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSAN00M  : NAME=RSAN00M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00M /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV305MAA
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/GV305MAA.BEX111GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV305MAB
       ;;
(GV305MAB)
       m_CondExec 04,GE,GV305MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAD
       ;;
(GV305MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV305MAA.BEX111GM
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/GV305MAD.BEX111HM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305MAE
       ;;
(GV305MAE)
       m_CondExec 00,EQ,GV305MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BGV305  EXTRACTION DES VENTES DES 4 DERNIERES SEMAINES               
#       RTHV04 SELON LA SEGMENTATION PRESENTE DANS LE FICHIER FEX001           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAG
       ;;
(GV305MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
#                                                                              
#    RSHV04M  : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04M /dev/null
#                                                                              
# ***CARTE SOCIETE                                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PTEM/GV305MAD.BEX111HM
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 FGV305 ${DATA}/PTEM/GV305MAG.BGV305GM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV305 
       JUMP_LABEL=GV305MAH
       ;;
(GV305MAH)
       m_CondExec 04,GE,GV305MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR MAGASIN                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAJ
       ;;
(GV305MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GV305MAG.BGV305GM
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305MAJ.BGV305HM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305MAK
       ;;
(GV305MAK)
       m_CondExec 00,EQ,GV305MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR ZONE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAM
       ;;
(GV305MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GV305MAG.BGV305GM
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305MAM.BGV305JM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_7_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305MAN
       ;;
(GV305MAN)
       m_CondExec 00,EQ,GV305MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR GROUPE                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAQ
       ;;
(GV305MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GV305MAG.BGV305GM
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305MAQ.BGV305KM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_CH_9_2 9 CH 2
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_54_6 54 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_9_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305MAR
       ;;
(GV305MAR)
       m_CondExec 00,EQ,GV305MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DES ETATS IGV015 ET IGV018                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAT
       ;;
(GV305MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
#                                                                              
# ******* FICHIER TRI� PAR MAGASIN                                             
       m_FileAssign -d SHR -g ${G_A6} FGV305 ${DATA}/PTEM/GV305MAJ.BGV305HM
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305MAT
#  FICHIERS EDITION                                                            
       m_OutputAssign -c Z IGV015S
#  IGV015M  REPORT SYSOUT=(9,IGV018),SPIN=UNALLOC                              
       m_OutputAssign -c Z IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305MAU
       ;;
(GV305MAU)
       m_CondExec 04,GE,GV305MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV016                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV305MAX
       ;;
(GV305MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
#                                                                              
# ******* FICHIER TRI� PAR ZONE                                                
       m_FileAssign -d SHR -g ${G_A7} FGV305 ${DATA}/PTEM/GV305MAM.BGV305JM
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305MAX
#  FICHIERS EDITION                                                            
#  IGV015S  REPORT SYSOUT=(9,IGV016),SPIN=UNALLOC                              
       m_OutputAssign -c Z IGV015S
#  IGV015M  REPORT SYSOUT=(9,IGV016),SPIN=UNALLOC                              
       m_OutputAssign -c Z IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305MAY
       ;;
(GV305MAY)
       m_CondExec 04,GE,GV305MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV017                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV305MBA
       ;;
(GV305MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
#                                                                              
# ******* FICHIER TRI� PAR GRP DE MAGASINS                                     
       m_FileAssign -d SHR -g ${G_A8} FGV305 ${DATA}/PTEM/GV305MAQ.BGV305KM
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305MBA
#  FICHIERS EDITION                                                            
       m_OutputAssign -c 9 -w IGV017 IGV015S
       m_OutputAssign -c 9 -w IGV017 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305MBB
       ;;
(GV305MBB)
       m_CondExec 04,GE,GV305MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV305MZA
       ;;
(GV305MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV305MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
