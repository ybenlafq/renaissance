#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PP100M.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPP100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/10/28 AT 09.12.59 BY BURTECR                      
#    STANDARDS: P  JOBSET: PP100M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM BPP100                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL REEL DES PIECES TLM DE LA NCG                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PP100MA
       ;;
(PP100MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PP100MAA
       ;;
(PP100MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ TABLE RTGA00                                                          
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTEX30   EXTRACTEUR                                             
#    RTEX30   : NAME=RSEX30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEX30 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP100 
       JUMP_LABEL=PP100MAB
       ;;
(PP100MAB)
       m_CondExec 04,GE,PP100MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP101                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL REEL DES PIECES ELA DE LA NCG                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAD
       ;;
(PP100MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTEX30   EXTRACTEUR                                             
#    RTEX30   : NAME=RSEX30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEX30 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP101 
       JUMP_LABEL=PP100MAE
       ;;
(PP100MAE)
       m_CondExec 04,GE,PP100MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AM ISSU DU BHV030 CHAINE STAT0P                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAG
       ;;
(PP100MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BHV030AM
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/PP100MAG.BPP102AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "907"
 /DERIVEDFIELD CST_3_11 "VEN"
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /CONDITION CND_2 FLD_CH_10_3 EQ CST_1_7 AND FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3
 /INCLUDE CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP100MAH
       ;;
(PP100MAH)
       m_CondExec 00,EQ,PP100MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP102                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL DES B.E TLM                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAJ
       ;;
(PP100MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ FICHIER FGS40 (ISSU DU TRI PRECEDANT)                                 
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PXX0/PP100MAG.BPP102AM
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP102 
       JUMP_LABEL=PP100MAK
       ;;
(PP100MAK)
       m_CondExec 04,GE,PP100MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AM ISSU DU BHV030 CHAINE STAT0P                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAM
       ;;
(PP100MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BHV030AM
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/PP100MAM.BPP103AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_11 "VEN"
 /DERIVEDFIELD CST_1_7 "907"
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /CONDITION CND_2 FLD_CH_10_3 EQ CST_1_7 AND FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3
 /INCLUDE CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PP100MAN
       ;;
(PP100MAN)
       m_CondExec 00,EQ,PP100MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP103                                                                  
#  ----------                                                                  
#  IMPORTATION DU VOLUME MENSUEL EN EMPORTE MAGASIN                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAQ
       ;;
(PP100MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------ FICHIER FGS40 (ISSU DU TRI PRECEDANT)                                 
       m_FileAssign -d SHR -g ${G_A2} FGS40 ${DATA}/PXX0/PP100MAM.BPP103AM
# ------ TABLE RTGA00   ARTICLE                                                
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
# ------ TABLE RTGA01   S TABLE PPIMP                                          
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ------ TABLE RTGA10   LIEUX                                                  
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ------ TABLE RTPP01   ENVELOPPES DETAILS                                     
#    RTPP01   : NAME=RSPP01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTPP01 /dev/null
# ------ TABLE RTPP10   PARAMETRES DE CALCUL D UNE ENVELOPPE                   
#    RTPP10   : NAME=RSPP10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPP10 /dev/null
# ------ FMOIS                                                                 
       m_FileAssign -i FMOIS
$FMOIS
_end
# ------ FNSOC                                                                 
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP103 
       JUMP_LABEL=PP100MAR
       ;;
(PP100MAR)
       m_CondExec 04,GE,PP100MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPP030                                                                  
#  ----------                                                                  
#  MISE A JOUR DE LA PERIODE DE STAT TRAITEE                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PP100MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PP100MAT
       ;;
(PP100MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE RTGA01                                                                
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPP030 
       JUMP_LABEL=PP100MAU
       ;;
(PP100MAU)
       m_CondExec 04,GE,PP100MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PP100MZA
       ;;
(PP100MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PP100MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
