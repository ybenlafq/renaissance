#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV80P.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGPV80 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/12 AT 05.44.17 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GPV80P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#                                                                              
#    REPORTEE  :  CREATION D'UN ETAT IGV810 STATISTIQUE COMMERCIALE            
#    -=-=-=-=     PEUT ETRE REPRISE DANS LA JOURNEE,CAR CETTE CHAINE           
#                 NE FAIT PAS DE M.A.J                                         
#                                                                              
# ********************************************************************         
#  PGM : BGV800                                                                
# ********************************************************************         
#  EXTRACTION DES CODICS POUR STAT COMMERCIALE SUR LA RTHV04                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV80PA
       ;;
(GPV80PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV80PAA
       ;;
(GPV80PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (907)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV04 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV800 ${DATA}/PXX0/GPV80PAA.BGV800AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV800 
       JUMP_LABEL=GPV80PAB
       ;;
(GPV80PAB)
       m_CondExec 04,GE,GPV80PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV800                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAD
       ;;
(GPV80PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GPV80PAA.BGV800AP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PAD.BGV800BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PAE
       ;;
(GPV80PAE)
       m_CondExec 00,EQ,GPV80PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV800                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAG
       ;;
(GPV80PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE ??????                                                         
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A2} FGV800 ${DATA}/PXX0/GPV80PAD.BGV800BP
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80PAG.BGV805AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80PAH
       ;;
(GPV80PAH)
       m_CondExec 04,GE,GPV80PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAJ
       ;;
(GPV80PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GPV80PAG.BGV805AP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PAJ.BGV805BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_74_6 74 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PAK
       ;;
(GPV80PAK)
       m_CondExec 00,EQ,GPV80PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV815 AVEC FPARAM BGV81 POUR ALIMENTATION DE FPV815                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
#  AJOUT PAR PHILIPPE DU FPARAM LE 070793                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAM
       ;;
(GPV80PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV815DIF
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV815 ${DATA}/PXX0/GPV80PAM.BGV815AP
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d SHR FGV816 /dev/null
#                                                                              
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80PAN
       ;;
(GPV80PAN)
       m_CondExec 04,GE,GPV80PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV815                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAQ
       ;;
(GPV80PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/GPV80PAM.BGV815AP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PAQ.BGV815BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PAR
       ;;
(GPV80PAR)
       m_CondExec 00,EQ,GPV80PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV815 (CODIC GOUPE)                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAT
       ;;
(GPV80PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A5} FGV800 ${DATA}/PXX0/GPV80PAQ.BGV815BP
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80PAT.BGV805CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80PAU
       ;;
(GPV80PAU)
       m_CondExec 04,GE,GPV80PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805 (CODIC GROUPE)                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PAX
       ;;
(GPV80PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/GPV80PAT.BGV805CP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PAX.BGV805DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_70_4 70 CH 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PAY
       ;;
(GPV80PAY)
       m_CondExec 00,EQ,GPV80PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV810                                                                
#  EDITION DES STATS COMMERCIALES BBTE  (CODIC GROUPE)                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBA PGM=BGV810     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBA
       ;;
(GPV80PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (907)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  FICHIERS LEADERS ET PRIMES TRIES                                     
       m_FileAssign -d SHR -g ${G_A7} FGV805 ${DATA}/PXX0/GPV80PAX.BGV805DP
# ******  ETAT IGV815                                                          
       m_OutputAssign -c 9 -w IGV815 IGV810
       m_ProgramExec BGV810 
# ********************************************************************         
# ********************************************************************         
#  PGM : BGV815 AVEC FPARAM BGV80 POUR ALIMENTATION DE FPV816                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
#  AJOUT PAR PHILIPPE DU FPARAM LE 070793                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBD
       ;;
(GPV80PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV816DIF
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR FGV815 /dev/null
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV816 ${DATA}/PXX0/GPV80PBD.BGV816AP
#                                                                              
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80PBE
       ;;
(GPV80PBE)
       m_CondExec 04,GE,GPV80PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV816                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBG
       ;;
(GPV80PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/GPV80PBD.BGV816AP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PXX0/GPV80PAA.BGV800AP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PBG.BGV816BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PBH
       ;;
(GPV80PBH)
       m_CondExec 00,EQ,GPV80PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV805  : ENRICHISSEMENT DU FICHIER FGV816 (CODIC GOUPE)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBJ
       ;;
(GPV80PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A10} FGV800 ${DATA}/PXX0/GPV80PBG.BGV816BP
# ******  FICHIER FGV816 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80PBJ.BGV816CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80PBK
       ;;
(GPV80PBK)
       m_CondExec 04,GE,GPV80PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805 (CODIC GROUPE)                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBM
       ;;
(GPV80PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/GPV80PBJ.BGV816CP
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80PBM.BGV816DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80PBN
       ;;
(GPV80PBN)
       m_CondExec 00,EQ,GPV80PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV810  : EDITION DES STATS COMMERCIALES BBTE  (CODIC GROUPE)               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80PBQ PGM=BGV810     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PBQ
       ;;
(GPV80PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (907)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  FICHIERS LEADERS ET PRIMES TRIES                                     
       m_FileAssign -d SHR -g ${G_A12} FGV805 ${DATA}/PXX0/GPV80PBM.BGV816DP
# ******  ETAT IGV810                                                          
       m_OutputAssign -c 9 -w IGV810 IGV810
       m_ProgramExec BGV810 
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV80PZA
       ;;
(GPV80PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV80PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
