#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS052L.ksh                       --- VERSION DU 08/10/2016 17:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLFS052 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/24 AT 12.35.42 BY BURTECA                      
#    STANDARDS: P  JOBSET: FS052L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  BFS052 *   COBOL2/DB2                                                       
# *********                                                                    
#       COMPTABILISATION DES VENTES TOPEES DU JOUR                             
#                                                                              
#       RENSEIGNE LA DATE COMPTABLE DANS LES TABLES RTGV11 + 13 + 14           
#                                                                              
#       CALL AU SOUS PROG MFX053 : MAJ DES FICHIER PSE                         
#       CALL AU SOUS PROG MFV002 : INTERFACE G.L                               
#                                                                              
# *********                                                                    
#   REPRISE OUI APRES EXECUTION DU BACKOUT CORTRANS                            
#   LA REPRISE DOIT OBLIGATOIREMENT SE FAIRE :                                 
#   1. AVEC LA FDATE DU PLANTAGE                                               
#   2. AVEC LE PARAM�TRE DCOMPTA EGAL � FDATE (FORMAT JJMMSSAA)                
#      (DATE DE PREP)                                                          
#   ET                                                                         
#   3. SAUVEGARDER LE FICHIER FFV001 (BFS052A*) POUR LA COMPTA (FV001*         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS052LA
       ;;
(FS052LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FS052LAA
       ;;
(FS052LAA)
       m_CondExec ${EXAAA},NE,YES 
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#  PRED GV115L POUR QUE BFS052 FLAG CARTES T MGI POUR GQC20L                   
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00L,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE DES COMPTES                                                    
#    RSFX00   : NAME=RSFX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE D'ATTRIBUTION DES N�DE PIECES                                  
#    RSFX05   : NAME=RSFX05L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFX05 /dev/null
# ******* TABLE ENTETE DE VENTES                                               
#    RSGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE VENTE PRESTATION                                               
#    RSGV13   : NAME=RSGV13L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV13 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES SEQUENCES D'INTERFACES                                     
#    RSFT29   : NAME=RSFT29L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMENTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00 /dev/null
# ******* FICHIER DES VENTES VERS GCT                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FFV001 ${DATA}/PNCGL/F61.BFS052AL
# ******* FICHIER DES ANNULATIONS POUR PGM BPS020 HEBDO                        
       m_FileAssign -d SHR FX053 /dev/null
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PXX0/F61.BFS052CL
# ******* FICHIER DES PRESTATIONS                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052LAA.BFS052EL
#                                                                              
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IFX052 IFX052
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FS052LOM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS052L
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS052 
       JUMP_LABEL=FS052LAB
       ;;
(FS052LAB)
       m_CondExec 04,GE,FS052LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BBS052 : DUPLICATION DU BFS052 POUR BIENS ET SERVICES                   
#  ----------                                                                  
#  REPRISE : OUI (VOIR EXPLICATION EN D�BUT DE PCL)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FS052LAD
       ;;
(FS052LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN ENTREE                                                      
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGV02   : NAME=RSGV02L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
#    RSBC12   : NAME=RSBC12P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC12 /dev/null
#    RSBC15   : NAME=RSBC15P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC15 /dev/null
#    RSPR00   : NAME=RSPR00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR30   : NAME=RSPR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR30 /dev/null
#    RSPR55   : NAME=RSPR55P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR55 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSFT23   : NAME=RSFT23L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT23 /dev/null
#                                                                              
# ******* TABLES EN MAJ                                                        
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMANTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00 /dev/null
#                                                                              
# ******* FICHIERS EN SORTIE                                                   
# ******* FICHIER DES VENTES VERS FTICSL                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PNCGL/F61.BBS052AL
# ******* FICHIER DES ANNULATIONS                                              
       m_FileAssign -d SHR FX053 /dev/null
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PNCGL/F61.BBS052CL
# ******* FICHIER DES PRETATIONS                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052LAD.BBS052EL
#                                                                              
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IBS052 IFX052
#                                                                              
# ******* FICHIERS PARAM�TRE                                                   
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/BS052LOM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/BS052L
#                                                                              
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F61.FDATMGDL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS052 
       JUMP_LABEL=FS052LAE
       ;;
(FS052LAE)
       m_CondExec 04,GE,FS052LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL DU FICHIER DES PRESTATIONS                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FS052LAG
       ;;
(FS052LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FS052LAA.BFS052EL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.BFS052FL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/FS052LAD.BBS052EL
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGL/F61.BFS052FL
# *****************                                                            
#  DEPENDANCES POUR PLAN :                                                     
#                                                                              
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_76_6 76 CH 6
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_2_6 2 CH 6
 /KEYS
   FLD_CH_82_5 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_52_8 ASCENDING,
   FLD_CH_76_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FS052LZA
       ;;
(FS052LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FS052LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FS052LAH
       ;;
(FS052LAH)
       m_CondExec 00,EQ,FS052LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
