#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV180F.ksh                       --- VERSION DU 08/10/2016 22:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGV180 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/10/27 AT 16.20.21 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV180F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#  PASSAGE DU BGV180 POUR PARIS                                                
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV180FA
       ;;
(GV180FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV180FAA
       ;;
(GV180FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAA.BGV180AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAA.BDA189AP
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MGI OUEST                                            
#                                                                              
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV180FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAD
       ;;
(GV180FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAD.BGV180AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAD.BDA189AO
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR LYON                                                 
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV180FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAG
       ;;
(GV180FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAG.BGV180AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAG.BDA189AY
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR LILLE                                                
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV180FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAJ
       ;;
(GV180FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAJ.BGV180AL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAJ.BDA189AL
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR METZ                                                 
#                                                                              
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP GV180FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAM
       ;;
(GV180FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAM.BGV180AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAM.BDA189AM
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MARSEILLE                                            
#                                                                              
# ************************************************************                 
#                                                                              
# ***********************************                                          
# *   STEP GV180FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAQ
       ;;
(GV180FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAQ.BGV180AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAQ.BDA189AD
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR LUXEMBOURG                                           
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV180FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAT
       ;;
(GV180FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV14   : NAME=RSGV14X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV14 /dev/null
#    RTGV11   : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV180FAT.BGV180AX
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV180FAT.BDA189AX
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180J
# ******                                                                       
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER FGV180                                                    
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV180FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV180FAX
       ;;
(GV180FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV180FAA.BGV180AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GV180FAD.BGV180AO
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GV180FAG.BGV180AY
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GV180FAJ.BGV180AL
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GV180FAM.BGV180AM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GV180FAQ.BGV180AD
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GV180FAT.BGV180AX
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV180FAX.BGV183AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV180FAY
       ;;
(GV180FAY)
       m_CondExec 00,EQ,GV180FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#  PASSAGE DU BGV183 POUR LE CUMUL                                             
#                                                                              
# ********************************************************************         
# BJ      STEP  PGM=BGV183                                                     
# YSABOUT REPORT SYSOUT=*                                                      
# YSPRINT REPORT SYSOUT=*                                                      
# YSOUT   REPORT SYSOUT=*                                                      
# YSTSPRT REPORT SYSOUT=*                                                      
# ****** FICHIER EN ENTREE                                                     
# GV180   FILE  NAME=BGV183AP,MODE=I                                           
# ****** FICHIER EN SORTIE                                                     
# GV183   FILE  NAME=BGV183BP,MODE=O                                           
#                                                                              
# *********************************************************                    
# *                                                                            
# *     EXTRACT DU CA - PGM = BGV189                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV180FBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV180FBA
       ;;
(GV180FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A8} FDA189 ${DATA}/PTEM/GV180FAA.BDA189AP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GV180FAD.BDA189AO
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GV180FAG.BDA189AY
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GV180FAJ.BDA189AL
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GV180FAM.BDA189AM
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GV180FAQ.BDA189AD
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GV180FAT.BDA189AX
# ****** TABLE EN ENTREE                                                       
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FGV189 ${DATA}/PTEM/GV180FBA.BGV189BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV189 
       JUMP_LABEL=GV180FBB
       ;;
(GV180FBB)
       m_CondExec 04,GE,GV180FBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV185 ET BGV186                                                      
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV180FBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GV180FBD
       ;;
(GV180FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A15} FGV189 ${DATA}/PTEM/GV180FBA.BGV189BP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A16} FGV183 ${DATA}/PTEM/GV180FAX.BGV183AP
# *********** 1 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV185 IGV185
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV189BP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV180FBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          

       m_ProgramExec -b BGV185 
       JUMP_LABEL=GV180FBG
       ;;
(GV180FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/GV180FBA.BGV189BP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV180FBG.BGV189EP
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183AP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV180FBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_PD_13_7 13 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV180FBJ
       ;;
(GV180FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GV180FAX.BGV183AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV180FBJ.BGV183HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_7_1 EQ CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV180FBK
       ;;
(GV180FBK)
       m_CondExec 00,EQ,GV180FBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV186                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV180FBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GV180FBM
       ;;
(GV180FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A19} FGV189 ${DATA}/PTEM/GV180FBG.BGV189EP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A20} FGV183 ${DATA}/PTEM/GV180FBJ.BGV183HP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186J IGV186
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183AP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV180FBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          

       m_ProgramExec -b BGV186 
       JUMP_LABEL=GV180FBQ
       ;;
(GV180FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GV180FAX.BGV183AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV180FBQ.BGV183IP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_5 14 CH 5
 /CONDITION CND_1 FLD_CH_7_1 NE CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV180FBR
       ;;
(GV180FBR)
       m_CondExec 00,EQ,GV180FBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV196                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV180FBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GV180FBT
       ;;
(GV180FBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A22} FGV189 ${DATA}/PTEM/GV180FBG.BGV189EP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A23} FGV183 ${DATA}/PTEM/GV180FBQ.BGV183IP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186J IGV186
# *********************                                                        
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV196 
       JUMP_LABEL=GV180FBU
       ;;
(GV180FBU)
       m_CondExec 04,GE,GV180FBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV180FZA
       ;;
(GV180FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV180FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
