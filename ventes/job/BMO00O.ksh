#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BMO00O.ksh                       --- VERSION DU 08/10/2016 12:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POBMO00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/11/02 AT 11.55.17 BY BURTEC2                      
#    STANDARDS: P  JOBSET: BMO00O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE LA CHAINE BS001O (PGM BBS001)                        
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BMO00OA
       ;;
(BMO00OA)
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BMO00OAA
       ;;
(BMO00OAA)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F16.BBS001BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BMO010AO.RECYCLE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/BMO00OAA.BMO010AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BMO00OAB
       ;;
(BMO00OAB)
       m_CondExec 00,EQ,BMO00OAA ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMO010 : INTERFACE COMPTABLE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00OAD PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BMO00OAD
       ;;
(BMO00OAD)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPR00   : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSMO01   : NAME=RSMO01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSMO01 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER ISSU DE LA CHAINE BS001O (PGM BBS001)                        
       m_FileAssign -d SHR -g ${G_A1} FBMO10 ${DATA}/PTEM/BMO00OAA.BMO010AO
#                                                                              
# ******* FICHIER DE RECYCLAGE DU JOUR  (LRECL 400)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FRECYC ${DATA}/PXX0/F16.BMO010AO.RECYCLE
# ***                                                                          
       m_OutputAssign -c 9 -w FANO02 FANO02
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMO010 
       JUMP_LABEL=BMO00OAE
       ;;
(BMO00OAE)
       m_CondExec 04,GE,BMO00OAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMO020 : INTERFACE COMPTABLE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00OAG PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BMO00OAG
       ;;
(BMO00OAG)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSMO01   : NAME=RSMO01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSMO01 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER POUR LA CHAINE FTI01P (LRECL 400)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FBMO20 ${DATA}/PXX0/F16.BMO020AO.FTI01O
#                                                                              
# ******* FICHIER A DESTINATION DE GETAWAY                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FBMO30 ${DATA}/PXX0/F16.BMO020BO.GETAWAY
# ***                                                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMO020 
       JUMP_LABEL=BMO00OAH
       ;;
(BMO00OAH)
       m_CondExec 04,GE,BMO00OAG ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BMO00OZA
       ;;
(BMO00OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
