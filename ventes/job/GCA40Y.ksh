#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GCA40Y.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGCA40 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/01/05 AT 15.26.35 BY BURTECA                      
#    STANDARDS: P  JOBSET: GCA40Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BCA040  : EXTRACT                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GCA40YA
       ;;
(GCA40YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GCA40YAA
       ;;
(GCA40YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  SOUS TABLES                                                          
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  LIEUX                                                                
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  STOCK MAG                                                            
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30Y /dev/null
# ******  MVT DE STOCKS                                                        
#    RSGS40Y  : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40Y /dev/null
#                                                                              
# *****   PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *****   PARAMETRE  FDATE                                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  FICHIER EXTRACTION                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ICA040 ${DATA}/PTEM/GCA40YAA.BCA040AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCA040 
       JUMP_LABEL=GCA40YAB
       ;;
(GCA40YAB)
       m_CondExec 04,GE,GCA40YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI  EXTRACTION                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCA40YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GCA40YAD
       ;;
(GCA40YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GCA40YAA.BCA040AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GCA40YAD.BCA040BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_3 10 CH 03
 /FIELDS FLD_BI_7_3 07 CH 03
 /FIELDS FLD_BI_16_8 16 CH 08
 /FIELDS FLD_BI_13_3 13 CH 03
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_BI_13_3 ASCENDING,
   FLD_BI_16_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GCA40YAE
       ;;
(GCA40YAE)
       m_CondExec 00,EQ,GCA40YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG050 : CREATION D'UN FICHIER FCUMULS BCA040C                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCA40YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GCA40YAG
       ;;
(GCA40YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00Y  : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00Y /dev/null
#    RTEG05Y  : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05Y /dev/null
#    RTEG10Y  : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10Y /dev/null
#    RTEG15Y  : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15Y /dev/null
#    RTEG25Y  : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25Y /dev/null
#    RTEG30Y  : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30Y /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PTEM/GCA40YAD.BCA040BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GCA40YAG.BCA040CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GCA40YAH
       ;;
(GCA40YAH)
       m_CondExec 04,GE,GCA40YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCA40YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GCA40YAJ
       ;;
(GCA40YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GCA40YAG.BCA040CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GCA40YAJ.BCA040DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GCA40YAK
       ;;
(GCA40YAK)
       m_CondExec 00,EQ,GCA40YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEG060 : CREATION DE L'ETAT : ICA040                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GCA40YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GCA40YAM
       ;;
(GCA40YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01Y /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71Y  : NAME=RSGA71Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71Y /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00Y  : NAME=RSEG00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00Y /dev/null
#    RTEG05Y  : NAME=RSEG05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05Y /dev/null
#    RTEG10Y  : NAME=RSEG10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10Y /dev/null
#    RTEG11Y  : NAME=RSEG11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11Y /dev/null
#    RTEG15Y  : NAME=RSEG15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15Y /dev/null
#    RTEG20Y  : NAME=RSEG20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20Y /dev/null
#    RTEG25Y  : NAME=RSEG25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25Y /dev/null
#    RTEG30Y  : NAME=RSEG30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30Y /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PTEM/GCA40YAD.BCA040BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PTEM/GCA40YAJ.BCA040DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ICA040 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GCA40YAN
       ;;
(GCA40YAN)
       m_CondExec 04,GE,GCA40YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GCA40YZA
       ;;
(GCA40YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GCA40YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
