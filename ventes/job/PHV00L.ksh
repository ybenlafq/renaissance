#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV00L.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLPHV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 11.54.48 BY BURTECR                      
#    STANDARDS: P  JOBSET: PHV00L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSHV09L RSHV10L RSHV15L                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV00LA
       ;;
(PHV00LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PHV00LAA
       ;;
(PHV00LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPHV00L
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PHV00LAA
       m_ProgramExec IEFBR14 "RDAR,PHV00L.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00LAD
       ;;
(PHV00LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPHV00L
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PHV00LAE
       ;;
(PHV00LAE)
       m_CondExec 00,EQ,PHV00LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE FICHIER HV01 PAR TRI                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAG
       ;;
(PHV00LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.HV01AL
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SORTOUT ${DATA}/PXX0/F61.HV01BKL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00LAH
       ;;
(PHV00LAH)
       m_CondExec 00,EQ,PHV00LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV000 : PURGE DES TABLES RTHV09 RTHV10 ET SEQUENTIEL HV01                 
#                  SELECT DES LIGNES OU DVENTELIVREE >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAJ
       ;;
(PHV00LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d SHR -g +0 HV01S ${DATA}/PXX0/F61.HV01AL
# *****   HISTO VENTE LIVREE PAR FAMILLE                                       
#    RSHV09L  : NAME=RSHV09L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV09L /dev/null
# *****   HISTO VENTE LIVREE PAR CODIC                                         
#    RSHV10L  : NAME=RSHV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV10L /dev/null
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU  EPURE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV01 ${DATA}/PXX0/PHV00LAJ.BPHV00AL
# *****   FICHIER HISTO VENTES PAR FAMILLE SERVANT A RELOADER RTHV09           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FHV09 ${DATA}/PXX0/F61.RELOAD.HV09RL
# *****   FICHIER HISTO VENTES PAR CODIC   SERVANT A RELOADER RTHV10           
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FHV10 ${DATA}/PXX0/F61.RELOAD.HV10RL
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 1126 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00LAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV000 
       JUMP_LABEL=PHV00LAK
       ;;
(PHV00LAK)
       m_CondExec 04,GE,PHV00LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAM
       ;;
(PHV00LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F61.RELOAD.HV09RL
# ******  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                   
#    RSHV09L  : NAME=RSHV09L,MODE=(U,N) - DYNAM=YES                            
# -X-PHV00LR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09L /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00LAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00L_PHV00LAM_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00LAN
       ;;
(PHV00LAN)
       m_CondExec 04,GE,PHV00LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECHARGEMENT DE LA TABLE RTHV10                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAQ
       ;;
(PHV00LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F61.RELOAD.HV10RL
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
#    RSHV10L  : NAME=RSHV10L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV10L /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00LAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00L_PHV00LAQ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00LAR
       ;;
(PHV00LAR)
       m_CondExec 04,GE,PHV00LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV015 : PURGE DE LA TABLE RTHV15                                          
#                  SELECT DES LIGNES OU DVENTECIALE  >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAT
       ;;
(PHV00LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE DE CONTROLE DE REMONTEE DE CAISSE                              
#    RSHV15L  : NAME=RSHV15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15L /dev/null
#                                                                              
# *****   FICHIER DE LOAD                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 FHV15 ${DATA}/PXX0/F61.RELOAD.HV15RL
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 90 JOURS                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00LAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV015 
       JUMP_LABEL=PHV00LAU
       ;;
(PHV00LAU)
       m_CondExec 04,GE,PHV00LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV15                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LAX
       ;;
(PHV00LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F61.RELOAD.HV15RL
# ******  TABLE CONTROLE REMONTEE DE CAISSES                                   
#    RSHV15L  : NAME=RSHV15L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV15L /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00LAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00L_PHV00LAX_RTHV15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00LAY
       ;;
(PHV00LAY)
       m_CondExec 04,GE,PHV00LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER HISTO VENTES/CODIC/LIEU                                      
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LBA
       ;;
(PHV00LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PHV00LAJ.BPHV00AL
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/F61.HV01AL
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_9 11 CH 9
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00LBB
       ;;
(PHV00LBB)
       m_CondExec 00,EQ,PHV00LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PHV00LZA
       ;;
(PHV00LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
