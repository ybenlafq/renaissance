#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV630D.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGV630 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/18 AT 14.54.07 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV630D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGV630                                                                
# ********************************************************************         
#  TOPER LES LIGNES DEPENDANTES D UNE LIGNE ARTICLE DEJA TOPEE                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV630DA
       ;;
(GV630DA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV630DAA
       ;;
(GV630DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* TABLES EN LECTURE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
#    RSGV08D  : NAME=RSGV08D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08D /dev/null
#    RSPR00D  : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00D /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV10D  : NAME=RSGV10D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSRB21   : NAME=RSRB21,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSRB21 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV630 
       JUMP_LABEL=GV630DAB
       ;;
(GV630DAB)
       m_CondExec 04,GE,GV630DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
