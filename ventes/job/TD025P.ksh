#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD025P.ksh                       --- VERSION DU 08/10/2016 22:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD025 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/11/03 AT 16.03.21 BY BURTECA                      
#    STANDARDS: P  JOBSET: TD025P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DE L ENSEMBLE DES FICHIERS PROVENANT DE LA CHAINE BS001?                
#                                                                              
#                                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TD025PA
       ;;
(TD025PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TD025PAA
       ;;
(TD025PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
#                                                                              
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F91.BBS001BD
       m_FileAssign -d SHR -C ${DATA}/PXX0.F61.BBS001BL
       m_FileAssign -d SHR -C ${DATA}/PXX0.F89.BBS001BM
       m_FileAssign -d SHR -C ${DATA}/PXX0.F16.BBS001BO
       m_FileAssign -d SHR -C ${DATA}/PXX0.F07.BBS001BP
       m_FileAssign -d SHR -C ${DATA}/PXX0.F45.BBS001BY
       m_FileAssign -d SHR -C ${DATA}/P908.SEM.BBS001BX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 SORTOUT ${DATA}/PTEM/TD025PAA.BTD025AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD UKNOWN_2_8 PD
 /DERIVEDFIELD UKNOWN_3_9 LENGTH=6
 /FIELDS FLD_CH_70_2 70 CH 2
 /FIELDS FLD_CH_61_8 61 CH 8
 /FIELDS FLD_CH_72_329 72 CH 329
 /FIELDS FLD_CH_1_60 1 CH 60
 /KEYS
   FLD_CH_1_60 ASCENDING
 /MT_INFILE_SORT SORTIN
 /REFORMAT FLD_CH_1_60,FLD_CH_61_8,FLD_CH_70_2,FLD_CH_72_329
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_60,FLD_CH_61_8,FLD_CH_70_2,UKNOWN_2_8,UKNOWN_3_9,FLD_CH_72_329
_end
       m_FileSort -s SYSIN
       JUMP_LABEL=TD025PAB
       ;;
(TD025PAB)
       m_CondExec 00,EQ,TD025PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP TD025PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TD025PAD
       ;;
(TD025PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/TD025PAA.BTD025AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 SORTOUT ${DATA}/PTEM/TD025PAD.BTD025BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD FLD_PD_61_6 FLD_WITHOUT_EDIT_PD_61_6 (+9999999,99)
 /FIELDS FLD_PD_61_6 61 PD 6
 /FIELDS FLD_CH_67_END 67 CH 
 /FIELDS FLD_CH_1_60 1 CH 60
 /FIELDS FLD_WITHOUT_EDIT_PD_61_6 61 PD 06
 /KEYS
   FLD_CH_1_60 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_61_6
 /* MT_ERROR (unknown field) ,72:67 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_60,FLD_PD_61_6,FLD_CH_67_END
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=TD025PAE
       ;;
(TD025PAE)
       m_CondExec 00,EQ,TD025PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD025  ALIMENTATION AVEC LE FICHIER TRIE DE LA TABLE RTTD025         
#  ------------                                                                
#                                                                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD025PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TD025PAG
       ;;
(TD025PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  PARAMETRE MOIS                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCHIER  ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A2} FTD025E ${DATA}/PTEM/TD025PAD.BTD025BP
#                                                                              
# *****   TABLE EN MAJ                                                         
#    RTTD04   : NAME=RSTD04P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTTD04 /dev/null
#                                                                              
# *****   TABLE EN LECTURE                                                     
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTTD01   : NAME=RSTD01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTTD01 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD025 
       JUMP_LABEL=TD025PAH
       ;;
(TD025PAH)
       m_CondExec 04,GE,TD025PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TD025PZA
       ;;
(TD025PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD025PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
