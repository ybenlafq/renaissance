#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV20M.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGPV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/24 AT 17.39.10 BY BURTECN                      
#    STANDARDS: P  JOBSET: GPV20M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV20MA
       ;;
(GPV20MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV20MAA
       ;;
(GPV20MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )                
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F89.DATPV20M
#                                                                              
# ------  TABLE EN LECTURE                                                     
#                                                                              
#    RTGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00M /dev/null
#    RTGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01M /dev/null
#    RTGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10M /dev/null
#    RTGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14M /dev/null
#    RTGA59M  : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA59M /dev/null
#    RTGA66M  : NAME=RSGA66M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66M /dev/null
#    RTGA73M  : NAME=RSGA73M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA73M /dev/null
#    RTGA75M  : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75M /dev/null
#    RTGS30M  : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGS30M /dev/null
#                                                                              
# ------  FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/GPV20MAA.BPV200AM
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220 ${DATA}/PTEM/GPV20MAA.BPV200BM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV20MAB
       ;;
(GPV20MAB)
       m_CondExec 04,GE,GPV20MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200AM (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)         
#  ET CREATION DU FICHIER BPV211AM ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAD
       ;;
(GPV20MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GPV20MAA.BPV200AM
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GPV20MAD.BPV211AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20MAE
       ;;
(GPV20MAE)
       m_CondExec 00,EQ,GPV20MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200BM (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,         
#  ET CREATION DU FICHIER BPV211BM ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAG
       ;;
(GPV20MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GPV20MAA.BPV200BM
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GPV20MAG.BPV211BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20MAH
       ;;
(GPV20MAH)
       m_CondExec 00,EQ,GPV20MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV211                                                                
# ********************************************************************         
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAJ
       ;;
(GPV20MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- TABLES EN ENTREE                                                     
#                                                                              
#    RTGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11M /dev/null
#    RTGA12M  : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12M /dev/null
#    RTGA25M  : NAME=RSGA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25M /dev/null
#    RTGA26M  : NAME=RSGA26M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA26M /dev/null
#    RTGA53M  : NAME=RSGA53M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53M /dev/null
#                                                                              
# ------- FICHIER D'EXTRACTION DES TRIS PRECEDENTS                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC1 ${DATA}/PTEM/GPV20MAD.BPV211AM
       m_FileAssign -d SHR -g ${G_A4} FPV220 ${DATA}/PTEM/GPV20MAG.BPV211BM
#                                                                              
# ------- FICHIER D'EXTRACTION DE BPV210 (EN DUMMY) CAR CE PROG                
# ------- TOURNE SEULEMENT EN HEBDO DANS GPV21M                                
#                                                                              
       m_FileAssign -d SHR FEXTRAC2 /dev/null
#                                                                              
# ------- FICHIER D'EDITION EN DUMMY CAR ON NE LES FAIT PAS EN QUOTIDI         
#                                                                              
       m_FileAssign -d SHR FEXTR1B /dev/null
       m_FileAssign -d SHR FEXTR2B /dev/null
#                                                                              
# ------- FICHIER FPV220B ENTRANT DANS LE PGM BPV212                           
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220B ${DATA}/PTEM/GPV20MAJ.BPV220AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV20MAK
       ;;
(GPV20MAK)
       m_CondExec 04,GE,GPV20MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV220AM POUR CREATION DU FICHIER COMPLET DES                
#  MODIFICATIONS DE L'OFFRE ACTIVE (BPV220BM)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAM
       ;;
(GPV20MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GPV20MAJ.BPV220AM
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/F89.BPV220BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_42_7 42 CH 7
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20MAN
       ;;
(GPV20MAN)
       m_CondExec 00,EQ,GPV20MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV212                                                                
# ********************************************************************         
#  CREATION DU FICHIER FPV212 A ENVOYER SUR LES 36 DES MAGS PAR                
#  COMPARAISON DES FICHIERS BPV220BM                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAQ PGM=BPV212     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAQ
       ;;
(GPV20MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- DATE (JJMMSSAA)                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ------- FICHIER DE L'OFFRE ACTIVE DE LA VEILLE                               
       m_FileAssign -d SHR -g ${G_A6} FPV220O ${DATA}/PXX0/F89.BPV220BM
# ------- FICHIER DE L'OFFRE ACTIVE DU JOUR                                    
       m_FileAssign -d SHR -g ${G_A7} FPV220B ${DATA}/PXX0/F89.BPV220BM
# ------- FICHIER DE COMPARAISON A ENVOYER SUR LE 36 (GDGNB=5)                 
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FPV212 ${DATA}/PXX0/F89.BPV212AM
       m_ProgramExec BPV212 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MAT
       ;;
(GPV20MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA                                                       
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F89.DATPV20M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20MAU
       ;;
(GPV20MAU)
       m_CondExec 00,EQ,GPV20MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV20MZA
       ;;
(GPV20MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV20MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
