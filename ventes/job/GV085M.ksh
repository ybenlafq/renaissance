#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV085M.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGV085 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/04/03 AT 14.14.18 BY PREPA3                       
#    STANDARDS: P  JOBSET: GV085M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGV085 CREE FIC CONTENANT VENTES ELEMENTS DE GROUPE PRODUIT DU MOIS         
#              ET EN CUMUL SUR EXERCICE ANNEE N ET N-1                         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV085MA
       ;;
(GV085MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV085MAA
       ;;
(GV085MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************                                            
# ******* TABLES DB2 UTILISEES  ***                                            
# *********************************                                            
#                                                                              
# ******* TABLE ARTICLES METZ                                                  
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******* TABLE RELATION ETAT FAMILLE/RAYON                                    
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******* TABLE HISTO VENTES/CODIC,GROUP,MAG,MOIS                              
#    RSHV06M  : NAME=RSHV06M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV06M /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 FGV085 ${DATA}/PXX0/GV085MAA.GV0085AM
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV085 
       JUMP_LABEL=GV085MAB
       ;;
(GV085MAB)
       m_CondExec 04,GE,GV085MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5 FAMILLE GROUP 12,5 FAMILLE ELEMEN         
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV085MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV085MAD
       ;;
(GV085MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GV085MAA.GV0085AM
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PXX0/GV085MAD.GV0085BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_86_7 86 PD 7
 /FIELDS FLD_PD_48_7 48 PD 7
 /FIELDS FLD_PD_22_7 22 PD 7
 /FIELDS FLD_PD_36_5 36 PD 5
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_60_7 60 PD 7
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_17_5 17 PD 5
 /FIELDS FLD_PD_41_7 41 PD 7
 /FIELDS FLD_PD_29_7 29 PD 7
 /FIELDS FLD_PD_67_7 67 PD 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_79_7 79 PD 7
 /FIELDS FLD_PD_74_5 74 PD 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_17_5,
    TOTAL FLD_PD_22_7,
    TOTAL FLD_PD_29_7,
    TOTAL FLD_PD_36_5,
    TOTAL FLD_PD_41_7,
    TOTAL FLD_PD_48_7,
    TOTAL FLD_PD_55_5,
    TOTAL FLD_PD_60_7,
    TOTAL FLD_PD_67_7,
    TOTAL FLD_PD_74_5,
    TOTAL FLD_PD_79_7,
    TOTAL FLD_PD_86_7
 /* Record Type = F  Record Length = 94 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV085MAE
       ;;
(GV085MAE)
       m_CondExec 00,EQ,GV085MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV090 VENTES DES FAMILLES DE GROUPES DE PRODUITS              *            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV085MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV085MAG
       ;;
(GV085MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************                                            
# ******* TABLES DB2 UTILISEES ****                                            
# *********************************                                            
#                                                                              
# ******* TABLE HISTO VENTE/SOC,ARTICLE,JOUR                                   
#    RSHV09M  : NAME=RSHV09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV09M /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FGV085T ${DATA}/PXX0/GV085MAD.GV0085BM
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 FGV090 ${DATA}/PXX0/GV085MAG.GV0090AM
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV090 
       JUMP_LABEL=GV085MAH
       ;;
(GV085MAH)
       m_CondExec 04,GE,GV085MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5 FAMILLE GROUP 12,5 FAMILLE ELEMEN         
#   17,1 TYPE ENR                                                              
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV085MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV085MAJ
       ;;
(GV085MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GV085MAG.GV0090AM
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PAS0/F89.GV0090BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_75_5 75 PD 5
 /FIELDS FLD_PD_37_5 37 PD 5
 /FIELDS FLD_PD_80_7 80 PD 7
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_56_5 56 PD 5
 /FIELDS FLD_PD_42_7 42 PD 7
 /FIELDS FLD_PD_61_7 61 PD 7
 /FIELDS FLD_PD_87_7 87 PD 7
 /FIELDS FLD_PD_30_7 30 PD 7
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_PD_23_7 23 PD 7
 /FIELDS FLD_PD_49_7 49 PD 7
 /FIELDS FLD_CH_17_1 17 CH 1
 /FIELDS FLD_PD_68_7 68 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_18_5 18 PD 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_17_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_18_5,
    TOTAL FLD_PD_23_7,
    TOTAL FLD_PD_30_7,
    TOTAL FLD_PD_37_5,
    TOTAL FLD_PD_42_7,
    TOTAL FLD_PD_49_7,
    TOTAL FLD_PD_56_5,
    TOTAL FLD_PD_61_7,
    TOTAL FLD_PD_68_7,
    TOTAL FLD_PD_75_5,
    TOTAL FLD_PD_80_7,
    TOTAL FLD_PD_87_7
 /* Record Type = F  Record Length = 94 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV085MAK
       ;;
(GV085MAK)
       m_CondExec 00,EQ,GV085MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV085MZA
       ;;
(GV085MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV085MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
