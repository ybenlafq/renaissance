#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE010Y.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYRE010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/04/06 AT 16.09.40 BY BURTEC4                      
#    STANDARDS: P  JOBSET: RE010Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   PROG    QUIESCE DE LA TABLE RTRE10Y                                        
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE010YA
       ;;
(RE010YA)
#
#RE010YAA
#RE010YAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#RE010YAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE010YAD
       ;;
(RE010YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BHV030AY
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/RE010YAD.BRE001FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "VEN"
 /DERIVEDFIELD CST_5_13 "      "
 /DERIVEDFIELD CST_1_5 "VEN"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_93_6 93 CH 6
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_CH_13_3 13 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 AND FLD_CH_93_6 GT CST_5_13 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_104_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 146 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE010YAE
       ;;
(RE010YAE)
       m_CondExec 00,EQ,RE010YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE010  EXTRACTION DES MVTS DU MOIS                                
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAG
       ;;
(RE010YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE DES PRIX STANDARDS                                                    
#    RTGA59   : NAME=RSGA59Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59 /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGV11   : NAME=RSGV11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES GAMMES : PRIX EXCEPTIONNELS                                       
#    RTGG20   : NAME=RSGG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#  TABLE DES PRMP                                                              
#    RTGG50   : NAME=RSGG50Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#  TABLE DES PRMP DACEM                                                        
#    RTGG55   : NAME=RSGG55Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#                                                                              
#  ENTREE: PARAMETRE FMOIS                                                     
       m_FileAssign -i FMOIS
$FMOIS
_end
#  FCOMPTA                                                                     
       m_FileAssign -i FCOMPTA
$FDATE
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#  FRTGS40 : VENANT DU TRI PREALABLE                                           
       m_FileAssign -d SHR -g ${G_A1} FRTGS40 ${DATA}/PXX0/RE010YAD.BRE001FY
#  SORTIE FICHIER EXTRACTION :100 DE LONG                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE010 ${DATA}/PXX0/RE010YAG.BRE011AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE010 
       JUMP_LABEL=RE010YAH
       ;;
(RE010YAH)
       m_CondExec 04,GE,RE010YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EXTRACTION PRECEDEMMENT OBTENU                             
#  TRI  SUR FICHIER 18,05,A,90,07,A,1,17,A                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAJ
       ;;
(RE010YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/RE010YAG.BRE011AY
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PXX0/RE010YAJ.BRE010BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 05
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_PD_64_5 64 PD 5
 /FIELDS FLD_CH_90_7 90 CH 07
 /FIELDS FLD_PD_69_4 69 PD 4
 /FIELDS FLD_PD_54_5 54 PD 5
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_PD_49_5 49 PD 5
 /FIELDS FLD_PD_59_5 59 PD 5
 /KEYS
   FLD_CH_18_5 ASCENDING,
   FLD_CH_90_7 ASCENDING,
   FLD_CH_1_17 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_49_5,
    TOTAL FLD_PD_54_5,
    TOTAL FLD_PD_59_5,
    TOTAL FLD_PD_64_5,
    TOTAL FLD_PD_69_4,
    TOTAL FLD_PD_73_5
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE010YAK
       ;;
(RE010YAK)
       m_CondExec 00,EQ,RE010YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE015 :ENRICHISSEMENT DU FICHIER SEQUENTIEL ET                    
#   MISE A JOUR DE LA TABLE RTRE10Y                                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAM
       ;;
(RE010YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE D EDITION DES ETATS                                                   
#    RTGA09   : NAME=RSGA09Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09 /dev/null
#  TABLE FAMILLE : EDITION DES ETATS                                           
#    RTGA11   : NAME=RSGA11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11 /dev/null
#  TABLE D EDITION DES ETATS (MARKETTING)                                      
#    RTGA12   : NAME=RSGA12Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12 /dev/null
#  TABLE FAMILLES                                                              
#    RTGA14   : NAME=RSGA14Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14 /dev/null
#  TABLE DES RAYONS: LIBELLES RAYONS                                           
#    RTGA20   : NAME=RSGA20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20 /dev/null
#  TABLE DES ASSOCIATIONS (CODE VALEUR ET DESCRIPTIF)                          
#    RTGA25   : NAME=RSGA25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25 /dev/null
#  TABLE EDITION GENERALISE (LIBELLE AGREGAT)                                  
#    RTGA29   : NAME=RSGA29Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29 /dev/null
#  TABLE                                                                       
#    RTGA53   : NAME=RSGA53Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53 /dev/null
#  TABLE DES PARAMETRE ASSOCIE AUX FAMILLES                                    
#    RTGA30   : NAME=RSGA30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30 /dev/null
#                                                                              
#  ENTREE: TRI DU FICHIER SEQUENTIEL                                           
       m_FileAssign -d SHR -g ${G_A3} FRE010 ${DATA}/PXX0/RE010YAJ.BRE010BY
#  FICHIER EN SORTIE : LRECL 100                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE015 ${DATA}/PXX0/RE010YAM.BRE015AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE015 
       JUMP_LABEL=RE010YAN
       ;;
(RE010YAN)
       m_CondExec 04,GE,RE010YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D EXTRACTION PRECEDEMMENT OBTENU                             
#  TRI  SUR FICHIER 01,48,A,78,05,A                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAQ
       ;;
(RE010YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/RE010YAM.BRE015AY
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PXX0/RE010YAQ.BRE015BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_CH_1_48 01 CH 48
 /FIELDS FLD_PD_49_5 49 PD 5
 /FIELDS FLD_PD_64_5 64 PD 5
 /FIELDS FLD_CH_78_5 78 CH 05
 /FIELDS FLD_PD_69_4 69 PD 4
 /FIELDS FLD_PD_54_5 54 PD 5
 /FIELDS FLD_PD_59_5 59 PD 5
 /KEYS
   FLD_CH_1_48 ASCENDING,
   FLD_CH_78_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_49_5,
    TOTAL FLD_PD_54_5,
    TOTAL FLD_PD_59_5,
    TOTAL FLD_PD_64_5,
    TOTAL FLD_PD_69_4,
    TOTAL FLD_PD_73_5
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE010YAR
       ;;
(RE010YAR)
       m_CondExec 00,EQ,RE010YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE018  CREATION DANS LA TABLE RTRE10Y DES OCCURENCES DU M         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAT
       ;;
(RE010YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES GENERALISE                                               
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#                                                                              
#    RTRE10Y  : NAME=RSRE10Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTRE10Y /dev/null
#                                                                              
#  ENTREE FICHIER FRE015                                                       
       m_FileAssign -d SHR -g ${G_A5} FRE010 ${DATA}/PXX0/RE010YAQ.BRE015BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE018 
       JUMP_LABEL=RE010YAU
       ;;
(RE010YAU)
       m_CondExec 04,GE,RE010YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRE011 CREE ETAT QRE000                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE010YAX
       ;;
(RE010YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTRE10   : NAME=RSRE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRE10 /dev/null
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_OutputAssign -c 9 -w QRE000 QRE000
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE011 
       JUMP_LABEL=RE010YAY
       ;;
(RE010YAY)
       m_CondExec 04,GE,RE010YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRE011E CREE ETAT QRE000E                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE010YBA
       ;;
(RE010YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTRE10   : NAME=RSRE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTRE10 /dev/null
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
       m_OutputAssign -c 9 -w QRE000E QRE000E
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE011E 
       JUMP_LABEL=RE010YBB
       ;;
(RE010YBB)
       m_CondExec 04,GE,RE010YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE000M                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE010YBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE010YBD
       ;;
(RE010YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE000M DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QRE000M (&&FMOIS='$FMOIS_3_4$FMOIS_1_2' FORM=ADMFIL.FRE000M
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE010Y3 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE010YBE
       ;;
(RE010YBE)
       m_CondExec 04,GE,RE010YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE010YZA
       ;;
(RE010YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE010YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
