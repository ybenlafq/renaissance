#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV00O.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPHV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 11.12.04 BY BURTECN                      
#    STANDARDS: P  JOBSET: PHV00O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSHV09O RSHV10O RSHV15O                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV00OA
       ;;
(PHV00OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PHV00OAA
       ;;
(PHV00OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPHV00O
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PHV00OAA
       m_ProgramExec IEFBR14 "RDAR,PHV00O.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00OAD
       ;;
(PHV00OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPHV00O
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PHV00OAE
       ;;
(PHV00OAE)
       m_CondExec 00,EQ,PHV00OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE FICHIER HV01 PAR TRI                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAG
       ;;
(PHV00OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F16.HV01AO
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SORTOUT ${DATA}/PXX0/F16.HV01BKO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00OAH
       ;;
(PHV00OAH)
       m_CondExec 00,EQ,PHV00OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV000 : PURGE DES TABLES RTHV09 RTHV10 ET SEQUENTIEL HV01                 
#                  SELECT DES LIGNES OU DVENTELIVREE >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAJ
       ;;
(PHV00OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d SHR -g +0 HV01S ${DATA}/PGV0/F16.HV01AO
# *****   HISTO VENTE LIVREE PAR FAMILLE                                       
#    RSHV09O  : NAME=RSHV09O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV09O /dev/null
# *****   HISTO VENTE LIVREE PAR CODIC                                         
#    RSHV10O  : NAME=RSHV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV10O /dev/null
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU  EPURE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV01 ${DATA}/PTEM/PHV00OAJ.BPHV00AO
# *****   FICHIER HISTO VENTES PAR FAMILLE SERVANT A RELOADER RTHV09           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FHV09 ${DATA}/PXX0/F16.RELOAD.HV09RO
# *****   FICHIER HISTO VENTES PAR CODIC   SERVANT A RELOADER RTHV10           
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FHV10 ${DATA}/PXX0/F16.RELOAD.HV10RO
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 1126 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00OAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV000 
       JUMP_LABEL=PHV00OAK
       ;;
(PHV00OAK)
       m_CondExec 04,GE,PHV00OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAM
       ;;
(PHV00OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F16.RELOAD.HV09RO
# ******  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                   
#    RSHV09O  : NAME=RSHV09O,MODE=(U,N) - DYNAM=YES                            
# -X-PHV00OR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00OAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00O_PHV00OAM_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00OAN
       ;;
(PHV00OAN)
       m_CondExec 04,GE,PHV00OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECHARGEMENT DE LA TABLE RTHV10                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAQ
       ;;
(PHV00OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F16.RELOAD.HV10RO
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
#    RSHV10O  : NAME=RSHV10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV10O /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00OAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00O_PHV00OAQ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00OAR
       ;;
(PHV00OAR)
       m_CondExec 04,GE,PHV00OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV015 : PURGE DE LA TABLE RTHV15                                          
#                  SELECT DES LIGNES OU DVENTECIALE  >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAT
       ;;
(PHV00OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE DE CONTROLE DE REMONTEE DE CAISSE                              
#    RSHV15O  : NAME=RSHV15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15O /dev/null
#                                                                              
# *****   FICHIER DE LOAD                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 FHV15 ${DATA}/PXX0/F16.RELOAD.HV15RO
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 90 JOURS                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00OAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV015 
       JUMP_LABEL=PHV00OAU
       ;;
(PHV00OAU)
       m_CondExec 04,GE,PHV00OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV15                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OAX
       ;;
(PHV00OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F16.RELOAD.HV15RO
# ******  TABLE CONTROLE REMONTEE DE CAISSES                                   
#    RSHV15O  : NAME=RSHV15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV15O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00OAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00O_PHV00OAX_RTHV15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00OAY
       ;;
(PHV00OAY)
       m_CondExec 04,GE,PHV00OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER HISTO VENTES/CODIC/LIEU                                      
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OBA
       ;;
(PHV00OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PHV00OAJ.BPHV00AO
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PGV0/F16.HV01AO
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_9 11 CH 9
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00OBB
       ;;
(PHV00OBB)
       m_CondExec 00,EQ,PHV00OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PHV00OZA
       ;;
(PHV00OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
