#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EM927D.ksh                       --- VERSION DU 09/10/2016 05:49
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDEM927 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/01/13 AT 10.41.19 BY BURTECA                      
#    STANDARDS: P  JOBSET: EM927D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#  MERGE DU FICHIER BNM155AD AVEC EVENTUELLEMENT LES MVTS DE LA VEILLE         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EM927DA
       ;;
(EM927DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2004/01/13 AT 10.41.19 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: EM927D                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ARCHIV. CAISSES'                       
# *                           APPL...: REPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EM927DAA
       ;;
(EM927DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ************************                                                     
#  DEPENDANCE POUR PLAN                                                        
# ************************                                                     
# *****   FIC VENANT DU PCL NM001D                                             
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM155AD
# *****   FICHIER UTILISE DANS LA CHAINE                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/EM927DAA.BIC927AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_3 26 CH 3
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_17_3 17 CH 3
 /FIELDS FLD_CH_20_3 20 CH 3
 /FIELDS FLD_CH_23_3 23 CH 3
 /FIELDS FLD_CH_9_8 09 CH 8
 /KEYS
   FLD_CH_1_8 ASCENDING,
   FLD_CH_20_3 ASCENDING,
   FLD_CH_23_3 ASCENDING,
   FLD_CH_26_3 ASCENDING,
   FLD_CH_9_8 ASCENDING,
   FLD_CH_43_1 ASCENDING,
   FLD_CH_17_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EM927DAB
       ;;
(EM927DAB)
       m_CondExec 00,EQ,EM927DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ARCHIVAGE DES TRANSACTIONS DE CAISSE                                        
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EM927DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EM927DAD
       ;;
(EM927DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PTEM/EM927DAA.BIC927AD
#                                                                              
#   TABLE DES MODES DE PAIEMENT NEM                                            
#    RTPM06   : NAME=RSPM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPM06 /dev/null
#   TABLE DES ARTICLES                                                         
#    RTGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA58 /dev/null
#   TABLE                                                                      
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#   TABLE DES VENTES                                                           
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#   TABLE DES ENTETES DE CAISSE                                                
#    RTEM51   : NAME=RSEM51,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM51 /dev/null
#   TABLE DES LIGNES DETAIL TRANSACTIONS CAISSE                                
#    RTEM52   : NAME=RSEM52,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM52 /dev/null
#   TABLE DES REGLEMENTS DE TRANSACTIONS CAISSE                                
#    RTEM53   : NAME=RSEM53,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM53 /dev/null
#   TABLE DES TRANSACTIONS ADMINISTRATIVES                                     
#    RTEM54   : NAME=RSEM54,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM54 /dev/null
#   TABLE DES DECOMPTES CAISSE                                                 
#    RTEM55   : NAME=RSEM55,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM55 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM927 
       JUMP_LABEL=EM927DAE
       ;;
(EM927DAE)
       m_CondExec 04,GE,EM927DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#  DEPENDANCE POUR PLAN                                                        
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EM927DZA
       ;;
(EM927DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EM927DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
