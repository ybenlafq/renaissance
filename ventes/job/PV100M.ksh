#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100M.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 16.19.04 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV100M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100MA
       ;;
(PV100MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+2'}
       MENS=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=PV100MAA
       ;;
(PV100MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F89.PV100HM.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV100MAA.BPV108EM
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPV102                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAD
       ;;
(PV100MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV100MAA.BPV108EM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100MAD.BPV108MM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100MAE
       ;;
(PV100MAE)
       m_CondExec 00,EQ,PV100MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV102  CALCUL DES NIVEAUX D AGREGATION                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAG
       ;;
(PV100MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00M  : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00M /dev/null
#  TABLE DES IMBRICATIONS DES  CODES MARKETING                                 
#    RTGA09M  : NAME=RSGA09M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09M /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGA11M  : NAME=RSGA11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11M /dev/null
#  TABLE DES EDITIONS                                                          
#    RTGA12M  : NAME=RSGA12M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14M  : NAME=RSGA14M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA20M  : NAME=RSGA20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20M /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA25M  : NAME=RSGA25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25M /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA29M  : NAME=RSGA29M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29M /dev/null
#  TABLE CODES DESCRIPTIFS CODIC                                               
#    RTGA53M  : NAME=RSGA53M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53M /dev/null
#  TABLE HISTORIQUE PRMP                                                       
#    RTGG50M  : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50M /dev/null
#  TABLE HISTORIQUE PRMP DACEM                                                 
#    RTGG55M  : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55M /dev/null
#  TABLM DES MVTS  DE STOCK                                                    
#    RTGS40M  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40M /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A2} FPV100 ${DATA}/PTEM/PV100MAD.BPV108MM
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00M  : NAME=RSAN00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00M /dev/null
#  SORTIE FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV102 ${DATA}/PTEM/PV100MAG.BPV102AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV102 
       JUMP_LABEL=PV100MAH
       ;;
(PV100MAH)
       m_CondExec 04,GE,PV100MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV103 MAJ DE LA TABLE HV32                   
#  POUR PGM BPV103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAJ
       ;;
(PV100MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PV100MAG.BPV102AM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100MAJ.BPV103AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100MAK
       ;;
(PV100MAK)
       m_CondExec 00,EQ,PV100MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV103  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAM
       ;;
(PV100MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A4} FPV103 ${DATA}/PTEM/PV100MAJ.BPV103AM
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  MAJ DE LA TABLE RTHV32                                                      
#    RSHV32M  : NAME=RSHV32M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV32M /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00M  : NAME=RSAN00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00M /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV103 
       JUMP_LABEL=PV100MAN
       ;;
(PV100MAN)
       m_CondExec 04,GE,PV100MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV104                                        
#  POUR PGM BPV104                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAQ
       ;;
(PV100MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV100MAA.BPV108EM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100MAQ.BPV104AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_21 "     "
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 OR FLD_CH_171_5 EQ CST_3_21 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100MAR
       ;;
(PV100MAR)
       m_CondExec 00,EQ,PV100MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV104  MAJ DE  LA TABLE HV26                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAT
       ;;
(PV100MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** FICHIER FMOIS                                 
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *************************TABLE HISTORIQUE DES VENTES DE PSE                  
#    RTGV26M  : NAME=RSGV26M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV26M /dev/null
#                                                                              
# ******************************FICHIER ISSU DU TRI PRECEDENT                  
       m_FileAssign -d SHR -g ${G_A6} FPV104 ${DATA}/PTEM/PV100MAQ.BPV104AM
# *********************MAJ DE LA TABLE RTHV26 HISTORIQUE VENTE PSE             
#    RSHV26M  : NAME=RSHV26M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV26M /dev/null
# *********************SORTIE TABLES ANOMALIES                                 
#    RTAN00M  : NAME=RSAN00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00M /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV104 
       JUMP_LABEL=PV100MAU
       ;;
(PV100MAU)
       m_CondExec 04,GE,PV100MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV107 MAJ DE LA TABLE HV33                   
#  POUR PGM BPV107                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV100MAX
       ;;
(PV100MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV100MAG.BPV102AM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100MAX.BPV107AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100MAY
       ;;
(PV100MAY)
       m_CondExec 00,EQ,PV100MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV107  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBA
       ;;
(PV100MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE DES VENDEURS                                                          
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A8} FPV107 ${DATA}/PTEM/PV100MAX.BPV107AM
#  MAJ DE LA TABLE RTHV32 HISTORIQUE VENTES                                    
#    RSHV33M  : NAME=RSHV33M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV33M /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV107 
       JUMP_LABEL=PV100MBB
       ;;
(PV100MBB)
       m_CondExec 04,GE,PV100MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBD
       ;;
(PV100MBD)
       m_CondExec ${EXABT},NE,YES 1,NE,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
       m_OutputAssign -c 9 -w IPV110 IMPRIM
#  FORMATTAGE DU FICHIER A DESTINATION DE L EDITION GENERALISE                 
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG110 ${DATA}/PXX0/F89.BPV110AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100MBE
       ;;
(PV100MBE)
       m_CondExec 04,GE,PV100MBD ${EXABT},NE,YES 1,NE,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBG
       ;;
(PV100MBG)
       m_CondExec ${EXABY},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31M  : NAME=RSGV31M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
#  IMPRIM   REPORT SYSOUT=(9,IPV110),LRECL=189,RECFM=FBA,BLKSIZE=1890          
       m_FileAssign -d NEW,CATLG,DELETE -r 189 -t LSEQ -g +1 IMPRIM ${DATA}/PXX0/F89.IPV110AM
#  FORMATTAGE DU FICHIER A DESTINATION DE L EDITION GENERALISE                 
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g ${G_A9} FEG110 ${DATA}/PXX0/F89.BPV110AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100MBH
       ;;
(PV100MBH)
       m_CondExec 04,GE,PV100MBG ${EXABY},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV110 ETAT DE PREPARATION DE PAYE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBJ
       ;;
(PV100MBJ)
       m_CondExec ${EXACD},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A10} SYSUT1 ${DATA}/PXX0/F89.IPV110AM
       m_OutputAssign -c 9 -w IPV110 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PV100MBK
       ;;
(PV100MBK)
       m_CondExec 00,EQ,PV100MBJ ${EXACD},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBM
       ;;
(PV100MBM)
       m_CondExec ${EXACI},NE,YES 1,NE,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE                                                                       
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
       m_OutputAssign -c 9 -w IPV111 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100MBN
       ;;
(PV100MBN)
       m_CondExec 04,GE,PV100MBM ${EXACI},NE,YES 1,NE,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBQ
       ;;
(PV100MBQ)
       m_CondExec ${EXACN},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE                                                                       
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
#  IMPRIM   REPORT SYSOUT=(9,IPV111),LRECL=199,RECFM=FBA,BLKSIZE=1990          
       m_FileAssign -d NEW,CATLG,DELETE -r 199 -t LSEQ -g +1 IMPRIM ${DATA}/PXX0/F89.IPV111AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100MBR
       ;;
(PV100MBR)
       m_CondExec 04,GE,PV100MBQ ${EXACN},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV111 ETAT DE PREPARATION DE PAYE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBT
       ;;
(PV100MBT)
       m_CondExec ${EXACS},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A11} SYSUT1 ${DATA}/PXX0/F89.IPV111AM
       m_OutputAssign -c 9 -w IPV111 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PV100MBU
       ;;
(PV100MBU)
       m_CondExec 00,EQ,PV100MBT ${EXACS},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MBX PGM=BPV105     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PV100MBX
       ;;
(PV100MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F89.PV100HM.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F89.PV100HM.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  : PGM D EPURATION DE HV32 + HV33                                
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PV100MCA
       ;;
(PV100MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTHV32D  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV33D  : NAME=RSHV33M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV33D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01D /dev/null
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100MCB
       ;;
(PV100MCB)
       m_CondExec 04,GE,PV100MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV116  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PV100MCD
       ;;
(PV100MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE                                                                       
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE                                                                       
#    RTGA11M  : NAME=RSGA11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV116                                                    
       m_OutputAssign -c 9 -w IPV116 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV116 
       JUMP_LABEL=PV100MCE
       ;;
(PV100MCE)
       m_CondExec 04,GE,PV100MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV117  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100MCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PV100MCG
       ;;
(PV100MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01M  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01M /dev/null
#  TABLE                                                                       
#    RTGA10M  : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10M /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32M  : NAME=RSHV32M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32M /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV117                                                    
       m_OutputAssign -c 9 -w IPV117 IMPRIM
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV117 
       JUMP_LABEL=PV100MCH
       ;;
(PV100MCH)
       m_CondExec 04,GE,PV100MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV100MZA
       ;;
(PV100MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV100MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
