#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD3L.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGNMD3 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/03/04 AT 17.56.51 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GNMD3L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   TRI DES FICHIERS ISSU DE GNMD2 ET GNMD1                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD3LA
       ;;
(GNMD3LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=GNMD3LAA
       ;;
(GNMD3LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------                                                      
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P961/SEM.BTF001AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/P961/SEM.BTF866AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/P961/SEM.BTF867AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/P961/SEM.BTF868AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/P961/SEM.BTF869AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF876AL
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD3LAA.BTF001BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_385 1 CH 385
 /KEYS
   FLD_CH_1_385 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD3LAB
       ;;
(GNMD3LAB)
       m_CondExec 00,EQ,GNMD3LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  TRI DU FICHIER BTF01XY SUR LUI-MEME POUR OMIT TABLES NON DESCENDUS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAD
       ;;
(GNMD3LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GNMD3LAA.BTF001BL
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g ${G_A2} SORTOUT ${DATA}/PTEM/GNMD3LAA.BTF001BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "RTEE03"
 /DERIVEDFIELD CST_7_16 "X-RTGG20"
 /DERIVEDFIELD CST_5_12 "RTYF00"
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /DERIVEDFIELD CST_9_20 "X-RTGA59"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_1_385 1 CH 385
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_6 EQ CST_3_8 OR FLD_CH_1_6 EQ CST_5_12 OR FLD_CH_1_8 EQ CST_7_16 OR FLD_CH_1_8 EQ CST_9_20 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD3LAE
       ;;
(GNMD3LAE)
       m_CondExec 00,EQ,GNMD3LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAG PGM=BTF900     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAG
       ;;
(GNMD3LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A3} FTF600 ${DATA}/PTEM/GNMD3LAA.BTF001BL
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/P961/SEM.BTF01XL
# ******* FICHIER D'EXTRACTION VENANT DE GNMD1                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF873AL
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/GNMD3LAG.BTF002XL
       m_ProgramExec BTF900 
# ********************************************************************         
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BMQ915 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAJ
       ;;
(GNMD3LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A4} FMQ915 ${DATA}/PTEM/GNMD3LAG.BTF002XL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF802AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF901CL
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/GNMD3L1N
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/GNMD3LAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ915 
       JUMP_LABEL=GNMD3LAK
       ;;
(GNMD3LAK)
       m_CondExec 04,GE,GNMD3LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FICHIERS SEQUENTIELS                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAM
       ;;
(GNMD3LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD3LAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD3LAN
       ;;
(GNMD3LAN)
       m_CondExec 16,NE,GNMD3LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  TRI DU FICHIER BTF001BD SUR BTF01XD POUR COMPARAISON DU LENDEMAIN           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAQ
       ;;
(GNMD3LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GNMD3LAA.BTF001BL
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/P961/SEM.BTF01XL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_10_22 "RTEE03"
 /DERIVEDFIELD CST_18_38 "RTFR50"
 /DERIVEDFIELD CST_16_34 "X-RTGA59"
 /DERIVEDFIELD CST_3_8 "RTGA59"
 /DERIVEDFIELD CST_12_26 "X-RTGG20"
 /DERIVEDFIELD CST_14_30 "RTYF00"
 /DERIVEDFIELD CST_6_14 (1,6,EQ,"RTGA69',AND,154,1,EQ,C'N")
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /DERIVEDFIELD CST_5_12 "RTGA65"
 /DERIVEDFIELD CST_8_18 "RTGG50"
 /DERIVEDFIELD CST_20_42 "RTGG40"
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_1_385 1 CH 385
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_6 EQ CST_3_8 OR FLD_CH_1_6 EQ CST_5_12 OR CST_6_14 OR FLD_CH_1_6 EQ CST_8_18 OR FLD_CH_1_6 EQ CST_10_22 OR FLD_CH_1_8 EQ CST_12_26 OR FLD_CH_1_6 EQ CST_14_30 OR FLD_CH_1_8 EQ CST_16_34 OR FLD_CH_1_6 EQ CST_18_38 OR FLD_CH_1_6 EQ CST_20_42 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD3LAR
       ;;
(GNMD3LAR)
       m_CondExec 00,EQ,GNMD3LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FICHIERS SEQUENTIELS                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD3LAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LAT
       ;;
(GNMD3LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD3LAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD3LAU
       ;;
(GNMD3LAU)
       m_CondExec 16,NE,GNMD3LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD3LZA
       ;;
(GNMD3LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD3LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
