#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV001D.ksh                       --- VERSION DU 08/10/2016 22:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPV001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/02/01 AT 16.12.40 BY BURTEC6                      
#    STANDARDS: P  JOBSET: PV001D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV120            ETAT MENSUEL DE PREPARATION DE PAIE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV001DA
       ;;
(PV001DA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PV001DAA
       ;;
(PV001DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTHV32   : NAME=RSHV32D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTHV32 /dev/null
# *****   FICHIER PARAMETRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 125 -g +1 FPP000 ${DATA}/PXX0/F91.BPP120AD
# *****   EDITION DE L ETAT IPV120                                             
       m_OutputAssign -c 9 -w IPV120 IPV120
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV120 
       JUMP_LABEL=PV001DAB
       ;;
(PV001DAB)
       m_CondExec 04,GE,PV001DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
