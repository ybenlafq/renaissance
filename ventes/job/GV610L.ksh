#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV610L.ksh                       --- VERSION DU 08/10/2016 17:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV610 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/05/21 AT 14.59.29 BY PREPA3                       
#    STANDARDS: P  JOBSET: GV610L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGV600 : EXTRACTION DES VENTES DE LA RTGV11 NON TOPEE LIVREES               
#           DONT LA DATE DE LIVRAISON EST INFERIEURE AU SAMEDI PRECEDE         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV610LA
       ;;
(GV610LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV610LAA
       ;;
(GV610LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  DETAILS DES VENTES                                                   
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 FGV600 ${DATA}/PXX0/GV610LAA.BGV600AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV600 
       JUMP_LABEL=GV610LAB
       ;;
(GV610LAB)
       m_CondExec 04,GE,GV610LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   EDITION DES VENTES NON TOPEE LIVREES DEPUIS LE SAMEDI PRECEDENT            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV610LAD PGM=BGV610     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV610LAD
       ;;
(GV610LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  VENTES NON TOPEE LIVREES DEPUIS SAMEDI                               
       m_FileAssign -d SHR -g ${G_A1} FGV600 ${DATA}/PXX0/GV610LAA.BGV600AL
# ******  DATE  JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_OutputAssign -c 9 -w BGV610 FIMP
       m_ProgramExec BGV610 
#  DEPENDANCE PLAN                                                             
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV610LZA
       ;;
(GV610LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV610LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
