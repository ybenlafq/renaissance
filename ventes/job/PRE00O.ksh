#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PRE00O.ksh                       --- VERSION DU 09/10/2016 00:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPRE00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/13 AT 11.13.43 BY BURTECN                      
#    STANDARDS: P  JOBSET: PRE00O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#     PURGE DES HISTO REMISES COMMERCIALES PAR VENDEURS                        
#               RTRE00 RTRE10                                                  
#                                                                              
#           GARDER 1 ANS + L'HISTO EN COURS                                    
#           ==> ON FORCE DANS BETDATC LE MOIS LU DANS FEXERCIC                 
#                  ET ON SOUSTRAIT NJOURS     LU DANS FCARTE                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PRE00OA
       ;;
(PRE00OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=PRE00OAA
       ;;
(PRE00OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   HISTO DETAILS REMISES COMMERCIALES                                   
#    RSRE00O  : NAME=RSRE00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRE00O /dev/null
# *****   HISTO REMISES COMMERCIALES PAR VENDEUR                               
#    RSRE10O  : NAME=RSRE10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSRE10O /dev/null
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 0365 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PRE00OAA
# *****   N� DU MOIS DE L'EXERCICE DARTY : 02                                  
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPRE000 
       JUMP_LABEL=PRE00OAB
       ;;
(PRE00OAB)
       m_CondExec 04,GE,PRE00OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTRE00                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00OAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PRE00OAD
       ;;
(PRE00OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 48 -g +1 SYSREC01 ${DATA}/PXX0/F16.RE00UNO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00OAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSRE00                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PRE00OR0                                     
#                  VERIFIER LE BACKOUT RPRE00O ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00OAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PRE00OAJ
       ;;
(PRE00OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 97 -g +1 SYSREC01 ${DATA}/PXX0/F16.RE10UNO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00OAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   REORGANISATION DU TABLESPACE RSRE10                                        
#                                                                              
#   REPRISE: NON   BACKOUT CORTEX PRE00OR1                                     
#                  VERIFIER LE BACKOUT RPRE00O ET LAISSER INCIDENT NET         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PRE00OAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PRE00OZA
       ;;
(PRE00OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PRE00OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
