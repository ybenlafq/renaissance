#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV182F.ksh                       --- VERSION DU 08/10/2016 13:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGV182 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/10/27 AT 16.23.33 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV182F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR PARIS                                                
#                                                                              
# ****************************************************************             
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV182FA
       ;;
(GV182FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV182FAA
       ;;
(GV182FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAA.BGV180CP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAA.BDA189CP
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MGI OUEST                                            
#                                                                              
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV182FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAD
       ;;
(GV182FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAD.BGV180CO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAD.BDA189CO
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# *******                                                                      
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR LUXEMBOURG                                           
#                                                                              
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV182FAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAG
       ;;
(GV182FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAG.BGV180CX
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAG.BDA189CX
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# *******                                                                      
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR LYON                                                 
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV182FAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAJ
       ;;
(GV182FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAJ.BGV180CY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAJ.BDA189CY
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR LILLE                                                
#                                                                              
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV182FAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAM
       ;;
(GV182FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAM.BGV180CL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAM.BDA189CL
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# ******                                                                       
# **************************************************************               
#                                                                              
#  PASSAGE DU BGV180 POUR METZ                                                 
#                                                                              
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP GV182FAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAQ
       ;;
(GV182FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAQ.BGV180CM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAQ.BDA189CM
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# ******                                                                       
# ***************************************************************              
#                                                                              
#  PASSAGE DU BGV180 POUR MARSEILLE                                            
#                                                                              
# ************************************************************                 
#                                                                              
# ***********************************                                          
# *   STEP GV182FAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAT
       ;;
(GV182FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV14 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV180 ${DATA}/PTEM/GV182FAT.BGV180CD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDA189 ${DATA}/PTEM/GV182FAT.BDA189CD
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV180M
# ******                                                                       
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER FGV180 PARIS                                              
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          

       m_ProgramExec -b BGV180 
       JUMP_LABEL=GV182FAX
       ;;
(GV182FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV182FAA.BGV180CP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GV182FAD.BGV180CO
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GV182FAJ.BGV180CY
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GV182FAM.BGV180CL
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GV182FAQ.BGV180CM
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GV182FAT.BGV180CD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FAX.BGV183EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_7_1 7 CH 1
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FAY
       ;;
(GV182FAY)
       m_CondExec 00,EQ,GV182FAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#  PASSAGE DU BGV183 POUR LE CUMUL                                             
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV182FBA
       ;;
(GV182FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A7} FGV180 ${DATA}/PTEM/GV182FAX.BGV183EP
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FGV183 ${DATA}/PTEM/GV182FBA.BGV183FP
# **********************************************************                   
#                                                                              
#     TRI OMIT DU FICHIER FCR002 : REMONTEE MGI                                
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          

       m_ProgramExec -b BGV183 
       JUMP_LABEL=GV182FBD
       ;;
(GV182FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.CREDITF
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FBD.FGV184DP
# *********************************************************                    
# *                                                                            
# *   SELECTION FORMATAGE DES ENREGISTREMENTS DE CREDITS                       
# *   PGM BGV184 B22                                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV182FBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "CA "
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_6 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FBG
       ;;
(GV182FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A8} FGV184 ${DATA}/PTEM/GV182FBD.FGV184DP
       m_FileAssign -d SHR -g ${G_A9} FDA189 ${DATA}/PTEM/GV182FAA.BDA189CP
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GV182FAD.BDA189CO
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GV182FAJ.BDA189CY
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GV182FAM.BDA189CL
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GV182FAQ.BDA189CM
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GV182FAT.BDA189CD
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GV182FAG.BDA189CX
# ****** TABLE EN ENTREE                                                       
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 FCR003 ${DATA}/PTEM/GV182FBG.FGV184EP
# **********************************************************                   
#                                                                              
#     TRI OMIT DU FICHIER FCR002 : REMONTEE MGI                                
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          

       m_ProgramExec -b BGV184 
       JUMP_LABEL=GV182FBJ
       ;;
(GV182FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F99.CREDITF
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FBJ.FGV182CP
# *********************************************************                    
# *                                                                            
# *   SELECTION FORMATAGE DES ENREGISTREMENTS DE C.A.                          
# *   PGM BGV182 B22                                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV182FBM PGM=BGV182     ** ID=ACI                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "CRE"
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_6 
 /KEYS
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FBM
       ;;
(GV182FBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A16} FGV182 ${DATA}/PTEM/GV182FBJ.FGV182CP
       m_FileAssign -d SHR -g ${G_A17} FDA189 ${DATA}/PTEM/GV182FAA.BDA189CP
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GV182FAD.BDA189CO
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GV182FAJ.BDA189CY
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GV182FAM.BDA189CL
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GV182FAQ.BDA189CM
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GV182FAT.BDA189CD
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GV182FAG.BDA189CX
# ****** TABLE EN ENTREE                                                       
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FCR004 ${DATA}/PTEM/GV182FBM.FGV182DP
       m_ProgramExec BGV182 
# *********************************************************                    
# *                                                                            
# *     EXTRACT DU CA - PGM = BGV189                                           
# *                                                                            
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV182FBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GV182FBQ
       ;;
(GV182FBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A24} FDA189 ${DATA}/PTEM/GV182FAA.BDA189CP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GV182FAD.BDA189CO
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GV182FAJ.BDA189CY
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GV182FAM.BDA189CL
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GV182FAQ.BDA189CM
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GV182FAT.BDA189CD
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GV182FAG.BDA189CX
# ****** TABLE EN ENTREE                                                       
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FGV189 ${DATA}/PTEM/GV182FBQ.BGV189DP
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV183FP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          

       m_ProgramExec -b BGV189 
       JUMP_LABEL=GV182FBT
       ;;
(GV182FBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PTEM/GV182FBA.BGV183FP
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GV182FBG.FGV184EP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FBT.BGV183GP
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV189DP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_19_7 ASCENDING,
   FLD_CH_14_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FBX
       ;;
(GV182FBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/GV182FBQ.BGV189DP
       m_FileAssign -d SHR -g ${G_A34} -C ${DATA}/PTEM/GV182FBM.FGV182DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FBX.BGV189GP
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183GP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_PD_13_7 13 PD 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FCA
       ;;
(GV182FCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A35} SORTIN ${DATA}/PTEM/GV182FBT.BGV183GP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FCA.BGV183LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_7_1 7 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_5 14 CH 5
 /CONDITION CND_1 FLD_CH_7_1 EQ CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FCB
       ;;
(GV182FCB)
       m_CondExec 00,EQ,GV182FCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV186                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GV182FCD
       ;;
(GV182FCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLES EN LECTURE                                                 
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A36} FGV189 ${DATA}/PTEM/GV182FBX.BGV189GP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A37} FGV183 ${DATA}/PTEM/GV182FCA.BGV183LP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186M IGV186
# **********************************************************                   
#  NEW                                                                         
#     TRI DU FICHIER BGV183GP                                                  
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          

       m_ProgramExec -b BGV186 
       JUMP_LABEL=GV182FCG
       ;;
(GV182FCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/GV182FBT.BGV183GP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FCG.BGV183MP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "L"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_14_5 14 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_1 7 CH 1
 /CONDITION CND_1 FLD_CH_7_1 NE CST_1_8 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_8_6 ASCENDING,
   FLD_CH_7_1 ASCENDING,
   FLD_CH_14_5 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FCH
       ;;
(GV182FCH)
       m_CondExec 00,EQ,GV182FCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV196                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GV182FCJ
       ;;
(GV182FCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLE EN LECTURE                                                  
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A39} FGV189 ${DATA}/PTEM/GV182FBX.BGV189GP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A40} FGV183 ${DATA}/PTEM/GV182FCG.BGV183MP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV186M IGV186
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV196 
       JUMP_LABEL=GV182FCK
       ;;
(GV182FCK)
       m_CondExec 04,GE,GV182FCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV197   NEW                                                          
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GV182FCM
       ;;
(GV182FCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLE EN LECTURE                                                  
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A41} FGV189 ${DATA}/PTEM/GV182FBX.BGV189GP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A42} FGV183 ${DATA}/PTEM/GV182FCG.BGV183MP
# *********** 1 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV187 IGV187
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV197 
       JUMP_LABEL=GV182FCN
       ;;
(GV182FCN)
       m_CondExec 04,GE,GV182FCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV198   NEW                                                          
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GV182FCQ
       ;;
(GV182FCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLE EN LECTURE                                                  
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A43} FGV189 ${DATA}/PTEM/GV182FBX.BGV189GP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A44} FGV183 ${DATA}/PTEM/GV182FCG.BGV183MP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV188 IGV188
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV189DP ET FGV182DP                                      
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          

       m_ProgramExec -b BGV198 
       JUMP_LABEL=GV182FCT
       ;;
(GV182FCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/GV182FBQ.BGV189DP
       m_FileAssign -d SHR -g ${G_A46} -C ${DATA}/PTEM/GV182FBM.FGV182DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FCT.BGV187BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_13_7 13 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FCU
       ;;
(GV182FCU)
       m_CondExec 00,EQ,GV182FCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV187                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GV182FCX
       ;;
(GV182FCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLE EN LECTURE                                                  
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A47} FGV189 ${DATA}/PTEM/GV182FCT.BGV187BP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A48} FGV183 ${DATA}/PTEM/GV182FCA.BGV183LP
# *********** 1 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV187 IGV187
# **********************************************************                   
#                                                                              
#     TRI DU FICHIER BGV189DP ET FGV182DP                                      
#                                                                              
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          

       m_ProgramExec -b BGV187 
       JUMP_LABEL=GV182FDA
       ;;
(GV182FDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PTEM/GV182FBQ.BGV189DP
       m_FileAssign -d SHR -g ${G_A50} -C ${DATA}/PTEM/GV182FBM.FGV182DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FDA.BGV187CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_13_7 13 PD 7
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FDB
       ;;
(GV182FDB)
       m_CondExec 00,EQ,GV182FDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV188                                                                
#  ------------                                                                
#  EDITION                                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GV182FDD
       ;;
(GV182FDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********** TABLE EN LECTURE                                                  
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A51} FGV189 ${DATA}/PTEM/GV182FDA.BGV187CP
# ********** FICHIER EN ENTREE                                                 
       m_FileAssign -d SHR -g ${G_A52} FGV183 ${DATA}/PTEM/GV182FCA.BGV183LP
# *********** 2 COPIE VERS DIRECTEUR MAGASIN                                   
       m_OutputAssign -c 9 -w IGV188 IGV188
# *************************************************************                
#  TRI DU FICHIER BGV183F  ET FGV184E                                          
#  EXTRACTION DES VENTES DE CREDITS PAR VENDEUR POUR LA PERIODE                
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP GV182FDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          

       m_ProgramExec -b BGV188 
       JUMP_LABEL=GV182FDG
       ;;
(GV182FDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A53} SORTIN ${DATA}/PTEM/GV182FBA.BGV183FP
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/GV182FBG.FGV184EP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FDG.BGV190AP
# **********************************************************                   
#  TRI DU FICHIER BGV189D  ET FGV182D                                          
#  EXTRACTION DES CA POUR LA PERIODE                                           
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_8_6 8 CH 6
 /KEYS
   FLD_CH_8_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FDJ
       ;;
(GV182FDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PTEM/GV182FBQ.BGV189DP
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/GV182FBM.FGV182DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV182FDJ.BGV190BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_PD_13_7 13 PD 7
 /KEYS
   FLD_CH_7_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_13_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FDK
       ;;
(GV182FDK)
       m_CondExec 00,EQ,GV182FDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV190                                                                
#  ------------                                                                
#  CREATION FICHIER HIT-VENDEURS                                               
#  EXTRACTION CUMUL-CLASSEMENT DES VENDEURS AVEC VOLUMES ET TRC%               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GV182FDM
       ;;
(GV182FDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURES                                                   
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV31 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A57} FGV183 ${DATA}/PTEM/GV182FDG.BGV190AP
       m_FileAssign -d SHR -g ${G_A58} FGV189 ${DATA}/PTEM/GV182FDJ.BGV190BP
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ -g +1 FGV191 ${DATA}/PTEM/GV182FDM.BGV190CP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV190 
       JUMP_LABEL=GV182FDN
       ;;
(GV182FDN)
       m_CondExec 04,GE,GV182FDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DU FICHIER SEQUENTIELS BGV190DP                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV182FDQ PGM=IDCAMS     ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GV182FDQ
       ;;
(GV182FDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV182FDQ.sysin
       m_UtilityExec
# **********************************************************                   
#  TRI DU FICHIER BGV183F  ET FGV184E                                          
#  CLASSEMENT DECROISSANT DES VENDEURS : CREATION BGV190DP                     
#  CE FICHIER EST ENVOYE VIA NOTES                                             
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP GV182FDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GV182FDT
       ;;
(GV182FDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A59} SORTIN ${DATA}/PTEM/GV182FDM.BGV190CP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -t LSEQ SORTOUT ${DATA}/PXX0.F07.BGV190DP
# **********************************************************                   
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_4 111 CH 4
 /KEYS
   FLD_CH_111_4 DESCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV182FDU
       ;;
(GV182FDU)
       m_CondExec 00,EQ,GV182FDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV182FZA
       ;;
(GV182FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV182FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
