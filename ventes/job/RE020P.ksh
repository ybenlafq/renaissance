#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE020P.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRE020 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/12/01 AT 13.24.59 BY BURTECN                      
#    STANDARDS: P  JOBSET: RE020P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  PGM : BRE020                                                                
# ********************************************************************         
#  CREATION DES FICHIERS POUR EDITION                                          
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE020PA
       ;;
(RE020PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE020PAA
       ;;
(RE020PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  APRES PURGE DES TABLES DESTOCK.     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** TABLES DB2                                    
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTRE10   : NAME=RSRE10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTRE10 /dev/null
# ****************************** FIC D'EXTRACTION                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC1 ${DATA}/PTEM/RE020PAA.BRE020AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC2 ${DATA}/PTEM/RE020PAA.BRE021AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC3 ${DATA}/PTEM/RE020PAA.BRE022AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC4 ${DATA}/PTEM/RE020PAA.BRE023AP
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FEXERCIC ${DATA}/CORTEX4.P.MTXTFIX1/FEXERCIC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE020 
       JUMP_LABEL=RE020PAB
       ;;
(RE020PAB)
       m_CondExec 04,GE,RE020PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE020AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAD
       ;;
(RE020PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/RE020PAA.BRE020AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PAD.BRE020BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_169_8 169 CH 8
 /FIELDS FLD_CH_185_8 185 CH 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_177_8 177 CH 8
 /FIELDS FLD_CH_21_2 21 CH 2
 /FIELDS FLD_CH_193_8 193 CH 8
 /FIELDS FLD_CH_16_2 16 CH 2
 /FIELDS FLD_CH_10_1 10 CH 1
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_161_8 161 CH 8
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_CH_209_8 209 CH 8
 /FIELDS FLD_CH_201_8 201 CH 8
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_1 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_16_2 ASCENDING,
   FLD_CH_18_3 ASCENDING,
   FLD_CH_21_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_161_8,
    TOTAL FLD_CH_169_8,
    TOTAL FLD_CH_177_8,
    TOTAL FLD_CH_185_8,
    TOTAL FLD_CH_193_8,
    TOTAL FLD_CH_201_8,
    TOTAL FLD_CH_209_8
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PAE
       ;;
(RE020PAE)
       m_CondExec 00,EQ,RE020PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE020BP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAG
       ;;
(RE020PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE020PAA.BRE021AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PAG.BRE021BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_74_7 74 PD 7
 /FIELDS FLD_PD_46_7 46 PD 7
 /FIELDS FLD_CH_10_1 10 CH 1
 /FIELDS FLD_CH_14_2 14 CH 2
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_PD_30_8 30 PD 8
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_67_7 67 PD 7
 /FIELDS FLD_CH_19_2 19 CH 2
 /FIELDS FLD_PD_53_7 53 PD 7
 /FIELDS FLD_CH_16_3 16 CH 3
 /FIELDS FLD_PD_38_8 38 PD 8
 /FIELDS FLD_PD_60_7 60 PD 7
 /FIELDS FLD_PD_81_7 81 PD 7
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_1 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_2 ASCENDING,
   FLD_CH_16_3 ASCENDING,
   FLD_CH_19_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_30_8,
    TOTAL FLD_PD_38_8,
    TOTAL FLD_PD_46_7,
    TOTAL FLD_PD_53_7,
    TOTAL FLD_PD_60_7,
    TOTAL FLD_PD_67_7,
    TOTAL FLD_PD_74_7,
    TOTAL FLD_PD_81_7
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PAH
       ;;
(RE020PAH)
       m_CondExec 00,EQ,RE020PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE020CP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAJ
       ;;
(RE020PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE020PAA.BRE022AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PAJ.BRE022BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_103_8 103 PD 8
 /FIELDS FLD_PD_47_8 47 PD 8
 /FIELDS FLD_CH_7_3 07 CH 3
 /FIELDS FLD_PD_95_8 95 PD 8
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_PD_63_8 63 PD 8
 /FIELDS FLD_PD_71_8 71 PD 8
 /FIELDS FLD_PD_79_8 79 PD 8
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_PD_87_8 87 PD 8
 /FIELDS FLD_PD_55_8 55 PD 8
 /FIELDS FLD_CH_21_2 21 CH 2
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING,
   FLD_CH_18_3 ASCENDING,
   FLD_CH_21_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_47_8,
    TOTAL FLD_PD_55_8,
    TOTAL FLD_PD_63_8,
    TOTAL FLD_PD_71_8,
    TOTAL FLD_PD_79_8,
    TOTAL FLD_PD_87_8,
    TOTAL FLD_PD_95_8,
    TOTAL FLD_PD_103_8
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PAK
       ;;
(RE020PAK)
       m_CondExec 00,EQ,RE020PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE023CP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAM
       ;;
(RE020PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RE020PAA.BRE023AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PAM.BRE023BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_79_8 79 PD 8
 /FIELDS FLD_PD_95_8 95 PD 8
 /FIELDS FLD_PD_55_8 55 PD 8
 /FIELDS FLD_PD_87_8 87 PD 8
 /FIELDS FLD_PD_47_8 47 PD 8
 /FIELDS FLD_CH_18_3 18 CH 3
 /FIELDS FLD_PD_63_8 63 PD 8
 /FIELDS FLD_CH_21_2 21 CH 2
 /FIELDS FLD_CH_10_8 10 CH 8
 /FIELDS FLD_PD_103_8 103 PD 8
 /FIELDS FLD_CH_7_3 07 CH 3
 /FIELDS FLD_PD_71_8 71 PD 8
 /KEYS
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_8 ASCENDING,
   FLD_CH_18_3 ASCENDING,
   FLD_CH_21_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_47_8,
    TOTAL FLD_PD_55_8,
    TOTAL FLD_PD_63_8,
    TOTAL FLD_PD_71_8,
    TOTAL FLD_PD_79_8,
    TOTAL FLD_PD_87_8,
    TOTAL FLD_PD_95_8,
    TOTAL FLD_PD_103_8
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PAN
       ;;
(RE020PAN)
       m_CondExec 00,EQ,RE020PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT                                            
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRE020BP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAQ
       ;;
(RE020PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/RE020PAD.BRE020BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE020PAQ.BRE020CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE020PAR
       ;;
(RE020PAR)
       m_CondExec 04,GE,RE020PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAT
       ;;
(RE020PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/RE020PAQ.BRE020CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PAT.FRE020AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PAU
       ;;
(RE020PAU)
       m_CondExec 00,EQ,RE020PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE020PAX
       ;;
(RE020PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PTEM/RE020PAD.BRE020BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FCUMULS ${DATA}/PTEM/RE020PAT.FRE020AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE005A FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE020PAY
       ;;
(RE020PAY)
       m_CondExec 04,GE,RE020PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBA
       ;;
(RE020PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/RE020PAG.BRE021BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE020PBA.BRE021CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE020PBB
       ;;
(RE020PBB)
       m_CondExec 04,GE,RE020PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBD
       ;;
(RE020PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/RE020PBA.BRE021CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PBD.FRE021AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PBE
       ;;
(RE020PBE)
       m_CondExec 00,EQ,RE020PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBG
       ;;
(RE020PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FEXTRAC ${DATA}/PTEM/RE020PAG.BRE021BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FCUMULS ${DATA}/PTEM/RE020PBD.FRE021AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE007 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE020PBH
       ;;
(RE020PBH)
       m_CondExec 04,GE,RE020PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT  POUR IRE008                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBJ
       ;;
(RE020PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/RE020PAJ.BRE022BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE020PBJ.BRE022CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE020PBK
       ;;
(RE020PBK)
       m_CondExec 04,GE,RE020PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBM
       ;;
(RE020PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/RE020PBJ.BRE022CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PBM.FRE022AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PBN
       ;;
(RE020PBN)
       m_CondExec 00,EQ,RE020PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBQ
       ;;
(RE020PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/RE020PAJ.BRE022BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/RE020PBM.FRE022AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE008 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE020PBR
       ;;
(RE020PBR)
       m_CondExec 04,GE,RE020PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT  POUR IRE009                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBT
       ;;
(RE020PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/RE020PAM.BRE023BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE020PBT.BRE023CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE020PBU
       ;;
(RE020PBU)
       m_CondExec 04,GE,RE020PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=RE020PBX
       ;;
(RE020PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/RE020PBT.BRE023CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE020PBX.FRE023AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE020PBY
       ;;
(RE020PBY)
       m_CondExec 00,EQ,RE020PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE020PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=RE020PCA
       ;;
(RE020PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/RE020PAM.BRE023BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A20} FCUMULS ${DATA}/PTEM/RE020PBM.FRE022AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE009 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE020PCB
       ;;
(RE020PCB)
       m_CondExec 04,GE,RE020PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE020PZA
       ;;
(RE020PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE020PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
