#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PX000P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPX000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/27 AT 12.17.30 BY BURTECA                      
#    STANDARDS: P  JOBSET: PX000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPX100                                                                
#  CALCUL DES PRIMES VENDEURS SUR PSE VENDUES                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PX000PA
       ;;
(PX000PA)
       DIMANCH=${DIMANCH}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PX000PAA
       ;;
(PX000PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA40   : NAME=RSGA40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA40 /dev/null
#    RTGA52   : NAME=RSGA52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *****   DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 FDPX100 ${DATA}/PXX0/F07.FDPX100P
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX100 ${DATA}/PTEM/PX000PAA.BPX100AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX100 
       JUMP_LABEL=PX000PAB
       ;;
(PX000PAB)
       m_CondExec 04,GE,PX000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX106                                                                
#  EXTRACTION DES MOUVEMENTS SUR LA RTGS40 POUR LES CODICS GROUPE              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAD
       ;;
(PX000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA62   : NAME=RSGA62,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA62 /dev/null
#    RTGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
# *****   FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 FDPX100 ${DATA}/PXX0/F07.FDPX100P
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX106 ${DATA}/PTEM/PX000PAD.BPX106AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX106 
       JUMP_LABEL=PX000PAE
       ;;
(PX000PAE)
       m_CondExec 04,GE,PX000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX101                                                                
#  EXTRACTION DES MOUVEMENTS SUR LA RTGS40                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAG
       ;;
(PX000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA62   : NAME=RSGA62,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA62 /dev/null
#    RTGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 FDPX100 ${DATA}/PXX0/F07.FDPX100P
# ******  MAJ DE LA DATE DE TRAIT.                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDPX100O ${DATA}/PXX0/F07.FDPX100P
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX101 ${DATA}/PTEM/PX000PAG.BPX101AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX101 
       JUMP_LABEL=PX000PAH
       ;;
(PX000PAH)
       m_CondExec 04,GE,PX000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PROG    BPX122 => CALCUL DE LA RéPARTITION DE LA PRIME LS                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAJ
       ;;
(PX000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00P  : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00P /dev/null
#    RTGA01P  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01P /dev/null
#    RTGA30P  : NAME=RSGA30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA30P /dev/null
#    RTLI00P  : NAME=RSLI00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTLI00P /dev/null
#    RTGV81P  : NAME=RSGV81,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV81P /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  FICHIER  EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A1} FPX122I ${DATA}/PTEM/PX000PAA.BPX100AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PX000PAG.BPX101AP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/PX000PAD.BPX106AP
# ******  FICHIERS EN SORTIES                                                  
# ******  FICHIERS VENDEURS PRIMéS                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX122O ${DATA}/PTEM/PX000PAJ.FPX122AP
# ******  FICHIERS VENDEURS LS                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX122V ${DATA}/PTEM/PX000PAJ.FPX122BP
# ******  FICHIER CUMUL COMPLET + RAZ PRIMES VENDEURS LS DES FAMILLES          
# ******                          SéLECTIONNéES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX200O ${DATA}/PTEM/PX000PAJ.FPX200AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX122 
       JUMP_LABEL=PX000PAK
       ;;
(PX000PAK)
       m_CondExec 04,GE,PX000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FPX122AP                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAM
       ;;
(PX000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PX000PAJ.FPX122AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPX122CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_151_5 151 PD 5
 /FIELDS FLD_PD_161_5 161 PD 5
 /FIELDS FLD_PD_156_5 156 PD 5
 /FIELDS FLD_CH_1_12 1 CH 12
 /FIELDS FLD_PD_146_5 146 PD 5
 /FIELDS FLD_PD_166_5 166 PD 5
 /FIELDS FLD_PD_68_4 68 PD 4
 /KEYS
   FLD_CH_1_12 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_68_4,
    TOTAL FLD_PD_146_5,
    TOTAL FLD_PD_151_5,
    TOTAL FLD_PD_156_5,
    TOTAL FLD_PD_161_5,
    TOTAL FLD_PD_166_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PAN
       ;;
(PX000PAN)
       m_CondExec 00,EQ,PX000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FPX122BP                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAQ
       ;;
(PX000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PX000PAJ.FPX122BP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPX122DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /FIELDS FLD_PD_68_4 68 PD 4
 /FIELDS FLD_PD_156_5 156 PD 5
 /FIELDS FLD_PD_151_5 151 PD 5
 /FIELDS FLD_PD_146_5 146 PD 5
 /FIELDS FLD_PD_166_5 166 PD 5
 /FIELDS FLD_PD_161_5 161 PD 5
 /KEYS
   FLD_CH_1_12 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_68_4,
    TOTAL FLD_PD_146_5,
    TOTAL FLD_PD_151_5,
    TOTAL FLD_PD_156_5,
    TOTAL FLD_PD_161_5,
    TOTAL FLD_PD_166_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PAR
       ;;
(PX000PAR)
       m_CondExec 00,EQ,PX000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FPX122AP                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAT
       ;;
(PX000PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/PX000PAJ.FPX122AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPX200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_146_5 146 PD 5
 /FIELDS FLD_PD_68_4 68 PD 4
 /FIELDS FLD_CH_1_6 1 CH 06
 /FIELDS FLD_PD_161_5 161 PD 5
 /FIELDS FLD_PD_166_5 166 PD 5
 /FIELDS FLD_PD_156_5 156 PD 5
 /FIELDS FLD_PD_151_5 151 PD 5
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_68_4,
    TOTAL FLD_PD_146_5,
    TOTAL FLD_PD_151_5,
    TOTAL FLD_PD_156_5,
    TOTAL FLD_PD_161_5,
    TOTAL FLD_PD_166_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PAU
       ;;
(PX000PAU)
       m_CondExec 00,EQ,PX000PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX123                                                                
#  CALCUL DE LA PRIME _A RéPARTIR ET RAZ DES PRIMES SUR FAMILLES                
#  PARAMéTRéES POUR LE VENDEUR LS.                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PX000PAX
       ;;
(PX000PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A7} FPX122X ${DATA}/PXX0/F07.FPX122DP
       m_FileAssign -d SHR -g ${G_A8} FPX122S ${DATA}/PXX0/F07.FPX200BP
       m_FileAssign -d SHR -g ${G_A9} FPX122T ${DATA}/PXX0/F07.FPX122CP
# *****   FICHIER EN ECRITURE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX123O ${DATA}/PXX0/F07.FPX123AP
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX123 
       JUMP_LABEL=PX000PAY
       ;;
(PX000PAY)
       m_CondExec 04,GE,PX000PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FPX200AP + FPX123AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBA
       ;;
(PX000PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/PX000PAJ.FPX200AP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PXX0/F07.FPX123AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPX200CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_18 "G"
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_101_5 101 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_18 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_49_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5,
    TOTAL FLD_CH_101_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PBB
       ;;
(PX000PBB)
       m_CondExec 00,EQ,PX000PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPX108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBD PGM=BPX108     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBD
       ;;
(PX000PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PV100HP (0)                                                          
       m_FileAssign -d SHR -g +0 FPX100H ${DATA}/PEX0/F07.PX100HP.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX108M ${DATA}/PTEM/PX000PBD.BPX108BP
       m_ProgramExec BPX108 
# ********************************************************************         
#  TRI SUR FICHIER ISSU PGM BPX108                                             
#  REPRISE: OUI.                                                               
#  ATTENTION SI NCGFC (BPX11 PARAMETRE = OUI) TRI SUR CODE VENDEUR             
#  SORT FIELDS=(1,3,A,4,3,A,7,6,A,32,10,A,66,1,A),FORMAT=CH                    
#  SINON                                      TRI SUR LIBELLE VENDEUR          
#  SORT FIELDS=(1,3,A,4,3,A,32,10,A,7,6,A,66,1,A),FORMAT=CH                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBG
       ;;
(PX000PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/PX000PBD.BPX108BP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PX000PBG.BPX109AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "G"
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_7_6 07 CH 06
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PBH
       ;;
(PX000PBH)
       m_CondExec 00,EQ,PX000PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPX109  EDITION DU DETAIL DES VENTES ET DU CUMUL MENSUEL           
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBJ
       ;;
(PX000PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00P  : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00P /dev/null
#  TABLE DES ARTICLES GENERALISE                                               
#    RTGA01P  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01P /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV81P  : NAME=RSGV81,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV81P /dev/null
#  TABLE DES ANOMALIES                                                         
#    RTAN00P  : NAME=RSAN00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTAN00P /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d SHR -g ${G_A13} FPX109J ${DATA}/PXX0/F07.FPX200CP
#                                                                              
       m_FileAssign -d SHR -g ${G_A14} FPX109M ${DATA}/PTEM/PX000PBG.BPX109AP
#  SORTIE FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 IPX109 ${DATA}/PXX0/F07.BPX109MP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX109 
       JUMP_LABEL=PX000PBK
       ;;
(PX000PBK)
       m_CondExec 04,GE,PX000PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV109 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBM
       ;;
(PX000PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.BPX109MP
       m_OutputAssign -c 9 -w IPX109 SORTOUT
# *********************************************************FIN NEW 7/1         
#  TRI SUR FICHIERS ISSU DU REGROUPEMENT JOURNALIER + HISTO                    
#  POUR CONSTITUTION FICHIER HISTORIQUE                                        
#  REPRISE: OUI                                                                
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PX000PBN
       ;;
(PX000PBN)
       m_CondExec 00,EQ,PX000PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP PX000PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBQ
       ;;
(PX000PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/PX000PAJ.FPX200AP
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PXX0/F07.FPX123AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F07.PX100HP.HISTO
# ******  PV100HP (+1)                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.PX100HP.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PBR
       ;;
(PX000PBR)
       m_CondExec 00,EQ,PX000PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPX108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBT PGM=BPX108     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBT
       ;;
(PX000PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR -g ${G_A18} FPX100H ${DATA}/PEX0/F07.PX100HP.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX108M ${DATA}/PTEM/PX000PBT.BPX108AP
       m_ProgramExec BPX108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPX102                                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PX000PBX
       ;;
(PX000PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/PX000PBT.BPX108AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PX000PBX.BPX102AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PBY
       ;;
(PX000PBY)
       m_CondExec 00,EQ,PX000PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX102                                                                
#  CALCUL DES NIVEAUX D AGREGATION                                             
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCA
       ;;
(PX000PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA09 /dev/null
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA20 /dev/null
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA29   : NAME=RSGA29,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA29 /dev/null
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
#    RTGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
#    RTAN00   : NAME=RSAN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTAN00 /dev/null
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A20} FPX100 ${DATA}/PTEM/PX000PBX.BPX102AP
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPX102 ${DATA}/PTEM/PX000PCA.BPX102BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX102 
       JUMP_LABEL=PX000PCB
       ;;
(PX000PCB)
       m_CondExec 04,GE,PX000PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPX103 MAJ DE LA TABLE HV82                   
#  POUR PGM BPX103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCD
       ;;
(PX000PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/PX000PCA.BPX102BP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PX000PCD.BPX103AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PCE
       ;;
(PX000PCE)
       m_CondExec 00,EQ,PX000PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX103       MAJ DE LA TABLE HV82                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCG
       ;;
(PX000PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
#    RTHV82   : NAME=RSHV82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV82 /dev/null
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A22} FPX103 ${DATA}/PTEM/PX000PCD.BPX103AP
# *****   FICHIERS PARAMETRES                                                  
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *****   MAJ DE LA TABLE RTHV82                                               
#    RSHV82   : NAME=RSHV82,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV82 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX103 
       JUMP_LABEL=PX000PCH
       ;;
(PX000PCH)
       m_CondExec 04,GE,PX000PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPX104 POUR PGM BPX104                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCJ
       ;;
(PX000PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/PX000PBT.BPX108AP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PX000PCJ.BPX104AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_21 "     "
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_91_5 91 CH 5
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 OR FLD_CH_171_5 EQ CST_3_21 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PCK
       ;;
(PX000PCK)
       m_CondExec 00,EQ,PX000PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX104      MAJ DE  LA TABLE HV26                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCM
       ;;
(PX000PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV26   : NAME=RSGV26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV26 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A24} FPX104 ${DATA}/PTEM/PX000PCJ.BPX104AP
# *****   TABLE EN MAJ                                                         
#    RSHV26   : NAME=RSHV26,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV26 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX104 
       JUMP_LABEL=PX000PCN
       ;;
(PX000PCN)
       m_CondExec 04,GE,PX000PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE DU FICHIER FPX107 MAJ DE LA HV83 POUR PGM BPX107           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCQ
       ;;
(PX000PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/PX000PCA.BPX102BP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PX000PCQ.BPX107AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PX000PCR
       ;;
(PX000PCR)
       m_CondExec 00,EQ,PX000PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX107             MAJ DE LA RTHV83                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCT
       ;;
(PX000PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****    FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ****    TABLE DES VENDEURS                                                   
#    RTGV81   : NAME=RSGV81,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGV81 /dev/null
# *****   TABLE DES VENDEURS                                                   
#    RTHV82   : NAME=RSHV82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV82 /dev/null
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A26} FPX107 ${DATA}/PTEM/PX000PCQ.BPX107AP
# *****   TABLE RTHV83 HISTORIQUE VENTES                                       
#    RSHV83   : NAME=RSHV83,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV83 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX107 
       JUMP_LABEL=PX000PCU
       ;;
(PX000PCU)
       m_CondExec 04,GE,PX000PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPX121         ETAT DE SYNTHESE DES COMMISSIONS                       
#  REPRISE: OUI                  LE DIMANCHE SOIR                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PX000PCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=PX000PCX
       ;;
(PX000PCX)
       m_CondExec ${EXAEL},NE,YES 1,EQ,$[DIMANCH] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTHV82   : NAME=RSHV82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV82 /dev/null
# *****   FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ****    EDITION DE L ETAT IPX121                                             
       m_OutputAssign -c "*" -w IPX121 IPX121
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPX121 
       JUMP_LABEL=PX000PCY
       ;;
(PX000PCY)
       m_CondExec 04,GE,PX000PCX ${EXAEL},NE,YES 1,EQ,$[DIMANCH] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PX000PZA
       ;;
(PX000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PX000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
