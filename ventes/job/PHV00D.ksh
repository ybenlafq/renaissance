#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV00D.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPHV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 11.52.21 BY BURTECR                      
#    STANDARDS: P  JOBSET: PHV00D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSHV09D RSHV10D RSHV15D                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV00DA
       ;;
(PHV00DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PHV00DAA
       ;;
(PHV00DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPHV00D
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PHV00DAA
       m_ProgramExec IEFBR14 "RDAR,PHV00D.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00DAD
       ;;
(PHV00DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPHV00D
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PHV00DAE
       ;;
(PHV00DAE)
       m_CondExec 00,EQ,PHV00DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE FICHIER HV01 PAR TRI                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAG
       ;;
(PHV00DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.HV01AD
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SORTOUT ${DATA}/PXX0/F91.HV01BKD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00DAH
       ;;
(PHV00DAH)
       m_CondExec 00,EQ,PHV00DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV000 : PURGE DES TABLES RTHV09 RTHV10 ET SEQUENTIEL HV01                 
#                  SELECT DES LIGNES OU DVENTELIVREE >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAJ
       ;;
(PHV00DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d SHR -g +0 HV01S ${DATA}/PXX0/F91.HV01AD
# *****   HISTO VENTE LIVREE PAR FAMILLE                                       
#    RSHV09D  : NAME=RSHV09D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV09D /dev/null
# *****   HISTO VENTE LIVREE PAR CODIC                                         
#    RSHV10D  : NAME=RSHV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV10D /dev/null
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU  EPURE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV01 ${DATA}/PXX0/PHV00DAJ.BPHV00AD
# *****   FICHIER HISTO VENTES PAR FAMILLE SERVANT A RELOADER RTHV09           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FHV09 ${DATA}/PXX0/F91.RELOAD.HV09RD
# *****   FICHIER HISTO VENTES PAR CODIC   SERVANT A RELOADER RTHV10           
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FHV10 ${DATA}/PXX0/F91.RELOAD.HV10RD
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 1126 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00DAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV000 
       JUMP_LABEL=PHV00DAK
       ;;
(PHV00DAK)
       m_CondExec 04,GE,PHV00DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAM
       ;;
(PHV00DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F91.RELOAD.HV09RD
# ******  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                   
#    RSHV09D  : NAME=RSHV09D,MODE=(U,N) - DYNAM=YES                            
# -X-PHV00DR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09D /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00DAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00D_PHV00DAM_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00DAN
       ;;
(PHV00DAN)
       m_CondExec 04,GE,PHV00DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECHARGEMENT DE LA TABLE RTHV10                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAQ
       ;;
(PHV00DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F91.RELOAD.HV10RD
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
#    RSHV10D  : NAME=RSHV10D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV10D /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00DAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00D_PHV00DAQ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00DAR
       ;;
(PHV00DAR)
       m_CondExec 04,GE,PHV00DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV015 : PURGE DE LA TABLE RTHV15                                          
#                  SELECT DES LIGNES OU DVENTECIALE  >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAT
       ;;
(PHV00DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE DE CONTROLE DE REMONTEE DE CAISSE                              
#    RSHV15D  : NAME=RSHV15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15D /dev/null
#                                                                              
# *****   FICHIER DE LOAD                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 FHV15 ${DATA}/PXX0/F91.RELOAD.HV15RD
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 90 JOURS                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00DAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV015 
       JUMP_LABEL=PHV00DAU
       ;;
(PHV00DAU)
       m_CondExec 04,GE,PHV00DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV15                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DAX
       ;;
(PHV00DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F91.RELOAD.HV15RD
# ******  TABLE CONTROLE REMONTEE DE CAISSES                                   
#    RSHV15D  : NAME=RSHV15D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV15D /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00DAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00D_PHV00DAX_RTHV15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00DAY
       ;;
(PHV00DAY)
       m_CondExec 04,GE,PHV00DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER HISTO VENTES/CODIC/LIEU                                      
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DBA
       ;;
(PHV00DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PHV00DAJ.BPHV00AD
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/F91.HV01AD
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_9 11 CH 9
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00DBB
       ;;
(PHV00DBB)
       m_CondExec 00,EQ,PHV00DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PHV00DZA
       ;;
(PHV00DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
