#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GBA00M.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGBA00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/03 AT 09.16.29 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GBA00M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSBA01M RSBA10M RSBA20M RSBA21M RSFT29M             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GBA00MA
       ;;
(GBA00MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GBA00MAA
       ;;
(GBA00MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGBA00M
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GBA00MAA
       m_ProgramExec IEFBR14 "RDAR,GBA00M.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GBA00MAD
       ;;
(GBA00MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGBA00M
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GBA00MAE
       ;;
(GBA00MAE)
       m_CondExec 00,EQ,GBA00MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA000 : ALIMENTATION DES TABLES STATISTIQUES                              
#   SELECTION DES BONS D'ACHATS DONT LE CODE ALIMENTATION DES STATS            
#   DANS LA TABLE DES BA EST A BLANC                                           
#   MAJ DU CODE STATISTIQUE A * DANS LA RTBA01                                 
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STAT MENSUELL RTBA2         
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STST ANNUELLE RTBA2         
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAG
       ;;
(GBA00MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00M  : NAME=RSBA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00M /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01M  : NAME=RSBA01M,MODE=U - DYNAM=YES                                
# -X-GBA00MR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSBA01M /dev/null
# ******* TABLE DES STATS MENSUELLES                                           
#    RSBA20M  : NAME=RSBA20M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA20M /dev/null
# ******* TABLE DES STATS ANNUELLES                                            
#    RSBA21M  : NAME=RSBA21M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA21M /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA000 
       JUMP_LABEL=GBA00MAH
       ;;
(GBA00MAH)
       m_CondExec 04,GE,GBA00MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA001 : EDITION DU RECAPITULATIF DES COMMANDES                            
#   EXTRACTION ET MISE EN FORME DES DONNEES ISSUES DES TABLES                  
#   RTBA01  RTBA00  RTGA30 . CONSTITUTION D'UN FICHIER DESTINE AU              
#   GENERATEUR D'ETAT IBA001                                                   
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAJ
       ;;
(GBA00MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00M  : NAME=RSBA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00M /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01M  : NAME=RSBA01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01M /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30M  : NAME=RSBA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA30M /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******* FICHIER A DESTINATION DU GENERATEUR D'ETAT                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA001 ${DATA}/PTEM/GBA00MAJ.BBA001AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA001 
       JUMP_LABEL=GBA00MAK
       ;;
(GBA00MAK)
       m_CondExec 04,GE,GBA00MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA001AM)                                           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAM
       ;;
(GBA00MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GBA00MAJ.BBA001AM
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00MAM.BBA001BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_15_10 15 CH 10
 /FIELDS FLD_BI_31_6 31 CH 06
 /FIELDS FLD_BI_25_6 25 CH 06
 /FIELDS FLD_BI_37_10 37 CH 10
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_10 ASCENDING,
   FLD_BI_25_6 ASCENDING,
   FLD_BI_31_6 ASCENDING,
   FLD_BI_37_10 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00MAN
       ;;
(GBA00MAN)
       m_CondExec 00,EQ,GBA00MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BBA001CM                                      
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAQ
       ;;
(GBA00MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/GBA00MAM.BBA001BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00MAQ.BBA001CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00MAR
       ;;
(GBA00MAR)
       m_CondExec 04,GE,GBA00MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAT
       ;;
(GBA00MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GBA00MAQ.BBA001CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00MAT.BBA001DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00MAU
       ;;
(GBA00MAU)
       m_CondExec 00,EQ,GBA00MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBA001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MAX
       ;;
(GBA00MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01Y  : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01Y /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71Y  : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71Y /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/GBA00MAM.BBA001BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/GBA00MAT.BBA001DM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00MAY
       ;;
(GBA00MAY)
       m_CondExec 04,GE,GBA00MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA210 : IMPUTATIONS COMPTABLES DES COMMANDES                              
#   GENERER DES ECRITURES POUR LES COMMANDES OU ANNULATION DE COMMANDE         
#   DANS LE SYSTEME COMPTABLE                                                  
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBA
       ;;
(GBA00MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00Y  : NAME=RSBA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00Y /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01M  : NAME=RSBA01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01M /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30Y  : NAME=RSBA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA30Y /dev/null
# ******* TABLE DES                                                            
#    RSFT17   : NAME=RSFT17M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT17 /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29   : NAME=RSFT29M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00   : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00MBA.BBA210M
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA210 
       JUMP_LABEL=GBA00MBB
       ;;
(GBA00MBB)
       m_CondExec 04,GE,GBA00MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA212 : IMPUTATIONS COMPTABLES DES RECEPTIONS                             
#   GENERER DES ECRITURES POUR LES BONS D'ACHAT RECEPTIONNES ET LES            
#   ECARTS CREES LORS DE LA RECETION DES BA DANS LE SYSTEME COMPTABLE          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBD
       ;;
(GBA00MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01M  : NAME=RSBA01M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01M /dev/null
# ******* TABLE DES RECEPTIONS                                                 
#    RSBA10Y  : NAME=RSBA10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA10Y /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29Y  : NAME=RSFT29M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT29Y /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00Y  : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00Y /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00MBD.BBA212M
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA212 
       JUMP_LABEL=GBA00MBE
       ;;
(GBA00MBE)
       m_CondExec 04,GE,GBA00MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS ISSUS DES PGMS BBA210 ET BBA212                            
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBG
       ;;
(GBA00MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER ISSU DU PGM BBA210                                            
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GBA00MBA.BBA210M
# ****** FICHIER ISSU DU PGM BBA212                                            
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GBA00MBD.BBA212M
# ****** FICHIER CONCATEN� A DESTINATION DE GCT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/F89.GBA00M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_4_8 4 PD 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_PD_4_8 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00MBH
       ;;
(GBA00MBH)
       m_CondExec 00,EQ,GBA00MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BBA230 .EDITION DE L ETAT DE SUIVI PARAMETRAGE TIERS                  
# ********************************************************************         
#  CREATION DU FICHIER POUR EDITION                                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBJ
       ;;
(GBA00MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** TABLES DB2                                    
#    RTBA30   : NAME=RSBA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBA30 /dev/null
#    RTFT17   : NAME=RSFT17M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFT17 /dev/null
# ****************************** FIC D'EXTRACTION                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA130 ${DATA}/PTEM/GBA00MBJ.BBA130AM
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE                                                            
# **   N�TACHE =01 + N� CARTE =05 + NOM PGM TBA00                    *         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/BBA230AM
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA230 
       JUMP_LABEL=GBA00MBK
       ;;
(GBA00MBK)
       m_CondExec 04,GE,GBA00MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA130ARM)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBM
       ;;
(GBA00MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GBA00MBJ.BBA130AM
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00MBM.BBA130BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_6 07 CH 06
 /FIELDS FLD_BI_13_2 13 CH 02
 /KEYS
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00MBN
       ;;
(GBA00MBN)
       m_CondExec 00,EQ,GBA00MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT                                            
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BBA130BR                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBQ
       ;;
(GBA00MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/GBA00MBM.BBA130BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00MBQ.BBA130CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00MBR
       ;;
(GBA00MBR)
       m_CondExec 04,GE,GBA00MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBT
       ;;
(GBA00MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GBA00MBQ.BBA130CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00MBT.FBA130AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00MBU
       ;;
(GBA00MBU)
       m_CondExec 00,EQ,GBA00MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MBX
       ;;
(GBA00MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/GBA00MBM.BBA130BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FCUMULS ${DATA}/PTEM/GBA00MBT.FBA130AM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE FMOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA130 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00MBY
       ;;
(GBA00MBY)
       m_CondExec 04,GE,GBA00MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GBA00MZA
       ;;
(GBA00MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GBA00MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
