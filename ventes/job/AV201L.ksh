#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV201L.ksh                       --- VERSION DU 08/10/2016 14:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLAV201 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/22 AT 11.22.27 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AV201L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BAV201 **  COBOL2/DB2  �PURATION DES AVOIRS P�RIM�S                     
#  REPRISE: OUI               ETAT IAV006                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AV201LA
       ;;
(AV201LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=AV201LAA
       ;;
(AV201LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******* SOUS TABLES G�N�RALIS�ES                                             
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE DES AVOIRS EN M.A.J                                            
#    RSAV01   : NAME=RSAV01L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV01 /dev/null
# ******* TABLE DES AVOIRS EN LECTURE                                          
#    RSAV02   : NAME=RSAV02L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV02 /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00   : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* TABLE GCT                                                            
#    RSFT60   : NAME=RSFT60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT60 /dev/null
# ******* TABLE GCT                                                            
#    RSFM20   : NAME=RSFM20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20 /dev/null
# ******* TABLE FX                                                             
#    RSFX00   : NAME=RSFX00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* FIC POUR GENERATEUR D'ETAT (ETAT IAV006)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IAV006 ${DATA}/PNCGL/F61.BAV201AL
# ******* FIC AVOIRS COMMERCIAUX PERIM�S POUR COMPTA GCT                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PXX0/F61.AV201L
# ***********  PARAMETRE DATE                                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *                                                                            
# **************W*A*R*N*I*N*G*****************************************         
# *C EST ICI QUE L ON INDIQUE SI LE TRAITEMANT EST EN SIMUL OU NON             
# *SI SIMUL METTRE SIMU . SI REEL METTRE REEL                                  
# **************W*A*R*N*I*N*G*****************************************         
# *                                                                            
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/AV201LAA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV201 
       JUMP_LABEL=AV201LAB
       ;;
(AV201LAB)
       m_CondExec 04,GE,AV201LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
