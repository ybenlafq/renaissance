#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BMO00G.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGBMO00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/14 AT 15.24.44 BY BURTECA                      
#    STANDARDS: P  JOBSET: BMO00G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CUMUL DES FICHIERS POUR ENVOYER VERS LA GATEWAY                             
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BMO00GA
       ;;
(BMO00GA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2015/04/14 AT 15.24.44 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: BMO00G                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'ITERF COMPT MOBILE'                    
# *                           APPL...: REPGROUP                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BMO00GAA
       ;;
(BMO00GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BMO020BD.GETAWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BMO020BL.GETAWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BMO020BM.GETAWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BMO020BO.GETAWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BMO020BP.GETAWAY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BMO020BY.GETAWAY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BMO020BG.GATEWAY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BMO00GAB
       ;;
(BMO00GAB)
       m_CondExec 00,EQ,BMO00GAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER VERS                                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=BMO020BG,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=BMO020CG,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -ZIPPED_DSN(PXX0.F99.BMO020BG.*,BMO020BG.TXT)                               
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00GAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GAD
       ;;
(BMO00GAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BMO00GAD.FTBMO01G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00GAG PGM=JVMLDM76   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GAG
       ;;
(BMO00GAG)
       m_CondExec ${EXAAK},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F99.BMO020BG.GATEWAY
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F99.BMO020CG.ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GAG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FICHIER VERS                                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BMO020CG                                                            
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  CHANGEMENT PCL => BMO00G <= CHANGEMENT PCL                                  
# ###############################################################              
# ###########           DCLF _A CR��R                    #########              
# ###############################################################              
# FTBMO00G  DCLF LRECL=080,RECNB=5,CLASS=XP,RECFM=FB                           
# ###############################################################              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTBMO00G                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00GAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GAJ
       ;;
(BMO00GAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BMO00GAJ.FTBMO00G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBMO00G                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00GAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GAM
       ;;
(BMO00GAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.BMO00GAJ.FTBMO00G(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP BMO00GAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GAQ
       ;;
(BMO00GAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BMO00GZA
       ;;
(BMO00GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
