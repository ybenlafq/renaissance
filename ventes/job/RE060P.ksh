#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE060P.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRE060 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/02 AT 10.29.45 BY PREPA2                       
#    STANDARDS: P  JOBSET: RE060P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BRE060  CONSTITUTION DE LA SYSIN POUR FAST UNLOAD .                
#   WARNING : LE FICHIER FPARAM EST EN DUR DANS LE PCL .                       
#              M = MENSUEL OU H = HEBDO                                        
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE060PA
       ;;
(RE060PA)
#
#RE060PBJ
#RE060PBJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#RE060PBJ
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RAAF=${RAAF:-RE060PAD}
       RUN=${RUN}
       JUMP_LABEL=RE060PAA
       ;;
(RE060PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  FPARAM M = MENSUEL OU H = HEBDO                                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE060PAA
       m_FileAssign -d SHR FLOGIS ${DATA}/CORTEX4.P.MTXTFIX1/FLOGISP
#                                                                              
#  CONSTITUTION DE LA SYSIN POUR UNLOAD                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX2/RE60HUNL.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRE060 ${DATA}/PTEM/RE060PAA.BRE060AP
       m_ProgramExec BRE060 
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAD
       ;;
(RE060PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SYSREC01 ${DATA}/PTEM/RE060PAD.UNLGS40P
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SYSREC02 ${DATA}/PTEM/RE060PAD.UNLGV10P
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SYSREC03 ${DATA}/PTEM/RE060PAD.UNLGV11P
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/RE060PAA.BRE060AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SI CODE RETOUR > 04  ABEND                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAG PGM=ZUTABEND   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAG
       ;;
(RE060PAG)
       m_CondExec ${EXAAK},NE,YES 04,GE,$[RAAF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAJ
       ;;
(RE060PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE060PAD.UNLGS40P
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PTEM/RE060PAJ.GS40AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 1 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /* Record Type = F  Record Length = 073 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060PAK
       ;;
(RE060PAK)
       m_CondExec 00,EQ,RE060PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAM
       ;;
(RE060PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE060PAD.UNLGV11P
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PTEM/RE060PAM.GV11AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 094 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060PAN
       ;;
(RE060PAN)
       m_CondExec 00,EQ,RE060PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   LIEU  SUR 4            N� DE VENTE SUR 7 :                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAQ
       ;;
(RE060PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RE060PAD.UNLGV10P
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/RE060PAQ.GV10AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 011 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060PAR
       ;;
(RE060PAR)
       m_CondExec 00,EQ,RE060PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE061 .CE PGM COMPARE LES DEUX FICHIERS D UNLOAD                      
#   IL CHARGE LE FIC FRE061 AVEC LES DONNEES ABSENTES DE RTGS40                
#   ET CHARGE SUR FREMIS TOUS LES CODES REMISES RENCONTRES                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAT PGM=BRE061     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAT
       ;;
(RE060PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
#  FICHIER D UNLOAD RTGV10 TRIE                                                
       m_FileAssign -d SHR -g ${G_A5} RTGV10 ${DATA}/PTEM/RE060PAQ.GV10AP
#                                                                              
#  FICHIER D UNLOAD RTGV11 TRIE                                                
       m_FileAssign -d SHR -g ${G_A6} RTGV11 ${DATA}/PTEM/RE060PAM.GV11AP
#                                                                              
#  FICHIER DE SORTIE DE LRECL 110                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 FRE061 ${DATA}/PTEM/RE060PAT.BRE061AP
       m_ProgramExec BRE061 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER BRE061AP                                                   
#   CODIC + LIEU + VENTE + VENDEUR                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE060PAX
       ;;
(RE060PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE060PAT.BRE061AP
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 SORTOUT ${DATA}/PTEM/RE060PAX.BRE061BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_6 24 CH 6
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_6 ASCENDING
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060PAY
       ;;
(RE060PAY)
       m_CondExec 00,EQ,RE060PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE062 .CE PGM ALIGNE LES REMISES DE TOUTES LES VENTES A PARTI         
#   DE FREMIS ET PREPARE UN FRE062 :                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBA
       ;;
(RE060PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA52   : NAME=RSGA52,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA52 /dev/null
#  TABLE                                                                       
#    RTGA59   : NAME=RSGA59,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA59 /dev/null
#  TABLE                                                                       
#    RTGA65   : NAME=RSGA65,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA65 /dev/null
#  TABLE                                                                       
#    RTGG20   : NAME=RSGG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG20 /dev/null
#  TABLE                                                                       
#    RTGG50   : NAME=RSGG50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
#  TABLE                                                                       
#    RTPR00   : NAME=RSPR00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPR00 /dev/null
#  TABLE                                                                       
#    RTPR10   : NAME=RSPR10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTPR10 /dev/null
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRE061 ${DATA}/PTEM/RE060PAX.BRE061BP
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/RE060PAJ.GS40AP
#                                                                              
#    FICHIER EN SORTIE DE LRECL 140                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FRE062 ${DATA}/PXX0/F07.RESULTAT.HISTO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE062 
       JUMP_LABEL=RE060PBB
       ;;
(RE060PBB)
       m_CondExec 04,GE,RE060PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RE060PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBD
       ;;
(RE060PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F07.RESULTAT.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PTEM/RE060PBD.BRE062BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_112_8 112 CH 8
 /FIELDS FLD_CH_88_1 88 CH 1
 /FIELDS FLD_CH_68_5 68 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_CH_89_5 89 CH 5
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_CH_78_5 78 CH 5
 /FIELDS FLD_CH_73_5 73 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_CH_21_6 21 CH 6
 /FIELDS FLD_CH_99_5 99 CH 5
 /FIELDS FLD_CH_63_5 63 CH 5
 /FIELDS FLD_CH_58_5 58 CH 5
 /FIELDS FLD_CH_94_2 94 CH 2
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_PD_104_8 104 PD 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_21_6 ASCENDING,
   FLD_CH_58_5 ASCENDING,
   FLD_CH_63_5 ASCENDING,
   FLD_CH_68_5 ASCENDING,
   FLD_CH_73_5 ASCENDING,
   FLD_CH_78_5 ASCENDING,
   FLD_CH_83_5 ASCENDING,
   FLD_CH_88_1 ASCENDING,
   FLD_CH_89_5 ASCENDING,
   FLD_CH_94_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_99_5,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8
 /* Record Type = F  Record Length = 071 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_11_3,FLD_CH_21_6,FLD_CH_58_5,FLD_CH_63_5,FLD_CH_68_5,FLD_CH_73_5,FLD_CH_78_5,FLD_CH_83_5,FLD_CH_88_1,FLD_CH_89_5,FLD_CH_94_2,FLD_CH_99_5,FLD_CH_104_8,FLD_CH_112_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE060PBE
       ;;
(RE060PBE)
       m_CondExec 00,EQ,RE060PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DSNUTILB                                                                   
#   LOAD DE LA TABLE RTRE60                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBG
       ;;
(RE060PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ****** FICHIER DES DONNEES.                                                  
       m_FileAssign -d SHR -g ${G_A11} SYSREC ${DATA}/PTEM/RE060PBD.BRE062BP
# ****** TABLE RESULTAT                                                        
#    RSRE60   : NAME=RSRE60,MODE=(U,N) - DYNAM=YES                             
# -X-RSRE60   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSRE60 /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060PBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/RE060P_RE060PBG_RTRE60.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE060PBH
       ;;
(RE060PBH)
       m_CondExec 04,GE,RE060PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPAIR NOCOPY DU TABLESPACE RSRE060                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBM
       ;;
(RE060PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060P1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060P1 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060PBN
       ;;
(RE060PBN)
       m_CondExec 04,GE,RE060PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE604 (DETAIL DU CA ET DES REMISES)                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBQ
       ;;
(RE060PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE604 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060P2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060P2 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060PBR
       ;;
(RE060PBR)
       m_CondExec 04,GE,RE060PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE605 (FICHIER DES VENDEURS)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBT
       ;;
(RE060PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE605 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060P3
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060PBU
       ;;
(RE060PBU)
       m_CondExec 04,GE,RE060PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : REQUETE QRE603D (ETAT DES REMISES PAR TYPE DE REMISE)            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=RE060PBX
       ;;
(RE060PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603D DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060P4
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060P4 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060PBY
       ;;
(RE060PBY)
       m_CondExec 04,GE,RE060PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE060PZA
       ;;
(RE060PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
