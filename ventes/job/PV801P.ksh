#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV801P.ksh                       --- VERSION DU 09/10/2016 05:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPV801 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.11.53 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV801P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV800                                                                
#  ------------                                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PV801PA
       ;;
(PV801PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2013/07/05 AT 08.11.53 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: PV801P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'CREATION FIC BIRD'                     
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PV801PAA
       ;;
(PV801PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************************************         
#                                                                              
#  FIC ISSU DU PCL PV800R OU PV500R                                            
       m_FileAssign -d SHR -g +0 FPV500I ${DATA}/PNCGP/F07.PV800PED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.PV800LED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.PV800MED
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/F08.PV800XED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.PV800OED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.PV800YED
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.PV800DED
#  SORTIE :202 DE LONG  FIC POUR BIRD                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 219 -t LSEQ -g +1 FPV800O ${DATA}/PNCGP/F07.BPVBIRDP
       m_ProgramExec BPV800 
#                                                                              
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY POUR QLIKVIEW                                    
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BIRDQLKW,                                                           
#      FNAME=":BPVBIRDP""(0),                                                  
#      NFNAME=R_PS_PRIME_COMM_REFIN_&FDATE.TXT                                 
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTPV801P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV801PAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV801PAD
       ;;
(PV801PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV801PAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/PV801PAD.FTPV801P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPV801P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV801PAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV801PAG
       ;;
(PV801PAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PV801PAG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.PV801PAD.FTPV801P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PV801PAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV801PAJ
       ;;
(PV801PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV801PAJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV801PZA
       ;;
(PV801PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV801PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
