#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV510Y.ksh                       --- VERSION DU 08/10/2016 22:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGV510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/17 AT 08.56.52 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV510Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV514  POSITIONNE UN FLAG SUR LES ENTETES DE VENTE NON NEM A EPURE         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV510YA
       ;;
(GV510YA)
#
#GV510YAD
#GV510YAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV510YAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV510YAA
       ;;
(GV510YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV10Y  : NAME=RSGV10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510YAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV514 
       JUMP_LABEL=GV510YAB
       ;;
(GV510YAB)
       m_CondExec 04,GE,GV510YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU  TABLESPACE : RSMQ15Y                                            
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510YAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV510YAG
       ;;
(GV510YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510YAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GV510YAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15Y  : NAME=RSMQ15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15Y /dev/null
# ******  FICHIER LG 19 EPURATION DES RESERVATIONS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FTS053 ${DATA}/PXX0/F45.BNM153AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GV510YAH
       ;;
(GV510YAH)
       m_CondExec 04,GE,GV510YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV517  SUPPRESSION DANS LA TABLE RTGV10                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV510YAJ
       ;;
(GV510YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10Y  : NAME=RSGV10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
#    RSNV20   : NAME=RSNV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV20 /dev/null
# ******  FICHIER MAGASIN NEM                                                  
       m_FileAssign -d SHR -g ${G_A1} FVTENP ${DATA}/PXX0/F45.BNM153AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV517 
       JUMP_LABEL=GV510YAK
       ;;
(GV510YAK)
       m_CondExec 04,GE,GV510YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV516   ENVOIE MESSAGE MQSERIES DANS LA QUEUE LOCAL HOST                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV510YAM
       ;;
(GV510YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10Y  : NAME=RSGV10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510YAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV516 
       JUMP_LABEL=GV510YAN
       ;;
(GV510YAN)
       m_CondExec 04,GE,GV510YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  NON IMPERATIVE CAR ON PEUT ANNULER LES PRE-RESERVATION LE LENDEMAIN         
#  PAS DE BACKOUT CAR LKES DEUX PROGS NE SONT PAS LIES DANS LA LOGIQUE         
#  ON PEUT EN PASSER UN SANS PASSER L'AUTRE ET VICE VERSA                      
# ********************************************************************         
#  BGV515  DERESERVATION STOCKS ET QUOTAS + SUPPRESSION SUR TABLES             
#          MOUCHARD DES VENTES NON VALIDEES DONT LA DATE DE VENTE EST          
#          < OU = A LA DATE DU TRAITEMENT MOINS N JOURS                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510YAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV510YAQ
       ;;
(GV510YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE MOUCHARD DES RESERVATIONS STOCKS                               
#    RSGV40Y  : NAME=RSGV40Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV40Y /dev/null
# ******  TABLE MOUCHARD DES RESERVATIONS QUOTAS                               
#    RSGV41Y  : NAME=RSGV41Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV41Y /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  TABLE DES EN-TETES DE MUTATION                                       
#    RSGB05Y  : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05Y /dev/null
# ******  TABLE DES MUTATIONS DETAIL                                           
#    RSGB15Y  : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15Y /dev/null
# ******  TABLE DES LIGNES COMMANDES SPECIFIQUES                               
#    RSGF00Y  : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00Y /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (EMPORTE)                             
#    RSGQ02Y  : NAME=RSGQ02Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ02Y /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (LIVRE)                               
#    RSGQ03Y  : NAME=RSGQ03Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ03Y /dev/null
# ******  TABLE DES STOCKS ENTREPOT                                            
#    RSGS10Y  : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10Y /dev/null
# ******  TABLE DU JUSTIFICATIF DU LIEU DE TRANSIT                             
#    RSGS20Y  : NAME=RSGS20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS20Y /dev/null
# ******  TABLE DES STOCKS MAGASINS                                            
#    RSGS30Y  : NAME=RSGS30Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  TABLE DES                                                            
#    RSGS43Y  : NAME=RSGS43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43Y /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS ENTREPOT                           
#    RSGV21Y  : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21Y /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS MAGASINS                           
#    RSGV22Y  : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22Y /dev/null
# ******  TABLE VENTES                                                         
#    RSGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  TABLE DES LIAISONS ARTICLES                                          
#    RSGA58Y  : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58Y /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE FNSOC                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  DELAI DE PURGE                                                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510YAQ
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV515 
       JUMP_LABEL=GV510YAR
       ;;
(GV510YAR)
       m_CondExec 04,GE,GV510YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV518  SUPPRIME LES VENTES NON VALIDEES (NON ENCAISSEES)                   
#          DONT LA DATE DE VENTE EST                                           
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
#          EX : LE CLIENT N EST PAS PASSE EN CAISSE CAR TROP DE MONDE          
# ********************************************************************         
#  REPRISE: OUI                                                                
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV510YAT
       ;;
(GV510YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02Y  : NAME=RSGV02Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02Y /dev/null
# ******  TABLE GV03                                                           
#    RSGV03Y  : NAME=RSGV03Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03Y /dev/null
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10Y  : NAME=RSGV10Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11Y  : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
# ******  TABLE DES PRESTATIONS DE VENTES                                      
#    RSGV13Y  : NAME=RSGV13Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13Y /dev/null
# ******  TABLE DES CARACTERISTIQUES ARTICLES DES VENTES                       
#    RSGV15Y  : NAME=RSGV15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15Y /dev/null
# ******  TABLE DES VENTES : AUTORISATIONS PREL STOCK PAR ENTREPOT             
#    RSGV20Y  : NAME=RSGV20Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV20Y /dev/null
# ******  TABLE DES VENTES : AUTORISATIONS PREL STOCK PAR ENTREPOT             
#    RSGV27Y  : NAME=RSGV27Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27Y /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSGV23Y  : NAME=RSGV23Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23Y /dev/null
# ******  TABLE PSE                                                            
#    RSPS00Y  : NAME=RSPS00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00Y /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510YAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV518 
       JUMP_LABEL=GV510YAU
       ;;
(GV510YAU)
       m_CondExec 04,GE,GV510YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
