#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BMO00P.ksh                       --- VERSION DU 08/10/2016 12:46
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBMO00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/11/02 AT 11.55.30 BY BURTEC2                      
#    STANDARDS: P  JOBSET: BMO00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER ISSU DE LA CHAINE BS001P (PGM BBS001)                        
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BMO00PA
       ;;
(BMO00PA)
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BMO00PAA
       ;;
(BMO00PAA)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BBS001BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BMO010AP.RECYCLE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/BMO00PAA.BMO010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BMO00PAB
       ;;
(BMO00PAB)
       m_CondExec 00,EQ,BMO00PAA ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMO010 : INTERFACE COMPTABLE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00PAD PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BMO00PAD
       ;;
(BMO00PAD)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSPR00   : NAME=RSPR00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSMO01   : NAME=RSMO01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSMO01 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER ISSU DE LA CHAINE BS001P (PGM BBS001)                        
       m_FileAssign -d SHR -g ${G_A1} FBMO10 ${DATA}/PTEM/BMO00PAA.BMO010AP
#                                                                              
# ******* FICHIER DE RECYCLAGE DU JOUR  (LRECL 400)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FRECYC ${DATA}/PXX0/F07.BMO010AP.RECYCLE
# ***                                                                          
       m_OutputAssign -c 9 -w FANO02 FANO02
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMO010 
       JUMP_LABEL=BMO00PAE
       ;;
(BMO00PAE)
       m_CondExec 04,GE,BMO00PAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMO020 : INTERFACE COMPTABLE                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BMO00PAG PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BMO00PAG
       ;;
(BMO00PAG)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN ENTREE                                                     
#                                                                              
#    RSMO01   : NAME=RSMO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSMO01 /dev/null
#                                                                              
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER POUR LA CHAINE FTI01P (LRECL 400)                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FBMO20 ${DATA}/PXX0/F07.BMO020AP.FTI01P
#                                                                              
# ******* FICHIER A DESTINATION DE GETAWAY                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FBMO30 ${DATA}/PXX0/F07.BMO020BP.GETAWAY
# ***                                                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMO020 
       JUMP_LABEL=BMO00PAH
       ;;
(BMO00PAH)
       m_CondExec 04,GE,BMO00PAG ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BMO00PZA
       ;;
(BMO00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BMO00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
