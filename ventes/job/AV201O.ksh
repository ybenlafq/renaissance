#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV201O.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POAV201 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/22 AT 11.23.10 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AV201O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BAV201 **  COBOL2/DB2  �PURATION DES AVOIRS P�RIM�S                     
#  REPRISE: OUI               ETAT IAV006                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AV201OA
       ;;
(AV201OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=AV201OAA
       ;;
(AV201OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
# ******* SOUS TABLES G�N�RALIS�ES                                             
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE DES AVOIRS EN M.A.J                                            
#    RSAV01O  : NAME=RSAV01O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV01O /dev/null
# ******* TABLE DES AVOIRS EN LECTURE                                          
#    RSAV02O  : NAME=RSAV02O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAV02O /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ******* TABLE GCT                                                            
#    RSFT60O  : NAME=RSFT60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT60O /dev/null
# ******* TABLE GCT                                                            
#    RSFM20O  : NAME=RSFM20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM20O /dev/null
# ******* TABLE FX                                                             
#    RSFX00O  : NAME=RSFX00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00O /dev/null
# ******* FIC POUR GENERATEUR D'ETAT (ETAT IAV006)                             
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IAV006 ${DATA}/PNCGO/F16.BAV201AO
# ******* FIC AVOIRS COMMERCIAUX PERIM�S POUR COMPTA GCT                       
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PXX0/F16.AV201O
# ***********  PARAMETRE DATE                                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *                                                                            
# **************W*A*R*N*I*N*G*****************************************         
# *C EST ICI QUE L ON INDIQUE SI LE TRAITEMANT EST EN SIMUL OU NON             
# *SI SIMUL METTRE SIMU . SI REEL METTRE REEL                                  
# **************W*A*R*N*I*N*G*****************************************         
# *                                                                            
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/AV201OAA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV201 
       JUMP_LABEL=AV201OAB
       ;;
(AV201OAB)
       m_CondExec 04,GE,AV201OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
