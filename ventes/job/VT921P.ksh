#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VT921P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVT921 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/04 AT 14.57.10 BY BURTECA                      
#    STANDARDS: P  JOBSET: VT921P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ POUR LA PARTIE VENTES AVEC ADRESSES GV         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VT921PA
       ;;
(VT921PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VT921PAA
       ;;
(VT921PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# PRED17   LINK  NAME=$SA202P,MODE=I                                           
# **************************************                                       
#    RSVT00   : NAME=RSVT00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT00 /dev/null
#    RSVT01   : NAME=RSVT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT01 /dev/null
#    RSVT02   : NAME=RSVT02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT02 /dev/null
#    RSVT05   : NAME=RSVT05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT05 /dev/null
#    RSVT06   : NAME=RSVT06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT06 /dev/null
#    RSVT08   : NAME=RSVT08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT08 /dev/null
#    RSVT11   : NAME=RSVT11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT11 /dev/null
#    RSVT12   : NAME=RSVT12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT12 /dev/null
#    RSVT13   : NAME=RSVT13,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT13 /dev/null
#    RSVT14   : NAME=RSVT14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT14 /dev/null
#    RSVT15   : NAME=RSVT15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT15 /dev/null
#    RSVT16   : NAME=RSVT16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT16 /dev/null
#    RSVT17   : NAME=RSVT17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT17 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PARG/SEM.VT921PA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VT921PAA
       m_ProgramExec IEFBR14 "RDAR,VT921P.UX"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT921PAD
       ;;
(VT921PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 SORTOUT ${DATA}/PTEM/VT921PAD.VT921P01
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAE
       ;;
(VT921PAE)
       m_CondExec 00,EQ,VT921PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV05                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAG
       ;;
(VT921PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 SORTOUT ${DATA}/PTEM/VT921PAG.VT921P02
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAH
       ;;
(VT921PAH)
       m_CondExec 00,EQ,VT921PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV06                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAJ
       ;;
(VT921PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/VT921PAJ.VT921P03
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAK
       ;;
(VT921PAK)
       m_CondExec 00,EQ,VT921PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV08                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAM
       ;;
(VT921PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 SORTOUT ${DATA}/PTEM/VT921PAM.VT921P04
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAN
       ;;
(VT921PAN)
       m_CondExec 00,EQ,VT921PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV23                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAQ
       ;;
(VT921PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900ML
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 SORTOUT ${DATA}/PTEM/VT921PAQ.VT921P05
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAR
       ;;
(VT921PAR)
       m_CondExec 00,EQ,VT921PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV11                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAT
       ;;
(VT921PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SORTOUT ${DATA}/PTEM/VT921PAT.VT921P06
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_75_2 75 CH 2
 /KEYS
   FLD_CH_1_13 ASCENDING,
   FLD_CH_75_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAU
       ;;
(VT921PAU)
       m_CondExec 00,EQ,VT921PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTGV14                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VT921PAX
       ;;
(VT921PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VT900YJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900PJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900LJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900MJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900DJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900RJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900OJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VT900XJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 SORTOUT ${DATA}/PTEM/VT921PAX.VT921P07
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PAY
       ;;
(VT921PAY)
       m_CondExec 00,EQ,VT921PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVT922 :ARCHIVAGE DES VENTES GV                                             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBA
       ;;
(VT921PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSVT00   : NAME=RSVT00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT00 /dev/null
#    RSVT01   : NAME=RSVT01,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT01 /dev/null
#    RSVT05   : NAME=RSVT05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT05 /dev/null
#    RSVT06   : NAME=RSVT06,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT06 /dev/null
#    RSVT08   : NAME=RSVT08,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT08 /dev/null
#    RSVT11   : NAME=RSVT11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT11 /dev/null
#    RSVT12   : NAME=RSVT12,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT12 /dev/null
#    RSVT13   : NAME=RSVT13,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT13 /dev/null
#    RSVT14   : NAME=RSVT14,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT14 /dev/null
#    RSVT15   : NAME=RSVT15,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT15 /dev/null
#    RSVT16   : NAME=RSVT16,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT16 /dev/null
#    RSVT17   : NAME=RSVT17,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT17 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g ${G_A1} FVT020 ${DATA}/PTEM/VT921PAD.VT921P01
       m_FileAssign -d SHR -g ${G_A2} FVT005 ${DATA}/PTEM/VT921PAG.VT921P02
       m_FileAssign -d SHR -g ${G_A3} FVT006 ${DATA}/PTEM/VT921PAJ.VT921P03
       m_FileAssign -d SHR -g ${G_A4} FVT008 ${DATA}/PTEM/VT921PAM.VT921P04
       m_FileAssign -d SHR -g ${G_A5} FVT100 ${DATA}/PTEM/VT921PAQ.VT921P05
       m_FileAssign -d SHR -g ${G_A6} FVT110 ${DATA}/PTEM/VT921PAT.VT921P06
       m_FileAssign -d SHR -g ${G_A7} FVT140 ${DATA}/PTEM/VT921PAX.VT921P07
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT922 
       JUMP_LABEL=VT921PBB
       ;;
(VT921PBB)
       m_CondExec 04,GE,VT921PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES GDG                                                       
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBD PGM=IDCAMS     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBD
       ;;
(VT921PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
       m_FileAssign -d SHR IN41 /dev/null
       m_FileAssign -d SHR IN42 /dev/null
       m_FileAssign -d SHR IN43 /dev/null
       m_FileAssign -d SHR IN44 /dev/null
       m_FileAssign -d SHR IN45 /dev/null
       m_FileAssign -d SHR IN46 /dev/null
       m_FileAssign -d SHR IN47 /dev/null
       m_FileAssign -d SHR IN48 /dev/null
       m_FileAssign -d SHR IN49 /dev/null
       m_FileAssign -d SHR IN50 /dev/null
       m_FileAssign -d SHR IN51 /dev/null
       m_FileAssign -d SHR IN52 /dev/null
       m_FileAssign -d SHR IN53 /dev/null
       m_FileAssign -d SHR IN54 /dev/null
       m_FileAssign -d SHR IN55 /dev/null
       m_FileAssign -d SHR IN56 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT1 ${DATA}/PARG/SEM.VT900YF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT2 ${DATA}/PARG/SEM.VT900PF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT3 ${DATA}/PARG/SEM.VT900LF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT4 ${DATA}/PARG/SEM.VT900MF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT5 ${DATA}/PARG/SEM.VT900DF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT6 ${DATA}/PARG/SEM.VT900RF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT7 ${DATA}/PARG/SEM.VT900OF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT8 ${DATA}/PARG/SEM.VT900XF
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT9 ${DATA}/PARG/SEM.VT900YG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT10 ${DATA}/PARG/SEM.VT900PG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT11 ${DATA}/PARG/SEM.VT900LG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT12 ${DATA}/PARG/SEM.VT900MG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT13 ${DATA}/PARG/SEM.VT900DG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT14 ${DATA}/PARG/SEM.VT900RG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT15 ${DATA}/PARG/SEM.VT900OG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT16 ${DATA}/PARG/SEM.VT900XG
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT17 ${DATA}/PARG/SEM.VT900YH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT18 ${DATA}/PARG/SEM.VT900PH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT19 ${DATA}/PARG/SEM.VT900LH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT20 ${DATA}/PARG/SEM.VT900MH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT21 ${DATA}/PARG/SEM.VT900DH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT22 ${DATA}/PARG/SEM.VT900RH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT23 ${DATA}/PARG/SEM.VT900OH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT24 ${DATA}/PARG/SEM.VT900XH
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT25 ${DATA}/PARG/SEM.VT900YI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT26 ${DATA}/PARG/SEM.VT900PI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT27 ${DATA}/PARG/SEM.VT900LI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT28 ${DATA}/PARG/SEM.VT900MI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT29 ${DATA}/PARG/SEM.VT900DI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT30 ${DATA}/PARG/SEM.VT900RI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT31 ${DATA}/PARG/SEM.VT900OI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT32 ${DATA}/PARG/SEM.VT900XI
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT33 ${DATA}/PARG/SEM.VT900YL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT34 ${DATA}/PARG/SEM.VT900PL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT35 ${DATA}/PARG/SEM.VT900LL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT36 ${DATA}/PARG/SEM.VT900ML
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT37 ${DATA}/PARG/SEM.VT900DL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT38 ${DATA}/PARG/SEM.VT900RL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT39 ${DATA}/PARG/SEM.VT900OL
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT40 ${DATA}/PARG/SEM.VT900XL
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT41 ${DATA}/PARG/SEM.VT900YC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT42 ${DATA}/PARG/SEM.VT900PC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT43 ${DATA}/PARG/SEM.VT900LC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT44 ${DATA}/PARG/SEM.VT900MC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT45 ${DATA}/PARG/SEM.VT900DC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT46 ${DATA}/PARG/SEM.VT900RC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT47 ${DATA}/PARG/SEM.VT900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT48 ${DATA}/PARG/SEM.VT900XC
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT49 ${DATA}/PARG/SEM.VT900YJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT50 ${DATA}/PARG/SEM.VT900PJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT51 ${DATA}/PARG/SEM.VT900LJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT52 ${DATA}/PARG/SEM.VT900MJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT53 ${DATA}/PARG/SEM.VT900DJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT54 ${DATA}/PARG/SEM.VT900RJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT55 ${DATA}/PARG/SEM.VT900OJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT56 ${DATA}/PARG/SEM.VT900XJ
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT921PBD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VT921PBE
       ;;
(VT921PBE)
       m_CondExec 16,NE,VT921PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE02                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBG
       ;;
(VT921PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OF
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 SORTOUT ${DATA}/PTEM/VT921PBG.VT921P08
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBH
       ;;
(VT921PBH)
       m_CondExec 00,EQ,VT921PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE05                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBJ
       ;;
(VT921PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OG
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 SORTOUT ${DATA}/PTEM/VT921PBJ.VT921P09
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBK
       ;;
(VT921PBK)
       m_CondExec 00,EQ,VT921PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE06                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBM
       ;;
(VT921PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OH
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/VT921PBM.VT921P10
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBN
       ;;
(VT921PBN)
       m_CondExec 00,EQ,VT921PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE08                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBQ
       ;;
(VT921PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OI
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 SORTOUT ${DATA}/PTEM/VT921PBQ.VT921P11
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBR
       ;;
(VT921PBR)
       m_CondExec 00,EQ,VT921PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE23                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBT
       ;;
(VT921PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OK
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 SORTOUT ${DATA}/PTEM/VT921PBT.VT921P12
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBU
       ;;
(VT921PBU)
       m_CondExec 00,EQ,VT921PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTEV11                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VT921PBX
       ;;
(VT921PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OC
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SORTOUT ${DATA}/PTEM/VT921PBX.VT921P13
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_75_2 75 CH 2
 /KEYS
   FLD_CH_1_13 ASCENDING,
   FLD_CH_75_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PBY
       ;;
(VT921PBY)
       m_CondExec 00,EQ,VT921PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FICHIER EPURATION TABLE RTVE14                                          
#  ATTENTION 1 FICHIER PAR FILIALE ON DOIT LIRE 8 FICHIERS                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=VT921PCA
       ;;
(VT921PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PARG/SEM.VE900YJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900PJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900LJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900MJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900DJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900RJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900OJ
       m_FileAssign -d SHR -g +0 -C ${DATA}/PARG/SEM.VE900XJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 SORTOUT ${DATA}/PTEM/VT921PCA.VT921P14
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT921PCB
       ;;
(VT921PCB)
       m_CondExec 00,EQ,VT921PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVE922 :ARCHIVAGE DES VENTES VE                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=VT921PCD
       ;;
(VT921PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSVT00   : NAME=RSVT00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT00 /dev/null
#    RSVT01   : NAME=RSVT01,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT01 /dev/null
#    RSVT05   : NAME=RSVT05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT05 /dev/null
#    RSVT06   : NAME=RSVT06,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT06 /dev/null
#    RSVT08   : NAME=RSVT08,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT08 /dev/null
#    RSVT11   : NAME=RSVT11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT11 /dev/null
#    RSVT12   : NAME=RSVT12,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT12 /dev/null
#    RSVT13   : NAME=RSVT13,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT13 /dev/null
#    RSVT14   : NAME=RSVT14,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT14 /dev/null
#    RSVT15   : NAME=RSVT15,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT15 /dev/null
#    RSVT16   : NAME=RSVT16,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT16 /dev/null
#    RSVT17   : NAME=RSVT17,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT17 /dev/null
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g ${G_A8} FVE020 ${DATA}/PTEM/VT921PBG.VT921P08
       m_FileAssign -d SHR -g ${G_A9} FVE005 ${DATA}/PTEM/VT921PBJ.VT921P09
       m_FileAssign -d SHR -g ${G_A10} FVE006 ${DATA}/PTEM/VT921PBM.VT921P10
       m_FileAssign -d SHR -g ${G_A11} FVE008 ${DATA}/PTEM/VT921PBQ.VT921P11
       m_FileAssign -d SHR -g ${G_A12} FVE100 ${DATA}/PTEM/VT921PBT.VT921P12
       m_FileAssign -d SHR -g ${G_A13} FVE110 ${DATA}/PTEM/VT921PBX.VT921P13
       m_FileAssign -d SHR -g ${G_A14} FVE140 ${DATA}/PTEM/VT921PCA.VT921P14
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE922 
       JUMP_LABEL=VT921PCE
       ;;
(VT921PCE)
       m_CondExec 04,GE,VT921PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES GDG                                                       
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT921PCG PGM=IDCAMS     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=VT921PCG
       ;;
(VT921PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
       m_FileAssign -d SHR IN41 /dev/null
       m_FileAssign -d SHR IN42 /dev/null
       m_FileAssign -d SHR IN43 /dev/null
       m_FileAssign -d SHR IN44 /dev/null
       m_FileAssign -d SHR IN45 /dev/null
       m_FileAssign -d SHR IN46 /dev/null
       m_FileAssign -d SHR IN47 /dev/null
       m_FileAssign -d SHR IN48 /dev/null
       m_FileAssign -d SHR IN49 /dev/null
       m_FileAssign -d SHR IN50 /dev/null
       m_FileAssign -d SHR IN51 /dev/null
       m_FileAssign -d SHR IN52 /dev/null
       m_FileAssign -d SHR IN53 /dev/null
       m_FileAssign -d SHR IN54 /dev/null
       m_FileAssign -d SHR IN55 /dev/null
       m_FileAssign -d SHR IN56 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT1 ${DATA}/PARG/SEM.VE900YF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT2 ${DATA}/PARG/SEM.VE900PF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT3 ${DATA}/PARG/SEM.VE900LF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT4 ${DATA}/PARG/SEM.VE900MF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT5 ${DATA}/PARG/SEM.VE900DF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT6 ${DATA}/PARG/SEM.VE900RF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT7 ${DATA}/PARG/SEM.VE900OF
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 OUT8 ${DATA}/PARG/SEM.VE900XF
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT9 ${DATA}/PARG/SEM.VE900YG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT10 ${DATA}/PARG/SEM.VE900PG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT11 ${DATA}/PARG/SEM.VE900LG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT12 ${DATA}/PARG/SEM.VE900MG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT13 ${DATA}/PARG/SEM.VE900DG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT14 ${DATA}/PARG/SEM.VE900RG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT15 ${DATA}/PARG/SEM.VE900OG
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 OUT16 ${DATA}/PARG/SEM.VE900XG
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT17 ${DATA}/PARG/SEM.VE900YH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT18 ${DATA}/PARG/SEM.VE900PH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT19 ${DATA}/PARG/SEM.VE900LH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT20 ${DATA}/PARG/SEM.VE900MH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT21 ${DATA}/PARG/SEM.VE900DH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT22 ${DATA}/PARG/SEM.VE900RH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT23 ${DATA}/PARG/SEM.VE900OH
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 OUT24 ${DATA}/PARG/SEM.VE900XH
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT25 ${DATA}/PARG/SEM.VE900YI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT26 ${DATA}/PARG/SEM.VE900PI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT27 ${DATA}/PARG/SEM.VE900LI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT28 ${DATA}/PARG/SEM.VE900MI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT29 ${DATA}/PARG/SEM.VE900DI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT30 ${DATA}/PARG/SEM.VE900RI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT31 ${DATA}/PARG/SEM.VE900OI
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 OUT32 ${DATA}/PARG/SEM.VE900XI
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT33 ${DATA}/PARG/SEM.VE900YK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT34 ${DATA}/PARG/SEM.VE900PK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT35 ${DATA}/PARG/SEM.VE900LK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT36 ${DATA}/PARG/SEM.VE900MK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT37 ${DATA}/PARG/SEM.VE900DK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT38 ${DATA}/PARG/SEM.VE900RK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT39 ${DATA}/PARG/SEM.VE900OK
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 OUT40 ${DATA}/PARG/SEM.VE900XK
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT41 ${DATA}/PARG/SEM.VE900YC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT42 ${DATA}/PARG/SEM.VE900PC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT43 ${DATA}/PARG/SEM.VE900LC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT44 ${DATA}/PARG/SEM.VE900MC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT45 ${DATA}/PARG/SEM.VE900DC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT46 ${DATA}/PARG/SEM.VE900RC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT47 ${DATA}/PARG/SEM.VE900OC
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 OUT48 ${DATA}/PARG/SEM.VE900XC
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT49 ${DATA}/PARG/SEM.VE900YJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT50 ${DATA}/PARG/SEM.VE900PJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT51 ${DATA}/PARG/SEM.VE900LJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT52 ${DATA}/PARG/SEM.VE900MJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT53 ${DATA}/PARG/SEM.VE900DJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT54 ${DATA}/PARG/SEM.VE900RJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT55 ${DATA}/PARG/SEM.VE900OJ
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 OUT56 ${DATA}/PARG/SEM.VE900XJ
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT921PCG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VT921PCH
       ;;
(VT921PCH)
       m_CondExec 16,NE,VT921PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# *******************************************************************          
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VT921PZA
       ;;
(VT921PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT921PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
