#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV20O.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGPV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/24 AT 17.41.14 BY BURTECN                      
#    STANDARDS: P  JOBSET: GPV20O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV20OA
       ;;
(GPV20OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV20OAA
       ;;
(GPV20OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )                
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F16.DATPV20O
#                                                                              
# ------  TABLE EN LECTURE                                                     
#                                                                              
#    RTGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00O /dev/null
#    RTGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01O /dev/null
#    RTGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10O /dev/null
#    RTGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14O /dev/null
#    RTGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA59O /dev/null
#    RTGA66O  : NAME=RSGA66O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66O /dev/null
#    RTGA73O  : NAME=RSGA73O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA73O /dev/null
#    RTGA75O  : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75O /dev/null
#    RTGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGS30O /dev/null
#                                                                              
# ------  FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/GPV20OAA.BPV200AO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220 ${DATA}/PTEM/GPV20OAA.BPV200BO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV20OAB
       ;;
(GPV20OAB)
       m_CondExec 04,GE,GPV20OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200AO (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)         
#  ET CREATION DU FICHIER BPV211AM ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAD
       ;;
(GPV20OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GPV20OAA.BPV200AO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GPV20OAD.BPV211AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20OAE
       ;;
(GPV20OAE)
       m_CondExec 00,EQ,GPV20OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200BO (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,         
#  ET CREATION DU FICHIER BPV211BO ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAG
       ;;
(GPV20OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GPV20OAA.BPV200BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GPV20OAG.BPV211BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20OAH
       ;;
(GPV20OAH)
       m_CondExec 00,EQ,GPV20OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV211                                                                
# ********************************************************************         
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAJ
       ;;
(GPV20OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- TABLES EN ENTREE                                                     
#                                                                              
#    RTGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11O /dev/null
#    RTGA12O  : NAME=RSGA12O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12O /dev/null
#    RTGA25O  : NAME=RSGA25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25O /dev/null
#    RTGA26O  : NAME=RSGA26O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA26O /dev/null
#    RTGA53O  : NAME=RSGA53O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53O /dev/null
#                                                                              
# ------- FICHIER D'EXTRACTION DES TRIS PRECEDENTS                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC1 ${DATA}/PTEM/GPV20OAD.BPV211AO
       m_FileAssign -d SHR -g ${G_A4} FPV220 ${DATA}/PTEM/GPV20OAG.BPV211BO
#                                                                              
# ------- FICHIER D'EXTRACTION DE BPV210 (EN DUMMY) CAR CE PROG                
# ------- TOURNE SEULEMENT EN HEBDO DANS GPV21O                                
#                                                                              
       m_FileAssign -d SHR FEXTRAC2 /dev/null
#                                                                              
# ------- FICHIER D'EDITION EN DUMMY CAR ON NE LES FAIT PAS EN QUOTIDI         
#                                                                              
       m_FileAssign -d SHR FEXTR1B /dev/null
       m_FileAssign -d SHR FEXTR2B /dev/null
#                                                                              
# ------- FICHIER FPV220B ENTRANT DANS LE PGM BPV212                           
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220B ${DATA}/PTEM/GPV20OAJ.BPV220AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV20OAK
       ;;
(GPV20OAK)
       m_CondExec 04,GE,GPV20OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV220AO POUR CREATION DU FICHIER COMPLET DES                
#  MODIFICATIONS DE L'OFFRE ACTIVE (BPV220BO)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAM
       ;;
(GPV20OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GPV20OAJ.BPV220AO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/F16.BPV220BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_42_7 42 CH 7
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_4_3 4 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20OAN
       ;;
(GPV20OAN)
       m_CondExec 00,EQ,GPV20OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV212                                                                
# ********************************************************************         
#  CREATION DU FICHIER FPV212 A ENVOYER SUR LES 36 DES MAGS PAR                
#  COMPARAISON DES FICHIERS BPV220BO                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAQ PGM=BPV212     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAQ
       ;;
(GPV20OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- DATE (JJMMSSAA)                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ------- FICHIER DE L'OFFRE ACTIVE DE LA VEILLE                               
       m_FileAssign -d SHR -g ${G_A6} FPV220O ${DATA}/PXX0/F16.BPV220BO
# ------- FICHIER DE L'OFFRE ACTIVE DU JOUR                                    
       m_FileAssign -d SHR -g ${G_A7} FPV220B ${DATA}/PXX0/F16.BPV220BO
# ------- FICHIER DE COMPARAISON A ENVOYER SUR LE 36 (GDGNB=5)                 
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FPV212 ${DATA}/PXX0/F16.BPV212AO
       m_ProgramExec BPV212 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OAT
       ;;
(GPV20OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA                                                       
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F16.DATPV20O
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20OAU
       ;;
(GPV20OAU)
       m_CondExec 00,EQ,GPV20OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV20OZA
       ;;
(GPV20OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV20OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
