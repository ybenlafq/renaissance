#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV80Y.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGPV80 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/02/08 AT 13.08.13 BY BURTEC5                      
#    STANDARDS: P  JOBSET: GPV80Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BGV800                                                                
# ********************************************************************         
#  EXTRACTION DES CODICS POUR STAT COMMERCIALE SUR LA RTHV04                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV80YA
       ;;
(GPV80YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV80YAA
       ;;
(GPV80YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV04 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV800 ${DATA}/PXX0/GPV80YAA.BGV800AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV800 
       JUMP_LABEL=GPV80YAB
       ;;
(GPV80YAB)
       m_CondExec 04,GE,GPV80YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV800                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAD
       ;;
(GPV80YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GPV80YAA.BGV800AY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YAD.BGV800BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_6 19 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YAE
       ;;
(GPV80YAE)
       m_CondExec 00,EQ,GPV80YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV800                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAG
       ;;
(GPV80YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE                                                                
#    RTGA53   : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A2} FGV800 ${DATA}/PXX0/GPV80YAD.BGV800BY
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80YAG.BGV805AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80YAH
       ;;
(GPV80YAH)
       m_CondExec 04,GE,GPV80YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAJ
       ;;
(GPV80YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GPV80YAG.BGV805AY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YAJ.BGV805BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_64_6 64 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YAK
       ;;
(GPV80YAK)
       m_CondExec 00,EQ,GPV80YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV815 AVEC FPARAM BGV81 POUR ALIMENTATION DE FPV815                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
#  AJOUT PAR PHILIPPE DU FPARAM LE 070793                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAM
       ;;
(GPV80YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV815DRA
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV815 ${DATA}/PXX0/GPV80YAM.BGV815AY
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d SHR FGV816 /dev/null
#                                                                              
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80YAN
       ;;
(GPV80YAN)
       m_CondExec 04,GE,GPV80YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV815                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAQ
       ;;
(GPV80YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/GPV80YAM.BGV815AY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YAQ.BGV815BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YAR
       ;;
(GPV80YAR)
       m_CondExec 00,EQ,GPV80YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV815 (CODIC GOUPE)                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAT
       ;;
(GPV80YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A5} FGV800 ${DATA}/PXX0/GPV80YAQ.BGV815BY
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80YAT.BGV805CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80YAU
       ;;
(GPV80YAU)
       m_CondExec 04,GE,GPV80YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805 (CODIC GROUPE)                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YAX
       ;;
(GPV80YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/GPV80YAT.BGV805CY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YAX.BGV805DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_26_4 26 CH 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YAY
       ;;
(GPV80YAY)
       m_CondExec 00,EQ,GPV80YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV810                                                                
#  EDITION DES STATS COMMERCIALES BBTE  (CODIC GROUPE)                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBA PGM=BGV810     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBA
       ;;
(GPV80YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIERS LEADERS ET PRIMES TRIES                                     
       m_FileAssign -d SHR -g ${G_A7} FGV805 ${DATA}/PXX0/GPV80YAX.BGV805DY
# ******  ETAT IGV815                                                          
       m_OutputAssign -c 9 -w IGV815 IGV810
       m_ProgramExec BGV810 
# ********************************************************************         
# ********************************************************************         
#  PGM : BGV815 AVEC FPARAM BGV80 POUR ALIMENTATION DE FPV816                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBD
       ;;
(GPV80YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE LGT                                     
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV816DRA
#                                                                              
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR FGV815 /dev/null
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV816 ${DATA}/PXX0/GPV80YBD.BGV816AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80YBE
       ;;
(GPV80YBE)
       m_CondExec 04,GE,GPV80YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV816                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBG
       ;;
(GPV80YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/GPV80YBD.BGV816AY
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PXX0/GPV80YAA.BGV800AY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YBG.BGV816BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_5 11 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YBH
       ;;
(GPV80YBH)
       m_CondExec 00,EQ,GPV80YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV805  : ENRICHISSEMENT DU FICHIER FGV816 (CODIC GOUPE)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBJ
       ;;
(GPV80YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A10} FGV800 ${DATA}/PXX0/GPV80YBG.BGV816BY
# ******  FICHIER FGV816 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80YBJ.BGV816CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80YBK
       ;;
(GPV80YBK)
       m_CondExec 04,GE,GPV80YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805 (CODIC GROUPE)                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBM
       ;;
(GPV80YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/GPV80YBJ.BGV816CY
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80YBM.BGV816DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_58_6 58 CH 6
 /FIELDS FLD_CH_52_6 52 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80YBN
       ;;
(GPV80YBN)
       m_CondExec 00,EQ,GPV80YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV810  : EDITION DES STATS COMMERCIALES BBTE  (CODIC GROUPE)               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80YBQ PGM=BGV810     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YBQ
       ;;
(GPV80YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (945)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIERS LEADERS ET PRIMES TRIES                                     
       m_FileAssign -d SHR -g ${G_A12} FGV805 ${DATA}/PXX0/GPV80YBM.BGV816DY
# ******  ETAT IGV810                                                          
       m_OutputAssign -c 9 -w IGV810 IGV810
       m_ProgramExec BGV810 
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV80YZA
       ;;
(GPV80YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV80YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
