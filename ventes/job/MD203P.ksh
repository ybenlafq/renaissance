#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MD203P.ksh                       --- VERSION DU 08/10/2016 13:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMD203 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.01.26 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MD203P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BMD203 : LISTE DES VENTES DESEQUILIBREE                                     
#  VUE UTILISE RVVE1099 RVVE1499 RVGV1099 RVGV1499 ACCES TABLES FILIAL         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MD203PA
       ;;
(MD203PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MD203PAA
       ;;
(MD203PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSVE10   : NAME=RSVE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE10Y  : NAME=RSVE10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10Y /dev/null
#    RSVE10M  : NAME=RSVE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10M /dev/null
#    RSVE10D  : NAME=RSVE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10D /dev/null
#    RSVE10L  : NAME=RSVE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10L /dev/null
#    RSVE10O  : NAME=RSVE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10O /dev/null
#    RSVE10X  : NAME=RSVE10X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10X /dev/null
#    RSVE14   : NAME=RSVE14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE14 /dev/null
#    RSVE14Y  : NAME=RSVE14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14Y /dev/null
#    RSVE14M  : NAME=RSVE14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14M /dev/null
#    RSVE14D  : NAME=RSVE14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14D /dev/null
#    RSVE14L  : NAME=RSVE14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14L /dev/null
#    RSVE14O  : NAME=RSVE14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14O /dev/null
#    RSVE14X  : NAME=RSVE14X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE14X /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV10Y  : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10Y /dev/null
#    RSGV10M  : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10M /dev/null
#    RSGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10D /dev/null
#    RSGV10L  : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10L /dev/null
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV10X  : NAME=RSGV10X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10X /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14Y  : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14Y /dev/null
#    RSGV14M  : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14M /dev/null
#    RSGV14D  : NAME=RSGV14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14D /dev/null
#    RSGV14L  : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14L /dev/null
#    RSGV14O  : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14O /dev/null
#    RSGV14X  : NAME=RSGV14X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14X /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSVE11Y  : NAME=RSVE11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11Y /dev/null
#    RSVE11M  : NAME=RSVE11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11M /dev/null
#    RSVE11D  : NAME=RSVE11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11D /dev/null
#    RSVE11L  : NAME=RSVE11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11L /dev/null
#    RSVE11O  : NAME=RSVE11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11O /dev/null
#    RSVE11X  : NAME=RSVE11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11X /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11X  : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11X /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/MD203PAA
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 126 -t LSEQ -g +1 FMD203 ${DATA}/PTEM/MD203PAA.BMD203BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD203 
       JUMP_LABEL=MD203PAB
       ;;
(MD203PAB)
       m_CondExec 04,GE,MD203PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD203PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MD203PAD
       ;;
(MD203PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/MD203PAA.BMD203BP
       m_FileAssign -d NEW,CATLG,DELETE -r 126 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BMD203AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_17 2 CH 17
 /KEYS
   FLD_CH_2_17 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD203PAE
       ;;
(MD203PAE)
       m_CondExec 00,EQ,MD203PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER EXTRACTION VENTES A LAURENT KONSKIER                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BMD203AP,                                                           
#      FNAME=":BMD203AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTMD203P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD203PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MD203PAG
       ;;
(MD203PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD203PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/MD203PAG.FTMD203P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTMD203P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD203PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MD203PAJ
       ;;
(MD203PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MD203PAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.MD203PAG.FTMD203P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD203PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MD203PAM
       ;;
(MD203PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD203PAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MD203PZA
       ;;
(MD203PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD203PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
