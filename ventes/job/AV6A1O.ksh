#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV6A1O.ksh                       --- VERSION DU 09/10/2016 05:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POAV6A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/05/02 AT 16.53.49 BY BURTECO                      
#    STANDARDS: P  JOBSET: AV6A1O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#          CREATION D'UN FICHIER VIDE POUR RA002O EN CAS DE PLANTAGE           
#          DE LA CHAINE AV600O                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=AV6A1OA
       ;;
(AV6A1OA)
#
#AV6A1OAG
#AV6A1OAG Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#AV6A1OAG
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2007/05/02 AT 16.53.49 BY BURTECO                
# *    JOBSET INFORMATION:    NAME...: AV6A1O                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-AV600O'                            
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=AV6A1OAA
       ;;
(AV6A1OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DUMMY                                                            
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC DES AVOIR DE REMBOURSSEMENT DU JOUR ( VIDE )                     
       m_FileAssign -d NEW,CATLG,DELETE -r 260 -g +1 OUT1 ${DATA}/PXX0/F16.BAV600AO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AV6A1OAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=AV6A1OAB
       ;;
(AV6A1OAB)
       m_CondExec 16,NE,AV6A1OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AV6A1OAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
