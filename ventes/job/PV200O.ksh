#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV200O.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPV200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/01/04 AT 17.35.04 BY PREPA2                       
#    STANDARDS: P  JOBSET: PV200O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV050                                                                
#  PROG D EXTRACTION AFIN DE FORMATTER UN FICHIER SAM QUI SERA RE-TRIE         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV200OA
       ;;
(PV200OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV200OAA
       ;;
(PV200OAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******************************** TABLE DES LIEUX                             
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ********************************  TABLE DES VENDEURS                         
#    RTGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
# ******************************* TABLE HISTO DES VENTES                       
#    RTHV32   : NAME=RSHV32O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTHV32 /dev/null
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ********************************  SORTIE D UN SAM AVANT TRI (LRECL 1         
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV050 ${DATA}/PTEM/PV200OAA.BPV050AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV050 
       JUMP_LABEL=PV200OAB
       ;;
(PV200OAB)
       m_CondExec 04,GE,PV200OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050                                               
#  C EST LE FICHIER EN SORTIE QUI SERVIRA POUR LES PGM D EDITIONS              
#  REPRISE: OUI.                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAD
       ;;
(PV200OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV200OAA.BPV050AO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/PV200OAD.BPV050BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_107_6 107 CH 6
 /FIELDS FLD_CH_1_9 1 CH 09
 /FIELDS FLD_CH_99_4 99 CH 4
 /FIELDS FLD_CH_81_6 81 CH 6
 /FIELDS FLD_CH_73_4 73 CH 4
 /FIELDS FLD_CH_93_6 93 CH 6
 /FIELDS FLD_CH_53_20 53 CH 20
 /FIELDS FLD_CH_103_4 103 CH 4
 /FIELDS FLD_CH_77_4 77 CH 4
 /FIELDS FLD_CH_87_6 87 CH 6
 /KEYS
   FLD_CH_1_9 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_73_4,
    TOTAL FLD_CH_77_4,
    TOTAL FLD_CH_81_6,
    TOTAL FLD_CH_87_6,
    TOTAL FLD_CH_93_6,
    TOTAL FLD_CH_99_4,
    TOTAL FLD_CH_103_4,
    TOTAL FLD_CH_107_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200OAE
       ;;
(PV200OAE)
       m_CondExec 00,EQ,PV200OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU TRI PRECEDENT POUR ETAT IPV050                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAG
       ;;
(PV200OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PV200OAD.BPV050BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/PV200OAG.BPV050CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_53_20 53 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200OAH
       ;;
(PV200OAH)
       m_CondExec 00,EQ,PV200OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV051  : EDITION IPV050 .VENTES PAR RUBRIQUE PRODUITS                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAJ PGM=BPV051     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAJ
       ;;
(PV200OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A3} FPV050 ${DATA}/PTEM/PV200OAG.BPV050CO
# ******  EDITION DE L ETAT IPV050                                             
#  //IPV050   DD SYSOUT=(9,IPV050),LRECL=133,RECFM=FBA,BLKSIZE=1330,           
#  //       SPIN=UNALLOC                                                       
       m_OutputAssign -c "*" IPV050
       m_ProgramExec BPV051 
# ********************************************************************         
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV051 IL SE NOMMERA FPV050                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAM
       ;;
(PV200OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PV200OAD.BPV050BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/PV200OAM.BPV051AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 0
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_PD_73_4 EQ CST_1_6 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING,
   FLD_PD_81_6 DESCENDING,
   FLD_PD_73_4 DESCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200OAN
       ;;
(PV200OAN)
       m_CondExec 00,EQ,PV200OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV052 IL SE NOMMERA FCUMUL                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAQ
       ;;
(PV200OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV200OAD.BPV050BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/PV200OAQ.BPV051BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 0
 /FIELDS FLD_PD_93_6 93 PD 6
 /FIELDS FLD_PD_103_4 103 PD 4
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_99_4 99 PD 4
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_PD_107_6 107 PD 6
 /CONDITION CND_2 FLD_PD_73_4 EQ CST_1_10 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_4,
    TOTAL FLD_PD_81_6,
    TOTAL FLD_PD_93_6,
    TOTAL FLD_PD_99_4,
    TOTAL FLD_PD_103_4,
    TOTAL FLD_PD_107_6
 /OMIT CND_2
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200OAR
       ;;
(PV200OAR)
       m_CondExec 00,EQ,PV200OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV052                                                                
#  PROG DE FORMATTAGE DE L ETAT IPV051                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAT
       ;;
(PV200OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ********************************  DATE JJMMSSAA                              
       m_FileAssign -i FDATE
$FDATE
_end
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ********************************  ENTREE BPV051AP                            
       m_FileAssign -d SHR -g ${G_A6} FPV050 ${DATA}/PTEM/PV200OAM.BPV051AO
# ********************************  ENTREE BPV051BP                            
       m_FileAssign -d SHR -g ${G_A7} FCUMUL ${DATA}/PTEM/PV200OAQ.BPV051BO
# ******  EDITION DE L ETAT IPV051                                             
#  //IPV051   DD SYSOUT=(9,IPV051),LRECL=133,RECFM=FBA,BLKSIZE=1330,           
#  //       SPIN=UNALLOC                                                       
       m_OutputAssign -c "*" IPV051
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV052 
       JUMP_LABEL=PV200OAU
       ;;
(PV200OAU)
       m_CondExec 04,GE,PV200OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET DU TRI POUR ETAT IPV052                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV200OAX
       ;;
(PV200OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/PV200OAD.BPV050BO
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/PV200OAX.BPV053AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_CH_4_6 4 CH 06
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_6 ASCENDING,
   FLD_PD_81_6 DESCENDING,
   FLD_PD_73_4 DESCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200OAY
       ;;
(PV200OAY)
       m_CondExec 00,EQ,PV200OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV053  : EDITION IPV052 .STAT DE VENTES PAR VENDEUR                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200OBA PGM=BPV053     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV200OBA
       ;;
(PV200OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A9} FPV050 ${DATA}/PTEM/PV200OAX.BPV053AO
# ******  EDITION DE L ETAT IPV052                                             
#  //IPV052   DD SYSOUT=(9,IPV052),LRECL=133,RECFM=FBA,BLKSIZE=1330,           
#  //       SPIN=UNALLOC                                                       
       m_OutputAssign -c "*" IPV052
       m_ProgramExec BPV053 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV200OZA
       ;;
(PV200OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV200OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
