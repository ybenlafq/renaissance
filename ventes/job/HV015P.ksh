#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015P.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/01/06 AT 11.08.24 BY BURTEC3                      
#    STANDARDS: P  JOBSET: HV015P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='CONTROLE'                                                          
# ********************************************************************         
# *  TRI DU FICHIER SEQUENTIEL BHV000AP                                        
# *  FAMILLE: 48,5,A  SOCIET 1,13 ; DATE VENTE 17,8 ; CODE COMMUNE 34,         
# *  REPRISE:OUI MAIS ATTENTION A LA GENERATION                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015PA
       ;;
(HV015PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=HV015PAA
       ;;
(HV015PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.BHV000AP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/HV015PAA.BHV015AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_34_5 34 CH 5
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015PAB
       ;;
(HV015PAB)
       m_CondExec 00,EQ,HV015PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  PGM=BHV015 GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE               
# *  DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                              
# *  REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HV015PAD
       ;;
(HV015PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FMOISJ
_end
#  TABLE DES ARTICLES  (GENERALITES ARTICLES)                                  
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE DES FAMILLE   (EDITION DES ETATS)                                     
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#  TABLE DES RAYONS    (COMPOSANTS DU RAYON)                                   
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
#  TABLE DES QUOTAS LIV  (POPULATION MARCHE/DONNEES DE BASE INSEE)             
#    RSGQ01   : NAME=RSGQ01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ01 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PXX0/HV015PAA.BHV015AP
#  SORTIE                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PXX0/HV015PAD.BHV015BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015PAE
       ;;
(HV015PAE)
       m_CondExec 04,GE,HV015PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  TRI DU FICHIER SEQUENTIEL BHV015BP                                        
# *  CODE RAYON  1,11 ; CODE COMMUNE  32,5                                     
# *  REPRISE:OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HV015PAG
       ;;
(HV015PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/HV015PAD.BHV015BP
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PXX0/HV015PAG.BHV015CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_PD_20_5 20 PD 5
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_32_5 32 CH 05
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015PAH
       ;;
(HV015PAH)
       m_CondExec 00,EQ,HV015PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  PGM=BHV020  MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOI         
# *  ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                              
# *  REPRISE:OUI FIN ANORMALE                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HV015PAJ
       ;;
(HV015PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  ENTREE DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
#  TABLE HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                               
#    RSHV20   : NAME=RSHV20,MODE=U - DYNAM=YES                                 
# -X-RSHV20   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV20 /dev/null
#  TABLE HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                        
#    RSHV25   : NAME=RSHV25,MODE=U - DYNAM=YES                                 
# -X-RSHV25   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV25 /dev/null
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PXX0/HV015PAG.BHV015CP
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015PAK
       ;;
(HV015PAK)
       m_CondExec 04,GE,HV015PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=HV015PZA
       ;;
(HV015PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
