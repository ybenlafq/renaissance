#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV400P.ksh                       --- VERSION DU 08/10/2016 12:54
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/08/03 AT 10.20.39 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV400P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='VENTES'                                                            
# ********************************************************************         
# ********************************************************************         
#   TRI DU FICHIER SUR CODIC,SOC ORIG,LIEU ORIG,SOC DEST,LIEU DEST,DAT         
#   OPER,CODE INSEE,DATE CREAT,DATE STAT                                       
#   INCLUDE SUR LIEU ORIGINE ET LIEU DESTINATION                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV400PA
       ;;
(GV400PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV400PAA
       ;;
(GV400PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
# *** PASSE DE 127 A 141                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PXX0/GV400PAA.FGS400AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_18 "VEN"
 /DERIVEDFIELD CST_1_14 "VEN"
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_122_6 122 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_88_5 88 CH 5
 /CONDITION CND_2 FLD_CH_4_3 EQ CST_1_14 OR FLD_CH_13_3 EQ CST_3_18 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_70_8 ASCENDING,
   FLD_CH_88_5 ASCENDING,
   FLD_CH_104_8 ASCENDING,
   FLD_CH_122_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3,
    TOTAL FLD_PD_99_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400PAB
       ;;
(GV400PAB)
       m_CondExec 00,EQ,GV400PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV400 : EXTRACTION DES INFOS NECESSAIRES A L'EDITION DES VENTES            
#                          PAR ZONES DE SAV                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=GV400PAD
       ;;
(GV400PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******* TABLE DES PARAMETRES LIES AUX FAMILLES                               
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
# ******* TABLE DE ARTICLES PAR SECTEUR DE SAV CODE DEPT AFFEC CODE ZO         
#    RTGA43   : NAME=RSGA43,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA43 /dev/null
# ******* FIC ISSU DU TRI                                                      
       m_FileAssign -d SHR -g ${G_A1} FGS400 ${DATA}/PXX0/GV400PAA.FGS400AP
# ******* FIC D'EXTRACT DES INFOS PAR ZONES SAV                                
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -g +1 FGV400 ${DATA}/PXX0/GV400PAD.BGV400AP
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******* SOCIETE TRAITEE:907                                                  
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV400 
       JUMP_LABEL=GV400PAE
       ;;
(GV400PAE)
       m_CondExec 04,GE,GV400PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES INFOS D'EDITION DES VENTES                               
#  SECTEUR: 1,5,A   ZONE-SAV: 6,5,A    AGREGAT: 54,2   N�SEQ: 51,3,A           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400PAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=GV400PAG
       ;;
(GV400PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV400PAD.BGV400AP
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -g +1 SORTOUT ${DATA}/PXX0/GV400PAG.BGV410AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_56_5 56 PD 5
 /FIELDS FLD_PD_61_6 61 PD 6
 /FIELDS FLD_CH_11_20 11 CH 20
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_54_2 54 CH 2
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_11_20 ASCENDING,
   FLD_CH_54_2 ASCENDING,
   FLD_CH_51_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_56_5,
    TOTAL FLD_PD_61_6
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400PAH
       ;;
(GV400PAH)
       m_CondExec 00,EQ,GV400PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV410 : EDITION DES VENTES PAR SAV A PARTIR DU FICHIER D'EXTRACTS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400PAJ PGM=BGV410     **                                          
# ***********************************                                          
       JUMP_LABEL=GV400PAJ
       ;;
(GV400PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FIC D'EXTRACT DES INFOS TRI�                                         
       m_FileAssign -d SHR -g ${G_A3} FGV410 ${DATA}/PXX0/GV400PAG.BGV410AP
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* EDITION DES VENTES PAR ZONE DE SAV                                   
       m_OutputAssign -c 9 -w IGV410 IGV410
       m_ProgramExec BGV410 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=GV400PZA
       ;;
(GV400PZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV400PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
