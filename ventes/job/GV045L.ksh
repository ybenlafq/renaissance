#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV045L.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV045 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/09/20 AT 19.41.12 BY BURTEC4                      
#    STANDARDS: P  JOBSET: GV045L                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  BGV045 CREE FIC VENTES REALISEES SUR 36 MOIS                   *            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV045LA
       ;;
(GV045LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       EXAAK=${EXAAK:-0}
       G_A2=${G_A2:-'+1'}
       EXAAP=${EXAAP:-0}
       G_A3=${G_A3:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=GV045LAA
       ;;
(GV045LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
# ******* TABLES DB2 UTILISEES ***                                             
# ********************************                                             
#                                                                              
# ******* TABLE ARTICLES LILLE                                                 
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE FAMILLE/VALEURS CODE MARKETING                                 
#    RSGA09   : NAME=RSGA09L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
# ******* TABLE RELATION ETAT FAMILLE/RAYON                                    
#    RSGA11   : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******* TABLE RELATION FAMILLE/CODE MAKETING                                 
#    RSGA12   : NAME=RSGA12L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
# ******* HISTO VENTE/SOC,FAMILLE,MOIS                                         
#    RSHV10   : NAME=RSHV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV10 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -g +1 FGV045 ${DATA}/PXX0/GV045LAA.GV0045AL
       m_FileAssign -d NEW,CATLG,DELETE -r 170 -g +1 FGV047 ${DATA}/PXX0/GV045LAA.GV0047AL
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV045 
       JUMP_LABEL=GV045LAB
       ;;
(GV045LAB)
       m_CondExec 04,GE,GV045LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5CODE FAMILLE 12,1 TYPE  TABLEAU            
#   13,2 SEQUENCE EDITION 16,1 AGREGATION 18,5 CODE MARKET                     
#   23,5 CODE VALEUR MARKETING                                                 
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV045LAD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=GV045LAD
       ;;
(GV045LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GV045LAA.GV0045AL
       m_FileAssign -d NEW,CATLG,DELETE -r 175 -g +1 SORTOUT ${DATA}/PXX0/GV045LAD.GV0045BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_39_7 39 PD 7
 /FIELDS FLD_CH_13_2 13 CH 2
 /FIELDS FLD_PD_104_7 104 PD 7
 /FIELDS FLD_PD_147_7 147 PD 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_82_4 82 PD 4
 /FIELDS FLD_PD_86_7 86 PD 7
 /FIELDS FLD_PD_154_4 154 PD 4
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_PD_57_7 57 PD 7
 /FIELDS FLD_PD_111_7 111 PD 7
 /FIELDS FLD_PD_165_7 165 PD 7
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_118_4 118 PD 4
 /FIELDS FLD_CH_23_5 23 CH 5
 /FIELDS FLD_PD_100_4 100 PD 4
 /FIELDS FLD_PD_46_4 46 PD 4
 /FIELDS FLD_PD_93_7 93 PD 7
 /FIELDS FLD_PD_32_7 32 PD 7
 /FIELDS FLD_CH_16_2 16 CH 2
 /FIELDS FLD_PD_129_7 129 PD 7
 /FIELDS FLD_PD_136_4 136 PD 4
 /FIELDS FLD_CH_12_1 12 CH 1
 /FIELDS FLD_PD_122_7 122 PD 7
 /FIELDS FLD_PD_28_4 28 PD 4
 /FIELDS FLD_PD_75_7 75 PD 7
 /FIELDS FLD_PD_158_7 158 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_50_7 50 PD 7
 /FIELDS FLD_PD_68_7 68 PD 7
 /FIELDS FLD_PD_64_4 64 PD 4
 /FIELDS FLD_PD_140_7 140 PD 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_1 ASCENDING,
   FLD_CH_13_2 ASCENDING,
   FLD_CH_16_2 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_23_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_28_4,
    TOTAL FLD_PD_32_7,
    TOTAL FLD_PD_39_7,
    TOTAL FLD_PD_46_4,
    TOTAL FLD_PD_50_7,
    TOTAL FLD_PD_57_7,
    TOTAL FLD_PD_64_4,
    TOTAL FLD_PD_68_7,
    TOTAL FLD_PD_75_7,
    TOTAL FLD_PD_82_4,
    TOTAL FLD_PD_86_7,
    TOTAL FLD_PD_93_7,
    TOTAL FLD_PD_100_4,
    TOTAL FLD_PD_104_7,
    TOTAL FLD_PD_111_7,
    TOTAL FLD_PD_118_4,
    TOTAL FLD_PD_122_7,
    TOTAL FLD_PD_129_7,
    TOTAL FLD_PD_136_4,
    TOTAL FLD_PD_140_7,
    TOTAL FLD_PD_147_7,
    TOTAL FLD_PD_154_4,
    TOTAL FLD_PD_158_7,
    TOTAL FLD_PD_165_7
 /* Record Type = F  Record Length = 175 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV045LAE
       ;;
(GV045LAE)
       m_CondExec 00,EQ,GV045LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5CODE FAMILLE 12,2 SEQ EDITION              
#   14,2 AGREGATION 16,5 CODE MARKET 21,5 CODE VALEUR MARKETING                
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV045LAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=GV045LAG
       ;;
(GV045LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV045LAA.GV0047AL
       m_FileAssign -d NEW,CATLG,DELETE -r 170 -g +1 SORTOUT ${DATA}/PAS0/F61.GV0047BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_66_7 66 PD 7
 /FIELDS FLD_PD_98_4 98 PD 4
 /FIELDS FLD_PD_80_4 80 PD 4
 /FIELDS FLD_PD_138_7 138 PD 7
 /FIELDS FLD_PD_84_7 84 PD 7
 /FIELDS FLD_PD_134_4 134 PD 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_21_5 21 CH 5
 /FIELDS FLD_PD_102_7 102 PD 7
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_156_7 156 PD 7
 /FIELDS FLD_PD_48_7 48 PD 7
 /FIELDS FLD_PD_127_7 127 PD 7
 /FIELDS FLD_PD_163_7 163 PD 7
 /FIELDS FLD_PD_145_7 145 PD 7
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_55_7 55 PD 7
 /FIELDS FLD_PD_26_4 26 PD 4
 /FIELDS FLD_PD_109_7 109 PD 7
 /FIELDS FLD_PD_152_4 152 PD 4
 /FIELDS FLD_CH_12_2 12 CH 2
 /FIELDS FLD_PD_73_7 73 PD 7
 /FIELDS FLD_PD_44_4 44 PD 4
 /FIELDS FLD_PD_91_7 91 PD 7
 /FIELDS FLD_CH_14_2 14 CH 2
 /FIELDS FLD_PD_120_7 120 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_37_7 37 PD 7
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_62_4 62 PD 4
 /FIELDS FLD_PD_30_7 30 PD 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_2 ASCENDING,
   FLD_CH_14_2 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_21_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_26_4,
    TOTAL FLD_PD_30_7,
    TOTAL FLD_PD_37_7,
    TOTAL FLD_PD_44_4,
    TOTAL FLD_PD_48_7,
    TOTAL FLD_PD_55_7,
    TOTAL FLD_PD_62_4,
    TOTAL FLD_PD_66_7,
    TOTAL FLD_PD_73_7,
    TOTAL FLD_PD_80_4,
    TOTAL FLD_PD_84_7,
    TOTAL FLD_PD_91_7,
    TOTAL FLD_PD_98_4,
    TOTAL FLD_PD_102_7,
    TOTAL FLD_PD_109_7,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_7,
    TOTAL FLD_PD_127_7,
    TOTAL FLD_PD_134_4,
    TOTAL FLD_PD_138_7,
    TOTAL FLD_PD_145_7,
    TOTAL FLD_PD_152_4,
    TOTAL FLD_PD_156_7,
    TOTAL FLD_PD_163_7
 /* Record Type = F  Record Length = 170 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV045LAH
       ;;
(GV045LAH)
       m_CondExec 00,EQ,GV045LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV050  EDITION                                                *            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV045LAJ PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=GV045LAJ
       ;;
(GV045LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
# ******* TABLES DB2 UTILISEES ***                                             
# ********************************                                             
#                                                                              
# ******** TABLE DES FAMILLES                                                  
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******** TABLE CODES MARKETING                                               
#    RSGA23   : NAME=RSGA23L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA23 /dev/null
# ******** TABLE LIBELLES EDITION DES CODES MARKETING                          
#    RSGA29   : NAME=RSGA29L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FGV045T ${DATA}/PXX0/GV045LAD.GV0045BL
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_OutputAssign -c 9 -w BGV050 IGV050
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV050 
       JUMP_LABEL=GV045LAK
       ;;
(GV045LAK)
       m_CondExec 04,GE,GV045LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=GV045LZA
       ;;
(GV045LZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV045LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
