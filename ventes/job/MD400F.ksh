#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MD400F.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFMD400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/22 AT 11.33.07 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MD400F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REMISE A ZERO DU FICHIER DE CUMUL DES DEGRADATIONS DE MARGE MAGASIN         
#  CREE UNE GENERATION  A VIDE JUSTE APRES LE DERNIER TRAITEMENT               
#  DE L'ANNEE COMPTABLE POUR TOUTE FILIALES                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#    REMISE A ZERO DE DACEM                                                    
# ******************************************************************           
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MD400FA
       ;;
(MD400FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/08/22 AT 11.33.07 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: MD400F                                  
# *                           FREQ...: Y                                       
# *                           TITLE..: 'DEGRA MARGE MAG'                       
# *                           APPL...: REPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MD400FAA
       ;;
(MD400FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGB/F96.BMD401DB
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAB
       ;;
(MD400FAB)
       m_CondExec 16,NE,MD400FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE MARSEILLE                                                
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAD
       ;;
(MD400FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGD/F91.BMD401DD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAE
       ;;
(MD400FAE)
       m_CondExec 16,NE,MD400FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE LILLE                                                    
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAG
       ;;
(MD400FAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGL/F61.BMD401DL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAH
       ;;
(MD400FAH)
       m_CondExec 16,NE,MD400FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE METZ                                                     
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAJ
       ;;
(MD400FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGM/F89.BMD401DM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAK
       ;;
(MD400FAK)
       m_CondExec 16,NE,MD400FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE PARIS                                                    
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAM
       ;;
(MD400FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGP/F07.BMD401DP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAN
       ;;
(MD400FAN)
       m_CondExec 16,NE,MD400FAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE ROUEN                                                    
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAQ
       ;;
(MD400FAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGR/F94.BMD401DR
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAR
       ;;
(MD400FAR)
       m_CondExec 16,NE,MD400FAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE LYON                                                     
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAT
       ;;
(MD400FAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FAU
       ;;
(MD400FAU)
       m_CondExec 16,NE,MD400FAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    REMISE A ZERO DE DARTY OUEST                                              
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MD400FAX
       ;;
(MD400FAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGO/F16.BMD401DO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FAX.sysin
       m_UtilityExec
# ******************************************************************           
#    REMISE A ZERO DU LUXEMBOURG                                               
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FBA PGM=IDCAMS     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MD400FBA
       ;;
(MD400FBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/P908/SEM.BMD401DX
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FBA.sysin
       m_UtilityExec
# ******************************************************************           
#    REMISE A ZERO DE DNN+DAL = DNE                                            
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FBD PGM=IDCAMS     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MD400FBD
       ;;
(MD400FBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGL/F69.BMD401DE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FBD.sysin
       m_UtilityExec
# ******************************************************************           
#    REMISE A ZERO DE DO + DNN  = GO ( 905 )                                   
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FBG PGM=IDCAMS     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MD400FBG
       ;;
(MD400FBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGO/F16.MD401DO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FBG.sysin
       m_UtilityExec
# ******************************************************************           
#    REMISE A ZERO DE DRA + DAL + DPM = GE ( 911 )                             
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD400FBJ PGM=IDCAMS     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MD400FBJ
       ;;
(MD400FBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES DEGRADATIONS                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PNCGY/F45.MD401DY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD400FBJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD400FBK
       ;;
(MD400FBK)
       m_CondExec 16,NE,MD400FBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
