#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV2A1L.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLAV2A1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/21 AT 11.59.19 BY BURTEC6                      
#    STANDARDS: P  JOBSET: AV2A1L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#       *      CREATION D'UN FICHIER VIDE POUR DEBLOCAGE DE LA                 
# REPRO *      COMPTA GCT (FV001L)                                             
# *******                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=AV2A1LA
       ;;
(AV2A1LA)
#
#AV2A1LAJ
#AV2A1LAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#AV2A1LAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON MONDAY    2008/04/21 AT 11.59.18 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: AV2A1L                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-AV2A1L'                            
# *                           APPL...: IMPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=AV2A1LAA
       ;;
(AV2A1LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#    APRES JOB TETE DE NUIT            *                                       
# ******* FIC DES AVOIRS COMMERCIAUX P�RIM�S                                   
       m_FileAssign -d SHR IN1 /dev/null
# ******* FIC DES AVOIRS COMMERCIAUX P�RIM�S                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT1 ${DATA}/PXX0/F61.AV201L
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AV2A1LAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=AV2A1LAB
       ;;
(AV2A1LAB)
       m_CondExec 16,NE,AV2A1LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AV2A1LAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
