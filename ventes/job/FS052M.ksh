#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS052M.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFS052 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/28 AT 10.57.13 BY BURTECA                      
#    STANDARDS: P  JOBSET: FS052M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGV630                                                                
# ********************************************************************         
#  TOPER LES LIGNES DEPENDANTES D UNE LIGNE ARTICLE DEJA TOPEE                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# ********************************************************************         
#  BFS052 *   COBOL2/DB2                                                       
# *********                                                                    
#       COMPTABILISATION DES VENTES TOPEES DU JOUR                             
#                                                                              
#       RENSEIGNE LA DATE COMPTABLE DANS LES TABLES RTGV11 + 13 + 14           
#                                                                              
#       CALL AU SOUS PROG MFX053 : MAJ DES FICHIER PSE                         
#       CALL AU SOUS PROG MFV002 : INTERFACE G.L                               
#                                                                              
# *********                                                                    
#   REPRISE OUI APRES EXECUTION DU BACKOUT CORTRANS                            
#   LA REPRISE DOIT OBLIGATOIREMENT SE FAIRE :                                 
#   1. AVEC LA FDATE DU PLANTAGE                                               
#   2. AVEC LE PARAM�TRE DCOMPTA EGAL � FDATE (FORMAT JJMMSSAA)                
#      (DATE DE PREP)                                                          
#   ET                                                                         
#   3. SAUVEGARDER LE FICHIER FFV001 (BFS052A*) POUR LA COMPTA (FV001*         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS052MA
       ;;
(FS052MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FS052MAA
       ;;
(FS052MAA)
       m_CondExec ${EXAAA},NE,YES 
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#  PRED GV115M POUR QUE BFS052 FLAG CARTES T MGI POUR GQC20M                   
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00M,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLES INCONNUS                                              
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
# ******* TABLE DES COMPTES                                                    
#    RSFX00   : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE D'ATTRIBUTION DES N�DE PIECES                                  
#    RSFX05   : NAME=RSFX05M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFX05 /dev/null
# ******* TABLE ENTETE DE VENTES                                               
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE VENTE PRESTATION                                               
#    RSGV13   : NAME=RSGV13M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV13 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES SEQUENCES D'INTERFACES                                     
#    RSFT29   : NAME=RSFT29M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMENTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00 /dev/null
# ******* FICHIER DES VENTES VERS GCT                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FFV001 ${DATA}/PNCGM/F89.BFS052AM
# ******* FICHIER DES ANNULATIONS POUR PGM BPS020 HEBDO                        
       m_FileAssign -d SHR FX053 /dev/null
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PXX0/F89.BFS052CM
# ******* FICHIER DES PRESTATIONS                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052MAA.BFS052EM
#                                                                              
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IFX052 IFX052
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FS052MOM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS052M
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS052 
       JUMP_LABEL=FS052MAB
       ;;
(FS052MAB)
       m_CondExec 04,GE,FS052MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BBS052 : DUPLICATION DU BFS052 POUR BIENS ET SERVICES                   
#  ----------                                                                  
#  REPRISE : OUI (VOIR EXPLICATION EN D�BUT DE PCL)                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FS052MAD
       ;;
(FS052MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN ENTREE                                                      
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGS65   : NAME=RSGS65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS65 /dev/null
#    RSBC12   : NAME=RSBC12P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC12 /dev/null
#    RSBC15   : NAME=RSBC15P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBC15 /dev/null
#    RSPR00   : NAME=RSPR00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#    RSPR30   : NAME=RSPR30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPR30 /dev/null
#    RSPR55   : NAME=RSPR55P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR55 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSFT23   : NAME=RSFT23M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT23 /dev/null
#                                                                              
# ******* TABLES EN MAJ                                                        
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV11   : NAME=RSGV11M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE LIGNE DE REGLEMENT                                             
#    RSGV14   : NAME=RSGV14M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* TABLE DES DOSSIERS DE CREDIT                                         
#    RSDC00   : NAME=RSDC00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC00 /dev/null
# ******* TABLE DES PAIEMANTS DIFFERES                                         
#    RSDC10   : NAME=RSDC10M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSDC10 /dev/null
# ******* TABLE DES CONTRATS                                                   
#    RSPS00   : NAME=RSPS00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00 /dev/null
#                                                                              
# ******* FICHIER EN SORTIE                                                    
# ******* FICHIER DES VENTES VERS FTICSM                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI00 ${DATA}/PNCGM/F89.BBS052AM
# ******* FICHIER DES ANNULATIONS                                              
       m_FileAssign -d SHR FX053 /dev/null
# ******* FICHIER DES MVTS DES CONTRATS                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FPS200 ${DATA}/PNCGM/F89.BBS052CM
# ******* FICHIER DES PRETATIONS                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 FFS052 ${DATA}/PTEM/FS052MAD.BBS052EM
#                                                                              
# ******* EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w IBS052 IFX052
#                                                                              
# ******* FICHIERS PARAM�TRE                                                   
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* CODE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******* DATE DE DCOMPTA (A BLANC SAUF INSTRUCTIONS SPECIALES)                
       m_FileAssign -d SHR DCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/BS052MOM
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/BS052M
#                                                                              
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F89.FDATMGDM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBS052 
       JUMP_LABEL=FS052MAE
       ;;
(FS052MAE)
       m_CondExec 04,GE,FS052MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL DU FICHIER DES PRESTATIONS                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FS052MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FS052MAG
       ;;
(FS052MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FS052MAA.BFS052EM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BFS052FM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/FS052MAD.BBS052EM
       m_FileAssign -d NEW,CATLG,DELETE -r 300 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGM/F89.BFS052FM
# ****                                                                         
#  DEPENDANCES POUR PLAN :                                                     
#                                                                              
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_76_6 76 CH 6
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_52_8 52 CH 8
 /KEYS
   FLD_CH_82_5 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_52_8 ASCENDING,
   FLD_CH_76_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FS052MZA
       ;;
(FS052MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FS052MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FS052MAH
       ;;
(FS052MAH)
       m_CondExec 00,EQ,FS052MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
