#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRD00O.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGRD00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/05/16 AT 10.33.00 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GRD00O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DU TABLESPACE RSRD00O                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRD00OA
       ;;
(GRD00OA)
#
#GRD00OAA
#GRD00OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GRD00OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=GRD00OAD
       ;;
(GRD00OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BHV030AO
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00OAD.BRD005FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "VEN"
 /DERIVEDFIELD CST_3_11 "VEN"
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_33_7 33 CH 7
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_7 OR FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_7 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OAE
       ;;
(GRD00OAE)
       m_CondExec 00,EQ,GRD00OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD005                                                                
# ********************************************************************         
#   TRAITEMENT DE LA REDEVANCE AUDIOVISUELLE                                   
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAG
       ;;
(GRD00OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A1} FGS41 ${DATA}/PTEM/GRD00OAD.BRD005FO
# ******  TABLE DES ARTICLES                                                   
#    RSGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE DES FAMILLES/TYPE DE DECLARATION                               
#    RSGA15O  : NAME=RSGA15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15O /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22O  : NAME=RSGA22O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22O /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02O  : NAME=RSGV02O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV02O /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10O  : NAME=RSGV10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  TABLE DES LIGNES DE VENTES                                           
#    RSGV11O  : NAME=RSGV11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00O  : NAME=RSRD00O,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00O  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00O /dev/null
# ******  FICHIER DES VENTES A DECLARER                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 FRD005 ${DATA}/PXX0/F16.BRD005HO
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 FRD006 ${DATA}/PTEM/GRD00OAG.BRD006AO
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 278 -g +1 FRD007 ${DATA}/PTEM/GRD00OAG.BRD007AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD005 
       JUMP_LABEL=GRD00OAH
       ;;
(GRD00OAH)
       m_CondExec 04,GE,GRD00OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD020                                                                
# ********************************************************************         
#  EDITION DES VENTES SOUMISES A LA REDEVANCE AUDIOVISUELLE                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#  AAK      STEP  PGM=BRD020,LANG=CBL                                          
#  FICHIER DATE                                                                
# FDATE    DATA CLASS=VAR,PARMS=FDATE,MBR=FDATE                                
#  FICHIER DES VENTES A DECLARER                                               
#  FRD005   FILE  NAME=BRD005HO,MODE=I                                         
#  EDITION DES VENTES A DECLARER                                               
#  IRD020   REPORT SYSOUT=(9,IRD020)                                           
# ********************************************************************         
#  PGM : BRD015                                                                
# ********************************************************************         
#  EDITION DES ANOMALIES                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAJ PGM=BRD015     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAJ
       ;;
(GRD00OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d SHR -g ${G_A2} FRD007 ${DATA}/PTEM/GRD00OAG.BRD007AO
# ******  EDITION DES ANOMALIES                                                
# IRD015   REPORT SYSOUT=(9,IRD015)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD015 ${DATA}/PXX0/F16.BIRD015O
       m_ProgramExec BRD015 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAM PGM=IEBGENER   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAM
       ;;
(GRD00OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PXX0/F16.BIRD015O
       m_OutputAssign -c 9 -w IRD015 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OAN
       ;;
(GRD00OAN)
       m_CondExec 00,EQ,GRD00OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI DU FICHIER FRD006 AVANT EDITION DE CONTROLE                          
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAQ
       ;;
(GRD00OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GRD00OAG.BRD006AO
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 SORTOUT ${DATA}/PEX0/F16.BRD006BM.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_10 17 CH 10
 /KEYS
   FLD_CH_17_10 ASCENDING
 /* Record Type = F  Record Length = 191 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OAR
       ;;
(GRD00OAR)
       m_CondExec 00,EQ,GRD00OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD010                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAT PGM=BRD010     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAT
       ;;
(GRD00OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A5} FRD006 ${DATA}/PEX0/F16.BRD006BM.ETAT
# ******  EDITION LISTE CONTROLE                                               
# IRD010   REPORT SYSOUT=(9,IRD010)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD010 ${DATA}/PXX0/F16.BIRD010O
       m_ProgramExec BRD010 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OAX PGM=IEBGENER   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OAX
       ;;
(GRD00OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A6} SYSUT1 ${DATA}/PXX0/F16.BIRD010O
       m_OutputAssign -c 9 -w IRD010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OAY
       ;;
(GRD00OAY)
       m_CondExec 00,EQ,GRD00OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER STOCK (DD FGS41 DU PGM BRD005)                             
#   CODIC = 19,7                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBA
       ;;
(GRD00OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GRD00OAD.BRD005FO
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00OBA.BRD005GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 07
 /KEYS
   FLD_CH_19_7 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OBB
       ;;
(GRD00OBB)
       m_CondExec 00,EQ,GRD00OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER STOCK (DD FGS41 DU PGM BRD005)                             
#   CODIC = 19,7                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBD
       ;;
(GRD00OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GRD00OAD.BRD005FO
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g ${G_A9} SORTOUT ${DATA}/PTEM/GRD00OBA.BRD005GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 07
 /KEYS
   FLD_CH_19_7 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OBE
       ;;
(GRD00OBE)
       m_CondExec 00,EQ,GRD00OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **AJOUT*PGM*BRD040**************************************************         
# ********************************************************************         
#  PGM : BRD040                                                                
# ********************************************************************         
#   CONSTITUTION DES FICHIERS EDDITION DE CONTROLE DES DECLARATION             
#   REPRISE: OUI SI ABEND                                                      
#            CE PGM NE FAIT AUCUNE MAJ SUR LES TABLES DB2                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBG
       ;;
(GRD00OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#         FICHIERS EN ENTREE                                                   
#                                                                              
# ****  FICHIER RESULTAT DES STOCKS TRIES                                      
       m_FileAssign -d SHR -g ${G_A10} FGS41 ${DATA}/PTEM/GRD00OBA.BRD005GO
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER SOCIMTE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER MOIS                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******  CODIC SOUMIS A DECLARATION                                           
#    RSGA51O  : NAME=RSGA51O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA51O /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10O  : NAME=RSGV10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00R  : NAME=RSRD00O,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00O  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00R /dev/null
#                                                                              
#         FICHIERS EN SORTIES                                                  
#                                                                              
# ******  FICHIER DES CODICS A DECLARER ET DECLARES : LRECL 63                 
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 FRD040 ${DATA}/PTEM/GRD00OBG.BRD040AO
# ******  FICHIER DES CODICS NON DECLARABLES:         LRECL 75                 
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 FRD050 ${DATA}/PTEM/GRD00OBG.BRD050AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD040 
       JUMP_LABEL=GRD00OBH
       ;;
(GRD00OBH)
       m_CondExec 04,GE,GRD00OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD040 DU PGM BRD041            
#   POUR EDITION ETAT IRD040                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBJ
       ;;
(GRD00OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GRD00OBG.BRD040AO
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00OBJ.BRD040BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OBK
       ;;
(GRD00OBK)
       m_CondExec 00,EQ,GRD00OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD041 DU PGM BRD041            
#   POUR EDITION ETAT IRD041                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBM
       ;;
(GRD00OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GRD00OBG.BRD040AO
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00OBM.BRD040CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OBN
       ;;
(GRD00OBN)
       m_CondExec 00,EQ,GRD00OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD042 DU PGM BRD041            
#   POUR EDITION ETAT IRD042                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBQ
       ;;
(GRD00OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GRD00OBG.BRD040AO
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PEX0/F16.BRD040DM.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OBR
       ;;
(GRD00OBR)
       m_CondExec 00,EQ,GRD00OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD041                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD040 + IRD041 + IRD042                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBT PGM=BRD041     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBT
       ;;
(GRD00OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A14} FRD040 ${DATA}/PTEM/GRD00OBJ.BRD040BO
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A15} FRD041 ${DATA}/PTEM/GRD00OBM.BRD040CO
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A16} FRD042 ${DATA}/PEX0/F16.BRD040DM.ETAT
# ******  EDITION LISTE CONTROLE                                               
# IRD040   REPORT SYSOUT=(9,IRD040),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -g +1 IRD040 ${DATA}/PXX0/F16.BIRD040O
# ******  EDITION LISTE CONTROLE                                               
# IRD041   REPORT SYSOUT=(9,IRD041),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD041 ${DATA}/PXX0/F16.BIRD041O
# ******  EDITION LISTE CONTROLE NON UILISE CGA LE 081098                      
# IRD042   REPORT SYSOUT=(9,IRD042),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_OutputAssign -c "*" IRD042
       m_ProgramExec BRD041 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OBX PGM=IEBGENER   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OBX
       ;;
(GRD00OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A17} SYSUT1 ${DATA}/PXX0/F16.BIRD041O
       m_OutputAssign -c 9 -w IRD041 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OBY
       ;;
(GRD00OBY)
       m_CondExec 00,EQ,GRD00OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCA PGM=IEBGENER   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCA
       ;;
(GRD00OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A18} SYSUT1 ${DATA}/PXX0/F16.BIRD040O
       m_OutputAssign -c 9 -w IRD040 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OCB
       ;;
(GRD00OCB)
       m_CondExec 00,EQ,GRD00OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD050 DU PGM BRD050            
#   POUR EDITION ETAT IRD050                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCD
       ;;
(GRD00OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/GRD00OBG.BRD050AO
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00OCD.BRD050BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_53_6 53 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OCE
       ;;
(GRD00OCE)
       m_CondExec 00,EQ,GRD00OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD051 DU PGM BRD050            
#   POUR EDITION ETAT IRD051                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCG
       ;;
(GRD00OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GRD00OBG.BRD050AO
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00OCG.BRD050CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_PD_53_6 53 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OCH
       ;;
(GRD00OCH)
       m_CondExec 00,EQ,GRD00OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD052 DU PGM BRD050            
#   POUR EDITION ETAT IRD052                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCJ
       ;;
(GRD00OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/GRD00OBG.BRD050AO
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PEX0/F16.BRD050DM.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_26_7 26 CH 7
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00OCK
       ;;
(GRD00OCK)
       m_CondExec 00,EQ,GRD00OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD050                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD050 + IRD051 + IRD052                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCM PGM=BRD050     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCM
       ;;
(GRD00OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A22} FRD050 ${DATA}/PTEM/GRD00OCD.BRD050BO
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A23} FRD051 ${DATA}/PTEM/GRD00OCG.BRD050CO
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A24} FRD052 ${DATA}/PEX0/F16.BRD050DM.ETAT
# ******  EDITION LISTE CONTROLE                                               
# IRD050   REPORT SYSOUT=(9,IRD050),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -g +1 IRD050 ${DATA}/PXX0/F16.BIRD050O
# ******  EDITION LISTE CONTROLE                                               
# IRD051   REPORT SYSOUT=(9,IRD051),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD051 ${DATA}/PXX0/F16.BIRD051O
# ******  EDITION LISTE CONTROLE NON UTILISE CGA LE 081098                     
# IRD052   REPORT SYSOUT=(9,IRD052),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_OutputAssign -c "*" IRD052
       m_ProgramExec BRD050 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCQ PGM=IEBGENER   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCQ
       ;;
(GRD00OCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A25} SYSUT1 ${DATA}/PXX0/F16.BIRD051O
       m_OutputAssign -c 9 -w IRD051 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OCR
       ;;
(GRD00OCR)
       m_CondExec 00,EQ,GRD00OCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCT PGM=IEBGENER   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCT
       ;;
(GRD00OCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A26} SYSUT1 ${DATA}/PXX0/F16.BIRD050O
       m_OutputAssign -c 9 -w IRD050 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00OCU
       ;;
(GRD00OCU)
       m_CondExec 00,EQ,GRD00OCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BUR0075                                                               
# ********************************************************************         
#   PURGE DE LA RTRD00 TOUS CE QUI EST INFERIEUR A 90 JOURS                    
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00OCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OCX
       ;;
(GRD00OCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00O  : NAME=RSRD00O,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00O  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00O /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BUR0075 
       JUMP_LABEL=GRD00OCY
       ;;
(GRD00OCY)
       m_CondExec 04,GE,GRD00OCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSRD00O                                       
# ********************************************************************         
#   REPRISE : OUI APRES UN TERM UTILITY                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00ODA PGM=DSNUTILB   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00OZA
       ;;
(GRD00OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRD00OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
