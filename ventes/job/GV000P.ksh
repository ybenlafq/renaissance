#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV000P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/08/02 AT 16.04.26 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GV000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  CUMUL DU FICHIER HISTO FHV01                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GV000PA
       ;;
(GV000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2008/08/02 AT 16.04.26 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: GV000P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'CHRGT TABLES HISTO'                    
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GV000PAA
       ;;
(GV000PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.HV01RP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV0/F07.GV0152AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV0/F07.GV0153AP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PGV0/F07.HV01RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_9 11 CH 9
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
 /* Record Type = F  Record Length = 39 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV000PAB
       ;;
(GV000PAB)
       m_CondExec 00,EQ,GV000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV000PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV000PAD
       ;;
(GV000PAD)
       m_CondExec ${EXAAF},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSHV09   : NAME=RSHV09,MODE=I - DYNAM=YES                                 
#    RSHV10   : NAME=RSHV10,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 SYSREC01 ${DATA}/PTEM/GV000PAD.HV09UP
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 SYSREC02 ${DATA}/PTEM/GV000PAD.HV10UP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV000PAD.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  TRI DU FICHIER D'UNLOAD DE LA RTHV10 AVEC LE FIC CREE PAR BGV150            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV000PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV000PAG
       ;;
(GV000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV000PAD.HV10UP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV0/F07.GV0150AP
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.HV10RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_16 1 CH 16
 /KEYS
   FLD_CH_1_16 ASCENDING
 /* Record Type = F  Record Length = 36 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV000PAH
       ;;
(GV000PAH)
       m_CondExec 00,EQ,GV000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : LOAD                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA TABLE RTHV10                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV000PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV000PAJ
       ;;
(GV000PAJ)
       m_CondExec ${EXAAP},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                                      
#    RSHV10   : NAME=RSHV10,MODE=(U,N) - DYNAM=YES                             
# -X-RSHV10   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV10 /dev/null
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F07.RELOAD.HV10RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV000PAJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GV000P_GV000PAJ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV000PAK
       ;;
(GV000PAK)
       m_CondExec 04,GE,GV000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER D'UNLOAD DE LA RTHV09 AVEC LE FIC CREE PAR BGV150            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV000PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV000PAM
       ;;
(GV000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GV000PAD.HV09UP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGV0/F07.GV0158AP
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 SORTOUT ${DATA}/PXX0/F07.RELOAD.HV09RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_6 9 CH 6
 /FIELDS FLD_PD_15_4 15 PD 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_19_6 19 PD 6
 /FIELDS FLD_PD_25_6 25 PD 6
 /FIELDS FLD_CH_4_5 4 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_5 ASCENDING,
   FLD_CH_9_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_15_4,
    TOTAL FLD_PD_19_6,
    TOTAL FLD_PD_25_6
 /* Record Type = F  Record Length = 37 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV000PAN
       ;;
(GV000PAN)
       m_CondExec 00,EQ,GV000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : LOAD                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV000PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV000PAQ
       ;;
(GV000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                          
#    RSHV09   : NAME=RSHV09,MODE=(U,N) - DYNAM=YES                             
# -X-RSHV09   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09 /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F07.RELOAD.HV09RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV000PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/GV000P_GV000PAQ_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV000PAR
       ;;
(GV000PAR)
       m_CondExec 04,GE,GV000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV000PZA
       ;;
(GV000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
