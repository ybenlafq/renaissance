#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BAC5FP.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBAC5F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/11/19 AT 12.18.37 BY BURTEC2                      
#    STANDARDS: P  JOBSET: BAC5FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PURGE DE La TABLE RTAC01                                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=BAC5FPA
       ;;
(BAC5FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2012/11/19 AT 12.18.37 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: BAC5FP                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'PURG TB CA'                            
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=BAC5FPAA
       ;;
(BAC5FPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# ********************************************                                 
       m_FileAssign -d NEW,CATLG,CATLG -r 80 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.FAC01UNP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BAC5FPAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  SELECT DATE DU JOUR - 3 ANS                                                 
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BAC5FPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BAC5FPAD
       ;;
(BAC5FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 SYSPRINT ${DATA}/PTEM/BAC5FPAD.BAC05FAP
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BAC5FPAD.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=BAC5FPAE
       ;;
(BAC5FPAE)
       m_CondExec 04,GE,BAC5FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   FORMATTAGE DE LA SYSIN                                                     
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BAC5FPAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BAC5FPAG
       ;;
(BAC5FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/BAC5FPAD.BAC05FAP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BAC05FBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_BLANK_1_5  X"27"
 /DERIVEDFIELD CST_1_3 "1_!"
 /DERIVEDFIELD CST_BLANK_3_7  X"27"
 /DERIVEDFIELD CST_0_4  "WHERE DVENTE < "
 /FIELDS FLD_CH_65_8 65 CH 8
 /FIELDS FLD_CH_61_3 61 CH 3
 /CONDITION CND_1 FLD_CH_61_3 EQ CST_1_3 
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT CST_0_4,CST_BLANK_1_5,FLD_CH_65_8,CST_BLANK_3_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=BAC5FPAH
       ;;
(BAC5FPAH)
       m_CondExec 00,EQ,BAC5FPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **                    STEP REORG DISCARD                           *         
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BAC5FPAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=BAC5FPAJ
       ;;
(BAC5FPAJ)
       m_CondExec ${EXAAP},NE,YES 
#    RSAC01   : NAME=RSAC01P,MODE=(U,N) - DYNAM=YES                            
# -X-BAC5FPR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSAC01 /dev/null
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_OutputAssign -c "*" UTPRINT
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSPUNCH ${MT_TMP}/SYSPUNCH_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSREC ${MT_TMP}/SYSREC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ******* FICHIER DE FULL IMAGE COPY                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSCOPY ${DATA}/PXX0/F07.BAC05CPY.RSAC01
# ******* FICHIER DES ENREGS SUPPRIM�S DE LA TABLE                             
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSDISC ${DATA}/PXX0/F07.BAC05DIS.RSAC01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BAC5FPAJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.BAC05FBP(+1),DISP=SHR                    ~         
#
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=BAC5FPAK
       ;;
(BAC5FPAK)
       m_CondExec 04,GE,BAC5FPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BAC5FPZA
       ;;
(BAC5FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BAC5FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
