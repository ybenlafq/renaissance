#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MD204P.ksh                       --- VERSION DU 17/10/2016 18:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMD204 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/30 AT 17.41.34 BY PREPA3                       
#    STANDARDS: P  JOBSET: MD204P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ******************************************************************           
#  DELETE DES FICHIERS NON GDG                                                 
#  REPRISE : OUI                                                               
# ******************************************************************           
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MD204PA
       ;;
(MD204PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MD204PAA
       ;;
(MD204PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD204PAB
       ;;
(MD204PAB)
       m_CondExec 16,NE,MD204PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSYNT10 : EXTRACTION DES VENTES GV _A DESTINATION DU METIER                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAD
       ;;
(MD204PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#                                                                              
# ****                                                                         
# **** FICHIERS D'EXTACT DES VENTES                                            
# ****                                                                         
# ** LISTE DES VENTES ENTI�REMENTS TOP�ES LIVRAISONS DETAIL                    
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ FSYNT1 ${DATA}/PXX0.F07.BSYNT1CP
# ** LISTE DES VENTES ENTI�REMENTS TOP�ES LIVRAISONS CUISINES                  
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ FSYNT2 ${DATA}/PXX0.F07.BSYNT1DP
# ** LISTE DES PRESTATIONS ACOMPTE UNIQUEMENT, NON REPRISES                    
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ FSYNT3 ${DATA}/PXX0.F07.BSYNT1EP
# ** LISTE AVEC VENTES CONTENANT UNE PRESTATION ACCOMPTE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ FSYNT4 ${DATA}/PXX0.F07.BSYNT1FP
# ** VENTES DESEQUILIBR�ES ENCAISSEMENT CREDITS                                
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ FSYNT5 ${DATA}/PXX0.F07.BSYNT1GP
# ** VENTES DESEQUILIBR�ES ENCAISSEMENT A RECEPTION FACTURE                    
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ FSYNT6 ${DATA}/PXX0.F07.BSYNT1HP
# ** LISTE DES VENTE AVEC DES REPRISES NON TOP�ES                              
       m_FileAssign -d NEW,CATLG,DELETE -r 111 -t LSEQ FSYNT7 ${DATA}/PXX0.F07.BSYNT1IP
# ** LISTE DES VENTE PAY�ES NON D�STOCK�ES.                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -t LSEQ FSYNT8 ${DATA}/PXX0.F07.BSYNT1JP
# ** LISTE DES VENTE PAY�ES NON D�STOCK�ES.                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ FSYNT9 ${DATA}/PXX0.F07.BSYNT1KP
# ** LISTE DES VENTE PAY�ES NON D�STOCK�ES.                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ FSYNT10 ${DATA}/PXX0.F07.BSYNT1LP
#                                                                              
# ***  DATE DE TRAITEMENT                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSYNT10 
       JUMP_LABEL=MD204PAE
       ;;
(MD204PAE)
       m_CondExec 04,GE,MD204PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 907                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAG
       ;;
(MD204PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAG.BTTGV907
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAH
       ;;
(MD204PAH)
       m_CondExec 00,EQ,MD204PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 908                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAJ
       ;;
(MD204PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAJ.BTTGV908
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAK
       ;;
(MD204PAK)
       m_CondExec 00,EQ,MD204PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 916                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAM
       ;;
(MD204PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAM.BTTGV916
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAN
       ;;
(MD204PAN)
       m_CondExec 00,EQ,MD204PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 945                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAQ
       ;;
(MD204PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAQ.BTTGV945
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_CH_1_35 01 CH 35
 /FIELDS FLD_CH_2_3 02 CH 03
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAR
       ;;
(MD204PAR)
       m_CondExec 00,EQ,MD204PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 961                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAT
       ;;
(MD204PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAT.BTTGV961
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAU
       ;;
(MD204PAU)
       m_CondExec 00,EQ,MD204PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 989                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MD204PAX
       ;;
(MD204PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PAX.BTTGV989
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "989"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PAY
       ;;
(MD204PAY)
       m_CondExec 00,EQ,MD204PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE GV POUR 991                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBA
       ;;
(MD204PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1KP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBA.BTTGV991
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "991"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBB
       ;;
(MD204PBB)
       m_CondExec 00,EQ,MD204PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 907                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBD
       ;;
(MD204PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBD.BTTVE907
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBE
       ;;
(MD204PBE)
       m_CondExec 00,EQ,MD204PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 908                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBG
       ;;
(MD204PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBG.BTTVE908
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBH
       ;;
(MD204PBH)
       m_CondExec 00,EQ,MD204PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 916                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBJ
       ;;
(MD204PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBJ.BTTVE916
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBK
       ;;
(MD204PBK)
       m_CondExec 00,EQ,MD204PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 945                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBM
       ;;
(MD204PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBM.BTTVE945
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBN
       ;;
(MD204PBN)
       m_CondExec 00,EQ,MD204PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 961                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBQ
       ;;
(MD204PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBQ.BTTVE961
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_CH_2_3 02 CH 03
 /FIELDS FLD_CH_1_35 01 CH 35
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBR
       ;;
(MD204PBR)
       m_CondExec 00,EQ,MD204PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 989                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBT
       ;;
(MD204PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBT.BTTVE989
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "989"
 /FIELDS FLD_CH_1_35 01 CH 35
 /FIELDS FLD_CH_2_3 02 CH 03
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBU
       ;;
(MD204PBU)
       m_CondExec 00,EQ,MD204PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC VTE NON TOPE VE POUR 991                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=MD204PBX
       ;;
(MD204PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.BSYNT1LP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD204PBX.BTTVE991
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "991"
 /FIELDS FLD_CH_1_35 01 CH 35
 /FIELDS FLD_CH_2_3 02 CH 03
 /CONDITION CND_1 FLD_CH_2_3 EQ CST_1_4 
 /KEYS
   FLD_CH_1_35 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD204PBY
       ;;
(MD204PBY)
       m_CondExec 00,EQ,MD204PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DES FICHIERS  DES VENTES _A DESTINATION DU METIER                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PCA PGM=IDCAMS     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=MD204PCA
       ;;
(MD204PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PCA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD204PCB
       ;;
(MD204PCB)
       m_CondExec 16,NE,MD204PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#        CONCATENATION AVEC FICHIER EN TETE COML.AD.ENTETE                     
#  REPRISE: AU DELETE PRECEDENT                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PCD PGM=IDCAMS     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=MD204PCD
       ;;
(MD204PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******* FIC GV 907                                                           
       m_FileAssign -d SHR IN1 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PTEM/MD204PAG.BTTGV907
# ******* FIC GV 908                                                           
       m_FileAssign -d SHR IN2 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/MD204PAJ.BTTGV908
# ******* FIC GV 916                                                           
       m_FileAssign -d SHR IN3 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/MD204PAM.BTTGV916
# ******* FIC GV 945                                                           
       m_FileAssign -d SHR IN4 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/MD204PAQ.BTTGV945
# ******* FIC GV 961                                                           
       m_FileAssign -d SHR IN5 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/MD204PAT.BTTGV961
# ******* FIC GV 989                                                           
       m_FileAssign -d SHR IN6 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/MD204PAX.BTTGV989
# ******* FIC GV 991                                                           
       m_FileAssign -d SHR IN7 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/MD204PBA.BTTGV991
# ******* FIC VE 907                                                           
       m_FileAssign -d SHR IN8 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/MD204PBD.BTTVE907
# ******* FIC VE 908                                                           
       m_FileAssign -d SHR IN9 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/MD204PBG.BTTVE908
# ******* FIC VE 916                                                           
       m_FileAssign -d SHR IN10 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/MD204PBJ.BTTVE916
# ******* FIC VE 945                                                           
       m_FileAssign -d SHR IN11 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/MD204PBM.BTTVE945
# ******* FIC VE 961                                                           
       m_FileAssign -d SHR IN12 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/MD204PBQ.BTTVE961
# ******* FIC VE 989                                                           
       m_FileAssign -d SHR IN13 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/MD204PBT.BTTVE989
# ******* FIC VE 991                                                           
       m_FileAssign -d SHR IN14 ${DATA}/PXX0.F07.BENTETEP
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/MD204PBX.BTTVE991
# ******* FIC TRIE CUMUL� CONCATAINER _A L'EN-T�TE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT1 ${DATA}/PXX0.F07.BVTGV907
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT2 ${DATA}/PXX0.F07.BVTGV908
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT3 ${DATA}/PXX0.F07.BVTGV916
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT4 ${DATA}/PXX0.F07.BVTGV945
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT5 ${DATA}/PXX0.F07.BVTGV961
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT6 ${DATA}/PXX0.F07.BVTGV989
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT7 ${DATA}/PXX0.F07.BVTGV991
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT8 ${DATA}/PXX0.F07.BVTVE907
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT9 ${DATA}/PXX0.F07.BVTVE908
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT10 ${DATA}/PXX0.F07.BVTVE916
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT11 ${DATA}/PXX0.F07.BVTVE945
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT12 ${DATA}/PXX0.F07.BVTVE961
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT13 ${DATA}/PXX0.F07.BVTVE989
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ OUT14 ${DATA}/PXX0.F07.BVTVE991
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PCD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD204PCE
       ;;
(MD204PCE)
       m_CondExec 16,NE,MD204PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHIERS                                                            
#  REPRISE : NON IL FAUT REPRENDRE AU 1ER STEP                                 
# ******************************************************************           
# ADM      STEP  PGM=PKZIP,RSTRT=SAME                                          
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -ARCHIVE(":BSYNT999"")                                                      
#  -ARCHUNIT(SEM350)                                                           
#  -FILE_TERMINATOR()                                                          
#  -ZIPPED_DSN(":BSYNT1CP"",VTE_AUTRE_TOPE_NON_ENCAISS.CSV)                    
#  -ZIPPED_DSN(":BSYNT1DP"",VTE_CUIS_TOPE_NON_ENCAISS.CSV)                     
#  -ZIPPED_DSN(":BSYNT1EP"",PRESTA_ACOMPTE_NON_REPRISE.CSV)                    
#  -ZIPPED_DSN(":BSYNT1FP"",PRESTA_ACOMPTE_SEULE_NON_REPRISE.CSV)              
#  -ZIPPED_DSN(":BSYNT1GP"",DESEQUILIBRES_VTES_ENCAIS_CREDIT.CSV)              
#  -ZIPPED_DSN(":BSYNT1HP"",DESEQUILIBRES_VTES_ENCAIS_ARF.CSV)                 
#  -ZIPPED_DSN(":BSYNT1IP"",REPRISES_NON_TOPEES.CSV)                           
#  -ZIPPED_DSN(":BSYNT1JP"",VTES_PAYEES_NON_DESTOCKEES.CSV)                    
#  -ZIPPED_DSN(":BVTGV907"",VTES_NON_TOPEES_GV907.CSV)                         
#  -ZIPPED_DSN(":BVTGV908"",VTES_NON_TOPEES_GV908.CSV)                         
#  -ZIPPED_DSN(":BVTGV916"",VTES_NON_TOPEES_GV916.CSV)                         
#  -ZIPPED_DSN(":BVTGV945"",VTES_NON_TOPEES_GV945.CSV)                         
#  -ZIPPED_DSN(":BVTGV961"",VTES_NON_TOPEES_GV961.CSV)                         
#  -ZIPPED_DSN(":BVTGV989"",VTES_NON_TOPEES_GV989.CSV)                         
#  -ZIPPED_DSN(":BVTGV991"",VTES_NON_TOPEES_GV991.CSV)                         
#  -ZIPPED_DSN(":BVTVE907"",VTES_NON_TOPEES_VE907.CSV)                         
#  -ZIPPED_DSN(":BVTVE908"",VTES_NON_TOPEES_VE908.CSV)                         
#  -ZIPPED_DSN(":BVTVE916"",VTES_NON_TOPEES_VE916.CSV)                         
#  -ZIPPED_DSN(":BVTVE945"",VTES_NON_TOPEES_VE945.CSV)                         
#  -ZIPPED_DSN(":BVTVE961"",VTES_NON_TOPEES_VE961.CSV)                         
#  -ZIPPED_DSN(":BVTVE989"",VTES_NON_TOPEES_VE989.CSV)                         
#  -ZIPPED_DSN(":BVTVE991"",VTES_NON_TOPEES_VE991.CSV)                         
#  ":BSYNT1CP""                                                                
#  ":BSYNT1DP""                                                                
#  ":BSYNT1EP""                                                                
#  ":BSYNT1FP""                                                                
#  ":BSYNT1GP""                                                                
#  ":BSYNT1HP""                                                                
#  ":BSYNT1IP""                                                                
#  ":BSYNT1JP""                                                                
#  ":BVTGV907""                                                                
#  ":BVTGV908""                                                                
#  ":BVTGV916""                                                                
#  ":BVTGV945""                                                                
#  ":BVTGV961""                                                                
#  ":BVTGV989""                                                                
#  ":BVTGV991""                                                                
#  ":BVTVE907""                                                                
#  ":BVTVE908""                                                                
#  ":BVTVE916""                                                                
#  ":BVTVE945""                                                                
#  ":BVTVE961""                                                                
#  ":BVTVE989""                                                                
#  ":BVTVE991""                                                                
#         DATAEND                                                              
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PCG PGM=JVMLDM76   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=MD204PCG
       ;;
(MD204PCG)
       m_CondExec ${EXADM},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ FICZIP ${DATA}/PXX0.F07.BSYNT999
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PCG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY ( DESTINATAIRE LOTUS )                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADR      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BSYNT999,                                                           
#      FNAME=":BSYNT999""                                                      
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTMD204P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD204PCJ PGM=FTP        ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=MD204PCJ
       ;;
(MD204PCJ)
       m_CondExec ${EXADR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PCJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD204PCM PGM=EZACFSM1   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=MD204PCM
       ;;
(MD204PCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PCM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MD204PZA
       ;;
(MD204PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD204PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
