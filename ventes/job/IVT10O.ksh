#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT10O.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POIVT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/12/08 AT 16.02.49 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVT10O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    QUIESCE DU TABLESPACE RSIT15                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT10OA
       ;;
(IVT10OA)
#
#IVT10OAA
#IVT10OAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT10OAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVT10OAD
       ;;
(IVT10OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLE / GENERALITES ARTICLES                                 
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLES DES FAMILLES                                                  
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK SOUS LIEU MAGASIN                                        
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00O /dev/null
# ******  TABLE LIEUX INVENTORIES                                              
#    RSIT05O  : NAME=RSIT05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT05O /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSIT10O  : NAME=RSIT10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT10O /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSFL90O  : NAME=RSFL90O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFL90O /dev/null
# ******  PARAMETRE SOCIETE 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES CODICS A INVENTORIER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 FIT010 ${DATA}/PGI916/F16.BIT010AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT010 
       JUMP_LABEL=IVT10OAE
       ;;
(IVT10OAE)
       m_CondExec 04,GE,IVT10OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTIT15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAG
       ;;
(IVT10OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -g +1 SYSREC01 ${DATA}/PTEM/IVT10OAG.IT15UO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10OAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR N�INVENTAIRE ; NLIEU ; NSOCIETE ; NSSLIEU ; NCODIC                  
#  OUTREC 1 ; 35                                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAJ
       ;;
(IVT10OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI916/F16.BIT010AO
# ******  FICHIER DES CODICS POUR LOAD RTIT15                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PTEM/IVT10OAJ.BIT010GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_35 1 CH 35
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_35
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10OAK
       ;;
(IVT10OAK)
       m_CondExec 00,EQ,IVT10OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI N�2                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAM
       ;;
(IVT10OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVT10OAJ.BIT010GO
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/IVT10OAG.IT15UO
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PTEM/IVT10OAM.IT15RO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OAN
       ;;
(IVT10OAN)
       m_CondExec 00,EQ,IVT10OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTIT15                                                     
# ********************************************************************         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAQ
       ;;
(IVT10OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTIT15                                                
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PTEM/IVT10OAM.IT15RO
#                                                                              
#    RSIT15O  : NAME=RSIT15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15O /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10OAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVT10O_IVT10OAQ_RTIT15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVT10OAR
       ;;
(IVT10OAR)
       m_CondExec 04,GE,IVT10OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAT
       ;;
(IVT10OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI916/F16.BIT010AO
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10OAT.BIT010BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_39_5 39 CH 5
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OAU
       ;;
(IVT10OAU)
       m_CondExec 00,EQ,IVT10OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT011 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OAX PGM=BIT011     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OAX
       ;;
(IVT10OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A6} FIT011 ${DATA}/PTEM/IVT10OAT.BIT010BO
#  IIT011   REPORT SYSOUT=(9,BIT011),RECFM=FBA,BLKSIZE=1330,LRECL=133          
       m_OutputAssign -c "*" IIT011
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F16.BIT010CO
       m_ProgramExec BIT011 
# ********************************************************************         
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBA
       ;;
(IVT10OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI916/F16.BIT010AO
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10OBA.BIT010DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "2"
 /DERIVEDFIELD CST_3_14 " "
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_36_3 36 CH 3
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OBB
       ;;
(IVT10OBB)
       m_CondExec 00,EQ,IVT10OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT031 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBD PGM=BIT031     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBD
       ;;
(IVT10OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A8} FIT011 ${DATA}/PTEM/IVT10OBA.BIT010DO
       m_OutputAssign -c 9 -w BIT031 IIT031
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F16.BIT010EO
       m_ProgramExec BIT031 
# ********************************************************************         
#  BIT015  POUR TOUS LES CODICS A INVENTORIER DONT LA DATE DE                  
#          CONSTITUTUION DU STOCK THEORIQUE EST EGALE A LA DATE DE             
#          TRAITEMENT IL Y A CALCUL DU STOCK ET INSCRIPTION DANS RTIT1         
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBG
       ;;
(IVT10OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLES DES LIEUX                                                     
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  TABLE STOCK SOUS LIEU / MAGASINS                                     
#    RSGS30O  : NAME=RSGS30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  TABLE STOCK                                                          
#    RSGS60O  : NAME=RSGS60O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60O /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00O /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15O  : NAME=RSIT15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15O /dev/null
# ******  PARAMETRE SOCIETE 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT015 
       JUMP_LABEL=IVT10OBH
       ;;
(IVT10OBH)
       m_CondExec 04,GE,IVT10OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT016  CORRECTION DES STOCKS CALCULES AU BIT015 QUAND IL Y A               
#  DES MVTS DE MUTS OU DE LIVRAISONS                                           
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
# ACD      STEP  PGM=IKJEFT01                                                  
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *******  TABLE  GENERALISEE                                                  
# RSGA01O  FILE  DYNAM=YES,NAME=RSGA01O,MODE=I                                 
# *******  TABLES DES LIEUX                                                    
# RSGA10O  FILE  DYNAM=YES,NAME=RSGA10O,MODE=I                                 
# *******  TABLE MUTATIONS                                                     
# RSGB05   FILE  DYNAM=YES,NAME=RSGB05,MODE=I                                  
# *******  TABLE MUTATIONS                                                     
# RSGB15   FILE  DYNAM=YES,NAME=RSGB15,MODE=I                                  
# *******  TABLE                                                               
# RSTL10   FILE  DYNAM=YES,NAME=RSTL10,MODE=I                                  
# *******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                          
# RSIT00O  FILE  DYNAM=YES,NAME=RSIT00O,MODE=I                                 
# *******  TABLE DES CODICS A INVENTORIER                                      
# RSIT15O  FILE  DYNAM=YES,NAME=RSIT15O,MODE=U,REST=NO                         
# *******  PARAMETRE SOCIETE 916                                               
# FNSOC    DATA  CLASS=FIX1,MBR=SOCMGIO                                        
# *******  FICHIER DATE SOUS LA FORME JJMMSSAA                                 
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BIT016)  PLAN(BTDARO)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BIT035                                                                      
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBJ
       ;;
(IVT10OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE STOCKS                                                         
#    RSGS36O  : NAME=RSGS36O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36O /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00O /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15O  : NAME=RSIT15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15O /dev/null
# ******  PARAMETRE SOCIETE 916                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT035 
       JUMP_LABEL=IVT10OBK
       ;;
(IVT10OBK)
       m_CondExec 04,GE,IVT10OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT100  BUT: PRAPARATION DU FICHIER POUR EDITION DES ETIQUETTES             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBM
       ;;
(IVT10OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
#    RSGA59O  : NAME=RSGA59O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59O /dev/null
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00O /dev/null
#    RSIT15O  : NAME=RSIT15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15O /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PTEM/IVT10OBM.BIT100AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT100 
       JUMP_LABEL=IVT10OBN
       ;;
(IVT10OBN)
       m_CondExec 04,GE,IVT10OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBQ
       ;;
(IVT10OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/IVT10OBM.BIT100AO
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/IVT10OBQ.BIT100BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_16_15 16 CH 15
 /FIELDS FLD_CH_97_25 97 CH 25
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OBR
       ;;
(IVT10OBR)
       m_CondExec 00,EQ,IVT10OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBT PGM=BIV105     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBT
       ;;
(IVT10OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES                                               
       m_FileAssign -d SHR -g ${G_A10} FIN105 ${DATA}/PTEM/IVT10OBQ.BIT100BO
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIT040  BUT: EDITION D'UN ETAT D INVENTAIRE COMLPLET                        
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OBX
       ;;
(IVT10OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
#    RSGA67O  : NAME=RSGA67O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67O /dev/null
#    RSEG90O  : NAME=RSEG90O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG90O /dev/null
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00O /dev/null
#    RSIT15O  : NAME=RSIT15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15O /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IIT004 ${DATA}/PTEM/IVT10OBX.BIT040AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT040 
       JUMP_LABEL=IVT10OBY
       ;;
(IVT10OBY)
       m_CondExec 04,GE,IVT10OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIT040AO)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OCA
       ;;
(IVT10OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IVT10OBX.BIT040AO
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10OCA.BIT040BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_5 18 CH 05
 /FIELDS FLD_BI_12_3 12 CH 03
 /FIELDS FLD_BI_28_9 28 CH 09
 /FIELDS FLD_BI_23_5 23 CH 05
 /FIELDS FLD_BI_37_7 37 CH 07
 /FIELDS FLD_BI_7_5 7 CH 05
 /FIELDS FLD_BI_44_2 44 CH 02
 /FIELDS FLD_BI_15_3 15 CH 03
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING,
   FLD_BI_28_9 ASCENDING,
   FLD_BI_37_7 ASCENDING,
   FLD_BI_44_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OCB
       ;;
(IVT10OCB)
       m_CondExec 00,EQ,IVT10OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIT040CO                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OCD
       ;;
(IVT10OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/IVT10OCA.BIT040BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/IVT10OCD.BIT040CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IVT10OCE
       ;;
(IVT10OCE)
       m_CondExec 04,GE,IVT10OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OCG
       ;;
(IVT10OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/IVT10OCD.BIT040CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10OCG.BIT040DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10OCH
       ;;
(IVT10OCH)
       m_CondExec 00,EQ,IVT10OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIT004                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10OCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OCJ
       ;;
(IVT10OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/IVT10OCA.BIT040BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/IVT10OCG.BIT040DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIT004 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IVT10OCK
       ;;
(IVT10OCK)
       m_CondExec 04,GE,IVT10OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVT10OZA
       ;;
(IVT10OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
