#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD010P.ksh                       --- VERSION DU 08/10/2016 12:55
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/05/16 AT 08.26.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: TD010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DES FICHIERS SEQUENTIELS BTD010BP                                   
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TD010PA
       ;;
(TD010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RAAK=${RAAK:-TD010PAG}
       RUN=${RUN}
       JUMP_LABEL=TD010PAA
       ;;
(TD010PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD010PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD010PAB
       ;;
(TD010PAB)
       m_CondExec 16,NE,TD010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COPIE DU FICHIER GDG CR�ER PAR PROC VENAT DE NT VERS NON GDG                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAD
       ;;
(TD010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F99.FTD001P.ZIP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ OUT1 ${DATA}/PXX0.F07.BTD001BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD010PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD010PAE
       ;;
(TD010PAE)
       m_CondExec 16,NE,TD010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PRINT DU FICHIER POUR V�RIFIER S'IL EST VIDE OU PLEIN                       
#  DE FA�CON _A NE PAS PLANTER L'UNZIP SI LE FICHIER EST VIDE.                  
#  SI LE FICHIER EST VIDE, ON BYPASS TTE LA SUITE.                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAG
       ;;
(TD010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 ${DATA}/PXX0.F07.BTD001BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD010PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD010PAH
       ;;
(TD010PAH)
       m_CondExec 16,NE,TD010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEZIPPAGE DU FICHIER ZIPPE GDG                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAJ PGM=PKUNZIP    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAM
       ;;
(TD010PAM)
       m_CondExec ${EXAAU},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/TD001PAJ.BTD001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD010PAM.BTD001CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD010PAN
       ;;
(TD010PAN)
       m_CondExec 00,EQ,TD010PAM ${EXAAU},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD010                                                                
#  ------------                                                                
#  CREATION D UN FICHIER SEQUENTIEL A PARTIR DES BASES TD00                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAQ
       ;;
(TD010PAQ)
       m_CondExec ${EXAAZ},NE,YES 04,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   TABLE EN LECTURE                                                     
#    RTTD00   : NAME=RSTD00P,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTTD00 /dev/null
#    RTTD01   : NAME=RSTD01P,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTTD01 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#                                                                              
# *****   SORTIE                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 FTD010 ${DATA}/PTEM/TD010PAQ.BTD010AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD010 
       JUMP_LABEL=TD010PAR
       ;;
(TD010PAR)
       m_CondExec 04,GE,TD010PAQ ${EXAAZ},NE,YES 04,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2                                                                        
#  SORT DU FIC BTD010AP (RECL=42)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAT
       ;;
(TD010PAT)
       m_CondExec ${EXABE},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/TD010PAQ.BTD010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD010PAT.BTD010BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD010PAU
       ;;
(TD010PAU)
       m_CondExec 00,EQ,TD010PAT ${EXABE},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD011                                                                
#  ------------                                                                
#  FUSION DE FICHIERS FTD001 ET FTD010 POUR MAJ RTTD00                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=TD010PAX
       ;;
(TD010PAX)
       m_CondExec ${EXABJ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS FTD001 ET FTD010                                   
       m_FileAssign -d SHR -g ${G_A3} FTD001 ${DATA}/PTEM/TD010PAM.BTD001CP
       m_FileAssign -d SHR -g ${G_A4} FTD010 ${DATA}/PTEM/TD010PAT.BTD010BP
#                                                                              
# *****   TABLE EN MAJ                                                         
#    RTTD00   : NAME=RSTD00P,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTTD00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD011 
       JUMP_LABEL=TD010PAY
       ;;
(TD010PAY)
       m_CondExec 04,GE,TD010PAX ${EXABJ},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CR�ATION D'UNE G�N�RATION A VIDE                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD010PBA PGM=IDCAMS     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=TD010PBA
       ;;
(TD010PBA)
       m_CondExec ${EXABO},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.FTD001P.ZIP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD010PBA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD010PBB
       ;;
(TD010PBB)
       m_CondExec 16,NE,TD010PBA ${EXABO},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TD010PZA
       ;;
(TD010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
