#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV400L.ksh                       --- VERSION DU 09/10/2016 05:44
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/01/02 AT 10.07.05 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV400L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='VENTES'                                                            
# ********************************************************************         
#   TRI DU FICHIER SUR CODIC,SOC ORIG,LIEU ORIG,SOC DEST,LIEU DEST,DAT         
#   OPER,CODE INSEE,DATE CREAT,DATE STAT                                       
#   INCLUDE SUR LIEU ORIGINE ET LIEU DESTINATION                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV400LA
       ;;
(GV400LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV400LAA
       ;;
(GV400LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BHV030AL
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV400LAA.FGS400AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_18 "VEN"
 /DERIVEDFIELD CST_1_14 "VEN"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_88_5 88 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_122_6 122 CH 6
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_104_8 104 CH 8
 /CONDITION CND_2 FLD_CH_4_3 EQ CST_1_14 OR FLD_CH_13_3 EQ CST_3_18 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_70_8 ASCENDING,
   FLD_CH_88_5 ASCENDING,
   FLD_CH_104_8 ASCENDING,
   FLD_CH_122_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3,
    TOTAL FLD_PD_99_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400LAB
       ;;
(GV400LAB)
       m_CondExec 00,EQ,GV400LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV400 : EXTRACTION DES INFOS NECESSAIRES A L'EDITION DES VENTES            
#                          PAR ZONES DE SAV                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV400LAD
       ;;
(GV400LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******* TABLE DES PARAMETRES LIES AUX FAMILLES                               
#    RSGA30L  : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30L /dev/null
# ******* TABLE DE ARTICLES PAR SECTEUR DE SAV CODE DEPT AFFEC CODE ZO         
#    RSGA43L  : NAME=RSGA43L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA43L /dev/null
# ******* FIC ISSU DU TRI                                                      
       m_FileAssign -d SHR -g ${G_A1} FGS400 ${DATA}/PTEM/GV400LAA.FGS400AL
# ******* FIC D'EXTRACT DES INFOS PAR ZONES SAV                                
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 FGV400 ${DATA}/PTEM/GV400LAD.BGV400AL
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV400 
       JUMP_LABEL=GV400LAE
       ;;
(GV400LAE)
       m_CondExec 04,GE,GV400LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES INFOS D'EDITION DES VENTES                               
#  SECTEUR: 1,5,A   ZONE-SAV: 6,5,A    AGREGAT: 54,2   N�SEQ: 51,3,A           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV400LAG
       ;;
(GV400LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GV400LAD.BGV400AL
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV400LAG.BGV410AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_20 11 CH 20
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_CH_54_2 54 CH 2
 /FIELDS FLD_PD_56_5 56 PD 5
 /FIELDS FLD_PD_61_6 61 PD 6
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_11_20 ASCENDING,
   FLD_CH_54_2 ASCENDING,
   FLD_CH_51_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_56_5,
    TOTAL FLD_PD_61_6
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400LAH
       ;;
(GV400LAH)
       m_CondExec 00,EQ,GV400LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV410 : EDITION DES VENTES PAR SAV A PARTIR DU FICHIER D'EXTRACTS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400LAJ PGM=BGV410     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV400LAJ
       ;;
(GV400LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FIC D'EXTRACT DES INFOS TRI�                                         
       m_FileAssign -d SHR -g ${G_A3} FGV410 ${DATA}/PTEM/GV400LAG.BGV410AL
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* EDITION DES VENTES PAR ZONE DE SAV                                   
       m_OutputAssign -c 9 -w IGV410 IGV410
       m_ProgramExec BGV410 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV400LZA
       ;;
(GV400LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV400LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
