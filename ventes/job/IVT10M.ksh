#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT10M.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMIVT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/08/14 AT 09.21.26 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT10M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    QUIESCE DU TABLESPACE RSIT15                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT10MA
       ;;
(IVT10MA)
#
#IVT10MAA
#IVT10MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT10MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVT10MAD
       ;;
(IVT10MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLE / GENERALITES ARTICLES                                 
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLES DES FAMILLES                                                  
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK SOUS LIEU MAGASIN                                        
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE LIEUX INVENTORIES                                              
#    RSIT05   : NAME=RSIT05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT05 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSIT10   : NAME=RSIT10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT10 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSFL90   : NAME=RSFL90M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  PARAMETRE SOCIETE 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES CODICS A INVENTORIER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 FIT010 ${DATA}/PGI989/F89.BIT010AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT010 
       JUMP_LABEL=IVT10MAE
       ;;
(IVT10MAE)
       m_CondExec 04,GE,IVT10MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTIT15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAG
       ;;
(IVT10MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -t LSEQ -g +1 SYSREC01 ${DATA}/PGI989/F89.UNLOAD.IT15UM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10MAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR N�INVENTAIRE ; NLIEU ; NSOCIETE ; NSSLIEU ; NCODIC                  
#  OUTREC 1 ; 35                                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAJ
       ;;
(IVT10MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI989/F89.BIT010AM
# ******  FICHIER DES CODICS POUR LOAD RTIT15                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MAJ.BIT010GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_35 1 CH 35
 /FIELDS FLD_CH_16_7 16 CH 7
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_35
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10MAK
       ;;
(IVT10MAK)
       m_CondExec 00,EQ,IVT10MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI N�2                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAM
       ;;
(IVT10MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVT10MAJ.BIT010GM
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PGI989/F89.UNLOAD.IT15UM
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -t LSEQ -g +1 SORTOUT ${DATA}/PGI989/F89.RELOAD.IT15RM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MAN
       ;;
(IVT10MAN)
       m_CondExec 00,EQ,IVT10MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTIT15                                                     
# ********************************************************************         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAQ
       ;;
(IVT10MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTIT15                                                
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGI989/F89.RELOAD.IT15RM
#                                                                              
#    RSIT15   : NAME=RSIT15M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10MAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVT10M_IVT10MAQ_RTIT15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVT10MAR
       ;;
(IVT10MAR)
       m_CondExec 04,GE,IVT10MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAT
       ;;
(IVT10MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI989/F89.BIT010AM
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MAT.BIT010BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_44_5 44 CH 5
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MAU
       ;;
(IVT10MAU)
       m_CondExec 00,EQ,IVT10MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT011 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MAX PGM=BIT011     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MAX
       ;;
(IVT10MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A6} FIT011 ${DATA}/PTEM/IVT10MAT.BIT010BM
       m_OutputAssign -c 9 -w BIT011 IIT011
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEDG36 ${DATA}/PXX0/F89.BIT010CM
       m_ProgramExec BIT011 
# ********************************************************************         
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBA
       ;;
(IVT10MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI989/F89.BIT010AM
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MBA.BIT010DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "2"
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_16_7 16 CH 7
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MBB
       ;;
(IVT10MBB)
       m_CondExec 00,EQ,IVT10MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT031 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBD PGM=BIT031     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBD
       ;;
(IVT10MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A8} FIT011 ${DATA}/PTEM/IVT10MBA.BIT010DM
       m_OutputAssign -c 9 -w BIT031 IIT031
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEDG36 ${DATA}/PXX0/F89.BIT010EM
       m_ProgramExec BIT031 
# ********************************************************************         
#  BIT015  POUR TOUS LES CODICS A INVENTORIER DONT LA DATE DE                  
#          CONSTITUTUION DU STOCK THEORIQUE EST EGALE A LA DATE DE             
#          TRAITEMENT IL Y A CALCUL DU STOCK ET INSCRIPTION DANS RTIT1         
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBG
       ;;
(IVT10MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLES DES LIEUX                                                     
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  TABLE STOCK SOUS LIEU / MAGASINS                                     
#    RSGS30   : NAME=RSGS30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS60   : NAME=RSGS60M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT015 
       JUMP_LABEL=IVT10MBH
       ;;
(IVT10MBH)
       m_CondExec 04,GE,IVT10MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT035                                                                      
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBJ
       ;;
(IVT10MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE STOCKS                                                         
#    RSGS36   : NAME=RSGS36M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 989                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT035 
       JUMP_LABEL=IVT10MBK
       ;;
(IVT10MBK)
       m_CondExec 04,GE,IVT10MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT100  BUT: PRAPARATION DU FICHIER POUR EDITION DES ETIQUETTES             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBM
       ;;
(IVT10MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSIT00   : NAME=RSIT00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 FIN100 ${DATA}/PTEM/IVT10MBM.BIT100AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT100 
       JUMP_LABEL=IVT10MBN
       ;;
(IVT10MBN)
       m_CondExec 04,GE,IVT10MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBQ
       ;;
(IVT10MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/IVT10MBM.BIT100AM
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MBQ.BIT100BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_16_15 16 CH 15
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MBR
       ;;
(IVT10MBR)
       m_CondExec 00,EQ,IVT10MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBT PGM=BIV105     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBT
       ;;
(IVT10MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES                                               
       m_FileAssign -d SHR -g ${G_A10} FIN105 ${DATA}/PTEM/IVT10MBQ.BIT100BM
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIT040  BUT: EDITION D'UN ETAT D INVENTAIRE COMLPLET                        
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MBX
       ;;
(IVT10MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA67   : NAME=RSGA67M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSEG90   : NAME=RSEG90M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG90 /dev/null
#    RSIT00   : NAME=RSIT00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IIT004 ${DATA}/PTEM/IVT10MBX.BIT040AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT040 
       JUMP_LABEL=IVT10MBY
       ;;
(IVT10MBY)
       m_CondExec 04,GE,IVT10MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIT040AM)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MCA
       ;;
(IVT10MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IVT10MBX.BIT040AM
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MCA.BIT040BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_18_5 18 CH 05
 /FIELDS FLD_BI_12_3 12 CH 03
 /FIELDS FLD_BI_23_5 23 CH 05
 /FIELDS FLD_BI_37_7 37 CH 07
 /FIELDS FLD_BI_28_9 28 CH 09
 /FIELDS FLD_BI_15_3 15 CH 03
 /FIELDS FLD_BI_44_2 44 CH 02
 /FIELDS FLD_BI_7_5 7 CH 05
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING,
   FLD_BI_28_9 ASCENDING,
   FLD_BI_37_7 ASCENDING,
   FLD_BI_44_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MCB
       ;;
(IVT10MCB)
       m_CondExec 00,EQ,IVT10MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIT040CM                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MCD
       ;;
(IVT10MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/IVT10MCA.BIT040BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/IVT10MCD.BIT040CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IVT10MCE
       ;;
(IVT10MCE)
       m_CondExec 04,GE,IVT10MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MCG
       ;;
(IVT10MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/IVT10MCD.BIT040CM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVT10MCG.BIT040DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10MCH
       ;;
(IVT10MCH)
       m_CondExec 00,EQ,IVT10MCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIT004                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10MCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MCJ
       ;;
(IVT10MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/IVT10MCA.BIT040BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/IVT10MCG.BIT040DM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIT004 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IVT10MCK
       ;;
(IVT10MCK)
       m_CondExec 04,GE,IVT10MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVT10MZA
       ;;
(IVT10MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
