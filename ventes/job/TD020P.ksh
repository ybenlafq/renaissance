#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD020P.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD020 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/24 AT 09.07.20 BY BURTECA                      
#    STANDARDS: P  JOBSET: TD020P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DU FICHIERS NON GDG  BTD020AP DE LA VEILLE                          
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TD020PA
       ;;
(TD020PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+2'}
       G_A13=${G_A13:-'+2'}
       G_A14=${G_A14:-'+2'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+2'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+2'}
       G_A19=${G_A19:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+2'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+2'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+2'}
       G_A25=${G_A25:-'+2'}
       G_A26=${G_A26:-'+2'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+2'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+2'}
       G_A31=${G_A31:-'+2'}
       G_A32=${G_A32:-'+2'}
       G_A33=${G_A33:-'+2'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+1'}
       RAAK=${RAAK:-TD020PAG}
       RUN=${RUN}
       JUMP_LABEL=TD020PAA
       ;;
(TD020PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#  FAUSSE D�PENDANCE                                                           
# ********************************************                                 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD020PAB
       ;;
(TD020PAB)
       m_CondExec 16,NE,TD020PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  COPIE DU FICHIER GDG CR�E PAR PROC VENANT DE NT VERS NON GDG                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAD
       ;;
(TD020PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F99.FTD02SP.ZIP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ OUT1 ${DATA}/PXX0.F07.BTD020AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD020PAE
       ;;
(TD020PAE)
       m_CondExec 16,NE,TD020PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PRINT DU FICHIER POUR V�RIFIER S'IL EST VIDE OU PLEIN                       
#  DE FA�CON _A NE PAS PLANTER L'UNZIP SI LE FICHIER EST VIDE.                  
#  SI LE FICHIER EST VIDE, ON BYPASS TTE LA SUITE.                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAG
       ;;
(TD020PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 ${DATA}/PXX0.F07.BTD020AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD020PAH
       ;;
(TD020PAH)
       m_CondExec 16,NE,TD020PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SI RIEN A �T� ENVOY� PAR LA DAFTEL ( FICHIER BTD020 VIDE )                  
#  REMISE A Z�RO DU FICHIER POUR NE PAS RENVOYER LES MVTS DE LA VEILLE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAJ
       ;;
(TD020PAJ)
       m_CondExec ${EXAAP},NE,YES 4,NE,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR OUT1 ${DATA}/PXX0.F07.BTD021QP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD020PAK
       ;;
(TD020PAK)
       m_CondExec 16,NE,TD020PAJ ${EXAAP},NE,YES 4,NE,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEZIPPAGE DU FICHIER ZIPPE GDG                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=PKUNZIP,COND1=(4,EQ,_AAAK)                                 
# //STEPLIB  DD  DSN=SYS2.ZIP390.D21ZTAB,DISP=SHR                              
# //         DD  DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                              
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# TARGET   FILE  NAME=BTD020BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ARCHIVE(":BTD020AP"")                                                      
#  -EXTRACT                                                                    
#  -OUTFILE(TARGET)                                                            
#  -TRAN(ASCII850)                                                             
#  -FTRAN(ASCIIMAJ)                                                            
#          DATAEND                                                             
# *******************************************************************          
#  DEZIPPAGE DU FICHIER ZIPPE                                                  
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP TD020PAM PGM=JVMLDM76   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAM
       ;;
(TD020PAM)
       m_CondExec ${EXAAU},NE,YES 4,EQ,$[RAAK] 
# ***** FICHIER EN SORTIE DE ZIP                                               
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PAM.sysin
       m_ProgramExec JVMLDM76_UNZIP.ksh
# ********************************************************************         
#   N O U V E A U  ***************************************************         
# ********************************************************************         
# ********************************************************************         
#  PGM : BTD019                                                                
#  ------------                                                                
#  CREATION DE LA SYSIN POUR NOUVEAU PGM BTD020                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAQ
       ;;
(TD020PAQ)
       m_CondExec ${EXAAZ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   SORTIE                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FTD019 ${DATA}/PTEM/TD020PAQ.BTD019AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD019 
       JUMP_LABEL=TD020PAR
       ;;
(TD020PAR)
       m_CondExec 04,GE,TD020PAQ ${EXAAZ},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   UNLOAD DE LA TABLE RTTD00 + RTDD01 ET  RTGA01                              
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAT
       ;;
(TD020PAT)
       m_CondExec ${EXABE},NE,YES 4,EQ,$[RAAK] 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
#    RSTD00P  : NAME=RSTD00P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTD00P /dev/null
#    RSTD01P  : NAME=RSTD01P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSTD01P /dev/null
#    RSGA01   : NAME=RSGA01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/TD020PAT.UNL020AP
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/TD020PAQ.BTD019AP
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SORT DU FICHIER UNLOAD PRECEDENT                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=TD020PAX
       ;;
(TD020PAX)
       m_CondExec ${EXABJ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/TD020PAT.UNL020AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 102 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PAX.UNL020BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_56_15 56 CH 15
 /FIELDS FLD_BI_102_1 102 CH 1
 /KEYS
   FLD_BI_56_15 ASCENDING,
   FLD_BI_102_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PAY
       ;;
(TD020PAY)
       m_CondExec 00,EQ,TD020PAX ${EXABJ},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD020                                                                
#  ------------                                                                
#  CREATION D UN FICHIER SEQUENTIEL A PARTIR DES BASES TD00                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBA
       ;;
(TD020PBA)
       m_CondExec ${EXABO},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   TABLE EN LECTURE                                                     
#                                                                              
# *****   ENTREE                                                               
       m_FileAssign -d SHR -g ${G_A3} ITD020 ${DATA}/PTEM/TD020PAX.UNL020BP
#                                                                              
# *****   SORTIE                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 OTD020 ${DATA}/PTEM/TD020PBA.BTD020CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD020 
       JUMP_LABEL=TD020PBB
       ;;
(TD020PBB)
       m_CondExec 04,GE,TD020PBA ${EXABO},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ------------------------------------------------------------------           
#                         1ER  PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02D SUR LA CL� "1"  **** 1ER PASSAGE ****                           
#            TRI DU FIC ISSU DE LA R�CEPTION FTP DU FICHIER DE LA DAF          
#            SI PB REPRENDRE L'EXECUTION DU BTD020.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBD
       ;;
(TD020PBD)
       m_CondExec ${EXABT},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/TD020PAM.BTD020BP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PBD.BTD020DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_35 6 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PBE
       ;;
(TD020PBE)
       m_CondExec 00,EQ,TD020PBD ${EXABT},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "1"  **** 1ER PASSAGE ****                           
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BTD020.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBG
       ;;
(TD020PBG)
       m_CondExec ${EXABY},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/TD020PBA.BTD020CP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g ${G_A6} SORTOUT ${DATA}/PTEM/TD020PBA.BTD020CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "        "
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_35 6 CH 35
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_211_8 211 CH 8
 /CONDITION CND_1 FLD_CH_211_8 EQ CST_1_7 
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PBH
       ;;
(TD020PBH)
       m_CondExec 00,EQ,TD020PBG ${EXABY},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "1"              
#  ------------                                                                
#  ------------    **** 1ER PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBJ PGM=BTD021     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBJ
       ;;
(TD020PBJ)
       m_CondExec ${EXACD},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A7} FTD02DE ${DATA}/PTEM/TD020PBD.BTD020DP
       m_FileAssign -d SHR -g ${G_A8} FTD020E ${DATA}/PTEM/TD020PBA.BTD020CP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PBJ.BTD021AP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PBJ.BTD021BP
# -------- FICHIER CLE1                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PBJ.BTD021CP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PBJ
       m_ProgramExec BTD021 
# ------------------------------------------------------------------           
#                         2EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "2"  **** 2EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBM
       ;;
(TD020PBM)
       m_CondExec ${EXACI},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/TD020PBJ.BTD021AP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g ${G_A10} SORTOUT ${DATA}/PTEM/TD020PBJ.BTD021AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_41_35 41 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_41_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PBN
       ;;
(TD020PBN)
       m_CondExec 00,EQ,TD020PBM ${EXACI},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "2"   **** 2EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBQ
       ;;
(TD020PBQ)
       m_CondExec ${EXACN},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/TD020PBJ.BTD021BP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g ${G_A12} SORTOUT ${DATA}/PTEM/TD020PBJ.BTD021BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_41_35 41 CH 35
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_41_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PBR
       ;;
(TD020PBR)
       m_CondExec 00,EQ,TD020PBQ ${EXACN},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "2"              
#  ------------                                                                
#  ------------   **** 2EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBT PGM=BTD021     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBT
       ;;
(TD020PBT)
       m_CondExec ${EXACS},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A13} FTD02DE ${DATA}/PTEM/TD020PBJ.BTD021AP
       m_FileAssign -d SHR -g ${G_A14} FTD020E ${DATA}/PTEM/TD020PBJ.BTD021BP
#                                                                              
# ***** FICHIERS EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PBT.BTD021DP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PBT.BTD021EP
# -------- FICHIER CLE2                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PBT.BTD021FP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PBT
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         3EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "3"  **** 3EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=TD020PBX
       ;;
(TD020PBX)
       m_CondExec ${EXACX},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/TD020PBT.BTD021DP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g ${G_A16} SORTOUT ${DATA}/PTEM/TD020PBT.BTD021DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_76_35 76 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_76_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PBY
       ;;
(TD020PBY)
       m_CondExec 00,EQ,TD020PBX ${EXACX},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "3"   **** 3EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCA
       ;;
(TD020PCA)
       m_CondExec ${EXADC},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/TD020PBT.BTD021EP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g ${G_A18} SORTOUT ${DATA}/PTEM/TD020PBT.BTD021EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_76_35 76 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_76_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PCB
       ;;
(TD020PCB)
       m_CondExec 00,EQ,TD020PCA ${EXADC},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "3"              
#  ------------                                                                
#  ------------   **** 3EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCD PGM=BTD021     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCD
       ;;
(TD020PCD)
       m_CondExec ${EXADH},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A19} FTD02DE ${DATA}/PTEM/TD020PBT.BTD021DP
       m_FileAssign -d SHR -g ${G_A20} FTD020E ${DATA}/PTEM/TD020PBT.BTD021EP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PCD.BTD021GP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PCD.BTD021HP
# -------- FICHIER CLE3                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PCD.BTD021IP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PCD
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         4EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "4"  **** 4EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCG
       ;;
(TD020PCG)
       m_CondExec ${EXADM},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/TD020PCD.BTD021GP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g ${G_A22} SORTOUT ${DATA}/PTEM/TD020PCD.BTD021GP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_111_35 111 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_111_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PCH
       ;;
(TD020PCH)
       m_CondExec 00,EQ,TD020PCG ${EXADM},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "4"   **** 4EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCJ
       ;;
(TD020PCJ)
       m_CondExec ${EXADR},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/TD020PCD.BTD021HP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g ${G_A24} SORTOUT ${DATA}/PTEM/TD020PCD.BTD021HP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_111_35 111 CH 35
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_111_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PCK
       ;;
(TD020PCK)
       m_CondExec 00,EQ,TD020PCJ ${EXADR},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "4"              
#  ------------                                                                
#  ------------   **** 4EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCM PGM=BTD021     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCM
       ;;
(TD020PCM)
       m_CondExec ${EXADW},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A25} FTD02DE ${DATA}/PTEM/TD020PCD.BTD021GP
       m_FileAssign -d SHR -g ${G_A26} FTD020E ${DATA}/PTEM/TD020PCD.BTD021HP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PCM.BTD021JP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PCM.BTD021KP
# -------- FICHIER CLE4                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PCM.BTD021LP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PCM
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         5EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "5"  **** 5EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCQ
       ;;
(TD020PCQ)
       m_CondExec ${EXAEB},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PTEM/TD020PCM.BTD021JP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g ${G_A28} SORTOUT ${DATA}/PTEM/TD020PCM.BTD021JP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_146_35 146 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_146_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PCR
       ;;
(TD020PCR)
       m_CondExec 00,EQ,TD020PCQ ${EXAEB},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "5"   **** 5EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCT
       ;;
(TD020PCT)
       m_CondExec ${EXAEG},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/TD020PCM.BTD021KP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g ${G_A30} SORTOUT ${DATA}/PTEM/TD020PCM.BTD021KP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_146_35 146 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_197_8 197 CH 8
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_146_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PCU
       ;;
(TD020PCU)
       m_CondExec 00,EQ,TD020PCT ${EXAEG},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "5"              
#  ------------                                                                
#  ------------   **** 5EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PCX PGM=BTD021     ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=TD020PCX
       ;;
(TD020PCX)
       m_CondExec ${EXAEL},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A31} FTD02DE ${DATA}/PTEM/TD020PCM.BTD021JP
       m_FileAssign -d SHR -g ${G_A32} FTD020E ${DATA}/PTEM/TD020PCM.BTD021KP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PCX.BTD021MP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PCX.BTD021NP
# -------- FICHIER CLE5                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PCX.BTD021OP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PCX
       m_ProgramExec BTD021 
#                                                                              
# ********************************************************************         
#  TRI1 FTD02D SUR LA CL� "1"                                                  
#            TRI DU FIC ISSU DE LA R�CEPTION FTP DU FICHIER DE LA DAF          
#            TRAITEMENT DES DOSSIERS ANNULES (AJOUT DU 10/01/05)               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDA
       ;;
(TD020PDA)
       m_CondExec ${EXAEQ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/TD020PBA.BTD020CP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PDA.BTD211AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 "        "
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_6_35 6 CH 35
 /FIELDS FLD_CH_211_8 211 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_204_8 204 CH 8
 /CONDITION CND_1 FLD_CH_211_8 NE CST_1_7 
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PDB
       ;;
(TD020PDB)
       m_CondExec 00,EQ,TD020PDA ${EXAEQ},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "1"              
#  ------------                                                                
#  ------------    **** 1ER PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDD PGM=BTD021     ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDD
       ;;
(TD020PDD)
       m_CondExec ${EXAEV},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A34} FTD02DE ${DATA}/PTEM/TD020PCX.BTD021MP
       m_FileAssign -d SHR -g ${G_A35} FTD020E ${DATA}/PTEM/TD020PDA.BTD211AP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PDD.BTD211BP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PDD.BTD211CP
# -------- FICHIER CLE1                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PDD.BTD212AP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PDD
       m_ProgramExec BTD021 
# ------------------------------------------------------------------           
#                         2EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "2"  **** 2EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDG
       ;;
(TD020PDG)
       m_CondExec ${EXAFA},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/TD020PDD.BTD211BP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PDG.BTD211DP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_35 41 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_41_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PDH
       ;;
(TD020PDH)
       m_CondExec 00,EQ,TD020PDG ${EXAFA},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "2"   **** 2EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDJ
       ;;
(TD020PDJ)
       m_CondExec ${EXAFF},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/TD020PDD.BTD211CP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PDJ.BTD211EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_41_35 41 CH 35
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_197_8 197 CH 8
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_41_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PDK
       ;;
(TD020PDK)
       m_CondExec 00,EQ,TD020PDJ ${EXAFF},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "2"              
#  ------------                                                                
#  ------------   **** 2EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDM PGM=BTD021     ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDM
       ;;
(TD020PDM)
       m_CondExec ${EXAFK},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A38} FTD02DE ${DATA}/PTEM/TD020PDG.BTD211DP
       m_FileAssign -d SHR -g ${G_A39} FTD020E ${DATA}/PTEM/TD020PDJ.BTD211EP
#                                                                              
# ***** FICHIERS EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PDM.BTD211FP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PDM.BTD211GP
# -------- FICHIER CLE2                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PDM.BTD212BP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PDM
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         3EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "3"  **** 3EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDQ
       ;;
(TD020PDQ)
       m_CondExec ${EXAFP},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/TD020PDM.BTD211FP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PDQ.BTD211HP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_76_35 76 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_76_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PDR
       ;;
(TD020PDR)
       m_CondExec 00,EQ,TD020PDQ ${EXAFP},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "3"   **** 3EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDT
       ;;
(TD020PDT)
       m_CondExec ${EXAFU},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/TD020PDM.BTD211GP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PDT.BTD211IP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_76_35 76 CH 35
 /FIELDS FLD_CH_197_8 197 CH 8
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_76_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PDU
       ;;
(TD020PDU)
       m_CondExec 00,EQ,TD020PDT ${EXAFU},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "3"              
#  ------------                                                                
#  ------------   **** 3EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PDX PGM=BTD021     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=TD020PDX
       ;;
(TD020PDX)
       m_CondExec ${EXAFZ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A42} FTD02DE ${DATA}/PTEM/TD020PDQ.BTD211HP
       m_FileAssign -d SHR -g ${G_A43} FTD020E ${DATA}/PTEM/TD020PDT.BTD211IP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PDX.BTD211JP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PDX.BTD211KP
# -------- FICHIER CLE3                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PDX.BTD212CP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PDX
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         4EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "4"  **** 4EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEA
       ;;
(TD020PEA)
       m_CondExec ${EXAGE},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PTEM/TD020PDX.BTD211JP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PEA.BTD211LP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_111_35 111 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_111_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEB
       ;;
(TD020PEB)
       m_CondExec 00,EQ,TD020PEA ${EXAGE},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "4"   **** 4EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PED PGM=SORT       ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=TD020PED
       ;;
(TD020PED)
       m_CondExec ${EXAGJ},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/TD020PDX.BTD211KP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PED.BTD211MP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_111_35 111 CH 35
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_197_8 197 CH 8
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_111_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEE
       ;;
(TD020PEE)
       m_CondExec 00,EQ,TD020PED ${EXAGJ},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "4"              
#  ------------                                                                
#  ------------   **** 4EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEG PGM=BTD021     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEG
       ;;
(TD020PEG)
       m_CondExec ${EXAGO},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A46} FTD02DE ${DATA}/PTEM/TD020PEA.BTD211LP
       m_FileAssign -d SHR -g ${G_A47} FTD020E ${DATA}/PTEM/TD020PED.BTD211MP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PEG.BTD211NP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PEG.BTD211OP
# -------- FICHIER CLE4                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PEG.BTD212DP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PEG
       m_ProgramExec BTD021 
#                                                                              
# ------------------------------------------------------------------           
#                         5EME PASSAGE                                         
# ------------------------------------------------------------------           
# ********************************************************************         
#  TRI1 FTD02DS SUR LA CL� "5"  **** 5EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 PR�CEDENT  DDNAME=FTD02DS              
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEJ PGM=SORT       ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEJ
       ;;
(TD020PEJ)
       m_CondExec ${EXAGT},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A48} SORTIN ${DATA}/PTEM/TD020PEG.BTD211NP
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PEJ.BTD211PP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_146_35 146 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_146_35 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEK
       ;;
(TD020PEK)
       m_CondExec 00,EQ,TD020PEJ ${EXAGT},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI2 FTD020 SUR LA CL� "5"   **** 5EME PASSAGE ****                         
#            TRI DU FIC ISSU PGM BTD021 2 PASSAGE DDNAME=FTD020S               
#            TRI DU FICHIER SUR LUI M�ME POUR GAIN DE PLACE                    
#            SI PB REPRENDRE L'EXECUTION DU BFT021.                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEM
       ;;
(TD020PEM)
       m_CondExec ${EXAGY},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PTEM/TD020PEG.BTD211OP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PEM.BTD211QP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_204_8 204 CH 8
 /FIELDS FLD_CH_197_8 197 CH 8
 /FIELDS FLD_CH_146_35 146 CH 35
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_146_35 ASCENDING,
   FLD_CH_197_8 DESCENDING,
   FLD_CH_204_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEN
       ;;
(TD020PEN)
       m_CondExec 00,EQ,TD020PEM ${EXAGY},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD021    MATCHING DES DEUX FICHIERS ISSUS DE LA CL� "5"              
#  ------------                                                                
#  ------------   **** 5EME PASSAGE ****                                       
#  ------------                                                                
#  LE FPARAM PREND LA VALEUR DE LA CL�.                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEQ PGM=BTD021     ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEQ
       ;;
(TD020PEQ)
       m_CondExec ${EXAHD},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   ENTREE : FICHIERS ISSUS DES TRIS PR�C�DENTS                          
       m_FileAssign -d SHR -g ${G_A50} FTD02DE ${DATA}/PTEM/TD020PEJ.BTD211PP
       m_FileAssign -d SHR -g ${G_A51} FTD020E ${DATA}/PTEM/TD020PEM.BTD211QP
#                                                                              
# *****   FICHIERS EN SORTIE                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD02DS ${DATA}/PTEM/TD020PEQ.BTD211RP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FTD020S ${DATA}/PTEM/TD020PEQ.BTD211SP
# -------- FICHIER CLE5                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FTD021 ${DATA}/PTEM/TD020PEQ.BTD212EP
#                                                                              
# *****   VALEUR DE LA CL� EN PARAMETRE                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/TD020PEQ
       m_ProgramExec BTD021 
#                                                                              
# ********************************************************************         
#  MERGE DE TOUS LES FICHIERS CLE                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=TD020PET
       ;;
(TD020PET)
       m_CondExec ${EXAHI},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
# ******  CLE 1                                                                
       m_FileAssign -d SHR -g ${G_A52} SORTIN ${DATA}/PTEM/TD020PBJ.BTD021CP
# ******  CLE 2                                                                
       m_FileAssign -d SHR -g ${G_A53} -C ${DATA}/PTEM/TD020PBT.BTD021FP
# ******  CLE 3                                                                
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/TD020PCD.BTD021IP
# ******  CLE 4                                                                
       m_FileAssign -d SHR -g ${G_A55} -C ${DATA}/PTEM/TD020PCM.BTD021LP
# ******  CLE 5                                                                
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/TD020PCX.BTD021OP
# ******  DERNIER BTD212AP ANNULE                                              
       m_FileAssign -d SHR -g ${G_A57} -C ${DATA}/PTEM/TD020PDD.BTD212AP
# ******  DERNIER BTD212BP ANNULE                                              
       m_FileAssign -d SHR -g ${G_A58} -C ${DATA}/PTEM/TD020PDM.BTD212BP
# ******  DERNIER BTD212CP ANNULE                                              
       m_FileAssign -d SHR -g ${G_A59} -C ${DATA}/PTEM/TD020PDX.BTD212CP
# ******  DERNIER BTD212DP ANNULE                                              
       m_FileAssign -d SHR -g ${G_A60} -C ${DATA}/PTEM/TD020PEG.BTD212DP
# ******  DERNIER BTD212EP ANNULE                                              
       m_FileAssign -d SHR -g ${G_A61} -C ${DATA}/PTEM/TD020PEQ.BTD212EP
# ******  DERNIER FTD02DS  ANNULE                                              
       m_FileAssign -d SHR -g ${G_A62} -C ${DATA}/PTEM/TD020PEQ.BTD211RP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PET.BTD021PP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEU
       ;;
(TD020PEU)
       m_CondExec 00,EQ,TD020PET ${EXAHI},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  RECUPERATION DE TOUS LES ENREGISTREMENTS DE TYPE "RGLT"                
#       CREATION D'UN FICHIER POUR ENVOI FTP                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PEX PGM=SORT       ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=TD020PEX
       ;;
(TD020PEX)
       m_CondExec ${EXAHN},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER TTES CLES MERG�ES                                            
       m_FileAssign -d SHR -g ${G_A63} SORTIN ${DATA}/PTEM/TD020PET.BTD021PP
# ******  DERNIER FTD02S G�N�R� ( VERS DXGSMP )                                
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ SORTOUT ${DATA}/PXX0.F07.BTD021QP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "RGLT"
 /FIELDS FLD_CH_231_5 231 CH 5
 /FIELDS FLD_CH_181_16 181 CH 16
 /CONDITION CND_1 FLD_CH_231_5 EQ CST_1_4 
 /KEYS
   FLD_CH_181_16 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PEY
       ;;
(TD020PEY)
       m_CondExec 00,EQ,TD020PEX ${EXAHN},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  RECUPERATION DE TOUS LES ENREGISTREMENTS DE TYPE "ARO" ET "ANO         
#       CREATION D'UN FICHIER POUR ENVOI FTP                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFA
       ;;
(TD020PFA)
       m_CondExec ${EXAHS},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER TTES CLES MERG�ES                                            
       m_FileAssign -d SHR -g ${G_A64} SORTIN ${DATA}/PTEM/TD020PET.BTD021PP
# ******  FICHIER ARO ET ANO                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PFA.BTD021RP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "ARO  ")
 /FIELDS FLD_CH_241_8 241 CH 8
 /FIELDS FLD_CH_231_5 231 CH 5
 /FIELDS FLD_CH_181_16 181 CH 16
 /CONDITION CND_1 FLD_CH_231_5 EQ CST_1_5 AND 
 /KEYS
   FLD_CH_181_16 ASCENDING,
   FLD_CH_241_8 DESCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PFB
       ;;
(TD020PFB)
       m_CondExec 00,EQ,TD020PFA ${EXAHS},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD023                                                                
#  ------------                                                                
#  MISE A JOUR DE L'ARO DANS RTTD00                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFD
       ;;
(TD020PFD)
       m_CondExec ${EXAHX},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FCHIER "ARO" ISSU DU TRI                                             
       m_FileAssign -d SHR -g ${G_A65} FTD023 ${DATA}/PTEM/TD020PFA.BTD021RP
# *****   TABLE EN MAJ                                                         
#    RTTD00   : NAME=RSTD00P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTTD00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD023 
       JUMP_LABEL=TD020PFE
       ;;
(TD020PFE)
       m_CondExec 04,GE,TD020PFD ${EXAHX},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  RECUPERATION DE TOUS LES ENREGISTREMENTS DE TYPE "ANO"                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFG
       ;;
(TD020PFG)
       m_CondExec ${EXAIC},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER TTES CLES MERG�ES                                            
       m_FileAssign -d SHR -g ${G_A66} SORTIN ${DATA}/PTEM/TD020PET.BTD021PP
# ******  FICHIER ANO                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/TD020PFG.BTD021SP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "ANO  ")
 /FIELDS FLD_CH_231_5 231 CH 5
 /FIELDS FLD_CH_181_16 181 CH 16
 /CONDITION CND_1 FLD_CH_231_5 EQ CST_1_4 AND 
 /KEYS
   FLD_CH_181_16 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=TD020PFH
       ;;
(TD020PFH)
       m_CondExec 00,EQ,TD020PFG ${EXAIC},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD024                                                                
#  ------------                                                                
#  INSERT DES ANOMALIES DANS RTTD03                                            
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFJ
       ;;
(TD020PFJ)
       m_CondExec ${EXAIH},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FCHIER "ANO" ISSU DU TRI                                             
       m_FileAssign -d SHR -g ${G_A67} FTD024 ${DATA}/PTEM/TD020PFG.BTD021SP
# *****   TABLE EN MAJ                                                         
#    RTTD03   : NAME=RSTD03P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTTD03 /dev/null
# *****   DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD024 
       JUMP_LABEL=TD020PFK
       ;;
(TD020PFK)
       m_CondExec 04,GE,TD020PFJ ${EXAIH},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AJOUT DU STEP D ALIMENTATION DE LA TABLE RTTD05     ***************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFM
       ;;
(TD020PFM)
       m_CondExec ${EXAIM},NE,YES 4,EQ,$[RAAK] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE EN LECTURE                                                     
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#                                                                              
# *****   DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR FTD022E ${DATA}/PXX0.F07.BTD021QP
#                                                                              
# *****   ALIMENTATION DE LA TABLE RTTD05                                      
#    RTTD05   : NAME=RSTD05P,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTTD05 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD022 
       JUMP_LABEL=TD020PFN
       ;;
(TD020PFN)
       m_CondExec 04,GE,TD020PFM ${EXAIM},NE,YES 4,EQ,$[RAAK] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE _A ZERO DU FICHIER NT ( CR�ATION D'UNE GEN�RATION VIDE )              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD020PFQ PGM=IDCAMS     ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=TD020PFQ
       ;;
(TD020PFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.FTD02SP.ZIP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PFQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD020PFR
       ;;
(TD020PFR)
       m_CondExec 16,NE,TD020PFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TD020PZA
       ;;
(TD020PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD020PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
