#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BAC3FP.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBAC3F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/05/27 AT 14.00.14 BY BURTECM                      
#    STANDARDS: P  JOBSET: BAC3FP                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='D.VENTES'                                                          
# ********************************************************************         
#  PGM : BAC003                                                                
#  ------------                                                                
#       EXTRACTION DES FICHIERS INFOCENTRE POUR ACTIVITE COMMERCIALE           
#       SAISIE DU C.A A PARTIR DU CICS MAIL (TRAN AC00 + AC10 + AC20)          
#       (LRECL DE 51)                                                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BAC3FPA
       ;;
(BAC3FPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=BAC3FPAA
       ;;
(BAC3FPAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE DES C.A COMMERCIAUX                  
#    RTAC01   : NAME=RSAC01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTAC01 /dev/null
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ********************************  DATE JJMMSSAA                              
       m_FileAssign -i FDATE
$FDATE
_end
# ********************************  SORTIE FIC SOCIETE PARIS                   
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC907 ${DATA}/PAS0/F07.CA907PAP
# ********************************  SORTIE FIC SOCIETE LYON                    
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC945 ${DATA}/PAS0/F45.CA945YAY
# ********************************  SORTIE FIC SOCIETE LILLE                   
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC961 ${DATA}/PAS0/F61.CA961LAL
# ********************************  SORTIE FIC SOCIETE METZ                    
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC989 ${DATA}/PAS0/F89.CA989MAM
# ********************************  SORTIE FIC SOCIETE MARSEILLE               
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC991 ${DATA}/PAS0/F91.CA991DAD
# ********************************  SORTIE FIC SOCIETE ROUEN                   
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC994 ${DATA}/PAS0/F94.CA994RAR
# ********************************  SORTIE FIC SOCIETE D.OUEST                 
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC916 ${DATA}/PAS0/F16.CA916OAO
# ********************************  SORTIE FIC SOCIETE LUXEMBOURG              
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 FSOC908 ${DATA}/PAS0/F08.CA908XAX
# ********************************                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAC003 
       JUMP_LABEL=BAC3FPAB
       ;;
(BAC3FPAB)
       m_CondExec 04,GE,BAC3FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    MERGE DES FICHIERS POUR CREER UN FICHIER GROUPE                           
# ********************************************************************         
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BAC3FPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BAC3FPAD
       ;;
(BAC3FPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PAS0/F07.CA907PAP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PAS0/F45.CA945YAY
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PAS0/F61.CA961LAL
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PAS0/F89.CA989MAM
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PAS0/F91.CA991DAD
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PAS0/F94.CA994RAR
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PAS0/F16.CA916OAO
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PAS0/F08.CA908XAX
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 SORTOUT ${DATA}/PTEM/BAC3FPAD.CA999GAP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BAC3FPAE
       ;;
(BAC3FPAE)
       m_CondExec 00,EQ,BAC3FPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE POUR DEPACKER LES ZONES                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BAC3FPAG PGM=EZTPA00    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BAC3FPAG
       ;;
(BAC3FPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} FILEA ${DATA}/PTEM/BAC3FPAD.CA999GAP
       m_FileAssign -d NEW,CATLG,DELETE -r 83 -g +1 SORTIE ${DATA}/PAS0/F99.CA999G
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/BAC3FPAG
       m_ProgramExec BAC3FPAG
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BAC3FPZA
       ;;
(BAC3FPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BAC3FPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
