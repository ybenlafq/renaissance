#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FAS02P.ksh                       --- VERSION DU 19/10/2016 16:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFAS02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 14.38.15 BY BURTEC6                      
#    STANDARDS: P  JOBSET: FAS02P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BRB210 : EXTRACTION DES INTER THD DE TYPE STSB4                         
#  REPRISE: NON                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FAS02PA
       ;;
(FAS02PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FAS02PAA
       ;;
(FAS02PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER VENANT DE LA GATEWAY                                         
       m_FileAssign -d SHR FRB210I ${DATA}/PXX0/FTP.F97.RB200AK
# *****   FICHIER TEMPORAIRE DE SORTIE                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 105 -t LSEQ -g +1 FRB210O ${DATA}/PTEM/FAS02PAA.BRB210AP
       m_ProgramExec BRB210 
# *******************************************************************          
#  TRI DU FICHIER ISSU DE COMPLETEL                                            
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FAS02PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PAD
       ;;
(FAS02PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# SORTIN   FILE  NAME=REPRISE,MODE=I                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FAS02PAA.BRB210AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F97.BRB210BK
       m_FileAssign -d NEW,CATLG,DELETE -r 105 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F97.FAS02K0
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "ID_CONTRAT"
 /FIELDS FLD_CH_26_2 26 CH 2
 /FIELDS FLD_CH_36_4 36 CH 4
 /FIELDS FLD_CH_2_10 2 CH 10
 /FIELDS FLD_CH_2_18 2 CH 18
 /FIELDS FLD_CH_29_3 29 CH 3
 /FIELDS FLD_CH_23_2 23 CH 2
 /CONDITION CND_1 FLD_CH_2_10 EQ CST_1_8 
 /KEYS
   FLD_CH_2_18 ASCENDING,
   FLD_CH_29_3 ASCENDING,
   FLD_CH_26_2 ASCENDING,
   FLD_CH_23_2 ASCENDING,
   FLD_CH_36_4 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FAS02PAE
       ;;
(FAS02PAE)
       m_CondExec 00,EQ,FAS02PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM AGRPRB03       RECUP N�IMMO                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FAS02PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PAG
       ;;
(FAS02PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE MOIS                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER RECYCLAGE ERREUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FRECYCL ${DATA}/PXX0/F97.FASRECYK
# *****   FICHIER CREE DANS FAS01K                                             
       m_FileAssign -d SHR -g +0 FPROENT ${DATA}/PXX0/F97.BRBCESBK
# *****   FICHIER FOURNI PAR LES ETUDES PUI TRIE AU STEP PRECEDENT             
       m_FileAssign -d SHR -g ${G_A2} FCOMPLET ${DATA}/PXX0/F97.FAS02K0
# *****   TABLE DB2 EN LECTURE                                                 
#    RTRB50   : NAME=RSRB50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB50 /dev/null
#    RTRB50   : NAME=RSRB50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB50 /dev/null
#    RTRB51   : NAME=RSRB51,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB51 /dev/null
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FPRB04 ${DATA}/PXX0/F97.FAS00K2
# *****   FILE EN SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-404 -t LSEQ -g +1 FPROSOR ${DATA}/PXX0/F97.BRBCESBK
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b AGRPRB03 
       JUMP_LABEL=FAS02PAH
       ;;
(FAS02PAH)
       m_CondExec 04,GE,FAS02PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM AGRPRB14                                                                
# ********************************************************************         
#  REPRISE :OUI SI JOB DE RESTURATION DES FICHIERS A BIEN FONCTIONN�           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FAS02PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PAJ
       ;;
(FAS02PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   TABLE DB2 EN LECTURE                                                 
#    RTRB51   : NAME=RSRB51,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB51 /dev/null
# *****   TABLE DB2 EN MAJ                                                     
#    RTRB51   : NAME=RSRB51,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTRB51 /dev/null
# *****   FICHIER IMMOS FAS                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FIMMOFAS ${DATA}/PXX0/F97.FAS02K1
# *****   FICHIER IMMOS FAS                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 FCRFAS ${DATA}/PXX0/F97.FAS02K2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b AGRPRB14 
       JUMP_LABEL=FAS02PAK
       ;;
(FAS02PAK)
       m_CondExec 04,GE,FAS02PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FAS002AP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FAS02PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PAM
       ;;
(FAS02PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FAS02PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FAS02PAM.FAS002AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAS002AK                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FAS02PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PAQ
       ;;
(FAS02PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FAS02PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FAS02PAM.FAS002AP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FAS02PZA
       ;;
(FAS02PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FAS02PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
