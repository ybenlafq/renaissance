#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015L.ksh                       --- VERSION DU 09/10/2016 00:40
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/08/21 AT 14.45.32 BY BURTECB                      
#    STANDARDS: P  JOBSET: HV015L                                              
# --------------------------------------------------------------------         
# **--USER='LILLE'                                                             
# ********************************************************************         
#  QUIESCE DE TABLESPACES RSHV20L RSHV25L AVANT MAJ                            
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015LA
       ;;
(HV015LA)
#
#HV015LAA
#HV015LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV015LAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       EXAAP=${EXAAP:-0}
       G_A2=${G_A2:-'+1'}
       EXAAU=${EXAAU:-0}
       G_A3=${G_A3:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=HV015LAD
       ;;
(HV015LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F61.BHV000AL
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/HV015LAD.BHV015AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_34_5 34 CH 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015LAE
       ;;
(HV015LAE)
       m_CondExec 00,EQ,HV015LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV015  : GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE                  
#            DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                      
#  REPRISE :OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015LAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015LAG
       ;;
(HV015LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES (GENERALITES ARTICLES)                                      
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  FAMILLE (EDITION DES ETATS)                                          
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
# ******  RAYONS (COMPOSANTS DU RAYON)                                         
#    RSGA21L  : NAME=RSGA21L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21L /dev/null
# ******  QUOTAS DE LIVRAISON (POPULATION MARCHE/DONNEES DE BASE INSEE         
#    RSGQ01L  : NAME=RSGQ01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01L /dev/null
#                                                                              
# ******  FIC DES VENTES DU MOIS TRI�                                          
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PXX0/HV015LAD.BHV015AL
# ******  FIC DES STATS PAR MAGS/GRPE DE STAT/COMMUNE                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PXX0/HV015LAG.BHV015BL
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015LAH
       ;;
(HV015LAH)
       m_CondExec 04,GE,HV015LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SEQUENTIEL BHV015BL                                          
#  CODE RAYON  1,11 ; CODE COMMUNE  32,5                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015LAJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=HV015LAJ
       ;;
(HV015LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/HV015LAG.BHV015BL
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PXX0/HV015LAJ.BHV015CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_32_5 32 CH 05
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_PD_20_5 20 PD 5
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015LAK
       ;;
(HV015LAK)
       m_CondExec 00,EQ,HV015LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV020 : MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOIS             
#           ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                       
#  REPRISE: OUI SI FIN ANORMALE                                                
#          NON SI FIN NORMALE.FAIRE UN RECOVER DE RSHV20L RSHV25L AVEC         
#          LA VALEUR DU RBA DU PREMIER STEP.UTILISEZ LA CHAINE DB2RBL          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015LAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015LAM
       ;;
(HV015LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES STATS PAR MAGS/GROUPE DE STATS/COMMUNE                       
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PXX0/HV015LAJ.BHV015CL
#                                                                              
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                              
#    RSHV20L  : NAME=RSHV20L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV20L /dev/null
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                       
#    RSHV25L  : NAME=RSHV25L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV25L /dev/null
#                                                                              
# ******  DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
#  DEPENDANCE PLAN                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015LAN
       ;;
(HV015LAN)
       m_CondExec 04,GE,HV015LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=HV015LZA
       ;;
(HV015LZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
