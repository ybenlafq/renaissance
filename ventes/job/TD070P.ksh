#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD070P.ksh                       --- VERSION DU 08/10/2016 22:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD070 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/09 AT 10.12.53 BY BURTECC                      
#    STANDARDS: P  JOBSET: TD070P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    DELETE DES FICHIERS NON GDG AVANT RECREATION PAR PGM                      
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=TD070PA
       ;;
(TD070PA)
       EXAAA=${EXAAA:-0}
       EXABT=${EXABT:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2004/03/09 AT 10.12.53 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: TD070P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXTR SOUS-TABLES'                      
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=TD070PAA
       ;;
(TD070PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d OLD,KEEP,DELETE DD1 ${DATA}/PXX0.F07.FTD070AP
       m_FileAssign -d OLD,KEEP,DELETE DD2 ${DATA}/PXX0.F07.FTD070BP
       m_FileAssign -d OLD,KEEP,DELETE DD3 ${DATA}/PXX0.F07.FTD070CP
       m_FileAssign -d OLD,KEEP,DELETE DD4 ${DATA}/PXX0.F07.FTD070DP
       m_FileAssign -d OLD,KEEP,DELETE DD5 ${DATA}/PXX0.F07.FTD070EP
       m_FileAssign -d OLD,KEEP,DELETE DD6 ${DATA}/PXX0.F07.FTD070FP
       m_FileAssign -d OLD,KEEP,DELETE DD7 ${DATA}/PXX0.F07.FTD070GP
       m_FileAssign -d OLD,KEEP,DELETE DD8 ${DATA}/PXX0.F07.FTD070HP
       m_FileAssign -d OLD,KEEP,DELETE DD9 ${DATA}/PXX0.F07.FTD070IP
       m_FileAssign -d OLD,KEEP,DELETE DD10 ${DATA}/PXX0.F07.FTD070JP
       m_FileAssign -d OLD,KEEP,DELETE DD11 ${DATA}/PXX0.F07.FTD070KP
       m_FileAssign -d OLD,KEEP,DELETE DD12 ${DATA}/PXX0.F07.FTD070LP
       m_FileAssign -d OLD,KEEP,DELETE DD13 ${DATA}/PXX0.F07.FTD070MP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD070PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD070PAB
       ;;
(TD070PAB)
       m_CondExec 16,NE,TD070PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD070                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD070PAD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=TD070PAD
       ;;
(TD070PAD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FICHIER EN SORTIE POUR LA DAFTEL                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRCTR ${DATA}/PXX0.F07.FTD070AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRDDS ${DATA}/PXX0.F07.FTD070BP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRDOS ${DATA}/PXX0.F07.FTD070CP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRJUS ${DATA}/PXX0.F07.FTD070DP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRVAL ${DATA}/PXX0.F07.FTD070EP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FCRFUN ${DATA}/PXX0.F07.FTD070FP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDACT ${DATA}/PXX0.F07.FTD070GP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDANO ${DATA}/PXX0.F07.FTD070HP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDARI ${DATA}/PXX0.F07.FTD070IP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDARO ${DATA}/PXX0.F07.FTD070JP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDFAC ${DATA}/PXX0.F07.FTD070KP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDLAC ${DATA}/PXX0.F07.FTD070LP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 FTDLBA ${DATA}/PXX0.F07.FTD070MP
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD070 
       JUMP_LABEL=TD070PAE
       ;;
(TD070PAE)
       m_CondExec 04,GE,TD070PAD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
