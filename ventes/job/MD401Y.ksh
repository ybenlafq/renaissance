#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MD401Y.ksh                       --- VERSION DU 08/10/2016 22:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYMD401 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/01/10 AT 09.30.44 BY BURTECR                      
#    STANDARDS: P  JOBSET: MD401Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REPRO DU FICHIER FPARAM POUR SAUVEGARDE                                     
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MD401YA
       ;;
(MD401YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+2'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+2'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MD401YAA
       ;;
(MD401YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  PRED1    LINK  NAME=$MD400F,MODE=I                                          
# **************************************                                       
# ******* DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 INPUT ${DATA}/PNCGY/F45.BMD401AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUTPUT ${DATA}/PNCGY/F45.BMD401BY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD401YAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD401YAB
       ;;
(MD401YAB)
       m_CondExec 16,NE,MD401YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD401          EXTRACTION DES LIGNES DE LA RTGS40 _A PARTIR DES             
#                  LIEUX PARAMéTRéS DANS LA SOUS TABLE MD400.                  
#                                                                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAD
       ;;
(MD401YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER D'EXTRACTION DU JOUR MVTS RTGS40                             
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 BMD401O ${DATA}/PTEM/MD401YAD.BMD401CY
# ******  FICHIER DATE EN MISE A JOUR POUR PROCHAIN PASSAGE                    
# ******  CE FICHIER N'EST MIS A JOUR QUE SI LE PGM SE TERMINE OK              
# -M-BMD401BY - IS UPDATED IN PLACE WITH MODE=(U,REST=NO)                      
       m_FileAssign -d OLD,KEEP,DELETE -g ${G_A1} FPARAM ${DATA}/PNCGY/F45.BMD401BY
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD401 
       JUMP_LABEL=MD401YAE
       ;;
(MD401YAE)
       m_CondExec 04,GE,MD401YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  INCREMENTATION ET SAUVEGARDE DU FICHIER PERMANENT                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAG
       ;;
(MD401YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FICHIER MENS ENRICHI                                                 
       m_FileAssign -d SHR -g +0 INPUT ${DATA}/PNCGY/F45.BMD401DY
# ******  FICHIER D'EXTRACTION DU JOUR MVTS RTGS40                             
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/MD401YAD.BMD401CY
# ******  FICHIER MENS ENRICHI                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUTPUT ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD401YAG.sysin
       m_UtilityExec
# *************************************************************                
#  CUMUL TOUS MAGASINS CONFONDUS POUR LIEU EXTERNE                             
#  ET CODE OPERATION DONNE                                                     
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAJ
       ;;
(MD401YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YAJ.BMD410AY
# *********************************************************                    
#  BMD410    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD4101.                          
#                                                                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD401YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_4_3 04 CH 03
 /FIELDS FLD_BI_38_10 38 CH 10
 /KEYS
   FLD_BI_27_3 ASCENDING,
   FLD_BI_38_10 ASCENDING,
   FLD_BI_4_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YAM
       ;;
(MD401YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTAN00   : NAME=RSAN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A4} BMD410I ${DATA}/PTEM/MD401YAJ.BMD410AY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD410O ${DATA}/PTEM/MD401YAM.BMD410BY
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD410 
       JUMP_LABEL=MD401YAN
       ;;
(MD401YAN)
       m_CondExec 04,GE,MD401YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAQ
       ;;
(MD401YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/MD401YAM.BMD410BY
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YAQ.BMD410CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YAR
       ;;
(MD401YAR)
       m_CondExec 00,EQ,MD401YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAT
       ;;
(MD401YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/MD401YAQ.BMD410CY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YAT.BMD410DY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YAU
       ;;
(MD401YAU)
       m_CondExec 04,GE,MD401YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=MD401YAX
       ;;
(MD401YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/MD401YAT.BMD410DY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A8} SORTOUT ${DATA}/PTEM/MD401YAM.BMD410BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YAY
       ;;
(MD401YAY)
       m_CondExec 00,EQ,MD401YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD410                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBA
       ;;
(MD401YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/MD401YAQ.BMD410CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/MD401YAM.BMD410BY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD410 FEDITION
#                                                                              
# **************************************************************               
#  CUMUL DES CODES OPERATION POUR UN LIEU EXTERNE                              
#  ET MAGASIN DONNE                                                            
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YBD
       ;;
(MD401YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YBD.BMD411AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_3 04 CH 03
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_38_10 38 CH 10
 /KEYS
   FLD_BI_27_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_38_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YBE
       ;;
(MD401YBE)
       m_CondExec 00,EQ,MD401YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD411          ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                
#                  EN VUE DE LA CREATION DE L'ETAT JMD411.                     
#                                                                              
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBG
       ;;
(MD401YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTAN00   : NAME=RSAN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A12} BMD411I ${DATA}/PTEM/MD401YBD.BMD411AY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD411O ${DATA}/PTEM/MD401YBG.BMD411BY
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD411 
       JUMP_LABEL=MD401YBH
       ;;
(MD401YBH)
       m_CondExec 04,GE,MD401YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBJ
       ;;
(MD401YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/MD401YBG.BMD411BY
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YBJ.BMD411CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YBK
       ;;
(MD401YBK)
       m_CondExec 00,EQ,MD401YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBM
       ;;
(MD401YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/MD401YBJ.BMD411CY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YBM.BMD411DY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YBN
       ;;
(MD401YBN)
       m_CondExec 04,GE,MD401YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBQ
       ;;
(MD401YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/MD401YBM.BMD411DY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g ${G_A16} SORTOUT ${DATA}/PTEM/MD401YBG.BMD411BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YBR
       ;;
(MD401YBR)
       m_CondExec 00,EQ,MD401YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD411                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MD401YBT
       ;;
(MD401YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/MD401YBJ.BMD411CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FCUMULS ${DATA}/PTEM/MD401YBG.BMD411BY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD411 FEDITION
#                                                                              
# *************************************************************                
#  TRI PéPARATOIRE _A LA CRéATION DE L'ETAT DE DéGRADATION                      
#  DES MARGES PAR CODE OPéRATION.  JMD412                                      
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YBX
       ;;
(MD401YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YBX.BMD412AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_38_10 38 CH 10
 /FIELDS FLD_BI_81_7 81 CH 7
 /KEYS
   FLD_BI_38_10 ASCENDING,
   FLD_BI_81_7 ASCENDING,
   FLD_BI_4_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YBY
       ;;
(MD401YBY)
       m_CondExec 00,EQ,MD401YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT INTERMIDIAIRE POUR EPURATION D'ENEREGISTREMENTS                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=MD401YCA
       ;;
(MD401YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/MD401YBX.BMD412AY
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/MD401YCA.BMD412EY
# *********************************************************                    
#  BMD412    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD412 .                          
#                                                                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD401YCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_81_7 81 CH 7
 /FIELDS FLD_CH_81_7 81 CH 7
 /KEYS
   FLD_BI_81_7 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_81_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MD401YCD
       ;;
(MD401YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A21} BMD412I ${DATA}/PTEM/MD401YBX.BMD412AY
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A22} BMD412T ${DATA}/PTEM/MD401YCA.BMD412EY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD412O ${DATA}/PTEM/MD401YCD.BMD412BY
# ******                                                                       
# *************************************************************                
#  TRI PREPARATOIRE AU PASSAGE DANS LE GENERATEUR D ETAT                       
#  POUR JMD412                                                                 
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          

       m_ProgramExec -b BMD412 
       JUMP_LABEL=MD401YCG
       ;;
(MD401YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PTEM/MD401YCD.BMD412BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YCG.BMD401EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_96_3 96 CH 3
 /FIELDS FLD_BI_7_5 7 CH 5
 /FIELDS FLD_BI_17_5 17 CH 5
 /FIELDS FLD_BI_89_7 89 CH 7
 /FIELDS FLD_BI_24_5 24 CH 5
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_17_5 ASCENDING,
   FLD_BI_24_5 ASCENDING,
   FLD_BI_89_7 ASCENDING,
   FLD_BI_96_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YCH
       ;;
(MD401YCH)
       m_CondExec 00,EQ,MD401YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=MD401YCJ
       ;;
(MD401YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A24} FEXTRAC ${DATA}/PTEM/MD401YCG.BMD401EY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YCJ.BMD412CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YCK
       ;;
(MD401YCK)
       m_CondExec 04,GE,MD401YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=MD401YCM
       ;;
(MD401YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/MD401YCJ.BMD412CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YCM.BMD412DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YCN
       ;;
(MD401YCN)
       m_CondExec 00,EQ,MD401YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD412                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=MD401YCQ
       ;;
(MD401YCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A26} FEXTRAC ${DATA}/PTEM/MD401YCG.BMD401EY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A27} FCUMULS ${DATA}/PTEM/MD401YCM.BMD412DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD412 FEDITION
#                                                                              
# *************************************************************                
#  TRI PéPARATOIRE _A LA CRéATION DE L'ETAT DE DéGRADATION                      
#  VENTILATION PAR FAMILLES / OPERATIONS / MAGASINS JMD413                     
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YCT
       ;;
(MD401YCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YCT.BMD413AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_81_7 81 CH 7
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_38_10 38 CH 10
 /KEYS
   FLD_BI_4_3 ASCENDING,
   FLD_BI_38_10 ASCENDING,
   FLD_BI_81_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YCU
       ;;
(MD401YCU)
       m_CondExec 00,EQ,MD401YCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT INTERMIDIAIRE POUR EPURATION D'ENEREGISTREMENTS                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=MD401YCX
       ;;
(MD401YCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/MD401YCT.BMD413AY
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/MD401YCX.BMD413EY
# *********************************************************                    
#  BMD413    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD413 .                          
#                                                                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD401YDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_7 81 CH 7
 /FIELDS FLD_BI_81_7 81 CH 7
 /KEYS
   FLD_BI_81_7 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_81_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MD401YDA
       ;;
(MD401YDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A30} BMD413I ${DATA}/PTEM/MD401YCT.BMD413AY
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A31} BMD413T ${DATA}/PTEM/MD401YCX.BMD413EY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD413O ${DATA}/PTEM/MD401YDA.BMD413BY
# ******                                                                       
# *************************************************************                
#  TRI PREPARATOIRE AU PASSAGE DANS LE GENERATEUR D ETAT                       
#  POUR JMD413                                                                 
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          

       m_ProgramExec -b BMD413 
       JUMP_LABEL=MD401YDD
       ;;
(MD401YDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/MD401YDA.BMD413BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YDD.BMD401FY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_5 27 CH 5
 /FIELDS FLD_BI_112_7 112 CH 7
 /FIELDS FLD_BI_20_5 20 CH 5
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_5 10 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING,
   FLD_BI_20_5 ASCENDING,
   FLD_BI_27_5 ASCENDING,
   FLD_BI_112_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YDE
       ;;
(MD401YDE)
       m_CondExec 00,EQ,MD401YDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=MD401YDG
       ;;
(MD401YDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A33} FEXTRAC ${DATA}/PTEM/MD401YDD.BMD401FY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YDG.BMD413CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YDH
       ;;
(MD401YDH)
       m_CondExec 04,GE,MD401YDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=MD401YDJ
       ;;
(MD401YDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/MD401YDG.BMD413CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YDJ.BMD413DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YDK
       ;;
(MD401YDK)
       m_CondExec 00,EQ,MD401YDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD413                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=MD401YDM
       ;;
(MD401YDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A35} FEXTRAC ${DATA}/PTEM/MD401YDD.BMD401FY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A36} FCUMULS ${DATA}/PTEM/MD401YDJ.BMD413DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD413 FEDITION
#                                                                              
# *************************************************************                
#  TRI PéPARATOIRE _A LA CRéATION DE L'ETAT D'ECART DE MUT DACEM                
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YDQ
       ;;
(MD401YDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YDQ.BMD414AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_4_3 04 CH 3
 /KEYS
   FLD_BI_4_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YDR
       ;;
(MD401YDR)
       m_CondExec 00,EQ,MD401YDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT INTERMIDIAIRE POUR EPURATION D'ENEREGISTREMENTS                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=MD401YDT
       ;;
(MD401YDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A38} SORTIN ${DATA}/PTEM/MD401YDQ.BMD414AY
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/MD401YDT.BMD414EY
# *********************************************************                    
#  BMD414    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD414 .                          
#                                                                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD401YDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_7 81 CH 7
 /FIELDS FLD_BI_81_7 81 CH 7
 /KEYS
   FLD_BI_81_7 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_81_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MD401YDX
       ;;
(MD401YDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A39} BMD414I ${DATA}/PTEM/MD401YDQ.BMD414AY
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A40} BMD414T ${DATA}/PTEM/MD401YDT.BMD414EY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD414O ${DATA}/PTEM/MD401YDX.BMD414BY
# ******                                                                       
# *************************************************************                
#  TRI PREPARATOIRE AU PASSAGE DANS LE GENERATEUR D ETAT                       
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          

       m_ProgramExec -b BMD414 
       JUMP_LABEL=MD401YEA
       ;;
(MD401YEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/MD401YDX.BMD414BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YEA.BMD401GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_10_5 10 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YEB
       ;;
(MD401YEB)
       m_CondExec 00,EQ,MD401YEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=MD401YED
       ;;
(MD401YED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A42} FEXTRAC ${DATA}/PTEM/MD401YEA.BMD401GY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YED.BMD414CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YEE
       ;;
(MD401YEE)
       m_CondExec 04,GE,MD401YED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YEG PGM=SORT       ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=MD401YEG
       ;;
(MD401YEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A43} SORTIN ${DATA}/PTEM/MD401YED.BMD414CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YEG.BMD414DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YEH
       ;;
(MD401YEH)
       m_CondExec 00,EQ,MD401YEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD414                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=MD401YEJ
       ;;
(MD401YEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A44} FEXTRAC ${DATA}/PTEM/MD401YEA.BMD401GY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A45} FCUMULS ${DATA}/PTEM/MD401YEG.BMD414DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD414 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YEK
       ;;
(MD401YEK)
       m_CondExec 04,GE,MD401YEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT INTERMIDIAIRE POUR EPURATION D'ENEREGISTREMENTS                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=MD401YEM
       ;;
(MD401YEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A46} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/MD401YEM.BMD415EY
# *********************************************************                    
#  BMD415    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD415 .                          
#                                                                              
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD401YEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_7 81 CH 7
 /FIELDS FLD_BI_81_7 81 CH 7
 /KEYS
   FLD_BI_81_7 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_81_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MD401YEQ
       ;;
(MD401YEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA56   : NAME=RSGA56Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTGG50   : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A47} BMD415I ${DATA}/PNCGY/F45.BMD401DY
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A48} BMD415T ${DATA}/PTEM/MD401YEM.BMD415EY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 BMD415O ${DATA}/PTEM/MD401YEQ.BMD415BY
# ******                                                                       
# *************************************************************                
#  TRI PREPARATOIRE AU PASSAGE DANS LE GENERATEUR D ETAT                       
#  REPRISE OUI                                                                 
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD401YET PGM=SORT       ** ID=AHI                                   
# ***********************************                                          

       m_ProgramExec -b BMD415 
       JUMP_LABEL=MD401YET
       ;;
(MD401YET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A49} SORTIN ${DATA}/PTEM/MD401YEQ.BMD415BY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YET.BMD401HY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_75_3 75 CH 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_BI_94_8 94 CH 8
 /FIELDS FLD_BI_49_7 49 CH 7
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_94_8 ASCENDING,
   FLD_BI_49_7 ASCENDING,
   FLD_BI_75_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YEU
       ;;
(MD401YEU)
       m_CondExec 00,EQ,MD401YET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=MD401YEX
       ;;
(MD401YEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A50} FEXTRAC ${DATA}/PTEM/MD401YET.BMD401HY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/MD401YEX.BMD415CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD401YEY
       ;;
(MD401YEY)
       m_CondExec 04,GE,MD401YEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFA PGM=SORT       ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFA
       ;;
(MD401YFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PTEM/MD401YEX.BMD415CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/MD401YFA.BMD415DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YFB
       ;;
(MD401YFB)
       m_CondExec 00,EQ,MD401YFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD415                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFD
       ;;
(MD401YFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A52} FEXTRAC ${DATA}/PTEM/MD401YET.BMD401HY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A53} FCUMULS ${DATA}/PTEM/MD401YFA.BMD415DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD415 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD401YFE
       ;;
(MD401YFE)
       m_CondExec 04,GE,MD401YFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PRéPARATOIRE _A L'éLABORATION DE L'ETAT JMD418                           
#  (DEGRADATION DES MARGES PAR FAMILLE ET CODES OPéRATIONS)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFG PGM=SORT       ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFG
       ;;
(MD401YFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A54} SORTIN ${DATA}/PNCGY/F45.BMD401DY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/MD401YFG.BMD417AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_6 1 CH 6
 /FIELDS FLD_BI_68_5 68 CH 5
 /FIELDS FLD_BI_38_10 38 CH 10
 /KEYS
   FLD_BI_1_6 ASCENDING,
   FLD_BI_68_5 ASCENDING,
   FLD_BI_38_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YFH
       ;;
(MD401YFH)
       m_CondExec 00,EQ,MD401YFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT INTERMIDIAIRE POUR EPURATION D'ENREGISTREMENTS                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFJ
       ;;
(MD401YFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A55} SORTIN ${DATA}/PTEM/MD401YFG.BMD417AY
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -g +1 SORTOUT ${DATA}/PTEM/MD401YFJ.BMD417BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_7 81 CH 7
 /FIELDS FLD_BI_81_7 81 CH 7
 /KEYS
   FLD_BI_81_7 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_81_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=MD401YFK
       ;;
(MD401YFK)
       m_CondExec 00,EQ,MD401YFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD417                                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFM
       ;;
(MD401YFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#                                                                              
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A56} BMD417I ${DATA}/PTEM/MD401YFG.BMD417AY
       m_FileAssign -d SHR -g ${G_A57} BMD417T ${DATA}/PTEM/MD401YFJ.BMD417BY
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 BMD417O ${DATA}/PTEM/MD401YFM.BMD417CY
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD417 
       JUMP_LABEL=MD401YFN
       ;;
(MD401YFN)
       m_CondExec 04,GE,MD401YFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BMD417O                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFQ PGM=SORT       ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFQ
       ;;
(MD401YFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A58} SORTIN ${DATA}/PTEM/MD401YFM.BMD417CY
       m_FileAssign -d NEW,CATLG,DELETE -r 167 -g +1 SORTOUT ${DATA}/PTEM/MD401YFQ.BMD417DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_74_7 74 PD 7
 /FIELDS FLD_PD_98_4 98 PD 4
 /FIELDS FLD_PD_81_7 81 PD 7
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_67_7 67 PD 7
 /FIELDS FLD_BI_1_11 1 CH 11
 /FIELDS FLD_BI_37_10 37 CH 10
 /KEYS
   FLD_BI_1_11 ASCENDING,
   FLD_BI_37_10 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_67_7,
    TOTAL FLD_PD_74_7,
    TOTAL FLD_PD_81_7,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_4
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD401YFR
       ;;
(MD401YFR)
       m_CondExec 00,EQ,MD401YFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD418  EDITION DE L'ETAT JMD418                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFT PGM=BMD418     ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFT
       ;;
(MD401YFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A59} BMD418I ${DATA}/PTEM/MD401YFQ.BMD417DY
# ****** ETAT _A EDITER                                                         
       m_OutputAssign -c 9 -w JMD418 BMD418O
       m_ProgramExec BMD418 
#                                                                              
# ********************************************************************         
#  RECOPIE DE LA DATE POUR PROCHAIN PASSAGE                                    
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD401YFX PGM=IDCAMS     ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=MD401YFX
       ;;
(MD401YFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  RECOPIE DATE POUR PROCHAIN PASSAGE                                   
       m_FileAssign -d SHR -g ${G_A60} INPUT ${DATA}/PNCGY/F45.BMD401BY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUTPUT ${DATA}/PNCGY/F45.BMD401AY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD401YFX.sysin
       m_UtilityExec
# ******************************************************                       
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MD401YZA
       ;;
(MD401YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD401YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD401YFY
       ;;
(MD401YFY)
       m_CondExec 16,NE,MD401YFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
