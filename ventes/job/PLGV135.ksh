#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PLGV135.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV135 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/30 AT 11.23.35 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV135L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BGV135 : 1. POUR VENTE 1 => EXTRACTION DES VENTES TLM DU JOUR              
#            2. MAJ DE LA DSTAT = FDATE DANS RTGV10 RTGV11                     
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV135LA
       ;;
(GV135LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV135LAA
       ;;
(GV135LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* ARTICLES                                                             
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* GENERALISEE                                                          
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* LIEUX                                                                
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* MARQUES                                                              
#    RSGA22   : NAME=RSGA22L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* ARTICLES COMMISSIONS PAR ZONE DE PRIX                                
#    RSGA62   : NAME=RSGA62L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA62 /dev/null
# ******* BBTE                                                                 
#    RSGA75   : NAME=RSGA75L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PRMP DACEM                                                           
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
#                                                                              
# ******* VENTES (ENTETES DE VENTES)                                           
#    RSGV10   : NAME=RSGV10L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
# ******* VENTES (LIGNES DE VENTES )                                           
#    RSGV11   : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******* TABLE MAGASINS DU GROUPE  LGT                                        
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******  TABLE FAMILLES TRAITES  LGT                                          
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EXTRACTION DES VENTES                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 FGV135 ${DATA}/PGV0/F61.BGV135AL
# ******  DETAILS DES VENTES EXTRAITES                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 216 -g +1 FRTGV11 ${DATA}/PGV0/F61.BGV135BL
# ******  VENTES PAR GROUPE DE PRODUITS DE 80 A 93                             
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV105 ${DATA}/PGV0/F61.BGV135CL
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV135 
       JUMP_LABEL=GV135LAB
       ;;
(GV135LAB)
       m_CondExec 04,GE,GV135LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
