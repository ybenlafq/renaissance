#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV519D.ksh                       --- VERSION DU 09/10/2016 00:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGV519 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 99/07/13 AT 16.57.14 BY BURTEC6                      
#    STANDARDS: P  JOBSET: GV519D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#    REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE           
#    -=-=-=-=                                                                  
# ********************************************************************         
#  QUIESCE DE RSGV27D                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV519DA
       ;;
(GV519DA)
#
#GV519DAA
#GV519DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV519DAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=GV519DAD
       ;;
(GV519DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER DATE :JJMMSSAA + 1                                           
       m_FileAssign -i FDATE1
$VDATEJ1_JJMMANN
_end
# ******  PARAMETRE C SOC,NBRES D'AUTORISATIONS,DATEJ1 SSAAMMJJ                
# ******  SOIT        991300SSAAMMJJ                                           
       m_FileAssign -i FCARTE
$DPMDEP_1_3900$VDATEJ1_ANNMMJJ
_end
# ******  TABLE DES AUTORISATIONS                                              
#    RSGV27D  : NAME=RSGV27D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27D /dev/null
# ******  LISTE DES AUTORISATIONS ENTREPOT                                     
       m_OutputAssign -c 9 -w IGV519 IGV519
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV519 
       JUMP_LABEL=GV519DAE
       ;;
(GV519DAE)
       m_CondExec 04,GE,GV519DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
