#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV200L.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLPV200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/08/22 AT 10.12.39 BY BURTECB                      
#    STANDARDS: P  JOBSET: PV200L                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  PGM : BPV050                                                                
#  PROG D EXTRACTION AFIN DE FORMATTER UN FICHIER SAM QUI SERA RE-TRIE         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV200LA
       ;;
(PV200LA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       EXAAK=${EXAAK:-0}
       G_A2=${G_A2:-'+1'}
       EXAAP=${EXAAP:-0}
       G_A3=${G_A3:-'+1'}
       EXAAU=${EXAAU:-0}
       G_A4=${G_A4:-'+1'}
       EXAAZ=${EXAAZ:-0}
       G_A5=${G_A5:-'+1'}
       EXABE=${EXABE:-0}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       EXABJ=${EXABJ:-0}
       G_A8=${G_A8:-'+1'}
       EXABO=${EXABO:-0}
       G_A9=${G_A9:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=PV200LAA
       ;;
(PV200LAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******************************** TABLE DES LIEUX                             
#    RTGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ********************************  TABLE DES VENDEURS                         
#    RTGV31   : NAME=RSGV31L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
# ******************************* TABLE HISTO DES VENTES                       
#    RTHV32   : NAME=RSHV32L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTHV32 /dev/null
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ********************************  SORTIE D UN SAM AVANT TRI (LRECL 1         
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV050 ${DATA}/PXX0/PV200LAA.BPV050AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV050 
       JUMP_LABEL=PV200LAB
       ;;
(PV200LAB)
       m_CondExec 04,GE,PV200LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050                                               
#  C EST LE FICHIER EN SORTIE QUI SERVIRA POUR LES PGM D EDITIONS              
#  REPRISE: OUI.                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAD
       ;;
(PV200LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/PV200LAA.BPV050AL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200LAD.BPV050BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_6 87 CH 6
 /FIELDS FLD_CH_107_6 107 CH 6
 /FIELDS FLD_CH_103_4 103 CH 4
 /FIELDS FLD_CH_99_4 99 CH 4
 /FIELDS FLD_CH_53_20 53 CH 20
 /FIELDS FLD_CH_77_4 77 CH 4
 /FIELDS FLD_CH_1_9 1 CH 09
 /FIELDS FLD_CH_73_4 73 CH 4
 /FIELDS FLD_CH_93_6 93 CH 6
 /FIELDS FLD_CH_81_6 81 CH 6
 /KEYS
   FLD_CH_1_9 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_73_4,
    TOTAL FLD_CH_77_4,
    TOTAL FLD_CH_81_6,
    TOTAL FLD_CH_87_6,
    TOTAL FLD_CH_93_6,
    TOTAL FLD_CH_99_4,
    TOTAL FLD_CH_103_4,
    TOTAL FLD_CH_107_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200LAE
       ;;
(PV200LAE)
       m_CondExec 00,EQ,PV200LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU TRI PRECEDENT POUR ETAT IPV050                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAG
       ;;
(PV200LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/PV200LAD.BPV050BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200LAG.BPV050CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_53_20 53 CH 20
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200LAH
       ;;
(PV200LAH)
       m_CondExec 00,EQ,PV200LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV051  : EDITION IPV050 .VENTES PAR RUBRIQUE PRODUITS                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAJ PGM=BPV051     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAJ
       ;;
(PV200LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A3} FPV050 ${DATA}/PXX0/PV200LAG.BPV050CL
# ******  EDITION DE L ETAT IPV050                                             
       m_OutputAssign -c 9 -w IPV050 IPV050
       m_ProgramExec BPV051 
# ********************************************************************         
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV051 IL SE NOMMERA FPV050                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAM PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAM
       ;;
(PV200LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/PV200LAD.BPV050BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200LAM.BPV051AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 0
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_PD_73_4 EQ CST_1_6 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING,
   FLD_PD_81_6 DESCENDING,
   FLD_PD_73_4 DESCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200LAN
       ;;
(PV200LAN)
       m_CondExec 00,EQ,PV200LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV052 IL SE NOMMERA FCUMUL                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAQ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAQ
       ;;
(PV200LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PV200LAD.BPV050BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200LAQ.BPV051BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 0
 /FIELDS FLD_PD_103_4 103 PD 4
 /FIELDS FLD_PD_107_6 107 PD 6
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_99_4 99 PD 4
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_PD_93_6 93 PD 6
 /CONDITION CND_2 FLD_PD_73_4 EQ CST_1_10 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_4,
    TOTAL FLD_PD_81_6,
    TOTAL FLD_PD_93_6,
    TOTAL FLD_PD_99_4,
    TOTAL FLD_PD_103_4,
    TOTAL FLD_PD_107_6
 /OMIT CND_2
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200LAR
       ;;
(PV200LAR)
       m_CondExec 00,EQ,PV200LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV052                                                                
#  PROG DE FORMATTAGE DE L ETAT IPV051                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAT
       ;;
(PV200LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ********************************  DATE JJMMSSAA                              
       m_FileAssign -i FDATE
$FDATE
_end
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ********************************  ENTREE BPV051AP                            
       m_FileAssign -d SHR -g ${G_A6} FPV050 ${DATA}/PXX0/PV200LAM.BPV051AL
# ********************************  ENTREE BPV051BP                            
       m_FileAssign -d SHR -g ${G_A7} FCUMUL ${DATA}/PXX0/PV200LAQ.BPV051BL
# ******  EDITION DE L ETAT IPV051                                             
       m_OutputAssign -c 9 -w IPV051 IPV051
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV052 
       JUMP_LABEL=PV200LAU
       ;;
(PV200LAU)
       m_CondExec 04,GE,PV200LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET DU TRI POUR ETAT IPV052                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LAX PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LAX
       ;;
(PV200LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/PV200LAD.BPV050BL
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200LAX.BPV053AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_6 4 CH 06
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_73_4 73 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_6 ASCENDING,
   FLD_PD_81_6 DESCENDING,
   FLD_PD_73_4 DESCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200LAY
       ;;
(PV200LAY)
       m_CondExec 00,EQ,PV200LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV053  : EDITION IPV052 .STAT DE VENTES PAR VENDEUR                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200LBA PGM=BPV053     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LBA
       ;;
(PV200LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A9} FPV050 ${DATA}/PXX0/PV200LAX.BPV053AL
# ******  EDITION DE L ETAT IPV052                                             
       m_OutputAssign -c 9 -w IPV052 IPV052
       m_ProgramExec BPV053 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200LZA
       ;;
(PV200LZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV200LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
