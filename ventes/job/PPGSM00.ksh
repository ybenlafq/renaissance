#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPGSM00.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGSM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/30 AT 11.17.55 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#       TRI DU FICHIER D'EXTRACTION DES STOCKS MAGASINS                        
#       ***********************************************                        
#  (VERSION/SOCIETE/LIEU/NO SEQUENCE/LIBELLE MARKETING 1 2 3/MARQUE/           
#   MONTANT POUR TRI/CODIC1/CODIC2)                                            
#  SORT FIELDS=(1,10,CH,A,11,3,CH,A,14,3,CH,A,82,5,CH,A,17,20,CH,A,            
#             37,20,CH,A,57,20,CH,A,87,5,CH,A,202,5,CH,A,207,5,CH,A,           
#             92,5,BI,D,97,7,CH,A,                                             
#             104,7,CH,A)                                                      
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM00PA
       ;;
(GSM00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       GSM00PA=${LUNDI}
       GSM00PB=${MARDI}
       GSM00PC=${MERCRED}
       GSM00PD=${JEUDI}
       GSM00PE=${VENDRED}
       GSM00PF=${SAMEDI}
       GSM00PG=${DIMANCH}
       JUMP_LABEL=GSM00PAA
       ;;
(GSM00PAA)
       m_CondExec ${EXAAA},NE,YES 1,EQ,$[GSM00PF] 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "TM4"
 /DERIVEDFIELD CST_6_25 FORMA
 /DERIVEDFIELD CST_5_24 "TM2")
 /DERIVEDFIELD CST_3_20 "EM1"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_207_5 207 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 CST_6_25 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAB
       ;;
(GSM00PAB)
       m_CondExec 00,EQ,GSM00PAA ${EXAAA},NE,YES 1,EQ,$[GSM00PF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU LUNDI                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAD
       ;;
(GSM00PAD)
       m_CondExec ${EXAAF},NE,YES 1,EQ,$[GSM00PG] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A1} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_37_20 37 CH 20
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAE
       ;;
(GSM00PAE)
       m_CondExec 00,EQ,GSM00PAD ${EXAAF},NE,YES 1,EQ,$[GSM00PG] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MARDI                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAG
       ;;
(GSM00PAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[GSM00PA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A2} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_87_5 87 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAH
       ;;
(GSM00PAH)
       m_CondExec 00,EQ,GSM00PAG ${EXAAK},NE,YES 1,EQ,$[GSM00PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MERCREDI                    
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAJ
       ;;
(GSM00PAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[GSM00PB] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A3} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAK
       ;;
(GSM00PAK)
       m_CondExec 00,EQ,GSM00PAJ ${EXAAP},NE,YES 1,EQ,$[GSM00PB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU JEUDI                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAM
       ;;
(GSM00PAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[GSM00PC] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A4} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_11_3 11 CH 3
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAN
       ;;
(GSM00PAN)
       m_CondExec 00,EQ,GSM00PAM ${EXAAU},NE,YES 1,EQ,$[GSM00PC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU VENDREDI                    
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAQ
       ;;
(GSM00PAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[GSM00PD] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A5} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_57_20 57 CH 20
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAR
       ;;
(GSM00PAR)
       m_CondExec 00,EQ,GSM00PAQ ${EXAAZ},NE,YES 1,EQ,$[GSM00PD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU SAMEDI                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM00PAT
       ;;
(GSM00PAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[GSM00PE] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGM075DP
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A6} SORTOUT ${DATA}/PXX0/F07.BSM025BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00PAU
       ;;
(GSM00PAU)
       m_CondExec 00,EQ,GSM00PAT ${EXABE},NE,YES 1,EQ,$[GSM00PE] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
