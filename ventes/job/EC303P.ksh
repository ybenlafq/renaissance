#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC303P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC303 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/09/02 AT 10.10.46 BY BURTECA                      
#    STANDARDS: P  JOBSET: EC303P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM = BEC303  -  EXTRACTION QUOTIDIENNE DES LIGNES DE CR�DITS               
#                -     OU � R�CEPTION FACTURE ANNUL�ES                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC303PA
       ;;
(EC303PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+3'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+3'}
       G_A14=${G_A14:-'+4'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+4'}
       G_A18=${G_A18:-'+5'}
       G_A19=${G_A19:-'+5'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=EC303PAA
       ;;
(EC303PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  PRED     LINK  NAME=$EX010P,MODE=I                                          
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN ECRITURE                                                   
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV10   : NAME=RSGV10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EXTRAIT                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEC303M ${DATA}/PXX0/F07.FEC303AP
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEC303L ${DATA}/PXX0/F07.FEC303BP
# ******* FICHIER EXTRAIT                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEC303R ${DATA}/PXX0/F07.FEC303CP
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEC303T ${DATA}/PXX0/F07.FEC303DP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC303 
       JUMP_LABEL=EC303PAB
       ;;
(EC303PAB)
       m_CondExec 04,GE,EC303PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER VERS                                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=FEC303AP,MODE=I                                          
# DD2      FILE  NAME=FEC303BP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=EC303ZIP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -INFILE(DD2)                                                                
#  -ZIPPED_DSN(PXX0.F07.FEC303AP.*,FEC303M.TXT)                                
#  -ZIPPED_DSN(PXX0.F07.FEC303BP.*,FEC303L.TXT)                                
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAD
       ;;
(EC303PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PAD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/EC303PAD.FTEC303P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP 040                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAG PGM=JVMLDM76   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAG
       ;;
(EC303PAG)
       m_CondExec ${EXAAK},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F07.FEC303AP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.EC303ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PAG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
#  -ZIPPED_DSN(PXX0.F07.FEC303CP.*,+                                           
#  MOUVEMENT_ASSUR_DCOM_&YR4.&LMON.&LDAY..CSV)                                 
#  -ZIPPED_DSN(PXX0.F07.FEC303DP.*,+                                           
#  PRODUITS_NON_MUTES_DCOM_&YR4.&LMON.&LDAY..CSV)                              
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FTEC303P,MODE=O                                          
# ********************************************************************         
#  ZIP des FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=FEC303CP,MODE=I                                          
# DD2      FILE  NAME=FEC303DP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=FC303ZIP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -INFILE(DD2)                                                                
#          DATAEND                                                             
#          FILE  NAME=FTEC303P,MODE=I                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTEC303P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAJ
       ;;
(EC303PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PAJ.sysin
       m_UtilityExec INPUT
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTEC303P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=FTP,PARM='(EXIT'                                          
# *                                                                            
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# BIN                                                                          
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
# PUT 'PXX0.F07.FC303ZIP(0)' Mouvement_produit_stock_DCOM.zip                  
# QUIT                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  ECLATEMENT DES FICHIERS FEC303CP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAM
       ;;
(EC303PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.FEC303CP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F07.FEC303EP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F07.FEC303FP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F07.FEC303GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "240"
 /DERIVEDFIELD CST_1_6 "099"
 /DERIVEDFIELD CST_3_13 "099"
 /DERIVEDFIELD CST_1_9 "240"
 /FIELDS FLD_CH_61_3 61 CH 3
 /CONDITION CND_2 FLD_CH_61_3 EQ CST_1_6 
 /CONDITION CND_1 FLD_CH_61_3 EQ CST_1_3 
 /CONDITION CND_3 FLD_CH_61_3 EQ CST_1_9 OR FLD_CH_61_3 EQ CST_3_13 
 /COPY
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /OMIT CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=EC303PAN
       ;;
(EC303PAN)
       m_CondExec 00,EQ,EC303PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ECLATEMENT DES FICHIERS FEC303DP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAQ
       ;;
(EC303PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F07.FEC303DP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF1 ${DATA}/PXX0/F07.FEC303HP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF2 ${DATA}/PXX0/F07.FEC303IP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 SORTOF3 ${DATA}/PXX0/F07.FEC303JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "240"
 /DERIVEDFIELD CST_1_3 "240"
 /DERIVEDFIELD CST_1_6 "099"
 /DERIVEDFIELD CST_3_13 "099"
 /FIELDS FLD_CH_61_3 61 CH 3
 /CONDITION CND_3 FLD_CH_61_3 EQ CST_1_9 OR FLD_CH_61_3 EQ CST_3_13 
 /CONDITION CND_1 FLD_CH_61_3 EQ CST_1_3 
 /CONDITION CND_2 FLD_CH_61_3 EQ CST_1_6 
 /COPY
 /MT_OUTFILE_SUF 1
 /INCLUDE CND_1
 /MT_OUTFILE_SUF 2
 /INCLUDE CND_2
 /MT_OUTFILE_SUF 3
 /OMIT CND_3
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=EC303PAR
       ;;
(EC303PAR)
       m_CondExec 00,EQ,EC303PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP 040                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAT
       ;;
(EC303PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PTEM/EC303PAD.FTEC303P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP 040                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PAX PGM=JVMLDM76   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EC303PAX
       ;;
(EC303PAX)
       m_CondExec ${EXABJ},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A7} DD1 ${DATA}/PXX0/F07.FEC303GP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.FC303ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PAX.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN ZIP 099                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBA
       ;;
(EC303PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A10} SYSOUT ${DATA}/PTEM/EC303PAD.FTEC303P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP 099                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBD PGM=JVMLDM76   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBD
       ;;
(EC303PBD)
       m_CondExec ${EXABT},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A11} DD1 ${DATA}/PXX0/F07.FEC303FP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.GC303ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN ZIP 240                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBG PGM=EZACFSM1   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBG
       ;;
(EC303PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A14} SYSOUT ${DATA}/PTEM/EC303PAD.FTEC303P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP 240                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBJ PGM=JVMLDM76   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBJ
       ;;
(EC303PBJ)
       m_CondExec ${EXACD},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A15} DD1 ${DATA}/PXX0/F07.FEC303EP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.HC303ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTEC303P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBM PGM=EZACFSM1   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBM
       ;;
(EC303PBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A18} SYSOUT ${DATA}/PTEM/EC303PAD.FTEC303P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTEC303P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC303PBQ PGM=FTP        ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=EC303PBQ
       ;;
(EC303PBQ)
       m_CondExec ${EXACN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSTCPD ${DATA}/IPOX.PARMLIB/TCPDAT09
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PBQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.EC303PAD.FTEC303P(+5),DISP=SHR              ~         
#
       m_UtilityExec INPUT
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC303PZA
       ;;
(EC303PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC303PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
