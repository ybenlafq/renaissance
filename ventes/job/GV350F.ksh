#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV350F.ksh                       --- VERSION DU 08/10/2016 12:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGV350 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/22 AT 10.57.07 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV350F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV350FA
       ;;
(GV350FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       RUN=${RUN}
       JUMP_LABEL=GV350FAA
       ;;
(GV350FAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#                                                                              
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV350FAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GV350FAB
       ;;
(GV350FAB)
       m_CondExec 16,NE,GV350FAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BGV350 : EXTRACTION POUR TRANSMISSION DES REGLEMENTS DE VENTE              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV350FAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV350FAD
       ;;
(GV350FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TOUTES LES GV14 FILIALES                                             
#    RSGV14   : NAME=RSGV14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14   : NAME=RSGV14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14   : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14   : NAME=RSGV14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV14   : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
# *********************************** PARAMETRE FMOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FIC D'EXTRACTION                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ FGV350 ${DATA}/PXX0.F99.BGV350AF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV350 
       JUMP_LABEL=GV350FAE
       ;;
(GV350FAE)
       m_CondExec 04,GE,GV350FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BPKZIP : ZIP DU FICHIER D'EXTRACTION POUR ENVOI GATEWAY                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -ARCHIVE(":GV350F"")                                                        
#  -ARCHUNIT(SEM350)                                                           
#  -FILE_TERMINATOR()                                                          
#  -ZIPPED_DSN(":BGV350AF"",REGLEMENTS_VENTES.CSV)                             
#  ":BGV350AF""                                                                
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV350FAG PGM=JVMLDM76   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV350FAG
       ;;
(GV350FAG)
       m_CondExec ${EXAAK},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ FICZIP ${DATA}/PXX0.F99.GV350F
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV350FAG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FICHIER EXTRACTION REGLEMENTS VENTES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=GV350F,                                                             
#      FNAME=":GV350F""                                                        
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGV350F                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV350FAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV350FAJ
       ;;
(GV350FAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GV350FAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GV350FAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV350FAM
       ;;
(GV350FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV350FAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
