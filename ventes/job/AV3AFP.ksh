#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV3AFP.ksh                       --- VERSION DU 08/10/2016 12:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAV3AF -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/22 AT 14.22.30 BY BURTECM                      
#    STANDARDS: P  JOBSET: AV3AFP                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  - MISE COMPLETE DE LA CHAINE ET DES SUCCESSEURS                             
#         DE LA CHAINE AV30FP                                                  
#  - RELEASE DE LA CHAINE DE REMONTEE DES MGI VERS BASE AVOIR : AV250*         
#  - INFO AOI DE LA MISE COMPLETE DES CHAINES ET DU RELEASE                    
# ********************************************************************         
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION  POUR DXREBP                             
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=AV3AFPA
       ;;
(AV3AFPA)
#
#AV3AFPCA
#AV3AFPCA Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#AV3AFPCA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2003/04/22 AT 14.22.30 BY BURTECM                
# *    JOBSET INFORMATION:    NAME...: AV3AFP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-AV3AFP'                            
# *                           APPL...: IMPLUX                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=AV3AFPAA
       ;;
(AV3AFPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT1 ${DATA}/PXX0/F91.BAV240AD
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT2 ${DATA}/PXX0/F61.BAV240AL
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT3 ${DATA}/PXX0/F89.BAV240AM
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT4 ${DATA}/PXX0/F16.BAV240AO
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT5 ${DATA}/PXX0/F07.BAV240AP
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT6 ${DATA}/PXX0/F94.BAV240AR
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -g +1 OUT7 ${DATA}/PXX0/F45.BAV240AY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AV3AFPAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=AV3AFPAB
       ;;
(AV3AFPAB)
       m_CondExec 16,NE,AV3AFPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMIN� AV240R SOUS PLAN PAR JOB UTILITAIRE DE PLAN                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AV3AFPAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
