#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT99P.ksh                       --- VERSION DU 08/10/2016 17:20
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIVT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/05/14 AT 10.04.01 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT99P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  UNLOAD DES TABLES RTIT 00 05 10 15                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT99PA
       ;;
(IVT99PA)
#
#IVT99PAD
#IVT99PAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT99PAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=IVT99PAA
       ;;
(IVT99PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#    RSIT00   : NAME=RSIT00,MODE=I - DYNAM=YES                                 
#    RSIT05   : NAME=RSIT05,MODE=I - DYNAM=YES                                 
#    RSIT10   : NAME=RSIT10,MODE=I - DYNAM=YES                                 
#    RSIT15   : NAME=RSIT15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,CATLG -r 45 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNIT00AP
       m_FileAssign -d NEW,CATLG,CATLG -r 21 -t LSEQ -g +1 SYSREC02 ${DATA}/PXX0/F07.UNIT05AP
       m_FileAssign -d NEW,CATLG,CATLG -r 17 -t LSEQ -g +1 SYSREC03 ${DATA}/PXX0/F07.UNIT10AP
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -t LSEQ -g +1 SYSREC04 ${DATA}/PXX0/F07.UNIT15AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99PAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  QUIESCE DES TABLES RTIT00 05 10 15                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99PAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVT99PAG
       ;;
(IVT99PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE PARAMETRES GENERAUX D'INVENTAIRE                                      
#    RSIT00   : NAME=RSIT00,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSIT00 /dev/null
#  TABLE LIEUX INVENTORIES                                                     
#    RSIT05   : NAME=RSIT05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSIT05 /dev/null
#  TABLE FAMILLES INVENTORIES                                                  
#    RSIT10   : NAME=RSIT10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSIT10 /dev/null
#  TABLE CODICS A INVENTORIER                                                  
#    RSIT15   : NAME=RSIT15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSIT15 /dev/null
#  TABLE GENERALISEE                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  NUMERO DE SOCIETE                                                           
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT020 
       JUMP_LABEL=IVT99PAH
       ;;
(IVT99PAH)
       m_CondExec 04,GE,IVT99PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSIT00                                        
#   REPRISE: NON BACKOUT CORTEX RIVT99P                                        
#            VERIFIER LE BACKOUT RIVT99P                                       
#            REPRISE FORCEE EN DEBUT DE CHAINE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT99PZA
       ;;
(IVT99PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
