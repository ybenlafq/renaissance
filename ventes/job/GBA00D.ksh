#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GBA00D.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGBA00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/03 AT 09.14.35 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GBA00D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSBA01D RSBA10D RSBA20D RSBA21D RSFT29D             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GBA00DA
       ;;
(GBA00DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GBA00DAA
       ;;
(GBA00DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGBA00D
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GBA00DAA
       m_ProgramExec IEFBR14 "RDAR,GBA00D.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GBA00DAD
       ;;
(GBA00DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGBA00D
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GBA00DAE
       ;;
(GBA00DAE)
       m_CondExec 00,EQ,GBA00DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA000 : ALIMENTATION DES TABLES STATISTIQUES                              
#   SELECTION DES BONS D'ACHATS DONT LE CODE ALIMENTATION DES STATS            
#   DANS LA TABLE DES BA EST A BLANC                                           
#   MAJ DU CODE STATISTIQUE A * DANS LA RTBA01                                 
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STAT MENSUELL RTBA2         
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STST ANNUELLE RTBA2         
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAG
       ;;
(GBA00DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00D  : NAME=RSBA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00D /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01D  : NAME=RSBA01D,MODE=U - DYNAM=YES                                
# -X-GBA00DR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSBA01D /dev/null
# ******* TABLE DES STATS MENSUELLES                                           
#    RSBA20D  : NAME=RSBA20D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA20D /dev/null
# ******* TABLE DES STATS ANNUELLES                                            
#    RSBA21D  : NAME=RSBA21D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA21D /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA000 
       JUMP_LABEL=GBA00DAH
       ;;
(GBA00DAH)
       m_CondExec 04,GE,GBA00DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA001 : EDITION DU RECAPITULATIF DES COMMANDES                            
#   EXTRACTION ET MISE EN FORME DES DONNEES ISSUES DES TABLES                  
#   RTBA01  RTBA00  RTGA30 . CONSTITUTION D'UN FICHIER DESTINE AU              
#   GENERATEUR D'ETAT IBA001                                                   
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAJ
       ;;
(GBA00DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00D  : NAME=RSBA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00D /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01D  : NAME=RSBA01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01D /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30D  : NAME=RSBA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA30D /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******* FICHIER A DESTINATION DU GENERATEUR D'ETAT                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA001 ${DATA}/PTEM/GBA00DAJ.BBA001AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA001 
       JUMP_LABEL=GBA00DAK
       ;;
(GBA00DAK)
       m_CondExec 04,GE,GBA00DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA001AD)                                           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAM
       ;;
(GBA00DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GBA00DAJ.BBA001AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DAM.BBA001BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_31_6 31 CH 06
 /FIELDS FLD_BI_25_6 25 CH 06
 /FIELDS FLD_BI_15_10 15 CH 10
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_37_10 37 CH 10
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_10 ASCENDING,
   FLD_BI_25_6 ASCENDING,
   FLD_BI_31_6 ASCENDING,
   FLD_BI_37_10 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DAN
       ;;
(GBA00DAN)
       m_CondExec 00,EQ,GBA00DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BBA001CD                                      
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAQ
       ;;
(GBA00DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/GBA00DAM.BBA001BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00DAQ.BBA001CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00DAR
       ;;
(GBA00DAR)
       m_CondExec 04,GE,GBA00DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAT
       ;;
(GBA00DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GBA00DAQ.BBA001CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DAT.BBA001DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DAU
       ;;
(GBA00DAU)
       m_CondExec 00,EQ,GBA00DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBA001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DAX
       ;;
(GBA00DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/GBA00DAM.BBA001BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/GBA00DAT.BBA001DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00DAY
       ;;
(GBA00DAY)
       m_CondExec 04,GE,GBA00DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA002 : EDITION DE LA LISTE DE CONTROLE DES BONS D'ACHAT ANNULES          
#   TOUS LES BA DONT LE CODE ANNULATION EST RENSEIGNE DANS LA TABLE DE         
#   BA ET DONT LA DATE D'ANNULATION EST EGALE _A LA DATE DE TRAITEMENT          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBA
       ;;
(GBA00DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00D  : NAME=RSBA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00D /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01D  : NAME=RSBA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01D /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER A DESTINATION DU GENERATEUR D'ETAT                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA002 ${DATA}/PTEM/GBA00DBA.BBA002AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA002 
       JUMP_LABEL=GBA00DBB
       ;;
(GBA00DBB)
       m_CondExec 04,GE,GBA00DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA002AD)                                           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBD
       ;;
(GBA00DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GBA00DBA.BBA002AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DBD.BBA002BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_1 07 CH 01
 /FIELDS FLD_BI_8_2 08 CH 02
 /KEYS
   FLD_BI_7_1 ASCENDING,
   FLD_BI_8_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DBE
       ;;
(GBA00DBE)
       m_CondExec 00,EQ,GBA00DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BBA002CD                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBG
       ;;
(GBA00DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/GBA00DBD.BBA002BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00DBG.BBA002CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00DBH
       ;;
(GBA00DBH)
       m_CondExec 04,GE,GBA00DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBJ
       ;;
(GBA00DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GBA00DBG.BBA002CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DBJ.BBA002DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DBK
       ;;
(GBA00DBK)
       m_CondExec 00,EQ,GBA00DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBA001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBM
       ;;
(GBA00DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71D  : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71D /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00D  : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00D /dev/null
#    RTEG05D  : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05D /dev/null
#    RTEG10D  : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10D /dev/null
#    RTEG11D  : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11D /dev/null
#    RTEG15D  : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15D /dev/null
#    RTEG20D  : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20D /dev/null
#    RTEG25D  : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25D /dev/null
#    RTEG30D  : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30D /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/GBA00DBD.BBA002BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/GBA00DBJ.BBA002DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA002 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00DBN
       ;;
(GBA00DBN)
       m_CondExec 04,GE,GBA00DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA210 : IMPUTATIONS COMPTABLES DES COMMANDES                              
#   GENERER DES ECRITURES POUR LES COMMANDES OU ANNULATION DE COMMANDE         
#   DANS LE SYSTEME COMPTABLE                                                  
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBQ
       ;;
(GBA00DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00D  : NAME=RSBA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA00D /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01D  : NAME=RSBA01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01D /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30D  : NAME=RSBA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBA30D /dev/null
# ******* TABLE DES                                                            
#    RSFT17   : NAME=RSFT17D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFT17 /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29   : NAME=RSFT29D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00   : NAME=RSFX00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00DBQ.BBA210D
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA210 
       JUMP_LABEL=GBA00DBR
       ;;
(GBA00DBR)
       m_CondExec 04,GE,GBA00DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA212 : IMPUTATIONS COMPTABLES DES RECEPTIONS                             
#   GENERER DES ECRITURES POUR LES BONS D'ACHAT RECEPTIONNES ET LES            
#   ECARTS CREES LORS DE LA RECETION DES BA DANS LE SYSTEME COMPTABLE          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBT
       ;;
(GBA00DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01D  : NAME=RSBA01D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA01D /dev/null
# ******* TABLE DES RECEPTIONS                                                 
#    RSBA10D  : NAME=RSBA10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSBA10D /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29D  : NAME=RSFT29D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSFT29D /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00D  : NAME=RSFX00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00D /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00DBT.BBA212D
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA212 
       JUMP_LABEL=GBA00DBU
       ;;
(GBA00DBU)
       m_CondExec 04,GE,GBA00DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS ISSUS DES PGMS BBA210 ET BBA212                            
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DBX
       ;;
(GBA00DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER ISSU DU PGM BBA210                                            
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GBA00DBQ.BBA210D
# ****** FICHIER ISSU DU PGM BBA212                                            
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GBA00DBT.BBA212D
# ****** FICHIER CONCATEN� A DESTINATION DE GCT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/F91.GBA00D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_4_8 4 PD 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_PD_4_8 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DBY
       ;;
(GBA00DBY)
       m_CondExec 00,EQ,GBA00DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BBA230 .EDITION DE L ETAT DE SUIVI PARAMETRAGE TIERS                  
# ********************************************************************         
#  CREATION DU FICHIER POUR EDITION                                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DCA
       ;;
(GBA00DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** TABLES DB2                                    
#    RTBA30   : NAME=RSBA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBA30 /dev/null
#    RTFT17   : NAME=RSFT17D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTFT17 /dev/null
# ****************************** FIC D'EXTRACTION                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA130 ${DATA}/PTEM/GBA00DCA.BBA130AD
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE                                                            
# **   N�TACHE =01 + N� CARTE =05 + NOM PGM TBA00                    *         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/BBA230AD
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA230 
       JUMP_LABEL=GBA00DCB
       ;;
(GBA00DCB)
       m_CondExec 04,GE,GBA00DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA130ARD)                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DCD
       ;;
(GBA00DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GBA00DCA.BBA130AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DCD.BBA130BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_13_2 13 CH 02
 /FIELDS FLD_BI_7_6 07 CH 06
 /KEYS
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DCE
       ;;
(GBA00DCE)
       m_CondExec 00,EQ,GBA00DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT                                            
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BBA130BD                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DCG
       ;;
(GBA00DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/GBA00DCD.BBA130BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00DCG.BBA130CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00DCH
       ;;
(GBA00DCH)
       m_CondExec 04,GE,GBA00DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DCJ
       ;;
(GBA00DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GBA00DCG.BBA130CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00DCJ.FBA130AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00DCK
       ;;
(GBA00DCK)
       m_CondExec 00,EQ,GBA00DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00DCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DCM
       ;;
(GBA00DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/GBA00DCD.BBA130BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FCUMULS ${DATA}/PTEM/GBA00DCJ.FBA130AD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE FMOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA130 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00DCN
       ;;
(GBA00DCN)
       m_CondExec 04,GE,GBA00DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GBA00DZA
       ;;
(GBA00DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GBA00DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
