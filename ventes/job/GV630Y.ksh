#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV630Y.ksh                       --- VERSION DU 09/10/2016 05:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGV630 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/18 AT 14.54.44 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV630Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGV630                                                                
# ********************************************************************         
#  TOPER LES LIGNES DEPENDANTES D UNE LIGNE ARTICLE DEJA TOPEE                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV630YA
       ;;
(GV630YA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV630YAA
       ;;
(GV630YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* TABLES EN LECTURE                                                    
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
#    RSGV08Y  : NAME=RSGV08Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08Y /dev/null
#    RSPR00Y  : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00Y /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV10Y  : NAME=RSGV10Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10Y /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11   : NAME=RSRB21,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV630 
       JUMP_LABEL=GV630YAB
       ;;
(GV630YAB)
       m_CondExec 04,GE,GV630YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
