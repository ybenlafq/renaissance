#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV00P.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPHV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 12.00.47 BY BURTECR                      
#    STANDARDS: P  JOBSET: PHV00P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSHV09 RSHV10 RSHV15                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV00PA
       ;;
(PHV00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PHV00PAA
       ;;
(PHV00PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPHV00P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PHV00PAA
       m_ProgramExec IEFBR14 "RDAR,PHV00P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00PAD
       ;;
(PHV00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPHV00P
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PHV00PAE
       ;;
(PHV00PAE)
       m_CondExec 00,EQ,PHV00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE FICHIER HV01 PAR TRI                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAG
       ;;
(PHV00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.HV01RP
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SORTOUT ${DATA}/PXX0/F07.HV01BKP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00PAH
       ;;
(PHV00PAH)
       m_CondExec 00,EQ,PHV00PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV000 : PURGE DES TABLES RTHV09 RTHV10 ET SEQUENTIEL HV01                 
#                  SELECT DES LIGNES OU DVENTELIVREE >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAJ
       ;;
(PHV00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d SHR -g +0 HV01S ${DATA}/PGV0/F07.HV01RP
# *****   HISTO VENTE LIVREE PAR FAMILLE                                       
#    RSHV09   : NAME=RSHV09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV09 /dev/null
# *****   HISTO VENTE LIVREE PAR CODIC                                         
#    RSHV10   : NAME=RSHV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV10 /dev/null
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU  EPURE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV01 ${DATA}/PXX0/PHV00PAJ.BPHV00AP
# *****   FICHIER HISTO VENTES PAR FAMILLE SERVANT A RELOADER RTHV09           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FHV09 ${DATA}/PXX0/F07.RELOAD.HV09RP
# *****   FICHIER HISTO VENTES PAR CODIC   SERVANT A RELOADER RTHV10           
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FHV10 ${DATA}/PXX0/F07.RELOAD.HV10RP
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 1126 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00PAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV000 
       JUMP_LABEL=PHV00PAK
       ;;
(PHV00PAK)
       m_CondExec 04,GE,PHV00PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAM
       ;;
(PHV00PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F07.RELOAD.HV09RP
# ******  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                   
#    RSHV09   : NAME=RSHV09,MODE=(U,N) - DYNAM=YES                             
# -X-PHV00PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00PAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00P_PHV00PAM_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00PAN
       ;;
(PHV00PAN)
       m_CondExec 04,GE,PHV00PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECHARGEMENT DE LA TABLE RTHV10                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAQ
       ;;
(PHV00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F07.RELOAD.HV10RP
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
#    RSHV10   : NAME=RSHV10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV10 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00PAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00P_PHV00PAQ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00PAR
       ;;
(PHV00PAR)
       m_CondExec 04,GE,PHV00PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV015 : PURGE DE LA TABLE RTHV15                                          
#                  SELECT DES LIGNES OU DVENTECIALE  >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAT
       ;;
(PHV00PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE DE CONTROLE DE REMONTEE DE CAISSE                              
#    RSHV15   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
#                                                                              
# *****   FICHIER DE LOAD                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 FHV15 ${DATA}/PXX0/F07.RELOAD.HV15RP
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 90 JOURS                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00PAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV015 
       JUMP_LABEL=PHV00PAU
       ;;
(PHV00PAU)
       m_CondExec 04,GE,PHV00PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV15                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PAX
       ;;
(PHV00PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F07.RELOAD.HV15RP
# ******  TABLE CONTROLE REMONTEE DE CAISSES                                   
#    RSHV15   : NAME=RSHV15,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV15 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00PAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00P_PHV00PAX_RTHV15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00PAY
       ;;
(PHV00PAY)
       m_CondExec 04,GE,PHV00PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER HISTO VENTES/CODIC/LIEU                                      
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PBA
       ;;
(PHV00PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PHV00PAJ.BPHV00AP
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PGV0/F07.HV01RP
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_9 11 CH 9
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00PBB
       ;;
(PHV00PBB)
       m_CondExec 00,EQ,PHV00PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PHV00PZA
       ;;
(PHV00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
