#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV500D.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPV500 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/10/15 AT 10.49.57 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV500D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV500                                                                
#  ------------                                                                
#  EXTRACTION DES VENTES SANS ADRESSE                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV500DA
       ;;
(PV500DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV500DAA
       ;;
(PV500DAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV10   : NAME=RSGV10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTVE10   : NAME=RSVE10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
#    RTVE11   : NAME=RSVE11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS00 /dev/null
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV500DAA
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500 ${DATA}/PTEM/PV500DAA.PV500D1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV500 
       JUMP_LABEL=PV500DAB
       ;;
(PV500DAB)
       m_CondExec 04,GE,PV500DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BPV500                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAD
       ;;
(PV500DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV500DAA.PV500D1
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DAD.PV500D2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_5 57 CH 5
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_133_2 133 CH 2
 /KEYS
   FLD_CH_133_2 ASCENDING,
   FLD_CH_57_5 DESCENDING,
   FLD_CH_31_8 DESCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_40_7 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DAE
       ;;
(PV500DAE)
       m_CondExec 00,EQ,PV500DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTPR12                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAG
       ;;
(PV500DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/PV500DAG.PV500D3
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV500DAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'UNLOAD DE LA TABLE RTPR12                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAJ
       ;;
(PV500DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PV500DAG.PV500D3
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DAJ.PV500D4
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_8 13 CH 8
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_6_2 6 CH 2
 /KEYS
   FLD_CH_6_2 ASCENDING,
   FLD_CH_1_5 DESCENDING,
   FLD_CH_13_8 DESCENDING
 /* Record Type = F  Record Length = 27 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DAK
       ;;
(PV500DAK)
       m_CondExec 00,EQ,PV500DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV504                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR LES SERVICES                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAM
       ;;
(PV500DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A3} FPV500I ${DATA}/PTEM/PV500DAD.PV500D2
       m_FileAssign -d SHR -g ${G_A4} FTPR12 ${DATA}/PTEM/PV500DAJ.PV500D4
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DAM.PV500D5
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV504 
       JUMP_LABEL=PV500DAN
       ;;
(PV500DAN)
       m_CondExec 04,GE,PV500DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV504                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAQ
       ;;
(PV500DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV500DAM.PV500D5
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DAQ.PV500D6
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_47_2 47 CH 2
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_50_7 50 CH 7
 /KEYS
   FLD_CH_47_2 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 DESCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DAR
       ;;
(PV500DAR)
       m_CondExec 00,EQ,PV500DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV503                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES PSE                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAT
       ;;
(PV500DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA40   : NAME=RSGA40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA40 /dev/null
#    RTGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A6} FPV500I ${DATA}/PTEM/PV500DAQ.PV500D6
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DAT.PV500D7
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV503 
       JUMP_LABEL=PV500DAU
       ;;
(PV500DAU)
       m_CondExec 04,GE,PV500DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV503                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV500DAX
       ;;
(PV500DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV500DAT.PV500D7
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DAX.PV500D8
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_47_2 47 CH 2
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_31_8 31 CH 8
 /KEYS
   FLD_CH_47_2 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DAY
       ;;
(PV500DAY)
       m_CondExec 00,EQ,PV500DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV501                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENT                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBA
       ;;
(PV500DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A8} FPV500I ${DATA}/PTEM/PV500DAX.PV500D8
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DBA.PV500D9
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV501 
       JUMP_LABEL=PV500DBB
       ;;
(PV500DBB)
       m_CondExec 04,GE,PV500DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV501                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBD
       ;;
(PV500DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/PV500DBA.PV500D9
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DBD.PV500D10
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_39_1 39 CH 1
 /KEYS
   FLD_CH_39_1 ASCENDING,
   FLD_CH_40_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_17_6 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DBE
       ;;
(PV500DBE)
       m_CondExec 00,EQ,PV500DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV502                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC GROUPE                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBG
       ;;
(PV500DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A10} FPV500I ${DATA}/PTEM/PV500DBD.PV500D10
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DBG.PV500D11
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV502 
       JUMP_LABEL=PV500DBH
       ;;
(PV500DBH)
       m_CondExec 04,GE,PV500DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV502                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBJ
       ;;
(PV500DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/PV500DBG.PV500D11
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DBJ.PV500D12
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_39_1 39 CH 1
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_57_5 57 CH 5
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_39_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_40_7 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_57_5 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DBK
       ;;
(PV500DBK)
       m_CondExec 00,EQ,PV500DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV505                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENTS                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBM
       ;;
(PV500DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A12} FPV500I ${DATA}/PTEM/PV500DBJ.PV500D12
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DBM.PV500D13
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV505 
       JUMP_LABEL=PV500DBN
       ;;
(PV500DBN)
       m_CondExec 04,GE,PV500DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV506                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENTS                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBQ
       ;;
(PV500DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTPV00   : NAME=RSPV00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPV00 /dev/null
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV500DBQ
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV506 ${DATA}/PTEM/PV500DBQ.PV500D19
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV506 
       JUMP_LABEL=PV500DBR
       ;;
(PV500DBR)
       m_CondExec 04,GE,PV500DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV507                                                                
#  ------------                                                                
#  INTEGRATION DES COMMISSIONS OPERATEURS                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBT
       ;;
(PV500DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES DES PRESTATIONS                                               
#    RTPR21   : NAME=RSPR21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR21 /dev/null
#    RTPR22   : NAME=RSPR22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR22 /dev/null
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A13} FPV500I ${DATA}/PTEM/PV500DBM.PV500D13
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV500DBT.PV500D20
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV507 
       JUMP_LABEL=PV500DBU
       ;;
(PV500DBU)
       m_CondExec 04,GE,PV500DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER PV500D   POUR LOAD DE LA TABLE RTPV00                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PV500DBX
       ;;
(PV500DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/PV500DBQ.PV500D19
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/PV500DBT.PV500D20
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.RELOAD.PV00RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_147 1 CH 147
 /KEYS
   FLD_CH_1_147 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DBY
       ;;
(PV500DBY)
       m_CondExec 00,EQ,PV500DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTPV00                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCA PGM=DSNUTILB   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCA
       ;;
(PV500DCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
# ******* TABLE RTPV00                                                         
#    RSPV00   : NAME=RSPV00D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSPV00 /dev/null
# ******* FIC DE LOAD                                                          
       m_FileAssign -d SHR -g ${G_A16} SYSREC ${DATA}/PXX0/F91.RELOAD.PV00RD
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV500DCA.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PV500D_PV500DCA_RTPV00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PV500DCB
       ;;
(PV500DCB)
       m_CondExec 04,GE,PV500DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV510                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENTS                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCD
       ;;
(PV500DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A17} FPV500 ${DATA}/PTEM/PV500DBT.PV500D20
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IPV511 ${DATA}/PTEM/PV500DCD.PV500D14
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/PV500DCD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV510 
       JUMP_LABEL=PV500DCE
       ;;
(PV500DCE)
       m_CondExec 04,GE,PV500DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER PV500D14 POUR EDITION                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCG
       ;;
(PV500DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/PV500DCD.PV500D14
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DCG.PV500D16
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_98_3 98 PD 3
 /FIELDS FLD_PD_95_3 95 PD 3
 /FIELDS FLD_PD_75_5 75 PD 5
 /FIELDS FLD_PD_40_5 40 PD 5
 /FIELDS FLD_PD_45_5 45 PD 5
 /FIELDS FLD_PD_60_5 60 PD 5
 /FIELDS FLD_PD_13_3 13 PD 03
 /FIELDS FLD_PD_50_5 50 PD 5
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_PD_80_5 80 PD 5
 /FIELDS FLD_PD_70_5 70 PD 5
 /FIELDS FLD_BI_18_2 18 CH 02
 /FIELDS FLD_BI_10_3 10 CH 03
 /FIELDS FLD_BI_7_3 07 CH 03
 /FIELDS FLD_PD_90_5 90 PD 5
 /FIELDS FLD_BI_16_2 16 CH 02
 /FIELDS FLD_PD_65_5 65 PD 5
 /FIELDS FLD_PD_85_5 85 PD 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_2 ASCENDING,
   FLD_BI_18_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_5,
    TOTAL FLD_PD_45_5,
    TOTAL FLD_PD_50_5,
    TOTAL FLD_PD_55_5,
    TOTAL FLD_PD_60_5,
    TOTAL FLD_PD_65_5,
    TOTAL FLD_PD_70_5,
    TOTAL FLD_PD_75_5,
    TOTAL FLD_PD_80_5,
    TOTAL FLD_PD_85_5,
    TOTAL FLD_PD_90_5,
    TOTAL FLD_PD_95_3,
    TOTAL FLD_PD_98_3
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DCH
       ;;
(PV500DCH)
       m_CondExec 00,EQ,PV500DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCJ
       ;;
(PV500DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/PV500DCG.PV500D16
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/PV500DCJ.PV500D17
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=PV500DCK
       ;;
(PV500DCK)
       m_CondExec 04,GE,PV500DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCM
       ;;
(PV500DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/PV500DCJ.PV500D17
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV500DCM.PV500D18
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV500DCN
       ;;
(PV500DCN)
       m_CondExec 00,EQ,PV500DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DE L'ETAT IPV511                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCQ
       ;;
(PV500DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/PV500DCG.PV500D16
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A22} FCUMULS ${DATA}/PTEM/PV500DCM.PV500D18
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE FMOIS                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ******  FICHIER S/36 (LRECL 156) EN DUMMY POUR DIF                           
       m_FileAssign -d SHR FEG132 /dev/null
# ******  FICHIER S/36 (LRECL 222) EN DUMMY POUR DIF                           
       m_FileAssign -d SHR FEG198 /dev/null
# ******  ETAT IVA511                                                          
       m_OutputAssign -c 9 -w IPV511 FEDITION
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=PV500DCR
       ;;
(PV500DCR)
       m_CondExec 04,GE,PV500DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * RECOPIE DU FICHIER JOURNALIER SUR L HEBDO                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV500DCT PGM=IDCAMS     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=PV500DCT
       ;;
(PV500DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DE GEN POUR LE TRAITEMENT MENSUEL                                
       m_FileAssign -d SHR -g ${G_A23} IN1 ${DATA}/PTEM/PV500DBT.PV500D20
# ******  FIC DE CUMUL                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 OUT1 ${DATA}/PNCGD/F91.PV800DED
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV500DCT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PV500DCU
       ;;
(PV500DCU)
       m_CondExec 16,NE,PV500DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV500DZA
       ;;
(PV500DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV500DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
