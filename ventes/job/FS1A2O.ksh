#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS1A2O.ksh                       --- VERSION DU 08/10/2016 13:41
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POFS1A2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/04 AT 10.48.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: FS1A2O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#   IDCAMS CREATION A VIDE DES FICHIERS :                                      
#   - FIC1 :                                                                   
# ********************************************************************         
#    CREATION A VIDE D UNE GENERATION                                          
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FS1A2OA
       ;;
(FS1A2OA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2004/03/04 AT 10.48.45 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: FS1A2O                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'ALT-FS052O'                            
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FS1A2OAA
       ;;
(FS1A2OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **** CREATION DU FIC VIRTUEL DE FS052O QUI NE TOURNE PAS                     
# **** CREATION DU FIC VIRTUEL DE FS080O QUI NE TOURNE PAS                     
# ****  SI FS052M EST PLANTEE                                                  
# ******** FIC ENTREE DUMMY                                                    
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
# ******** RECREATION DU NOUVEAU FIC                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT1 ${DATA}/PNCGO/F16.BFS052AO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT2 ${DATA}/PNCGO/F16.BFS080AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -g +1 OUT3 ${DATA}/PNCGO/F16.BBS052AO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FS1A2OAA.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FS1A2OAB
       ;;
(FS1A2OAB)
       m_CondExec 16,NE,FS1A2OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
