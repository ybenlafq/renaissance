#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC306P.ksh                       --- VERSION DU 08/10/2016 13:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC306 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 17.29.31 BY BURTECC                      
#    STANDARDS: P  JOBSET: EC306P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DU FICHIER VENANT DE MORPHEUS                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=EC306PA
       ;;
(EC306PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/07/04 AT 17.29.31 BY BURTECC                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: EC306P                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'REMUT PDTS CEN'                        
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=EC306PAA
       ;;
(EC306PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.FEC306AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FEC306BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_13 17 CH 13
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_17_13 ASCENDING,
   FLD_CH_1_7 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC306PAB
       ;;
(EC306PAB)
       m_CondExec 00,EQ,EC306PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC306  -                                                             
# ********************************************************************         
#  LE PROGRAMME VA LIRE LA QUEUE PARAM�TR�E DANS MQGET POUR MEC06/CAN,         
#  C'EST-_A-DIRE LES MESSAGES RE�US ET TRAIT�S EN JOURN�E PAR LE MEC06          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC306PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC306PAD
       ;;
(EC306PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV80P  : NAME=RSGV80P,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV80P /dev/null
#    RSEC03   : NAME=RSEC03,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEC03 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
#    RTGV11   : NAME=RSGV11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
# ***     TABLES EN ECRITURE                                                   
#    RSGV23   : NAME=RSGV23,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV27   : NAME=RSGV27,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV27 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB15 /dev/null
#    RTSL20   : NAME=RSSL20,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RTSL20 /dev/null
#    RSSL21   : NAME=RSSL21,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL21 /dev/null
#    RSSL22   : NAME=RSSL22,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL22 /dev/null
#    RSSL23   : NAME=RSSL23,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL23 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11X,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV22 /dev/null
# ******* DATE JJMMSSAA                                                        
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
       m_FileAssign -d SHR -g +0 FDATE ${DATA}/PEX0/F07.DATEJ
# ******* FICHIER ISSU DU TRI PR�C�DENT                                        
       m_FileAssign -d SHR -g ${G_A1} FEC306 ${DATA}/PXX0/F07.FEC306BP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC306 
       JUMP_LABEL=EC306PAE
       ;;
(EC306PAE)
       m_CondExec 04,GE,EC306PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC307  -                                                             
# ********************************************************************         
#  LE PGM BEC307 PART DES DONN�ES DE LA RTSL22 CR��E ET METS _A JOUR            
#  LES VENTES (RTGV11, RTGV22) AINSI QUE LES MUTATIONS (RTGB*) ET LES          
#  DONN�ES DE MUTATIONS ET EN RETARD / ANOMALIES (RTSL2*)                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC306PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC306PAG
       ;;
(EC306PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  JJMMSSAA                                                             
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
       m_FileAssign -d SHR -g +0 FDATE ${DATA}/PEX0/F07.DATEJ
# ******  FIC PARAM�TRE                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/EC306PAG
# ******* TABLES EN ECRITURE MIS EN LECTURE POUR EVITER BLOCAGE                
#    RSEC03   : NAME=RSEC03,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSEC03 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV80P  : NAME=RSGV80P,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV80P /dev/null
#    RTGV11   : NAME=RSGV11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV11 /dev/null
# ******* TABLES EN ECRITURE                                                   
#    RSGV23   : NAME=RSGV23,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV27   : NAME=RSGV27,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV27 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB15 /dev/null
#    RTSL20   : NAME=RSSL20,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RTSL20 /dev/null
#    RSSL21   : NAME=RSSL21,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL21 /dev/null
#    RSSL22   : NAME=RSSL22,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL22 /dev/null
#    RSSL23   : NAME=RSSL23,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSSL23 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11X,MODE=(U,I) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=(U,I) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV22 /dev/null
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC307 
       JUMP_LABEL=EC306PAH
       ;;
(EC306PAH)
       m_CondExec 04,GE,EC306PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
