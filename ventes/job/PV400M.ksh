#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV400M.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMPV400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/02 AT 14.41.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: PV400M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV400 SELECTION DES ENREGISTREMENTS POUR EDITION                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV400MA
       ;;
(PV400MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV400MAA
       ;;
(PV400MAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA40   : NAME=RSGA40M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA40 /dev/null
#    RTGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA62   : NAME=RSGA62M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA62 /dev/null
#    RTGA65   : NAME=RSGA65M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA66   : NAME=RSGA66M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA68   : NAME=RSGA68M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS00 /dev/null
#    RTBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS01 /dev/null
#    RTGG40   : NAME=RSGG40M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTPR00   : NAME=RSPR00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR00 /dev/null
#    RTPR03   : NAME=RSPR03M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR03 /dev/null
#    RTPR05   : NAME=RSPR05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR05 /dev/null
#    RTPR12   : NAME=RSPR12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR12 /dev/null
#    RTGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGV31   : NAME=RSGV31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 FDPV400 ${DATA}/PXX0/F89.FDPV400M
# *****   DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FDPV400O ${DATA}/PXX0/F89.FDPV400M
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 FPV400 ${DATA}/PTEM/PV400MAA.BPV400AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV400 
       JUMP_LABEL=PV400MAB
       ;;
(PV400MAB)
       m_CondExec 04,GE,PV400MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER EN VUE DE L EDITION                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAD
       ;;
(PV400MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV400MAA.BPV400AM
# * FICHIER HISTO DU JOUR                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM/PV400MAD.BPV400BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_54_5 54 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_54_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV400MAE
       ;;
(PV400MAE)
       m_CondExec 00,EQ,PV400MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV408 : REPRISE DES MOUVEMENTS DU MOIS DU FICHIER HISTO                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAG PGM=BPV408     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAG
       ;;
(PV400MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  PARAMETRE FNSOC ET FDATE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR -g +0 FPV400H ${DATA}/PXX0/F89.BPV400HM.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 FPV408M ${DATA}/PTEM/PV400MAG.BPV408AM
       m_ProgramExec BPV408 
# ********************************************************************         
#  TRI DU FICHIER EN VUE DE L EDITION                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAJ
       ;;
(PV400MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PV400MAG.BPV408AM
# * FICHIER HISTO DU JOUR                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PTEM/PV400MAJ.BPV408BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "G"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_87_4 87 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 210 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV400MAK
       ;;
(PV400MAK)
       m_CondExec 00,EQ,PV400MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV409 : CREATION DU FICHIER D EDITION                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAM
       ;;
(PV400MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  PARAMETRE FNSOC ET FDATE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR -g ${G_A3} FPV409J ${DATA}/PTEM/PV400MAD.BPV400BM
       m_FileAssign -d SHR -g ${G_A4} FPV409M ${DATA}/PTEM/PV400MAJ.BPV408BM
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 IPV409 ${DATA}/PTEM/PV400MAM.BPV409AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV409 
       JUMP_LABEL=PV400MAN
       ;;
(PV400MAN)
       m_CondExec 04,GE,PV400MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION                                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAQ
       ;;
(PV400MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV400MAM.BPV409AM
       m_OutputAssign -c 9 -w IPV409 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PV400MAR
       ;;
(PV400MAR)
       m_CondExec 00,EQ,PV400MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL DU FICHIER HISTORIQUE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV400MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV400MAT
       ;;
(PV400MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/PV400MAA.BPV400AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BPV400HM.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 210 -g +1 SORTOUT ${DATA}/PXX0/F89.BPV400HM.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING
 /* Record Type = F  Record Length = (210) */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV400MAU
       ;;
(PV400MAU)
       m_CondExec 00,EQ,PV400MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV400MZA
       ;;
(PV400MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV400MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
