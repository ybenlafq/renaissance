#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PHV00Y.ksh                       --- VERSION DU 17/10/2016 18:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPHV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 12.07.12 BY BURTECR                      
#    STANDARDS: P  JOBSET: PHV00Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSHV09Y RSHV10Y RSHV15Y                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PHV00YA
       ;;
(PHV00YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PHV00YAA
       ;;
(PHV00YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QPHV00Y
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PHV00YAA
       m_ProgramExec IEFBR14 "RDAR,PHV00Y.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00YAD
       ;;
(PHV00YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QPHV00Y
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PHV00YAE
       ;;
(PHV00YAE)
       m_CondExec 00,EQ,PHV00YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE FICHIER HV01 PAR TRI                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAG
       ;;
(PHV00YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F45.HV01AY
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,CATLG -r 52 -g +1 SORTOUT ${DATA}/PXX0/F45.HV01BKY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00YAH
       ;;
(PHV00YAH)
       m_CondExec 00,EQ,PHV00YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV000 : PURGE DES TABLES RTHV09 RTHV10 ET SEQUENTIEL HV01                 
#                  SELECT DES LIGNES OU DVENTELIVREE >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAJ
       ;;
(PHV00YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d SHR -g +0 HV01S ${DATA}/PGV0/F45.HV01AY
# *****   HISTO VENTE LIVREE PAR FAMILLE                                       
#    RSHV09Y  : NAME=RSHV09Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV09Y /dev/null
# *****   HISTO VENTE LIVREE PAR CODIC                                         
#    RSHV10Y  : NAME=RSHV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV10Y /dev/null
#                                                                              
# *****   FICHIER HISTO VENTES/CODIC/LIEU  EPURE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FHV01 ${DATA}/PXX0/PHV00YAJ.BPHV00AY
# *****   FICHIER HISTO VENTES PAR FAMILLE SERVANT A RELOADER RTHV09           
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FHV09 ${DATA}/PXX0/F45.RELOAD.HV09RY
# *****   FICHIER HISTO VENTES PAR CODIC   SERVANT A RELOADER RTHV10           
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FHV10 ${DATA}/PXX0/F45.RELOAD.HV10RY
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 1126 JOURS                     
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00YAJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV000 
       JUMP_LABEL=PHV00YAK
       ;;
(PHV00YAK)
       m_CondExec 04,GE,PHV00YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV09                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAM PGM=DSNUTILB   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAM
       ;;
(PHV00YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PXX0/F45.RELOAD.HV09RY
# ******  TABLE HISTO VENTE SOC/FAMILLE/MOIS                                   
#    RSHV09Y  : NAME=RSHV09Y,MODE=(U,N) - DYNAM=YES                            
# -X-PHV00YR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV09Y /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00YAM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00Y_PHV00YAM_RTHV09.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00YAN
       ;;
(PHV00YAN)
       m_CondExec 04,GE,PHV00YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECHARGEMENT DE LA TABLE RTHV10                                             
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAQ
       ;;
(PHV00YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
       m_FileAssign -d SHR -g ${G_A3} SYSREC ${DATA}/PXX0/F45.RELOAD.HV10RY
# ******  TABLE HISTO DE VENTES SOC/ARTICLE/MOIS                               
#    RSHV10Y  : NAME=RSHV10Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV10Y /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00YAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00Y_PHV00YAQ_RTHV10.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00YAR
       ;;
(PHV00YAR)
       m_CondExec 04,GE,PHV00YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPHV015 : PURGE DE LA TABLE RTHV15                                          
#                  SELECT DES LIGNES OU DVENTECIALE  >= FDATE - NJOURS         
#                  FCARTE: NBRE DE JOURS A EXTRAIRE SUR 4 CARACTERES           
#                                                                              
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAT
       ;;
(PHV00YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE DE CONTROLE DE REMONTEE DE CAISSE                              
#    RSHV15Y  : NAME=RSHV15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15Y /dev/null
#                                                                              
# *****   FICHIER DE LOAD                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 FHV15 ${DATA}/PXX0/F45.RELOAD.HV15RY
#                                                                              
# *****   FDATE : JJMMSSAA                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FCARTE: NOMBRE DE JOURS A CONSERVER = 90 JOURS                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/PHV00YAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPHV015 
       JUMP_LABEL=PHV00YAU
       ;;
(PHV00YAU)
       m_CondExec 04,GE,PHV00YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA TABLE RTHV15                                               
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YAX
       ;;
(PHV00YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PXX0/F45.RELOAD.HV15RY
# ******  TABLE CONTROLE REMONTEE DE CAISSES                                   
#    RSHV15Y  : NAME=RSHV15Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV15Y /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00YAX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PHV00Y_PHV00YAX_RTHV15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=PHV00YAY
       ;;
(PHV00YAY)
       m_CondExec 04,GE,PHV00YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER HISTO VENTES/CODIC/LIEU                                      
#  REPRISE FORCEE EN TETE DE CHAINE PAR RSTRT=SAME                             
#          VERIFIER LE BACKOUT ET LAISSER TOMBER                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PHV00YBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YBA
       ;;
(PHV00YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PHV00YAJ.BPHV00AY
# *****   FICHIER HISTO VENTES/CODIC/LIEU                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PGV0/F45.HV01AY
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_9 11 CH 9
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PHV00YBB
       ;;
(PHV00YBB)
       m_CondExec 00,EQ,PHV00YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PHV00YZA
       ;;
(PHV00YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PHV00YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
