#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PLGSM00B.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGSM00B -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: GSM00L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
#  BSM030 :   EDITION DE L'ETAT STOCKS MAGASINS                                
#  REPRISE:   OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GSM00LB
       ;;
(GSM00LB)
       C_A1=${C_A1:-0}
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXC98=${EXC98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON MONDAY    2009/03/30 AT 11.16.57 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: GSM00L                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'EDITION STOCK'                         
# *                           APPL...: REPLILLE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GSM00LBA
       ;;
(GSM00LBA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **************                       NUMERO DE SOCIETE                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FDATE JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE1
_end
# ******  FICHIER EXTRACTION DES STOCKS MAGASINS TRIE                          
       m_FileAssign -d SHR -g +0 FSM025 ${DATA}/PXX0/F61.BSM025BL
# ******  TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  FAMILLES                                                             
#    RSGA14L  : NAME=RSGA14L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA14L /dev/null
# ******  PRIX                                                                 
#    RSGA59L  : NAME=RSGA59L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA59L /dev/null
# ******  COMMISSIONS VENDEURS                                                 
#    RSGA62L  : NAME=RSGA62L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA62L /dev/null
# ******  FICHIER EDITION GENERALISEE POUR GEG00L                              
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG100 ${DATA}/PTEM/GSM00LBA.BSM030AL
# ******  IMPRESSION                                                           
       m_OutputAssign -c 9 -w ISM030 ISM030
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM030 
       JUMP_LABEL=GSM00LBB
       ;;
(GSM00LBB)
       m_CondExec 04,GE,GSM00LBA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER GLOBAL ETAT DE STOCKS                                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00LBD PGM=SORT       ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00LBD
       ;;
(GSM00LBD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BGM075DL
       m_FileAssign -d NEW,CATLG,DELETE -r 213 -g +1 SORTOUT ${DATA}/PTEM/GSM00LBD.BSM036BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_213 1 CH 213
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /* Record Type = F  Record Length = 0213 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_213
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GSM00LBE
       ;;
(GSM00LBE)
       m_CondExec 00,EQ,GSM00LBD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                    EXECUTION BSM036                                          
#                    ****************                                          
#             EDITION DE L'ETAT STOCKS MGI                                     
#           ********************************                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00LBG PGM=IKJEFT01   ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM00LBG
       ;;
(GSM00LBG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **************                       NUMERO DE SOCIETE                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER DATE ***                                                     
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.PARAM.GSM00L.R$[RUN]/FDATE1
# ******  FICHIER EXTRACTION DES STOCKS MAGASINS TRIE ***                      
       m_FileAssign -d SHR -g ${G_A1} FSM025 ${DATA}/PTEM/GSM00LBD.BSM036BL
# ******  TABLE FAMILLES ***                                                   
#    RTGA14   : NAME=RSGA14L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  FPARAM LISTE DES VERSIONS A EXTRAIRE                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GSM00LBG
# ******  FICHIER CONTENANT LES VERSIONS MGI DE L ISM030                       
       m_FileAssign -d NEW,CATLG,DELETE -r 192 -g +1 FSM036 ${DATA}/PXX0.F61.BSM036AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM036 
       JUMP_LABEL=GSM00LBH
       ;;
(GSM00LBH)
       m_CondExec 04,GE,GSM00LBG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   WAIT DE 10 MINUTES POUR L'ETAT CREE PRECEDAMENT ET LE TEMPS D'ETRE         
#   OFFLOADER DANS EOS ET AINSI SOULAGER LE SPOOL.                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00LBJ PGM=HWAIT      ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM00LBJ
       ;;
(GSM00LBJ)
       m_CondExec ${EXCAP},NE,YES 
       m_ProgramExec HWAIT "600"
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=GSM00LZB
       ;;
(GSM00LZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GSM00LZB.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
