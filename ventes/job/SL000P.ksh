#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SL000P.ksh                       --- VERSION DU 09/10/2016 05:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSL000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/07/03 AT 09.58.47 BY BURTECA                      
#    STANDARDS: P  JOBSET: SL000P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI  DU FICHIER ENCAISSEMENT NEM   SAV                                      
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SL000PA
       ;;
(SL000PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SL000PAA
       ;;
(SL000PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BEE005SP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.BEE005SV
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 SORTOUT ${DATA}/PTEM/SL000PAA.BSL000AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "2"
 /DERIVEDFIELD CST_1_5 "0"
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_CH_72_17 72 CH 17
 /FIELDS FLD_CH_47_1 47 CH 1
 /FIELDS FLD_CH_1_8 1 CH 8
 /CONDITION CND_1 FLD_CH_43_1 EQ CST_1_5 AND FLD_CH_47_1 EQ CST_3_9 
 /KEYS
   FLD_CH_72_17 ASCENDING,
   FLD_CH_1_8 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SL000PAB
       ;;
(SL000PAB)
       m_CondExec 00,EQ,SL000PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSL000 : EXTRACTION DES MONTANTS ENCAISSES PAR LIEUX                        
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SL000PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SL000PAD
       ;;
(SL000PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  SOUS TABLE : NASL1                                                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* ENCAISSEMENT SAV (BEE005-FNM001)                                     
       m_FileAssign -d SHR -g ${G_A1} FNM001 ${DATA}/PTEM/SL000PAA.BSL000AP
#                                                                              
# ******* CONTROLE REMONTEE DE CAISSE                                          
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSSL00   : NAME=RSSL00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSL000 
       JUMP_LABEL=SL000PAE
       ;;
(SL000PAE)
       m_CondExec 04,GE,SL000PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI  DU FICHIER ENCAISSEMENT NASL                                           
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SL000PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SL000PAG
       ;;
(SL000PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.AS400.WSLST05P
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -g +1 SORTOUT ${DATA}/PTEM/SL000PAG.BSL005AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_37_2 37 CH 2
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_18_5 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_37_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SL000PAH
       ;;
(SL000PAH)
       m_CondExec 00,EQ,SL000PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSL005 : EXTRACTION DES ENCAISSEMENT NASL                                   
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SL000PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SL000PAJ
       ;;
(SL000PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FIC DES ENCAISSEMENT PROVENANT DE L'AS400 (VOIR DXSL0XXX)            
       m_FileAssign -d SHR -g ${G_A2} FNASL05 ${DATA}/PTEM/SL000PAG.BSL005AP
#                                                                              
# ******* CONTROLE REMONTEE DE CAISSE                                          
#    RSHV15   : NAME=RSHV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
# ******* ENCAISSEMENT NASL                                                    
#    RSSL05   : NAME=RSSL05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL05 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSL005 
       JUMP_LABEL=SL000PAK
       ;;
(SL000PAK)
       m_CondExec 04,GE,SL000PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSL010 : EDITION ETAT COMPARAISON NASL-ENCAISSEMENT                         
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SL000PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SL000PAM
       ;;
(SL000PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* ENCAISSEMENT NASL                                                    
#    RSSL05   : NAME=RSSL05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL05 /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSSL00   : NAME=RSSL00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL00 /dev/null
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* EDITION IEN001                                                       
       m_OutputAssign -c 9 -w IEN001 IEN001
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSL010 
       JUMP_LABEL=SL000PAN
       ;;
(SL000PAN)
       m_CondExec 04,GE,SL000PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSL020 : PURGE DES MONTANTS RAPPROCHES OU NON DES TABLES RTSL00-05          
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SL000PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SL000PAQ
       ;;
(SL000PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* ENCAISSEMENT NASL                                                    
#    RSSL05   : NAME=RSSL05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL05 /dev/null
# ******* COCIC / VALEUR CODE MARKETING                                        
#    RSSL00   : NAME=RSSL00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL00 /dev/null
#                                                                              
# ******* SOCIETE : 907                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* DELAI DE PURGE                                                       
       m_FileAssign -d SHR DELAI ${DATA}/CORTEX4.P.MTXTFIX1/BSL020AP
#                                                                              
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSL020 
       JUMP_LABEL=SL000PAR
       ;;
(SL000PAR)
       m_CondExec 04,GE,SL000PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SL000PZA
       ;;
(SL000PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SL000PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
