#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100Y.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 16.19.32 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV100Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100YA
       ;;
(PV100YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       LASTJO=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=PV100YAA
       ;;
(PV100YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F45.PV100HY.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV100YAA.BPV108EY
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPV102                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAD
       ;;
(PV100YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV100YAA.BPV108EY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100YAD.BPV108MY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_84_3 84 CH 3
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100YAE
       ;;
(PV100YAE)
       m_CondExec 00,EQ,PV100YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV102  CALCUL DES NIVEAUX D AGREGATION                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAG
       ;;
(PV100YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00Y  : NAME=RSGA00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00Y /dev/null
#  TABLE DES IMBRICATIONS DES  CODES MARKETING                                 
#    RTGA09Y  : NAME=RSGA09Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09Y /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGA11Y  : NAME=RSGA11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11Y /dev/null
#  TABLE DES EDITIONS                                                          
#    RTGA12Y  : NAME=RSGA12Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12Y /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14Y  : NAME=RSGA14Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14Y /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA20Y  : NAME=RSGA20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20Y /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA25Y  : NAME=RSGA25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25Y /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA29Y  : NAME=RSGA29Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29Y /dev/null
#  TABLE CODES DESCRIPTIFS CODIC                                               
#    RTGA53Y  : NAME=RSGA53Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53Y /dev/null
#  TABLE HISTORIQUE PRMP                                                       
#    RTGG50Y  : NAME=RSGG50Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50Y /dev/null
#  TABLE HISTORIQUE PRMP DACEM                                                 
#    RTGG55Y  : NAME=RSGG55Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55Y /dev/null
#  TABLE DES MVTS  DE STOCK                                                    
#    RTGS40Y  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40Y /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31Y  : NAME=RSGV31Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31Y /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A2} FPV100 ${DATA}/PTEM/PV100YAD.BPV108MY
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00Y  : NAME=RSAN00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00Y /dev/null
#  SORTIE FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV102 ${DATA}/PTEM/PV100YAG.BPV102AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV102 
       JUMP_LABEL=PV100YAH
       ;;
(PV100YAH)
       m_CondExec 04,GE,PV100YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV103 MAJ DE LA TABLE HV32                   
#  POUR PGM BPV103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAJ
       ;;
(PV100YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PV100YAG.BPV102AY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100YAJ.BPV103AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100YAK
       ;;
(PV100YAK)
       m_CondExec 00,EQ,PV100YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV103  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAM
       ;;
(PV100YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTGV31Y  : NAME=RSGV31Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31Y /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A4} FPV103 ${DATA}/PTEM/PV100YAJ.BPV103AY
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  MAJ DE LA TABLE RTHV32                                                      
#    RSHV32Y  : NAME=RSHV32Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV32Y /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00Y  : NAME=RSAN00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00Y /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV103 
       JUMP_LABEL=PV100YAN
       ;;
(PV100YAN)
       m_CondExec 04,GE,PV100YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV104                                        
#  POUR PGM BPV104                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAQ
       ;;
(PV100YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV100YAA.BPV108EY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100YAQ.BPV104AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_19 "G"
 /DERIVEDFIELD CST_3_23 "     "
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_106_END 106 CH 
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_101_5 101 CH 5
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_19 OR FLD_CH_171_5 EQ CST_3_23 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5,
    TOTAL FLD_CH_101_5,
    TOTAL FLD_CH_106_END
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100YAR
       ;;
(PV100YAR)
       m_CondExec 00,EQ,PV100YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV104  MAJ DE  LA TABLE HV26                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAT
       ;;
(PV100YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE HISTORIQUE DES VENTES DE PSE                                          
#    RTGV26Y  : NAME=RSGV26Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV26Y /dev/null
#                                                                              
# FICHIER ISSU DU TRI PRECEDENT                                                
       m_FileAssign -d SHR -g ${G_A6} FPV104 ${DATA}/PTEM/PV100YAQ.BPV104AY
#  MAJ DE LA TABLE RTHV26 HISTORIQUE VENTE PSE                                 
#    RSHV26Y  : NAME=RSHV26Y,MODE=U - DYNAM=YES                                
# -X-RSHV26Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSHV26Y /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00Y  : NAME=RSAN00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00Y /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV104 
       JUMP_LABEL=PV100YAU
       ;;
(PV100YAU)
       m_CondExec 04,GE,PV100YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV107 MAJ DE LA TABLE HV33                   
#  POUR PGM BPV107                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV100YAX
       ;;
(PV100YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV100YAG.BPV102AY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100YAX.BPV107AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_80_4 80 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100YAY
       ;;
(PV100YAY)
       m_CondExec 00,EQ,PV100YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV107  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBA
       ;;
(PV100YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE DES VENDEURS                                                          
#    RTGV31Y  : NAME=RSGV31Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31Y /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A8} FPV107 ${DATA}/PTEM/PV100YAX.BPV107AY
#  MAJ DE LA TABLE RTHV32 HISTORIQUE VENTES                                    
#    RSHV33Y  : NAME=RSHV33Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV33Y /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV107 
       JUMP_LABEL=PV100YBB
       ;;
(PV100YBB)
       m_CondExec 04,GE,PV100YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBD
       ;;
(PV100YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01Y  : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01Y /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10Y  : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10Y /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31Y  : NAME=RSGV31Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31Y /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
       m_OutputAssign -c 9 -w IPV110 IMPRIM
#  FORMATTAGE DU FICHIER BPV110 POUR EDITION GENERALISE (LRECL 222)            
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG110 ${DATA}/PXX0/F45.BPV110AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100YBE
       ;;
(PV100YBE)
       m_CondExec 04,GE,PV100YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBG
       ;;
(PV100YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01Y  : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01Y /dev/null
#  TABLE                                                                       
#    RTGA10Y  : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10Y /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
       m_OutputAssign -c 9 -w IPV111 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100YBH
       ;;
(PV100YBH)
       m_CondExec 04,GE,PV100YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV116  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBJ
       ;;
(PV100YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01Y  : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01Y /dev/null
#  TABLE                                                                       
#    RTGA10Y  : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10Y /dev/null
#  TABLE                                                                       
#    RTGA11Y  : NAME=RSGA11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11Y /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV116                                                    
       m_OutputAssign -c 9 -w IPV116 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV116 
       JUMP_LABEL=PV100YBK
       ;;
(PV100YBK)
       m_CondExec 04,GE,PV100YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBM PGM=BPV105     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBM
       ;;
(PV100YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F45.PV100HY.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F45.PV100HY.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  : PGM D EPURATION DE HV32 + HV33                                
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBQ
       ;;
(PV100YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTHV32Y  : NAME=RSHV32Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32Y /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV33Y  : NAME=RSHV33Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV33Y /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01Y /dev/null
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100YBR
       ;;
(PV100YBR)
       m_CondExec 04,GE,PV100YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV121         ETAT DE SYNTHESE DES COMMISSIONS                       
#  REPRISE: OUI                 LE DERNIER JOUR DU MOIS                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100YBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV100YBT
       ;;
(PV100YBT)
       m_CondExec ${EXACS},NE,YES 1,EQ,$[LASTJO] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTHV32   : NAME=RSHV32Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTHV32 /dev/null
# *****   FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ****    EDITION DE L ETAT IPV121                                             
       m_OutputAssign -c 9 -w IPV121 IPV121
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV121 
       JUMP_LABEL=PV100YBU
       ;;
(PV100YBU)
       m_CondExec 04,GE,PV100YBT ${EXACS},NE,YES 1,EQ,$[LASTJO] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV100YZA
       ;;
(PV100YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV100YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
