#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE000D.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDRE000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/01/17 AT 09.15.04 BY BURTECA                      
#    STANDARDS: P  JOBSET: RE000D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#   REPRISE: NON BACKOUT JOBSET                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE000DA
       ;;
(RE000DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE000DAA
       ;;
(RE000DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGS105AD
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -g +1 SORTOUT ${DATA}/PTEM/RE000DAA.BRE200AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_19 1 CH 19
 /FIELDS FLD_CH_81_9 81 CH 9
 /KEYS
   FLD_CH_1_19 ASCENDING,
   FLD_CH_81_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DAB
       ;;
(RE000DAB)
       m_CondExec 00,EQ,RE000DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRE200   EXTRACTION DES VENTES DU FICHIER CAISSES 36 VENTE-EMPORTEE         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAD PGM=BRE200     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAD
       ;;
(RE000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE PGM: SI 'N' TRAITEMENT NCG                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE000DAD
#                                                                              
# ******  FICHIER VENTES EMPORTEES TRI�                                        
       m_FileAssign -d SHR -g ${G_A1} FEMPORT ${DATA}/PTEM/RE000DAA.BRE200AD
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000DAD.BRE200BD
       m_ProgramExec BRE200 
# ********************************************************************         
#  BRE205   EXTRACTION DES VENTES DU FICHIER CAISSES AS400 NEM                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAG PGM=BRE205     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAG
       ;;
(RE000DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******  FICHIER VENTES AS400 LG 107                                          
       m_FileAssign -d SHR -g +0 FTS057 ${DATA}/PXX0/F91.BNM157AD
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000DAG.BRE205AD
       m_ProgramExec BRE205 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAJ
       ;;
(RE000DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE000DAD.BRE200BD
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/RE000DAG.BRE205AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/RE000DAJ.BRE100AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_10_4 10 CH 4
 /FIELDS FLD_CH_21_6 21 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_4 ASCENDING,
   FLD_CH_21_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DAK
       ;;
(RE000DAK)
       m_CondExec 00,EQ,RE000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE001 : PROG D EXTRACTION (DEPENDANCE AVEC BGV135=>DSTAT)         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAM
       ;;
(RE000DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE FAMILLES                                                              
#    RTGA14   : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14 /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES VENTES                                                            
#    RTGV11   : NAME=RSGV11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES VENTES : ADRESSES                                                 
#    RTGV02   : NAME=RSGV02D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES GAMMES : PRIX EXCEPTIONNELS                                       
#    RTGG20   : NAME=RSGG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#                                                                              
#  ENTREE: FICHIER SOCIETE                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  ENTREE: TRI DU FICHIER PRECEDEND                                            
       m_FileAssign -d SHR -g ${G_A4} FRE000 ${DATA}/PTEM/RE000DAJ.BRE100AD
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/RE000DAM.BRE001AD
#  FICHIER EN SORTIE : FRE002  DE 512 DE LONG MENSUEL                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE002 ${DATA}/PTEM/RE000DAM.BRE002JD
#  FICHIER EN SORTIE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE003 ${DATA}/PTEM/RE000DAM.BRE003JD
#  FICHIER EN SORTIE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 RTRE00 ${DATA}/PTEM/RE000DAM.BRE010JD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE001 
       JUMP_LABEL=RE000DAN
       ;;
(RE000DAN)
       m_CondExec 04,GE,RE000DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE001AD)                                           
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAQ
       ;;
(RE000DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/RE000DAM.BRE001AD
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000DAQ.BRE001BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_BI_13_5 13 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_BI_13_5 ASCENDING,
   FLD_PD_18_3 DESCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DAR
       ;;
(RE000DAR)
       m_CondExec 00,EQ,RE000DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRE001CD                                      
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAT
       ;;
(RE000DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/RE000DAQ.BRE001BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE000DAT.BRE001CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE000DAU
       ;;
(RE000DAU)
       m_CondExec 04,GE,RE000DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE000DAX
       ;;
(RE000DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE000DAT.BRE001CD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000DAX.FRE001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DAY
       ;;
(RE000DAY)
       m_CondExec 00,EQ,RE000DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRE001                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBA
       ;;
(RE000DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/RE000DAQ.BRE001BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FCUMULS ${DATA}/PTEM/RE000DAX.FRE001AD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F91.BRE065AD
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IRE001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE000DBB
       ;;
(RE000DBB)
       m_CondExec 04,GE,RE000DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION BIT011 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBD
       ;;
(RE000DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F91.BRE065AD
       m_OutputAssign -c 9 -w IRE001 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE000DBE
       ;;
(RE000DBE)
       m_CondExec 00,EQ,RE000DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL DU FRE002                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBG
       ;;
(RE000DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE002  DE 512 DE LONG (JOURNALIER)                     
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/RE000DAM.BRE002JD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BRE002MD.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F91.BRE002MD.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_BI_15_5 15 CH 5
 /FIELDS FLD_BI_9_3 9 CH 3
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DBH
       ;;
(RE000DBH)
       m_CondExec 00,EQ,RE000DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL POUR HISTO VENDEUR (RTRE00)               
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBJ
       ;;
(RE000DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/RE000DAM.BRE010JD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BRE010MD.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 SORTOUT ${DATA}/PXX0/F91.BRE010MD.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_13_6 13 CH 6
 /FIELDS FLD_BI_7_6 7 CH 6
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_6 ASCENDING
 /* Record Type = F  Record Length = 048 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DBK
       ;;
(RE000DBK)
       m_CondExec 00,EQ,RE000DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL FICHIER JOURNALIER FRE003 SUR L HEBDO                                
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBM
       ;;
(RE000DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/RE000DAM.BRE003JD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BRE003HD.HEBDO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F91.BRE003HD.HEBDO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_1 15 CH 1
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_1 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000DBN
       ;;
(RE000DBN)
       m_CondExec 00,EQ,RE000DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DU FICHIER DE REPRISE CREE EN CAS DE PLANTAGE                        
#  REPRISE FORCEE AU DEBUT                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000DBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RE000DBQ
       ;;
(RE000DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -g +1 OUT1 ${DATA}/PXX0/F91.BNM157AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000DBQ.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RE000DBR
       ;;
(RE000DBR)
       m_CondExec 16,NE,RE000DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE000DZA
       ;;
(RE000DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
