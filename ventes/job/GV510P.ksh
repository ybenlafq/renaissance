#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV510P.ksh                       --- VERSION DU 08/10/2016 17:15
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/17 AT 08.56.39 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV510P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV514  POSITIONNE UN FLAG SUR LES ENTETES DE VENTE NON NEM A EPURE         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV510PA
       ;;
(GV510PA)
#
#GV510PAD
#GV510PAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV510PAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV510PAA
       ;;
(GV510PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510PAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV514 
       JUMP_LABEL=GV510PAB
       ;;
(GV510PAB)
       m_CondExec 04,GE,GV510PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU  TABLESPACE : RSMQ15                                             
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510PAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV510PAG
       ;;
(GV510PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  PARAMETRAGE PGM                                                      
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510PAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GV510PAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15   : NAME=RSMQ15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSMQ15 /dev/null
# ******  FICHIER LG 19 EPURATION DES RESERVATIONS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FTS053 ${DATA}/PXX0/F07.BNM153AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GV510PAH
       ;;
(GV510PAH)
       m_CondExec 04,GE,GV510PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV517  SUPPRESSION DANS LA TABLE RTGV10                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV510PAJ
       ;;
(GV510PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSNV20   : NAME=RSNV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV20 /dev/null
# ******  FICHIER MAGASIN NEM                                                  
       m_FileAssign -d SHR -g ${G_A1} FVTENP ${DATA}/PXX0/F07.BNM153AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV517 
       JUMP_LABEL=GV510PAK
       ;;
(GV510PAK)
       m_CondExec 04,GE,GV510PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV516   ENVOI MESSAGES MQSERIES DANS QUEUE LOCALE HOST                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV510PAM
       ;;
(GV510PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510PAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV516 
       JUMP_LABEL=GV510PAN
       ;;
(GV510PAN)
       m_CondExec 04,GE,GV510PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV515  DERESERVATION STOCKS ET QUOTAS + SUPPRESSION SUR TABLES             
#          MOUCHARD DES VENTES NON VALIDEES DONT LA DATE DE VENTE EST          
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
# ********************************************************************         
#  REPRISE: OUI                                                                
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV510PAQ
       ;;
(GV510PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#   TABLE MOUCHARD DES RESERVATIONS STOCKS                                     
#    RSGV40   : NAME=RSGV40,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV40 /dev/null
#   TABLE MOUCHARD DES RESERVATIONS QUOTAS                                     
#    RSGV41   : NAME=RSGV41,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV41 /dev/null
#   TABLE DES ANOMALIES                                                        
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#   TABLE DES EN-TETES DE MUTATION                                             
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#   TABLE DES MUTATIONS DETAIL                                                 
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#   TABLE DES LIGNES COMMANDES SPECIFIQUES                                     
#    RSGF00   : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00 /dev/null
#   TABLE DES QUOTAS DE DELIVRANCE (EMPORTE)                                   
#    RSGQ02   : NAME=RSGQ02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ02 /dev/null
#   TABLE DES QUOTAS DE DELIVRANCE (LIVRE)                                     
#    RSGQ03   : NAME=RSGQ03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ03 /dev/null
#   TABLE DES STOCKS ENTREPOT                                                  
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#   TABLE DU JUSTIFICATIF DU LIEU DE TRANSIT                                   
#    RSGS20   : NAME=RSGS20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS20 /dev/null
#   TABLE DES STOCKS MAGASINS                                                  
#    RSGS30   : NAME=RSGS30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#   TABLE DES                                                                  
#    RSGS43   : NAME=RSGS43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
#   TABLE DES RESERVATIONS SUR STOCKS ENTREPOT                                 
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#   TABLE DES RESERVATIONS SUR STOCKS MAGASINS                                 
#    RSGV22   : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#   TABLE  VENTES                                                              
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#   TABLE DES ARTICLES DES VENTES                                              
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#   TABLE DES ARTICLES                                                         
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#   TABLE GENERALISEE                                                          
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#   TABLE LIEUX                                                                
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#   TABLE DES LIAISONS ARTICLES                                                
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#   DATE JJMMSSAA                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#   CARTE FNSOC                                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510PAQ
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV515 
       JUMP_LABEL=GV510PAR
       ;;
(GV510PAR)
       m_CondExec 04,GE,GV510PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV518  SUPPRIME LES VENTES NON VALIDEES (NON ENCAISSEES)                   
#          DONT LA DATE DE VENTE EST                                           
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
#          EX : LE CLIENT N EST PAS PASSE EN CAISSE CAR TROP DE MONDE          
#  REPRISE: OUI                                                                
# ********************************************************************         
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV510PAT
       ;;
(GV510PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#   TABLE DES ANOMALIES                                                        
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#   TABLE DES ADRESSES DE VENTES                                               
#    RSGV02   : NAME=RSGV02,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#   TABLE GV03                                                                 
#    RSGV03   : NAME=RSGV03,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV03 /dev/null
#   TABLE DES EN-TETES DE VENTES                                               
#    RSGV10   : NAME=RSGV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#   TABLE DES ARTICLES DES VENTES                                              
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#   TABLE DES PRESTATIONS DE  VENTES                                           
#    RSGV13   : NAME=RSGV13,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV13 /dev/null
#   TABLE DES CARACTERISTIQUES ARTICLES DES VENTES                             
#    RSGV15   : NAME=RSGV15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV15 /dev/null
#   TABLE VENTES                                                               
#    RSGV20   : NAME=RSGV20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV20 /dev/null
#   TABLE DES VENTES : AUTORISATIONS PREL STOCK PAR ENTREPOT                   
#    RSGV27   : NAME=RSGV27,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV27 /dev/null
#   TABLE DES VENTES : TABLE MOUCHARD                                          
#    RSGV23   : NAME=RSGV23,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#   TABLE PSE                                                                  
#    RSPS00   : NAME=RSPS00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSPS00 /dev/null
#   DATE JJMMSSAA                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510PAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV518 
       JUMP_LABEL=GV510PAU
       ;;
(GV510PAU)
       m_CondExec 04,GE,GV510PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
