#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV301P.ksh                       --- VERSION DU 08/10/2016 23:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAV301 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 18.45.59 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AV301P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='CONTROLE'                                                          
#                                                                              
# ********************************************************************         
#  PGM BAV301 (BATCH COBOL2 DB2)                                               
#  MISE A JOUR DE LA RTAV10 A PARTIR DU FICHIER D ENRICHISSEMENT FAV30         
#  SOUS-SYSTEME : RDAR                                                         
#  REPRISE : OUI, CAR LE PGM DETECTE SI ENREGISTREMENT DEJA TRAITE             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=AV301PA
       ;;
(AV301PA)
       EXAAA=${EXAAA:-0}
       EXAAP=${EXAAP:-0}
       RUN=${RUN}
       JUMP_LABEL=AV301PAA
       ;;
(AV301PAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
# **    DEPENDANCE PLAN        ****                                            
# *********************************                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FIC POUR ENRICHISSEMENT RTAV10                                       
       m_FileAssign -d SHR -g +0 FAV301 ${DATA}/PXX0/F07.FAV301AP
#                                                                              
# *****   TABLE DE CONTROLE ET SUIVI                                           
#    RSAV10   : NAME=RSAV10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAV10 /dev/null
#                                                                              
# *****   FIC ANOMALIES                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FAV302 ${DATA}/PXX0/F07.FAV301BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV301 
       JUMP_LABEL=AV301PAB
       ;;
(AV301PAB)
       m_CondExec 04,GE,AV301PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICIER PXX0.F07.SOFDARF3                                   
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP AV301PAD PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=AV301PAD
       ;;
(AV301PAD)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.SOFDARTY
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/AV301PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=AV301PAE
       ;;
(AV301PAE)
       m_CondExec 16,NE,AV301PAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
