#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015D.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/03/03 AT 18.39.31 BY BURTECL                      
#    STANDARDS: P  JOBSET: HV015D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DPM'                                                               
# ********************************************************************         
#  QUIESCE DE TABLESPACES RSHV20D RSHV25D AVANT MAJ                            
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015DA
       ;;
(HV015DA)
#
#HV015DAA
#HV015DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV015DAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=HV015DAD
       ;;
(HV015DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F91.BHV000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/HV015DAD.BHV015AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_34_5 34 CH 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_17_8 17 CH 8
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015DAE
       ;;
(HV015DAE)
       m_CondExec 00,EQ,HV015DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV015  : GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE                  
#            DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                      
#  REPRISE :OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015DAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015DAG
       ;;
(HV015DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES (GENERALITES ARTICLES)                                      
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******  FAMILLE (EDITION DES ETATS)                                          
#    RSGA11D  : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11D /dev/null
# ******  RAYONS (COMPOSANTS DU RAYON)                                         
#    RSGA21D  : NAME=RSGA21D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21D /dev/null
# ******  QUOTAS DE LIVRAISON (POPULATION MARCHE/DONNEES DE BASE INSEE         
#    RSGQ01D  : NAME=RSGQ01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01D /dev/null
#                                                                              
# ******  FIC DES VENTES DU MOIS TRI�                                          
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PXX0/HV015DAD.BHV015AD
# ******  FIC DES STATS PAR MAGS/GRPE DE STAT/COMMUNE                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PXX0/HV015DAG.BHV015BD
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015DAH
       ;;
(HV015DAH)
       m_CondExec 04,GE,HV015DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SEQUENTIEL BHV015BD                                          
#  CODE RAYON  1,11 ; CODE COMMUNE  32,5                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015DAJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=HV015DAJ
       ;;
(HV015DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/HV015DAG.BHV015BD
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PXX0/HV015DAJ.BHV015CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_CH_32_5 32 CH 05
 /FIELDS FLD_PD_20_5 20 PD 5
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015DAK
       ;;
(HV015DAK)
       m_CondExec 00,EQ,HV015DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV020 : MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOIS             
#           ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                       
#  REPRISE: OUI SI FIN ANORMALE                                                
#          NON SI FIN NORMALE.FAIRE UN RECOVER DE RSHV20D RSHV25D AVEC         
#          LA VALEUR DU RBA DU PREMIER STEP.UTILISEZ LA CHAINE DB2RBD          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015DAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015DAM
       ;;
(HV015DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES STATS PAR MAGS/GROUPE DE STATS/COMMUNE                       
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PXX0/HV015DAJ.BHV015CD
#                                                                              
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                              
#    RSHV20D  : NAME=RSHV20D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV20D /dev/null
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                       
#    RSHV25D  : NAME=RSHV25D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV25D /dev/null
#                                                                              
# ******  DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015DAN
       ;;
(HV015DAN)
       m_CondExec 04,GE,HV015DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=HV015DZA
       ;;
(HV015DZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
