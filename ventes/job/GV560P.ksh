#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV560P.ksh                       --- VERSION DU 08/10/2016 13:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV560 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/25 AT 11.28.19 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV560P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV560: REAPRO STOCK DISPO PF                                               
#                                                                              
#  REPRISE : NON ENVOY� UN MAIL � YANN MARBAUD ET � LA PREP                    
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV560PA
       ;;
(GV560PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV560PAA
       ;;
(GV560PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#                                                                              
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11   : NAME=RSGV11O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV23   : NAME=RSGV23,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23   : NAME=RSGV23Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23   : NAME=RSGV23M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23   : NAME=RSGV23D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23   : NAME=RSGV23L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV23   : NAME=RSGV23O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV35   : NAME=RSGV35,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSGV35   : NAME=RSGV35Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSGV35   : NAME=RSGV35M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSGV35   : NAME=RSGV35D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSGV35   : NAME=RSGV35L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSGV35   : NAME=RSGV35O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSSL07   : NAME=RSSL07,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL07 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#                                                                              
# ******  TABLES EN M.A.J                                                      
#                                                                              
#    RSGB05   : NAME=RSGB05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGS30   : NAME=RSGS30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV23   : NAME=RSGV23,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#    RSGV35   : NAME=RSGV35,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSSL10   : NAME=RSSL10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL10 /dev/null
# ******                                                                       
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN LECTURE ET ECRITURE                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 FGV560 ${DATA}/PXX0/F07.FGV560AP
# ******                                                                       
# ******                                                                       
# ******************************************************                       
# ******************************************************                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV560 
       JUMP_LABEL=GV560PAB
       ;;
(GV560PAB)
       m_CondExec 04,GE,GV560PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
