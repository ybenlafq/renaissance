#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD2D.ksh                       --- VERSION DU 08/10/2016 17:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGNMD2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/21 AT 14.41.37 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD2D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF870 : MISE � JOUR DE CERTAINES SOUS TABLES DE LA RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD2DA
       ;;
(GNMD2DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+1'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+1'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A71=${G_A71:-'+1'}
       G_A72=${G_A72:-'+1'}
       G_A73=${G_A73:-'+1'}
       G_A74=${G_A74:-'+1'}
       G_A75=${G_A75:-'+1'}
       G_A76=${G_A76:-'+1'}
       G_A77=${G_A77:-'+1'}
       G_A78=${G_A78:-'+1'}
       G_A79=${G_A79:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A80=${G_A80:-'+1'}
       G_A81=${G_A81:-'+1'}
       G_A82=${G_A82:-'+1'}
       G_A83=${G_A83:-'+1'}
       G_A84=${G_A84:-'+1'}
       G_A85=${G_A85:-'+1'}
       G_A86=${G_A86:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GNMD2DAA
       ;;
(GNMD2DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF870 
       JUMP_LABEL=GNMD2DAB
       ;;
(GNMD2DAB)
       m_CondExec 04,GE,GNMD2DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF872 :  MAJ FLAG RTLI00 DES LIEUX MIGRES                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAD
       ;;
(GNMD2DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES LIEUX                                                      
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* PARAMETRE SOCIETE ET DATE                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF872 
       JUMP_LABEL=GNMD2DAE
       ;;
(GNMD2DAE)
       m_CondExec 04,GE,GNMD2DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF500 : EXTRACTION DE LA TABLE DES DEVISE RTFM04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAG
       ;;
(GNMD2DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE DES DEVISE                                                     
#    RSFM04D  : NAME=RSFM04D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM04D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF500 ${DATA}/PTEM/GNMD2DAG.BTF500AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF500 
       JUMP_LABEL=GNMD2DAH
       ;;
(GNMD2DAH)
       m_CondExec 04,GE,GNMD2DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF505 : EXTRACTION DE LA TABLE RTFM05                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAJ
       ;;
(GNMD2DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSFM05   : NAME=RSFM05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF505 ${DATA}/PTEM/GNMD2DAJ.BTF505AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF505 
       JUMP_LABEL=GNMD2DAK
       ;;
(GNMD2DAK)
       m_CondExec 04,GE,GNMD2DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF598 : EXTRACTION DE LA TABLE RTGA00                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAM
       ;;
(GNMD2DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIS�E                                                    
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALIS�E                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA67   : NAME=RSGA67D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
# ******* TABLE ANOMALIES                                                      
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF598 ${DATA}/PTEM/GNMD2DAM.BTF598AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF598 
       JUMP_LABEL=GNMD2DAN
       ;;
(GNMD2DAN)
       m_CondExec 04,GE,GNMD2DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF606 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAQ
       ;;
(GNMD2DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA06   : NAME=RSGA06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA06 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF606 ${DATA}/PTEM/GNMD2DAQ.BTF606AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF606 
       JUMP_LABEL=GNMD2DAR
       ;;
(GNMD2DAR)
       m_CondExec 04,GE,GNMD2DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF611 : CREATION DU FICHIER DE COMPARAISON POUR LES TABLES                 
#                    RTGA09, RTGA11, RTGA12 ET RTGA29                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAT
       ;;
(GNMD2DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA09   : NAME=RSGA09D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09 /dev/null
#    RSGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA29   : NAME=RSGA29D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA29 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS D'EXTRACTION DES ARTICLES                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611A ${DATA}/PTEM/GNMD2DAT.BTF611AD
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611B ${DATA}/PTEM/GNMD2DAT.BTF611BD
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611C ${DATA}/PTEM/GNMD2DAT.BTF611CD
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF611D ${DATA}/PTEM/GNMD2DAT.BTF611DD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF611 
       JUMP_LABEL=GNMD2DAU
       ;;
(GNMD2DAU)
       m_CondExec 04,GE,GNMD2DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF616 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DAX
       ;;
(GNMD2DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF616 ${DATA}/PTEM/GNMD2DAX.BTF616AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF616 
       JUMP_LABEL=GNMD2DAY
       ;;
(GNMD2DAY)
       m_CondExec 04,GE,GNMD2DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF620 : EXTRACTION DE LA SOUS TABLE NMDAT                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBA
       ;;
(GNMD2DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALIE                                                      
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF620 ${DATA}/PTEM/GNMD2DBA.BTF620AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF620 
       JUMP_LABEL=GNMD2DBB
       ;;
(GNMD2DBB)
       m_CondExec 04,GE,GNMD2DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF625 : CREATION DU FICHIER DE POUR LA TABLE RTGA30                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBD
       ;;
(GNMD2DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF625 ${DATA}/PTEM/GNMD2DBD.BTF625AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF625 
       JUMP_LABEL=GNMD2DBE
       ;;
(GNMD2DBE)
       m_CondExec 04,GE,GNMD2DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF631 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA14            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBG
       ;;
(GNMD2DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA30   : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF631 ${DATA}/PTEM/GNMD2DBG.BTF631AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF631 
       JUMP_LABEL=GNMD2DBH
       ;;
(GNMD2DBH)
       m_CondExec 04,GE,GNMD2DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF636 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBJ
       ;;
(GNMD2DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA20   : NAME=RSGA20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF636 ${DATA}/PTEM/GNMD2DBJ.BTF636AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF636 
       JUMP_LABEL=GNMD2DBK
       ;;
(GNMD2DBK)
       m_CondExec 04,GE,GNMD2DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF641 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA21            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBM
       ;;
(GNMD2DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE EN LECTURE                                                     
#    RSGA21   : NAME=RSGA21D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF641 ${DATA}/PTEM/GNMD2DBM.BTF641AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF641 
       JUMP_LABEL=GNMD2DBN
       ;;
(GNMD2DBN)
       m_CondExec 04,GE,GNMD2DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF646 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA22            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBQ
       ;;
(GNMD2DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA22   : NAME=RSGA22D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF646 ${DATA}/PTEM/GNMD2DBQ.BTF646AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF646 
       JUMP_LABEL=GNMD2DBR
       ;;
(GNMD2DBR)
       m_CondExec 04,GE,GNMD2DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF651 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA23            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBT
       ;;
(GNMD2DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA23   : NAME=RSGA23D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA23 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF651 ${DATA}/PTEM/GNMD2DBT.BTF651AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF651 
       JUMP_LABEL=GNMD2DBU
       ;;
(GNMD2DBU)
       m_CondExec 04,GE,GNMD2DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF656 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA24            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DBX
       ;;
(GNMD2DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA24   : NAME=RSGA24D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA24 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF656 ${DATA}/PTEM/GNMD2DBX.BTF656AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF656 
       JUMP_LABEL=GNMD2DBY
       ;;
(GNMD2DBY)
       m_CondExec 04,GE,GNMD2DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF662 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCA
       ;;
(GNMD2DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF662 ${DATA}/PTEM/GNMD2DCA.BTF662AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF662 
       JUMP_LABEL=GNMD2DCB
       ;;
(GNMD2DCB)
       m_CondExec 04,GE,GNMD2DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF666 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCD
       ;;
(GNMD2DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA26   : NAME=RSGA26D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA26 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF666 ${DATA}/PTEM/GNMD2DCD.BTF666AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF666 
       JUMP_LABEL=GNMD2DCE
       ;;
(GNMD2DCE)
       m_CondExec 04,GE,GNMD2DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF671 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA27            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCG
       ;;
(GNMD2DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA27   : NAME=RSGA27D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA27 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF671 ${DATA}/PTEM/GNMD2DCG.BTF671AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF671 
       JUMP_LABEL=GNMD2DCH
       ;;
(GNMD2DCH)
       m_CondExec 04,GE,GNMD2DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF682 : EXTRACTION DE LA TABLE DES GARANTIES RTGA40                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCJ
       ;;
(GNMD2DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLE                                                        
#    RSGA00D  : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00D /dev/null
# ******* TABLE GENERALISE                                                     
#    RSGA01D  : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01D /dev/null
# ******* TABLE                                                                
#    RSGA14D  : NAME=RSGA14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14D /dev/null
# ******* TABLE                                                                
#    RSGA30D  : NAME=RSGA30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30D /dev/null
# ******* TABLE CODICS LIE                                                     
#    RSGA58D  : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58D /dev/null
# ******* TABLE                                                                
#    RSGA67D  : NAME=RSGA67D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67D /dev/null
# ******* TABLE MONTANTS DES GARANTIES COMPLEMENTAIRES                         
#    RSGA40D  : NAME=RSGA40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA40D /dev/null
# ******* TABLE ANO                                                            
#    RSAN00D  : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00D /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF682 ${DATA}/PTEM/GNMD2DCJ.BTF682AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF682 
       JUMP_LABEL=GNMD2DCK
       ;;
(GNMD2DCK)
       m_CondExec 04,GE,GNMD2DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF686 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCM
       ;;
(GNMD2DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA41   : NAME=RSGA41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA41 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF686 ${DATA}/PTEM/GNMD2DCM.BTF686AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF686 
       JUMP_LABEL=GNMD2DCN
       ;;
(GNMD2DCN)
       m_CondExec 04,GE,GNMD2DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF687 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA51            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCQ
       ;;
(GNMD2DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA51   : NAME=RSGA51D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA51 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF687 ${DATA}/PTEM/GNMD2DCQ.BTF687AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF687 
       JUMP_LABEL=GNMD2DCR
       ;;
(GNMD2DCR)
       m_CondExec 04,GE,GNMD2DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF692 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA52            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCT
       ;;
(GNMD2DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA52   : NAME=RSGA52D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA52 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF692 ${DATA}/PTEM/GNMD2DCT.BTF692AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF692 
       JUMP_LABEL=GNMD2DCU
       ;;
(GNMD2DCU)
       m_CondExec 04,GE,GNMD2DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF702 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA58            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DCX
       ;;
(GNMD2DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES CODICS GROUPES                                             
#    RSGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF702 ${DATA}/PTEM/GNMD2DCX.BTF702AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF702 
       JUMP_LABEL=GNMD2DCY
       ;;
(GNMD2DCY)
       m_CondExec 04,GE,GNMD2DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF707 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA64            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDA
       ;;
(GNMD2DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA64   : NAME=RSGA64D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA64 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF707 ${DATA}/PTEM/GNMD2DDA.BTF707AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF707 
       JUMP_LABEL=GNMD2DDB
       ;;
(GNMD2DDB)
       m_CondExec 04,GE,GNMD2DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF710 : EXTRACTION DE LA TABLE GQ12                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDD
       ;;
(GNMD2DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE MODE DE DELIVRANCE                                             
#    RSGQ12   : NAME=RSGQ12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF710 ${DATA}/PTEM/GNMD2DDD.BTF710AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF710 
       JUMP_LABEL=GNMD2DDE
       ;;
(GNMD2DDE)
       m_CondExec 04,GE,GNMD2DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF715 : EXTRACTION TABLE DES CODES VENDEURS RTGV31                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDG
       ;;
(GNMD2DDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE CODES VENDEURS                                                 
#    RSGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF715 ${DATA}/PTEM/GNMD2DDG.BTF715AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF715 
       JUMP_LABEL=GNMD2DDH
       ;;
(GNMD2DDH)
       m_CondExec 04,GE,GNMD2DDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF718 : EXTRACTION TABLE COMPTEUR SAV RTPA70                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDJ
       ;;
(GNMD2DDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPA70   : NAME=RSPA70D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPA70 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF718 ${DATA}/PTEM/GNMD2DDJ.BTF718AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF718 
       JUMP_LABEL=GNMD2DDK
       ;;
(GNMD2DDK)
       m_CondExec 04,GE,GNMD2DDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF720 : EXTRACTION TABLE MODE PMT / RBMT RTPM06                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDM PGM=IKJEFT01   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDM
       ;;
(GNMD2DDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE COMPTEURS SAV                                                  
#    RSPM06   : NAME=RSPM06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF720 ${DATA}/PTEM/GNMD2DDM.BTF720AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF720 
       JUMP_LABEL=GNMD2DDN
       ;;
(GNMD2DDN)
       m_CondExec 04,GE,GNMD2DDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF725 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPM35            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDQ
       ;;
(GNMD2DDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PARAMETRES MQSERIES                                            
#    RSPM35   : NAME=RSPM35,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPM35 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF725 ${DATA}/PTEM/GNMD2DDQ.BTF725AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF725 
       JUMP_LABEL=GNMD2DDR
       ;;
(GNMD2DDR)
       m_CondExec 04,GE,GNMD2DDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF751 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDT PGM=IKJEFT01   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDT
       ;;
(GNMD2DDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE PRESTATION                                                     
#    RSPR00D  : NAME=RSPR00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00D /dev/null
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF751 ${DATA}/PTEM/GNMD2DDT.BTF751AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF751 
       JUMP_LABEL=GNMD2DDU
       ;;
(GNMD2DDU)
       m_CondExec 04,GE,GNMD2DDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF755 : EXTRACTION TABLE LIEN FAMILLE/CODE PRESTATION RTPR01               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DDX PGM=IKJEFT01   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DDX
       ;;
(GNMD2DDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN FAMILLE / CODE PRESTATION                                 
#    RSPR01   : NAME=RSPR01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF755 ${DATA}/PTEM/GNMD2DDX.BTF755AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF755 
       JUMP_LABEL=GNMD2DDY
       ;;
(GNMD2DDY)
       m_CondExec 04,GE,GNMD2DDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF761 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEA PGM=IKJEFT01   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEA
       ;;
(GNMD2DEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#    RSPR02D  : NAME=RSPR02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR02D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF761 ${DATA}/PTEM/GNMD2DEA.BTF761AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF761 
       JUMP_LABEL=GNMD2DEB
       ;;
(GNMD2DEB)
       m_CondExec 04,GE,GNMD2DEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF766 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTPR03  *         
#  REPRISE: OUI  *                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DED PGM=IKJEFT01   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DED
       ;;
(GNMD2DED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
#    RSPR03D  : NAME=RSPR03D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR03D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF766 ${DATA}/PTEM/GNMD2DED.BTF766AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF766 
       JUMP_LABEL=GNMD2DEE
       ;;
(GNMD2DEE)
       m_CondExec 04,GE,GNMD2DED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF770 : EXTRACTION TABLE LIEN PRESTATIONS RTPR04                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEG PGM=IKJEFT01   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEG
       ;;
(GNMD2DEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE LIEN PRESTATION                                                
#    RSPR04   : NAME=RSPR04D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR04 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF770 ${DATA}/PTEM/GNMD2DEG.BTF770AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF770 
       JUMP_LABEL=GNMD2DEH
       ;;
(GNMD2DEH)
       m_CondExec 04,GE,GNMD2DEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF777 : EXTRACT TABLE PRIX PRESTATION LIE A LA ZONE DE PRIX RTPR10         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEJ PGM=IKJEFT01   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEJ
       ;;
(GNMD2DEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06D  : NAME=RSPR06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06D /dev/null
# ******* TABLE PRIX PRESTATION LIE A ZONE DE PRIX                             
#    RSPR10D  : NAME=RSPR10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR10D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF777 ${DATA}/PTEM/GNMD2DEJ.BTF777AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF777 
       JUMP_LABEL=GNMD2DEK
       ;;
(GNMD2DEK)
       m_CondExec 04,GE,GNMD2DEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF782 : EXTRACTION TABLE PRIX PRESTATION LIE AU CODIC RTPR14               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEM PGM=IKJEFT01   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEM
       ;;
(GNMD2DEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10D  : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10D /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR06D  : NAME=RSPR06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR06D /dev/null
# ******* TABLE PRIX PRESTATION LIE A ARTICLE                                  
#    RSPR14D  : NAME=RSPR14D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR14D /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF782 ${DATA}/PTEM/GNMD2DEM.BTF782AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF782 
       JUMP_LABEL=GNMD2DEN
       ;;
(GNMD2DEN)
       m_CondExec 04,GE,GNMD2DEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF783 : EXTRACTION DE LA TABLE RTPR53                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEQ PGM=IKJEFT01   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEQ
       ;;
(GNMD2DEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE PRESTATION                                                     
#    RSPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR53 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF783 ${DATA}/PTEM/GNMD2DEQ.BTF783AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF783 
       JUMP_LABEL=GNMD2DER
       ;;
(GNMD2DER)
       m_CondExec 04,GE,GNMD2DEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF784 : EXTRACTION DE LA TABLE RTGA31                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DET PGM=IKJEFT01   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DET
       ;;
(GNMD2DET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE LIEUX                                                          
#    RSGA31   : NAME=RSGA31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA31 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF784 ${DATA}/PTEM/GNMD2DET.BTF784AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF784 
       JUMP_LABEL=GNMD2DEU
       ;;
(GNMD2DEU)
       m_CondExec 04,GE,GNMD2DET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BFT785 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DEX PGM=IKJEFT01   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DEX
       ;;
(GNMD2DEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF785 ${DATA}/PTEM/GNMD2DEX.BTF785AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF785 
       JUMP_LABEL=GNMD2DEY
       ;;
(GNMD2DEY)
       m_CondExec 04,GE,GNMD2DEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF786 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFA PGM=IKJEFT01   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFA
       ;;
(GNMD2DFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF786 ${DATA}/PTEM/GNMD2DFA.BTF786AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF786 
       JUMP_LABEL=GNMD2DFB
       ;;
(GNMD2DFB)
       m_CondExec 04,GE,GNMD2DFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF787 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFD PGM=IKJEFT01   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFD
       ;;
(GNMD2DFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF787 ${DATA}/PTEM/GNMD2DFD.BTF787AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF787 
       JUMP_LABEL=GNMD2DFE
       ;;
(GNMD2DFE)
       m_CondExec 04,GE,GNMD2DFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF788 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTXM03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFG PGM=IKJEFT01   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFG
       ;;
(GNMD2DFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF788 ${DATA}/PTEM/GNMD2DFG.BTF788AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF788 
       JUMP_LABEL=GNMD2DFH
       ;;
(GNMD2DFH)
       m_CondExec 04,GE,GNMD2DFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF794 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFJ PGM=IKJEFT01   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFJ
       ;;
(GNMD2DFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF794 ${DATA}/PTEM/GNMD2DFJ.BTF794AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF794 
       JUMP_LABEL=GNMD2DFK
       ;;
(GNMD2DFK)
       m_CondExec 04,GE,GNMD2DFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF795 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFM PGM=IKJEFT01   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFM
       ;;
(GNMD2DFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS01   : NAME=RSBS01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF795 ${DATA}/PTEM/GNMD2DFM.BTF795AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF795 
       JUMP_LABEL=GNMD2DFN
       ;;
(GNMD2DFN)
       m_CondExec 04,GE,GNMD2DFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF796 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFQ PGM=IKJEFT01   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFQ
       ;;
(GNMD2DFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS02   : NAME=RSBS02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF796 ${DATA}/PTEM/GNMD2DFQ.BTF796AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF796 
       JUMP_LABEL=GNMD2DFR
       ;;
(GNMD2DFR)
       m_CondExec 04,GE,GNMD2DFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF797 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS03            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFT PGM=IKJEFT01   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFT
       ;;
(GNMD2DFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS03   : NAME=RSBS03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS03 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF797 ${DATA}/PTEM/GNMD2DFT.BTF797AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF797 
       JUMP_LABEL=GNMD2DFU
       ;;
(GNMD2DFU)
       m_CondExec 04,GE,GNMD2DFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF798 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS25            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DFX PGM=IKJEFT01   ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DFX
       ;;
(GNMD2DFX)
       m_CondExec ${EXAJB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS25   : NAME=RSBS25P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS25 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF798 ${DATA}/PTEM/GNMD2DFX.BTF798AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF798 
       JUMP_LABEL=GNMD2DFY
       ;;
(GNMD2DFY)
       m_CondExec 04,GE,GNMD2DFX ${EXAJB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF799 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTBS26            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGA PGM=IKJEFT01   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGA
       ;;
(GNMD2DGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSBS26   : NAME=RSBS26P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSBS26 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF799 ${DATA}/PTEM/GNMD2DGA.BTF799AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF799 
       JUMP_LABEL=GNMD2DGB
       ;;
(GNMD2DGB)
       m_CondExec 04,GE,GNMD2DGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF802 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTIP02            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGD PGM=IKJEFT01   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGD
       ;;
(GNMD2DGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g +0 FPV212 ${DATA}/PXX0/F91.BPV212AD
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF802 ${DATA}/PXX0/F91.BTF802AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF802 
       JUMP_LABEL=GNMD2DGE
       ;;
(GNMD2DGE)
       m_CondExec 04,GE,GNMD2DGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF805 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA91            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGG PGM=IKJEFT01   ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGG
       ;;
(GNMD2DGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE DES                                                            
#    RSGA91   : NAME=RSGA91D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA91 /dev/null
# ******* PARAMETRE SOCIETE :                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF805 ${DATA}/PTEM/GNMD2DGG.BTF805AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF805 
       JUMP_LABEL=GNMD2DGH
       ;;
(GNMD2DGH)
       m_CondExec 04,GE,GNMD2DGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF810 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA92            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGJ PGM=IKJEFT01   ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGJ
       ;;
(GNMD2DGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA92   : NAME=RSGA92D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA92 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF810 ${DATA}/PTEM/GNMD2DGJ.BTF810AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF810 
       JUMP_LABEL=GNMD2DGK
       ;;
(GNMD2DGK)
       m_CondExec 04,GE,GNMD2DGJ ${EXAJV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF815 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA93            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGM PGM=IKJEFT01   ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGM
       ;;
(GNMD2DGM)
       m_CondExec ${EXAKA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA93   : NAME=RSGA93D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA93 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF815 ${DATA}/PTEM/GNMD2DGM.BTF815AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF815 
       JUMP_LABEL=GNMD2DGN
       ;;
(GNMD2DGN)
       m_CondExec 04,GE,GNMD2DGM ${EXAKA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF820 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA12            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGQ PGM=IKJEFT01   ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGQ
       ;;
(GNMD2DGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR12   : NAME=RSPR12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR12 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF820 ${DATA}/PTEM/GNMD2DGQ.BTF820AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF820 
       JUMP_LABEL=GNMD2DGR
       ;;
(GNMD2DGR)
       m_CondExec 04,GE,GNMD2DGQ ${EXAKF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF825 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGT PGM=IKJEFT01   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGT
       ;;
(GNMD2DGT)
       m_CondExec ${EXAKK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSPR16   : NAME=RSPR16D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF825 ${DATA}/PTEM/GNMD2DGT.BTF825AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF825 
       JUMP_LABEL=GNMD2DGU
       ;;
(GNMD2DGU)
       m_CondExec 04,GE,GNMD2DGT ${EXAKK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF835 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA56            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DGX PGM=IKJEFT01   ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DGX
       ;;
(GNMD2DGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGA56   : NAME=RSGA56D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA56 /dev/null
# ******* TABLE                                                                
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* TABLE                                                                
#    RSGA65   : NAME=RSGA65D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA65 /dev/null
# ******* TABLE                                                                
#    RSGA66   : NAME=RSGA66D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA66 /dev/null
# ******* TABLE                                                                
#    RSGA68   : NAME=RSGA68D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA68 /dev/null
# ******* TABLE                                                                
#    RSGA75   : NAME=RSGA75D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA75 /dev/null
# ******* TABLE                                                                
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF835 ${DATA}/PTEM/GNMD2DGX.BTF835AD
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2DGX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF835 
       JUMP_LABEL=GNMD2DGY
       ;;
(GNMD2DGY)
       m_CondExec 04,GE,GNMD2DGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF836 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHA PGM=IKJEFT01   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHA
       ;;
(GNMD2DHA)
       m_CondExec ${EXAKU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE                                                                
#    RSGQ01   : NAME=RSGQ01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF836 ${DATA}/PTEM/GNMD2DHA.BTF836AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF836 
       JUMP_LABEL=GNMD2DHB
       ;;
(GNMD2DHB)
       m_CondExec 04,GE,GNMD2DHA ${EXAKU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF837 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGQ06            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHD PGM=IKJEFT01   ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHD
       ;;
(GNMD2DHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGQ06   : NAME=RSGQ06D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ06 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF837 ${DATA}/PTEM/GNMD2DHD.BTF837AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF837 
       JUMP_LABEL=GNMD2DHE
       ;;
(GNMD2DHE)
       m_CondExec 04,GE,GNMD2DHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF838 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL01            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHG PGM=IKJEFT01   ** ID=ALE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHG
       ;;
(GNMD2DHG)
       m_CondExec ${EXALE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE STOCK LOCAUX                                                   
#    RSSL01   : NAME=RSSL01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL01 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF838 ${DATA}/PTEM/GNMD2DHG.BTF838AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF838 
       JUMP_LABEL=GNMD2DHH
       ;;
(GNMD2DHH)
       m_CondExec 04,GE,GNMD2DHG ${EXALE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF839 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL16            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHJ PGM=IKJEFT01   ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHJ
       ;;
(GNMD2DHJ)
       m_CondExec ${EXALJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSSL16   : NAME=RSSL16,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL16 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF839 ${DATA}/PTEM/GNMD2DHJ.BTF839AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF839 
       JUMP_LABEL=GNMD2DHK
       ;;
(GNMD2DHK)
       m_CondExec 04,GE,GNMD2DHJ ${EXALJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF840 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHM PGM=IKJEFT01   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHM
       ;;
(GNMD2DHM)
       m_CondExec ${EXALO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* TABLE                                                                
#    RSGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF840 ${DATA}/PTEM/GNMD2DHM.BTF840AD
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 HAD840 ${DATA}/PXX0/F91.BTF840BD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF840 
       JUMP_LABEL=GNMD2DHN
       ;;
(GNMD2DHN)
       m_CondExec 04,GE,GNMD2DHM ${EXALO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * BTF841 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTEE03           
# * REPRISE: OUI                                                               
# ********************************************************************         
# ALT      STEP  PGM=IKJEFT01                                                  
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# ******** TABLE CODES POSTAUX                                                 
# RSEE03   FILE  DYNAM=YES,NAME=RSEE03,MODE=I                                  
# ******** PARAMETRE SOCIETE                                                   
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDPM                                         
# ******** PARAMETRE FDATE                                                     
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** FICHIER D'EXTRACTION                                                
# FTF841   FILE  NAME=BTF841AD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BTF841) PLAN(BTF841D)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#  BTF842 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGB05            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHQ PGM=IKJEFT01   ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHQ
       ;;
(GNMD2DHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF842 ${DATA}/PTEM/GNMD2DHQ.BTF842AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF842 
       JUMP_LABEL=GNMD2DHR
       ;;
(GNMD2DHR)
       m_CondExec 04,GE,GNMD2DHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF843 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA54            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHT PGM=IKJEFT01   ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHT
       ;;
(GNMD2DHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CODES POSTAUX                                                  
#    RSGA54   : NAME=RSGA54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA54 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF843 ${DATA}/PTEM/GNMD2DHT.BTF843AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF843 
       JUMP_LABEL=GNMD2DHU
       ;;
(GNMD2DHU)
       m_CondExec 04,GE,GNMD2DHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF844 : CREATION DU FICHIER DE COMPARAISON POUR GV21/22/35/11 TL02         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DHX PGM=IKJEFT01   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DHX
       ;;
(GNMD2DHX)
       m_CondExec ${EXAMD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#    RSGV35   : NAME=RSGV35D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV35 /dev/null
#    RSTL02   : NAME=RSTL02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTL02 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF844 ${DATA}/PTEM/GNMD2DHX.BTF844AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF844 
       JUMP_LABEL=GNMD2DHY
       ;;
(GNMD2DHY)
       m_CondExec 04,GE,GNMD2DHX ${EXAMD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF845 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA53            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIA PGM=IKJEFT01   ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIA
       ;;
(GNMD2DIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA25   : NAME=RSGA25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA25 /dev/null
#    RSGA53   : NAME=RSGA53D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA53 /dev/null
# ******* PARAMETRE FDATE                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF845 ${DATA}/PTEM/GNMD2DIA.BTF845AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF845 
       JUMP_LABEL=GNMD2DIB
       ;;
(GNMD2DIB)
       m_CondExec 04,GE,GNMD2DIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF846 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL40            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DID PGM=IKJEFT01   ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DID
       ;;
(GNMD2DID)
       m_CondExec ${EXAMN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL40   : NAME=RSSL40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL40 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF846 ${DATA}/PTEM/GNMD2DID.BTF846AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF846 
       JUMP_LABEL=GNMD2DIE
       ;;
(GNMD2DIE)
       m_CondExec 04,GE,GNMD2DID ${EXAMN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF847 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL41            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIG PGM=IKJEFT01   ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIG
       ;;
(GNMD2DIG)
       m_CondExec ${EXAMS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RSSL41   : NAME=RSSL41D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSSL41 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF847 ${DATA}/PTEM/GNMD2DIG.BTF847AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF847 
       JUMP_LABEL=GNMD2DIH
       ;;
(GNMD2DIH)
       m_CondExec 04,GE,GNMD2DIG ${EXAMS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF848 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL10            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIJ PGM=IKJEFT01   ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIJ
       ;;
(GNMD2DIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE STOCK LOCAL - MODE I (POUR EVITER SERIALIS.DES FILIALE         
#    RSSL10   : NAME=RSSL10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL10 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF848 ${DATA}/PTEM/GNMD2DIJ.BTF848AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF848 
       JUMP_LABEL=GNMD2DIK
       ;;
(GNMD2DIK)
       m_CondExec 04,GE,GNMD2DIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF849 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTYF00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIM PGM=IKJEFT01   ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIM
       ;;
(GNMD2DIM)
       m_CondExec ${EXANC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE PARIS                                                          
#    RSYF00   : NAME=RSYF00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSYF00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******* FICHIER D'EXTRACTION DES CODES VENDEURS                              
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF849 ${DATA}/PTEM/GNMD2DIM.BTF849AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF849 
       JUMP_LABEL=GNMD2DIN
       ;;
(GNMD2DIN)
       m_CondExec 04,GE,GNMD2DIM ${EXANC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF860 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLI00            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIQ PGM=IKJEFT01   ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIQ
       ;;
(GNMD2DIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF860 ${DATA}/PTEM/GNMD2DIQ.BTF860AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF860 
       JUMP_LABEL=GNMD2DIR
       ;;
(GNMD2DIR)
       m_CondExec 04,GE,GNMD2DIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF862 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTLG09            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIT PGM=IKJEFT01   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIT
       ;;
(GNMD2DIT)
       m_CondExec ${EXANM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSLL09   : NAME=RSLG09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLL09 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF862 ${DATA}/PTEM/GNMD2DIT.BTF862AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF862 
       JUMP_LABEL=GNMD2DIU
       ;;
(GNMD2DIU)
       m_CondExec 04,GE,GNMD2DIT ${EXANM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF863 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA59            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DIX PGM=IKJEFT01   ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DIX
       ;;
(GNMD2DIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF863 ${DATA}/PTEM/GNMD2DIX.BTF863AD
# ******* PARAMETRE D'EXTRACTION                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2DIX
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF863 
       JUMP_LABEL=GNMD2DIY
       ;;
(GNMD2DIY)
       m_CondExec 04,GE,GNMD2DIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF864 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGG20            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJA PGM=IKJEFT01   ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJA
       ;;
(GNMD2DJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ZONES DE PRIX                                                  
#    RSGA59   : NAME=RSGA59D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSGG20   : NAME=RSGG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG20 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF864 ${DATA}/PTEM/GNMD2DJA.BTF864AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF864 
       JUMP_LABEL=GNMD2DJB
       ;;
(GNMD2DJB)
       m_CondExec 04,GE,GNMD2DJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF865 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJD PGM=IKJEFT01   ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJD
       ;;
(GNMD2DJD)
       m_CondExec ${EXAOB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE POUR LA CONTRIBUTION RECYCLAGE D3E                             
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
# ******* VARIABLE DATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF865 ${DATA}/PTEM/GNMD2DJD.BTF865AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF865 
       JUMP_LABEL=GNMD2DJE
       ;;
(GNMD2DJE)
       m_CondExec 04,GE,GNMD2DJD ${EXAOB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF875 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA82            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJG PGM=IKJEFT01   ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJG
       ;;
(GNMD2DJG)
       m_CondExec ${EXAOG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE GENERALISEE                                                    
#    RSGA82   : NAME=RSGA82,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA82 /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF875 ${DATA}/PTEM/GNMD2DJG.BTF875AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF875 
       JUMP_LABEL=GNMD2DJH
       ;;
(GNMD2DJH)
       m_CondExec 04,GE,GNMD2DJG ${EXAOG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF877 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTGA38            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJJ PGM=IKJEFT01   ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJJ
       ;;
(GNMD2DJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877A ${DATA}/PTEM/GNMD2DJJ.BTF877AD
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF877B ${DATA}/PTEM/GNMD2DJJ.BTF877BD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF877 
       JUMP_LABEL=GNMD2DJK
       ;;
(GNMD2DJK)
       m_CondExec 04,GE,GNMD2DJJ ${EXAOL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF878 : CREATION DU FICHIER DE COMPARAISON POUR LA TABLE RTSL17            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJM PGM=IKJEFT01   ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJM
       ;;
(GNMD2DJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* PARAMETRE QUE SUR DIF                                                
#    RSSL17   : NAME=RSSL17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL17 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* DATE DU JOUR JJMMSSAA                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF878 ${DATA}/PTEM/GNMD2DJM.BTF878AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF878 
       JUMP_LABEL=GNMD2DJN
       ;;
(GNMD2DJN)
       m_CondExec 04,GE,GNMD2DJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJQ PGM=SORT       ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJQ
       ;;
(GNMD2DJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GNMD2DAG.BTF500AD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GNMD2DAJ.BTF505AD
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GNMD2DAM.BTF598AD
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/GNMD2DAQ.BTF606AD
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/GNMD2DAX.BTF616AD
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/GNMD2DAT.BTF611AD
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/GNMD2DAT.BTF611BD
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GNMD2DAT.BTF611CD
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/GNMD2DAT.BTF611DD
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/GNMD2DBA.BTF620AD
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GNMD2DBD.BTF625AD
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GNMD2DBG.BTF631AD
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GNMD2DBJ.BTF636AD
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PTEM/GNMD2DBM.BTF641AD
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GNMD2DBQ.BTF646AD
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/GNMD2DBT.BTF651AD
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GNMD2DBX.BTF656AD
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PTEM/GNMD2DCA.BTF662AD
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GNMD2DCD.BTF666AD
       m_FileAssign -d SHR -g ${G_A20} -C ${DATA}/PTEM/GNMD2DCG.BTF671AD
       m_FileAssign -d SHR -g ${G_A21} -C ${DATA}/PTEM/GNMD2DCJ.BTF682AD
       m_FileAssign -d SHR -g ${G_A22} -C ${DATA}/PTEM/GNMD2DCM.BTF686AD
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GNMD2DCQ.BTF687AD
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PTEM/GNMD2DCT.BTF692AD
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GNMD2DCX.BTF702AD
       m_FileAssign -d SHR -g ${G_A26} -C ${DATA}/PTEM/GNMD2DDA.BTF707AD
       m_FileAssign -d SHR -g ${G_A27} -C ${DATA}/PTEM/GNMD2DDD.BTF710AD
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PTEM/GNMD2DDG.BTF715AD
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PTEM/GNMD2DDJ.BTF718AD
       m_FileAssign -d SHR -g ${G_A30} -C ${DATA}/PTEM/GNMD2DDM.BTF720AD
       m_FileAssign -d SHR -g ${G_A31} -C ${DATA}/PTEM/GNMD2DDQ.BTF725AD
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PTEM/GNMD2DDT.BTF751AD
       m_FileAssign -d SHR -g ${G_A33} -C ${DATA}/PTEM/GNMD2DDX.BTF755AD
       m_FileAssign -d SHR -g ${G_A34} -C ${DATA}/PTEM/GNMD2DEA.BTF761AD
       m_FileAssign -d SHR -g ${G_A35} -C ${DATA}/PTEM/GNMD2DED.BTF766AD
       m_FileAssign -d SHR -g ${G_A36} -C ${DATA}/PTEM/GNMD2DEG.BTF770AD
       m_FileAssign -d SHR -g ${G_A37} -C ${DATA}/PTEM/GNMD2DEJ.BTF777AD
       m_FileAssign -d SHR -g ${G_A38} -C ${DATA}/PTEM/GNMD2DEM.BTF782AD
       m_FileAssign -d SHR -g ${G_A39} -C ${DATA}/PTEM/GNMD2DEQ.BTF783AD
       m_FileAssign -d SHR -g ${G_A40} -C ${DATA}/PTEM/GNMD2DET.BTF784AD
       m_FileAssign -d SHR -g ${G_A41} -C ${DATA}/PTEM/GNMD2DEX.BTF785AD
       m_FileAssign -d SHR -g ${G_A42} -C ${DATA}/PTEM/GNMD2DFA.BTF786AD
       m_FileAssign -d SHR -g ${G_A43} -C ${DATA}/PTEM/GNMD2DFD.BTF787AD
       m_FileAssign -d SHR -g ${G_A44} -C ${DATA}/PTEM/GNMD2DFG.BTF788AD
       m_FileAssign -d SHR -g ${G_A45} -C ${DATA}/PTEM/GNMD2DFJ.BTF794AD
       m_FileAssign -d SHR -g ${G_A46} -C ${DATA}/PTEM/GNMD2DFM.BTF795AD
       m_FileAssign -d SHR -g ${G_A47} -C ${DATA}/PTEM/GNMD2DFQ.BTF796AD
       m_FileAssign -d SHR -g ${G_A48} -C ${DATA}/PTEM/GNMD2DFT.BTF797AD
       m_FileAssign -d SHR -g ${G_A49} -C ${DATA}/PTEM/GNMD2DFX.BTF798AD
       m_FileAssign -d SHR -g ${G_A50} -C ${DATA}/PTEM/GNMD2DGA.BTF799AD
       m_FileAssign -d SHR -g ${G_A51} -C ${DATA}/PTEM/GNMD2DGG.BTF805AD
       m_FileAssign -d SHR -g ${G_A52} -C ${DATA}/PTEM/GNMD2DGJ.BTF810AD
       m_FileAssign -d SHR -g ${G_A53} -C ${DATA}/PTEM/GNMD2DGM.BTF815AD
       m_FileAssign -d SHR -g ${G_A54} -C ${DATA}/PTEM/GNMD2DGQ.BTF820AD
       m_FileAssign -d SHR -g ${G_A55} -C ${DATA}/PTEM/GNMD2DGT.BTF825AD
       m_FileAssign -d SHR -g ${G_A56} -C ${DATA}/PTEM/GNMD2DGX.BTF835AD
       m_FileAssign -d SHR -g ${G_A57} -C ${DATA}/PTEM/GNMD2DHA.BTF836AD
       m_FileAssign -d SHR -g ${G_A58} -C ${DATA}/PTEM/GNMD2DHD.BTF837AD
       m_FileAssign -d SHR -g ${G_A59} -C ${DATA}/PTEM/GNMD2DHG.BTF838AD
       m_FileAssign -d SHR -g ${G_A60} -C ${DATA}/PTEM/GNMD2DHJ.BTF839AD
       m_FileAssign -d SHR -g ${G_A61} -C ${DATA}/PTEM/GNMD2DHM.BTF840AD
#         FILE  NAME=BTF841AD,MODE=I                                           
       m_FileAssign -d SHR -g ${G_A62} -C ${DATA}/PTEM/GNMD2DHQ.BTF842AD
       m_FileAssign -d SHR -g ${G_A63} -C ${DATA}/PTEM/GNMD2DHT.BTF843AD
       m_FileAssign -d SHR -g ${G_A64} -C ${DATA}/PTEM/GNMD2DHX.BTF844AD
       m_FileAssign -d SHR -g ${G_A65} -C ${DATA}/PTEM/GNMD2DIA.BTF845AD
       m_FileAssign -d SHR -g ${G_A66} -C ${DATA}/PTEM/GNMD2DID.BTF846AD
       m_FileAssign -d SHR -g ${G_A67} -C ${DATA}/PTEM/GNMD2DIG.BTF847AD
       m_FileAssign -d SHR -g ${G_A68} -C ${DATA}/PTEM/GNMD2DIJ.BTF848AD
       m_FileAssign -d SHR -g ${G_A69} -C ${DATA}/PTEM/GNMD2DIM.BTF849AD
       m_FileAssign -d SHR -g ${G_A70} -C ${DATA}/PTEM/GNMD2DIQ.BTF860AD
       m_FileAssign -d SHR -g ${G_A71} -C ${DATA}/PTEM/GNMD2DIT.BTF862AD
       m_FileAssign -d SHR -g ${G_A72} -C ${DATA}/PTEM/GNMD2DIX.BTF863AD
       m_FileAssign -d SHR -g ${G_A73} -C ${DATA}/PTEM/GNMD2DJA.BTF864AD
       m_FileAssign -d SHR -g ${G_A74} -C ${DATA}/PTEM/GNMD2DJD.BTF865AD
       m_FileAssign -d SHR -g ${G_A75} -C ${DATA}/PTEM/GNMD2DJG.BTF875AD
       m_FileAssign -d SHR -g ${G_A76} -C ${DATA}/PTEM/GNMD2DJJ.BTF877AD
       m_FileAssign -d SHR -g ${G_A77} -C ${DATA}/PTEM/GNMD2DJJ.BTF877BD
       m_FileAssign -d SHR -g ${G_A78} -C ${DATA}/PTEM/GNMD2DJM.BTF878AD
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2DJQ.BTF001XD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2DJR
       ;;
(GNMD2DJR)
       m_CondExec 00,EQ,GNMD2DJQ ${EXAOV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF001AD     NOUVEAU POUR EXCLURE LES ENRS RTXM0            
#   SUR LUI-MEME                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJT
       ;;
(GNMD2DJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P991/SEM.BTF001AD
# ****    MODIF                                                                
       m_FileAssign -d SHR -g +0 SORTOUT ${DATA}/P991/SEM.BTF001AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "X-RTGA59"
 /DERIVEDFIELD CST_1_4 "RTGA56"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_1_8 1 CH 8
 /FIELDS FLD_CH_1_385 1 CH 385
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_8 EQ CST_3_8 
 /KEYS
   FLD_CH_1_385 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2DJU
       ;;
(GNMD2DJU)
       m_CondExec 00,EQ,GNMD2DJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DJX PGM=BTF900     ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DJX
       ;;
(GNMD2DJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A79} FTF600 ${DATA}/PTEM/GNMD2DJQ.BTF001XD
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/P991/SEM.BTF001AD
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PXX0/F91.BTF002AD
       m_ProgramExec BTF900 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER BTF001X      NOUVEAUTE                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKA
       ;;
(GNMD2DKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A80} SORTIN ${DATA}/PTEM/GNMD2DJQ.BTF001XD
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD2DKA.BTF901AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_13 "RTPR14"
 /DERIVEDFIELD CST_3_9 "RTPR12"
 /DERIVEDFIELD CST_1_5 "RTPR10"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_25_8 25 CH 8
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_5 OR FLD_CH_1_6 EQ CST_3_9 OR FLD_CH_1_6 EQ CST_5_13 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_25_8 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2DKB
       ;;
(GNMD2DKB)
       m_CondExec 00,EQ,GNMD2DKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF901 :                                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKD PGM=IKJEFT01   ** ID=APP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKD
       ;;
(GNMD2DKD)
       m_CondExec ${EXAPP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A81} FTF600 ${DATA}/PTEM/GNMD2DKA.BTF901AD
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FTF602 ${DATA}/PTEM/GNMD2DKD.BTF901BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF901 
       JUMP_LABEL=GNMD2DKE
       ;;
(GNMD2DKE)
       m_CondExec 04,GE,GNMD2DKD ${EXAPP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BTF901                                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKG PGM=SORT       ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKG
       ;;
(GNMD2DKG)
       m_CondExec ${EXAPU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A82} SORTIN ${DATA}/PTEM/GNMD2DKD.BTF901BD
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.BTF901CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_133 1 CH 133
 /KEYS
   FLD_CH_1_133 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD2DKH
       ;;
(GNMD2DKH)
       m_CondExec 00,EQ,GNMD2DKG ${EXAPU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMQ910 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKJ PGM=IKJEFT01   ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKJ
       ;;
(GNMD2DKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A83} FMQ910 ${DATA}/PXX0/F91.BTF802AD
       m_FileAssign -d SHR -g ${G_A84} -C ${DATA}/PXX0/F91.BTF002AD
       m_FileAssign -d SHR -g ${G_A85} -C ${DATA}/PXX0/F91.BTF901CD
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/GNMD2D1N
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/GNMD2DKJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ910 
       JUMP_LABEL=GNMD2DKK
       ;;
(GNMD2DKK)
       m_CondExec 04,GE,GNMD2DKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DU FICHIER SEQUENTIEL POUR LIBERER DE LA PLACE                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKM PGM=IDCAMS     ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKM
       ;;
(GNMD2DKM)
       m_CondExec ${EXAQE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2DKM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2DKN
       ;;
(GNMD2DKN)
       m_CondExec 16,NE,GNMD2DKM ${EXAQE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER BTF001XD POUR COMPARAISON DU LENDEMAIN                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD2DKQ PGM=IEBGENER   ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DKQ
       ;;
(GNMD2DKQ)
       m_CondExec ${EXAQJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A86} SYSUT1 ${DATA}/PTEM/GNMD2DJQ.BTF001XD
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SYSUT2 ${DATA}/P991/SEM.BTF001AD
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GNMD2DKR
       ;;
(GNMD2DKR)
       m_CondExec 00,EQ,GNMD2DKQ ${EXAQJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD2DZA
       ;;
(GNMD2DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD2DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
