#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT99D.ksh                       --- VERSION DU 08/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDIVT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/08/12 AT 10.24.55 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT99D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  UNLOAD DES TABLES RTIT 00 05 10 15                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT99DA
       ;;
(IVT99DA)
#
#IVT99DAD
#IVT99DAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT99DAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=IVT99DAA
       ;;
(IVT99DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#    RSIT00   : NAME=RSIT00D,MODE=I - DYNAM=YES                                
#    RSIT05   : NAME=RSIT05D,MODE=I - DYNAM=YES                                
#    RSIT10   : NAME=RSIT10D,MODE=I - DYNAM=YES                                
#    RSIT15   : NAME=RSIT15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,CATLG -r 45 -g +1 SYSREC01 ${DATA}/PXX0/F91.UNIT00AD
       m_FileAssign -d NEW,CATLG,CATLG -r 21 -g +1 SYSREC02 ${DATA}/PXX0/F91.UNIT05AD
       m_FileAssign -d NEW,CATLG,CATLG -r 17 -g +1 SYSREC03 ${DATA}/PXX0/F91.UNIT10AD
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -g +1 SYSREC04 ${DATA}/PXX0/F91.UNIT15AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99DAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  QUIESCE DES TABLES RTIT00 05 10 15                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99DAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVT99DAG
       ;;
(IVT99DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE PARAMETRES GENERAUX D'INVENTAIRE                                      
#    RSIT00   : NAME=RSIT00D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT00 /dev/null
#  TABLE LIEUX INVENTORIES                                                     
#    RSIT05R  : NAME=RSIT05D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT05R /dev/null
#  TABLE FAMILLES INVENTORIES                                                  
#    RSIT10   : NAME=RSIT10D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT10 /dev/null
#  TABLE CODICS A INVENTORIER                                                  
#    RSIT15   : NAME=RSIT15D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15 /dev/null
#  TABLE GENERALISEE                                                           
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/IVT99DAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT020 
       JUMP_LABEL=IVT99DAH
       ;;
(IVT99DAH)
       m_CondExec 04,GE,IVT99DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSIT00                                        
#   REPRISE: NON BACKOUT CORTEX RIVT99D                                        
#            VERIFIER LE BACKOUT RIVT99D                                       
#            REPRISE FORCEE EN DEBUT DE CHAINE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99DAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT99DZA
       ;;
(IVT99DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
