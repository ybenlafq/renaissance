#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV305P.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV305 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/12/31 AT 11.54.02 BY BURTECC                      
#    STANDARDS: P  JOBSET: GV305P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='PARIS'                                                             
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV305PA
       ;;
(GV305PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV305PAA
       ;;
(GV305PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSAN00   : NAME=RSAN00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSAN00 /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV305PAA
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/GV305PAA.BEX111GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV305PAB
       ;;
(GV305PAB)
       m_CondExec 04,GE,GV305PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAD
       ;;
(GV305PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV305PAA.BEX111GP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/GV305PAD.BEX111HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305PAE
       ;;
(GV305PAE)
       m_CondExec 00,EQ,GV305PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BGV305  EXTRACTION DES VENTES DES 4 DERNIERES SEMAINES               
#       RTHV04 SELON LA SEGMENTATION PRESENTE DANS LE FICHIER FEX001           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAG
       ;;
(GV305PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
#                                                                              
#    RSHV84   : NAME=RSHV84,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV84 /dev/null
#                                                                              
# ***CARTE SOCIETE                                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A2} FEX001 ${DATA}/PTEM/GV305PAD.BEX111HP
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 FGV305 ${DATA}/PTEM/GV305PAG.BGV305GP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV305 
       JUMP_LABEL=GV305PAH
       ;;
(GV305PAH)
       m_CondExec 04,GE,GV305PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR MAGASIN                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAJ
       ;;
(GV305PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GV305PAG.BGV305GP
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305PAJ.BGV305HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305PAK
       ;;
(GV305PAK)
       m_CondExec 00,EQ,GV305PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR ZONE                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAM
       ;;
(GV305PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GV305PAG.BGV305GP
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305PAM.BGV305JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_CH_7_2 7 CH 2
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_7_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305PAN
       ;;
(GV305PAN)
       m_CondExec 00,EQ,GV305PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI CUMMUL DU FICHIER FGV305 PAR GROUPE                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAQ
       ;;
(GV305PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GV305PAG.BGV305GP
       m_FileAssign -d NEW,CATLG,DELETE -r 225 -g +1 SORTOUT ${DATA}/PTEM/GV305PAQ.BGV305KP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_154_6 154 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_170_6 170 PD 6
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_164_6 164 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_54_6 54 PD 6
 /FIELDS FLD_PD_138_4 138 PD 4
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_148_6 148 PD 6
 /FIELDS FLD_PD_182_4 182 PD 4
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_208_6 208 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_CH_9_2 9 CH 2
 /FIELDS FLD_PD_132_6 132 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_186_6 186 PD 6
 /FIELDS FLD_PD_220_6 220 PD 6
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_176_6 176 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_214_6 214 PD 6
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_192_6 192 PD 6
 /FIELDS FLD_PD_204_4 204 PD 4
 /FIELDS FLD_PD_198_6 198 PD 6
 /FIELDS FLD_CH_41_6 41 CH 6
 /FIELDS FLD_PD_50_4 50 PD 4
 /FIELDS FLD_PD_11_4 11 PD 4
 /FIELDS FLD_PD_160_4 160 PD 4
 /FIELDS FLD_PD_110_6 110 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_9_2 ASCENDING,
   FLD_PD_11_4 ASCENDING,
   FLD_CH_41_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_50_4,
    TOTAL FLD_PD_54_6,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_6,
    TOTAL FLD_PD_138_4,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_6,
    TOTAL FLD_PD_154_6,
    TOTAL FLD_PD_160_4,
    TOTAL FLD_PD_164_6,
    TOTAL FLD_PD_170_6,
    TOTAL FLD_PD_176_6,
    TOTAL FLD_PD_182_4,
    TOTAL FLD_PD_186_6,
    TOTAL FLD_PD_192_6,
    TOTAL FLD_PD_198_6,
    TOTAL FLD_PD_204_4,
    TOTAL FLD_PD_208_6,
    TOTAL FLD_PD_214_6,
    TOTAL FLD_PD_220_6
 /* Record Type = F  Record Length = 225 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV305PAR
       ;;
(GV305PAR)
       m_CondExec 00,EQ,GV305PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DES ETATS IGV015 ET IGV018                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAT
       ;;
(GV305PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#                                                                              
# ******* FICHIER TRI� PAR MAGASIN                                             
       m_FileAssign -d SHR -g ${G_A6} FGV305 ${DATA}/PTEM/GV305PAJ.BGV305HP
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305PAT
#  FICHIERS EDITION                                                            
       m_OutputAssign -c Z -w IGV015 IGV015S
       m_OutputAssign -c Z -w IGV018 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305PAU
       ;;
(GV305PAU)
       m_CondExec 04,GE,GV305PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV016                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV305PAX
       ;;
(GV305PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#                                                                              
# ******* FICHIER TRI� PAR ZONE                                                
       m_FileAssign -d SHR -g ${G_A7} FGV305 ${DATA}/PTEM/GV305PAM.BGV305JP
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305PAX
#  FICHIERS EDITION                                                            
       m_OutputAssign -c Z -w IGV016 IGV015S
       m_OutputAssign -c Z -w IGV016 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305PAY
       ;;
(GV305PAY)
       m_CondExec 04,GE,GV305PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV315                                                                
#  GENERATION DE L'ETAT IGV017                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV305PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV305PBA
       ;;
(GV305PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#                                                                              
# ******* FICHIER TRI� PAR GRP DE MAGASINS                                     
       m_FileAssign -d SHR -g ${G_A8} FGV305 ${DATA}/PTEM/GV305PAQ.BGV305KP
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV305PBA
#  FICHIERS EDITION                                                            
       m_OutputAssign -c 9 -w IGV017 IGV015S
       m_OutputAssign -c 9 -w IGV017 IGV015M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV315 
       JUMP_LABEL=GV305PBB
       ;;
(GV305PBB)
       m_CondExec 04,GE,GV305PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV305PZA
       ;;
(GV305PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV305PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
