#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV015L.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/14 AT 16.54.31 BY BURTEC4                      
#    STANDARDS: P  JOBSET: GV015L                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DNPC'                                                              
# ********************************************************************         
#  TRI FIC FHV02 VENANT DE GV135L                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV015LA
       ;;
(GV015LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV015LAA
       ;;
(GV015LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    TABLE COMMERCIAL                  *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BGV145CL.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/GV015LAA.BGV015GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_11_8 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015LAB
       ;;
(GV015LAB)
       m_CondExec 00,EQ,GV015LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV004                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAD PGM=BGV004     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAD
       ;;
(GV015LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FICHIER ENTRE FHV02 TRI�                                             
       m_FileAssign -d SHR -g ${G_A1} FHV02 ${DATA}/PXX0/GV015LAA.BGV015GL
#                                                                              
# ******* FICHIER SORTIE DE LRECL 80                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FICDAT ${DATA}/PXX0/GV015LAD.BGV014AL
       m_ProgramExec BGV004 
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAG
       ;;
(GV015LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00L /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV015LAG
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PXX0/GV015LAG.BEX011GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV015LAH
       ;;
(GV015LAH)
       m_CondExec 04,GE,GV015LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAJ
       ;;
(GV015LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV015LAG.BEX011GL
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/GV015LAJ.BEX011HL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015LAK
       ;;
(GV015LAK)
       m_CondExec 00,EQ,GV015LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FIC FHV02 VENANT DE GV135L SUR SOCIETE ; CODIC ; DATE                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAM
       ;;
(GV015LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BGV145CL.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/GV015LAM.BGV015JL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_18 1 CH 18
 /KEYS
   FLD_CH_1_18 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015LAN
       ;;
(GV015LAN)
       m_CondExec 00,EQ,GV015LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV016   CALCUL DES VENTES DU MOIS SELON LA SEGMENTATION              
#         VOULUE                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAQ PGM=BGV016     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAQ
       ;;
(GV015LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FICDAT ${DATA}/PXX0/GV015LAD.BGV014AL
#                                                                              
# **  FICHIER FEX001 TRI�                                                      
       m_FileAssign -d SHR -g ${G_A4} FEX001 ${DATA}/PXX0/GV015LAJ.BEX011HL
#                                                                              
# **  FICHIER FHV02 TRI�                                                       
       m_FileAssign -d SHR -g ${G_A5} FHV02 ${DATA}/PXX0/GV015LAM.BGV015JL
#                                                                              
# **  FICHIER DE SORTIE DE LRECL 87                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 FGV016 ${DATA}/PXX0/GV015LAQ.BGV016AL
       m_ProgramExec BGV016 
# ********************************************************************         
#   PGM : BGV015  SI LA DATE FICDAT EST POSTERIEUR AU DEBUT DE MOIS            
#       DE L ANNEE PRECEDENTE LES DONNEES HISTORIQUES SONT EXTRAITES           
#       PAR FAMILLES DEMANDEES PAR L ETAT IGV020                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAT
       ;;
(GV015LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA09L  : NAME=RSGA09L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09L /dev/null
#                                                                              
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#                                                                              
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
#                                                                              
#    RSGA20L  : NAME=RSGA20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20L /dev/null
#                                                                              
#    RSGA21L  : NAME=RSGA21L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21L /dev/null
#                                                                              
#    RSHV08L  : NAME=RSHV08L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV08L /dev/null
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A6} FICDAT ${DATA}/PXX0/GV015LAD.BGV014AL
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 FGV015 ${DATA}/PXX0/GV015LAT.BGV015KL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV015 
       JUMP_LABEL=GV015LAU
       ;;
(GV015LAU)
       m_CondExec 04,GE,GV015LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIER FGV015 ET FGV016                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABJ      SORT  SORT=(00087,300000)                                           
#                                                                              
# ***********************************                                          
# *   STEP GV015LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV015LAX
       ;;
(GV015LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/GV015LAQ.BGV016AL
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PXX0/GV015LAT.BGV015KL
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 SORTOUT ${DATA}/PXX0/GV015LAX.BGV015LL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_66_6 66 PD 6
 /KEYS
   FLD_CH_1_39 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6
 /* Record Type = F  Record Length = 87 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015LAY
       ;;
(GV015LAY)
       m_CondExec 00,EQ,GV015LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV020                                                                
#  EDITION ETAT SUIVI DES VENTES HEBDO                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV015LBA
       ;;
(GV015LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11L  : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11L /dev/null
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR -g ${G_A9} FGV015T ${DATA}/PXX0/GV015LAX.BGV015LL
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FMOISJ
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
#                                                                              
       m_OutputAssign -c 9 -w BGV020 IGV020
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV020 
       JUMP_LABEL=GV015LBB
       ;;
(GV015LBB)
       m_CondExec 04,GE,GV015LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV015LZA
       ;;
(GV015LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV015LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
