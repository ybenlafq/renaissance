#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV055P.ksh                       --- VERSION DU 09/10/2016 00:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV055 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/05/25 AT 10.20.59 BY BURTECR                      
#    STANDARDS: P  JOBSET: GV055P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BGV055  CREE FIC VENTES REALISEES SUR 6 MOIS                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV055PA
       ;;
(GV055PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV055PAA
       ;;
(GV055PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSHV10   : NAME=RSHV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV10 /dev/null
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL05 /dev/null
#    RSFL06   : NAME=RSFL06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL06 /dev/null
#    RSGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ05 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 112 -g +1 FGV055 ${DATA}/PXX0/GV055PAA.GV0055AP
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV055 
       JUMP_LABEL=GV055PAB
       ;;
(GV055PAB)
       m_CondExec 04,GE,GV055PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5CODE FAMILLE 13,20LIBELLE MARQUE           
#   33,7 CODE ARTICLE                                                          
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV055PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV055PAD
       ;;
(GV055PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GV055PAA.GV0055AP
       m_FileAssign -d NEW,CATLG,DELETE -r 112 -g +1 SORTOUT ${DATA}/PXX0/GV055PAD.GV0055BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_CH_33_7 33 CH 7
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_PD_92_5 92 PD 5
 /FIELDS FLD_PD_85_7 85 PD 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_97_7 97 PD 7
 /FIELDS FLD_CH_13_20 13 CH 20
 /FIELDS FLD_PD_78_7 78 PD 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_104_7 104 PD 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_20 ASCENDING,
   FLD_CH_33_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_5,
    TOTAL FLD_PD_78_7,
    TOTAL FLD_PD_85_7,
    TOTAL FLD_PD_92_5,
    TOTAL FLD_PD_97_7,
    TOTAL FLD_PD_104_7
 /* Record Type = F  Record Length = 112 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV055PAE
       ;;
(GV055PAE)
       m_CondExec 00,EQ,GV055PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   1,3 SOC 4,3 SEQUENCE EDITION 7,5CODE FAMILLE 13,20LIBELLE MARQUE           
#   111,1     92,5 CODE ARTICLE                                                
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV055PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV055PAG
       ;;
(GV055PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV055PAD.GV0055BP
       m_FileAssign -d NEW,CATLG,DELETE -r 112 -g +1 SORTOUT ${DATA}/PAS0/F07.GV0056AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_13_20 13 CH 20
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_PD_92_5 92 PD 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_5 ASCENDING,
   FLD_CH_13_20 ASCENDING,
   FLD_CH_111_1 ASCENDING,
   FLD_PD_92_5 DESCENDING
 /* Record Type = F  Record Length = 112 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV055PAH
       ;;
(GV055PAH)
       m_CondExec 00,EQ,GV055PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV060  EDITION                                                *            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV055PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV055PAJ
       ;;
(GV055PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
       m_FileAssign -d SHR -g ${G_A3} FGV056T ${DATA}/PAS0/F07.GV0056AP
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_OutputAssign -c 9 -w BGV060 IGV060
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV060 
       JUMP_LABEL=GV055PAK
       ;;
(GV055PAK)
       m_CondExec 04,GE,GV055PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV055PZA
       ;;
(GV055PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV055PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
