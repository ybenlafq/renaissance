#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV510O.ksh                       --- VERSION DU 08/10/2016 13:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGV510 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/17 AT 08.56.26 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV510O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BGV514  POSITIONNE UN FLAG SUR LES ENTETES DE VENTE NON NEM A EPURE         
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV510OA
       ;;
(GV510OA)
#
#GV510OAD
#GV510OAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV510OAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV510OAA
       ;;
(GV510OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d OLD,KEEP,DELETE -g +0 MQFILE ${DATA}/PXX0/F07.MQFILEAP
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV10O  : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510OAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV514 
       JUMP_LABEL=GV510OAB
       ;;
(GV510OAB)
       m_CondExec 04,GE,GV510OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QUIESCE DU  TABLESPACE : RSMQ15O                                            
#  SI BESOIN DE RECOVER PRENDRE LA VALEUR DU "RBA" DE CE STEP                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510OAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV510OAG
       ;;
(GV510OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRAGE PGM                                                      
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510OAG
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX3/GV510OAG
#                                                                              
# ******  TABLE DE COMPTE-RENDU DES REMONT�ES MAGASINS                         
#    RSMQ15O  : NAME=RSMQ15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSMQ15O /dev/null
# ******  FICHIER LG 19 EPURATION DES RESERVATIONS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FTS053 ${DATA}/PXX0/F16.BNM153AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMQ100 
       JUMP_LABEL=GV510OAH
       ;;
(GV510OAH)
       m_CondExec 04,GE,GV510OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV517  SUPPRESSION DANS LA TABLE RTGV10                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV510OAJ
       ;;
(GV510OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10O  : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSNV20   : NAME=RSNV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSNV20 /dev/null
# ******  FICHIER MAGASIN NEM                                                  
       m_FileAssign -d SHR -g ${G_A1} FVTENP ${DATA}/PXX0/F16.BNM153AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV517 
       JUMP_LABEL=GV510OAK
       ;;
(GV510OAK)
       m_CondExec 04,GE,GV510OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV516   ENVOI MESSAGE MQSERIES DANS QUEUE LOCALE HOST                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV510OAM
       ;;
(GV510OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10O  : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/GV510OAM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV516 
       JUMP_LABEL=GV510OAN
       ;;
(GV510OAN)
       m_CondExec 04,GE,GV510OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV515  DERESERVATION STOCKS ET QUOTAS + SUPPRESSION SUR TABLES             
#          MOUCHARD DES VENTES NON VALIDEES DONT LA DATE DE VENTE EST          
#          < OU = A LA DATE DU TRAITEMENT MOINS N JOURS                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510OAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV510OAQ
       ;;
(GV510OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE MOUCHARD DES RESERVATIONS STOCKS                               
#    RSGV40O  : NAME=RSGV40O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV40O /dev/null
# ******  TABLE MOUCHARD DES RESERVATIONS QUOTAS                               
#    RSGV41O  : NAME=RSGV41O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV41O /dev/null
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  TABLE DES EN-TETES DE MUTATION                                       
#    RSGB05   : NAME=RSGB05,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  TABLE DES MUTATIONS DETAIL                                           
#    RSGB15   : NAME=RSGB15,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
# ******  TABLE DES LIGNES COMMANDES SPECIFIQUES                               
#    RSGF00   : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00 /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (EMPORTE)                             
#    RSGQ02O  : NAME=RSGQ02O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ02O /dev/null
# ******  TABLE DES QUOTAS DE DELIVRANCE (LIVRE)                               
#    RSGQ03O  : NAME=RSGQ03O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ03O /dev/null
# ******  TABLE DES STOCKS ENTREPOT                                            
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE DU JUSTIFICATIF DU LIEU DE TRANSIT                             
#    RSGS20   : NAME=RSGS20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS20 /dev/null
# ******  TABLE DES STOCKS MAGASINS                                            
#    RSGS30O  : NAME=RSGS30O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  TABLE DES                                                            
#    RSGS43   : NAME=RSGS43,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS43 /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS ENTREPOT                           
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
# ******  TABLE DES RESERVATIONS SUR STOCKS MAGASINS                           
#    RSGV22   : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
# ******  TABLE VENTES                                                         
#    RSGV10O  : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00O  : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******  TABLE DES LIAISONS ARTICLES                                          
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CARTE FNSOC                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  DELAI DE PURGE                                                       
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510OAQ
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV515 
       JUMP_LABEL=GV510OAR
       ;;
(GV510OAR)
       m_CondExec 04,GE,GV510OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV518  SUPPRIME LES VENTES NON VALIDEES (NON ENCAISSEES)                   
#          DONT LA DATE DE VENTE EST                                           
#          < OU = A LA DATE DU TRAITEMENT MOIS N JOURS                         
#          EX : LE CLIENT N EST PAS PASSE EN CAISSE CAR TROP DE MONDE          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV510OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV510OAT
       ;;
(GV510OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02O  : NAME=RSGV02O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02O /dev/null
# ******  TABLE GV03                                                           
#    RSGV03O  : NAME=RSGV03O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV03O /dev/null
# ******  TABLE DES EN-TETES DE VENTES                                         
#    RSGV10O  : NAME=RSGV10O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10O /dev/null
# ******  TABLE DES ARTICLES DES VENTES                                        
#    RSGV11O  : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  TABLE DES PRESTATIONS DE VENTES                                      
#    RSGV13O  : NAME=RSGV13O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV13O /dev/null
# ******  TABLE DES CARACTERISTIQUES ARTICLES DES VENTES                       
#    RSGV15O  : NAME=RSGV15O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV15O /dev/null
# ******  TABLE DES VENTES                                                     
#    RSGV20O  : NAME=RSGV20O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV20O /dev/null
# ******  TABLE DES VENTES                                                     
#    RSGV23O  : NAME=RSGV23O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV23O /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSGV27O  : NAME=RSGV27O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV27O /dev/null
# ******  TABLE DES VENTES : TABLE MOUCHARD                                    
#    RSPS00O  : NAME=RSPS00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSPS00O /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 916                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  DELAI DE PURGE                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV510OAT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV518 
       JUMP_LABEL=GV510OAU
       ;;
(GV510OAU)
       m_CondExec 04,GE,GV510OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
