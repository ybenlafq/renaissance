#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PO001P.ksh                       --- VERSION DU 08/10/2016 22:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPO001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/19 AT 11.31.38 BY BURTECA                      
#    STANDARDS: P  JOBSET: PO001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPO001X -- CREATION DU FICHIER D'EXTRACTION DES BONS D'ECHANG         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PO001PA
       ;;
(PO001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PO001PAA
       ;;
(PO001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# ***************************************************************              
#  TRI DU FICHIER ISSU DE LA CHAINE GS100P (FTS054)                            
#  REPRISE : OUI                                                               
# ***************************************************************              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BNM154AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BNM154AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BNM154AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BNM154AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BNM154AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BNM154AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BNM154AY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FPO001BP
# **************************************                                       
# **       PROGRAMME BPO001          ***                                       
#  REPRISE : OUI                                                               
# **************************************                                       
#                                                                              
# ***********************************                                          
# *   STEP PO001PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_32_7 32 CH 7
 /FIELDS FLD_CH_18_8 18 CH 8
 /FIELDS FLD_CH_124_13 124 CH 13
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_124_13 ASCENDING,
   FLD_CH_32_7 ASCENDING,
   FLD_CH_18_8 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PO001PAD
       ;;
(PO001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE LECTURE                                                        
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11X  : NAME=RSGV11X,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11X /dev/null
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
#    RSPO01   : NAME=RSPO01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPO01 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
# *****   FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A1} FWS054 ${DATA}/PXX0/F07.FPO001BP
# *****   FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FPO001 ${DATA}/PXX0/F07.FPO001AP
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER PARAMETRE DATE DE DERNIER PASSAGE                            
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PXX0/F07.PO001PP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPO001 
       JUMP_LABEL=PO001PAE
       ;;
(PO001PAE)
       m_CondExec 04,GE,PO001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **             REPRO DE LA CARTE FDATE EN FPARAM                   *         
# **         POUR CR�ATION FICHIER DATE DE DERNIER PASSAGE           *         
# ** REPRISE : OUI                                                   *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PO001PAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PO001PAG
       ;;
(PO001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.PO001PP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PO001PAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PO001PAH
       ;;
(PO001PAH)
       m_CondExec 16,NE,PO001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DES FICHIERS VERS AS400 SAV                                           
#  REPRISE : OUI                                                               
# *******************************************************************          
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FPO001AP,                                                           
#      FNAME=":FPO001AP""(0)                                                   
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTPO001P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PO001PAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PO001PAJ
       ;;
(PO001PAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/PO001PAJ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP PO001PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PO001PAM
       ;;
(PO001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PO001PAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  PGM : BPO003 -- MAJ RTGA30 TOUTES FILILAES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PO001PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PO001PAQ
       ;;
(PO001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN MAJ                                                         
#    RSGA30   : NAME=RSGA30,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
#    RSGA30   : NAME=RSGA30X,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPO003 
       JUMP_LABEL=PO001PAR
       ;;
(PO001PAR)
       m_CondExec 04,GE,PO001PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
