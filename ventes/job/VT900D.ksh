#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VT900D.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDVT900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/05/28 AT 18.07.10 BY BURTEC2                      
#    STANDARDS: P  JOBSET: VT900D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTGV02-05-06-08-10-11-14-15-20-23-27-41                  
#  REPRISE: OUI                                                                
#  MODIF  LE 13/09/06 PJU RAJOUT PRED SUR MU600P CAR SI DéCALAGE DE            
#         MU600P (CHAINE GROUPE ) IL SE PEUT QUE LE RELOAD FAIT PAR LE         
#         VV900* ECRASE LES MAJ FAITES PAR LE MU600P ==> IMPACT ==>            
#         GéNéRATION DE MVTS DE STOCKS EN DOUBLE !!                            
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VT900DA
       ;;
(VT900DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VT900DAA
       ;;
(VT900DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PARG/SEM.VT900DA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VT900DAA
       m_ProgramExec IEFBR14 "RDAR,VT900D.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT900DAD
       ;;
(VT900DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VT900DA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VT900DAE
       ;;
(VT900DAE)
       m_CondExec 00,EQ,VT900DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTGV11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAG
       ;;
(VT900DAG)
       m_CondExec ${EXAAK},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSGV10D  : NAME=RSGV10D,MODE=I - DYNAM=YES                                
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
#    RSGV14D  : NAME=RSGV14D,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/VT900DAG.VT900D01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900DAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTGV11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAJ
       ;;
(VT900DAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSGV11D  : NAME=RSGV11D,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/VT900DAJ.VT900D0
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900DAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FIC SUR NLIEU,NVENTE 4,10,A                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAM
       ;;
(VT900DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VT900DAG.VT900D01
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900DAM.VT900D02
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900DAN
       ;;
(VT900DAN)
       m_CondExec 00,EQ,VT900DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9000                                                               
#   PREPARATION FICHIER PURGE ET FICHIER DE LOAD RTGV11                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAQ
       ;;
(VT900DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#         TABLE RTGV11                                                         
#    RSGV11D  : NAME=RSGV11D,MODE=(U,N) - DYNAM=YES                            
# -X-VT900DR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSGV11D /dev/null
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  RTGV11 TRI PAR LIEU ET NVENTE                                               
       m_FileAssign -d SHR -g ${G_A3} VT0011A ${DATA}/PTEM/VT900DAM.VT900D02
#  FICHIER POUR LE LOAD DE LA RTGV11                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 VT0011L ${DATA}/PARG/SEM.VT900DB
#  FICHIER DE PURGE DE LA RTGV11                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -t LSEQ -g +1 VT0011P ${DATA}/PARG/SEM.VT900DC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9000 
       JUMP_LABEL=VT900DAR
       ;;
(VT900DAR)
       m_CondExec 04,GE,VT900DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTGV11                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAT
       ;;
(VT900DAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSGV11D  : NAME=RSGV11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11D /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PARG/SEM.VT900DB
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/VT900DAJ.VT900D0
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900DAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VT900D_VT900DAT_RTGV11.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT900DAU
       ;;
(VT900DAU)
       m_CondExec 04,GE,VT900DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT910                                                                
#  EPURATION DES TABLES RTGV10 RTGV11                                          
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VT900DAX
       ;;
(VT900DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ENTETES DE VENTES                                                 
#    RSGV10D  : NAME=RSGV10D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10D /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RSGV11D  : NAME=RSGV11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11D /dev/null
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FVT910 ${DATA}/PARG/SEM.VT900DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT910 
       JUMP_LABEL=VT900DAY
       ;;
(VT900DAY)
       m_CondExec 04,GE,VT900DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DE PURGE POUR LE PASSER A UN LRECL = 20                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBA
       ;;
(VT900DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PARG/SEM.VT900DC
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900DBA.VT900D03
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_52_3 52 PD 3
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_52_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900DBB
       ;;
(VT900DBB)
       m_CondExec 00,EQ,VT900DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BVT910 ET DU TRI PRECEDENT POUR CREER            
#  LE FICHIER VT900DE                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBD
       ;;
(VT900DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/VT900DBA.VT900D03
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PARG/SEM.VT900DD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PARG/SEM.VT900DE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900DBE
       ;;
(VT900DBE)
       m_CondExec 00,EQ,VT900DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9002                                                               
#  PURGE DE LA TABLE RTGV02 TABLE DES ADRESSES                                 
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBG
       ;;
(VT900DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENTES ADRESSES                                                   
#    RSGV02D  : NAME=RSGV02D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV02D /dev/null
#                                                                              
#    RSTL11D  : NAME=RSTL11D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL11D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBG
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A9} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -t LSEQ -g +1 FVT020 ${DATA}/PARG/SEM.VT900DF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9002 
       JUMP_LABEL=VT900DBH
       ;;
(VT900DBH)
       m_CondExec 04,GE,VT900DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9005                                                               
#  PURGE DE LA TABLE RTGV05                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBJ
       ;;
(VT900DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV05D  : NAME=RSGV05D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV05D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBJ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A10} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -t LSEQ -g +1 FVT005 ${DATA}/PARG/SEM.VT900DG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9005 
       JUMP_LABEL=VT900DBK
       ;;
(VT900DBK)
       m_CondExec 04,GE,VT900DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9006                                                               
#  PURGE DE LA TABLE RTGV06                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBM
       ;;
(VT900DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV06D  : NAME=RSGV06D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV06D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBM
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A11} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 FVT006 ${DATA}/PARG/SEM.VT900DH
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9006 
       JUMP_LABEL=VT900DBN
       ;;
(VT900DBN)
       m_CondExec 04,GE,VT900DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9008                                                               
#  PURGE DE LA TABLE RTGV08                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBQ
       ;;
(VT900DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSGV08D  : NAME=RSGV08D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV08D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A12} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -t LSEQ -g +1 FVT008 ${DATA}/PARG/SEM.VT900DI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9008 
       JUMP_LABEL=VT900DBR
       ;;
(VT900DBR)
       m_CondExec 04,GE,VT900DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9014                                                               
#  PURGE DE LA TABLE DES REGLEMENTS                                            
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBT
       ;;
(VT900DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES REGLEMENTS                                                        
#    RSGV14D  : NAME=RSGV14D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV14D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBT
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A13} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -t LSEQ -g +1 FVT140 ${DATA}/PARG/SEM.VT900DJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9014 
       JUMP_LABEL=VT900DBU
       ;;
(VT900DBU)
       m_CondExec 04,GE,VT900DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9015                                                               
#  PURGE DE LA TABLE RTGV15                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VT900DBX
       ;;
(VT900DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES CARACTERISTIQUES SPECIFIQUES                                      
#    RSGV15D  : NAME=RSGV15D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV15D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DBX
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A14} FVT900 ${DATA}/PARG/SEM.VT900DE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9015 
       JUMP_LABEL=VT900DBY
       ;;
(VT900DBY)
       m_CondExec 04,GE,VT900DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9023                                                               
#  PURGE DE LA TABLE RTGV23                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCA
       ;;
(VT900DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV23D  : NAME=RSGV23D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV23D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DCA
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A15} FVT900 ${DATA}/PARG/SEM.VT900DE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9023 
       JUMP_LABEL=VT900DCB
       ;;
(VT900DCB)
       m_CondExec 04,GE,VT900DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9010                                                               
#  PURGE DE LA TABLE RTGV23                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCD
       ;;
(VT900DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV10D  : NAME=RSGV10D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DCD
#  FICHIER                                                                     
       m_FileAssign -d SHR -g ${G_A16} FVT900 ${DATA}/PARG/SEM.VT900DE
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FVT901 ${DATA}/PARG/SEM.VT900DK
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -t LSEQ -g +1 FVT100 ${DATA}/PARG/SEM.VT900DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9010 
       JUMP_LABEL=VT900DCE
       ;;
(VT900DCE)
       m_CondExec 04,GE,VT900DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9027                                                               
#  PURGE DE LA TABLE DES AUTORISATIONS ENTREPOT                                
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCG
       ;;
(VT900DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES AUTORISATIONS ENTREPOT                                            
#    RSGV27D  : NAME=RSGV27D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV27D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DCG
#  FICHIER POUR LES PURGES DE LA TABLE RTGV27                                  
       m_FileAssign -d SHR -g ${G_A17} FVT901 ${DATA}/PARG/SEM.VT900DK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9027 
       JUMP_LABEL=VT900DCH
       ;;
(VT900DCH)
       m_CondExec 04,GE,VT900DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9041                                                               
#  PURGE DE LA TABLE RTGV14                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCJ
       ;;
(VT900DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES MOUCHARDS DE QUOTAS                                               
#    RSGV41D  : NAME=RSGV41D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV41D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DCJ
#  FICHIER FEPURE                                                              
       m_FileAssign -d SHR -g ${G_A18} FVT901 ${DATA}/PARG/SEM.VT900DK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9041 
       JUMP_LABEL=VT900DCK
       ;;
(VT900DCK)
       m_CondExec 04,GE,VT900DCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER VT900DE                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCM
       ;;
(VT900DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PARG/SEM.VT900DE
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/VT900DCM.VT900D04
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT900DCN
       ;;
(VT900DCN)
       m_CondExec 00,EQ,VT900DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVT9020                                                               
#  PURGE DE LA TABLES HISTORIQUE ENTETE DE VENTES                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCQ
       ;;
(VT900DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSGV20D  : NAME=RSGV20D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV20D /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VT900DCQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A20} FVT900 ${DATA}/PTEM/VT900DCM.VT900D04
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT9020 
       JUMP_LABEL=VT900DCR
       ;;
(VT900DCR)
       m_CondExec 04,GE,VT900DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE VT900DB POUR LOAD RTGV11 CAR PAS DE PLACE SUR VOLUMES S1P*           
#  REPRISE AU STEP : OUI                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT900DCT PGM=IDCAMS     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=VT900DCT
       ;;
(VT900DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900DCT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=VT900DCU
       ;;
(VT900DCU)
       m_CondExec 16,NE,VT900DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VT900DZA
       ;;
(VT900DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT900DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
