#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV512P.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPV512 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/10/07 AT 14.51.34 BY BURTEC7                      
#    STANDARDS: P  JOBSET: PV512P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV506                                                                
#  EXTRACTION DES VENTES SANS ADRESSE                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV512PA
       ;;
(PV512PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV512PAA
       ;;
(PV512PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   VUE RVPV0077 (ENSEMBLE DES RTPV00 FILIALES)                          
#    RSPV00   : NAME=RSPV00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPV00 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV512PAA
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 FPV506 ${DATA}/PTEM/PV512PAA.BPV506AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV506 
       JUMP_LABEL=PV512PAB
       ;;
(PV512PAB)
       m_CondExec 04,GE,PV512PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV512                                                                
#  EXTRACTION DES VENTES SANS ADRESSE                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV512PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV512PAD
       ;;
(PV512PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER PARAMETRE                                                    
       m_FileAssign -i FDATE
$FDATE
_end
# *****   MMAANN                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FETAT ${DATA}/CORTEX4.P.MTXTFIX1/PV512PAD
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} FPV500 ${DATA}/PTEM/PV512PAA.BPV506AP
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IPV512 ${DATA}/PTEM/PV512PAD.BPV512AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV512 
       JUMP_LABEL=PV512PAE
       ;;
(PV512PAE)
       m_CondExec 04,GE,PV512PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BPV512                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV512PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV512PAG
       ;;
(PV512PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PV512PAD.BPV512AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/PV512PAG.BPV512BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_70_5 70 CH 5
 /FIELDS FLD_PD_13_3 13 PD 03
 /FIELDS FLD_BI_18_2 18 CH 02
 /FIELDS FLD_CH_45_5 45 CH 5
 /FIELDS FLD_CH_40_5 40 CH 5
 /FIELDS FLD_BI_10_3 10 CH 03
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_BI_16_2 16 CH 02
 /FIELDS FLD_CH_65_5 65 CH 5
 /FIELDS FLD_CH_80_5 80 CH 5
 /FIELDS FLD_CH_75_5 75 CH 5
 /FIELDS FLD_CH_50_5 50 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_BI_7_3 07 CH 03
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_3 ASCENDING,
   FLD_PD_13_3 ASCENDING,
   FLD_BI_16_2 ASCENDING,
   FLD_BI_18_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_40_5,
    TOTAL FLD_CH_45_5,
    TOTAL FLD_CH_50_5,
    TOTAL FLD_CH_55_5,
    TOTAL FLD_CH_60_5,
    TOTAL FLD_CH_65_5,
    TOTAL FLD_CH_70_5,
    TOTAL FLD_CH_75_5,
    TOTAL FLD_CH_80_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV512PAH
       ;;
(PV512PAH)
       m_CondExec 00,EQ,PV512PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTREAVE MISE EN FORME                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV512PAJ PGM=EZTPA00    ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV512PAJ
       ;;
(PV512PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c X SYSPRINT
       m_FileAssign -d SHR -g ${G_A3} FILEA ${DATA}/PTEM/PV512PAG.BPV512BP
       m_FileAssign -d NEW,CATLG,DELETE -r 132 -g +1 FILEB ${DATA}/PTEM/PV512PAJ.BPV512CP
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/PV512PAJ
       m_ProgramExec PV512PAJ
# ********************************************************************         
#  TRI DU FICHIER PRECEDENT                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV512PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV512PAM
       ;;
(PV512PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PV512PAJ.BPV512CP
       m_FileAssign -d NEW,CATLG,DELETE -r 234 -g +1 SORTOUT ${DATA}/PTEM/PV512PAM.BPV512DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD FLD_PD_106_5 FLD_WITHOUT_EDIT_PD_106_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_74_3 FLD_WITHOUT_EDIT_PD_74_3 (+999,99)
 /DERIVEDFIELD FLD_PD_124_4 FLD_WITHOUT_EDIT_PD_124_4 (+9999,99)
 /DERIVEDFIELD FLD_PD_84_3 FLD_WITHOUT_EDIT_PD_84_3 (+999,99)
 /DERIVEDFIELD UKNOWN_58_41 -
 /DERIVEDFIELD FLD_PD_62_5 FLD_WITHOUT_EDIT_PD_62_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_100_5 FLD_WITHOUT_EDIT_PD_100_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_88_5 FLD_WITHOUT_EDIT_PD_88_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_68_5 FLD_WITHOUT_EDIT_PD_68_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_34_3 FLD_WITHOUT_EDIT_PD_34_3 (+999999)
 /DERIVEDFIELD FLD_PD_129_4 FLD_WITHOUT_EDIT_PD_129_4 (S9999,99)
 /DERIVEDFIELD FLD_PD_94_5 FLD_WITHOUT_EDIT_PD_94_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_56_5 FLD_WITHOUT_EDIT_PD_56_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_112_5 FLD_WITHOUT_EDIT_PD_112_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_44_5 FLD_WITHOUT_EDIT_PD_44_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_50_5 FLD_WITHOUT_EDIT_PD_50_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_30_3 FLD_WITHOUT_EDIT_PD_30_3 (+999999)
 /DERIVEDFIELD UKNOWN_57_40 SIGNS=(+
 /DERIVEDFIELD FLD_PD_118_5 FLD_WITHOUT_EDIT_PD_118_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_78_5 FLD_WITHOUT_EDIT_PD_78_5 (+9999999,99)
 /DERIVEDFIELD FLD_PD_38_5 FLD_WITHOUT_EDIT_PD_38_5 (+9999999,99)
 /FIELDS FLD_CH_87_1 87 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_106_5 106 PD 5
 /FIELDS FLD_WITHOUT_EDIT_PD_44_5 44 PD 5
 /FIELDS FLD_WITHOUT_EDIT_PD_50_5 50 PD 5
 /FIELDS FLD_CH_61_1 61 CH 1
 /FIELDS FLD_CH_73_1 73 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_74_3 74 PD 3
 /FIELDS FLD_WITHOUT_EDIT_PD_94_5 94 PD 5
 /FIELDS FLD_WITHOUT_EDIT_PD_118_5 118 PD 5
 /FIELDS FLD_CH_128_1 128 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_124_4 124 PD 4
 /FIELDS FLD_WITHOUT_EDIT_PD_100_5 100 PD 5
 /FIELDS FLD_WITHOUT_EDIT_PD_30_3 30 PD 3
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_117_1 117 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_78_5 78 PD 5
 /FIELDS FLD_BI_1_132 1 CH 132
 /FIELDS FLD_CH_111_1 111 CH 1
 /FIELDS FLD_CH_99_1 99 CH 1
 /FIELDS FLD_CH_93_1 93 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_34_3 34 PD 3
 /FIELDS FLD_WITHOUT_EDIT_PD_129_4 129 PD 4
 /FIELDS FLD_WITHOUT_EDIT_PD_56_5 56 PD 5
 /FIELDS FLD_CH_37_1 37 CH 1
 /FIELDS FLD_CH_1_29 1 CH 29
 /FIELDS FLD_CH_77_1 77 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_62_5 62 PD 5
 /FIELDS FLD_CH_49_1 49 CH 1
 /FIELDS FLD_CH_33_1 33 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_84_3 84 PD 3
 /FIELDS FLD_WITHOUT_EDIT_PD_38_5 38 PD 5
 /FIELDS FLD_CH_83_1 83 CH 1
 /FIELDS FLD_CH_55_1 55 CH 1
 /FIELDS FLD_CH_43_1 43 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_112_5 112 PD 5
 /FIELDS FLD_WITHOUT_EDIT_PD_68_5 68 PD 5
 /FIELDS FLD_CH_123_1 123 CH 1
 /FIELDS FLD_CH_105_1 105 CH 1
 /FIELDS FLD_WITHOUT_EDIT_PD_88_5 88 PD 5
 /KEYS
   FLD_BI_1_132 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_29,FLD_PD_30_3,FLD_CH_33_1,FLD_PD_34_3,FLD_CH_37_1,FLD_PD_38_5,FLD_CH_43_1,FLD_PD_44_5,FLD_CH_49_1,FLD_PD_50_5,FLD_CH_55_1,FLD_PD_56_5,FLD_CH_61_1,FLD_PD_62_5,FLD_CH_67_1,FLD_PD_68_5,FLD_CH_73_1,FLD_PD_74_3,FLD_CH_77_1,FLD_PD_78_5,FLD_CH_83_1,FLD_PD_84_3,FLD_CH_87_1,FLD_PD_88_5,FLD_CH_93_1,FLD_PD_94_5,FLD_CH_99_1,FLD_PD_100_5,FLD_CH_105_1,FLD_PD_106_5,FLD_CH_111_1,FLD_PD_112_5,FLD_CH_117_1,FLD_PD_118_5,FLD_CH_123_1,FLD_PD_124_4,FLD_CH_128_1,FLD_PD_129_4,UKNOWN_57_40,UKNOWN_58_41
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PV512PAN
       ;;
(PV512PAN)
       m_CondExec 00,EQ,PV512PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV512.                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV512PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV512PAQ
       ;;
(PV512PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.IPV512C
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/PV512PAM.BPV512DP
       m_OutputAssign -c 9 -w IPV512 SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_234 1 CH 234
 /COPY
 /* Record Type = F  Record Length = (234) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_234
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PV512PAR
       ;;
(PV512PAR)
       m_CondExec 00,EQ,PV512PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV512PZA
       ;;
(PV512PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV512PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
