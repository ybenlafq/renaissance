#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PDGV135B.ksh                       --- VERSION DU 20/10/2016 15:56
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGV135B -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: GV135D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ******************************************************************           
#  SORT ET OUTREC SUR FGV137                                                   
#  REPRISE: OUI                                                                
# ******************************************************************           
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GV135DB
       ;;
(GV135DB)
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXCAU=${EXCAU:-0}
       EXCAZ=${EXCAZ:-0}
       EXCBE=${EXCBE:-0}
       EXCBJ=${EXCBJ:-0}
       EXCBO=${EXCBO:-0}
       EXCBT=${EXCBT:-0}
       EXCBY=${EXCBY:-0}
       EXCCD=${EXCCD:-0}
       EXCCI=${EXCCI:-0}
       EXCCN=${EXCCN:-0}
       EXCCS=${EXCCS:-0}
       EXCCX=${EXCCX:-0}
       EXCDC=${EXCDC:-0}
       EXCDH=${EXCDH:-0}
       EXCDM=${EXCDM:-0}
       EXC98=${EXC98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON MONDAY    2009/03/30 AT 11.23.08 BY BURTECA                
# *    JOBSET INFORMATION:    NAME...: GV135D                                  
# *                           FREQ...: 6W                                      
# *                           TITLE..: 'GEST� VENTES DAILY'                    
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GV135DBA
       ;;
(GV135DBA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#  TRI FGV137                                                                  
#  REPRISE: OUI                                                                
# ****************************************************************             
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV991/F91.BGV137AD
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 SORTOUT ${DATA}/PTEM/GV135DBA.BGV137CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DBB
       ;;
(GV135DBB)
       m_CondExec 00,EQ,GV135DBA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV138 :           PRISE EN COMPTE DES VENTES SAISIES EN MAGASIN            
#  CREE FIC FGV138                                                             
#  REPRISE: OUI                                                                
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP GV135DBD PGM=IKJEFT01   ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBD
       ;;
(GV135DBD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# *******                                                                      
#    RSGA22   : NAME=RSGA22D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA22 /dev/null
# *******                                                                      
#    RSGG40   : NAME=RSGG40D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG40 /dev/null
# *******                                                                      
#    RSGG50   : NAME=RSGG50D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# *******                                                                      
#    RSRM10   : NAME=RSRM10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM05   : NAME=RSRM05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSRM05 /dev/null
# ******* SORTIE                                                               
#    RSVE10   : NAME=RSVE10D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE11   : NAME=RSVE11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSVE11 /dev/null
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 FGV138 ${DATA}/PXX0/F91.BGV138AD
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV105 ${DATA}/PXX0/F91.BHV105AD
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV138 
       JUMP_LABEL=GV135DBE
       ;;
(GV135DBE)
       m_CondExec 04,GE,GV135DBD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATE FICHIER FGV142                                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DBG PGM=SORT       ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBG
       ;;
(GV135DBG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F91.BGV135AD
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PTEM/GV135DBA.BGV137CD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F91.BGV138AD
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 SORTOUT ${DATA}/PTEM/GV135DBG.BGV142AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_88_3 88 CH 3
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_CH_54_5 54 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_69_5 69 CH 5
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_CH_80_3 80 CH 3
 /FIELDS FLD_CH_59_5 59 CH 5
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_48_3 48 CH 3
 /FIELDS FLD_CH_64_5 64 CH 5
 /FIELDS FLD_CH_74_5 74 CH 5
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_8 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_80_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_48_3,
    TOTAL FLD_CH_51_3,
    TOTAL FLD_CH_54_5,
    TOTAL FLD_CH_59_5,
    TOTAL FLD_CH_64_5,
    TOTAL FLD_CH_69_5,
    TOTAL FLD_CH_74_5,
    TOTAL FLD_CH_83_5,
    TOTAL FLD_CH_88_3,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DBH
       ;;
(GV135DBH)
       m_CondExec 00,EQ,GV135DBG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BGV145                                                              
#   CREE FIC FGV137       VENTES ARTICLES NON NCP A RECYCLER                   
#   CREE FIC FGV145       VENTES TRAITEES SOCIETE ARTICLE JOUR                 
#   CREE FIC FGV147       VENTES TRAITEES SOCIETE MAG ARTICLE MOIS             
#   CREE FIC FGV148       DONNEES STATISTIQUES COMMERCIALES DES VENTES         
#                         PAR SOCIETE, MAGASIN, ARTICLE ET MOIS                
#   CREE FIC FGV149       VENTES TRAITEES SOCIETE FAMILLE MOIS                 
#   CREE FIC FGV150       FICHIER DES RESULTATS (INUTILIS� A DAL)              
#   REPRISE: OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GV135DBJ PGM=IKJEFT01   ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBJ
       ;;
(GV135DBJ)
       m_CondExec ${EXCAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE CPT RENDU REMONTEE MAG                                         
#    RSHV15   : NAME=RSHV15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV15 /dev/null
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* VENTES DU JOUR                                                       
       m_FileAssign -d SHR -g ${G_A3} FGV142 ${DATA}/PTEM/GV135DBG.BGV142AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGV137 ${DATA}/PGV991/F91.BGV137AD
# ******* FAYCAL METTRE CE FILE DE 74 A 100                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGV145 ${DATA}/PTEM/GV135DBJ.BGV145AD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV147 ${DATA}/PXX0/F91.BGV147AD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV149 ${DATA}/PTEM/GV135DBJ.BGV149AD
       m_FileAssign -d SHR FGV150 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV148 ${DATA}/PXX0/F91.BGV148AD
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 991                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV145 
       JUMP_LABEL=GV135DBK
       ;;
(GV135DBK)
       m_CondExec 04,GE,GV135DBJ ${EXCAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES GROUPES DE PRODUITS SERVANT A CHARGERLA RTHV12           
#  SUR NSOCIETE NLIEU NVENTE GRP-CODIC CMODDEL NCODIC DVENTECIALE              
#        1,3     4,3   7,7     14,7      21,3   24,7    31,8                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DBM PGM=SORT       ** ID=CAU                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBM
       ;;
(GV135DBM)
       m_CondExec ${EXCAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FIC DES VENTES PAR GROUPE DE PRODUITS DU JOUR                        
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F91.BGV135CD
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g ${G_A4} SORTOUT ${DATA}/PXX0/F91.BHV105AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_31_8 31 CH 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DBN
       ;;
(GV135DBN)
       m_CondExec 00,EQ,GV135DBM ${EXCAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV105  : MAJ DE L'HISTO VENTES COMMERCIALES RTHV12 A PARTIR DES            
#            VENTES PAR GROUPE DE PRODUITS EXTRAITES PAR BGV135                
#                                                                              
#            POUR CHAQUE VENTE DU FICHIER BHV105AM :                           
#            CALCUL DU CHIFFRE AFFAIRE(PCA) ET DU SON MONTANT(PMTACHAT         
#  REPRISE : OUI SI FIN ANORMALE .                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DBQ PGM=IKJEFT01   ** ID=CAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBQ
       ;;
(GV135DBQ)
       m_CondExec ${EXCAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FIC DES VENTES PAR GROUPE DE PRODUITS DU JOUR                        
       m_FileAssign -d SHR -g ${G_A5} FHV105 ${DATA}/PXX0/F91.BHV105AD
# ******* TABLE MAGASINS DU GROUPE  LGT                                        
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******* TABLE FAMILLES TRAITES  LGT                                          
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
# ******* LIENS ENTRES ARTICLES                                                
#    RSGA58   : NAME=RSGA58D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* HISTO VENTES COMMERCIALES PAR GROUPE DE PRODUITS  LGT                
#    RSHV12   : NAME=RSHV12,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV12 /dev/null
# ******* ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV105 
       JUMP_LABEL=GV135DBR
       ;;
(GV135DBR)
       m_CondExec 04,GE,GV135DBQ ${EXCAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV165 :           PREPARATION EPURATION DES TABLES HV                      
#  CREE FIC FGV165       SOC   ARTICLE   JOUR                                  
#  CREE FIC FGV169       SOC   FAMILLE   JOUR                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DBT PGM=IKJEFT01   ** ID=CBE                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBT
       ;;
(GV135DBT)
       m_CondExec ${EXCBE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* HISTO VENTE/SOC,ARTICLE,JOUR                                         
#    RSHV02   : NAME=RSHV02D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV02 /dev/null
# ******* HISTO VENTE/SOC,ARTICLE,MAG,MOIS LGT                                 
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
# ******* HISTO VENTE/SOC,FAMILLE,JOUR                                         
#    RSHV08   : NAME=RSHV08D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV08 /dev/null
# ******* HISTO VENTE/SOC,MAG,GROUPE-PRODUIT,ARTICLE,JOUR  LGT                 
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
#                                                                              
# ******* FILE FHV02 DE LA VEILLE A VIDE POUR LE 1ER PASSAGE                   
       m_FileAssign -d SHR -g +0 FHV02E ${DATA}/PXX0/F91.BGV145CD.HISTO
# ******* SORTIE DE 51 A 77                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 FGV165 ${DATA}/PTEM/GV135DBT.BGV165AD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV169 ${DATA}/PTEM/GV135DBT.BGV169AD
# ******* FAYCAL FILE A CREE EN 93 DE LONG                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV02S ${DATA}/PTEM/GV135DBT.BGV165BD
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  PARAMETRE DE PURGE DES TABLES HV                                     
#         POS 01 A  4 : NB JOURS A CONSERVER    SUR RTHV02  => 0045            
#         POS 05 A  8 : NB SEMAINES A CONSERVER SUR RTHV04  => 0010            
#         POS 09 A 12 : NB JOURS A CONSERVER    SUR RTHV08  => 1110            
#         POS 13 A 16 : NB JOURS A CONSERVER    SUR RTHV12 (IDEM RTHV0         
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV135DBT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV165 
       JUMP_LABEL=GV135DBU
       ;;
(GV135DBU)
       m_CondExec 04,GE,GV135DBT ${EXCBE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FIC FGV145                                                          
#   1,18 A   AVEC OUTREC 1,51                                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DBX PGM=SORT       ** ID=CBJ                                   
# ***********************************                                          
       JUMP_LABEL=GV135DBX
       ;;
(GV135DBX)
       m_CondExec ${EXCBJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GV135DBJ.BGV145AD
# ****    FICHIER                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 SORTOUT ${DATA}/PTEM/GV135DBX.BGV145BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_75_26 75 CH 26
 /FIELDS FLD_CH_1_51 1 CH 51
 /FIELDS FLD_CH_1_18 1 CH 18
 /KEYS
   FLD_CH_1_18 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51,FLD_CH_75_26
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GV135DBY
       ;;
(GV135DBY)
       m_CondExec 00,EQ,GV135DBX ${EXCBJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION AVEC  FIC FGV145                                                
#   1,3,A SOCIETE 4,7,A ARTICLE 11,8,A DATE VENTE                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCA PGM=SORT       ** ID=CBO                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCA
       ;;
(GV135DCA)
       m_CondExec ${EXCBO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GV135DBX.BGV145BD
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GV135DBT.BGV165AD
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 SORTOUT ${DATA}/PGV991/F91.RELOAD.HV02RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_PD_52_3 52 PD 3
 /FIELDS FLD_PD_68_5 68 PD 5
 /FIELDS FLD_PD_42_5 42 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_19_3 19 PD 3
 /FIELDS FLD_PD_27_5 27 PD 5
 /FIELDS FLD_PD_60_5 60 PD 5
 /FIELDS FLD_PD_39_3 39 PD 3
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_PD_47_5 47 PD 5
 /FIELDS FLD_PD_22_5 22 PD 5
 /FIELDS FLD_PD_65_3 65 PD 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_3,
    TOTAL FLD_PD_22_5,
    TOTAL FLD_PD_27_5,
    TOTAL FLD_PD_39_3,
    TOTAL FLD_PD_42_5,
    TOTAL FLD_PD_47_5,
    TOTAL FLD_PD_52_3,
    TOTAL FLD_PD_55_5,
    TOTAL FLD_PD_60_5,
    TOTAL FLD_PD_65_3,
    TOTAL FLD_PD_68_5,
    TOTAL FLD_PD_73_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DCB
       ;;
(GV135DCB)
       m_CondExec 00,EQ,GV135DCA ${EXCBO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTHV02                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCD PGM=DSNUTILB   ** ID=CBT                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCD
       ;;
(GV135DCD)
       m_CondExec ${EXCBT},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV02                                                
       m_FileAssign -d SHR -g ${G_A9} SYSREC ${DATA}/PGV991/F91.RELOAD.HV02RD
#    RSHV02   : NAME=RSHV02D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV02 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135DCD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PDGV135B_GV135DCD_RTHV02.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135DCE
       ;;
(GV135DCE)
       m_CondExec 04,GE,GV135DCD ${EXCBT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FIC FGV145                                                          
#   1,18 A     AVEC OUTREC FIELDS                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCG PGM=SORT       ** ID=CBY                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCG
       ;;
(GV135DCG)
       m_CondExec ${EXCBY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GV135DBJ.BGV145AD
# ****    FICHIER A CREE EN 93 = FHV02 DU JOUR                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PTEM/GV135DCG.BGV145DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_39_62 39 CH 62
 /FIELDS FLD_CH_1_18 1 CH 18
 /FIELDS FLD_CH_1_31 1 CH 31
 /KEYS
   FLD_CH_1_18 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_31,FLD_CH_39_62
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GV135DCH
       ;;
(GV135DCH)
       m_CondExec 00,EQ,GV135DCG ${EXCBY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION AVEC  FIC FGV149                                                
#   1,3,A SOCIETE 4,5,A FAMILLE 9,8,A DATE DE VENTE                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCJ PGM=SORT       ** ID=CCD                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCJ
       ;;
(GV135DCJ)
       m_CondExec ${EXCCD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GV135DBJ.BGV149AD
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GV135DBT.BGV169AD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 SORTOUT ${DATA}/PGV991/F91.HV08RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_86_3 86 PD 3
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_76_5 76 PD 5
 /FIELDS FLD_PD_58_5 58 PD 5
 /FIELDS FLD_PD_43_5 43 PD 5
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_PD_23_5 23 PD 5
 /FIELDS FLD_PD_94_5 94 PD 5
 /FIELDS FLD_PD_68_5 68 PD 5
 /FIELDS FLD_PD_33_5 33 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_81_5 81 PD 5
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_PD_63_5 63 PD 5
 /FIELDS FLD_PD_73_3 73 PD 3
 /FIELDS FLD_PD_28_5 28 PD 5
 /FIELDS FLD_PD_55_3 55 PD 3
 /FIELDS FLD_PD_38_5 38 PD 5
 /FIELDS FLD_CH_4_5 4 CH 5
 /FIELDS FLD_PD_89_5 89 PD 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_5 ASCENDING,
   FLD_CH_9_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_17_3,
    TOTAL FLD_PD_20_3,
    TOTAL FLD_PD_23_5,
    TOTAL FLD_PD_28_5,
    TOTAL FLD_PD_33_5,
    TOTAL FLD_PD_38_5,
    TOTAL FLD_PD_43_5,
    TOTAL FLD_PD_55_3,
    TOTAL FLD_PD_58_5,
    TOTAL FLD_PD_63_5,
    TOTAL FLD_PD_68_5,
    TOTAL FLD_PD_73_3,
    TOTAL FLD_PD_76_5,
    TOTAL FLD_PD_81_5,
    TOTAL FLD_PD_86_3,
    TOTAL FLD_PD_89_5,
    TOTAL FLD_PD_94_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DCK
       ;;
(GV135DCK)
       m_CondExec 00,EQ,GV135DCJ ${EXCCD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV08                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCM PGM=DSNUTILB   ** ID=CCI                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCM
       ;;
(GV135DCM)
       m_CondExec ${EXCCI},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV08                                                
       m_FileAssign -d SHR -g ${G_A13} SYSREC ${DATA}/PGV991/F91.HV08RD
#    RSHV08   : NAME=RSHV08D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV08 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135DCM.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PDGV135B_GV135DCM_RTHV08.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135DCN
       ;;
(GV135DCN)
       m_CondExec 04,GE,GV135DCM ${EXCCI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BGV137A                                                     
#   19,5,A CODE TYPE 24,5 MARQUE 1,7 ARTICLE 8,3 MAG 11,8 DATE VENTE           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCQ PGM=SORT       ** ID=CCN                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCQ
       ;;
(GV135DCQ)
       m_CondExec ${EXCCN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PGV991/F91.BGV137AD
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GV135DCQ.BGV137BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_24_5 24 CH 5
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_19_5 ASCENDING,
   FLD_CH_24_5 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_11_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DCR
       ;;
(GV135DCR)
       m_CondExec 00,EQ,GV135DCQ ${EXCCN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ANOMALIES DES ARTICLES (SI DATE=LENDEMAIN DANS FIC STOC         
#  BGV155                                                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCT PGM=IKJEFT01   ** ID=CCS                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCT
       ;;
(GV135DCT)
       m_CondExec ${EXCCS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSPT03   : NAME=RSPT03D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT03 /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$VDATEJ1_DDMMANN
_end
# ******  FICHIER DES VENTES DES ARTICLES A RECYCLER                           
       m_FileAssign -d SHR -g ${G_A15} FGV137 ${DATA}/PTEM/GV135DCQ.BGV137BD
# ******  EDITION DES ANOMALIES                                                
       m_OutputAssign -c 9 -w BGV155 IGV155
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV155 
       JUMP_LABEL=GV135DCU
       ;;
(GV135DCU)
       m_CondExec 04,GE,GV135DCT ${EXCCS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU BGV145DD AVEC BGV165BD                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DCX PGM=SORT       ** ID=CCX                                   
# ***********************************                                          
       JUMP_LABEL=GV135DCX
       ;;
(GV135DCX)
       m_CondExec ${EXCCX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FIC DES VENTES PAR GROUPE DE PRODUITS DU JOUR                        
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GV135DCG.BGV145DD
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GV135DBT.BGV165BD
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/F91.BGV145CD.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_89_5 89 PD 5
 /FIELDS FLD_PD_71_5 71 PD 5
 /FIELDS FLD_PD_53_5 53 PD 5
 /FIELDS FLD_PD_22_5 22 PD 5
 /FIELDS FLD_PD_48_5 48 PD 5
 /FIELDS FLD_PD_76_5 76 PD 5
 /FIELDS FLD_PD_84_5 84 PD 5
 /FIELDS FLD_PD_19_3 19 PD 3
 /FIELDS FLD_PD_81_3 81 PD 3
 /FIELDS FLD_PD_63_5 63 PD 5
 /FIELDS FLD_PD_27_5 27 PD 5
 /FIELDS FLD_PD_40_5 40 PD 5
 /FIELDS FLD_CH_1_18 1 CH 18
 /FIELDS FLD_PD_68_3 68 PD 3
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_PD_45_3 45 PD 3
 /FIELDS FLD_PD_58_5 58 PD 5
 /FIELDS FLD_PD_35_5 35 PD 5
 /KEYS
   FLD_CH_1_18 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_3,
    TOTAL FLD_PD_22_5,
    TOTAL FLD_PD_27_5,
    TOTAL FLD_PD_32_3,
    TOTAL FLD_PD_35_5,
    TOTAL FLD_PD_40_5,
    TOTAL FLD_PD_45_3,
    TOTAL FLD_PD_48_5,
    TOTAL FLD_PD_53_5,
    TOTAL FLD_PD_58_5,
    TOTAL FLD_PD_63_5,
    TOTAL FLD_PD_68_3,
    TOTAL FLD_PD_71_5,
    TOTAL FLD_PD_76_5,
    TOTAL FLD_PD_81_3,
    TOTAL FLD_PD_84_5,
    TOTAL FLD_PD_89_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DCY
       ;;
(GV135DCY)
       m_CondExec 00,EQ,GV135DCX ${EXCCX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV175  EPURATION DU FICHIER HISTORIQUE                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DDA PGM=IKJEFT01   ** ID=CDC                                   
# ***********************************                                          
       JUMP_LABEL=GV135DDA
       ;;
(GV135DDA)
       m_CondExec ${EXCDC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSPT01   : NAME=RSPT01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPT01 /dev/null
# ******  FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV135DDA
# ***  FICHIER DE LA VEILLE                                                    
       m_FileAssign -d SHR -g +0 FGVHIS ${DATA}/PXX0/F91.BGV178CD
# ***  SORTIE                                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV178 ${DATA}/PTEM/GV135DDA.BGV178AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV175 
       JUMP_LABEL=GV135DDB
       ;;
(GV135DDB)
       m_CondExec 04,GE,GV135DDA ${EXCDC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIERS FGV178 ET FGV148                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DDD PGM=SORT       ** ID=CDH                                   
# ***********************************                                          
       JUMP_LABEL=GV135DDD
       ;;
(GV135DDD)
       m_CondExec ${EXCDH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/F91.BGV148AD
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GV135DDA.BGV178AD
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/GV135DDD.BGV178BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_6 14 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DDE
       ;;
(GV135DDE)
       m_CondExec 00,EQ,GV135DDD ${EXCDH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135DDG PGM=SORT       ** ID=CDM                                   
# ***********************************                                          
       JUMP_LABEL=GV135DDG
       ;;
(GV135DDG)
       m_CondExec ${EXCDM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GV135DDD.BGV178BD
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/F91.BGV178CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_23_5 23 PD 5
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_28_5 28 PD 5
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_3,
    TOTAL FLD_PD_23_5,
    TOTAL FLD_PD_28_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135DDH
       ;;
(GV135DDH)
       m_CondExec 00,EQ,GV135DDG ${EXCDM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=GV135DZB
       ;;
(GV135DZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135DZB.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
