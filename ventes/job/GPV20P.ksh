#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV20P.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGPV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/24 AT 17.42.37 BY BURTECN                      
#    STANDARDS: P  JOBSET: GPV20P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV20PA
       ;;
(GPV20PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV20PAA
       ;;
(GPV20PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE  DATE DE DERNIER PASSAGE                                   
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F07.DATPV20P
#                                                                              
# ------  TABLE EN LECTURE                                                     
#                                                                              
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA59 /dev/null
#    RTGA66   : NAME=RSGA66,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA66 /dev/null
#    RTGA73   : NAME=RSGA73,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA73 /dev/null
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS30 /dev/null
#                                                                              
# ------  FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/GPV20PAA.BPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220 ${DATA}/PTEM/GPV20PAA.BPV200BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV20PAB
       ;;
(GPV20PAB)
       m_CondExec 04,GE,GPV20PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200AP (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)         
#  ET CREATION DU FICHIER BPV211AP ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAD
       ;;
(GPV20PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GPV20PAA.BPV200AP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GPV20PAD.BPV211AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20PAE
       ;;
(GPV20PAE)
       m_CondExec 00,EQ,GPV20PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200BP (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,         
#  ET CREATION DU FICHIER BPV211BP ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAG
       ;;
(GPV20PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GPV20PAA.BPV200BP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GPV20PAG.BPV211BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20PAH
       ;;
(GPV20PAH)
       m_CondExec 00,EQ,GPV20PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV211                                                                
# ********************************************************************         
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAJ
       ;;
(GPV20PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- TABLES EN ENTREE                                                     
#                                                                              
#    RTGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA11 /dev/null
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA26 /dev/null
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
#                                                                              
# ------- FICHIER D'EXTRACTION DES TRIS PRECEDENTS                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC1 ${DATA}/PTEM/GPV20PAD.BPV211AP
       m_FileAssign -d SHR -g ${G_A4} FPV220 ${DATA}/PTEM/GPV20PAG.BPV211BP
#                                                                              
# ------- FICHIER D'EXTRACTION DE BPV210 (EN DUMMY) CAR CE PROG                
# ------- TOURNE SEULEMENT EN HEBDO DANS GPV21P                                
#                                                                              
       m_FileAssign -d SHR FEXTRAC2 /dev/null
#                                                                              
# ------- FICHIER D'EDITION EN DUMMY CAR ON NE LES FAIT PAS EN QUOTIDI         
#                                                                              
       m_FileAssign -d SHR FEXTR1B /dev/null
       m_FileAssign -d SHR FEXTR2B /dev/null
#                                                                              
# ------- FICHIER FPV220B ENTRANT DANS LE PGM BPV212                           
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220B ${DATA}/PTEM/GPV20PAJ.BPV220AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV20PAK
       ;;
(GPV20PAK)
       m_CondExec 04,GE,GPV20PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV220AP POUR CREATION DU FICHIER COMPLET DES                
#  MODIFICATIONS DE L'OFFRE ACTIVE (BPV220BP)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAM
       ;;
(GPV20PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GPV20PAJ.BPV220AP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/F07.BPV220BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_4_3 4 CH 3
 /FIELDS FLD_BI_42_7 42 CH 7
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20PAN
       ;;
(GPV20PAN)
       m_CondExec 00,EQ,GPV20PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV212                                                                
# ********************************************************************         
#  CREATION DU FICHIER FPV212 A ENVOYER SUR LES 36 DES MAGS PAR                
#  COMPARAISON DES FICHIERS BPV220BP                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAQ PGM=BPV212     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAQ
       ;;
(GPV20PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- DATE (JJMMSSAA)                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ------- FICHIER DE L'OFFRE ACTIVE DE LA VEILLE                               
       m_FileAssign -d SHR -g ${G_A6} FPV220O ${DATA}/PXX0/F07.BPV220BP
# ------- FICHIER DE L'OFFRE ACTIVE DU JOUR                                    
       m_FileAssign -d SHR -g ${G_A7} FPV220B ${DATA}/PXX0/F07.BPV220BP
# ------- FICHIER DE COMPARAISON A ENVOYER SUR LE 36 (GDGNB=5)                 
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FPV212 ${DATA}/PXX0/F07.BPV212AP
#                                                                              
       m_ProgramExec BPV212
       m_UtilityExec SYSTSIN
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PAT
       ;;
(GPV20PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA                                                       
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F07.DATPV20P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20PAU
       ;;
(GPV20PAU)
       m_CondExec 00,EQ,GPV20PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV20PZA
       ;;
(GPV20PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV20PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
