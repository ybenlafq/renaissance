#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV800O.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPV800 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/20 AT 10.48.12 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV800O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPV500                                                                
#  ------------                                                                
#  EXTRACTION DES VENTES SANS ADRESSE                                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV800OA
       ;;
(PV800OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV800OAA
       ;;
(PV800OAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV10   : NAME=RSGV10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV10 /dev/null
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTVE10   : NAME=RSVE10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE10 /dev/null
#    RTVE11   : NAME=RSVE11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTVE11 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA58   : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTBS00   : NAME=RSBS00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTBS00 /dev/null
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# *****   FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV800OAA
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500 ${DATA}/PTEM/PV800OAA.PV800O1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV500 
       JUMP_LABEL=PV800OAB
       ;;
(PV800OAB)
       m_CondExec 04,GE,PV800OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU BPV500                                               
#  WARNING . AJOUT D UN FICHIER PERMANENT  EN FAIT SORTIE DU PGM BPV50         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAD
       ;;
(PV800OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV800OAA.PV800O1
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OAD.PV800O2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_5 57 CH 5
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_133_2 133 CH 2
 /KEYS
   FLD_CH_133_2 ASCENDING,
   FLD_CH_57_5 DESCENDING,
   FLD_CH_31_8 DESCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_40_7 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OAE
       ;;
(PV800OAE)
       m_CondExec 00,EQ,PV800OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTPR12                                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAG
       ;;
(PV800OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/PV800OAG.PV800O3
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV800OAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHIER ISSU DE L'UNLOAD DE LA TABLE RTPR12                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAJ
       ;;
(PV800OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/PV800OAG.PV800O3
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OAJ.PV800O4
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_8 13 CH 8
 /FIELDS FLD_CH_6_2 6 CH 2
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_6_2 ASCENDING,
   FLD_CH_1_5 DESCENDING,
   FLD_CH_13_8 DESCENDING
 /* Record Type = F  Record Length = 27 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OAK
       ;;
(PV800OAK)
       m_CondExec 00,EQ,PV800OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV504                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR LES SERVICES                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAM
       ;;
(PV800OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A3} FPV500I ${DATA}/PTEM/PV800OAD.PV800O2
       m_FileAssign -d SHR -g ${G_A4} FTPR12 ${DATA}/PTEM/PV800OAJ.PV800O4
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OAM.PV800O5
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV504 
       JUMP_LABEL=PV800OAN
       ;;
(PV800OAN)
       m_CondExec 04,GE,PV800OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV504                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAQ
       ;;
(PV800OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV800OAM.PV800O5
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OAQ.PV800O6
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_47_2 47 CH 2
 /KEYS
   FLD_CH_47_2 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 DESCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OAR
       ;;
(PV800OAR)
       m_CondExec 00,EQ,PV800OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV503                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES PSE                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAT
       ;;
(PV800OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV11   : NAME=RSGV11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV11 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA40   : NAME=RSGA40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA40 /dev/null
#    RTGA52   : NAME=RSGA52O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTBS01   : NAME=RSBS01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBS01 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A6} FPV500I ${DATA}/PTEM/PV800OAQ.PV800O6
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OAT.PV800O7
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV503 
       JUMP_LABEL=PV800OAU
       ;;
(PV800OAU)
       m_CondExec 04,GE,PV800OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV503                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV800OAX
       ;;
(PV800OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV800OAT.PV800O7
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OAX.PV800O8
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_47_2 47 CH 2
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_31_8 31 CH 8
 /KEYS
   FLD_CH_47_2 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OAY
       ;;
(PV800OAY)
       m_CondExec 00,EQ,PV800OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV501                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENT                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBA
       ;;
(PV800OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A8} FPV500I ${DATA}/PTEM/PV800OAX.PV800O8
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OBA.PV800O9
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV501 
       JUMP_LABEL=PV800OBB
       ;;
(PV800OBB)
       m_CondExec 04,GE,PV800OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV501                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBD
       ;;
(PV800OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/PV800OBA.PV800O9
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OBD.PV800O10
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_40_7 40 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_17_6 17 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_39_1 39 CH 1
 /KEYS
   FLD_CH_39_1 ASCENDING,
   FLD_CH_40_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_17_6 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OBE
       ;;
(PV800OBE)
       m_CondExec 00,EQ,PV800OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV502                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC GROUPE                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBG
       ;;
(PV800OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A10} FPV500I ${DATA}/PTEM/PV800OBD.PV800O10
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OBG.PV800O11
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV502 
       JUMP_LABEL=PV800OBH
       ;;
(PV800OBH)
       m_CondExec 04,GE,PV800OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BPV502                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBJ
       ;;
(PV800OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/PV800OBG.PV800O11
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV800OBJ.PV800O12
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_50_7 50 CH 7
 /FIELDS FLD_CH_57_5 57 CH 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_39_1 39 CH 1
 /FIELDS FLD_CH_40_7 40 CH 7
 /KEYS
   FLD_CH_39_1 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_40_7 ASCENDING,
   FLD_CH_50_7 ASCENDING,
   FLD_CH_31_8 ASCENDING,
   FLD_CH_57_5 ASCENDING
 /* Record Type = F  Record Length = 147 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV800OBK
       ;;
(PV800OBK)
       m_CondExec 00,EQ,PV800OBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV505                                                                
#  ------------                                                                
#  MISE A JOUR DU FICHIER DES PRIMES SUR CODIC ELEMENTS                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBM
       ;;
(PV800OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN LECTURE                                                    
#    RTGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA52   : NAME=RSGA52O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA52 /dev/null
#    RTGA65   : NAME=RSGA65O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA68   : NAME=RSGA68O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA68 /dev/null
#    RTGA75   : NAME=RSGA75O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTGG40   : NAME=RSGG40O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG40 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A12} FPV500I ${DATA}/PTEM/PV800OBJ.PV800O12
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OBM.PV800O13
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV505 
       JUMP_LABEL=PV800OBN
       ;;
(PV800OBN)
       m_CondExec 04,GE,PV800OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV507                                                                
#  ------------                                                                
#  INTEGRATION DES COMMISSIONS OPERATEURS                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV800OBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBQ
       ;;
(PV800OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES DES PRESTATIONS                                               
#    RTPR21   : NAME=RSPR21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR21 /dev/null
#    RTPR22   : NAME=RSPR22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR22 /dev/null
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ******  FICHIERS EN ENTREE                                                   
       m_FileAssign -d SHR -g ${G_A13} FPV500I ${DATA}/PTEM/PV800OBM.PV800O13
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 FPV500O ${DATA}/PTEM/PV800OBQ.PV800O20
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV507 
       JUMP_LABEL=PV800OBR
       ;;
(PV800OBR)
       m_CondExec 04,GE,PV800OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP PV800OBT PGM=IDCAMS     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV800OBT
       ;;
(PV800OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FIC DE GEN POUR LE TRAITEMENT MENSUEL                                
       m_FileAssign -d SHR -g ${G_A14} IN1 ${DATA}/PTEM/PV800OBQ.PV800O20
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.PV800OED
# ******  FIC DE CUMUL                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -t LSEQ -g +1 OUT1 ${DATA}/PNCGO/F16.PV800OED
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV800OBT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PV800OBU
       ;;
(PV800OBU)
       m_CondExec 16,NE,PV800OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV800OZA
       ;;
(PV800OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV800OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
