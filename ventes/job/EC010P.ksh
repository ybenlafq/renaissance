#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC010P.ksh                       --- VERSION DU 09/10/2016 05:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/16 AT 11.25.09 BY BURTEC2                      
#    STANDARDS: P  JOBSET: EC010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM = BEC010  -  EXTRACTION TRI-QUOTIDIENNE 040                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC010PA
       ;;
(EC010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC010PAA
       ;;
(EC010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  PRED     LINK  NAME=$EX010P,MODE=I                                          
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV20   : NAME=RSGV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV20 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSEC02   : NAME=RSEC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC02 /dev/null
#                                                                              
# ******* FNLIEU                                                               
       m_FileAssign -d SHR FNLIEU ${DATA}/CORTEX4.P.MTXTFIX1/EC010PAA
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 389 -t LSEQ -g +1 FEC01001 ${DATA}/PXX0/F07.BEC010AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC010 
       JUMP_LABEL=EC010PAB
       ;;
(EC010PAB)
       m_CondExec 04,GE,EC010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC010  -  EXTRACTION TRI-QUOTIDIENNE 240                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC010PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC010PAD
       ;;
(EC010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV20   : NAME=RSGV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV20 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSEC02   : NAME=RSEC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC02 /dev/null
#                                                                              
# ******* FNLIEU                                                               
       m_FileAssign -d SHR FNLIEU ${DATA}/CORTEX4.P.MTXTFIX1/EC010PAD
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 389 -t LSEQ -g +1 FEC01001 ${DATA}/PXX0/F07.BEC010BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC010 
       JUMP_LABEL=EC010PAE
       ;;
(EC010PAE)
       m_CondExec 04,GE,EC010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC010  -  EXTRACTION TRI-QUOTIDIENNE 099                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC010PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC010PAG
       ;;
(EC010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV10   : NAME=RSGV10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV20   : NAME=RSGV20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV20 /dev/null
#    RSGV14   : NAME=RSGV14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV14 /dev/null
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSEC02   : NAME=RSEC02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC02 /dev/null
#                                                                              
# ******* FNLIEU                                                               
       m_FileAssign -d SHR FNLIEU ${DATA}/CORTEX4.P.MTXTFIX1/EC010PAG
#                                                                              
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 389 -t LSEQ -g +1 FEC01001 ${DATA}/PXX0/F07.BEC010CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC010 
       JUMP_LABEL=EC010PAH
       ;;
(EC010PAH)
       m_CondExec 04,GE,EC010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP nomjob                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC010PAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC010PAJ
       ;;
(EC010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC010PAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/EC010PAJ.FTEC010P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTEC010P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC010PAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EC010PAM
       ;;
(EC010PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/EC010PAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.EC010PAJ.FTEC010P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC010PZA
       ;;
(EC010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
