#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV630O.ksh                       --- VERSION DU 08/10/2016 12:45
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGV630 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/18 AT 14.54.28 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV630O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BGV630                                                                
# ********************************************************************         
#  TOPER LES LIGNES DEPENDANTES D UNE LIGNE ARTICLE DEJA TOPEE                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV630OA
       ;;
(GV630OA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=GV630OAA
       ;;
(GV630OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* TABLES EN LECTURE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#    RSGV08O  : NAME=RSGV08O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV08O /dev/null
#    RSPR00O  : NAME=RSPR00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00O /dev/null
# ******* TABLE DETAIL DE VENTES                                               
#    RSGV10O  : NAME=RSGV10O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10O /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11   : NAME=RSRB21,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV630 
       JUMP_LABEL=GV630OAB
       ;;
(GV630OAB)
       m_CondExec 04,GE,GV630OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
