#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  BA800P.ksh                       --- VERSION DU 08/10/2016 17:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPBA800 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/12 AT 13.36.28 BY BURTEC2                      
#    STANDARDS: P  JOBSET: BA800P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BBA800                                                                     
#  EXTRACTION FILIALE                                                          
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=BA800PA
       ;;
(BA800PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       PROCSTEP=${PROCSTEP:-BA800PA}
       RUN=${RUN}
       JUMP_LABEL=BA800PAA
       ;;
(BA800PAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
# * DEPENDANCES POUR PLAN :             *                                      
# *   OBLIGATOIRE POUR LOGIQUE APPL     *                                      
# ***********************************                                          
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSBA01   : NAME=RSBA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA01 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# *****   FICHIER EXTRAC PARIS POR GATEWAY                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 65 -t LSEQ -g +1 FBA800A ${DATA}/PXX0/F07.BA800AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA800 
       JUMP_LABEL=BA800PAB
       ;;
(BA800PAB)
       m_CondExec 04,GE,BA800PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT CR�ATION FICHIER TOUTES FILIALES POUR GATEWAY                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BA800PAD PGM=MERGE      ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=BA800PAD
       ;;
(BA800PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BA800AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BA800AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BA800AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BA800AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F08.BA800AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BA800AY
       m_FileAssign -d NEW,CATLG,DELETE -r 65 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BA800BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 065 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=BA800PAE
       ;;
(BA800PAE)
       m_CondExec 00,EQ,BA800PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHIERS DIF ET TOUTES FILIALES POUR ENVOI VERS GATEWAY             
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=PKZIP                                                     
# //STEPLIB  DD DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                               
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ****** FICHIERS EN ENTREE DE ZIP                                             
# ****** DIF                                                                   
# DD1      FILE  NAME=BA800AP,MODE=I                                           
# ****** FILIALES                                                              
# DD2      FILE  NAME=BA800BP,MODE=I                                           
# ****** FICHIER EN SORTIE DE ZIP                                              
# FICZIP   FILE  NAME=BA80ZIPP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -INFILE(DD2)                                                                
#  -ZIPPED_DSN(PXX0.F07.BA800AP.*,BA800P.TXT)                                  
#  -ZIPPED_DSN(PXX0.F07.BA800BP.*,BA800F.TXT)                                  
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BA800PAG PROC=JVZIP     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=BA800PAG
       ;;
(BA800PAG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAK},NE,YES "
# ****** FICHIERS EN ENTREE DE ZIP                                             
# ****** DIF                                                                   
       m_FileOverride -d SHR -s JVZIPU -g ${G_A1} DD1 ${DATA}/PXX0/F07.BA800AP
       m_FileOverride -d SHR -s JVZIPU -g ${G_A2} DD2 ${DATA}/PXX0/F07.BA800BP
       m_FileOverride -d NEW,CATLG,DELETE -r 80 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.BA80ZIPP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BA800PAG.sysin
       JUMP_LABEL=BA800PAJ
       ;;
(BA800PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BA800PAJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/BA800PAJ.FTBA800P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTBA800P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP BA800PAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=BA800PAM
       ;;
(BA800PAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/BA800PAM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.BA800PAJ.FTBA800P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=BA800PZA
       ;;
(BA800PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/BA800PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
