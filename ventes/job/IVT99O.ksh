#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT99O.ksh                       --- VERSION DU 09/10/2016 00:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POIVT99 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/05/14 AT 10.04.18 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT99O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#                                                                              
#   REPORTEE  :  CETTE CHAINE PEUT ETRE REPORTEE EN CAS DE PLANTAGE.           
#   -=-=-=-=                                                                   
#                                                                              
# ********************************************************************         
#  UNLOAD DES TABLES RTIT 00 05 10 15                                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT99OA
       ;;
(IVT99OA)
#
#IVT99OAD
#IVT99OAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT99OAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       RUN=${RUN}
       JUMP_LABEL=IVT99OAA
       ;;
(IVT99OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#    RSIT00O  : NAME=RSIT00O,MODE=I - DYNAM=YES                                
#    RSIT05O  : NAME=RSIT05O,MODE=I - DYNAM=YES                                
#    RSIT10O  : NAME=RSIT10O,MODE=I - DYNAM=YES                                
#    RSIT15O  : NAME=RSIT15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,CATLG -r 45 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F16.UNIT00AO
       m_FileAssign -d NEW,CATLG,CATLG -r 21 -t LSEQ -g +1 SYSREC02 ${DATA}/PXX0/F16.UNIT05AO
       m_FileAssign -d NEW,CATLG,CATLG -r 17 -t LSEQ -g +1 SYSREC03 ${DATA}/PXX0/F16.UNIT10AO
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -t LSEQ -g +1 SYSREC04 ${DATA}/PXX0/F16.UNIT15AO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99OAA.sysin
       
       m_DBHpuUnload -f SYSIN
# ********************************************************************         
#  QUIESCE DES TABLES RTIT00 05 10 15                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99OAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVT99OAG
       ;;
(IVT99OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE PARAMETRES GENERAUX D'INVENTAIRE                                      
#    RSIT00O  : NAME=RSIT00O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT00O /dev/null
#  TABLE LIEUX INVENTORIES                                                     
#    RSIT05R  : NAME=RSIT05O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT05R /dev/null
#  TABLE FAMILLES INVENTORIES                                                  
#    RSIT10O  : NAME=RSIT10O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT10O /dev/null
#  TABLE CODICS A INVENTORIER                                                  
#    RSIT15O  : NAME=RSIT15O,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15O /dev/null
#  TABLE GENERALISEE                                                           
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/IVT99OAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT020 
       JUMP_LABEL=IVT99OAH
       ;;
(IVT99OAH)
       m_CondExec 04,GE,IVT99OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSIT00O                                       
#   REPRISE: NON BACKOUT CORTEX RIVT99O                                        
#            VERIFIER LE BACKOUT RIVT99O                                       
#            REPRISE FORCEE EN DEBUT DE CHAINE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT99OAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT99OZA
       ;;
(IVT99OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT99OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
