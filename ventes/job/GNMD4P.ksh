#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GNMD4P.ksh                       --- VERSION DU 21/10/2016 11:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGNMD4 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/18 AT 09.43.29 BY BURTECA                      
#    STANDARDS: P  JOBSET: GNMD4P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   TRI DU FICHIER ISSU DE GNMD2P P907.SEM.BTF001AP(0)                         
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GNMD4PA
       ;;
(GNMD4PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+2'}
       G_A22=${G_A22:-'+2'}
       G_A23=${G_A23:-'+3'}
       G_A24=${G_A24:-'+3'}
       G_A25=${G_A25:-'+4'}
       G_A26=${G_A26:-'+4'}
       G_A27=${G_A27:-'+5'}
       G_A28=${G_A28:-'+5'}
       G_A29=${G_A29:-'+6'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+6'}
       G_A31=${G_A31:-'+7'}
       G_A32=${G_A32:-'+7'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GNMD4PAA
       ;;
(GNMD4PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------                                                      
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/P907/SEM.BTF001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD4PAA.BTF001DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_9_20 "RTLI10"
 /DERIVEDFIELD CST_3_8 "RTGA22"
 /DERIVEDFIELD CST_1_4 "RTGA14"
 /DERIVEDFIELD CST_5_12 "RTGA31"
 /DERIVEDFIELD CST_7_16 "RTLI00"
 /DERIVEDFIELD CST_11_24 "RTGA54"
 /FIELDS FLD_CH_1_6 1 CH 06
 /CONDITION CND_1 FLD_CH_1_6 EQ CST_1_4 OR FLD_CH_1_6 EQ CST_3_8 OR FLD_CH_1_6 EQ CST_5_12 OR FLD_CH_1_6 EQ CST_7_16 OR FLD_CH_1_6 EQ CST_9_20 OR FLD_CH_1_6 EQ CST_11_24 
 /KEYS
   FLD_CH_1_6 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAB
       ;;
(GNMD4PAB)
       m_CondExec 00,EQ,GNMD4PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF950 : EXTRACTION                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAD PGM=BTF950     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAD
       ;;
(GNMD4PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A1} FTF601 ${DATA}/PTEM/GNMD4PAA.BTF001DP
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMGA14 ${DATA}/P907/SEM.FEMGA14P
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMGA22 ${DATA}/P907/SEM.FEMGA22P
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMGA31 ${DATA}/P907/SEM.FEMGA31P
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMGA54 ${DATA}/P907/SEM.FEMGA54P
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMLI00 ${DATA}/P907/SEM.FEMLI00P
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 FEMLI10 ${DATA}/P907/SEM.FEMLI10P
       m_ProgramExec BPV212
       m_UtilityExec SYSTSIN
# ********************************************************************         
#  TRI DU FICHIER FEMGA14P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAG
       ;;
(GNMD4PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/P907/SEM.FEMGA14P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 06
 /FIELDS FLD_BI_150_1 150 CH 1
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAH
       ;;
(GNMD4PAH)
       m_CondExec 00,EQ,GNMD4PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEMGA22P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAJ
       ;;
(GNMD4PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/P907/SEM.FEMGA22P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_150_1 150 CH 1
 /FIELDS FLD_CH_1_6 1 CH 06
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAK
       ;;
(GNMD4PAK)
       m_CondExec 00,EQ,GNMD4PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEMGA31P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAM
       ;;
(GNMD4PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/P907/SEM.FEMGA31P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 06
 /FIELDS FLD_BI_150_1 150 CH 1
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAN
       ;;
(GNMD4PAN)
       m_CondExec 00,EQ,GNMD4PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEMGA54P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAQ
       ;;
(GNMD4PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/P907/SEM.FEMGA54P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 06
 /FIELDS FLD_BI_150_1 150 CH 1
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAR
       ;;
(GNMD4PAR)
       m_CondExec 00,EQ,GNMD4PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEMLI00P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAT
       ;;
(GNMD4PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/P907/SEM.FEMLI00P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_150_1 150 CH 1
 /FIELDS FLD_CH_1_6 1 CH 06
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAU
       ;;
(GNMD4PAU)
       m_CondExec 00,EQ,GNMD4PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEMLI10P                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PAX
       ;;
(GNMD4PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/P907/SEM.FEMLI10P
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_150_1 150 CH 1
 /FIELDS FLD_CH_1_6 1 CH 06
 /KEYS
   FLD_BI_150_1 ASCENDING,
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 150 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PAY
       ;;
(GNMD4PAY)
       m_CondExec 00,EQ,GNMD4PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950BP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBA
       ;;
(GNMD4PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/P907/SEM.BFT950BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950IP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBB
       ;;
(GNMD4PBB)
       m_CondExec 00,EQ,GNMD4PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950CP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBD
       ;;
(GNMD4PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/P907/SEM.BFT950CP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBE
       ;;
(GNMD4PBE)
       m_CondExec 00,EQ,GNMD4PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950DP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBG
       ;;
(GNMD4PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/P907/SEM.BFT950DP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950KP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBH
       ;;
(GNMD4PBH)
       m_CondExec 00,EQ,GNMD4PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950EP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBJ
       ;;
(GNMD4PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/P907/SEM.BFT950EP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBK
       ;;
(GNMD4PBK)
       m_CondExec 00,EQ,GNMD4PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950FP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBM
       ;;
(GNMD4PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/P907/SEM.BFT950FP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950MP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBN
       ;;
(GNMD4PBN)
       m_CondExec 00,EQ,GNMD4PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BFT950GP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBQ
       ;;
(GNMD4PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/P907/SEM.BFT950GP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950OP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
/COPY                                                                
                                                                       
/FIELDS                                                              
    F_before 1 character 149,                                         
                                                                       
    F_current 150 character 1                                     
                                                                       
/COND COND_01 F_current = X"31"                                      
/COND COND_02 F_current = X"32"                                      
/COND COND_03 F_current = X"33"                                      
                                                                       
/DERIVEDFIELD V_TRANSLATED                                           
    if COND_01 then                                                  
        FUNCTIONCALL Translate(F_current, X"31", X"20")              
    else                                                             
    if COND_02 then                                                  
        FUNCTIONCALL Translate(F_current, X"32", X"20")              
    else                                                             
    if COND_03 then                                                  
        FUNCTIONCALL Translate(F_current, X"33", X"20")              
    else                                                             
        F_current                                                     
                                                                       
/REFORMAT                                                            
    F_before,                                                        
    V_TRANSLATED
F_BEFORE 1 CHARACTER 149,
F_CURRENT 150 CHARACTER 1
/DERIVEDFIELD V_TRANSLATED FUNCTIONCALL
TRANSLATE(F_CURRENT,X"F0",X"40")
/REFORMAT
F_BEFORE,
V_TRANSLATED
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PBR
       ;;
(GNMD4PBR)
       m_CondExec 00,EQ,GNMD4PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTGA00                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBT
       ;;
(GNMD4PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSPUNCH /dev/null
#                                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC00 ${DATA}/PTEM/GNMD4PBT.URTGA0AP
#                                                                              
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PBT.sysin
       m_DBHpuUnload -q SYSIN -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GNMD4PBU
       ;;
(GNMD4PBU)
       m_CondExec 04,GE,GNMD4PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER URTGA0AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PBX
       ;;
(GNMD4PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GNMD4PBT.URTGA0AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD4PBX.URTGA0BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_76_6 76 CH 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_18_6 18 CH 6
 /FIELDS FLD_CH_68_6 68 CH 6
 /FIELDS FLD_CH_49_2 49 CH 2
 /FIELDS FLD_CH_53_12 53 CH 12
 /FIELDS FLD_CH_1_81 1 CH 81
 /FIELDS FLD_CH_26_21 26 CH 21
 /KEYS
   FLD_CH_1_81 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_7,FLD_CH_10_6,FLD_CH_18_6,FLD_CH_26_21,FLD_CH_49_2,FLD_CH_53_12,FLD_CH_68_6,FLD_CH_76_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GNMD4PBY
       ;;
(GNMD4PBY)
       m_CondExec 00,EQ,GNMD4PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER URTGA0AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCA
       ;;
(GNMD4PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.ENTETE.RTGA00
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PTEM/GNMD4PBX.URTGA0BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_150 1 CH 150
 /KEYS
   FLD_CH_1_150 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PCB
       ;;
(GNMD4PCB)
       m_CondExec 00,EQ,GNMD4PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTGA67                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCD
       ;;
(GNMD4PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSPUNCH /dev/null
#                                                                              
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC00 ${DATA}/PTEM/GNMD4PCD.RTGA67AP
#                                                                              
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PCD.sysin
       m_DBHpuUnload -q SYSIN -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GNMD4PCE
       ;;
(GNMD4PCE)
       m_CondExec 04,GE,GNMD4PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RTGA67AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCG
       ;;
(GNMD4PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GNMD4PCD.RTGA67AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD4PCG.RTGA67BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_8 6 CH 8
 /FIELDS FLD_CH_16_12 16 CH 12
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_1_27 1 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_6_8,FLD_CH_16_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GNMD4PCH
       ;;
(GNMD4PCH)
       m_CondExec 00,EQ,GNMD4PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RTGA67AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCJ
       ;;
(GNMD4PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.ENTETE.RTGA67
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/GNMD4PCG.RTGA67BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/P907/SEM.BFT950XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_150 1 CH 150
 /KEYS
   FLD_CH_1_150 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PCK
       ;;
(GNMD4PCK)
       m_CondExec 00,EQ,GNMD4PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTFR50                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCM
       ;;
(GNMD4PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSPUNCH /dev/null
#                                                                              
#    RSFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFR50 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC00 ${DATA}/PTEM/GNMD4PCM.URTFR5AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PCM.sysin
       m_DBHpuUnload -q SYSIN -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GNMD4PCN
       ;;
(GNMD4PCN)
       m_CondExec 04,GE,GNMD4PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER URTFR5AP                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCQ
       ;;
(GNMD4PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GNMD4PCM.URTFR5AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GNMD4PCQ.URTFR5BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_33 1 CH 33
 /KEYS
   FLD_CH_1_33 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_33
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GNMD4PCR
       ;;
(GNMD4PCR)
       m_CondExec 00,EQ,GNMD4PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER URTFR5AP                                                     
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCT
       ;;
(GNMD4PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SORTIN ${DATA}/PXX0.F07.ENTETE.RTFR50
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/GNMD4PCQ.URTFR5BP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.URTFR5CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_150 1 CH 150
 /KEYS
   FLD_CH_1_150 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GNMD4PCU
       ;;
(GNMD4PCU)
       m_CondExec 00,EQ,GNMD4PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER VERS BFT950HP VERS GATEWAY ( FEMGA00_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADC      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950HP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950IP VERS GATEWAY ( FEMGA14_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADH      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950IP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950JP VERS GATEWAY ( FEMGA22_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADM      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950JP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950KP VERS GATEWAY ( FEMGA31_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADR      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950KP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950LP VERS GATEWAY ( FEMGA54_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# * ABE      STEP  PGM=IEFBR14,PATTERN=CFT                                     
# * CFTIN    DATA  *                                                           
# * SEND PART=XFBPRO,                                                          
# *     IDF=BFT950LP                                                           
# *         DATAEND                                                            
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950MP VERS GATEWAY ( FEMLI00_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# ADW      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950MP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS BFT950OP VERS GATEWAY ( FEMLI10_YYYYMMDD.CSV          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AEB      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BFT950OP                                                            
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PCX PGM=EZACFSM1   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PCX
       ;;
(GNMD4PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PCX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDA PGM=FTP        ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDA
       ;;
(GNMD4PDA)
       m_CondExec ${EXAEQ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDD PGM=EZACFSM1   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDD
       ;;
(GNMD4PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDG PGM=EZACFSM1   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDG
       ;;
(GNMD4PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A21} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDJ PGM=FTP        ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDJ
       ;;
(GNMD4PDJ)
       m_CondExec ${EXAFF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+2),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDM PGM=EZACFSM1   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDM
       ;;
(GNMD4PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDQ PGM=EZACFSM1   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDQ
       ;;
(GNMD4PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A23} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDT PGM=FTP        ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDT
       ;;
(GNMD4PDT)
       m_CondExec ${EXAFU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+3),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PDX PGM=EZACFSM1   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PDX
       ;;
(GNMD4PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PDX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEA PGM=EZACFSM1   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEA
       ;;
(GNMD4PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A25} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PED PGM=FTP        ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PED
       ;;
(GNMD4PED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PED.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+4),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEG PGM=EZACFSM1   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEG
       ;;
(GNMD4PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEG.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEJ PGM=EZACFSM1   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEJ
       ;;
(GNMD4PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A27} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEM PGM=FTP        ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEM
       ;;
(GNMD4PEM)
       m_CondExec ${EXAGY},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+5),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEQ PGM=EZACFSM1   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEQ
       ;;
(GNMD4PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PET PGM=EZACFSM1   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PET
       ;;
(GNMD4PET)
       m_CondExec ${EXAHI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PET.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A29} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PEX PGM=FTP        ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PEX
       ;;
(GNMD4PEX)
       m_CondExec ${EXAHN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PEX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+6),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PFA PGM=EZACFSM1   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PFA
       ;;
(GNMD4PFA)
       m_CondExec ${EXAHS},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PFA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTGNMD4P   NOUVEL ENVOI                             
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PFD PGM=EZACFSM1   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PFD
       ;;
(GNMD4PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PFD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A31} SYSOUT ${DATA}/PTEM/GNMD4PCX.FTGNMD4P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTGNMD4P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PFG PGM=FTP        ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PFG
       ;;
(GNMD4PFG)
       m_CondExec ${EXAIC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PFG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.GNMD4PCX.FTGNMD4P(+7),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP GNMD4PFJ PGM=EZACFSM1   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PFJ
       ;;
(GNMD4PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PFJ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GNMD4PZA
       ;;
(GNMD4PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GNMD4PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
