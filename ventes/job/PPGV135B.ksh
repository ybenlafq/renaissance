#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPGV135B.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV135B -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: GV135P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
# **************************************                                       
#  QUIESCE DES TABLES RTHV02 RTHV04 RTHV08 RTHV12                              
#  REPRISE: OUI                                                                
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GV135PB
       ;;
(GV135PB)
#
#GV135PBA
#GV135PBA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GV135PBA
#
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXCAU=${EXCAU:-0}
       EXCAZ=${EXCAZ:-0}
       EXCBE=${EXCBE:-0}
       EXCBJ=${EXCBJ:-0}
       EXCBO=${EXCBO:-0}
       EXCBT=${EXCBT:-0}
       EXCBY=${EXCBY:-0}
       EXCCD=${EXCCD:-0}
       EXCCI=${EXCCI:-0}
       EXCCN=${EXCCN:-0}
       EXCCS=${EXCCS:-0}
       EXCCX=${EXCCX:-0}
       EXCDC=${EXCDC:-0}
       EXCDH=${EXCDH:-0}
       EXCDM=${EXCDM:-0}
       EXCDR=${EXCDR:-0}
       EXCDW=${EXCDW:-0}
       EXCEB=${EXCEB:-0}
       EXCEG=${EXCEG:-0}
       EXCEL=${EXCEL:-0}
       EXCEQ=${EXCEQ:-0}
       EXCEV=${EXCEV:-0}
       EXCFA=${EXCFA:-0}
       EXC98=${EXC98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+2'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2010/03/16 AT 11.09.17 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: GV135P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'GEST� VENTES DAILY'                    
# *                           APPL...: IMPPARIS                                
# *                           BACKOUT: NONE                                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GV135PBD
       ;;
(GV135PBD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.BGV137AP
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 SORTOUT ${DATA}/PTEM/GV135PBD.BGV137CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PBE
       ;;
(GV135PBE)
       m_CondExec 00,EQ,GV135PBD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV138 :           PRISE EN COMPTE DES VENTES SAISIES EN MAGASIN            
#  CREE FIC FGV138                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBG PGM=IKJEFT01   ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBG
       ;;
(GV135PBG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# *******                                                                      
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# *******                                                                      
#    RSGG40   : NAME=RSGG40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG40 /dev/null
# *******                                                                      
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
# *******                                                                      
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******* SORTIE                                                               
#    RSVE10   : NAME=RSVE10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE10 /dev/null
#    RSVE11   : NAME=RSVE11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSVE11 /dev/null
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 FGV138 ${DATA}/PXX0/F07.BGV138AP
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV105 ${DATA}/PXX0/F07.BHV105AP
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV138 
       JUMP_LABEL=GV135PBH
       ;;
(GV135PBH)
       m_CondExec 04,GE,GV135PBG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FIC FGV135 FGV137                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBJ PGM=SORT       ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBJ
       ;;
(GV135PBJ)
       m_CondExec ${EXCAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.BGV135AP
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PTEM/GV135PBD.BGV137CP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F07.BGV138AP
       m_FileAssign -d NEW,CATLG,DELETE -r 117 -g +1 SORTOUT ${DATA}/PTEM/GV135PBJ.BGV142AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_CH_69_5 69 CH 5
 /FIELDS FLD_CH_74_5 74 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_80_3 80 CH 3
 /FIELDS FLD_CH_64_5 64 CH 5
 /FIELDS FLD_CH_88_3 88 CH 3
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_59_5 59 CH 5
 /FIELDS FLD_CH_48_3 48 CH 3
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_54_5 54 CH 5
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_8 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_80_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_48_3,
    TOTAL FLD_CH_51_3,
    TOTAL FLD_CH_54_5,
    TOTAL FLD_CH_59_5,
    TOTAL FLD_CH_64_5,
    TOTAL FLD_CH_69_5,
    TOTAL FLD_CH_74_5,
    TOTAL FLD_CH_83_5,
    TOTAL FLD_CH_88_3,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PBK
       ;;
(GV135PBK)
       m_CondExec 00,EQ,GV135PBJ ${EXCAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG : BGV145                                                              
#   CREE FIC FGV137       VENTES ARTICLES NON NCP A RECYCLER                   
#   CREE FIC FGV145       VENTES TRAITEES SOCIETE ARTICLE JOUR                 
#   CREE FIC FGV147       VENTES TRAITEES SOCIETE MAG ARTICLE MOIS             
#   CREE FIC FGV148       DONNEES STATISTIQUES COMMERCIALES DES VENTES         
#                         PAR SOCIETE, MAGASIN, ARTICLE ET MOIS                
#   CREE FIC FGV149       VENTES TRAITEES SOCIETE FAMILLE MOIS                 
#   CREE FIC FGV150       FICHIER DES RESULTATS (INUTILIS� A DAL)              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBM PGM=IKJEFT01   ** ID=CAU                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBM
       ;;
(GV135PBM)
       m_CondExec ${EXCAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLE                                                        
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* VENTES DU JOUR                                                       
       m_FileAssign -d SHR -g ${G_A3} FGV142 ${DATA}/PTEM/GV135PBJ.BGV142AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGV137 ${DATA}/PGV0/F07.BGV137AP
# *****   FAYCAL METTRE CE FICHIER PASSE DE 74 A 100                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FGV145 ${DATA}/PTEM/GV135PBM.BGV145AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV147 ${DATA}/PTEM/GV135PBM.BGV147AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV149 ${DATA}/PTEM/GV135PBM.BGV149AP
       m_FileAssign -d SHR FGV150 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV148 ${DATA}/PXX0/F07.BGV148AP
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE : 907                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV145 
       JUMP_LABEL=GV135PBN
       ;;
(GV135PBN)
       m_CondExec 04,GE,GV135PBM ${EXCAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES GROUPES DE PRODUITS SERVANT A CHARGERLA RTHV12           
#  SUR NSOCIETE NLIEU NVENTE GRP-CODIC CMODDEL NCODIC DVENTECIALE              
#        1,3     4,3   7,7     14,7      21,3   24,7    31,8                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBQ PGM=SORT       ** ID=CAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBQ
       ;;
(GV135PBQ)
       m_CondExec ${EXCAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FIC DES VENTES PAR GROUPE DE PRODUITS DU JOUR                        
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F07.BGV135CP
# ******  FIC SERVANT A LA MAJ DE RTHV12                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g ${G_A4} SORTOUT ${DATA}/PXX0/F07.BHV105AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_31_8 31 CH 8
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_14_7 14 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PBR
       ;;
(GV135PBR)
       m_CondExec 00,EQ,GV135PBQ ${EXCAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV105  : MAJ DE L'HISTO VENTES COMMERCIALES RTHV12 A PARTIR DES            
#            VENTES PAR GROUPE DE PRODUITS EXTRAITES PAR BGV135                
#                                                                              
#            POUR CHAQUE VENTE DU FICHIER BHV105AM :                           
#            CALCUL DU CHIFFRE AFFAIRE(PCA) ET DU SON MONTANT(PMTACHAT         
#  REPRISE : OUI SI FIN ANORMALE .                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBT PGM=IKJEFT01   ** ID=CBE                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBT
       ;;
(GV135PBT)
       m_CondExec ${EXCBE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES VENTES PAR GROUPE DE PRODUITS DU JOUR                        
       m_FileAssign -d SHR -g ${G_A5} FHV105 ${DATA}/PXX0/F07.BHV105AP
# ******* TABLE ARTICLES                                                       
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* LIENS ENTRES ARTICLES                                                
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ******* TABLE MAGASINS DU GROUPE                                             
#    RSRM05   : NAME=RSRM05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM05 /dev/null
# ******* TABLE FAMILLES TRAITES                                               
#    RSRM10   : NAME=RSRM10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM10 /dev/null
#                                                                              
# ******* HISTO VENTES COMMERCIALES PAR GROUPE DE PRODUITS                     
#    RSHV12   : NAME=RSHV12,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV12 /dev/null
# ******* ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV105 
       JUMP_LABEL=GV135PBU
       ;;
(GV135PBU)
       m_CondExec 04,GE,GV135PBT ${EXCBE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV165 :           PREPARATION EPURATION DES TABLES HV                      
#  CREE FIC FGV165       SOC   ARTICLE   JOUR                                  
#  CREE FIC FGV169       SOC   FAMILLE   JOUR                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PBX PGM=IKJEFT01   ** ID=CBJ                                   
# ***********************************                                          
       JUMP_LABEL=GV135PBX
       ;;
(GV135PBX)
       m_CondExec ${EXCBJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* HISTO VENTE/SOC,ARTICLE,JOUR                                         
#    RSHV02   : NAME=RSHV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV02 /dev/null
# ******* HISTO VENTE/SOC,FAMILLE,JOUR                                         
#    RSHV08   : NAME=RSHV08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV08 /dev/null
#                                                                              
# ******* FILE FHV02 DE LA VEILLE A VIDE POUR LE 1ER PASSAGE                   
       m_FileAssign -d SHR -g +0 FHV02E ${DATA}/PXX0/F07.BGV145CP.HISTO
# ******* SORTIE                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 FGV165 ${DATA}/PTEM/GV135PBX.BGV165AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV169 ${DATA}/PTEM/GV135PBX.BGV169AP
# *******                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 FHV02S ${DATA}/PTEM/GV135PBX.BGV165BP
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  PARAMETRE DE PURGE DES TABLES HV                                     
#         POS 01 A  4 : NB JOURS A CONSERVER    SUR RTHV02  => 0045            
#         POS 05 A  8 : NB SEMAINES A CONSERVER SUR RTHV04  => 0056            
#         POS 09 A 12 : NB JOURS A CONSERVER    SUR RTHV08  => 1110            
#         POS 13 A 16 : NB JOURS A CONSERVER    SUR RTHV12 (IDEM RTHV0         
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV135PBX
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV165 
       JUMP_LABEL=GV135PBY
       ;;
(GV135PBY)
       m_CondExec 04,GE,GV135PBX ${EXCBJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV166 :           PREPARATION EPURATION DES TABLES HV04 ET HV12            
#  CREE FIC FGV167       SOC   ARTICLE   MAG     SEMAINE                       
#  CREE FIC FGV171       SOC   MAG       CODIC   CODIC-ELEMENT  JOUR           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCA PGM=IKJEFT01   ** ID=CBO                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCA
       ;;
(GV135PCA)
       m_CondExec ${EXCBO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* HISTO VENTE/SOC,ARTICLE,MAG,MOIS                                     
#    RSHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV04 /dev/null
# ******* HISTO VENTE/SOC,MAG,GROUPE-PRODUIT,ARTICLE,JOUR                      
#    RSHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV12 /dev/null
# ******* HISTO VENTE                                                          
#    RSHV84   : NAME=RSHV84,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV84 /dev/null
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  PARAMETRE DE PURGE DES TABLES HV                                     
#         POS 01 A  3 : CODE SOCIETE                                           
#         POS 04 A  7 : NB SEMAINES A CONSERVER SUR RTHV84  => 0056            
#         POS 08 A 11 : NB JOURS A CONSERVER    SUR RTHV12  => 1110            
#         POS 12 A 15 : NB SEMAINES A CONSERVER SUR RTHV04  => 0013            
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV135PCA
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV167 ${DATA}/PTEM/GV135PCA.BGV167AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 FGV168 ${DATA}/PXX0/F07.BGV168AP
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -g +1 FGV171 ${DATA}/PTEM/GV135PCA.BGV171AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV166 
       JUMP_LABEL=GV135PCB
       ;;
(GV135PCB)
       m_CondExec 04,GE,GV135PCA ${EXCBO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FIC FGV145                                                             
#   1,18,A  AVEC OUTREC FIELDS 1,51,75,26                                      
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCD PGM=SORT       ** ID=CBT                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCD
       ;;
(GV135PCD)
       m_CondExec ${EXCBT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GV135PBM.BGV145AP
# ****    FAYCAL A CREER EN 77 DE LONG                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 SORTOUT ${DATA}/PTEM/GV135PCD.BGV145BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_51 1 CH 51
 /FIELDS FLD_CH_1_18 1 CH 18
 /FIELDS FLD_CH_75_26 75 CH 26
 /KEYS
   FLD_CH_1_18 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51,FLD_CH_75_26
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GV135PCE
       ;;
(GV135PCE)
       m_CondExec 00,EQ,GV135PCD ${EXCBT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION AVEC  FIC FGV145 TRIE                                           
#   1,3,A SOCIETE 4,7,A ARTICLE 11,8,A DATE VENTE                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCG PGM=SORT       ** ID=CBY                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCG
       ;;
(GV135PCG)
       m_CondExec ${EXCBY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GV135PCD.BGV145BP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GV135PBX.BGV165AP
       m_FileAssign -d NEW,CATLG,DELETE -r 77 -g +1 SORTOUT ${DATA}/PGV0/F07.RELOAD.HV02RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_39_3 39 PD 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_65_3 65 PD 3
 /FIELDS FLD_PD_22_5 22 PD 5
 /FIELDS FLD_PD_60_5 60 PD 5
 /FIELDS FLD_PD_19_3 19 PD 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_PD_73_5 73 PD 5
 /FIELDS FLD_PD_47_5 47 PD 5
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_PD_52_3 52 PD 3
 /FIELDS FLD_PD_27_5 27 PD 5
 /FIELDS FLD_PD_68_5 68 PD 5
 /FIELDS FLD_PD_42_5 42 PD 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_3,
    TOTAL FLD_PD_22_5,
    TOTAL FLD_PD_27_5,
    TOTAL FLD_PD_39_3,
    TOTAL FLD_PD_42_5,
    TOTAL FLD_PD_47_5,
    TOTAL FLD_PD_52_3,
    TOTAL FLD_PD_55_5,
    TOTAL FLD_PD_60_5,
    TOTAL FLD_PD_65_3,
    TOTAL FLD_PD_68_5,
    TOTAL FLD_PD_73_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PCH
       ;;
(GV135PCH)
       m_CondExec 00,EQ,GV135PCG ${EXCBY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTHV02                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCJ PGM=DSNUTILB   ** ID=CCD                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCJ
       ;;
(GV135PCJ)
       m_CondExec ${EXCCD},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV02                                                
       m_FileAssign -d SHR -g ${G_A9} SYSREC ${DATA}/PGV0/F07.RELOAD.HV02RP
#    RSHV02   : NAME=RSHV02,MODE=(U,N) - DYNAM=YES                             
# -X-RSHV02   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV02 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PCJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PPGV135B_GV135PCJ_RTHV02.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135PCK
       ;;
(GV135PCK)
       m_CondExec 04,GE,GV135PCJ ${EXCCD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FIC FGV145                                                          
#   1,18,A AVEC OUTREC FIELDS 1,31,39,36                                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCM PGM=SORT       ** ID=CCI                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCM
       ;;
(GV135PCM)
       m_CondExec ${EXCCI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/GV135PBM.BGV145AP
#  FAYCAL FIC A CREER EN 93 = FHV02 DU JOUR                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PTEM/GV135PCM.BGV145DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_31 1 CH 31
 /FIELDS FLD_CH_39_62 39 CH 62
 /FIELDS FLD_CH_1_18 1 CH 18
 /KEYS
   FLD_CH_1_18 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_31,FLD_CH_39_62
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GV135PCN
       ;;
(GV135PCN)
       m_CondExec 00,EQ,GV135PCM ${EXCCI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION AVEC  FIC FGV147 TOUTES FILIALES                                
# ********************************************************************         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCQ PGM=SORT       ** ID=CCN                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCQ
       ;;
(GV135PCQ)
       m_CondExec ${EXCCN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGV147AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BGV147AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BGV147AM
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PTEM/GV135PBM.BGV147AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BGV147AY
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PTEM/GV135PCA.BGV167AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BGV147AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/P908/SEM.BGV147AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRM600CP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 SORTOUT ${DATA}/PTEM/GV135PCQ.HV04XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_28_5 28 PD 5
 /FIELDS FLD_PD_63_3 63 PD 3
 /FIELDS FLD_PD_45_3 45 PD 3
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_84_5 84 PD 5
 /FIELDS FLD_PD_79_5 79 PD 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_PD_76_3 76 PD 3
 /FIELDS FLD_PD_40_5 40 PD 5
 /FIELDS FLD_PD_23_5 23 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_58_5 58 PD 5
 /FIELDS FLD_PD_66_5 66 PD 5
 /FIELDS FLD_PD_89_5 89 PD 5
 /FIELDS FLD_PD_71_5 71 PD 5
 /FIELDS FLD_PD_48_5 48 PD 5
 /FIELDS FLD_PD_94_5 94 PD 5
 /FIELDS FLD_PD_53_5 53 PD 5
 /FIELDS FLD_CH_14_6 14 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_3,
    TOTAL FLD_PD_23_5,
    TOTAL FLD_PD_28_5,
    TOTAL FLD_PD_40_5,
    TOTAL FLD_PD_45_3,
    TOTAL FLD_PD_48_5,
    TOTAL FLD_PD_53_5,
    TOTAL FLD_PD_58_5,
    TOTAL FLD_PD_63_3,
    TOTAL FLD_PD_66_5,
    TOTAL FLD_PD_71_5,
    TOTAL FLD_PD_76_3,
    TOTAL FLD_PD_79_5,
    TOTAL FLD_PD_84_5,
    TOTAL FLD_PD_89_5,
    TOTAL FLD_PD_94_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PCR
       ;;
(GV135PCR)
       m_CondExec 00,EQ,GV135PCQ ${EXCCN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FUSION AVEC LE FICHIER MGI ISSU DE RM601P                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCT PGM=SORT       ** ID=CCS                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCT
       ;;
(GV135PCT)
       m_CondExec ${EXCCS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GV135PCQ.HV04XP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 SORTOUT ${DATA}/PTEM/GV135PCT.HV04RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_6 14 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_6 ASCENDING
 /* Record Type = F  Record Length = 98 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PCU
       ;;
(GV135PCU)
       m_CondExec 00,EQ,GV135PCT ${EXCCS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV04                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PCX PGM=DSNUTILB   ** ID=CCX                                   
# ***********************************                                          
       JUMP_LABEL=GV135PCX
       ;;
(GV135PCX)
       m_CondExec ${EXCCX},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD RTHV04                                                   
       m_FileAssign -d SHR -g ${G_A14} SYSREC ${DATA}/PTEM/GV135PCT.HV04RP
#    RSHV04   : NAME=RSHV04,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV04 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PCX.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PPGV135B_GV135PCX_RTHV04.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135PCY
       ;;
(GV135PCY)
       m_CondExec 04,GE,GV135PCX ${EXCCX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AJOUT DANS LE SUM DE 68,5                                                   
# ********************************************************************         
#   TRI FUSION AVEC  FIC FGV149                                                
#   1,3,A SOCIETE 4,5,A FAMILLE 9,8,A DATE DE VENTE                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDA PGM=SORT       ** ID=CDC                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDA
       ;;
(GV135PDA)
       m_CondExec ${EXCDC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GV135PBM.BGV149AP
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/GV135PBX.BGV169AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 SORTOUT ${DATA}/PXX0/F07.HV08RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_33_5 33 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_23_5 23 PD 5
 /FIELDS FLD_PD_89_5 89 PD 5
 /FIELDS FLD_PD_58_5 58 PD 5
 /FIELDS FLD_CH_4_5 4 CH 5
 /FIELDS FLD_PD_63_5 63 PD 5
 /FIELDS FLD_PD_94_5 94 PD 5
 /FIELDS FLD_PD_76_5 76 PD 5
 /FIELDS FLD_PD_81_5 81 PD 5
 /FIELDS FLD_CH_9_8 9 CH 8
 /FIELDS FLD_PD_68_5 68 PD 5
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_28_5 28 PD 5
 /FIELDS FLD_PD_43_5 43 PD 5
 /FIELDS FLD_PD_86_3 86 PD 3
 /FIELDS FLD_PD_38_5 38 PD 5
 /FIELDS FLD_PD_73_3 73 PD 3
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_PD_55_3 55 PD 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_5 ASCENDING,
   FLD_CH_9_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_17_3,
    TOTAL FLD_PD_20_3,
    TOTAL FLD_PD_23_5,
    TOTAL FLD_PD_28_5,
    TOTAL FLD_PD_33_5,
    TOTAL FLD_PD_38_5,
    TOTAL FLD_PD_43_5,
    TOTAL FLD_PD_55_3,
    TOTAL FLD_PD_58_5,
    TOTAL FLD_PD_63_5,
    TOTAL FLD_PD_68_5,
    TOTAL FLD_PD_73_3,
    TOTAL FLD_PD_76_5,
    TOTAL FLD_PD_81_5,
    TOTAL FLD_PD_86_3,
    TOTAL FLD_PD_89_5,
    TOTAL FLD_PD_94_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PDB
       ;;
(GV135PDB)
       m_CondExec 00,EQ,GV135PDA ${EXCDC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV08                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDD PGM=DSNUTILB   ** ID=CDH                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDD
       ;;
(GV135PDD)
       m_CondExec ${EXCDH},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV08                                                
       m_FileAssign -d SHR -g ${G_A17} SYSREC ${DATA}/PXX0/F07.HV08RP
#    RSHV08   : NAME=RSHV08,MODE=(U,N) - DYNAM=YES                             
# -X-RSHV08   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV08 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PDD.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PPGV135B_GV135PDD_RTHV08.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135PDE
       ;;
(GV135PDE)
       m_CondExec 04,GE,GV135PDD ${EXCDH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MODIF DU 27.05.97 FRED POUR PASSER DANS L'INDEX CLUSTER                    
#            AVEC NCODIC NCODICLIE DVENTECIALE NSOCIETE NLIEU                  
#                 7,7    14,7      21,8        1,3       4,3                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDG PGM=SORT       ** ID=CDM                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDG
       ;;
(GV135PDG)
       m_CondExec ${EXCDM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/GV135PCA.BGV171AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BRM600DP
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -g +1 SORTOUT ${DATA}/PTEM/GV135PDG.HV12RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_PD_72_3 72 PD 3
 /FIELDS FLD_PD_62_5 62 PD 5
 /FIELDS FLD_PD_80_5 80 PD 5
 /FIELDS FLD_PD_93_5 93 PD 5
 /FIELDS FLD_PD_37_5 37 PD 5
 /FIELDS FLD_PD_49_3 49 PD 3
 /FIELDS FLD_PD_75_5 75 PD 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_PD_67_5 67 PD 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_88_5 88 PD 5
 /FIELDS FLD_CH_21_8 21 CH 8
 /FIELDS FLD_PD_29_3 29 PD 3
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_52_5 52 PD 5
 /FIELDS FLD_PD_32_5 32 PD 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_PD_57_5 57 PD 5
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_21_8 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_29_3,
    TOTAL FLD_PD_32_5,
    TOTAL FLD_PD_37_5,
    TOTAL FLD_PD_49_3,
    TOTAL FLD_PD_52_5,
    TOTAL FLD_PD_57_5,
    TOTAL FLD_PD_62_5,
    TOTAL FLD_PD_67_5,
    TOTAL FLD_PD_72_3,
    TOTAL FLD_PD_75_5,
    TOTAL FLD_PD_80_5,
    TOTAL FLD_PD_85_3,
    TOTAL FLD_PD_88_5,
    TOTAL FLD_PD_93_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PDH
       ;;
(GV135PDH)
       m_CondExec 00,EQ,GV135PDG ${EXCDM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTHV12                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDJ PGM=DSNUTILB   ** ID=CDR                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDJ
       ;;
(GV135PDJ)
       m_CondExec ${EXCDR},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTHV12                                                
       m_FileAssign -d SHR -g ${G_A19} SYSREC ${DATA}/PTEM/GV135PDG.HV12RP
#    RSHV12   : NAME=RSHV12,MODE=(U,N) - DYNAM=YES                             
# -X-RSHV12   - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV12 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PDJ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/PPGV135B_GV135PDJ_RTHV12.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV135PDK
       ;;
(GV135PDK)
       m_CondExec 04,GE,GV135PDJ ${EXCDR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER BGV137AP                                                    
#   19,5,A CODE TYPE 24,5 MARQUE 1,7 ARTICLE 8,3 MAG 11,8 DATE VENTE           
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDM PGM=SORT       ** ID=CDW                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDM
       ;;
(GV135PDM)
       m_CondExec ${EXCDW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PGV0/F07.BGV137AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GV135PDM.BGV137BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_24_5 24 CH 5
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_19_5 ASCENDING,
   FLD_CH_24_5 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_11_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PDN
       ;;
(GV135PDN)
       m_CondExec 00,EQ,GV135PDM ${EXCDW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  MODIF BBTE LE PROG BGV155 A ETE RENAME EN BGV155X DANS LA MVS               
# ********************************************************************         
#  EDITION DES ANOMALIES DES ARTICLES (SI DATE=LENDEMAIN DANS FIC STOC         
#  BGV155                                                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDQ PGM=IKJEFT01   ** ID=CEB                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDQ
       ;;
(GV135PDQ)
       m_CondExec ${EXCEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSPT03   : NAME=RSPT03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT03 /dev/null
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES VENTES DES ARTICLES A RECYCLER                           
       m_FileAssign -d SHR -g ${G_A21} FGV137 ${DATA}/PTEM/GV135PDM.BGV137BP
# ******  EDITION DES ANOMALIES PLUS D'UTILISATEUR                             
# IGV155   REPORT SYSOUT=(9,BGV155)                                            
       m_OutputAssign -c "*" IGV155
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV155 
       JUMP_LABEL=GV135PDR
       ;;
(GV135PDR)
       m_CondExec 04,GE,GV135PDQ ${EXCEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FIC BGV145DP ET BGV165BP                                               
#   AVEC SUM FIELDS                                                            
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDT PGM=SORT       ** ID=CEG                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDT
       ;;
(GV135PDT)
       m_CondExec ${EXCEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/GV135PCM.BGV145DP
       m_FileAssign -d SHR -g ${G_A23} -C ${DATA}/PTEM/GV135PBX.BGV165BP
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/F07.BGV145CP.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_45_3 45 PD 3
 /FIELDS FLD_PD_40_5 40 PD 5
 /FIELDS FLD_PD_27_5 27 PD 5
 /FIELDS FLD_PD_89_5 89 PD 5
 /FIELDS FLD_PD_19_3 19 PD 3
 /FIELDS FLD_PD_81_3 81 PD 3
 /FIELDS FLD_PD_76_5 76 PD 5
 /FIELDS FLD_PD_32_3 32 PD 3
 /FIELDS FLD_PD_68_3 68 PD 3
 /FIELDS FLD_PD_48_5 48 PD 5
 /FIELDS FLD_PD_22_5 22 PD 5
 /FIELDS FLD_PD_58_5 58 PD 5
 /FIELDS FLD_PD_71_5 71 PD 5
 /FIELDS FLD_PD_63_5 63 PD 5
 /FIELDS FLD_PD_53_5 53 PD 5
 /FIELDS FLD_CH_1_18 1 CH 18
 /FIELDS FLD_PD_35_5 35 PD 5
 /FIELDS FLD_PD_84_5 84 PD 5
 /KEYS
   FLD_CH_1_18 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_3,
    TOTAL FLD_PD_22_5,
    TOTAL FLD_PD_27_5,
    TOTAL FLD_PD_32_3,
    TOTAL FLD_PD_35_5,
    TOTAL FLD_PD_40_5,
    TOTAL FLD_PD_45_3,
    TOTAL FLD_PD_48_5,
    TOTAL FLD_PD_53_5,
    TOTAL FLD_PD_58_5,
    TOTAL FLD_PD_63_5,
    TOTAL FLD_PD_68_3,
    TOTAL FLD_PD_71_5,
    TOTAL FLD_PD_76_5,
    TOTAL FLD_PD_81_3,
    TOTAL FLD_PD_84_5,
    TOTAL FLD_PD_89_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PDU
       ;;
(GV135PDU)
       m_CondExec 00,EQ,GV135PDT ${EXCEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV175  EPURATION DU FICHIER HISTORIQUE                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PDX PGM=IKJEFT01   ** ID=CEL                                   
# ***********************************                                          
       JUMP_LABEL=GV135PDX
       ;;
(GV135PDX)
       m_CondExec ${EXCEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ******  FICHIERS PARAMETRES                                                  
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FCARTE ${DATA}/CORTEX4.P.MTXTFIX1/GV135PDX
#                                                                              
# *** FICHIER DE LA VEILLE                                                     
       m_FileAssign -d SHR -g +0 FGVHIS ${DATA}/PXX0/F07.BGV178CP
# ***                                                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV178 ${DATA}/PTEM/GV135PDX.BGV178AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV175 
       JUMP_LABEL=GV135PDY
       ;;
(GV135PDY)
       m_CondExec 04,GE,GV135PDX ${EXCEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIERS FGV178 ET FGV148                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PEA PGM=SORT       ** ID=CEQ                                   
# ***********************************                                          
       JUMP_LABEL=GV135PEA
       ;;
(GV135PEA)
       m_CondExec ${EXCEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PXX0/F07.BGV148AP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PTEM/GV135PDX.BGV178AP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PTEM/GV135PEA.BGV178BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_6 14 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PEB
       ;;
(GV135PEB)
       m_CondExec 00,EQ,GV135PEA ${EXCEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV135PED PGM=SORT       ** ID=CEV                                   
# ***********************************                                          
       JUMP_LABEL=GV135PED
       ;;
(GV135PED)
       m_CondExec ${EXCEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PTEM/GV135PEA.BGV178BP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/F07.BGV178CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_23_5 23 PD 5
 /FIELDS FLD_CH_1_19 1 CH 19
 /FIELDS FLD_PD_20_3 20 PD 3
 /FIELDS FLD_PD_28_5 28 PD 5
 /KEYS
   FLD_CH_1_19 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_3,
    TOTAL FLD_PD_23_5,
    TOTAL FLD_PD_28_5
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV135PEE
       ;;
(GV135PEE)
       m_CondExec 00,EQ,GV135PED ${EXCEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES FICHERS BGV147AX DES FILIALES                             
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GV135PEG PGM=IDCAMS     ** ID=CFA                                   
# ***********************************                                          
       JUMP_LABEL=GV135PEG
       ;;
(GV135PEG)
       m_CondExec ${EXCFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT1 ${DATA}/PXX0/F45.BGV147AY
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT2 ${DATA}/PXX0/F89.BGV147AM
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT3 ${DATA}/PXX0/F91.BGV147AD
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT4 ${DATA}/PXX0/F61.BGV147AL
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g ${G_A27} OUT5 ${DATA}/PTEM/GV135PBM.BGV147AP
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT6 ${DATA}/PXX0/F16.BGV147AO
       m_FileAssign -d NEW,CATLG,DELETE -r 98 -g +1 OUT7 ${DATA}/P908/SEM.BGV147AX
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PEG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GV135PEH
       ;;
(GV135PEH)
       m_CondExec 16,NE,GV135PEG ${EXCFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=GV135PZB
       ;;
(GV135PZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV135PZB.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
