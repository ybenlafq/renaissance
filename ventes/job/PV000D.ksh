#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV000D.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPV000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/11/16 AT 15.18.20 BY BURTECA                      
#    STANDARDS: P  JOBSET: PV000D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BPV100  EXTRACTION DES PSE VENDUES                                 
#   REPRISE: OUI SI FIN ANORMALE                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV000DA
       ;;
(PV000DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV000DAA
       ;;
(PV000DAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00D,MODE=(I,N) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE DES MONTANTS DE GARANTIE COMPLEMENTAIRE                               
#    RTGA40D  : NAME=RSGA40D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA40D /dev/null
#  TABLE DES DE GARANTIE COMPLEMENTAIRE                                        
#    RTGA52D  : NAME=RSGA52D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52D /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGV11D  : NAME=RSGV11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11D /dev/null
#  TABLE DES CODES VENDEURS                                                    
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F91.FDPV100D
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  DATE DU JOUR JJMMSSAA                                                       
       m_FileAssign -i FDATE
$FDATE
_end
#  SORTIE FICHIER PRIMES VENDEURS DU JOUR                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100 ${DATA}/PTEM/PV000DAA.BPV100AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV100 
       JUMP_LABEL=PV000DAB
       ;;
(PV000DAB)
       m_CondExec 04,GE,PV000DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV106  EXTRACTION DES VENTES DE PRODUITS GROUPE                   
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAD
       ;;
(PV000DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE DES PARAMETRES ASSOCIES AUX FAMILLES                                  
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#  TABLE DES GARANTIES COMPLEMENTAIRES                                         
#    RTGA52D  : NAME=RSGA52D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52D /dev/null
#  TABLE DES LIENS                                                             
#    RTGA58D  : NAME=RSGA58D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA58D /dev/null
#  TABLE DES ZONES DE PRIX STANDARDS                                           
#    RTGA59D  : NAME=RSGA59D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59D /dev/null
#  TABLE DES COMMISSIONS PAR ZONES DE PRIX                                     
#    RTGA62D  : NAME=RSGA62D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA62D /dev/null
#  TABLE DES HISTORIQUES STATUTS                                               
#    RTGA65D  : NAME=RSGA65D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65D /dev/null
#  TABLE DES HISTORIQUES SENSIBILITES                                          
#    RTGA66D  : NAME=RSGA66D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA66D /dev/null
#  TABLE COMMISSIONS BBTE                                                      
#    RTGA75D  : NAME=RSGA75D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA75D /dev/null
#  TABLE HISTORIQUE DES PRMP                                                   
#    RTGG50D  : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50D /dev/null
#  TABLE HISTORIQUE DES PRMP DACEM                                             
#    RTGG55D  : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55D /dev/null
#  TABLE DES MVTS DE STOCK                                                     
#    RTGS40D  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F91.FDPV100D
#  SORTIE FICHIER VENDEURS SUR CODICS GROUPE                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV106 ${DATA}/PTEM/PV000DAD.BPV106AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV106 
       JUMP_LABEL=PV000DAE
       ;;
(PV000DAE)
       m_CondExec 04,GE,PV000DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV101  PRIMES VENDEURS SUR VENTES DE CODICS ELEMENTS              
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAG
       ;;
(PV000DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10D  : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10D /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14D  : NAME=RSGA14D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14D /dev/null
#  TABLE DES PARAMETRES ASSOCIES AUX FAMILLES                                  
#    RTGA30D  : NAME=RSGA30D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA30D /dev/null
#  TABLE DES GARANTIES COMPLEMENTAIRES                                         
#    RTGA52D  : NAME=RSGA52D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52D /dev/null
#  TABLE DES PRIMES JAUNES                                                     
#    RTGA62D  : NAME=RSGA62D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA62D /dev/null
#  TABLE DES HISTORIQUES STATUTS                                               
#    RTGA65D  : NAME=RSGA65D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65D /dev/null
#  TABLE DES HISTORIQUES SENSIBILITES                                          
#    RTGA66D  : NAME=RSGA66D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA66D /dev/null
#  TABLE COMMISSION BBTE                                                       
#    RTGA75D  : NAME=RSGA75D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA75D /dev/null
#  TABLE DES PRMP                                                              
#    RTGG50D  : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50D /dev/null
#  TABLE HISTORIQUE DES PRMP DACEM                                             
#    RTGG55D  : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55D /dev/null
#  TABLE DES MVTS DE STOCK                                                     
#    RTGS40D  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#  DATE DU DERNIER TRAITEMENT                                                  
       m_FileAssign -d SHR -g +0 FDPV100 ${DATA}/PXX0/F91.FDPV100D
#  SORTIE FICHIER VENDEURS SUR CODICS ELEMENTS                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV101 ${DATA}/PTEM/PV000DAG.BPV101AD
#  DATE DU DERNIER TRAITEMENT GENERATION +1 DU FDPV100                         
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FDPV100O ${DATA}/PXX0/F91.FDPV100D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV101 
       JUMP_LABEL=PV000DAH
       ;;
(PV000DAH)
       m_CondExec 04,GE,PV000DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL DES FICHIERS DU JOUR SUR UN FICHIER HISTORIQUE DU JOUR                
#  TRI  SUR FICHIER 32,10                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAJ
       ;;
(PV000DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV000DAA.BPV100AD
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/PV000DAG.BPV101AD
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/PV000DAD.BPV106AD
# * FICHIER HISTO DU JOUR                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.BPV100JD.JOUR
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_32_10 32 CH 10
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_49_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000DAK
       ;;
(PV000DAK)
       m_CondExec 00,EQ,PV000DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAM PGM=BPV108     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAM
       ;;
(PV000DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  PARAMETRE FMOIS MMSSAA                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F91.PV100HD.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV000DAM.BPV108AD
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI SUR FICHIER ISSU PGM BPV108                                             
#  REPRISE: OUI.                                                               
#  ATTENTION SI NCGFC (BPV11 PARAMETRE = OUI) TRI SUR CODE VENDEUR             
#  SORT FIELDS=(1,3,A,4,3,A,7,6,A,32,10,A,66,1,A),FORMAT=CH                    
#  SINON                                      TRI SUR LIBELLE VENDEUR          
#  SORT FIELDS=(1,3,A,4,3,A,32,10,A,7,6,A,66,1,A),FORMAT=CH                    
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAQ
       ;;
(PV000DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/PV000DAM.BPV108AD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV000DAQ.BPV109AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "G"
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_32_10 32 CH 10
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_16 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_32_10 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000DAR
       ;;
(PV000DAR)
       m_CondExec 00,EQ,PV000DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV109  EDITION DU DETAIL DES VENTES ET DU CUMUL MENSUEL           
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAT
       ;;
(PV000DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00D  : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00D /dev/null
#  TABLE DES ARTICLES GENERALISE                                               
#    RTGA01D  : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31D  : NAME=RSGV31D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31D /dev/null
#  TABLE DES ANOMALIES                                                         
#    RTAN00D  : NAME=RSAN00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00D /dev/null
#  ENTREE JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
#                                                                              
       m_FileAssign -d SHR -g ${G_A5} FPV109J ${DATA}/PEX0/F91.BPV100JD.JOUR
#                                                                              
       m_FileAssign -d SHR -g ${G_A6} FPV109M ${DATA}/PTEM/PV000DAQ.BPV109AD
#  SORTIE FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 IPV109 ${DATA}/PXX0/F91.BPV109MD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV109 
       JUMP_LABEL=PV000DAU
       ;;
(PV000DAU)
       m_CondExec 04,GE,PV000DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV109 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV000DAX
       ;;
(PV000DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F91.BPV109MD
       m_OutputAssign -c 9 -w IPV109 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=PV000DAY
       ;;
(PV000DAY)
       m_CondExec 00,EQ,PV000DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR FICHIERS ISSU DU REGROUPEMENT JOURNALIER + HISTO                    
#  POUR CONSTITUTION FICHIER HISTORIQUE                                        
#  REPRISE: OUI MAIS ATTENTION AUX GENERATIONS                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV000DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV000DBA
       ;;
(PV000DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/PV000DAA.BPV100AD
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/PV000DAG.BPV101AD
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/PV000DAD.BPV106AD
# DOIT ETRE INITIALISE POUR LE 1ER PASSAGE                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PEX0/F91.PV100HD.HISTO
# FICHIER HISTORIQUE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F91.PV100HD.HISTO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_19_5 19 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV000DBB
       ;;
(PV000DBB)
       m_CondExec 00,EQ,PV000DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV000DZA
       ;;
(PV000DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV000DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
