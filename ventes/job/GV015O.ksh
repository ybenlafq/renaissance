#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV015O.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POGV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/12/08 AT 15.57.22 BY PREPA2                       
#    STANDARDS: P  JOBSET: GV015O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='MGIO'                                                              
# ********************************************************************         
#  TRI FIC FHV02 VENANT DE GV135O                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV015OA
       ;;
(GV015OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV015OAA
       ;;
(GV015OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    TABLE COMMERCIAL                  *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BGV145CO.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PTEM/GV015OAA.BGV015GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_11_8 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015OAB
       ;;
(GV015OAB)
       m_CondExec 00,EQ,GV015OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV004                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAD PGM=BGV004     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAD
       ;;
(GV015OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FICHIER ENTRE FHV02 TRI�                                             
       m_FileAssign -d SHR -g ${G_A1} FHV02 ${DATA}/PTEM/GV015OAA.BGV015GO
#                                                                              
# ******* FICHIER SORTIE DE LRECL 80                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FICDAT ${DATA}/PTEM/GV015OAD.BGV014AO
       m_ProgramExec BGV004 
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAG
       ;;
(GV015OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSAN00O  : NAME=RSAN00O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00O /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV015OAG
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PTEM/GV015OAG.BEX011GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV015OAH
       ;;
(GV015OAH)
       m_CondExec 04,GE,GV015OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAJ
       ;;
(GV015OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GV015OAG.BEX011GO
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PTEM/GV015OAJ.BEX011HO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015OAK
       ;;
(GV015OAK)
       m_CondExec 00,EQ,GV015OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FIC FHV02 VENANT DE GV135O SUR SOCIETE ; CODIC ; DATE                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAM
       ;;
(GV015OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BGV145CO.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PTEM/GV015OAM.BGV015JO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_18 1 CH 18
 /KEYS
   FLD_CH_1_18 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015OAN
       ;;
(GV015OAN)
       m_CondExec 00,EQ,GV015OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV016   CALCUL DES VENTES DU MOIS SELON LA SEGMENTATION              
#         VOULUE                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAQ PGM=BGV016     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAQ
       ;;
(GV015OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FICDAT ${DATA}/PTEM/GV015OAD.BGV014AO
#                                                                              
# **  FICHIER FEX001 TRI�                                                      
       m_FileAssign -d SHR -g ${G_A4} FEX001 ${DATA}/PTEM/GV015OAJ.BEX011HO
#                                                                              
# **  FICHIER FHV02 TRI�                                                       
       m_FileAssign -d SHR -g ${G_A5} FHV02 ${DATA}/PTEM/GV015OAM.BGV015JO
#                                                                              
# **  FICHIER DE SORTIE DE LRECL 87                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 FGV016 ${DATA}/PTEM/GV015OAQ.BGV016AO
       m_ProgramExec BGV016 
# ********************************************************************         
#   PGM : BGV015  SI LA DATE FICDAT EST POSTERIEUR AU DEBUT DE MOIS            
#       DE L ANNEE PRECEDENTE LES DONNEES HISTORIQUES SONT EXTRAITES           
#       PAR FAMILLES DEMANDEES PAR L ETAT IGV020                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAT
       ;;
(GV015OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA09O  : NAME=RSGA09O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA09O /dev/null
#                                                                              
#    RSGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
#                                                                              
#    RSGA14O  : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14O /dev/null
#                                                                              
#    RSGA20O  : NAME=RSGA20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA20O /dev/null
#                                                                              
#    RSGA21O  : NAME=RSGA21O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21O /dev/null
#                                                                              
#    RSHV08O  : NAME=RSHV08O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSHV08O /dev/null
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A6} FICDAT ${DATA}/PTEM/GV015OAD.BGV014AO
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 FGV015 ${DATA}/PTEM/GV015OAT.BGV015KO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV015 
       JUMP_LABEL=GV015OAU
       ;;
(GV015OAU)
       m_CondExec 04,GE,GV015OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIER FGV015 ET FGV016                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# ABJ      SORT  SORT=(00087,300000)                                           
#                                                                              
# ***********************************                                          
# *   STEP GV015OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV015OAX
       ;;
(GV015OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GV015OAQ.BGV016AO
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/GV015OAT.BGV015KO
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -g +1 SORTOUT ${DATA}/PTEM/GV015OAX.BGV015LO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_56_4 56 PD 4
 /FIELDS FLD_PD_76_6 76 PD 6
 /FIELDS FLD_PD_72_4 72 PD 4
 /FIELDS FLD_PD_60_6 60 PD 6
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_66_6 66 PD 6
 /KEYS
   FLD_CH_1_39 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_4,
    TOTAL FLD_PD_60_6,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_4,
    TOTAL FLD_PD_76_6,
    TOTAL FLD_PD_82_6
 /* Record Type = F  Record Length = 87 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV015OAY
       ;;
(GV015OAY)
       m_CondExec 00,EQ,GV015OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV020                                                                
#  EDITION ETAT SUIVI DES VENTES HEBDO                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV015OBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV015OBA
       ;;
(GV015OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************                                             
#         TABLES DB2 UTILISEES   *                                             
# ********************************                                             
# ******* TABLE DES FAMILLES                                                   
#    RSGA11O  : NAME=RSGA11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11O /dev/null
#                                                                              
# *******                                                                      
       m_FileAssign -d SHR -g ${G_A9} FGV015T ${DATA}/PTEM/GV015OAX.BGV015LO
# ******* DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FMOISJ
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
#                                                                              
#  IGV020   REPORT SYSOUT=(9,BGV020)                                           
       m_OutputAssign -c "*" IGV020
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV020 
       JUMP_LABEL=GV015OBB
       ;;
(GV015OBB)
       m_CondExec 04,GE,GV015OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV015OZA
       ;;
(GV015OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV015OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
