#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV80M.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGPV80 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/12 AT 12.23.52 BY PREPA3                       
#    STANDARDS: P  JOBSET: GPV80M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BGV800                                                                
# ********************************************************************         
#  EXTRACTION DES CODICS POUR STAT COMMERCIALE SUR LA RTHV04                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV80MA
       ;;
(GPV80MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV80MAA
       ;;
(GPV80MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******  CODE SOCIETE (989)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV04 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV800 ${DATA}/PXX0/GPV80MAA.BGV800AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV800 
       JUMP_LABEL=GPV80MAB
       ;;
(GPV80MAB)
       m_CondExec 04,GE,GPV80MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV800                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAD
       ;;
(GPV80MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GPV80MAA.BGV800AM
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80MAD.BGV800BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80MAE
       ;;
(GPV80MAE)
       m_CondExec 00,EQ,GPV80MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV800                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAG
       ;;
(GPV80MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE                                                                
#    RTGA53   : NAME=RSGA53M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A2} FGV800 ${DATA}/PXX0/GPV80MAD.BGV800BM
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80MAG.BGV805AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80MAH
       ;;
(GPV80MAH)
       m_CondExec 04,GE,GPV80MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV805                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAJ
       ;;
(GPV80MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GPV80MAG.BGV805AM
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80MAJ.BGV805BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_64_6 64 CH 6
 /FIELDS FLD_CH_26_4 26 CH 4
 /FIELDS FLD_CH_48_4 48 CH 4
 /FIELDS FLD_CH_80_6 80 CH 6
 /FIELDS FLD_CH_16_8 16 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_70_4 70 CH 4
 /FIELDS FLD_CH_30_6 30 CH 6
 /FIELDS FLD_CH_52_6 52 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_74_6 74 CH 6
 /FIELDS FLD_CH_146_20 146 CH 20
 /FIELDS FLD_CH_58_6 58 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_16_8 ASCENDING,
   FLD_CH_146_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_26_4,
    TOTAL FLD_CH_30_6,
    TOTAL FLD_CH_36_6,
    TOTAL FLD_CH_42_6,
    TOTAL FLD_CH_48_4,
    TOTAL FLD_CH_52_6,
    TOTAL FLD_CH_58_6,
    TOTAL FLD_CH_64_6,
    TOTAL FLD_CH_70_4,
    TOTAL FLD_CH_74_6,
    TOTAL FLD_CH_80_6
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80MAK
       ;;
(GPV80MAK)
       m_CondExec 00,EQ,GPV80MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV815 AVEC FPARAM BGV81 POUR ALIMENTATION DE FPV815                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
#  AJOUT PAR PHILIPPE DU FPARAM LE 070793                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAM
       ;;
(GPV80MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV815DAL
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV815 ${DATA}/PXX0/GPV80MAM.BGV815AM
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d SHR FGV816 /dev/null
#                                                                              
# ******  CODE SOCIETE (989)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80MAN
       ;;
(GPV80MAN)
       m_CondExec 04,GE,GPV80MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV815                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAQ
       ;;
(GPV80MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/GPV80MAM.BGV815AM
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80MAQ.BGV815BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_19_6 19 CH 6
 /FIELDS FLD_CH_11_5 11 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80MAR
       ;;
(GPV80MAR)
       m_CondExec 00,EQ,GPV80MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV805                                                                
#  ENRICHISSEMENT DU FICHIER FGV815 (CODIC GOUPE)                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAT
       ;;
(GPV80MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A5} FGV800 ${DATA}/PXX0/GPV80MAQ.BGV815BM
# ******  FICHIER FGV800 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80MAT.BGV805CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80MAU
       ;;
(GPV80MAU)
       m_CondExec 04,GE,GPV80MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV815 AVEC FPARAM BGV80 POUR ALIMENTATION DE FPV816                  
# ********************************************************************         
#  EXTRACTION DES CODICS GROUPES POUR STAT COMMERCIALE SUR LA RTHV04           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MAX PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MAX
       ;;
(GPV80MAX)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU JOUR (JJMMSSAA)                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE ARTICLE                                                        
#    RTGA00   : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE DES ETATS                                                      
#    RTGA11   : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11 /dev/null
# ******  TABLE DES LIBELLES RAYONS                                            
#    RTGA20   : NAME=RSGA20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA20 /dev/null
# ******  TABLE DES RAYONS                                                     
#    RTGA21   : NAME=RSGA21M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA21 /dev/null
# ******  TABLE COMISSION PAR ARTICLE                                          
#    RTGA75   : NAME=RSGA75M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE LGT                                     
#    RTHV12   : NAME=RSHV12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV12 /dev/null
#                                                                              
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/PV816DAL
#                                                                              
# ******  CODE SOCIETE (989)                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR FGV815 /dev/null
# ******  FICHIER DES PRIMES PAR ARTICLES ET VOLUME                            
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV816 ${DATA}/PXX0/GPV80MAX.BGV816AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV815 
       JUMP_LABEL=GPV80MAY
       ;;
(GPV80MAY)
       m_CondExec 04,GE,GPV80MAX ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION FGV816                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MBA PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MBA
       ;;
(GPV80MBA)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/GPV80MAX.BGV816AM
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PXX0/GPV80MAA.BGV800AM
# ******  FICHIER D'EXTRACT TRIE                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/GPV80MBA.BGV816BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_11_5 11 CH 5
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_6 19 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_5 ASCENDING,
   FLD_CH_19_6 ASCENDING,
   FLD_CH_4_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV80MBB
       ;;
(GPV80MBB)
       m_CondExec 00,EQ,GPV80MBA ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV805  : ENRICHISSEMENT DU FICHIER FGV816 (CODIC GOUPE)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV80MBD PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MBD
       ;;
(GPV80MBD)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE INTERLOCUTEUR COMMERCIAL                                       
#    RTGA09   : NAME=RSGA09M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA09 /dev/null
# ******  TABLE DES CODES MARKETING                                            
#    RTGA12   : NAME=RSGA12M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12 /dev/null
# ******  TABLE DES FAMILLES                                                   
#    RTGA14   : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  TABLE MARKETING / DESCRIP.                                           
#    RTGA25   : NAME=RSGA25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25 /dev/null
# ******  TABLE RELATION ARTICLE ET VALEUR DE CODE DESCRIPTIF                  
#    RTGA53   : NAME=RSGA53M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53 /dev/null
# ******  TABLE HISTO STAT COMMERCIALE                                         
#    RTAN00   : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d SHR -g ${G_A8} FGV800 ${DATA}/PXX0/GPV80MBA.BGV816BM
# ******  FICHIER FGV816 ENRICHI                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV805 ${DATA}/PXX0/GPV80MBD.BGV816CM
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV805 
       JUMP_LABEL=GPV80MBE
       ;;
(GPV80MBE)
       m_CondExec 04,GE,GPV80MBD ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV80MZA
       ;;
(GPV80MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV80MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
