#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE060O.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PORE060 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/02 AT 10.28.14 BY PREPA2                       
#    STANDARDS: P  JOBSET: RE060O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROG    BRE060  CONSTITUTION DE LA SYSIN POUR FAST UNLOAD .                
#   WARNING : LE FICHIER FPARAM EST EN DUR DANS LE PCL .                       
#                   M = MENSUEL OU H = HEBDO                                   
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE060OA
       ;;
(RE060OA)
#
#RE060OBJ
#RE060OBJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#RE060OBJ
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RAAF=${RAAF:-RE060OAD}
       RUN=${RUN}
       JUMP_LABEL=RE060OAA
       ;;
(RE060OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#  FPARAM M = MENSUEL OU H = HEBDO                                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE060OAA
       m_FileAssign -d SHR FLOGIS ${DATA}/CORTEX4.P.MTXTFIX1/FLOGISO
#  CONSTITUTION DE LA SYSIN POUR UNLOAD                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX2/RE60HUNL.sysin
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FRE060 ${DATA}/PTEM/RE060OAA.BRE060AO
       m_ProgramExec BRE060 
# ********************************************************************         
#  UNLOAD                                                                      
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAD
       ;;
(RE060OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SYSREC01 ${DATA}/PTEM/RE060OAD.UNLGS40O
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SYSREC02 ${DATA}/PTEM/RE060OAD.UNLGV10O
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SYSREC03 ${DATA}/PTEM/RE060OAD.UNLGV11O
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/RE060OAA.BRE060AO
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  SI CODE RETOUR > 04  ABEND                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAG PGM=ZUTABEND   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAG
       ;;
(RE060OAG)
       m_CondExec ${EXAAK},NE,YES 04,GE,$[RAAF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# ********************************************************************         
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAJ
       ;;
(RE060OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE060OAD.UNLGS40O
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -g +1 SORTOUT ${DATA}/PTEM/RE060OAJ.GS40AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 1 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /* Record Type = F  Record Length = 073 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060OAK
       ;;
(RE060OAK)
       m_CondExec 00,EQ,RE060OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   MAGASIN SUR 3          N� DE VENTE SUR 7 :                                 
#   CODIC : 19,7,A         CODE VENDEUR SUR 6:                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAM
       ;;
(RE060OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/RE060OAD.UNLGV11O
       m_FileAssign -d NEW,CATLG,DELETE -r 94 -g +1 SORTOUT ${DATA}/PTEM/RE060OAM.GV11AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_12 1 CH 12
 /KEYS
   FLD_CH_1_12 ASCENDING
 /* Record Type = F  Record Length = 094 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060OAN
       ;;
(RE060OAN)
       m_CondExec 00,EQ,RE060OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DES FICHIERS D UNLOAD                                                 
#   LIEU  SUR 4            N� DE VENTE SUR 7 :                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAQ
       ;;
(RE060OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/RE060OAD.UNLGV10O
       m_FileAssign -d NEW,CATLG,DELETE -r 11 -g +1 SORTOUT ${DATA}/PTEM/RE060OAQ.GV10AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 011 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060OAR
       ;;
(RE060OAR)
       m_CondExec 00,EQ,RE060OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE061 .CE PGM COMPARE LES DEUX FICHIERS D UNLOAD                      
#   IL CHARGE LE FIC FRE061 AVEC LES DONNEES ABSENTES DE RTGS40                
#   ET CHARGE SUR FREMIS TOUS LES CODES REMISES RENCONTRES                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAT PGM=BRE061     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAT
       ;;
(RE060OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
#  FICHIER D UNLOAD RTGV10 TRIE                                                
       m_FileAssign -d SHR -g ${G_A5} RTGV10 ${DATA}/PTEM/RE060OAQ.GV10AO
#                                                                              
#  FICHIER D UNLOAD RTGV11 TRIE                                                
       m_FileAssign -d SHR -g ${G_A6} RTGV11 ${DATA}/PTEM/RE060OAM.GV11AO
#                                                                              
#  FICHIER DE SORTIE DE LRECL 110                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 FRE061 ${DATA}/PTEM/RE060OAT.BRE061AO
       m_ProgramExec BRE061 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER BRE061A*                                                   
#   CODIC + LIEU + VENTE + VENDEUR                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE060OAX
       ;;
(RE060OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE060OAT.BRE061AO
       m_FileAssign -d NEW,CATLG,DELETE -r 110 -g +1 SORTOUT ${DATA}/PTEM/RE060OAX.BRE061BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_6 24 CH 6
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_24_6 ASCENDING
 /* Record Type = F  Record Length = 110 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE060OAY
       ;;
(RE060OAY)
       m_CondExec 00,EQ,RE060OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BRE062 .CE PGM ALIGNE LES REMISES DE TOUTES LES VENTES A PARTI         
#   DE FREMIS ET PREPARE UN FRE062 :                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE060OBA
       ;;
(RE060OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00   : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA52   : NAME=RSGA52O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA52 /dev/null
#  TABLE                                                                       
#    RTGA59   : NAME=RSGA59O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA59 /dev/null
#  TABLE                                                                       
#    RTGA65   : NAME=RSGA65O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA65 /dev/null
#  TABLE                                                                       
#    RTGG20   : NAME=RSGG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#  TABLE                                                                       
#    RTGG50   : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#  TABLE                                                                       
#    RTPR00   : NAME=RSPR00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPR00 /dev/null
#  TABLE                                                                       
#    RTPR10   : NAME=RSPR10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTPR10 /dev/null
#                                                                              
#  FDATE                                                                       
       m_FileAssign -i FDATE
$FMOISJ
_end
#  FNSOC                                                                       
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FRE061 ${DATA}/PTEM/RE060OAX.BRE061BO
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} RTGS40 ${DATA}/PTEM/RE060OAJ.GS40AO
#                                                                              
#    FICHIER EN SORTIE DE LRECL 140                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FRE062 ${DATA}/PXX0/F16.RESULTAT.HISTO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE062 
       JUMP_LABEL=RE060OBB
       ;;
(RE060OBB)
       m_CondExec 04,GE,RE060OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP RE060OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE060OBD
       ;;
(RE060OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F16.RESULTAT.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PTEM/RE060OBD.BRE062BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_58_5 58 CH 5
 /FIELDS FLD_CH_73_5 73 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_21_6 21 CH 6
 /FIELDS FLD_CH_83_5 83 CH 5
 /FIELDS FLD_PD_112_8 112 PD 8
 /FIELDS FLD_CH_99_5 99 CH 5
 /FIELDS FLD_CH_63_5 63 CH 5
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_PD_104_8 104 PD 8
 /FIELDS FLD_CH_78_5 78 CH 5
 /FIELDS FLD_CH_68_5 68 CH 5
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_89_5 89 CH 5
 /FIELDS FLD_CH_88_1 88 CH 1
 /FIELDS FLD_CH_94_2 94 CH 2
 /FIELDS FLD_CH_112_8 112 CH 8
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_21_6 ASCENDING,
   FLD_CH_58_5 ASCENDING,
   FLD_CH_63_5 ASCENDING,
   FLD_CH_68_5 ASCENDING,
   FLD_CH_73_5 ASCENDING,
   FLD_CH_78_5 ASCENDING,
   FLD_CH_83_5 ASCENDING,
   FLD_CH_88_1 ASCENDING,
   FLD_CH_89_5 ASCENDING,
   FLD_CH_94_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_99_5,
    TOTAL FLD_PD_104_8,
    TOTAL FLD_PD_112_8
 /* Record Type = F  Record Length = 071 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_3,FLD_CH_11_3,FLD_CH_21_6,FLD_CH_58_5,FLD_CH_63_5,FLD_CH_68_5,FLD_CH_73_5,FLD_CH_78_5,FLD_CH_83_5,FLD_CH_88_1,FLD_CH_89_5,FLD_CH_94_2,FLD_CH_99_5,FLD_CH_104_8,FLD_CH_112_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE060OBE
       ;;
(RE060OBE)
       m_CondExec 00,EQ,RE060OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DSNUTILB                                                                   
#   LOAD DE LA TABLE RTRE60                                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE060OBG
       ;;
(RE060OBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ****** FICHIER DES DONNEES.                                                  
       m_FileAssign -d SHR -g ${G_A11} SYSREC ${DATA}/PTEM/RE060OBD.BRE062BO
# ****** TABLE RESULTAT                                                        
#    RSRE60O  : NAME=RSRE60O,MODE=(U,N) - DYNAM=YES                            
# -X-RSRE60O  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSRE60O /dev/null
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060OBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/RE060O_RE060OBG_RTRE60.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=RE060OBH
       ;;
(RE060OBH)
       m_CondExec 04,GE,RE060OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPAIR NOCOPY DU TABLESPACE RSRE060                               *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE060OBJ PGM=DSNUTILB   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE060OBM
       ;;
(RE060OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QRE603 DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/RE060O1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p RE060O1 -a QMFPARM DSQPRINT
       JUMP_LABEL=RE060OBN
       ;;
(RE060OBN)
       m_CondExec 04,GE,RE060OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE060OZA
       ;;
(RE060OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE060OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
