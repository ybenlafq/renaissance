#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV520P.ksh                       --- VERSION DU 17/10/2016 18:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV520 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/18 AT 14.50.24 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV520P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#   CMU000                                                                     
#   NOTA : SI PLANTAGE DANS CE STEP ==> CONTINUER AU STEP SUIVANT              
#   ----                                                                       
# ********************************************************************         
#   PGM DE CORRECTION DES RESERVATIONS DE STOCK                                
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV520PA
       ;;
(GV520PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV520PAA
       ;;
(GV520PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************  TABLES EN LECTURE                                          
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ****************  TABLES EN LECTURE / MISE A JOUR                            
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ****************                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b CMU000 
       JUMP_LABEL=GV520PAB
       ;;
(GV520PAB)
       m_CondExec 04,GE,GV520PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE550                                                                      
# ********************************************************************         
#  EXTRAC DES RESERVATIONS POUR LES ARTICLES DONT LE STOCK EST NEGATIF         
#  SI PLANTAGE DANS CE STEP REPRENDRE AU PGM BGV525 + MAIL BT HOST             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAD
       ;;
(GV520PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *********************************************                                
       m_OutputAssign -c "*" SYSOUT
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -t LSEQ -g +1 FIE550 ${DATA}/PTEM/GV520PAD.BIE550AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE550 
       JUMP_LABEL=GV520PAE
       ;;
(GV520PAE)
       m_CondExec 04,GE,GV520PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT                                                                       
#     REPRISE: OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAG
       ;;
(GV520PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV520PAD.BIE550AP
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PAG.BIE550BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_63_7 63 CH 7
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_37_5 37 CH 5
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_56_1 56 CH 1
 /FIELDS FLD_CH_34_3 34 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_34_3 ASCENDING,
   FLD_CH_37_5 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_56_1 ASCENDING,
   FLD_CH_63_7 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PAH
       ;;
(GV520PAH)
       m_CondExec 00,EQ,GV520PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION DES ARTICLES DONT LE STOCK EST NEGATIF                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAJ PGM=BIE555     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAJ
       ;;
(GV520PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
       m_FileAssign -d SHR -g ${G_A2} FIE555 ${DATA}/PTEM/GV520PAG.BIE550BP
       m_FileAssign -i FDATE
$FDATE
_end
       m_OutputAssign -c 9 -w IIE555 JIE555
       m_ProgramExec BIE555 
# ********************************************************************         
#  BGV525                                                                      
# ********************************************************************         
#  MISE AU STATUS DE RESERVATION A Z SI LE DISPONIBLE EST NEGATIF              
#  SI PLANTAGE DANS CE STEP REPRENDRE AU STEP SUIVANT + MAIL BT HOST           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAM
       ;;
(GV520PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***  DATE DE TRAITAMENT JJMMSSAA                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ***  LIGNES DE VENTES                                                        
#    RSGV11   : NAME=RSGV11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
# ***  RESERVATION SUR STOCKS ENTREPOT                                         
#    RSGV21   : NAME=RSGV21,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV21 /dev/null
# ***  STOCK                                                                   
#    RSGS10   : NAME=RSGS10,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV525 
       JUMP_LABEL=GV520PAN
       ;;
(GV520PAN)
       m_CondExec 04,GE,GV520PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DELETE DES LIGNES DE LA TABLE RTGF55                                       
#   RESERVATIONS SUR COMMANDES FOURNISSEURS                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAQ
       ;;
(GV520PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#  TABLE DES RESERVATION SUR COMMANDES FOURNISSEURS                            
#    RTGF55   : NAME=RSGF55,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGF55 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV520PAQ.sysin
       m_ExecSQL -f SYSIN
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV520PAR
       ;;
(GV520PAR)
       m_CondExec 04,GE,GV520PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV520                                                                      
#  SI PLANTAGE DANS CE STEP REPRENDRE AU STEP DE SELECT RTGF55                 
#  AVANT DERNIER STEP + MAIL BT HOST                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAT
       ;;
(GV520PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***  COMMANDES FOURNISSEURS DEMANDEURS                                       
#    RSGF15   : NAME=RSGF15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF15 /dev/null
# ***  COMMANDES FOURNISSEURS ECHEANCEES                                       
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
# ***  TABLE RESERVATION SUR COMMANDES FOURNISSEURS                            
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
# ***  RESERVATION SUR STOCKS ENTREPOT                                         
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
# ******  TABLES DES ANOMALIES                                                 
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ***  FICHIER RESERVATION SUR COMMANDES FOURNISSEURS                          
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 FGV520 ${DATA}/PTEM/GV520PAT.FGV520AP
# ***                                                                          
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV520 
       JUMP_LABEL=GV520PAU
       ;;
(GV520PAU)
       m_CondExec 04,GE,GV520PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#      TRI DU  FICHIER RESERVATION SUR COMMANDES FOURNISSEURS                  
#     ********************************************************                 
#  (CODIC,DATE)                                                                
#  SORT FIELDS=(1,6,A,7,7,A,24,8,A),FORMAT=CH                                  
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV520PAX
       ;;
(GV520PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/GV520PAT.FGV520AP
       m_FileAssign -d NEW,CATLG,DELETE -r 40 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PAX.FGV520BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_24_8 ASCENDING
 /* Record Type = F  Record Length = 040 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PAY
       ;;
(GV520PAY)
       m_CondExec 00,EQ,GV520PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV530                                                                      
# ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *         
#  IMPORTANT MODIF DU 24.08.2011                                               
#  SI VOUS AVEZ UN PLANTAGE DANS LE PGM BGV530 VOUS DEVEZ MODIFIER LA          
#  CORTEX4.P.MTXTFIX1 MEMBRE BGV530P1 ET METTRE DEBUG AU LIEU DE REEL          
#  PUIS RELANCER LA CHAINE AU STEP DU PGM BGV530                               
#  SI ABEND _A NOUVEAU OU SI LA CHAINE TOMBE EN "TIME OUT CPU"                  
#                                                                              
# VOUS DEVEZ MODIFIER LA CORTEX4.P.MTXTFIX1 MEMBRE BGV530P1                    
# ET METTRE SANSZ AU LIEU DE REEL                                              
# PUIS RELANCER LA CHAINE AU STEP DU PGM BGV530 ,                              
#    SI ABEND _A NOUVEAU                                                        
#  REPRENDRE AU STEP DE SELECT SUR RTGF55 PLUS BAS                             
#       + MAIL BT HOST                                                         
#              P. DE L'AULNOIT                                                 
#              H. RUBIALES                                                     
#              N. TRECOIRE                                                     
# ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *         
# ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *         
# **    + SI VENDREDI SOIR OU SAMEDI SOIR EN PLUS DE LA REPRISE      *         
# **         APPEL ASTREINTE LE LENDEMAIN MATIN POUR                 *         
# **         CONTACT ASTREINTE LOGISTIQUE                            *         
# ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *         
# ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *         
#  PJU LE 25.10.2010 SUR LES CONSEILS DE FAY�AL                                
#                                                                              
#  NE PAS EX�CUTER LE STEP DE SCRATCH DES FICHIERS TEMPORAIRES                 
#  ( LES ETUDES DOIVENT FAIRE UNE MANIP IMP�RATIVE )                           
#  ( G520PZA  ) AFIN DE REPRENDRE LA CHAINE LE LENDEMAIN.                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBA
       ;;
(GV520PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***  RESERVATION SUR COMMANDES FOURNISSEURS                                  
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
# ***  COMMANDES FOURNISSEURS ECHEANCEES                                       
#    RSGF30   : NAME=RSGF30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF30 /dev/null
# ***  LIGNES DE VENTES ACTIVES                                                
#    RSGV11   : NAME=RSGV11,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV11Y  : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11Y /dev/null
#    RSGV11M  : NAME=RSGV11M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11M /dev/null
#    RSGV11D  : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11D /dev/null
#    RSGV11L  : NAME=RSGV11L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11L /dev/null
#    RSGV11O  : NAME=RSGV11O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11O /dev/null
#    RSGV11X  : NAME=RSGV11X,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11X /dev/null
# ***  RESERVATION SUR STOCKS ENTREPOT                                         
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
# ***  STOCK ENTREPOT                                                          
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ***  RENSEIGNEMENTS CLIENTS                                                  
#    RSGV02   : NAME=RSGV02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV02 /dev/null
# ***  LIENS ARTICLES                                                          
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
# ***  LIEUX                                                                   
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
# ***  ARTICLES                                                                
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#                                                                              
# ***     RESERVATION FOURNISSEUR (TMU44)                                      
#    RSGV71   : NAME=RSGV71,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV71 /dev/null
# ***     COMMANDES SPECIFIQUES                                                
#    RSGF00   : NAME=RSGF00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF00 /dev/null
# ***     COMMANDES SPECIFIQUES                                                
#    RSGB05   : NAME=RSGB05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  MUTS                                                                 
#    RSGB20   : NAME=RSGB20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB20 /dev/null
#                                                                              
# ******  SUITE INCIDENT AVEC GV903Y ON CODE RSGV11Y                           
#    RSGV11   : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ***     FICHIER TRIE DES RESERVATIONS SUR CMDES FOURNI. / COMMANDES          
       m_FileAssign -d SHR -g ${G_A4} FGV520 ${DATA}/PTEM/GV520PAX.FGV520BP
# ***     FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                   
       m_FileAssign -d NEW,CATLG,DELETE -r 129 -t LSEQ -g +1 FGV535 ${DATA}/PTEM/GV520PBA.FGV535AP
# ***     FICHIER EDITION IGV539                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 FGV539 ${DATA}/PTEM/GV520PBA.FGV539AP
# ***                                                                          
       m_FileAssign -i FDATE
$FDATE
_end
# ***                                                                          
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/BGV530P1
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV530 
       JUMP_LABEL=GV520PBB
       ;;
(GV520PBB)
       m_CondExec 04,GE,GV520PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER RESERVATION ISSU BGV530                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBD
       ;;
(GV520PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GV520PBA.FGV535AP
       m_FileAssign -d NEW,CATLG,DELETE -r 129 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PBD.FGV535BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_75_8 75 CH 8
 /FIELDS FLD_CH_15_30 15 CH 30
 /KEYS
   FLD_CH_2_6 ASCENDING,
   FLD_CH_15_30 ASCENDING,
   FLD_CH_75_8 ASCENDING
 /* Record Type = F  Record Length = 123 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PBE
       ;;
(GV520PBE)
       m_CondExec 00,EQ,GV520PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV535 : EDITION DES RESERVATIONS SUR COMMANDES FOURNISSEURS                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBG
       ;;
(GV520PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ***  FICHIER DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ***  FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                      
       m_FileAssign -d SHR -g ${G_A6} FGV535 ${DATA}/PTEM/GV520PBD.FGV535BP
# ***  FICHIER IMPRESSION                                                      
       m_OutputAssign -c 9 -w IGV535 IGV535
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV535 
       JUMP_LABEL=GV520PBH
       ;;
(GV520PBH)
       m_CondExec 04,GE,GV520PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU  FICHIER RESERVATION ISSU BGV530                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBJ
       ;;
(GV520PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GV520PBA.FGV535AP
       m_FileAssign -d NEW,CATLG,DELETE -r 129 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PBJ.FGV536AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_75_8 75 CH 8
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_15_30 15 CH 30
 /FIELDS FLD_CH_124_3 124 CH 3
 /FIELDS FLD_CH_63_3 63 CH 3
 /KEYS
   FLD_CH_2_6 ASCENDING,
   FLD_CH_124_3 ASCENDING,
   FLD_CH_63_3 ASCENDING,
   FLD_CH_15_30 ASCENDING,
   FLD_CH_75_8 ASCENDING
 /* Record Type = F  Record Length = 123 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PBK
       ;;
(GV520PBK)
       m_CondExec 00,EQ,GV520PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV536 : EDITION DES RESERVATIONS SUR COMMANDES FOURNISSEURS                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBM PGM=BGV536     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBM
       ;;
(GV520PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ***  FICHIER DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ***  FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                      
       m_FileAssign -d SHR -g ${G_A8} FGV535 ${DATA}/PTEM/GV520PBJ.FGV536AP
# ***  FICHIER IMPRESSION                                                      
       m_OutputAssign -c 9 -w IGV536 IGV536
       m_ProgramExec BGV536 
# ********************************************************************         
#      TRI DU  FICHIER RESERVATION ISSU BGV530                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBQ
       ;;
(GV520PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GV520PBA.FGV535AP
       m_FileAssign -d NEW,CATLG,DELETE -r 129 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PBQ.FGV537AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_7 2
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_3_END 3 CH 
 /FIELDS FLD_CH_15_30 15 CH 30
 /FIELDS FLD_CH_75_8 75 CH 8
 /FIELDS FLD_CH_127_3 127 CH 3
 /CONDITION CND_1 FLD_CH_127_3 EQ CST_1_7 FLD_CH_3_END 
 /KEYS
   FLD_CH_127_3 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_15_30 ASCENDING,
   FLD_CH_75_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PBR
       ;;
(GV520PBR)
       m_CondExec 00,EQ,GV520PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV538 : EDITION DES RESERVATIONS SUR COMMANDES FOURNISSEURS                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBT
       ;;
(GV520PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***  FICHIER DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ***  FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                      
       m_FileAssign -d SHR -g ${G_A10} FGV537 ${DATA}/PTEM/GV520PBQ.FGV537AP
# ***  FICHIER IMPRESSION                                                      
       m_OutputAssign -c 9 -w IGV537 IGV537
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV538 
       JUMP_LABEL=GV520PBU
       ;;
(GV520PBU)
       m_CondExec 04,GE,GV520PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER EDITION IGV539                                                 
# ***  FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                      
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GV520PBX
       ;;
(GV520PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/GV520PBA.FGV539AP
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PBX.FGV539BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_127_3 127 CH 3
 /FIELDS FLD_CH_75_8 75 CH 8
 /FIELDS FLD_CH_130_6 130 CH 6
 /FIELDS FLD_CH_15_30 15 CH 30
 /FIELDS FLD_CH_2_6 2 CH 6
 /KEYS
   FLD_CH_127_3 ASCENDING,
   FLD_CH_130_6 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_15_30 ASCENDING,
   FLD_CH_75_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PBY
       ;;
(GV520PBY)
       m_CondExec 00,EQ,GV520PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV539 : EDITION DES VENTES EN LIVRAISON PAR PLATE-FORME                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCA
       ;;
(GV520PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***     FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ***     FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                   
       m_FileAssign -d SHR -g ${G_A12} FGV539 ${DATA}/PTEM/GV520PBX.FGV539BP
# ***     FICHIER IMPRESSION                                                   
       m_OutputAssign -c 9 -w IGV539 IGV539
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV539 
       JUMP_LABEL=GV520PCB
       ;;
(GV520PCB)
       m_CondExec 04,GE,GV520PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                     
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCD
       ;;
(GV520PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GV520PBA.FGV539AP
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/GV520PCD.FGV542AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_75_8 75 CH 8
 /FIELDS FLD_CH_127_3 127 CH 3
 /FIELDS FLD_CH_15_30 15 CH 30
 /FIELDS FLD_CH_2_6 2 CH 6
 /FIELDS FLD_CH_130_6 130 CH 6
 /KEYS
   FLD_CH_127_3 ASCENDING,
   FLD_CH_130_6 ASCENDING,
   FLD_CH_2_6 ASCENDING,
   FLD_CH_75_8 ASCENDING,
   FLD_CH_15_30 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV520PCE
       ;;
(GV520PCE)
       m_CondExec 00,EQ,GV520PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV542 : EDITION DES VENTES EN LIVRAISON PAR PLATE-FORME ET                 
#                                               DATE DE DELIVRANCE             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCG
       ;;
(GV520PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***     FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ***     FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS TRIEES            
       m_FileAssign -d SHR -g ${G_A14} FGV542 ${DATA}/PTEM/GV520PCD.FGV542AP
# ***     FICHIER IMPRESSION                                                   
       m_OutputAssign -c 9 -w IGV542 IGV542
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV542 
       JUMP_LABEL=GV520PCH
       ;;
(GV520PCH)
       m_CondExec 04,GE,GV520PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV521 : RESERVATION LONGUE DUR�ES                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCJ
       ;;
(GV520PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGF49   : NAME=RSGF49,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF49 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV23   : NAME=RSGV23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#                                                                              
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGS10   : NAME=RSGS10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV21   : NAME=RSGV21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV21 /dev/null
#                                                                              
#    RSGV23   : NAME=RSGV23,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV23 /dev/null
#                                                                              
# **  FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****************************************************************             
#  SELECT RTGF55                                                               
#  REPRISE : OUI                                                               
# ****************************************************************             
#                                                                              
# ***********************************                                          
# *   STEP GV520PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          

       m_ProgramExec -b BGV521 
       JUMP_LABEL=GV520PCM
       ;;
(GV520PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC00 ${DATA}/PTEM/GV520PCM.UGF55UNP
#                                                                              
       m_FileAssign -d SHR SYSPUNCH /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV520PCM.sysin
       m_DBHpuUnload -q SYSIN -o SYSREC00
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GV520PCN
       ;;
(GV520PCN)
       m_CondExec 04,GE,GV520PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV531 : RESERVATION SUR COMMANDES FOURNISSEURS POUR PRODUIT                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCQ
       ;;
(GV520PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGV22   : NAME=RSGV22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#                                                                              
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#                                                                              
#    RSGV11   : NAME=RSGV11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#                                                                              
# **  FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#  TBALES EN MAJ                                                               
#    RSGF55   : NAME=RSGF55,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGF55 /dev/null
#    RSGV11   : NAME=RSGV11,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV22   : NAME=RSGV22,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV22 /dev/null
#    RSGV23   : NAME=RSGV23,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSGV23 /dev/null
#                                                                              
# **  FICHIER DE RESERVATIONS SUR COMMANDES FOURNISSEURS                       
       m_FileAssign -d SHR -g ${G_A15} FGV530 ${DATA}/PTEM/GV520PCM.UGF55UNP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV531 
       JUMP_LABEL=GV520PCR
       ;;
(GV520PCR)
       m_CondExec 04,GE,GV520PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMETTRE REEL DANS LE MEMBRE CORTEX4.P.MTXTFIX5(BGV530)                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV520PCT PGM=IDCAMS     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=GV520PCT
       ;;
(GV520PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR INPUT ${DATA}/CORTEX4.P.MTXTFIX1/GV520PCT
       m_FileAssign -d SHR OUTPUT ${DATA}/CORTEX4.P.MTXTFIX1/BGV530P1
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GV520PCU
       ;;
(GV520PCU)
       m_CondExec 16,NE,GV520PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV520PZA
       ;;
(GV520PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV520PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
