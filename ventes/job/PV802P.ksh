#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV802P.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPV802 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/12/02 AT 11.12.07 BY PREPA2                       
#    STANDARDS: P  JOBSET: PV802P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : DE REMISE A ZERO DU FICHIER HEBDO ET MENSUEL POUR MOIS PROCHA         
#  ------------                                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=PV802PA
       ;;
(PV802PA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2009/12/02 AT 11.12.07 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: PV802P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'RAZ DU FIC MENSUEL'                    
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=PV802PAA
       ;;
(PV802PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT1 ${DATA}/PNCGP/F07.PV800PED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT2 ${DATA}/PNCGL/F61.PV800LED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT3 ${DATA}/PNCGM/F89.PV800MED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT4 ${DATA}/P908/F08.PV800XED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT5 ${DATA}/PNCGO/F16.PV800OED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT6 ${DATA}/PNCGY/F45.PV800YED
       m_FileAssign -d NEW,CATLG,DELETE -r 147 -g +1 OUTPUT7 ${DATA}/PNCGD/F91.PV800DED
       m_FileAssign -d SHR INPUT1 /dev/null
       m_FileAssign -d SHR INPUT2 /dev/null
       m_FileAssign -d SHR INPUT3 /dev/null
       m_FileAssign -d SHR INPUT4 /dev/null
       m_FileAssign -d SHR INPUT5 /dev/null
       m_FileAssign -d SHR INPUT6 /dev/null
       m_FileAssign -d SHR INPUT7 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV802PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=PV802PAB
       ;;
(PV802PAB)
       m_CondExec 16,NE,PV802PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
