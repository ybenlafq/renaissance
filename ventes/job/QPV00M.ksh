#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QPV00M.ksh                       --- VERSION DU 08/10/2016 17:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMQPV00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/27 AT 11.37.43 BY PREPA3                       
#    STANDARDS: P  JOBSET: QPV00M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='DARTY'                                                             
# ********************************************************************         
#   LISTE DES PRIX EXCEPTIONNELS TRIE PAR FAMILLE                              
# ********************************************************************         
# AAA      STEP  PGM=IKJEFT01,PATTERN=EO3,PATKW=(L=QPV00,R=RDAR,F='M',         
#                P=QPV00)                                                      
# HOSTIN   DATA  CLASS=VAR,PARMS=QQFDATE1,MBR=QPV00M1                          
# ********************************************************************         
#   LISTE DES PRIX EXCEPTIONNELS TRIE PAR FAMILLE                              
# ********************************************************************         
# AAF      STEP  PGM=IKJEFT01,PATTERN=EO3,PATKW=(L=QPV01,R=RDAR,F='M',         
#                P=QPV01)                                                      
# HOSTIN   DATA  CLASS=VAR,PARMS=QQFDATE1,MBR=QPV00M2                          
# ********************************************************************         
#   LISTE DES PRIX EXCEPTIONNELS TRIE PAR CHEF PRODUIT                         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QPV00MA
       ;;
(QPV00MA)
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=QPV00MAA
       ;;
(QPV00MAA)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QPV02 DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN ADMFIL.QPV02 (&&JOUR='$VDATEJ1_ANNMMDD' &&NSOC='$DALDEP_1_3' FORM=ADMFIL.FPV02
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QPV02M1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QPV00MAB
       ;;
(QPV00MAB)
       m_CondExec 04,GE,QPV00MAA ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
