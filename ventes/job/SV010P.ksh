#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SV010P.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSV010 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/29 AT 16.45.49 BY OPERAT1                      
#    STANDARDS: P  JOBSET: SV010P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM BSV010 : CONSTITUTION DES FICHIERS D EXTRACTIONS HOST                  
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SV010PA
       ;;
(SV010PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+2'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=SV010PAA
       ;;
(SV010PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=(U,U) - DYNAM=YES                             
# -X-RSGA00   - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTPR01   : NAME=RSPR01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR01 /dev/null
#    RTPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR06 /dev/null
#    RTSV04   : NAME=RSSV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTSV04 /dev/null
#                                                                              
# ******  ENTREE: FICHIER DATE                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   SORTIE : FICHIERS D EXTRACTIONS HOST                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV010 ${DATA}/PXX0/F07.FSV010AP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FSV011 ${DATA}/PTEM/SV010PAA.FSV010BP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV012 ${DATA}/PXX0/F07.FSV010CP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV013 ${DATA}/PXX0/F07.FSV010DP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV014 ${DATA}/PXX0/F07.FSV010EP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV015 ${DATA}/PXX0/F07.FSV010FP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV016 ${DATA}/PXX0/F07.FSV010IP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV025 ${DATA}/PTEM/SV010PAA.FSV010JP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV010 
       JUMP_LABEL=SV010PAB
       ;;
(SV010PAB)
       m_CondExec 04,GE,SV010PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BSV201 :                                                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SV010PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAD
       ;;
(SV010PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA22 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTPR01   : NAME=RSPR01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR01 /dev/null
#    RTPR06   : NAME=RSPR06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTPR06 /dev/null
#                                                                              
# ******  ENTREE: FICHIER DATE                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   SORTIE : FICHIERS D EXTRACTIONS HOST                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV201 ${DATA}/PTEM/SV010PAD.BSV201AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV201 
       JUMP_LABEL=SV010PAE
       ;;
(SV010PAE)
       m_CondExec 04,GE,SV010PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FSV010AP                                                     
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAG
       ;;
(SV010PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.FSV010AP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A2} SORTOUT ${DATA}/PXX0/F07.FSV010AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PAH
       ;;
(SV010PAH)
       m_CondExec 00,EQ,SV010PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FSV010JP                                                     
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAJ
       ;;
(SV010PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SV010PAA.FSV010JP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g ${G_A4} SORTOUT ${DATA}/PTEM/SV010PAA.FSV010JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PAK
       ;;
(SV010PAK)
       m_CondExec 00,EQ,SV010PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BSV014 : ENRICHISSEMENT DU FICHER FSV025                                
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAM
       ;;
(SV010PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  ENTREE : FICHIER FSV010B                                             
       m_FileAssign -d SHR -g ${G_A5} FSV010 ${DATA}/PXX0/F07.FSV010AP
       m_FileAssign -d SHR -g ${G_A6} FSV025 ${DATA}/PTEM/SV010PAA.FSV010JP
# ******  SORTIE : FICHIER FSV026                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FSV026 ${DATA}/PTEM/SV010PAM.FSV026AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV014 
       JUMP_LABEL=SV010PAN
       ;;
(SV010PAN)
       m_CondExec 04,GE,SV010PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BSV011 : TRANSPOSITION DES FAMILLES                                     
#               TRAITEMENT DU FICHIER FSV010B                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAQ
       ;;
(SV010PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLES EN LECTURE                                                    
#    RTGA23   : NAME=RSGA23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA23 /dev/null
#    RTGA24   : NAME=RSGA24,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA24 /dev/null
#    RTGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA25 /dev/null
#    RTGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA26 /dev/null
#    RTGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA53 /dev/null
#    RTPR53   : NAME=RSPR53P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTPR53 /dev/null
#                                                                              
# ******  ENTREE : FICHIER FSV010B                                             
       m_FileAssign -d SHR -g ${G_A7} FSV011 ${DATA}/PTEM/SV010PAA.FSV010BP
#                                                                              
# ******  SORTIE : FICHIER FSV011A                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV018 ${DATA}/PXX0/F07.FSV011AP
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 FSV030 ${DATA}/PXX0/F07.FSV011CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV011 
       JUMP_LABEL=SV010PAR
       ;;
(SV010PAR)
       m_CondExec 04,GE,SV010PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SORTANT DE BSV011                                            
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAT
       ;;
(SV010PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.FSV011AP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FSV011BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_29 1 CH 29
 /KEYS
   FLD_CH_1_29 ASCENDING
 /SUMMARIZE
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_29
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SV010PAU
       ;;
(SV010PAU)
       m_CondExec 00,EQ,SV010PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SORTANT DE BSV010                                            
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SV010PAX
       ;;
(SV010PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER ENTREE                                                       
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F07.FSV010AP
# *****   FICHIER SORTIE TRIE                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FSV010GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "O"
 /FIELDS FLD_CH_171_1 171 CH 1
 /CONDITION CND_1 FLD_CH_171_1 EQ CST_1_4 
 /KEYS
   FLD_CH_171_1 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PAY
       ;;
(SV010PAY)
       m_CondExec 00,EQ,SV010PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI ET FUSION DES FICHIERS FSV01* POUR ENVOI                                
#  VERS SYSTEMES EXTERIEURS                                                    
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBA
       ;;
(SV010PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  ENTREE : FICHIERS D EXTRACTIONS                                      
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F07.FSV010CP
       m_FileAssign -d SHR -g ${G_A11} -C ${DATA}/PXX0/F07.FSV010DP
       m_FileAssign -d SHR -g ${G_A12} -C ${DATA}/PXX0/F07.FSV010EP
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PXX0/F07.FSV010FP
       m_FileAssign -d SHR -g ${G_A14} -C ${DATA}/PXX0/F07.FSV010IP
       m_FileAssign -d SHR -g ${G_A15} -C ${DATA}/PXX0/F07.FSV011AP
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PXX0/F07.FSV011BP
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/SV010PAD.BSV201AP
# ******  SORTIE : FICHIER CONCATENE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FSV010HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_171_1 171 CH 1
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_171_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PBB
       ;;
(SV010PBB)
       m_CondExec 00,EQ,SV010PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI ET FUSION DES FICHIERS FSV010HP ET FSV026AP                             
#  VERS SYSTEMES EXTERIEURS                                                    
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBD
       ;;
(SV010PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  ENTREE : FICHIERS D EXTRACTIONS                                      
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/F07.FSV010HP
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PTEM/SV010PAM.FSV026AP
# ******  SORTIE : FICHIER CONCATENE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/SV010PBD.FSV010XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_12_7 12 CH 7
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PBE
       ;;
(SV010PBE)
       m_CondExec 00,EQ,SV010PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AJOUT DU TRI POUR FIC BGA053BP                                              
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBG
       ;;
(SV010PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  ENTREE : FICHIERS D EXTRACTIONS                                      
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGA053BP
# ******  SORTIE : FICHIER CONCATENE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FGA053CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 07
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PBH
       ;;
(SV010PBH)
       m_CondExec 00,EQ,SV010PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BSV200 : AJOUT                                                          
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBJ
       ;;
(SV010PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE: FICHIER DATE                                                 
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  ENTREE : FICHIER CONCATENE FSV010H                                   
       m_FileAssign -d SHR -g ${G_A20} FSV020 ${DATA}/PTEM/SV010PBD.FSV010XP
#                                                                              
# ******  AJOUT FIC VENANT DU GM010P TRIE                                      
       m_FileAssign -d SHR -g ${G_A21} FGA053 ${DATA}/PXX0/F07.FGA053CP
#                                                                              
# ******  SORTIE : FICHIER FSV012B POUR SIEBEL                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FSV200 ${DATA}/PTEM/SV010PBJ.FSV200AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV200 
       JUMP_LABEL=SV010PBK
       ;;
(SV010PBK)
       m_CondExec 04,GE,SV010PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  AJOUT TRI                                                                   
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBM
       ;;
(SV010PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  ENTREE : FICHIERS D EXTRACTIONS                                      
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/SV010PBJ.FSV200AP
# ******  SORTIE :                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FSV200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_11 1 CH 11
 /FIELDS FLD_CH_171_1 171 CH 1
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_171_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SV010PBN
       ;;
(SV010PBN)
       m_CondExec 00,EQ,SV010PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BSV012 : TRADUCTION AU FORMAT XML DU FICHIER A TRANSMETTRE              
#               TRAITEMENT DU FICHIER FSV010H                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBQ
       ;;
(SV010PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE : FICHIER CONCATENE FSV010H                                   
       m_FileAssign -d SHR -g ${G_A23} FSV020 ${DATA}/PXX0/F07.FSV200BP
#                                                                              
# ******  SORTIE : FICHIER FSV012A POUR KAIDARA                                
       m_FileAssign -d NEW,CATLG,DELETE -r 10000 -t LSEQ -g +1 FSV021 ${DATA}/PXX0/F07.FSV012AP
#                                                                              
# ******  SORTIE : FICHIER FSV012B POUR SIEBEL                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 10000 -t LSEQ -g +1 FSV022 ${DATA}/PXX0/F07.FSV012BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV012 
       JUMP_LABEL=SV010PBR
       ;;
(SV010PBR)
       m_CondExec 04,GE,SV010PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    LOAD DE LA TABLE RTGA36                                                   
#    REPRISE OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SV010PBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBT
       ;;
(SV010PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTGA36                                                
       m_FileAssign -d SHR -g ${G_A24} SYSREC ${DATA}/PXX0/F07.FSV011CP
#    RSGA36   : NAME=RSGA36,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA36 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SV010PBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/SV010P_SV010PBT_RTGA36.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=SV010PBU
       ;;
(SV010PBU)
       m_CondExec 04,GE,SV010PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BSV013 : creation du fichier SAV                                        
#               TRAITEMENT DU FICHIER FSV010P                                  
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SV010PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=SV010PBX
       ;;
(SV010PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#                                                                              
# ******  ENTREE : FICHIER ISSU DU BSV010                                      
       m_FileAssign -d SHR -g ${G_A25} FSV010 ${DATA}/PXX0/F07.FSV010AP
# ******  SORTIE : FICHIER FSV040                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FSV040 ${DATA}/PXX0/F07.FSV040AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSV013 
       JUMP_LABEL=SV010PBY
       ;;
(SV010PBY)
       m_CondExec 04,GE,SV010PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI DU FICHIER TYPOLOGIE POUR SIEBEL VERS XFB-GATEWAY                     
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SV010PCA PGM=FTP        ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=SV010PCA
       ;;
(SV010PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
# **********************************                                           
# *** 1ERE LIGNE : ADRESSE IP                                                  
# *** 2EME LIGNE : USER                                                        
# *** 3EME LIGNE : PASSWORD (METTRE PASS SI IL N'Y A EN PAS)                   
# *** 4EME LIGNE : CD POUR DIRECTORY                                           
# *** 4EME LIGNE : CD D: SI ON VEUT LE METTRE SUR D                            
# *** 5EME LIGNE : TYPE I SI BINAIRE SINON VIRER CETTE LIGNE                   
# *** 6EME LIGNE : PUT PUIS NOM DU FICHIER HOST PUIS CELUI PC                  
# *** 7EME LIGNE : QUIT POUR FERMER LA SYSIN                                   
# **********************************                                           
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SV010PCA.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SV010PZA
       ;;
(SV010PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SV010PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
