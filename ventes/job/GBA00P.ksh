#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GBA00P.ksh                       --- VERSION DU 17/10/2016 18:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGBA00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/05/03 AT 10.17.10 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GBA00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLESPACES RSBA01  RSBA10  RSBA20  RSBA21  RSFT29              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GBA00PA
       ;;
(GBA00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GBA00PAA
       ;;
(GBA00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -g +1 SYSPRINT ${DATA}/PXX0/RBA.QGBA00P
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/GBA00PAA
       m_ProgramExec IEFBR14 "RDAR,GBA00P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=GBA00PAD
       ;;
(GBA00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PXX0/RBA.QGBA00P
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GBA00PAE
       ;;
(GBA00PAE)
       m_CondExec 00,EQ,GBA00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA000 : ALIMENTATION DES TABLES STATISTIQUES                              
#   SELECTION DES BONS D'ACHATS DONT LE CODE ALIMENTATION DES STATS            
#   DANS LA TABLE DES BA EST A BLANC                                           
#   MAJ DU CODE STATISTIQUE A * DANS LA RTBA01                                 
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STAT MENSUELL RTBA2         
#   CREAT OU MAJ D'UN ENREGISTREMENT DANS LA TABLE STST ANNUELLE RTBA2         
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAG
       ;;
(GBA00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00   : NAME=RSBA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA00 /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01   : NAME=RSBA01,MODE=U - DYNAM=YES                                 
# -X-GBA00PR1 - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSBA01 /dev/null
# ******* TABLE DES STATS MENSUELLES                                           
#    RSBA20   : NAME=RSBA20,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA20 /dev/null
# ******* TABLE DES STATS ANNUELLES                                            
#    RSBA21   : NAME=RSBA21,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA21 /dev/null
# ******* TABLE DES ANOMALIES                                                  
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA000 
       JUMP_LABEL=GBA00PAH
       ;;
(GBA00PAH)
       m_CondExec 04,GE,GBA00PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA001 : EDITION DU RECAPITULATIF DES COMMANDES                            
#   EXTRACTION ET MISE EN FORME DES DONNEES ISSUES DES TABLES                  
#   RTBA01  RTBA00  RTGA30 . CONSTITUTION D'UN FICHIER DESTINE AU              
#   GENERATEUR D'ETAT IBA001                                                   
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAJ
       ;;
(GBA00PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00Y  : NAME=RSBA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA00Y /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01   : NAME=RSBA01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA01 /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30Y  : NAME=RSBA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA30Y /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* FICHIER A DESTINATION DU GENERATEUR D'ETAT                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA001 ${DATA}/PTEM/GBA00PAJ.BBA001EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA001 
       JUMP_LABEL=GBA00PAK
       ;;
(GBA00PAK)
       m_CondExec 04,GE,GBA00PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA001EP)                                           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAM
       ;;
(GBA00PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GBA00PAJ.BBA001EP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PAM.BBA001FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_15_10 15 CH 10
 /FIELDS FLD_BI_37_10 37 CH 10
 /FIELDS FLD_BI_31_6 31 CH 06
 /FIELDS FLD_BI_7_8 07 CH 08
 /FIELDS FLD_BI_25_6 25 CH 06
 /KEYS
   FLD_BI_7_8 ASCENDING,
   FLD_BI_15_10 ASCENDING,
   FLD_BI_25_6 ASCENDING,
   FLD_BI_31_6 ASCENDING,
   FLD_BI_37_10 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PAN
       ;;
(GBA00PAN)
       m_CondExec 00,EQ,GBA00PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BBA001GP                                      
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAQ
       ;;
(GBA00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC ${DATA}/PTEM/GBA00PAM.BBA001FP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00PAQ.BBA001GP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00PAR
       ;;
(GBA00PAR)
       m_CondExec 04,GE,GBA00PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAT
       ;;
(GBA00PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GBA00PAQ.BBA001GP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PAT.BBA001HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PAU
       ;;
(GBA00PAU)
       m_CondExec 00,EQ,GBA00PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBA001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PAX
       ;;
(GBA00PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01Y  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01Y /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71Y  : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71Y /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FEXTRAC ${DATA}/PTEM/GBA00PAM.BBA001FP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FCUMULS ${DATA}/PTEM/GBA00PAT.BBA001HP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA001 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00PAY
       ;;
(GBA00PAY)
       m_CondExec 04,GE,GBA00PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA002 : EDITION DE LA LISTE DE CONTROLE DES BONS D'ACHAT ANNULES          
#   TOUS LES BA DONT LE CODE ANNULATION EST RENSEIGNE DANS LA TABLE DE         
#   BA ET DONT LA DATE D'ANNULATION EST EGALE _A LA DATE DE TRAITEMENT          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBA
       ;;
(GBA00PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00Y  : NAME=RSBA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA00Y /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01   : NAME=RSBA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA01 /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER A DESTINATION DU GENERATEUR D'ETAT                           
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA002 ${DATA}/PTEM/GBA00PBA.BBA002EP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA002 
       JUMP_LABEL=GBA00PBB
       ;;
(GBA00PBB)
       m_CondExec 04,GE,GBA00PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA002EP)                                           
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBD
       ;;
(GBA00PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GBA00PBA.BBA002EP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PBD.BBA002FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_1 07 CH 01
 /FIELDS FLD_BI_8_2 08 CH 02
 /KEYS
   FLD_BI_7_1 ASCENDING,
   FLD_BI_8_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PBE
       ;;
(GBA00PBE)
       m_CondExec 00,EQ,GBA00PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BBA002GP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBG
       ;;
(GBA00PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/GBA00PBD.BBA002FP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00PBG.BBA002GP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00PBH
       ;;
(GBA00PBH)
       m_CondExec 04,GE,GBA00PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBJ
       ;;
(GBA00PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/GBA00PBG.BBA002GP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PBJ.BBA002HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PBK
       ;;
(GBA00PBK)
       m_CondExec 00,EQ,GBA00PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IBA001                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBM
       ;;
(GBA00PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01Y  : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01Y /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71Y  : NAME=RSGA71,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA71Y /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00Y  : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00Y /dev/null
#    RTEG05Y  : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05Y /dev/null
#    RTEG10Y  : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10Y /dev/null
#    RTEG11Y  : NAME=RSEG11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG11Y /dev/null
#    RTEG15Y  : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15Y /dev/null
#    RTEG20Y  : NAME=RSEG20,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG20Y /dev/null
#    RTEG25Y  : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25Y /dev/null
#    RTEG30Y  : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30Y /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FEXTRAC ${DATA}/PTEM/GBA00PBD.BBA002FP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A11} FCUMULS ${DATA}/PTEM/GBA00PBJ.BBA002HP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA002 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00PBN
       ;;
(GBA00PBN)
       m_CondExec 04,GE,GBA00PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA210 : IMPUTATIONS COMPTABLES DES COMMANDES                              
#   GENERER DES ECRITURES POUR LES COMMANDES OU ANNULATION DE COMMANDE         
#   DANS LE SYSTEME COMPTABLE                                                  
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBQ
       ;;
(GBA00PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES FACTURES                                                   
#    RSBA00Y  : NAME=RSBA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA00Y /dev/null
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01   : NAME=RSBA01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA01 /dev/null
# ******* TABLE DES TIERS BA                                                   
#    RSBA30Y  : NAME=RSBA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA30Y /dev/null
# ******* TABLE DES                                                            
#    RSFT17   : NAME=RSFT17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT17 /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29   : NAME=RSFT29,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT29 /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00   : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00 /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00PBQ.BBA210P
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA210 
       JUMP_LABEL=GBA00PBR
       ;;
(GBA00PBR)
       m_CondExec 04,GE,GBA00PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   BBA212 : IMPUTATIONS COMPTABLES DES RECEPTIONS                             
#   GENERER DES ECRITURES POUR LES BONS D'ACHAT RECEPTIONNES ET LES            
#   ECARTS CREES LORS DE LA RECETION DES BA DANS LE SYSTEME COMPTABLE          
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBT
       ;;
(GBA00PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE DES BONS D'ACHATS                                              
#    RSBA01   : NAME=RSBA01,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA01 /dev/null
# ******* TABLE DES RECEPTIONS                                                 
#    RSBA10Y  : NAME=RSBA10,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSBA10Y /dev/null
# ******* TABLE SEQUENCES DES INTERFACES                                       
#    RSFT29Y  : NAME=RSFT29,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT29Y /dev/null
# ******* TABLE PAS ECS COMPTE SECTION ET RUBRIQUE ATTACHES / DB2 NCP          
#    RSFX00Y  : NAME=RSFX00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFX00Y /dev/null
# ******* TABLE GENERALISEE                                                    
#    RSGA01Y  : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FFTV02 ${DATA}/PTEM/GBA00PBT.BBA212P
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA212 
       JUMP_LABEL=GBA00PBU
       ;;
(GBA00PBU)
       m_CondExec 04,GE,GBA00PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS ISSUS DES PGMS BBA210 ET BBA212                            
#  REPRISE : NON BACKOUT JOBSET                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PBX
       ;;
(GBA00PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIER ISSU DU PGM BBA010                                            
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/GBA00PBQ.BBA210P
# ****** FICHIER ISSU DU PGM BBA012                                            
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PTEM/GBA00PBT.BBA212P
# ****** FICHIER CONCATEN� A DESTINATION DE GCT                                
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 SORTOUT ${DATA}/PXX0/F07.GBA00P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_4_8 4 PD 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_PD_4_8 ASCENDING
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PBY
       ;;
(GBA00PBY)
       m_CondExec 00,EQ,GBA00PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BBA230 .EDITION DE L ETAT DE SUIVI PARAMETRAGE TIERS                  
# ********************************************************************         
#  CREATION DU FICHIER POUR EDITION                                            
#  REPRISE OUI                                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PCA
       ;;
(GBA00PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** TABLES DB2                                    
#    RTBA30   : NAME=RSBA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTBA30 /dev/null
#    RTFT17   : NAME=RSFT17,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFT17 /dev/null
# ****************************** FIC D'EXTRACTION                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IBA130 ${DATA}/PTEM/GBA00PCA.BBA130DP
# ******* DATE DU JOUR SOUS FORME JJSSMMAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE                                                            
# **   N�TACHE =01 + N� CARTE =05 + NOM PGM TBA00                    *         
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/BBA230AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BBA230 
       JUMP_LABEL=GBA00PCB
       ;;
(GBA00PCB)
       m_CondExec 04,GE,GBA00PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BBA130DP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PCD
       ;;
(GBA00PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/GBA00PCA.BBA130DP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PCD.BBA130EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_13_2 13 CH 02
 /FIELDS FLD_BI_7_6 07 CH 06
 /KEYS
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PCE
       ;;
(GBA00PCE)
       m_CondExec 00,EQ,GBA00PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 : GENERATEUR D ETAT                                            
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BBA130EP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PCG
       ;;
(GBA00PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/GBA00PCD.BBA130EP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GBA00PCG.BBA130FP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GBA00PCH
       ;;
(GBA00PCH)
       m_CondExec 04,GE,GBA00PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PCJ
       ;;
(GBA00PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GBA00PCG.BBA130FP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GBA00PCJ.FBA130BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GBA00PCK
       ;;
(GBA00PCK)
       m_CondExec 00,EQ,GBA00PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT                                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GBA00PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PCM
       ;;
(GBA00PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FEXTRAC ${DATA}/PTEM/GBA00PCD.BBA130EP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FCUMULS ${DATA}/PTEM/GBA00PCJ.FBA130BP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE FMOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IBA130 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GBA00PCN
       ;;
(GBA00PCN)
       m_CondExec 04,GE,GBA00PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GBA00PZA
       ;;
(GBA00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GBA00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
