#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV200D.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDPV200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/03/03 AT 19.07.44 BY BURTECL                      
#    STANDARDS: P  JOBSET: PV200D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV050                                                                
#  PROG D EXTRACTION AFIN DE FORMATTER UN FICHIER SAM QUI SERA RE-TRIE         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV200DA
       ;;
(PV200DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV200DAA
       ;;
(PV200DAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******************************** TABLE DES LIEUX                             
#    RTGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ********************************  TABLE DES VENDEURS                         
#    RTGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGV31 /dev/null
# ******************************* TABLE HISTO DES VENTES                       
#    RTHV32   : NAME=RSHV32D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTHV32 /dev/null
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ********************************  SORTIE D UN SAM AVANT TRI (LRECL 1         
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV050 ${DATA}/PXX0/PV200DAA.BPV050AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV050 
       JUMP_LABEL=PV200DAB
       ;;
(PV200DAB)
       m_CondExec 04,GE,PV200DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050                                               
#  C EST LE FICHIER EN SORTIE QUI SERVIRA POUR LES PGM D EDITIONS              
#  REPRISE: OUI.                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAD
       ;;
(PV200DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/PV200DAA.BPV050AD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200DAD.BPV050BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_6 81 CH 6
 /FIELDS FLD_CH_53_20 53 CH 20
 /FIELDS FLD_CH_1_9 1 CH 09
 /FIELDS FLD_CH_103_4 103 CH 4
 /FIELDS FLD_CH_77_4 77 CH 4
 /FIELDS FLD_CH_107_6 107 CH 6
 /FIELDS FLD_CH_73_4 73 CH 4
 /FIELDS FLD_CH_93_6 93 CH 6
 /FIELDS FLD_CH_87_6 87 CH 6
 /FIELDS FLD_CH_99_4 99 CH 4
 /KEYS
   FLD_CH_1_9 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_73_4,
    TOTAL FLD_CH_77_4,
    TOTAL FLD_CH_81_6,
    TOTAL FLD_CH_87_6,
    TOTAL FLD_CH_93_6,
    TOTAL FLD_CH_99_4,
    TOTAL FLD_CH_103_4,
    TOTAL FLD_CH_107_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200DAE
       ;;
(PV200DAE)
       m_CondExec 00,EQ,PV200DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU TRI PRECEDENT POUR ETAT IPV050                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAG
       ;;
(PV200DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/PV200DAD.BPV050BD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200DAG.BPV050CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_53_20 53 CH 20
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_53_20 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200DAH
       ;;
(PV200DAH)
       m_CondExec 00,EQ,PV200DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV051  : EDITION IPV050 .VENTES PAR RUBRIQUE PRODUITS                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAJ PGM=BPV051     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAJ
       ;;
(PV200DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A3} FPV050 ${DATA}/PXX0/PV200DAG.BPV050CD
# ******  EDITION DE L ETAT IPV050                                             
       m_OutputAssign -c 9 -w IPV050 IPV050
       m_ProgramExec BPV051 
# ********************************************************************         
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV051 IL SE NOMMERA FPV050                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAM PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAM
       ;;
(PV200DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/PV200DAD.BPV050BD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200DAM.BPV051AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 0
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_PD_73_4 EQ CST_1_6 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING,
   FLD_PD_73_4 DESCENDING,
   FLD_PD_81_6 DESCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200DAN
       ;;
(PV200DAN)
       m_CondExec 00,EQ,PV200DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET APRES LE PREMIER TRI                       
#  EN ENTREE DU PGM BPV052 IL SE NOMMERA FCUMUL                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAQ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAQ
       ;;
(PV200DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/PV200DAD.BPV050BD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200DAQ.BPV051BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 0
 /FIELDS FLD_PD_103_4 103 PD 4
 /FIELDS FLD_PD_73_4 73 PD 4
 /FIELDS FLD_PD_93_6 93 PD 6
 /FIELDS FLD_PD_99_4 99 PD 4
 /FIELDS FLD_PD_107_6 107 PD 6
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_PD_81_6 81 PD 6
 /CONDITION CND_2 FLD_PD_73_4 EQ CST_1_10 AND FLD_PD_81_6 EQ 
 /KEYS
   FLD_CH_10_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_4,
    TOTAL FLD_PD_81_6,
    TOTAL FLD_PD_93_6,
    TOTAL FLD_PD_99_4,
    TOTAL FLD_PD_103_4,
    TOTAL FLD_PD_107_6
 /OMIT CND_2
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200DAR
       ;;
(PV200DAR)
       m_CondExec 00,EQ,PV200DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV052                                                                
#  PROG DE FORMATTAGE DE L ETAT IPV051                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAT
       ;;
(PV200DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********************************  TABLE GENERALISEE                          
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ********************************  DATE JJMMSSAA                              
       m_FileAssign -i FDATE
$FDATE
_end
# ********************************  PARAMETRE SMOIS MMSSAA                     
       m_FileAssign -i FMOIS
$FMOIS
_end
# ********************************  CARTE FNSOC                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ********************************  ENTREE BPV051AP                            
       m_FileAssign -d SHR -g ${G_A6} FPV050 ${DATA}/PXX0/PV200DAM.BPV051AD
# ********************************  ENTREE BPV051BP                            
       m_FileAssign -d SHR -g ${G_A7} FCUMUL ${DATA}/PXX0/PV200DAQ.BPV051BD
# ******  EDITION DE L ETAT IPV051                                             
       m_OutputAssign -c 9 -w IPV051 IPV051
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV052 
       JUMP_LABEL=PV200DAU
       ;;
(PV200DAU)
       m_CondExec 04,GE,PV200DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU SAM ISSU DU PGM BPV050 ET DU TRI POUR ETAT IPV052                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DAX PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DAX
       ;;
(PV200DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/PV200DAD.BPV050BD
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/PV200DAX.BPV053AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_6 4 CH 06
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_PD_81_6 81 PD 6
 /FIELDS FLD_PD_73_4 73 PD 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_6 ASCENDING,
   FLD_PD_81_6 DESCENDING,
   FLD_PD_73_4 DESCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV200DAY
       ;;
(PV200DAY)
       m_CondExec 00,EQ,PV200DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV053  : EDITION IPV052 .STAT DE VENTES PAR VENDEUR                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV200DBA PGM=BPV053     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DBA
       ;;
(PV200DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  PARAMETRE SMOIS MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER VENTES ISSU DU TRI PRECEDENT                                 
       m_FileAssign -d SHR -g ${G_A9} FPV050 ${DATA}/PXX0/PV200DAX.BPV053AD
# ******  EDITION DE L ETAT IPV052                                             
       m_OutputAssign -c 9 -w IPV052 IPV052
       m_ProgramExec BPV053 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=PV200DZA
       ;;
(PV200DZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV200DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
