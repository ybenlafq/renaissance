#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC200P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC200 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/04 AT 14.39.47 BY BURTECA                      
#    STANDARDS: P  JOBSET: EC200P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTSL11                                       
#  REPRISE OUI                                                                 
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC200PA
       ;;
(EC200PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC200PAA
       ;;
(EC200PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
#    RSGS10   : NAME=RSGS10,MODE=(I,U) - DYNAM=YES                             
#    RSSL11   : NAME=RSSL11,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/EC200PAA.UNS011AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC200PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHIER DU JOUR DE LA TABLE RTSL11                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAD
       ;;
(EC200PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EC200PAA.UNS011AP
# ******* FICHIER RTSL11                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/EC200PAD.UNS011BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC200PAE
       ;;
(EC200PAE)
       m_CondExec 00,EQ,EC200PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC200 SELECTION DES MAGS CLICK AND COLLECT                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAG
       ;;
(EC200PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A2} FEC200 ${DATA}/PTEM/EC200PAD.UNS011BP
# ******* FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FEC201 ${DATA}/PTEM/EC200PAG.BEC200AP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC200 
       JUMP_LABEL=EC200PAH
       ;;
(EC200PAH)
       m_CondExec 04,GE,EC200PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU JOUR DE LA TABLE RTSL11                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAJ
       ;;
(EC200PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/EC200PAG.BEC200AP
# ******* FICHIER RTSL11                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/EC200PAJ.BEC200BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC200PAK
       ;;
(EC200PAK)
       m_CondExec 00,EQ,EC200PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC201 ELIGIBILITé ARTICLES CLICK AND COLLECT                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAM
       ;;
(EC200PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSEC20   : NAME=RSEC20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC20 /dev/null
#    RSEC21   : NAME=RSEC21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC21 /dev/null
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A4} FEC201 ${DATA}/PTEM/EC200PAJ.BEC200BP
# ******* FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC202 ${DATA}/PXX0/F07.BEC201AP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC201 
       JUMP_LABEL=EC200PAN
       ;;
(EC200PAN)
       m_CondExec 04,GE,EC200PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC900  -  COMPARAISON FICHIERS J ET J-1                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAQ
       ;;
(EC200PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A5} FEC90001 ${DATA}/PXX0/F07.BEC201AP
       m_FileAssign -d SHR -g ${G_A6} FEC90002 ${DATA}/PXX0/F07.BEC201AP
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC90003 ${DATA}/PTEM/EC200PAQ.BEC201BP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC900 
       JUMP_LABEL=EC200PAR
       ;;
(EC200PAR)
       m_CondExec 04,GE,EC200PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC110  -  MISE EN FORME XML ET ENVOI MQ                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC200PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EC200PAT
       ;;
(EC200PAT)
       m_CondExec ${EXABE},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE - CONTIENT PARAM POUR MQ                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A7} FEC11001 ${DATA}/PTEM/EC200PAQ.BEC201BP
# ******* FICHIER FEC129X - FICHIER DES ENREGISTREMENTS XML PRODUITS           
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ -g +1 FEC11002 ${DATA}/PXX0/F07.FEC201X
# ******  LES PARAM POUR MQ SONT DANS                                          
#                                                                              
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          

       m_ProgramExec -b BEC110 
       JUMP_LABEL=EC200PZA
       ;;
(EC200PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC200PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=EC200PAU
       ;;
(EC200PAU)
       m_CondExec 04,GE,EC200PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
