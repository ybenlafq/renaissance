#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FS080M.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFS080 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 00/11/30 AT 09.35.10 BY PREPA3                       
#    STANDARDS: P  JOBSET: FS080M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BFS080 :                                                                    
#  REPRISE: NON BACKOUT JOBSET                                                 
#  MODIF LE 291100 CGA ETAT PLUS UTIL A DAL                                    
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FS080MA
       ;;
(FS080MA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=FS080MAA
       ;;
(FS080MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#  DEPENDANCE FORTE SUR FS052 CAR BFX080 NE TRAITE QUE LES VENTES              
#  AYANT DCOMPTA A LA DATE DU JOUR                                             
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ************ TABLE EN MISE A JOUR ********************                       
# ******  MAJ DU NUMERO DE SEQ ET DU STATUS                                    
#    RSFT29   : NAME=RSFT29M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSFT29 /dev/null
# ******  MAJ DE LA DCOMPTA SUR TABLE DES LIGNES DE REGLEMENTS                 
#    RSGV14   : NAME=RSGV14M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
# ************ TABLE EN LECTURE *************************                      
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGV02   : NAME=RSGV02M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV02 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  TABLE DES ECS                                                        
#    RSFX00   : NAME=RSFX00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFX00 /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# * A BLANC POUR TRAITEMENT NORMAL                                             
# * CODER LA DATE JJMMSSAA SI VOUS VOULEZ REPRENDRE UNE JOURNNEE               
       m_FileAssign -d SHR FCOMPTA ${DATA}/CORTEX4.P.MTXTFIX1/FS080MAA
# ********* SI FSIMU = SIMU ==> PAS DE MISE A JOUR                             
# ********* SI FSIMU = REEL ==> MISE A JOUR                                    
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/FS080M
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DAL090
# ******  REPORT DES ANOMALIES                                                 
#  MODIF LE 291100 CGA ETAT PLUS UTIL A DAL                                    
#  BCX0803  REPORT SYSOUT=(9,IFX080)                                           
       m_OutputAssign -c "*" BCX0803
# ******  FICHIER DES ENCAISSEMENTS LIVRAISON POUR GCT : PCL FV001M            
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 FFV001 ${DATA}/PNCGM/F89.BFS080AM
#  DEPENDANCE POUR PLAN                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFS080 
       JUMP_LABEL=FS080MAB
       ;;
(FS080MAB)
       m_CondExec 04,GE,FS080MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
