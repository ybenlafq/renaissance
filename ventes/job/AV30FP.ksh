#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  AV30FP.ksh                       --- VERSION DU 09/10/2016 00:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPAV30F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 18.45.25 BY BURTEC2                      
#    STANDARDS: P  JOBSET: AV30FP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='CONTROLE'                                                          
#                                                                              
# ********************************************************************         
#  PGM BAV300 (BATCH COBOL2 DB2)                                               
#  CREATION DU FICHIER FAV301 POUR ENRICHISSEMENT RTAV10                       
#  SOUS-SYSTEME : RMAC                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=AV30FPA
       ;;
(AV30FPA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 18.45.25 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: AV30FP                                  
# *                           FREQ...: 5W                                      
# *                           TITLE..: 'ALIM RTAV10'                           
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=AV30FPAA
       ;;
(AV30FPAA)
       m_CondExec ${EXAAA},NE,YES 
# *********************************                                            
# **    DEPENDANCE PLAN        ****                                            
# *********************************                                            
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   TABLE RTIF02 RMAC                                                    
#    RSIF02   : NAME=RSIF02,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIF02 /dev/null
#                                                                              
# *****   FIC SOFINCO GDG                                                      
       m_FileAssign -d SHR FAV300 ${DATA}/PXX0/FTP.F07.SOFDARTY
#                                                                              
# *****   FIC DE SORTIE POUR ALIMENTATION RTAV10 SUR RDAR                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FAV301 ${DATA}/PXX0/F07.FAV301AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BAV300 
       JUMP_LABEL=AV30FPAB
       ;;
(AV30FPAB)
       m_CondExec 04,GE,AV30FPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DEPENDANCE POUR PLAN                                                        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
