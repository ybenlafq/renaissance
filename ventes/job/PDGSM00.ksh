#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PDGSM00.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDGSM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/30 AT 11.16.38 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM00D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU LUNDI MATIN                 
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM00DA
       ;;
(GSM00DA)
       DIMANCH=${DIMANCH}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       FIRSTME=${FIRSTME}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       JEUDI=${JEUDI}
       LUNDI=${LUNDI}
       MARDI=${MARDI}
       MERCRED=${MERCRED}
       SAMEDI=${SAMEDI}
       TROISME=${TROISME}
       VENDRED=${VENDRED}
       JUMP_LABEL=GSM00DAA
       ;;
(GSM00DAA)
       m_CondExec ${EXAAA},NE,YES 1,EQ,$[DIMANCH] 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "TM2"
 /DERIVEDFIELD CST_1_16 "EM2"
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAB
       ;;
(GSM00DAB)
       m_CondExec 00,EQ,GSM00DAA ${EXAAA},NE,YES 1,EQ,$[DIMANCH] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MARDI MATIN                 
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAD
       ;;
(GSM00DAD)
       m_CondExec ${EXAAF},NE,YES 1,EQ,$[LUNDI] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A1} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_3_20 "TM2"
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_14_3 14 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAE
       ;;
(GSM00DAE)
       m_CondExec 00,EQ,GSM00DAD ${EXAAF},NE,YES 1,EQ,$[LUNDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MERCREDI MATIN              
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAG
       ;;
(GSM00DAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[MARDI] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A2} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_9_32 "DACEM"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_5_24 "EM3"
 /DERIVEDFIELD CST_7_28 "TM3"
 /DERIVEDFIELD CST_3_20 "TM2"
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR FLD_CH_1_3 EQ CST_7_28 OR FLD_CH_1_5 EQ CST_9_32 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAH
       ;;
(GSM00DAH)
       m_CondExec 00,EQ,GSM00DAG ${EXAAK},NE,YES 1,EQ,$[MARDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU JEUDI MATIN                 
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAJ
       ;;
(GSM00DAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[MERCRED] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A3} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_24 "DACEM"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_3_20 "TM2"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_5 EQ CST_5_24 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAK
       ;;
(GSM00DAK)
       m_CondExec 00,EQ,GSM00DAJ ${EXAAP},NE,YES 1,EQ,$[MERCRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU 1ER MERCREDI                
#    REPRISE : OUI                     AVEC VERSION ST2 ET ST1                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAM
       ;;
(GSM00DAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[FIRSTME] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A4} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_24 "ST1"
 /DERIVEDFIELD CST_7_28 "ST2"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_3_20 "TM2"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR FLD_CH_1_3 EQ CST_7_28 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAN
       ;;
(GSM00DAN)
       m_CondExec 00,EQ,GSM00DAM ${EXAAU},NE,YES 1,EQ,$[FIRSTME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU 3EM MERCREDI                
#    REPRISE : OUI                     AVEC VERSION ST1                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAQ
       ;;
(GSM00DAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[TROISME] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A5} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "TM2"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_5_24 "ST1"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAR
       ;;
(GSM00DAR)
       m_CondExec 00,EQ,GSM00DAQ ${EXAAZ},NE,YES 1,EQ,$[TROISME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU VENDREDI MATIN              
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAT
       ;;
(GSM00DAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[JEUDI] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A6} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_5_24 "EM1"
 /DERIVEDFIELD CST_7_28 "TM1"
 /DERIVEDFIELD CST_9_32 "ST3"
 /DERIVEDFIELD CST_1_16 "EM2"
 /DERIVEDFIELD CST_3_20 "TM2"
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR FLD_CH_1_3 EQ CST_7_28 OR FLD_CH_1_3 EQ CST_9_32 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAU
       ;;
(GSM00DAU)
       m_CondExec 00,EQ,GSM00DAT ${EXABE},NE,YES 1,EQ,$[JEUDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU SAMEDI MATIN                
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DAX
       ;;
(GSM00DAX)
       m_CondExec ${EXABJ},NE,YES 1,EQ,$[VENDRED] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BGM075DD
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A7} SORTOUT ${DATA}/PXX0/F91.BSM025BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "TM2"
 /DERIVEDFIELD CST_1_16 "EM2"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 
 /KEYS
   FLD_CH_1_10 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00DAY
       ;;
(GSM00DAY)
       m_CondExec 00,EQ,GSM00DAX ${EXABJ},NE,YES 1,EQ,$[VENDRED] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION DU FICHIER BSM025BD A VIDE POUR LES JOURS OU IL NE VEULENT         
#  PAS D ETATS DE STOCK                                                        
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00DBA PGM=IEBGENER   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GSM00DBA
       ;;
(GSM00DBA)
       m_CondExec ${EXABO},NE,YES 1,EQ,$[SAMEDI] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A8} SYSUT2 ${DATA}/PXX0/F91.BSM025BD
       m_FileAssign -d SHR SYSUT1 /dev/null
       m_FileAssign -d SHR SYSUT1 /dev/null
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GSM00DBB
       ;;
(GSM00DBB)
       m_CondExec 00,EQ,GSM00DBA ${EXABO},NE,YES 1,EQ,$[SAMEDI] 1000,LT 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
