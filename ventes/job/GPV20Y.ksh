#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GPV20Y.ksh                       --- VERSION DU 17/10/2016 18:09
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGPV20 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/04/24 AT 17.36.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: GPV20Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : BPV200                                                                
# ********************************************************************         
#  EXTRACTION DES DONNEES INTERESSEMENT-VENDEUR POUR LES CODICS EN APP         
#  ( CE PGM GENERE DES ENREGISTREMENTS POUR MAGASIN B.B.T.E )                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GPV20YA
       ;;
(GPV20YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'0'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GPV20YAA
       ;;
(GPV20YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------  PARAMETRE (DATE , SOCIETE , DATE DE DERNIER PASSAGE )                
#                                                                              
       m_FileAssign -i FDATE1
$FDATE1
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -d SHR -g +0 FPARAM ${DATA}/PEX0/F45.DATPV20Y
#                                                                              
# ------  TABLE EN LECTURE                                                     
#                                                                              
#    RTGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00Y /dev/null
#    RTGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01Y /dev/null
#    RTGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10Y /dev/null
#    RTGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14Y /dev/null
#    RTGA59Y  : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA59Y /dev/null
#    RTGA66Y  : NAME=RSGA66Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA66Y /dev/null
#    RTGA73Y  : NAME=RSGA73Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA73Y /dev/null
#    RTGA75Y  : NAME=RSGA75Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA75Y /dev/null
#    RTGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGS30Y /dev/null
#                                                                              
# ------  FICHIERS D'EXTRACTION                                                
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/GPV20YAA.BPV200AY
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220 ${DATA}/PTEM/GPV20YAA.BPV200BY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV200 
       JUMP_LABEL=GPV20YAB
       ;;
(GPV20YAB)
       m_CondExec 04,GE,GPV20YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200AY (FEXTRAC) ISSU DE BPV200 SUR CODIC (45,7,A)         
#  ET CREATION DU FICHIER BPV211AY ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAD
       ;;
(GPV20YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GPV20YAA.BPV200AY
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GPV20YAD.BPV211AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_45_7 45 CH 7
 /KEYS
   FLD_CH_45_7 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20YAE
       ;;
(GPV20YAE)
       m_CondExec 00,EQ,GPV20YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV200BY (FPV220) ISSU DE BPV200 SUR LE CODIC (42,7,         
#  ET CREATION DU FICHIER BPV211BY ENTRANT DS LE PGM BPV211                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAG
       ;;
(GPV20YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GPV20YAA.BPV200BY
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PTEM/GPV20YAG.BPV211BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_42_7 42 CH 7
 /KEYS
   FLD_CH_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20YAH
       ;;
(GPV20YAH)
       m_CondExec 00,EQ,GPV20YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV211                                                                
# ********************************************************************         
#  RAJOUT DU CODE MARKETING A PARTIR DES FICHIERS ISSUS DE BPV200              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAJ
       ;;
(GPV20YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- TABLES EN ENTREE                                                     
#                                                                              
#    RTGA11Y  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA11Y /dev/null
#    RTGA12Y  : NAME=RSGA12Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA12Y /dev/null
#    RTGA25Y  : NAME=RSGA25Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA25Y /dev/null
#    RTGA26Y  : NAME=RSGA26Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA26Y /dev/null
#    RTGA53Y  : NAME=RSGA53Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA53Y /dev/null
#                                                                              
# ------- FICHIER D'EXTRACTION DES TRIS PRECEDENTS                             
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FEXTRAC1 ${DATA}/PTEM/GPV20YAD.BPV211AY
       m_FileAssign -d SHR -g ${G_A4} FPV220 ${DATA}/PTEM/GPV20YAG.BPV211BY
#                                                                              
# ------- FICHIER D'EXTRACTION DE BPV210 (EN DUMMY) CAR CE PROG                
# ------- TOURNE SEULEMENT EN HEBDO DANS GPV21Y                                
#                                                                              
       m_FileAssign -d SHR FEXTRAC2 /dev/null
#                                                                              
# ------- FICHIER D'EDITION EN DUMMY CAR ON NE LES FAIT PAS EN QUOTIDI         
#                                                                              
       m_FileAssign -d SHR FEXTR1B /dev/null
       m_FileAssign -d SHR FEXTR2B /dev/null
#                                                                              
# ------- FICHIER FPV220B ENTRANT DANS LE PGM BPV212                           
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FPV220B ${DATA}/PTEM/GPV20YAJ.BPV220AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV211 
       JUMP_LABEL=GPV20YAK
       ;;
(GPV20YAK)
       m_CondExec 04,GE,GPV20YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BPV220AY POUR CREATION DU FICHIER COMPLET DES                
#  MODIFICATIONS DE L'OFFRE ACTIVE (BPV220BY)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAM
       ;;
(GPV20YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GPV20YAJ.BPV220AY
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PXX0/F45.BPV220BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_42_7 42 CH 7
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_4_3 4 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_42_7 ASCENDING
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20YAN
       ;;
(GPV20YAN)
       m_CondExec 00,EQ,GPV20YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV212                                                                
# ********************************************************************         
#  CREATION DU FICHIER FPV212 A ENVOYER SUR LES 36 DES MAGS PAR                
#  COMPARAISON DES FICHIERS BPV220BY                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAQ PGM=BPV212     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAQ
       ;;
(GPV20YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ------- DATE (JJMMSSAA)                                                      
       m_FileAssign -i FDATE
$FDATE
_end
# ------- FICHIER DE L'OFFRE ACTIVE DE LA VEILLE                               
       m_FileAssign -d SHR -g ${G_A6} FPV220O ${DATA}/PXX0/F45.BPV220BY
# ------- FICHIER DE L'OFFRE ACTIVE DU JOUR                                    
       m_FileAssign -d SHR -g ${G_A7} FPV220B ${DATA}/PXX0/F45.BPV220BY
# ------- FICHIER DE COMPARAISON A ENVOYER SUR LE 36 (GDGNB=5)                 
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -g +1 FPV212 ${DATA}/PXX0/F45.BPV212AY
       m_ProgramExec BPV212 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  CHARGEMENT DE LA DATE POUR LE PROCHAIN PASSAGE DE LA CHAINE                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GPV20YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YAT
       ;;
(GPV20YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  DATE J+1MMSSAA                                                       
       m_FileAssign -i SORTIN
$FDATE1
_end
# ------  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F45.DATPV20Y
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 80 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GPV20YAU
       ;;
(GPV20YAU)
       m_CondExec 00,EQ,GPV20YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GPV20YZA
       ;;
(GPV20YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GPV20YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
