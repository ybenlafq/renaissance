#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV005P.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGV005 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/10/29 AT 09.44.17 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV005P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='PARIS'                                                             
# ********************************************************************         
#  TRI FIC FHV02 VENANT DE GV135P                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV005PA
       ;;
(GV005PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV005PAA
       ;;
(GV005PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    TABLE COMMERCIAL                  *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGV145CP.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/GV005PAA.BGV005GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_11_8 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV005PAB
       ;;
(GV005PAB)
       m_CondExec 00,EQ,GV005PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV004                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAD PGM=BGV004     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAD
       ;;
(GV005PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FICHIER ENTRE FHV02 TRI�                                             
       m_FileAssign -d SHR -g ${G_A1} FHV02 ${DATA}/PXX0/GV005PAA.BGV005GP
#                                                                              
# ******* FICHIER SORTIE DE LRECL 80                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FICDAT ${DATA}/PXX0/GV005PAD.BGV004AP
       m_ProgramExec BGV004 
# ********************************************************************         
# ******             EXECUTION BEX001                          *******         
#                    ****************                                          
#               EXTRACTION DES ARTICLES                                        
#             **************************                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAG
       ;;
(GV005PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **  FIC SPACE POUR EVITER QUE TOUTES LES FILIALES TOURNENT ENSEMBLE          
# SPACE    FILE  DYNAM=YES,NAME=SPACEP,MODE=(U,N),REST=NO                      
#                                                                              
#    RSAN00   : NAME=RSAN00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSAN00 /dev/null
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/GV005PAG
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 FEX001 ${DATA}/PXX0/GV005PAG.BEX001GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=GV005PAH
       ;;
(GV005PAH)
       m_CondExec 04,GE,GV005PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#     TRI PAR CODIC                                                            
#                TRI DES ARTICLES SELECTIONNES                                 
#               ********************************                               
#  CODIC                                                                       
#  SORT FIELDS=(559,7,A),FORMAT=CH                                             
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAJ
       ;;
(GV005PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV005PAG.BEX001GP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -g +1 SORTOUT ${DATA}/PXX0/GV005PAJ.BEX001HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
 /* Record Type = F  Record Length = 600 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV005PAK
       ;;
(GV005PAK)
       m_CondExec 00,EQ,GV005PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FIC FHV02 VENANT DE GV135P SUR SOCIETE ; CODIC ; DATE                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAM
       ;;
(GV005PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BGV145CP.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 93 -g +1 SORTOUT ${DATA}/PXX0/GV005PAM.BGV005JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_18 1 CH 18
 /KEYS
   FLD_CH_1_18 ASCENDING
 /* Record Type = F  Record Length = 93 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV005PAN
       ;;
(GV005PAN)
       m_CondExec 00,EQ,GV005PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV006   CALCUL DES VENTES ET COMMISSIONS DE LA SEMAINE ET DU         
#         MOIS SUIVANT LA SEGMENTATION VOULUE                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAQ
       ;;
(GV005PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIERS ISSU DES TRIS PRECEDENTS                                    
       m_FileAssign -d SHR -g ${G_A3} FEX001 ${DATA}/PXX0/GV005PAJ.BEX001HP
       m_FileAssign -d SHR -g ${G_A4} FHV02 ${DATA}/PXX0/GV005PAM.BGV005JP
       m_FileAssign -d SHR -g ${G_A5} FICDAT ${DATA}/PXX0/GV005PAD.BGV004AP
# *****   PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV006 ${DATA}/PXX0/GV005PAQ.BGV006AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV006 
       JUMP_LABEL=GV005PAR
       ;;
(GV005PAR)
       m_CondExec 04,GE,GV005PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM : BGV005  SI LA DATE FICDAT EST POSTERIEUR AU DEBUT DE MOIS            
#       DE L ANNEE PRECEDENTE LES DONNEES HISTORIQUES SONT EXTRAITES           
#       PAR FAMILLES DEMANDEES PAR L ETAT IGV010                               
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAT
       ;;
(GV005PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA09   : NAME=RSGA09,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA09 /dev/null
#                                                                              
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#                                                                              
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#                                                                              
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
#                                                                              
#    RSGA21   : NAME=RSGA21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA21 /dev/null
#                                                                              
#    RSHV08   : NAME=RSHV08,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV08 /dev/null
#                                                                              
# **  FICHIER DATE   ***                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# **                                                                           
       m_FileAssign -d SHR -g ${G_A6} FICDAT ${DATA}/PXX0/GV005PAD.BGV004AP
# ** FICHIER DE SORTIE                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 FGV005 ${DATA}/PXX0/GV005PAT.BGV005KP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV005 
       JUMP_LABEL=GV005PAU
       ;;
(GV005PAU)
       m_CondExec 04,GE,GV005PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FUSION DES FICHIER FGV005 ET FGV006                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GV005PAX
       ;;
(GV005PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/GV005PAQ.BGV006AP
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PXX0/GV005PAT.BGV005KP
       m_FileAssign -d NEW,CATLG,DELETE -r 180 -g +1 SORTOUT ${DATA}/PNCGP/F07.BGV005LP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_44_6 44 PD 6
 /FIELDS FLD_PD_78_4 78 PD 4
 /FIELDS FLD_PD_88_6 88 PD 6
 /FIELDS FLD_PD_98_6 98 PD 6
 /FIELDS FLD_PD_116_4 116 PD 4
 /FIELDS FLD_PD_148_4 148 PD 4
 /FIELDS FLD_PD_94_4 94 PD 4
 /FIELDS FLD_PD_72_6 72 PD 6
 /FIELDS FLD_PD_66_6 66 PD 6
 /FIELDS FLD_PD_82_6 82 PD 6
 /FIELDS FLD_PD_56_6 56 PD 6
 /FIELDS FLD_CH_1_39 1 CH 39
 /FIELDS FLD_PD_168_6 168 PD 6
 /FIELDS FLD_PD_110_6 110 PD 6
 /FIELDS FLD_PD_40_4 40 PD 4
 /FIELDS FLD_PD_132_4 132 PD 4
 /FIELDS FLD_PD_136_6 136 PD 6
 /FIELDS FLD_PD_158_6 158 PD 6
 /FIELDS FLD_PD_152_6 152 PD 6
 /FIELDS FLD_PD_164_4 164 PD 4
 /FIELDS FLD_PD_142_6 142 PD 6
 /FIELDS FLD_PD_126_6 126 PD 6
 /FIELDS FLD_PD_62_4 62 PD 4
 /FIELDS FLD_PD_50_6 50 PD 6
 /FIELDS FLD_PD_174_6 174 PD 6
 /FIELDS FLD_PD_120_6 120 PD 6
 /FIELDS FLD_PD_104_6 104 PD 6
 /KEYS
   FLD_CH_1_39 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_40_4,
    TOTAL FLD_PD_44_6,
    TOTAL FLD_PD_50_6,
    TOTAL FLD_PD_56_6,
    TOTAL FLD_PD_62_4,
    TOTAL FLD_PD_66_6,
    TOTAL FLD_PD_72_6,
    TOTAL FLD_PD_78_4,
    TOTAL FLD_PD_82_6,
    TOTAL FLD_PD_88_6,
    TOTAL FLD_PD_94_4,
    TOTAL FLD_PD_98_6,
    TOTAL FLD_PD_104_6,
    TOTAL FLD_PD_110_6,
    TOTAL FLD_PD_116_4,
    TOTAL FLD_PD_120_6,
    TOTAL FLD_PD_126_6,
    TOTAL FLD_PD_132_4,
    TOTAL FLD_PD_136_6,
    TOTAL FLD_PD_142_6,
    TOTAL FLD_PD_148_4,
    TOTAL FLD_PD_152_6,
    TOTAL FLD_PD_158_6,
    TOTAL FLD_PD_164_4,
    TOTAL FLD_PD_168_6,
    TOTAL FLD_PD_174_6
 /* Record Type = F  Record Length = 180 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV005PAY
       ;;
(GV005PAY)
       m_CondExec 00,EQ,GV005PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV010                                                                
#  EDITION ETAT SUIVI DES VENTES HEBDO                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV005PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GV005PBA
       ;;
(GV005PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  DD SERVANT POUR LES CROSS REF                                               
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#  ENTREE DATE DU DERNIER JOUR DE LA SEMAINE TRAITEE                           
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FDATEURO ${DATA}/CORTEX4.P.MTXTFIX4/EUROGCA
       m_FileAssign -d SHR -g ${G_A9} FGV005T ${DATA}/PNCGP/F07.BGV005LP
#  SORTIE                                                                      
       m_OutputAssign -c 9 -w BGV010 IGV010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV010 
       JUMP_LABEL=GV005PBB
       ;;
(GV005PBB)
       m_CondExec 04,GE,GV005PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV005PZA
       ;;
(GV005PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV005PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
