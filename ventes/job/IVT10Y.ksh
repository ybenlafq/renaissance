#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT10Y.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/06/14 AT 15.05.12 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVT10Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    QUIESCE DU TABLESPACE RSIT15                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT10YA
       ;;
(IVT10YA)
#
#IVT10YAA
#IVT10YAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT10YAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVT10YAD
       ;;
(IVT10YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLE / GENERALITES ARTICLES                                 
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLES DES FAMILLES                                                  
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK SOUS LIEU MAGASIN                                        
#    RSGS30   : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE LIEUX INVENTORIES                                              
#    RSIT05   : NAME=RSIT05Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT05 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSIT10   : NAME=RSIT10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT10 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSFL90   : NAME=RSFL90Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  PARAMETRE SOCIETE 945                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES CODICS A INVENTORIER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 FIT010 ${DATA}/PXX0/F45.BIT010AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT010 
       JUMP_LABEL=IVT10YAE
       ;;
(IVT10YAE)
       m_CondExec 04,GE,IVT10YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTIT15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAG
       ;;
(IVT10YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -g +1 SYSREC01 ${DATA}/PGI945/F45.UNLOAD.IT15UY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10YAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR N�INVENTAIRE ; NLIEU ; NSOCIETE ; NSSLIEU ; NCODIC                  
#  OUTREC 1 ; 35                                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAJ
       ;;
(IVT10YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F45.BIT010AY
# ******  FICHIER DES CODICS POUR LOAD RTIT15                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PTEM/IVT10YAJ.BIT010GY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_1_35 1 CH 35
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_35
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10YAK
       ;;
(IVT10YAK)
       m_CondExec 00,EQ,IVT10YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI N�2                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAM
       ;;
(IVT10YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVT10YAJ.BIT010GY
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PGI945/F45.UNLOAD.IT15UY
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PGI945/F45.RELOAD.IT15RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_12_3 12 CH 3
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YAN
       ;;
(IVT10YAN)
       m_CondExec 00,EQ,IVT10YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTIT15                                                     
# ********************************************************************         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAQ
       ;;
(IVT10YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTIT15                                                
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGI945/F45.RELOAD.IT15RY
#                                                                              
#    RSIT15   : NAME=RSIT15Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10YAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVT10Y_IVT10YAQ_RTIT15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVT10YAR
       ;;
(IVT10YAR)
       m_CondExec 04,GE,IVT10YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAT
       ;;
(IVT10YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F45.BIT010AY
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10YAT.BIT010BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "1"
 /DERIVEDFIELD CST_3_14 " "
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_85_1 85 CH 1
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YAU
       ;;
(IVT10YAU)
       m_CondExec 00,EQ,IVT10YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT011 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YAX PGM=BIT011     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YAX
       ;;
(IVT10YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 945                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A6} FIT011 ${DATA}/PTEM/IVT10YAT.BIT010BY
       m_OutputAssign -c 9 -w BIT011 IIT011
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F45.BIT010CY
       m_ProgramExec BIT011 
# ********************************************************************         
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBA
       ;;
(IVT10YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F45.BIT010AY
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10YBA.BIT010DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "2"
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_16_7 16 CH 7
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YBB
       ;;
(IVT10YBB)
       m_CondExec 00,EQ,IVT10YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT031 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBD PGM=BIT031     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBD
       ;;
(IVT10YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 945                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A8} FIT011 ${DATA}/PTEM/IVT10YBA.BIT010DY
       m_OutputAssign -c 9 -w BIT031 IIT031
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F45.BIT010EY
       m_ProgramExec BIT031 
# ********************************************************************         
#  BIT015  POUR TOUS LES CODICS A INVENTORIER DONT LA DATE DE                  
#          CONSTITUTUION DU STOCK THEORIQUE EST EGALE A LA DATE DE             
#          TRAITEMENT IL Y A CALCUL DU STOCK ET INSCRIPTION DANS RTIT1         
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBG
       ;;
(IVT10YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLES DES LIEUX                                                     
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  TABLE STOCK SOUS LIEU / MAGASINS                                     
#    RSGS30   : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS60   : NAME=RSGS60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 945                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT015 
       JUMP_LABEL=IVT10YBH
       ;;
(IVT10YBH)
       m_CondExec 04,GE,IVT10YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT035                                                                      
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBJ
       ;;
(IVT10YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE STOCKS                                                         
#    RSGS36   : NAME=RSGS36Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 945                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT035 
       JUMP_LABEL=IVT10YBK
       ;;
(IVT10YBK)
       m_CondExec 04,GE,IVT10YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT100  BUT: PRAPARATION DU FICHIER POUR EDITION DES ETIQUETTES             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBM
       ;;
(IVT10YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSIT00   : NAME=RSIT00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PTEM/IVT10YBM.BIT100AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT100 
       JUMP_LABEL=IVT10YBN
       ;;
(IVT10YBN)
       m_CondExec 04,GE,IVT10YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBQ
       ;;
(IVT10YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/IVT10YBM.BIT100AY
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/IVT10YBQ.BIT100BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_16_15 16 CH 15
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YBR
       ;;
(IVT10YBR)
       m_CondExec 00,EQ,IVT10YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBT PGM=BIV105     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBT
       ;;
(IVT10YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES                                               
       m_FileAssign -d SHR -g ${G_A10} FIN105 ${DATA}/PTEM/IVT10YBQ.BIT100BY
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIT040  BUT: EDITION D'UN ETAT D INVENTAIRE COMLPLET                        
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YBX
       ;;
(IVT10YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA67   : NAME=RSGA67Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSEG90   : NAME=RSEG90Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG90 /dev/null
#    RSIT00   : NAME=RSIT00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IIT004 ${DATA}/PTEM/IVT10YBX.BIT040AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT040 
       JUMP_LABEL=IVT10YBY
       ;;
(IVT10YBY)
       m_CondExec 04,GE,IVT10YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIT040AY)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YCA
       ;;
(IVT10YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IVT10YBX.BIT040AY
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10YCA.BIT040BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_37_7 37 CH 07
 /FIELDS FLD_BI_44_2 44 CH 02
 /FIELDS FLD_BI_18_5 18 CH 05
 /FIELDS FLD_BI_12_3 12 CH 03
 /FIELDS FLD_BI_15_3 15 CH 03
 /FIELDS FLD_BI_23_5 23 CH 05
 /FIELDS FLD_BI_7_5 7 CH 05
 /FIELDS FLD_BI_28_9 28 CH 09
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING,
   FLD_BI_28_9 ASCENDING,
   FLD_BI_37_7 ASCENDING,
   FLD_BI_44_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YCB
       ;;
(IVT10YCB)
       m_CondExec 00,EQ,IVT10YCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIT040CY                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YCD
       ;;
(IVT10YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PTEM/IVT10YCA.BIT040BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/IVT10YCD.BIT040CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IVT10YCE
       ;;
(IVT10YCE)
       m_CondExec 04,GE,IVT10YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YCG
       ;;
(IVT10YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/IVT10YCD.BIT040CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10YCG.BIT040DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10YCH
       ;;
(IVT10YCH)
       m_CondExec 00,EQ,IVT10YCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIT004                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10YCJ PGM=IKJEFT01   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YCJ
       ;;
(IVT10YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/IVT10YCA.BIT040BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PTEM/IVT10YCG.BIT040DY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIT004 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IVT10YCK
       ;;
(IVT10YCK)
       m_CondExec 04,GE,IVT10YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVT10YZA
       ;;
(IVT10YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
