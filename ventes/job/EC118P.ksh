#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC118P.ksh                       --- VERSION DU 08/10/2016 22:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC118 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/04/05 AT 15.40.47 BY BURTEC5                      
#    STANDARDS: P  JOBSET: EC118P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM = BEC118  -  EXTRACTION QUOTIDIENNE DE LA TABLE RTGQ03                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC118PA
       ;;
(EC118PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'0'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC118PAA
       ;;
(EC118PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGQ03   : NAME=RSGQ03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGQ03 /dev/null
#    RSEC06   : NAME=RSEC06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEC06 /dev/null
# ******* PARM SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EXTRAIT                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FEC11801 ${DATA}/PTEM/EC118PAA.BEC118AP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC118 
       JUMP_LABEL=EC118PAB
       ;;
(EC118PAB)
       m_CondExec 04,GE,EC118PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU JOUR VENANT DE BEC118 - CREATION DU FEC118 DU JOU         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC118PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC118PAD
       ;;
(EC118PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EC118PAA.BEC118AP
# ******* FICHIER FEC118 DU JOUR                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 SORTOUT ${DATA}/PXX0/F07.FEC118P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_110 1 CH 110
 /KEYS
   FLD_CH_1_110 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC118PAE
       ;;
(EC118PAE)
       m_CondExec 00,EQ,EC118PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC900  -  COMPARAISON FICHIERS J ET J-1                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC118PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC118PAG
       ;;
(EC118PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A2} FEC90001 ${DATA}/PXX0/F07.FEC118P
       m_FileAssign -d SHR -g ${G_A3} FEC90002 ${DATA}/PXX0/F07.FEC118P
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FEC90003 ${DATA}/PTEM/EC118PAG.BEC118CP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC900 
       JUMP_LABEL=EC118PAH
       ;;
(EC118PAH)
       m_CondExec 04,GE,EC118PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC110  -  MISE EN FORME XML ET ENVOI MQ                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC118PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC118PAJ
       ;;
(EC118PAJ)
       m_CondExec ${EXAAP},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE - CONTIENT PARAM POUR MQ                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A4} FEC11001 ${DATA}/PTEM/EC118PAG.BEC118CP
# ******* FICHIER FEC118X - FICHIER DES ENREGISTREMENTS XML PRODUITS           
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -g +1 FEC11002 ${DATA}/PXX0/F07.FEC118XP
# ******  LES PARAM POUR MQ SONT DANS TABLES DB2                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC110 
       JUMP_LABEL=EC118PAK
       ;;
(EC118PAK)
       m_CondExec 04,GE,EC118PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC118PZA
       ;;
(EC118PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC118PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
