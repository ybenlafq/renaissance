#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VT922P.ksh                       --- VERSION DU 20/10/2016 10:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVT922 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/05/27 AT 17.03.10 BY BURTECC                      
#    STANDARDS: P  JOBSET: VT922P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES AVANT MAJ                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VT922PA
       ;;
(VT922PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VT922PAA
       ;;
(VT922PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#    RSEM49   : NAME=RSEM49,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM49 /dev/null
#    RSEM51   : NAME=RSEM51,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM51 /dev/null
#    RSEM52   : NAME=RSEM52,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM52 /dev/null
#    RSEM53   : NAME=RSEM53,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM53 /dev/null
#    RSEM54   : NAME=RSEM54,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM54 /dev/null
#    RSEM55   : NAME=RSEM55,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM55 /dev/null
#    RSVT50   : NAME=RSVT50,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT50 /dev/null
#    RSVT57   : NAME=RSVT57,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT57 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,CATLG -r 1-128 -t LSEQ -g +1 SYSPRINT ${DATA}/PARG/SEM.VT922PA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VT922PAA
       m_ProgramExec IEFBR14 "RDAR,VT922P.U1"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT922PAD
       ;;
(VT922PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VT922PA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VT922PAE
       ;;
(VT922PAE)
       m_CondExec 00,EQ,VT922PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BEM930 : PURGES DES TABLES RTEM51 _A 55                                      
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP VT922PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VT922PAG
       ;;
(VT922PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSEM51   : NAME=RSEM51,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEM51 /dev/null
#    RSEM52   : NAME=RSEM52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEM52 /dev/null
#    RSEM53   : NAME=RSEM53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEM53 /dev/null
#    RSEM54   : NAME=RSEM54,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEM54 /dev/null
#    RSEM55   : NAME=RSEM55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSEM55 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#    RSEM51   : NAME=RSEM51,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM51 /dev/null
#    RSEM52   : NAME=RSEM52,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM52 /dev/null
#    RSEM53   : NAME=RSEM53,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM53 /dev/null
#    RSEM54   : NAME=RSEM54,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM54 /dev/null
#    RSEM55   : NAME=RSEM55,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM55 /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM930 
       JUMP_LABEL=VT922PAH
       ;;
(VT922PAH)
       m_CondExec 04,GE,VT922PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTEM51                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT922PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VT922PAJ
       ;;
(VT922PAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSEM51   : NAME=RSEM51,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 172 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/VT922PAJ.VT922P01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT922PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  BEM939 : CREATION DES FICHIERS DE LOAD RTEM51 ET RTEM49                     
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP VT922PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VT922PAM
       ;;
(VT922PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSVT01   : NAME=RSVT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT01 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} BEM939A ${DATA}/PTEM/VT922PAJ.VT922P01
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 115 -t LSEQ -g +1 EM49RP ${DATA}/PTEM/VT922PAM.VT922P02
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 172 -t LSEQ -g +1 EM51RP ${DATA}/PTEM/VT922PAM.VT922P03
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM939 
       JUMP_LABEL=VT922PAN
       ;;
(VT922PAN)
       m_CondExec 04,GE,VT922PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RELOAD DE LA TABLE RTEM51 EPUR�E DANS LA SEQUENCE DES CLES DE               
#         L'INDEX CLUSTER (PARAMETRE ==>'RECLUSTER YES' DANS LA SYSIN)         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT922PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=VT922PAQ
#       ;;
#(VT922PAQ)
#       m_CondExec ${EXAAZ},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
## SORTWK01 DD UNIT=(TEM350,26),SPACE=(CYL,(95,99)),DISP=(,DELETE)              
## SORTWK02 DD UNIT=(TEM350,26),SPACE=(CYL,(95,99)),DISP=(,DELETE)              
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
##    RSEM51   : NAME=RSEM51,MODE=(U,N) - DYNAM=YES                             
#       m_FileAssign -d SHR RSEM51 /dev/null
#       m_FileAssign -d SHR -g ${G_A3} SYSULD ${DATA}/PTEM/VT922PAM.VT922P03
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT922PAQ.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=VT922PAQ_SORT
       ;;
(VT922PAQ_SORT)
     m_CondExec ${EXAAA},NE,YES
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/VT922PAM.VT922P03
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/VT922P_VT922PAQ_RTFM99_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS FLD_CH_1_3 1 CH 3
/FIELDS FLD_CH_4_3 4 CH 3
/FIELDS FLD_CH_7_8 7 CH 8
/FIELDS FLD_CH_15_3 15 CH 3
/FIELDS FLD_CH_18_8 18 CH 8
/FIELDS FLD_CH_26_3 26 CH 3 
/FIELDS FLD_CH_29_3 29 CH 3 
/FIELDS FLD_CH_32_3 32 CH 3
/FIELDS FLD_CH_35_3 35 CH 3
/FIELDS FLD_CH_38_3 38 CH 3
/FIELDS FLD_CH_41_7 41 CH 7
/KEYS  
   FLD_CH_1_3  ASCENDING,
   FLD_CH_4_3  ASCENDING,
   FLD_CH_7_8  ASCENDING,
   FLD_CH_15_3  ASCENDING,
   FLD_CH_18_8  ASCENDING,
   FLD_CH_26_3  ASCENDING,
   FLD_CH_29_3  ASCENDING,
   FLD_CH_32_3  ASCENDING,
   FLD_CH_35_3  ASCENDING,
   FLD_CH_38_3  ASCENDING,
   FLD_CH_41_7  ASCENDING
_end
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VT922PAQ
       ;;
(VT922PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX5/DFSPARM
# SORTWK01 DD UNIT=(TEM350,26),SPACE=(CYL,(95,99)),DISP=(,DELETE)              
# SORTWK02 DD UNIT=(TEM350,26),SPACE=(CYL,(95,99)),DISP=(,DELETE)              
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSEM51   : NAME=RSEM51,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM51 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT922PAQ.sysin
       m_FileAssign -d SHR SYSULD ${MT_TMP}/VT922P_VT922PAQ_RTFM99_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VT922P_VT922PAQ_RTFM99.sysload
       m_UtilityExec
#} Post-translation
# ********************************************************************         
#   IMAGECOPY DU TS RSEM49 EN CAS DE PB DANS LE LOAD RESUME YES                
#   IMAGECOPY DU TS RSEM51 CHARGE DANS LE STEP PRECEDENT                       
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT922PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VT922PAT
       ;;
(VT922PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#    RSEM49   : NAME=RSEM49,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM49 /dev/null
#    RSEM51   : NAME=RSEM51,MODE=(I,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM51 /dev/null
       m_FileAssign -d ,CATLG,CATLG -r 4096 -t LSEQ -g +1 EM49IP ${DATA}/PFIC/F07.EM49IP
       m_FileAssign -d ,CATLG,CATLG -r 4096 -t LSEQ -g +1 EM51IP ${DATA}/PFIC/F07.EM51IP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT922PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT922PAU
       ;;
(VT922PAU)
       m_CondExec 04,GE,VT922PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTEM49 EN RESUME YES                                       
#  REPRISE:NON - SI PLANTAGE DANS CE STEP FAIRE LA REPRISE AU STEP             
#  SUIVANT EN PENSANT _A METTRE LE STEP DE DELETE DES FICHIERS _A NO             
#  POUR CONSERVER LES FICHIERS DE LA CHAINE.                                   
#  VOIR AVEC LA PERSONNE DU BT D'ASTREINTE SI IL Y A UN DOUTE                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT922PAX PGM=DSNUTILB   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VT922PAX
       ;;
(VT922PAX)
       m_CondExec ${EXABJ},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSEM49   : NAME=RSEM49,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSEM49 /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PTEM/VT922PAM.VT922P02
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_ProgramExec -b LRTEM4901 RESUME YES
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VT922PAY
       ;;
(VT922PAY)
       m_CondExec 04,GE,VT922PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVT933 : MAJ DES TABLES RTVT01 ET RTVT57                                    
#  REPRISE : OUI SI ABEND                                                      
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP VT922PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VT922PBA
       ;;
(VT922PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSVT01   : NAME=RSVT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT01 /dev/null
#    RSVT57   : NAME=RSVT57,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSVT57 /dev/null
#                                                                              
#    RSVT50   : NAME=RSVT50,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT50 /dev/null
#    RSVT57   : NAME=RSVT57,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVT57 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVT933 
       JUMP_LABEL=VT922PBB
       ;;
(VT922PBB)
       m_CondExec 04,GE,VT922PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CECI EST UNE INFORMATION                                                    
#  SI PLANTAGE AU STEP AAZ LOAD TABLE RTEM51 ET APRES LA REPRISE AU ST         
#  FASTLOAD RENVOIE UN RC=04 ET LAISSE LA TABLE EN STOP                        
#  DONC AJOUT DU START TS ET INDEX EN FIN DE CHAINE                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VT922PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VT922PZA
       ;;
(VT922PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VT922PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
