#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EC108P.ksh                       --- VERSION DU 09/10/2016 05:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPEC108 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/09/16 AT 11.37.24 BY BURTECA                      
#    STANDARDS: P  JOBSET: EC108P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM = BEC108  -  EXTRACTION QUOTIDIENNE DE LA TABLE RTGA52                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EC108PA
       ;;
(EC108PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'0'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EC108PAA
       ;;
(EC108PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA40   : NAME=RSGA40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA40 /dev/null
#    RSGA52   : NAME=RSGA52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA52 /dev/null
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EXTRAIT                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC10801 ${DATA}/PTEM/EC108PAA.BEC108AP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC108 
       JUMP_LABEL=EC108PAB
       ;;
(EC108PAB)
       m_CondExec 04,GE,EC108PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DU JOUR VENANT DE BEC108 - CREATION DU FEC108 DU JOU         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC108PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EC108PAD
       ;;
(EC108PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EC108PAA.BEC108AP
# ******* FICHIER FEC108 DU JOUR                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FEC108
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_110 1 CH 110
 /KEYS
   FLD_CH_1_110 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EC108PAE
       ;;
(EC108PAE)
       m_CondExec 00,EQ,EC108PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC901  -  COMPARAISON FICHIERS J ET J-1                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC108PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EC108PAG
       ;;
(EC108PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIERS EN LECTURE                                                  
       m_FileAssign -d SHR -g ${G_A2} FEC90001 ${DATA}/PXX0/F07.FEC108
       m_FileAssign -d SHR -g ${G_A3} FEC90002 ${DATA}/PXX0/F07.FEC108
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 FEC90003 ${DATA}/PTEM/EC108PAG.BEC108CP
# *******                                                                      
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC901 
       JUMP_LABEL=EC108PAH
       ;;
(EC108PAH)
       m_CondExec 04,GE,EC108PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM = BEC110  -  MISE EN FORME XML ET ENVOI MQ                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EC108PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EC108PAJ
       ;;
(EC108PAJ)
       m_CondExec ${EXAAP},NE,YES 
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN LECTURE - CONTIENT PARAM POUR MQ                            
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* FICHIER EN LECTURE                                                   
       m_FileAssign -d SHR -g ${G_A4} FEC11001 ${DATA}/PTEM/EC108PAG.BEC108CP
# ******* FICHIER FEC108X - FICHIER DES ENREGISTREMENTS XML PRODUITS           
       m_FileAssign -d NEW,CATLG,DELETE -r 27956 -t LSEQ -g +1 FEC11002 ${DATA}/PXX0/F07.FEC108X
# ******  LES PARAM POUR MQ SONT DANS TABLES DB2                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEC110 
       JUMP_LABEL=EC108PAK
       ;;
(EC108PAK)
       m_CondExec 04,GE,EC108PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EC108PZA
       ;;
(EC108PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EC108PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
