#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PMGSM00.ksh                       --- VERSION DU 19/10/2016 19:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGSM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 09/03/30 AT 11.17.15 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM00M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#       TRI DU FICHIER D'EXTRACTION DES STOCKS MAGASINS                        
#       ***********************************************                        
#  (VERSION/SOCIETE/LIEU/NO SEQUENCE/LIBELLE MARKETING 1 2 3/MANQUE/           
#   MONTANT POUR TRI/CODIC1/CODIC2)                                            
# OMIT COND=(1,3,EQ,C'TM4',OR,1,3,EQ,C'EM1',OR,1,3,EQ,C'TM2',OR,               
#                 1,3,EQ,C'TM1'),FORMAT=CH                                     
#   REPRISE OUI                                                                
# ********************************************************************         
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU DIMANCHE                    
#    REPRISE : OUI                                                             
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM00MA
       ;;
(GSM00MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       GSM00MA=${LUNDI}
       GSM00MB=${MARDI}
       GSM00MC=${MERCRED}
       GSM00MD=${JEUDI}
       GSM00ME=${VENDRED}
       GSM00MF=${SAMEDI}
       GSM00MG=${DIMANCH}
       JUMP_LABEL=GSM00MAA
       ;;
(GSM00MAA)
       m_CondExec ${EXAAA},NE,YES 1,EQ,$[GSM00MF] 
       m_OutputAssign -c "*" SYSOUT
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
# ********************************************                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g +1 SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "TM1"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /CONDITION CND_4 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /KEYS
 /KEYS
 /KEYS
 /OMIT CND_4
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAB
       ;;
(GSM00MAB)
       m_CondExec 00,EQ,GSM00MAA ${EXAAA},NE,YES 1,EQ,$[GSM00MF] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU LUNDI                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAD
       ;;
(GSM00MAD)
       m_CondExec ${EXAAF},NE,YES 1,EQ,$[GSM00MG] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A1} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "TM1"
 /DERIVEDFIELD CST_1_16 "EM1"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_97_7 97 CH 7
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAE
       ;;
(GSM00MAE)
       m_CondExec 00,EQ,GSM00MAD ${EXAAF},NE,YES 1,EQ,$[GSM00MG] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MARDI                       
#                   EDITION TOTAL DES STOCKS                                   
#    OMIT COND=(1,3,EQ,C'EM1',OR,1,3,EQ,C'TM1'),FORMAT=CH                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAG
       ;;
(GSM00MAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[GSM00MA] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A2} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "TM1"
 /DERIVEDFIELD CST_3_20 "TM2"
 /DERIVEDFIELD CST_5_24 "TM4"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAH
       ;;
(GSM00MAH)
       m_CondExec 00,EQ,GSM00MAG ${EXAAK},NE,YES 1,EQ,$[GSM00MA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU MERCREDI                    
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAJ
       ;;
(GSM00MAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[GSM00MB] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A3} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "EM1"
 /DERIVEDFIELD CST_1_16 "TM1"
 /DERIVEDFIELD CST_5_24 "ST1"
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_BI_92_5 92 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAK
       ;;
(GSM00MAK)
       m_CondExec 00,EQ,GSM00MAJ ${EXAAP},NE,YES 1,EQ,$[GSM00MB] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU JEUDI                       
#    OMIT COND=(1,3,EQ,C'EM1',OR,1,3,EQ,C'TM1'),FORMAT=CH                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAM
       ;;
(GSM00MAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[GSM00MC] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A4} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_20 "ST1"
 /DERIVEDFIELD CST_1_16 "TM1"
 /DERIVEDFIELD CST_5_24 "ST2"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_104_7 104 CH 7
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR FLD_CH_1_3 EQ CST_5_24 OR 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAN
       ;;
(GSM00MAN)
       m_CondExec 00,EQ,GSM00MAM ${EXAAU},NE,YES 1,EQ,$[GSM00MC] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU VENDREDI                    
#    OMIT COND=(1,3,EQ,C'EM1'),FORMAT=CH                                       
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAQ
       ;;
(GSM00MAQ)
       m_CondExec ${EXAAZ},NE,YES 1,EQ,$[GSM00MD] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A5} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "ST1"
 /DERIVEDFIELD CST_3_20 "ST2"
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_97_7 97 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_92_5 92 CH 5
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 OR FLD_CH_1_3 EQ CST_3_20 OR 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAR
       ;;
(GSM00MAR)
       m_CondExec 00,EQ,GSM00MAQ ${EXAAZ},NE,YES 1,EQ,$[GSM00MD] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER DES DEMANDES POUR L'EDITION DU SAMEDI                      
#    REPRISE : OUI                                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM00MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM00MAT
       ;;
(GSM00MAT)
       m_CondExec ${EXABE},NE,YES 1,EQ,$[GSM00ME] 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BGM075DM
       m_FileAssign -d NEW,CATLG,DELETE -r 227 -g ${G_A6} SORTOUT ${DATA}/PXX0/F89.BSM025BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_16 "TM1"
 /FIELDS FLD_CH_207_5 207 CH 5
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_BI_92_5 92 CH 5
 /FIELDS FLD_CH_17_20 17 CH 20
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_37_20 37 CH 20
 /FIELDS FLD_CH_57_20 57 CH 20
 /FIELDS FLD_CH_104_7 104 CH 7
 /FIELDS FLD_CH_87_5 87 CH 5
 /FIELDS FLD_CH_82_5 82 CH 5
 /FIELDS FLD_CH_202_5 202 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_14_3 14 CH 3
 /FIELDS FLD_CH_97_7 97 CH 7
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_16 
 /KEYS
   FLD_CH_1_10 DESCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_14_3 ASCENDING,
   FLD_CH_82_5 ASCENDING,
   FLD_CH_17_20 ASCENDING,
   FLD_CH_37_20 ASCENDING,
   FLD_CH_57_20 ASCENDING,
   FLD_CH_87_5 ASCENDING,
   FLD_CH_202_5 ASCENDING,
   FLD_CH_207_5 ASCENDING,
   FLD_BI_92_5 DESCENDING,
   FLD_CH_97_7 ASCENDING,
   FLD_CH_104_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 0227 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM00MAU
       ;;
(GSM00MAU)
       m_CondExec 00,EQ,GSM00MAT ${EXABE},NE,YES 1,EQ,$[GSM00ME] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
