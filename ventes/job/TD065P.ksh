#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD065P.ksh                       --- VERSION DU 08/10/2016 23:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPTD065 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/08/22 AT 10.29.04 BY BURTECA                      
#    STANDARDS: P  JOBSET: TD065P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   DELETE DES FICHIERS SéQUENTIELS FTD*                                       
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TD065PA
       ;;
(TD065PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=TD065PAA
       ;;
(TD065PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD065PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD065PAB
       ;;
(TD065PAB)
       m_CondExec 16,NE,TD065PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD003    DELETE DES ANOMALIES DANS LA TABLE RTTD03                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD065PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TD065PAD
       ;;
(TD065PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE  (SOUS TABLE TDAMA)                                
#    RTGA01   : NAME=RSGA01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******* TABLES EN MISE A JOUR (TABLE DES ANOMALIES)                          
#    RTTD03   : NAME=RSTD03P,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTTD03 /dev/null
#                                                                              
# ******* DATE DU JOUR                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN SORTIE                                                    
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD003 
       JUMP_LABEL=TD065PAE
       ;;
(TD065PAE)
       m_CondExec 04,GE,TD065PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTD065                                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD065PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TD065PAG
       ;;
(TD065PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLES EN LECTURE                                                    
#    RSTD00   : NAME=RSTD00P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD00 /dev/null
#    RSTD01   : NAME=RSTD01P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD01 /dev/null
#    RSTD02   : NAME=RSTD02P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD02 /dev/null
#    RSTD03   : NAME=RSTD03P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD03 /dev/null
#    RSTD04   : NAME=RSTD04P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD04 /dev/null
#    RSTD05   : NAME=RSTD05P,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSTD05 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 240 -t LSEQ FTD065 ${DATA}/PXX0.F07.FTD065AP
       m_FileAssign -d NEW,CATLG,DELETE -r 113 -t LSEQ FTD165 ${DATA}/PXX0.F07.FTD165AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ FTD265 ${DATA}/PXX0.F07.FTD265AP
       m_FileAssign -d NEW,CATLG,DELETE -r 101 -t LSEQ FTD365 ${DATA}/PXX0.F07.FTD365AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ FTD465 ${DATA}/PXX0.F07.FTD465AP
#  AJOUT LE 080505 (LRECL=250)                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 288 -t LSEQ FTD565 ${DATA}/PXX0.F07.FTD565AP
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
# ******* FICHIER EN SORTIE                                                    
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD065 
       JUMP_LABEL=TD065PAH
       ;;
(TD065PAH)
       m_CondExec 04,GE,TD065PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
