#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100O.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 16.19.15 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV100O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100OA
       ;;
(PV100OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+2'}
       MENS=${LASTJO}
       RUN=${RUN}
       JUMP_LABEL=PV100OAA
       ;;
(PV100OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F16.PV100HO.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV100OAA.BPV108EO
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPV102                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAD
       ;;
(PV100OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV100OAA.BPV108EO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100OAD.BPV108MO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_49_5 49 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_59_7 59 CH 7
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100OAE
       ;;
(PV100OAE)
       m_CondExec 00,EQ,PV100OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV102  CALCUL DES NIVEAUX D AGREGATION                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAG
       ;;
(PV100OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00O /dev/null
#  TABLE DES IMBRICATIONS DES  CODES MARKETING                                 
#    RTGA09O  : NAME=RSGA09O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09O /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGA11O  : NAME=RSGA11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11O /dev/null
#  TABLE DES EDITIONS                                                          
#    RTGA12O  : NAME=RSGA12O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14O  : NAME=RSGA14O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA20O  : NAME=RSGA20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20O /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA25O  : NAME=RSGA25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25O /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA29O  : NAME=RSGA29O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29O /dev/null
#  TABLE CODES DESCRIPTIFS CODIC                                               
#    RTGA53O  : NAME=RSGA53O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53O /dev/null
#  TABLE HISTORIQUE PRMP                                                       
#    RTGG50O  : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50O /dev/null
#  TABLE HISTORIQUE PRMP DACEM                                                 
#    RTGG55O  : NAME=RSGG55O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55O /dev/null
#  TABLM DES MVTS  DE STOCK                                                    
#    RTGS40O  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40O /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31O  : NAME=RSGV31O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31O /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A2} FPV100 ${DATA}/PTEM/PV100OAD.BPV108MO
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00O  : NAME=RSAN00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00O /dev/null
#  SORTIE FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV102 ${DATA}/PTEM/PV100OAG.BPV102AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV102 
       JUMP_LABEL=PV100OAH
       ;;
(PV100OAH)
       m_CondExec 04,GE,PV100OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV103 MAJ DE LA TABLE HV32                   
#  POUR PGM BPV103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAJ
       ;;
(PV100OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PV100OAG.BPV102AO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100OAJ.BPV103AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100OAK
       ;;
(PV100OAK)
       m_CondExec 00,EQ,PV100OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV103  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAM
       ;;
(PV100OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTGV31O  : NAME=RSGV31O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31O /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32O  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32O /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A4} FPV103 ${DATA}/PTEM/PV100OAJ.BPV103AO
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  MAJ DE LA TABLE RTHV32                                                      
#    RSHV32O  : NAME=RSHV32O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV32O /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00O  : NAME=RSAN00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00O /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV103 
       JUMP_LABEL=PV100OAN
       ;;
(PV100OAN)
       m_CondExec 04,GE,PV100OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV104                                        
#  POUR PGM BPV104                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAQ
       ;;
(PV100OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV100OAA.BPV108EO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100OAQ.BPV104AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_21 "     "
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 OR FLD_CH_171_5 EQ CST_3_21 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100OAR
       ;;
(PV100OAR)
       m_CondExec 00,EQ,PV100OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV104  MAJ DE  LA TABLE HV26                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAT
       ;;
(PV100OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****************************** FICHIER FMOIS                                 
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# *************************TABLE HISTORIQUE DES VENTES DE PSE                  
#    RTGV26O  : NAME=RSGV26O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV26O /dev/null
#                                                                              
# ******************************FICHIER ISSU DU TRI PRECEDENT                  
       m_FileAssign -d SHR -g ${G_A6} FPV104 ${DATA}/PTEM/PV100OAQ.BPV104AO
# *********************MAJ DE LA TABLE RTHV26 HISTORIQUE VENTE PSE             
#    RSHV26O  : NAME=RSHV26O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV26O /dev/null
# *********************SORTIE TABLES ANOMALIES                                 
#    RTAN00O  : NAME=RSAN00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00O /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV104 
       JUMP_LABEL=PV100OAU
       ;;
(PV100OAU)
       m_CondExec 04,GE,PV100OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV107 MAJ DE LA TABLE HV33                   
#  POUR PGM BPV107                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV100OAX
       ;;
(PV100OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV100OAG.BPV102AO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100OAX.BPV107AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100OAY
       ;;
(PV100OAY)
       m_CondExec 00,EQ,PV100OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV107  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBA
       ;;
(PV100OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE DES VENDEURS                                                          
#    RTGV31O  : NAME=RSGV31O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31O /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32O  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32O /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A8} FPV107 ${DATA}/PTEM/PV100OAX.BPV107AO
#  MAJ DE LA TABLE RTHV32 HISTORIQUE VENTES                                    
#    RSHV33O  : NAME=RSHV33O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV33O /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV107 
       JUMP_LABEL=PV100OBB
       ;;
(PV100OBB)
       m_CondExec 04,GE,PV100OBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBD
       ;;
(PV100OBD)
       m_CondExec ${EXABT},NE,YES 1,NE,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01O /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10O /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31O  : NAME=RSGV31O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
       m_OutputAssign -c 9 -w IPV110 IMPRIM
#  FORMATTAGE DU FICHIER A DESTINATION DE L EDITION GENERALISE                 
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG110 ${DATA}/PXX0/F16.BPV110AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100OBE
       ;;
(PV100OBE)
       m_CondExec 04,GE,PV100OBD ${EXABT},NE,YES 1,NE,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBG
       ;;
(PV100OBG)
       m_CondExec ${EXABY},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01O /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10O /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31O  : NAME=RSGV31O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32D  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV110                                                    
#  IMPRIM   REPORT SYSOUT=(9,IPV110),LRECL=189,RECFM=FBA,BLKSIZE=1890          
       m_FileAssign -d NEW,CATLG,DELETE -r 189 -t LSEQ -g +1 IMPRIM ${DATA}/PXX0/F16.IPV110AO
#  FORMATTAGE DU FICHIER A DESTINATION DE L EDITION GENERALISE                 
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g ${G_A9} FEG110 ${DATA}/PXX0/F16.BPV110AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100OBH
       ;;
(PV100OBH)
       m_CondExec 04,GE,PV100OBG ${EXABY},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV110 ETAT DE PREPARATION DE PAYE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBJ PGM=IEBGENER   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBJ
       ;;
(PV100OBJ)
       m_CondExec ${EXACD},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A10} SYSUT1 ${DATA}/PXX0/F16.IPV110AO
       m_OutputAssign -c 9 -w IPV110 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PV100OBK
       ;;
(PV100OBK)
       m_CondExec 00,EQ,PV100OBJ ${EXACD},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBM
       ;;
(PV100OBM)
       m_CondExec ${EXACI},NE,YES 1,NE,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01O /dev/null
#  TABLE                                                                       
#    RTGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32O  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32O /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
       m_OutputAssign -c 9 -w IPV111 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100OBN
       ;;
(PV100OBN)
       m_CondExec 04,GE,PV100OBM ${EXACI},NE,YES 1,NE,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBQ
       ;;
(PV100OBQ)
       m_CondExec ${EXACN},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01O /dev/null
#  TABLE                                                                       
#    RTGA10O  : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10O /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32O  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32O /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
#  IMPRIM   REPORT SYSOUT=(9,IPV111),LRECL=199,RECFM=FBA,BLKSIZE=1990          
       m_FileAssign -d NEW,CATLG,DELETE -r 199 -t LSEQ -g +1 IMPRIM ${DATA}/PXX0/F16.IPV111AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100OBR
       ;;
(PV100OBR)
       m_CondExec 04,GE,PV100OBQ ${EXACN},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV111 ETAT DE PREPARATION DE PAYE                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBT
       ;;
(PV100OBT)
       m_CondExec ${EXACS},NE,YES 1,EQ,$[MENS] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A11} SYSUT1 ${DATA}/PXX0/F16.IPV111AO
       m_OutputAssign -c 9 -w IPV111 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=PV100OBU
       ;;
(PV100OBU)
       m_CondExec 00,EQ,PV100OBT ${EXACS},NE,YES 1,EQ,$[MENS] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OBX PGM=BPV105     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PV100OBX
       ;;
(PV100OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F16.PV100HO.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F16.PV100HO.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  : PGM D EPURATION DE HV32 + HV33                                
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100OCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=PV100OCA
       ;;
(PV100OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTHV32D  : NAME=RSHV32O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32D /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV33D  : NAME=RSHV33O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV33D /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01D  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01D /dev/null
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *****                                                                        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100OCB
       ;;
(PV100OCB)
       m_CondExec 04,GE,PV100OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV100OZA
       ;;
(PV100OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV100OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
