#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRD00L.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLGRD00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 05/05/16 AT 10.28.41 BY BURTEC7                      
#    STANDARDS: P  JOBSET: GRD00L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DU TABLESPACE RSRD00L                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GRD00LA
       ;;
(GRD00LA)
#
#GRD00LAA
#GRD00LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#GRD00LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GRD00LAD
       ;;
(GRD00LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BHV030AL
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00LAD.BRD005FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_11 "VEN"
 /DERIVEDFIELD CST_1_7 "VEN"
 /FIELDS FLD_CH_27_3 27 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_33_7 33 CH 7
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_7 OR FLD_CH_13_3 EQ CST_3_11 
 /KEYS
   FLD_CH_27_3 ASCENDING,
   FLD_CH_30_3 ASCENDING,
   FLD_CH_33_7 ASCENDING,
   FLD_CH_19_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LAE
       ;;
(GRD00LAE)
       m_CondExec 00,EQ,GRD00LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD005                                                                
# ********************************************************************         
#   TRAITEMENT DE LA REDEVANCE AUDIOVISUELLE                                   
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAG
       ;;
(GRD00LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A1} FGS41 ${DATA}/PTEM/GRD00LAD.BRD005FL
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE LIEUX                                                          
#    RSGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES FAMILLES/TYPE DE DECLARATION                               
#    RSGA15   : NAME=RSGA15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA15 /dev/null
# ******  TABLE DES MARQUES                                                    
#    RSGA22   : NAME=RSGA22L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  TABLE DES ADRESSES DE VENTES                                         
#    RSGV02   : NAME=RSGV02L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV02 /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10   : NAME=RSGV10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10 /dev/null
# ******  TABLE DES LIGNES DE VENTES                                           
#    RSGV11   : NAME=RSGV11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00   : NAME=RSRD00L,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00L  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00 /dev/null
# ******  FICHIER DES VENTES A DECLARER                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -g +1 FRD005 ${DATA}/PXX0/F61.BRD005HL
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 FRD006 ${DATA}/PTEM/GRD00LAG.BRD006AL
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 278 -g +1 FRD007 ${DATA}/PTEM/GRD00LAG.BRD007AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD005 
       JUMP_LABEL=GRD00LAH
       ;;
(GRD00LAH)
       m_CondExec 04,GE,GRD00LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD020                                                                
# ********************************************************************         
#  EDITION DES VENTES SOUMISES A LA REDEVANCE AUDIOVISUELLE                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#  AAK      STEP  PGM=BRD020,LANG=CBL                                          
#  FICHIER DATE                                                                
# FDATE    DATA CLASS=VAR,PARMS=FDATE                                          
#  FDATE    FILE  NAME=FDATE,MODE=I                                            
#  FICHIER DES VENTES A DECLARER                                               
#  FRD005   FILE  NAME=BRD005HL,MODE=I                                         
#  EDITION DES VENTES A DECLARER                                               
#  IRD020   REPORT SYSOUT=(9,IRD020)                                           
# ********************************************************************         
#  PGM : BRD015                                                                
# ********************************************************************         
#  EDITION DES ANOMALIES                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAJ PGM=BRD015     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAJ
       ;;
(GRD00LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DES ANOMALIES                                      
       m_FileAssign -d SHR -g ${G_A2} FRD007 ${DATA}/PTEM/GRD00LAG.BRD007AL
# ******  EDITION DES ANOMALIES                                                
# IRD015   REPORT SYSOUT=(9,IRD015)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD015 ${DATA}/PXX0/F61.BIRD015L
       m_ProgramExec BRD015 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAM PGM=IEBGENER   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAM
       ;;
(GRD00LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PXX0/F61.BIRD015L
       m_OutputAssign -c 9 -w IRD015 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00LAN
       ;;
(GRD00LAN)
       m_CondExec 00,EQ,GRD00LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD006 AVANT EDITION PGM BRD010                            
#   CODIC : 17,10,A (MAGASIN ET N� DE VENTE )                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAQ
       ;;
(GRD00LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GRD00LAG.BRD006AL
       m_FileAssign -d NEW,CATLG,DELETE -r 231 -g +1 SORTOUT ${DATA}/PEX0/F61.BRD006BL.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_10 17 CH 10
 /KEYS
   FLD_CH_17_10 ASCENDING
 /* Record Type = F  Record Length = 191 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LAR
       ;;
(GRD00LAR)
       m_CondExec 00,EQ,GRD00LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BRD010                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAT PGM=BRD010     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAT
       ;;
(GRD00LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A5} FRD006 ${DATA}/PEX0/F61.BRD006BL.ETAT
# ******  EDITION LISTE CONTROLE                                               
# IRD010   REPORT SYSOUT=(9,IRD010)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD010 ${DATA}/PXX0/F61.BIRD010L
       m_ProgramExec BRD010 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LAX PGM=IEBGENER   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LAX
       ;;
(GRD00LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A6} SYSUT1 ${DATA}/PXX0/F61.BIRD010L
       m_OutputAssign -c 9 -w IRD010 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00LAY
       ;;
(GRD00LAY)
       m_CondExec 00,EQ,GRD00LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER STOCK (DD FGS41 DU PGM BRD005)                             
#   CODIC = 19,7                                                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBA
       ;;
(GRD00LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GRD00LAD.BRD005FL
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/GRD00LBA.BRD005GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 07
 /KEYS
   FLD_CH_19_7 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LBB
       ;;
(GRD00LBB)
       m_CondExec 00,EQ,GRD00LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# * AJOUT PGM BRD040  ************************************************         
# ********************************************************************         
#  PGM : BRD040                                                                
# ********************************************************************         
#   CONSTITUTION DES FICHIERS EDDITION DE CONTROLE DES DECLARATION             
#   REPRISE: OUI SI ABEND                                                      
#            CE PGM NE FAIT AUCUNE MAJ SUR LES TABLES DB2                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBD
       ;;
(GRD00LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#         FICHIERS EN ENTREE                                                   
#                                                                              
# ****  FICHIER RESULTAT DES STOCKS TRIES                                      
       m_FileAssign -d SHR -g ${G_A8} FGS41 ${DATA}/PTEM/GRD00LBA.BRD005GL
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER SOCIETE                                                      
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER MOIS                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******  CODIC SOUMIS A DECLARATION                                           
#    RSGA51M  : NAME=RSGA51L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA51M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISE                                                     
#    RSGA01M  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE DES ENTETES DE VENTES                                          
#    RSGV10M  : NAME=RSGV10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV10M /dev/null
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00R  : NAME=RSRD00L,MODE=(U,U) - DYNAM=YES                            
# -X-RSRD00L  - IS UPDATED IN PLACE WITH MODE=(U,U)                            
       m_FileAssign -d SHR RSRD00R /dev/null
#                                                                              
#         FICHIERS EN SORTIES                                                  
#                                                                              
# ******  FICHIER DES CODICS A DECLARER ET DECLARES : LRECL 63                 
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 FRD040 ${DATA}/PEX0/F61.BRD040AL.ETAT
# ******  FICHIER DES CODICS NON DECLARABLES:         LRECL 75                 
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 FRD050 ${DATA}/PEX0/F61.BRD050AL.ETAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRD040 
       JUMP_LABEL=GRD00LBE
       ;;
(GRD00LBE)
       m_CondExec 04,GE,GRD00LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD040 DU PGM BRD041            
#   POUR EDITION ETAT IRD040                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBG
       ;;
(GRD00LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PEX0/F61.BRD040AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00LBG.BRD040BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_45_6 45 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LBH
       ;;
(GRD00LBH)
       m_CondExec 00,EQ,GRD00LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD041 DU PGM BRD041            
#   POUR EDITION ETAT IRD041                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBJ
       ;;
(GRD00LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PEX0/F61.BRD040AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00LBJ.BRD040CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_PD_33_6 33 PD 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LBK
       ;;
(GRD00LBK)
       m_CondExec 00,EQ,GRD00LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD040         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD042 DU PGM BRD041            
#   POUR EDITION ETAT IRD042                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBM
       ;;
(GRD00LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PEX0/F61.BRD040AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 63 -g +1 SORTOUT ${DATA}/PTEM/GRD00LBM.BRD040DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_27_6 27 PD 6
 /FIELDS FLD_PD_45_6 45 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_PD_33_6 33 PD 6
 /FIELDS FLD_PD_21_6 21 PD 6
 /FIELDS FLD_PD_51_6 51 PD 6
 /FIELDS FLD_PD_39_6 39 PD 6
 /FIELDS FLD_CH_10_6 10 CH 6
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_21_6,
    TOTAL FLD_PD_27_6,
    TOTAL FLD_PD_33_6,
    TOTAL FLD_PD_39_6,
    TOTAL FLD_PD_45_6,
    TOTAL FLD_PD_51_6
 /* Record Type = F  Record Length = 063 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LBN
       ;;
(GRD00LBN)
       m_CondExec 00,EQ,GRD00LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD041                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD040 + IRD041 + IRD042                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBQ PGM=BRD041     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBQ
       ;;
(GRD00LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A12} FRD040 ${DATA}/PTEM/GRD00LBG.BRD040BL
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A13} FRD041 ${DATA}/PTEM/GRD00LBJ.BRD040CL
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A14} FRD042 ${DATA}/PTEM/GRD00LBM.BRD040DL
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD040 IRD040
# ******  EDITION LISTE CONTROLE                                               
# IRD041   REPORT SYSOUT=(9,IRD041),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD041 ${DATA}/PXX0/F61.BIRD041L
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD042 IRD042
       m_ProgramExec BRD041 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBT PGM=IEBGENER   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBT
       ;;
(GRD00LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A15} SYSUT1 ${DATA}/PXX0/F61.BIRD041L
       m_OutputAssign -c 9 -w IRD041 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00LBU
       ;;
(GRD00LBU)
       m_CondExec 00,EQ,GRD00LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD050 DU PGM BRD050            
#   POUR EDITION ETAT IRD050                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LBX
       ;;
(GRD00LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PEX0/F61.BRD050AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00LBX.BRD050BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_16_5 16 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LBY
       ;;
(GRD00LBY)
       m_CondExec 00,EQ,GRD00LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD050 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD051 DU PGM BRD050            
#   POUR EDITION ETAT IRD051                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LCA
       ;;
(GRD00LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PEX0/F61.BRD050AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00LCA.BRD050CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_16_5 16 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LCB
       ;;
(GRD00LCB)
       m_CondExec 00,EQ,GRD00LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FRD040 OBTENU PRECEDEMMENT (PGM BRD040 + DD FRD050         
#   LE FICHIER EN SORTIE DEVIENDRA L ENTREE DD FRD052 DU PGM BRD050            
#   POUR EDITION ETAT IRD052                                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LCD
       ;;
(GRD00LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ****  FICHIER RESULTAT DU TRI PRECEDENT                                      
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PEX0/F61.BRD050AL.ETAT
       m_FileAssign -d NEW,CATLG,DELETE -r 75 -g +1 SORTOUT ${DATA}/PTEM/GRD00LCD.BRD050DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_26_7 26 CH 7
 /FIELDS FLD_CH_10_6 10 CH 6
 /FIELDS FLD_PD_53_6 53 PD 6
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_16_5 16 CH 5
 /KEYS
   FLD_CH_10_6 ASCENDING,
   FLD_CH_16_5 ASCENDING,
   FLD_CH_59_7 DESCENDING,
   FLD_CH_26_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_53_6
 /* Record Type = F  Record Length = 075 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GRD00LCE
       ;;
(GRD00LCE)
       m_CondExec 00,EQ,GRD00LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM :BRDD050                                                                
# ********************************************************************         
#  EDITION LISTE CONTROLE                                                      
#  ETATS IRD050 + IRD051 + IRD052                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCG PGM=BRD050     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LCG
       ;;
(GRD00LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******* FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A19} FRD050 ${DATA}/PTEM/GRD00LBX.BRD050BL
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A20} FRD051 ${DATA}/PTEM/GRD00LCA.BRD050CL
# ******  FICHIER D'EDITION DE LA LISTE DE CONTROLE                            
       m_FileAssign -d SHR -g ${G_A21} FRD052 ${DATA}/PTEM/GRD00LCD.BRD050DL
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD050 IRD050
# ******  EDITION LISTE CONTROLE                                               
# IRD051   REPORT SYSOUT=(9,IRD051),LRECL=133,RECFM=FBA,BLKSIZE=1330           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -g +1 IRD051 ${DATA}/PXX0/F61.BIRD051L
# ******  EDITION LISTE CONTROLE                                               
       m_OutputAssign -c 9 -w IRD052 IRD052
       m_ProgramExec BRD050 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCJ PGM=IEBGENER   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LCJ
       ;;
(GRD00LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A22} SYSUT1 ${DATA}/PXX0/F61.BIRD051L
       m_OutputAssign -c 9 -w IRD051 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=GRD00LCK
       ;;
(GRD00LCK)
       m_CondExec 00,EQ,GRD00LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#  PGM : BUR0075                                                               
# ********************************************************************         
#   PURGE DE LA RTRD00 TOUS CE QUI EST INFERIEUR A 90 JOURS                    
#   REPRISE: OUI SI ABEND                                                      
#            NON SI FIN NORMALE FAIRE UN RECOVER TO RBA A PARTIR DU            
#            QUIESCE DU DEBUT DE CHAINE POUR LA TABLE RTRD00                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LCM
       ;;
(GRD00LCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  TABLE DES HISTORIQUES DES DECLARATIONS                               
#    RSRD00L  : NAME=RSRD00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSRD00L /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BUR0075 
       JUMP_LABEL=GRD00LCN
       ;;
(GRD00LCN)
       m_CondExec 04,GE,GRD00LCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSRD00L                                       
# ********************************************************************         
#   REPRISE : OUI APRES UN TERM UTILITY                                        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GRD00LCQ PGM=DSNUTILB   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=GRD00LZA
       ;;
(GRD00LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRD00LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
