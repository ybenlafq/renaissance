#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VE900P.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVE900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/03/10 AT 11.01.02 BY BURTECA                      
#    STANDARDS: P  JOBSET: VE900P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTVE02-05-06-08-10-11-14                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VE900PA
       ;;
(VE900PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VE900PAA
       ;;
(VE900PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PARG/SEM.VE900PA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VE900PAA
       m_ProgramExec IEFBR14 "RDAR,VE900P.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900PAD
       ;;
(VE900PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VE900PA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VE900PAE
       ;;
(VE900PAE)
       m_CondExec 00,EQ,VE900PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAG
       ;;
(VE900PAG)
       m_CondExec ${EXAAK},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
#    RSVE14   : NAME=RSVE14,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900PAG.VE900P01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900PAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAJ
       ;;
(VE900PAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11   : NAME=RSVE11,MODE=I - DYNAM=YES                                 
#    RSVE14   : NAME=RSVE14,MODE=I - DYNAM=YES                                 
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900PAJ.UNVE11AP
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FIC SUR NLIEU,NVENTE 4,10,A                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAM
       ;;
(VE900PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VE900PAG.VE900P01
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SORTOUT ${DATA}/PTEM/VE900PAM.VE900P02
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900PAN
       ;;
(VE900PAN)
       m_CondExec 00,EQ,VE900PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9000                                                               
#   PREPARATION FICHIER PURGE ET FICHIER DE LOAD RTVE11                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAQ
       ;;
(VE900PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#         TABLE RTVE11                                                         
#    RSVE11   : NAME=RSVE11,MODE=(U,N) - DYNAM=YES                             
# -X-VE900PR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSVE11 /dev/null
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  RTVE11 TRI PAR LIEU ET NVENTE                                               
       m_FileAssign -d SHR -g ${G_A3} VE0011A ${DATA}/PTEM/VE900PAM.VE900P02
#  FICHIER POUR LE LOAD DE LA RTVE11                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011L ${DATA}/PARG/SEM.VE900PB
#  FICHIER DE PURGE DE LA RTVE11                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011P ${DATA}/PARG/SEM.VE900PC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9000 
       JUMP_LABEL=VE900PAR
       ;;
(VE900PAR)
       m_CondExec 04,GE,VE900PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVE11                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAT
       ;;
(VE900PAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSVE11   : NAME=RSVE11,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE11 /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PARG/SEM.VE900PB
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/VE900PAJ.UNVE11AP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR DFSPARM ${DATA}/CORTEX4.P.MTXTFIX3/VE900PAT
       m_FileAssign -d SHR SORTDIAG /dev/null
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900PAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VE900P_VE900PAT_RTVE11.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900PAU
       ;;
(VE900PAU)
       m_CondExec 04,GE,VE900PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE910                                                                
#  EPURATION DES TABLES RTVE10 RTVE11                                          
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VE900PAX
       ;;
(VE900PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ENTETES DE VENTES                                                 
#    RSVE10   : NAME=RSVE10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE10 /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RSVE11   : NAME=RSVE11,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE11 /dev/null
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 FVE910 ${DATA}/PARG/SEM.VE900PD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE910 
       JUMP_LABEL=VE900PAY
       ;;
(VE900PAY)
       m_CondExec 04,GE,VE900PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DE PURGE POUR LE PASSER A UN LRECL = 20                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBA
       ;;
(VE900PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PARG/SEM.VE900PC
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/VE900PBA.VE900P03
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_52_3 52 PD 3
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_52_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900PBB
       ;;
(VE900PBB)
       m_CondExec 00,EQ,VE900PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BVE910 ET DU TRI PRECEDENT POUR CREER            
#  LE FICHIER VE900PE                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBD
       ;;
(VE900PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/VE900PBA.VE900P03
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PARG/SEM.VE900PD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PARG/SEM.VE900PE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900PBE
       ;;
(VE900PBE)
       m_CondExec 00,EQ,VE900PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9002                                                               
#  PURGE DE LA TABLE RTVE02 TABLE DES ADRESSES                                 
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBG
       ;;
(VE900PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENTES ADRESSES                                                   
#    RSVE02   : NAME=RSVE02,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE02 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBG
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A9} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 FVE020 ${DATA}/PARG/SEM.VE900PF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9002 
       JUMP_LABEL=VE900PBH
       ;;
(VE900PBH)
       m_CondExec 04,GE,VE900PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9005                                                               
#  PURGE DE LA TABLE RTVE05                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBJ
       ;;
(VE900PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE05   : NAME=RSVE05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE05 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBJ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A10} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 FVE005 ${DATA}/PARG/SEM.VE900PG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9005 
       JUMP_LABEL=VE900PBK
       ;;
(VE900PBK)
       m_CondExec 04,GE,VE900PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9006                                                               
#  PURGE DE LA TABLE RTVE06                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBM
       ;;
(VE900PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE06   : NAME=RSVE06,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE06 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBM
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A11} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 FVE006 ${DATA}/PARG/SEM.VE900PH
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9006 
       JUMP_LABEL=VE900PBN
       ;;
(VE900PBN)
       m_CondExec 04,GE,VE900PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9008                                                               
#  PURGE DE LA TABLE RTVE08                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBQ
       ;;
(VE900PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE08   : NAME=RSVE08,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE08 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A12} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 FVE008 ${DATA}/PARG/SEM.VE900PI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9008 
       JUMP_LABEL=VE900PBR
       ;;
(VE900PBR)
       m_CondExec 04,GE,VE900PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9014                                                               
#  PURGE DE LA TABLE DES REGLEMENTS                                            
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBT
       ;;
(VE900PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES REGLEMENTS                                                        
#    RSVE14   : NAME=RSVE14,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE14 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBT
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A13} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 FVE140 ${DATA}/PARG/SEM.VE900PJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9014 
       JUMP_LABEL=VE900PBU
       ;;
(VE900PBU)
       m_CondExec 04,GE,VE900PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9010                                                               
#  PURGE DE LA TABLE RTVE10                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VE900PBX
       ;;
(VE900PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSVE10   : NAME=RSVE10,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSVE10 /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900PBX
#  FICHIER                                                                     
       m_FileAssign -d SHR -g ${G_A14} FVE900 ${DATA}/PARG/SEM.VE900PE
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 FVE100 ${DATA}/PARG/SEM.VE900PK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9010 
       JUMP_LABEL=VE900PBY
       ;;
(VE900PBY)
       m_CondExec 04,GE,VE900PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VE900PZA
       ;;
(VE900PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
