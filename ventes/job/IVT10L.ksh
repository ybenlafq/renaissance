#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVT10L.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLIVT10 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 06/03/03 AT 15.59.17 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVT10L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#    QUIESCE DU TABLESPACE RSIT15                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVT10LA
       ;;
(IVT10LA)
#
#IVT10LAA
#IVT10LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVT10LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVT10LAD
       ;;
(IVT10LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE ARTICLE / GENERALITES ARTICLES                                 
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLES DES FAMILLES                                                  
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK SOUS LIEU MAGASIN                                        
#    RSGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE LIEUX INVENTORIES                                              
#    RSIT05   : NAME=RSIT05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT05 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSIT10   : NAME=RSIT10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT10 /dev/null
# ******  TABLE DES FAMILLES INVENTORIES                                       
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  PARAMETRE SOCIETE 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES CODICS A INVENTORIER                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 FIT010 ${DATA}/PXX0/F61.BIT010AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT010 
       JUMP_LABEL=IVT10LAE
       ;;
(IVT10LAE)
       m_CondExec 04,GE,IVT10LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTIT15                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAG
       ;;
(IVT10LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -r 35 -g +1 SYSREC01 ${DATA}/PGI961/F61.UNLOAD.IT15UL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10LAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI SUR N�INVENTAIRE ; NLIEU ; NSOCIETE ; NSSLIEU ; NCODIC                  
#  OUTREC 1 ; 35                                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAJ
       ;;
(IVT10LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F61.BIT010AL
# ******  FICHIER DES CODICS POUR LOAD RTIT15                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PTEM/IVT10LAJ.BIT010GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_1_35 1 CH 35
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_35
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10LAK
       ;;
(IVT10LAK)
       m_CondExec 00,EQ,IVT10LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI N�2                                                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAM
       ;;
(IVT10LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVT10LAJ.BIT010GL
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PGI961/F61.UNLOAD.IT15UL
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 SORTOUT ${DATA}/PGI961/F61.RELOAD.IT15RL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_9_3 ASCENDING,
   FLD_CH_12_3 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /* Record Type = F  Record Length = 35 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LAN
       ;;
(IVT10LAN)
       m_CondExec 00,EQ,IVT10LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTIT15                                                     
# ********************************************************************         
#  REPRISE : OUI SI FIN ANORMALE                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAQ
       ;;
(IVT10LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK02 ${MT_TMP}/SORTWK02_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK03 ${MT_TMP}/SORTWK03_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK04 ${MT_TMP}/SORTWK04_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK05 ${MT_TMP}/SORTWK05_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTIT15                                                
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PGI961/F61.RELOAD.IT15RL
#                                                                              
#    RSIT15   : NAME=RSIT15L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSIT15 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10LAQ.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVT10L_IVT10LAQ_RTIT15.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVT10LAR
       ;;
(IVT10LAR)
       m_CondExec 04,GE,IVT10LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAT
       ;;
(IVT10LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F61.BIT010AL
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10LAT.BIT010BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_14 " "
 /DERIVEDFIELD CST_1_10 "1"
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_9_3 9 CH 3
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LAU
       ;;
(IVT10LAU)
       m_CondExec 00,EQ,IVT10LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT011 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LAX PGM=BIT011     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LAX
       ;;
(IVT10LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A6} FIT011 ${DATA}/PTEM/IVT10LAT.BIT010BL
       m_OutputAssign -c 9 -w BIT011 IIT011
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F61.BIT010CL
       m_ProgramExec BIT011 
# ********************************************************************         
#  EDITION BIT011 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBA
       ;;
(IVT10LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F61.BIT010CL
       m_OutputAssign -c 9 -w BIT011 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=IVT10LBB
       ;;
(IVT10LBB)
       m_CondExec 00,EQ,IVT10LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR MAG ; N INVENTRE ; SEQ FAMIL ; CODE FAM ; CODE MARQ ; CODIC         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBD
       ;;
(IVT10LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F61.BIT010AL
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 90 -g +1 SORTOUT ${DATA}/PTEM/IVT10LBD.BIT010DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_10 "2"
 /DERIVEDFIELD CST_3_14 " "
 /FIELDS FLD_CH_39_5 39 CH 5
 /FIELDS FLD_CH_85_1 85 CH 1
 /FIELDS FLD_CH_6_3 6 CH 3
 /FIELDS FLD_CH_12_3 12 CH 3
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_44_5 44 CH 5
 /FIELDS FLD_CH_36_3 36 CH 3
 /FIELDS FLD_CH_9_3 9 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /CONDITION CND_1 FLD_CH_85_1 EQ CST_1_10 AND FLD_CH_12_3 EQ CST_3_14 
 /KEYS
   FLD_CH_9_3 ASCENDING,
   FLD_CH_6_3 ASCENDING,
   FLD_CH_1_5 ASCENDING,
   FLD_CH_36_3 ASCENDING,
   FLD_CH_39_5 ASCENDING,
   FLD_CH_44_5 ASCENDING,
   FLD_CH_16_7 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 90 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LBE
       ;;
(IVT10LBE)
       m_CondExec 00,EQ,IVT10LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT031 : CREATION D UN FICHIER DESTINE A ETRE TRANSMIS AUX MAGASINS         
#           (TOUS LES PRODUITS A INVENTORIER)                                  
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBG PGM=BIT031     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBG
       ;;
(IVT10LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER TRIE                                                         
       m_FileAssign -d SHR -g ${G_A9} FIT011 ${DATA}/PTEM/IVT10LBD.BIT010DL
       m_OutputAssign -c 9 -w BIT031 IIT031
# ******  FICHIER A ENVOYER SUR LES 36                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEDG36 ${DATA}/PXX0/F61.BIT010EL
       m_ProgramExec BIT031 
# ********************************************************************         
#  BIT015  POUR TOUS LES CODICS A INVENTORIER DONT LA DATE DE                  
#          CONSTITUTUION DU STOCK THEORIQUE EST EGALE A LA DATE DE             
#          TRAITEMENT IL Y A CALCUL DU STOCK ET INSCRIPTION DANS RTIT1         
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBJ
       ;;
(IVT10LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLES DES LIEUX                                                     
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE STOCK / SOUS LIEU ENTREPOT                                     
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS15   : NAME=RSGS15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS15 /dev/null
# ******  TABLE STOCK SOUS LIEU / MAGASINS                                     
#    RSGS30   : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  TABLE STOCK                                                          
#    RSGS60   : NAME=RSGS60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT015 
       JUMP_LABEL=IVT10LBK
       ;;
(IVT10LBK)
       m_CondExec 04,GE,IVT10LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT035                                                                      
# ********************************************************************         
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBM
       ;;
(IVT10LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE STOCKS                                                         
#    RSGS36   : NAME=RSGS36L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36 /dev/null
# ******  TABLE DES PARAMETRES GENERAUX D INVENTAIRE                           
#    RSIT00   : NAME=RSIT00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
# ******  TABLE DES CODICS A INVENTORIER                                       
#    RSIT15   : NAME=RSIT15L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRE SOCIETE 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER DATE SOUS LA FORME JJMMSSAA                                  
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT035 
       JUMP_LABEL=IVT10LBN
       ;;
(IVT10LBN)
       m_CondExec 04,GE,IVT10LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIT100  BUT: PRAPARATION DU FICHIER POUR EDITION DES ETIQUETTES             
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBQ
       ;;
(IVT10LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA59   : NAME=RSGA59L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA59 /dev/null
#    RSIT00   : NAME=RSIT00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PTEM/IVT10LBQ.BIT100AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT100 
       JUMP_LABEL=IVT10LBR
       ;;
(IVT10LBR)
       m_CondExec 04,GE,IVT10LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBT
       ;;
(IVT10LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/IVT10LBQ.BIT100AL
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PTEM/IVT10LBT.BIT100BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_16_15 16 CH 15
 /FIELDS FLD_CH_2_14 2 CH 14
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LBU
       ;;
(IVT10LBU)
       m_CondExec 00,EQ,IVT10LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES                                             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LBX PGM=BIV105     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LBX
       ;;
(IVT10LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES                                               
       m_FileAssign -d SHR -g ${G_A11} FIN105 ${DATA}/PTEM/IVT10LBT.BIT100BL
#                                                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIT040  BUT: EDITION D'UN ETAT D INVENTAIRE COMLPLET                        
#  REPRISE : OUI SI ABEND                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LCA
       ;;
(IVT10LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA67   : NAME=RSGA67L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSEG90   : NAME=RSEG90L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSEG90 /dev/null
#    RSIT00   : NAME=RSIT00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT00 /dev/null
#    RSIT15   : NAME=RSIT15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIT15 /dev/null
# ******  PARAMETRES                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IIT004 ${DATA}/PTEM/IVT10LCA.BIT040AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIT040 
       JUMP_LABEL=IVT10LCB
       ;;
(IVT10LCB)
       m_CondExec 04,GE,IVT10LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BIT040AL)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LCD
       ;;
(IVT10LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/IVT10LCA.BIT040AL
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10LCD.BIT040BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_12_3 12 CH 03
 /FIELDS FLD_BI_23_5 23 CH 05
 /FIELDS FLD_BI_28_9 28 CH 09
 /FIELDS FLD_BI_18_5 18 CH 05
 /FIELDS FLD_BI_7_5 7 CH 05
 /FIELDS FLD_BI_44_2 44 CH 02
 /FIELDS FLD_BI_37_7 37 CH 07
 /FIELDS FLD_BI_15_3 15 CH 03
 /KEYS
   FLD_BI_7_5 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_3 ASCENDING,
   FLD_BI_18_5 ASCENDING,
   FLD_BI_23_5 ASCENDING,
   FLD_BI_28_9 ASCENDING,
   FLD_BI_37_7 ASCENDING,
   FLD_BI_44_2 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LCE
       ;;
(IVT10LCE)
       m_CondExec 00,EQ,IVT10LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BIT040CL                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LCG
       ;;
(IVT10LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/IVT10LCD.BIT040BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/IVT10LCG.BIT040CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=IVT10LCH
       ;;
(IVT10LCH)
       m_CondExec 04,GE,IVT10LCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LCJ
       ;;
(IVT10LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/IVT10LCG.BIT040CL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/IVT10LCJ.BIT040DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVT10LCK
       ;;
(IVT10LCK)
       m_CondExec 00,EQ,IVT10LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : IIT004                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVT10LCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LCM
       ;;
(IVT10LCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/IVT10LCD.BIT040BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/IVT10LCJ.BIT040DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IIT004 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=IVT10LCN
       ;;
(IVT10LCN)
       m_CondExec 04,GE,IVT10LCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVT10LZA
       ;;
(IVT10LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVT10LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
