#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TD075M.ksh                       --- VERSION DU 08/10/2016 22:01
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMTD075 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/14 AT 10.25.54 BY BURTECA                      
#    STANDARDS: P  JOBSET: TD075M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CREATION FICHIER POUR ENVOI GATEWAY                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#  DELETE AVANT TRAITEMENT DES FICHIERS NON GDG                                
#         POUR RECRéATION ET ENVOI VERS GFK.                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TD075MA
       ;;
(TD075MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TD075MAA
       ;;
(TD075MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :            **                                       
#    OBLIGATOIRE POUR LOGIQUE APPL    **                                       
# **************************************                                       
# ******* FICHIER                                                              
#    IN1      : NAME=FTD075M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR IN1 /dev/null
# ******  FIC SORTIE DU PKZIP (ENVOI VERS SERVEUR S1I351)                      
#    IN2      : NAME=FTD000M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=TD075MAB
       ;;
(TD075MAB)
       m_CondExec 16,NE,TD075MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP TD075MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=TD075MAD
       ;;
(TD075MAD)
       m_CondExec ${EXAAF},NE,YES 
#  BTD075  : PRODUCTION FICHIER            *                                   
#  REPRISE : OUI                           *                                   
# ***************************************                                      
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  TABLE                                                                
#    RSFR50   : NAME=RSFR50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFR50 /dev/null
# ******  TABLE                                                                
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE                                                                
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  TABLE                                                                
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# ******  TABLE                                                                
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
# ******  TABLE                                                                
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******  TABLE                                                                
#    RSGA59   : NAME=RSGA59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA59 /dev/null
# ******  TABLE                                                                
#    RSGA38   : NAME=RSGA38,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA38 /dev/null
# ******  TABLE                                                                
#    RSTH12   : NAME=RSTH12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSTH12 /dev/null
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ FTD075 ${DATA}/PXX0.F89.FTD075M
# ***********************************                                          
#  FICHIER CATALOGUE POUR LE SERVER S1I351 BIZSERV VERS GATEWAY                
# ***********************************                                          
# AAK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TEXT                                                                       
#  -ARCHIVE(":FTD000M"")                                                       
#  -ARCHUNIT(SEM350)                                                           
#  -ZIPPED_DSN(":FTD075M"",FTD000M.TXT)                                        
#  ":FTD075M""                                                                 
#          DATAEND                                                             
# ***********************************                                          
#  TRANSFERT VERS XFB-GATEWAY                                                  
# ***********************************                                          
# ***********************************                                          
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=FTD075XX,                                                           
#      FNAME=":FTD000M"",                                                      
#      NFNAME=FTD000M.ZIP                                                      
#          DATAEND                                                             
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTD075 
       JUMP_LABEL=TD075MAE
       ;;
(TD075MAE)
       m_CondExec 04,GE,TD075MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN ZIP                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD075MAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TD075MAG
       ;;
(TD075MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/TD075MAG.FTD075AM
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD075MAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TD075MAJ
       ;;
(TD075MAJ)
       m_CondExec ${EXAAP},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR DD1 ${DATA}/PXX0.F89.FTD075M
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 140 -t LSEQ FICZIP ${DATA}/PXX0.F89.FTD000M
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MAJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTTD075M                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TD075MAM PGM=FTP        ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=TD075MAM
       ;;
(TD075MAM)
       m_CondExec ${EXAAU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MAM.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP TD075MAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=TD075MAQ
       ;;
(TD075MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MAQ.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=TD075MZA
       ;;
(TD075MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/TD075MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
