#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GSM14P.ksh                       --- VERSION DU 17/10/2016 18:07
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGSM14 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/14 AT 09.04.16 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM14P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  BSM143                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM14PA
       ;;
(GSM14PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GSM14PAA
       ;;
(GSM14PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES                                                               
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA12 /dev/null
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA26 /dev/null
#    RTGA83   : NAME=RSGA83,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA83 /dev/null
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
#                                                                              
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 FSM143 ${DATA}/PTEM/GSM14PAA.BSM143AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM143 
       JUMP_LABEL=GSM14PAB
       ;;
(GSM14PAB)
       m_CondExec 04,GE,GSM14PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI                                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAD
       ;;
(GSM14PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GSM14PAA.BSM143AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAD.BSM143BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PAE
       ;;
(GSM14PAE)
       m_CondExec 00,EQ,GSM14PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FAY�AL                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAG
       ;;
(GSM14PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/GSM14PAA.BSM143AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAG.BSM143CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_85 8 CH 85
 /FIELDS FLD_CH_96_5 96 CH 5
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_CH_1_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PAH
       ;;
(GSM14PAH)
       m_CondExec 00,EQ,GSM14PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM144                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAJ
       ;;
(GSM14PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES                                                               
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA19   : NAME=RSGA19,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA19 /dev/null
#    RTGA65   : NAME=RSGA65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA65 /dev/null
#    RTGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA69 /dev/null
#    RTGA75   : NAME=RSGA75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA75 /dev/null
#    RTFL05   : NAME=RSFL05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL05 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL50 /dev/null
#    RTFL60   : NAME=RSFL60,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL60 /dev/null
#    RTFL65   : NAME=RSFL65,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL65 /dev/null
#    RTGN59   : NAME=RSGN59,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN59 /dev/null
#    RTGN68   : NAME=RSGN68,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGN68 /dev/null
#    RTHV04   : NAME=RSHV04,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTHV04 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS36 /dev/null
#    RTGF55   : NAME=RSGF55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGF55 /dev/null
#    RTRM58   : NAME=RSRM58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTRM58 /dev/null
#    RTGQ05   : NAME=RSGQ05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGQ05 /dev/null
#                                                                              
# *****   DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A3} FSM143 ${DATA}/PTEM/GSM14PAD.BSM143BP
#                                                                              
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 218 -g +1 FSM144 ${DATA}/PTEM/GSM14PAJ.BSM144AP
# *****   FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM160 ${DATA}/PTEM/GSM14PAJ.BSM144BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM165 ${DATA}/PTEM/GSM14PAJ.BSM144CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM144 
       JUMP_LABEL=GSM14PAK
       ;;
(GSM14PAK)
       m_CondExec 04,GE,GSM14PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FSM144                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAM
       ;;
(GSM14PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/GSM14PAJ.BSM144AP
       m_FileAssign -d NEW,CATLG,DELETE -r 218 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAM.BSM144DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_150_3 150 CH 3
 /FIELDS FLD_CH_101_7 101 CH 7
 /FIELDS FLD_CH_8_85 8 CH 85
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_PD_187_3 187 PD 3
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_CH_150_3 ASCENDING,
   FLD_CH_101_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_187_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PAN
       ;;
(GSM14PAN)
       m_CondExec 00,EQ,GSM14PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FSM144                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAQ
       ;;
(GSM14PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/GSM14PAM.BSM144DP
       m_FileAssign -d NEW,CATLG,DELETE -r 103 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAQ.BSM144EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_150_3 150 CH 3
 /FIELDS FLD_PD_187_3 187 PD 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_8_85 8 CH 85
 /FIELDS FLD_CH_101_7 101 CH 7
 /FIELDS FLD_CH_187_3 187 CH 3
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_CH_150_3 ASCENDING,
   FLD_PD_187_3 DESCENDING,
   FLD_CH_101_7 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_96_5,FLD_CH_8_85,FLD_CH_150_3,FLD_CH_187_3,FLD_CH_101_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GSM14PAR
       ;;
(GSM14PAR)
       m_CondExec 00,EQ,GSM14PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FSM144                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAT
       ;;
(GSM14PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/GSM14PAJ.BSM144AP
       m_FileAssign -d NEW,CATLG,DELETE -r 218 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAT.BSM144FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_101_7 101 CH 7
 /FIELDS FLD_CH_8_85 8 CH 85
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_PD_187_3 187 PD 3
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_CH_101_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_187_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PAU
       ;;
(GSM14PAU)
       m_CondExec 00,EQ,GSM14PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FSM144                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PAX
       ;;
(GSM14PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/GSM14PAT.BSM144FP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/GSM14PAX.BSM144GP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_187_3 187 CH 3
 /FIELDS FLD_PD_187_3 187 PD 3
 /FIELDS FLD_CH_8_85 8 CH 85
 /FIELDS FLD_CH_101_7 101 CH 7
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_PD_187_3 DESCENDING,
   FLD_CH_101_7 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_96_5,FLD_CH_8_85,FLD_CH_187_3,FLD_CH_101_7
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=GSM14PAY
       ;;
(GSM14PAY)
       m_CondExec 00,EQ,GSM14PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI FSM144                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBA
       ;;
(GSM14PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/GSM14PAJ.BSM144AP
       m_FileAssign -d NEW,CATLG,DELETE -r 218 -g +1 SORTOUT ${DATA}/PTEM/GSM14PBA.BSM145AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_196_2 196 CH 2
 /FIELDS FLD_CH_150_3 150 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_85 8 CH 85
 /KEYS
   FLD_CH_96_5 ASCENDING,
   FLD_CH_8_85 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_196_2 ASCENDING,
   FLD_CH_150_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PBB
       ;;
(GSM14PBB)
       m_CondExec 00,EQ,GSM14PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM145                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBD
       ;;
(GSM14PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES                                                               
#    RTGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA58 /dev/null
#    RTFL40   : NAME=RSFL40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTFL40 /dev/null
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#                                                                              
# *****   DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A9} FSM143 ${DATA}/PTEM/GSM14PAG.BSM143CP
       m_FileAssign -d SHR -g ${G_A10} FSM144 ${DATA}/PTEM/GSM14PBA.BSM145AP
       m_FileAssign -d SHR -g ${G_A11} FSMSOC ${DATA}/PTEM/GSM14PAQ.BSM144EP
       m_FileAssign -d SHR -g ${G_A12} FSMGPE ${DATA}/PTEM/GSM14PAX.BSM144GP
#                                                                              
# *****   FICHIER EDITION                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM145 ${DATA}/PTEM/GSM14PBD.BSM145BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM145 
       JUMP_LABEL=GSM14PBE
       ;;
(GSM14PBE)
       m_CondExec 04,GE,GSM14PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI ISM145                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBG
       ;;
(GSM14PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/GSM14PBD.BSM145BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GSM14PBG.BSM145CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_20_85 20 CH 85
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_454_2 454 CH 2
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_PD_105_2 105 PD 2
 /KEYS
   FLD_CH_454_2 ASCENDING,
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_85 ASCENDING,
   FLD_PD_105_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PBH
       ;;
(GSM14PBH)
       m_CondExec 00,EQ,GSM14PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM146                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBJ PGM=BSM146     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBJ
       ;;
(GSM14PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A14} FSM145 ${DATA}/PTEM/GSM14PBG.BSM145CP
#                                                                              
# *****   EDITION ISM145                                                       
       m_OutputAssign -c 9 -w ISM145BR ISM145BR
       m_OutputAssign -c 9 -w ISM145GR ISM145GR
       m_OutputAssign -c 9 -w ISM145BL ISM145BL
       m_OutputAssign -c 9 -w ISM145AC ISM145AC
       m_ProgramExec BSM146 
#                                                                              
# ********************************************************************         
#  TRI ISM160                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBM
       ;;
(GSM14PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/GSM14PAJ.BSM144BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GSM14PBM.BSM160AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_289_3 289 CH 3
 /FIELDS FLD_CH_298_3 298 CH 3
 /FIELDS FLD_CH_310_3 310 CH 3
 /FIELDS FLD_PD_108_2 108 PD 2
 /FIELDS FLD_CH_20_88 20 CH 88
 /FIELDS FLD_CH_301_3 301 CH 3
 /FIELDS FLD_CH_307_3 307 CH 3
 /FIELDS FLD_CH_295_3 295 CH 3
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_CH_304_3 304 CH 3
 /FIELDS FLD_CH_292_3 292 CH 3
 /FIELDS FLD_PD_17_3 17 PD 3
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_88 ASCENDING,
   FLD_PD_108_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_289_3,
    TOTAL FLD_CH_292_3,
    TOTAL FLD_CH_295_3,
    TOTAL FLD_CH_298_3,
    TOTAL FLD_CH_301_3,
    TOTAL FLD_CH_304_3,
    TOTAL FLD_CH_307_3,
    TOTAL FLD_CH_310_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PBN
       ;;
(GSM14PBN)
       m_CondExec 00,EQ,GSM14PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI ISM165                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBQ
       ;;
(GSM14PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/GSM14PAJ.BSM144CP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GSM14PBQ.BSM160BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_279_3 279 CH 3
 /FIELDS FLD_CH_294_3 294 CH 3
 /FIELDS FLD_PD_105_2 105 PD 2
 /FIELDS FLD_CH_288_3 288 CH 3
 /FIELDS FLD_CH_285_3 285 CH 3
 /FIELDS FLD_PD_17_3 17 PD 3
 /FIELDS FLD_CH_297_3 297 CH 3
 /FIELDS FLD_CH_20_85 20 CH 85
 /FIELDS FLD_CH_7_10 7 CH 10
 /FIELDS FLD_CH_282_3 282 CH 3
 /FIELDS FLD_CH_291_3 291 CH 3
 /KEYS
   FLD_CH_7_10 ASCENDING,
   FLD_PD_17_3 ASCENDING,
   FLD_CH_20_85 ASCENDING,
   FLD_PD_105_2 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_279_3,
    TOTAL FLD_CH_282_3,
    TOTAL FLD_CH_285_3,
    TOTAL FLD_CH_288_3,
    TOTAL FLD_CH_291_3,
    TOTAL FLD_CH_294_3,
    TOTAL FLD_CH_297_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PBR
       ;;
(GSM14PBR)
       m_CondExec 00,EQ,GSM14PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSM160                                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBT PGM=BSM160     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBT
       ;;
(GSM14PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A17} FSM160 ${DATA}/PTEM/GSM14PBM.BSM160AP
       m_FileAssign -d SHR -g ${G_A18} FSM165 ${DATA}/PTEM/GSM14PBQ.BSM160BP
#                                                                              
# *****   EDITION ISM160                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM160 ${DATA}/PTEM/GSM14PBT.ISM160AP
# *****   EDITION ISM165                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM165 ${DATA}/PTEM/GSM14PBT.ISM165AP
       m_ProgramExec BSM160 
#                                                                              
# ********************************************************************         
#  PGM : BEG050 ISM160                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PBX
       ;;
(GSM14PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A19} FEXTRAC ${DATA}/PTEM/GSM14PBT.ISM160AP
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GSM14PBX.ISM160BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GSM14PBY
       ;;
(GSM14PBY)
       m_CondExec 04,GE,GSM14PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PCA
       ;;
(GSM14PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/GSM14PBX.ISM160BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GSM14PCA.ISM160CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PCB
       ;;
(GSM14PCB)
       m_CondExec 00,EQ,GSM14PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : ISM160                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PCD
       ;;
(GSM14PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A21} FEXTRAC ${DATA}/PTEM/GSM14PBT.ISM160AP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A22} FCUMULS ${DATA}/PTEM/GSM14PCA.ISM160CP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISM160 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GSM14PCE
       ;;
(GSM14PCE)
       m_CondExec 04,GE,GSM14PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050 ISM165                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PCG
       ;;
(GSM14PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A23} FEXTRAC ${DATA}/PTEM/GSM14PBT.ISM165AP
# ******  FICHIER FCUMULS  RECL=512                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/GSM14PCG.ISM165BP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GSM14PCH
       ;;
(GSM14PCH)
       m_CondExec 04,GE,GSM14PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PCJ
       ;;
(GSM14PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/GSM14PCG.ISM165BP
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/GSM14PCJ.ISM165CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM14PCK
       ;;
(GSM14PCK)
       m_CondExec 00,EQ,GSM14PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : ISM165                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM14PCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PCM
       ;;
(GSM14PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A25} FEXTRAC ${DATA}/PTEM/GSM14PBT.ISM165AP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A26} FCUMULS ${DATA}/PTEM/GSM14PCJ.ISM165CP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w ISM165 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GSM14PCN
       ;;
(GSM14PCN)
       m_CondExec 04,GE,GSM14PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GSM14PZA
       ;;
(GSM14PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GSM14PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
