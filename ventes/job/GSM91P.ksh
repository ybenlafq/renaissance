#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GSM91P.ksh                       --- VERSION DU 17/10/2016 18:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPGSM91 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/09/10 AT 14.21.16 BY BURTECA                      
#    STANDARDS: P  JOBSET: GSM91P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#                                                                              
# ********************************************************************         
#   BSM091 : EXTRACTION DES CODICS                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GSM91PA
       ;;
(GSM91PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GSM91PAA
       ;;
(GSM91PAA)
       m_CondExec ${EXAAA},NE,YES 
# ********************************************                                 
# ********************************************                                 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  LIEUX                                                                
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  FAMILLES                                                             
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ******  ARTICLES : PRA FILIALES                                              
#    RTGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA67 /dev/null
# ******  STOCKS SOUS LIEU ENTREPOT                                            
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
#                                                                              
# *****   EXTRAC                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM091 ${DATA}/PXX0/GSM91PAA.BSM091AP
# *****   EXTRAC POUR EDITION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 ISM092 ${DATA}/PXX0/GSM91PAA.BSM092AP
# ** PARAMETRE SOCIETE                                                         
       m_FileAssign -d SHR FSOCDEP ${DATA}/CORTEX4.P.MTXTFIX1/GSM91PAA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSM091 
       JUMP_LABEL=GSM91PAB
       ;;
(GSM91PAB)
       m_CondExec 04,GE,GSM91PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BSM091AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAD
       ;;
(GSM91PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/GSM91PAA.BSM091AP
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GSM91PAD.BSM091BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_17_7 17 CH 7
 /FIELDS FLD_CH_31_2 31 CH 2
 /KEYS
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_17_7 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM91PAE
       ;;
(GSM91PAE)
       m_CondExec 00,EQ,GSM91PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAG
       ;;
(GSM91PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/GSM91PAD.BSM091BP
# ******  FICHIER FCUMULS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GSM91PAG.BSM091CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GSM91PAH
       ;;
(GSM91PAH)
       m_CondExec 04,GE,GSM91PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAJ
       ;;
(GSM91PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/GSM91PAG.BSM091CP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GSM91PAJ.BSM091DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM91PAK
       ;;
(GSM91PAK)
       m_CondExec 00,EQ,GSM91PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : ISM091                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAM
       ;;
(GSM91PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/GSM91PAD.BSM091BP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/GSM91PAJ.BSM091DP
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w ISM091 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GSM91PAN
       ;;
(GSM91PAN)
       m_CondExec 04,GE,GSM91PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BSM092AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAQ
       ;;
(GSM91PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *****   FICHIER FEXTRAC                                                      
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/GSM91PAA.BSM092AP
# *****   FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GSM91PAQ.BSM092BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_17_7 17 CH 7
 /FIELDS FLD_CH_31_2 31 CH 2
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /KEYS
   FLD_CH_7_5 ASCENDING,
   FLD_CH_12_5 ASCENDING,
   FLD_CH_17_7 ASCENDING,
   FLD_CH_24_7 ASCENDING,
   FLD_CH_31_2 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM91PAR
       ;;
(GSM91PAR)
       m_CondExec 00,EQ,GSM91PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAT
       ;;
(GSM91PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PXX0/GSM91PAQ.BSM092BP
# ******  FICHIER FCUMULS                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/GSM91PAT.BSM092CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=GSM91PAU
       ;;
(GSM91PAU)
       m_CondExec 04,GE,GSM91PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PAX
       ;;
(GSM91PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER FCUMULS ISSU DU BEG050                                       
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/GSM91PAT.BSM092CP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/GSM91PAX.BSM092DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GSM91PAY
       ;;
(GSM91PAY)
       m_CondExec 00,EQ,GSM91PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : ISM092                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GSM91PBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PBA
       ;;
(GSM91PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLE DES SOUS/TABLES                                                
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# ******  TABLE DU GENERATEUR D'ETATS                                          
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# ******  FICHIER FEXTRAC TRIE                                                 
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PXX0/GSM91PAQ.BSM092BP
# ******  FICHIER FCUMULS TRIE                                                 
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PXX0/GSM91PAX.BSM092DP
# ******  PARAMETRE DATE                                                       
       m_FileAssign -i FDATE
$FDATE
_end
# ******  PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******  PARAMETRE  MOIS                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'IMPRESSION                                                 
       m_OutputAssign -c 9 -w ISM092 FEDITION
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=GSM91PBB
       ;;
(GSM91PBB)
       m_CondExec 04,GE,GSM91PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GSM91PZA
       ;;
(GSM91PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GSM91PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
