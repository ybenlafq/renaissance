#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MD40EO.ksh                       --- VERSION DU 17/10/2016 18:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POMD40E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/09 AT 11.12.18 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MD40EO                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ***************************************************************              
#  REPRO DU FICHIER FPARAM POUR SAUVEGARDE                                     
# ***************************************************************              
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=MD40EOA
       ;;
(MD40EOA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=MD40EOAA
       ;;
(MD40EOAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
# ******* DATE DE DERNIER PASSAGE                                              
       m_FileAssign -d SHR -g +0 INPUT ${DATA}/PNCGO/F16.MD401AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUTPUT ${DATA}/PNCGO/F16.MD401BO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD40EOAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD40EOAB
       ;;
(MD40EOAB)
       m_CondExec 16,NE,MD40EOAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD402          EXTRACTION DES LIGNES DE LA RTGS40 _A PARTIR DES             
#                  LIEUX PARAMéTRéS DANS LA SOUS TABLE MD400                   
#  REPRISE OUI                                                                 
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOAD
       ;;
(MD40EOAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS40 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION DU JOUR MVTS RTGS40                             
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 BMD401O ${DATA}/PTEM/MD40EOAD.MD401CO
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGO
# ******  FICHIER DATE EN MISE A JOUR POUR PROCHAIN PASSAGE                    
# ******  CE FICHIER N'EST MIS A JOUR QUE SI LE PGM SE TERMINE OK              
# -M-MD401BO  - IS UPDATED IN PLACE WITH MODE=(U,REST=NO)                      
       m_FileAssign -d OLD,KEEP,DELETE -g ${G_A1} FPARAM ${DATA}/PNCGO/F16.MD401BO
# ******                                                                       
# ******************************************************************           
#  INCREMENTATION ET SAUVEGARDE DU FICHIER PERMANENT                           
#  REPRISE OUI                                                                 
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          

       m_ProgramExec -b BMD402 
       JUMP_LABEL=MD40EOAG
       ;;
(MD40EOAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  FICHIER MENS ENRICHI                                                 
       m_FileAssign -d SHR -g +0 INPUT ${DATA}/PNCGO/F16.MD401DO
# ******  FICHIER D'EXTRACTION DU JOUR MVTS RTGS40                             
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/MD40EOAD.MD401CO
# ******  FICHIER MENS ENRICHI                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUTPUT ${DATA}/PNCGO/F16.MD401DO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD40EOAG.sysin
       m_UtilityExec
# **********************************************************                   
#  CUMUL TOUS MAGASINS CONFONDUS POUR LIEU EXTERNE                             
#  ET CODE OPERATION DONNE                                                     
# **********************************************************                   
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOAJ
       ;;
(MD40EOAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PNCGO/F16.MD401DO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD40EOAJ.MD410AO
# *********************************************************                    
#  BMD420    ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                      
#            EN VUE DE LA CREATION DE L'ETAT JMD4101                           
#  REPRISE OUI                                                                 
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_1_3 01 CH 03
 /FIELDS FLD_BI_38_10 38 CH 10
 /FIELDS FLD_BI_4_3 04 CH 03
 /KEYS
   FLD_BI_27_3 ASCENDING,
   FLD_BI_38_10 ASCENDING,
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOAM
       ;;
(MD40EOAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG51   : NAME=RSGG51O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG51 /dev/null
#    RTGA56   : NAME=RSGA56O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTAN00   : NAME=RSAN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
#    RTGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ****** FDATE DATE DE TRAITEMENT JJMMSSAA                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A4} BMD410I ${DATA}/PTEM/MD40EOAJ.MD410AO
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 BMD410O ${DATA}/PTEM/MD40EOAM.MD410BO
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD420 
       JUMP_LABEL=MD40EOAN
       ;;
(MD40EOAN)
       m_CondExec 04,GE,MD40EOAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOAQ
       ;;
(MD40EOAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/MD40EOAM.MD410BO
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD40EOAQ.MD410CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOAR
       ;;
(MD40EOAR)
       m_CondExec 00,EQ,MD40EOAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# *******************************************************************          
#  CREATION D'UN FICHIER FCUMULS                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOAT
       ;;
(MD40EOAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/MD40EOAQ.MD410CO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/MD40EOAT.MD410DO
#                                                                              
# ******************************************************************           
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ******************************************************************           
#  REPRISE: OUI                                                                
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD40EOAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD40EOAX
       ;;
(MD40EOAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/MD40EOAT.MD410DO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g ${G_A8} SORTOUT ${DATA}/PTEM/MD40EOAM.MD410BO
# ****************************************************************             
#  PGM : BEG060                                                                
# ****************************************************************             
#  CREATION DE L'ETAT : JMD410                                                 
# *****************************************************************            
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOBA
       ;;
(MD40EOBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PTEM/MD40EOAQ.MD410CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PTEM/MD40EOAM.MD410BO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD410 FEDITION
#                                                                              
# **************************************************************               
#  CUMUL DES CODES OPERATION POUR UN LIEU EXTERNE                              
#  ET MAGASIN DONNE                                                            
# *************************************************************                
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD40EOBD
       ;;
(MD40EOBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PNCGO/F16.MD401DO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD40EOBD.MD411AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_38_10 38 CH 10
 /FIELDS FLD_BI_27_3 27 CH 03
 /FIELDS FLD_BI_1_3 01 CH 03
 /FIELDS FLD_BI_4_3 04 CH 03
 /KEYS
   FLD_BI_27_3 ASCENDING,
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_38_10 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOBE
       ;;
(MD40EOBE)
       m_CondExec 00,EQ,MD40EOBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BMD421          ENRICHISSEMENT DU FICHIER ISSU DU PGM BMD401                
#                  EN VUE DE LA CREATION DE L'ETAT JMD411                      
#  REPRISE OUI                                                                 
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBG
       ;;
(MD40EOBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG51   : NAME=RSGG51O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG51 /dev/null
#    RTGA56   : NAME=RSGA56O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA56 /dev/null
#    RTAN00   : NAME=RSAN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTAN00 /dev/null
#    RTGA30   : NAME=RSGA30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  FDATE DATE DE TRAITEMENT JJMMSSAA                                    
       m_FileAssign -i FDATE
$FDATE
_end
# ****** FICHIER EN ENTREE                                                     
       m_FileAssign -d SHR -g ${G_A12} BMD411I ${DATA}/PTEM/MD40EOBD.MD411AO
# ****** FICHIER EN SORTIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 BMD411O ${DATA}/PTEM/MD40EOBG.MD411BO
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BMD421 
       JUMP_LABEL=MD40EOBH
       ;;
(MD40EOBH)
       m_CondExec 04,GE,MD40EOBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC                                                      
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBJ
       ;;
(MD40EOBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/MD40EOBG.MD411BO
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD40EOBJ.MD411CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOBK
       ;;
(MD40EOBK)
       m_CondExec 00,EQ,MD40EOBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# *******************************************************************          
#  CREATION D'UN FICHIER FCUMULS                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBM
       ;;
(MD40EOBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PTEM/MD40EOBJ.MD411CO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/MD40EOBM.MD411DO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=MD40EOBN
       ;;
(MD40EOBN)
       m_CondExec 04,GE,MD40EOBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# *******************************************************************          
#  REPRISE: OUI                                                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBQ
       ;;
(MD40EOBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/MD40EOBM.MD411DO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/MD40EOBQ.MD411EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=MD40EOBR
       ;;
(MD40EOBR)
       m_CondExec 00,EQ,MD40EOBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
# ********************************************************************         
#  CREATION DE L'ETAT : JMD411                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBT
       ;;
(MD40EOBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FEXTRAC ${DATA}/PTEM/MD40EOBJ.MD411CO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A17} FCUMULS ${DATA}/PTEM/MD40EOBQ.MD411EO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w JMD411 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=MD40EOBU
       ;;
(MD40EOBU)
       m_CondExec 04,GE,MD40EOBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECOPIE DE LA DATE POUR PROCHAIN PASSAGE                                    
#  REPRISE OUI                                                                 
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP MD40EOBX PGM=IDCAMS     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOBX
       ;;
(MD40EOBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******  RECOPIE DATE POUR PROCHAIN PASSAGE                                   
       m_FileAssign -d SHR -g ${G_A18} INPUT ${DATA}/PNCGO/F16.MD401BO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUTPUT ${DATA}/PNCGO/F16.MD401AO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD40EOBX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MD40EOBY
       ;;
(MD40EOBY)
       m_CondExec 16,NE,MD40EOBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MD40EOZA
       ;;
(MD40EOZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MD40EOZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
