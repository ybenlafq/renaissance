#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VE900L.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLVE900 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/03/09 AT 10.49.33 BY BURTECA                      
#    STANDARDS: P  JOBSET: VE900L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTVE02-05-06-08-10-11-14                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VE900LA
       ;;
(VE900LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VE900LAA
       ;;
(VE900LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PARG/SEM.VE900LA
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/VE900LAA
       m_ProgramExec IEFBR14 "RDAR,VE900L.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900LAD
       ;;
(VE900LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PARG/SEM.VE900LA
       m_OutputAssign -c "*" SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=VE900LAE
       ;;
(VE900LAE)
       m_CondExec 00,EQ,VE900LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAG
       ;;
(VE900LAG)
       m_CondExec ${EXAAK},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11L  : NAME=RSVE11L,MODE=I - DYNAM=YES                                
#    RSVE14L  : NAME=RSVE14L,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900LAG.VE900L01
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900LAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DE LA TABLE RTVE11                                                   
#  S = S/SYSTEME                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAJ
       ;;
(VE900LAJ)
       m_CondExec ${EXAAP},NE,YES 
#  TABLE DES VENTES DETAIL                                                     
#    RSVE11L  : NAME=RSVE11L,MODE=I - DYNAM=YES                                
#    RSVE14L  : NAME=RSVE14L,MODE=I - DYNAM=YES                                
# **********  FICHIER D UNLOAD                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SYSREC01 ${DATA}/PTEM/VE900LAJ.UNVE11AL
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900LAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FIC SUR NLIEU,NVENTE 4,10,A                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAM
       ;;
(VE900LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/VE900LAG.VE900L01
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 SORTOUT ${DATA}/PTEM/VE900LAM.VE900L02
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_10 4 CH 10
 /KEYS
   FLD_CH_4_10 ASCENDING
 /* Record Type = F  Record Length = 362 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900LAN
       ;;
(VE900LAN)
       m_CondExec 00,EQ,VE900LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9000                                                               
#   PREPARATION FICHIER PURGE ET FICHIER DE LOAD RTVE11                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAQ
       ;;
(VE900LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#         TABLE RTVE11                                                         
#    RSVE11L  : NAME=RSVE11L,MODE=(U,N) - DYNAM=YES                            
# -X-VE900LR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSVE11L /dev/null
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  RTVE11 TRI PAR LIEU ET NVENTE                                               
       m_FileAssign -d SHR -g ${G_A3} VE0011A ${DATA}/PTEM/VE900LAM.VE900L02
#  FICHIER POUR LE LOAD DE LA RTVE11                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011L ${DATA}/PARG/SEM.VE900LB
#  FICHIER DE PURGE DE LA RTVE11                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 362 -g +1 VE0011P ${DATA}/PARG/SEM.VE900LC
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9000 
       JUMP_LABEL=VE900LAR
       ;;
(VE900LAR)
       m_CondExec 04,GE,VE900LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LOAD DE LA TABLE RTVE11                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAT PGM=DSNUTILB   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAT
       ;;
(VE900LAT)
       m_CondExec ${EXABE},NE,YES 
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#    RSVE11L  : NAME=RSVE11L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11L /dev/null
       m_FileAssign -d SHR -g ${G_A4} SYSREC ${DATA}/PARG/SEM.VE900LB
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/VE900LAJ.UNVE11AL
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900LAT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/VE900L_VE900LAT_RTVE11.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=VE900LAU
       ;;
(VE900LAU)
       m_CondExec 04,GE,VE900LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE910                                                                
#  EPURATION DES TABLES RTVE10 RTVE11                                          
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VE900LAX
       ;;
(VE900LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE DES ENTETES DE VENTES                                                 
#    RSVE10L  : NAME=RSVE10L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE10L /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RSVE11L  : NAME=RSVE11L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE11L /dev/null
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 FVE910 ${DATA}/PARG/SEM.VE900LD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE910 
       JUMP_LABEL=VE900LAY
       ;;
(VE900LAY)
       m_CondExec 04,GE,VE900LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC DE PURGE POUR LE PASSER A UN LRECL = 20                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBA
       ;;
(VE900LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PARG/SEM.VE900LC
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PTEM/VE900LBA.VE900L03
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_PD_52_3 52 PD 3
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_52_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900LBB
       ;;
(VE900LBB)
       m_CondExec 00,EQ,VE900LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ISSU DU PGM BVE910 ET DU TRI PRECEDENT POUR CREER            
#  LE FICHIER VE900LE                                                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBD
       ;;
(VE900LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/VE900LBA.VE900L03
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PARG/SEM.VE900LD
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PARG/SEM.VE900LE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VE900LBE
       ;;
(VE900LBE)
       m_CondExec 00,EQ,VE900LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9002                                                               
#  PURGE DE LA TABLE RTVE02 TABLE DES ADRESSES                                 
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBG
       ;;
(VE900LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENTES ADRESSES                                                   
#    RSVE02L  : NAME=RSVE02L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE02L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBG
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A9} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 467 -g +1 FVE020 ${DATA}/PARG/SEM.VE900LF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9002 
       JUMP_LABEL=VE900LBH
       ;;
(VE900LBH)
       m_CondExec 04,GE,VE900LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9005                                                               
#  PURGE DE LA TABLE RTVE05                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBJ
       ;;
(VE900LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE05L  : NAME=RSVE05L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE05L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBJ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A10} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 FVE005 ${DATA}/PARG/SEM.VE900LG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9005 
       JUMP_LABEL=VE900LBK
       ;;
(VE900LBK)
       m_CondExec 04,GE,VE900LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9006                                                               
#  PURGE DE LA TABLE RTVE06                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBM
       ;;
(VE900LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE06L  : NAME=RSVE06L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE06L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBM
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A11} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 FVE006 ${DATA}/PARG/SEM.VE900LH
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9006 
       JUMP_LABEL=VE900LBN
       ;;
(VE900LBN)
       m_CondExec 04,GE,VE900LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9008                                                               
#  PURGE DE LA TABLE RTVE08                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBQ
       ;;
(VE900LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES PRESTATIONS                                                       
#    RSVE08L  : NAME=RSVE08L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE08L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBQ
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A12} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 99 -g +1 FVE008 ${DATA}/PARG/SEM.VE900LI
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9008 
       JUMP_LABEL=VE900LBR
       ;;
(VE900LBR)
       m_CondExec 04,GE,VE900LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9014                                                               
#  PURGE DE LA TABLE DES REGLEMENTS                                            
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBT
       ;;
(VE900LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES REGLEMENTS                                                        
#    RSVE14L  : NAME=RSVE14L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE14L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBT
#  FICHIER POUR LES PURGES DES TABLES DE VENTES                                
       m_FileAssign -d SHR -g ${G_A13} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER POUR L'ARCHIVAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 131 -g +1 FVE140 ${DATA}/PARG/SEM.VE900LJ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9014 
       JUMP_LABEL=VE900LBU
       ;;
(VE900LBU)
       m_CondExec 04,GE,VE900LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVE9010                                                               
#  PURGE DE LA TABLE RTVE10                                                    
#  REPRISE: OUI EN CAS D ABEND                                                 
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VE900LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=VE900LBX
       ;;
(VE900LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIGNES DE VENTES SPECIFIQUES                                      
#    RSVE10L  : NAME=RSVE10L,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSVE10L /dev/null
       m_FileAssign -d SHR FSIMU ${DATA}/CORTEX4.P.MTXTFIX1/VE900LBX
#  FICHIER                                                                     
       m_FileAssign -d SHR -g ${G_A14} FVE900 ${DATA}/PARG/SEM.VE900LE
#  FICHIER                                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 33 -g +1 FVE100 ${DATA}/PARG/SEM.VE900LK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVE9010 
       JUMP_LABEL=VE900LBY
       ;;
(VE900LBY)
       m_CondExec 04,GE,VE900LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VE900LZA
       ;;
(VE900LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VE900LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
