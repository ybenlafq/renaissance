#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV400Y.ksh                       --- VERSION DU 08/10/2016 13:37
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYGV400 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/08/03 AT 10.22.58 BY BURTECA                      
#    STANDARDS: P  JOBSET: GV400Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='VENTES'                                                            
# ********************************************************************         
#   TRI DU FICHIER SUR CODIC,SOC ORIG,LIEU ORIG,SOC DEST,LIEU DEST,DAT         
#   OPER,CODE INSEE,DATE CREAT,DATE STAT                                       
#   INCLUDE SUR LIEU ORIGINE ET LIEU DESTINATION                               
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GV400YA
       ;;
(GV400YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=GV400YAA
       ;;
(GV400YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BHV030AY
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PXX0/GV400YAA.FGS400AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_14 "VEN"
 /DERIVEDFIELD CST_3_18 "VEN"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_122_6 122 CH 6
 /FIELDS FLD_PD_99_5 99 PD 5
 /FIELDS FLD_PD_78_3 78 PD 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_104_8 104 CH 8
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_88_5 88 CH 5
 /CONDITION CND_2 FLD_CH_4_3 EQ CST_1_14 OR FLD_CH_13_3 EQ CST_3_18 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_10_3 ASCENDING,
   FLD_CH_13_3 ASCENDING,
   FLD_CH_70_8 ASCENDING,
   FLD_CH_88_5 ASCENDING,
   FLD_CH_104_8 ASCENDING,
   FLD_CH_122_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_78_3,
    TOTAL FLD_PD_99_5
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400YAB
       ;;
(GV400YAB)
       m_CondExec 00,EQ,GV400YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV400 : EXTRACTION DES INFOS NECESSAIRES A L'EDITION DES VENTES            
#                          PAR ZONES DE SAV                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400YAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=GV400YAD
       ;;
(GV400YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE ARTICLES                                                       
#    RTGA00R  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00R /dev/null
# ******* TABLE GENERALISEE                                                    
#    RTGA01R  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01R /dev/null
# ******* TABLE DES FAMILLES                                                   
#    RTGA14R  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14R /dev/null
# ******* TABLE DES PARAMETRES LIES AUX FAMILLES                               
#    RTGA30R  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30R /dev/null
# ******* TABLE DE ARTICLES PAR SECTEUR DE SAV CODE DEPT AFFEC CODE ZO         
#    RTGA43R  : NAME=RSGA43Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA43R /dev/null
# ******* FIC ISSU DU TRI                                                      
       m_FileAssign -d SHR -g ${G_A1} FGS400 ${DATA}/PXX0/GV400YAA.FGS400AY
# ******* FIC D'EXTRACT DES INFOS PAR ZONES SAV                                
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -g +1 FGV400 ${DATA}/PXX0/GV400YAD.BGV400AY
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  PARAMETRE SOCIETE : 945                                              
       m_FileAssign -d SHR FSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV400 
       JUMP_LABEL=GV400YAE
       ;;
(GV400YAE)
       m_CondExec 04,GE,GV400YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES INFOS D'EDITION DES VENTES                               
#  SECTEUR: 1,5,A   ZONE-SAV: 6,5,A    AGREGAT: 54,2   N�SEQ: 51,3,A           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400YAG PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=GV400YAG
       ;;
(GV400YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/GV400YAD.BGV400AY
       m_FileAssign -d NEW,CATLG,DELETE -r 66 -g +1 SORTOUT ${DATA}/PXX0/GV400YAG.BGV410AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_20 11 CH 20
 /FIELDS FLD_PD_56_5 56 PD 5
 /FIELDS FLD_CH_51_3 51 CH 3
 /FIELDS FLD_PD_61_6 61 PD 6
 /FIELDS FLD_CH_1_5 1 CH 5
 /FIELDS FLD_CH_54_2 54 CH 2
 /KEYS
   FLD_CH_1_5 ASCENDING,
   FLD_CH_11_20 ASCENDING,
   FLD_CH_54_2 ASCENDING,
   FLD_CH_51_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_56_5,
    TOTAL FLD_PD_61_6
 /* Record Type = F  Record Length = 66 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV400YAH
       ;;
(GV400YAH)
       m_CondExec 00,EQ,GV400YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BGV410 : EDITION DES VENTES PAR SAV A PARTIR DU FICHIER D'EXTRACTS          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GV400YAJ PGM=BGV410     **                                          
# ***********************************                                          
       JUMP_LABEL=GV400YAJ
       ;;
(GV400YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******* FIC D'EXTRACT DES INFOS TRI�                                         
       m_FileAssign -d SHR -g ${G_A3} FGV410 ${DATA}/PXX0/GV400YAG.BGV410AY
#                                                                              
# ******* FMOIS MMSSAA                                                         
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# ******* EDITION DES VENTES PAR ZONE DE SAV                                   
       m_OutputAssign -c 9 -w IGV410 IGV410
       m_ProgramExec BGV410 
#                                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=GV400YZA
       ;;
(GV400YZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV400YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
