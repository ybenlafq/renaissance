#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PV100L.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLPV100 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/24 AT 16.18.53 BY BURTEC2                      
#    STANDARDS: P  JOBSET: PV100L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM BPV108  : EXTRACTION DES VENTES DU MOIS COURANT SUR HISTO               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PV100LA
       ;;
(PV100LA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=PV100LAA
       ;;
(PV100LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ********************************************                                 
#  DEPENDANCES POUR PLAN :                   *                                 
#    OBLIGATOIRE POUR LOGIQUE APPL           *                                 
#    POUR AVOIR LES STOCKS LE PLUS A JOUR    *                                 
#    POSSIBLE                                *                                 
# ********************************************                                 
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F61.PV100HL.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV108M ${DATA}/PTEM/PV100LAA.BPV108EL
       m_ProgramExec BPV108 
# ********************************************************************         
#  TRI PREPARATOIRE POUR FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE          
#  POUR PGM BPV102                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAD
       ;;
(PV100LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/PV100LAA.BPV108EL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100LAD.BPV108ML
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_59_7 59 CH 7
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_24_8 24 CH 8
 /FIELDS FLD_CH_49_5 49 CH 5
 /KEYS
   FLD_CH_49_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_24_8 ASCENDING,
   FLD_CH_59_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100LAE
       ;;
(PV100LAE)
       m_CondExec 00,EQ,PV100LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV102  CALCUL DES NIVEAUX D AGREGATION                            
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAG
       ;;
(PV100LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES ARTICLES                                                          
#    RTGA00L  : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00L /dev/null
#  TABLE DES IMBRICATIONS DES  CODES MARKETING                                 
#    RTGA09L  : NAME=RSGA09L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA09L /dev/null
#  TABLE DES LIGNES DE VENTES                                                  
#    RTGA11L  : NAME=RSGA11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11L /dev/null
#  TABLE DES EDITIONS                                                          
#    RTGA12L  : NAME=RSGA12L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA12L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA14L  : NAME=RSGA14L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTGA20L  : NAME=RSGA20L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA20L /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA25L  : NAME=RSGA25L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA25L /dev/null
#  TABLE DES LIBELLES RAYONS                                                   
#    RTGA29L  : NAME=RSGA29L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA29L /dev/null
#  TABLE CODES DESCRIPTIFS CODIC                                               
#    RTGA53L  : NAME=RSGA53L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA53L /dev/null
#  TABLE HISTORIQUE PRMP                                                       
#    RTGG50L  : NAME=RSGG50L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50L /dev/null
#  TABLE HISTORIQUE PRMP DACEM                                                 
#    RTGG55L  : NAME=RSGG55L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55L /dev/null
#  TABLE DES MVTS  DE STOCK                                                    
#    RTGS40L  : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40L /dev/null
#  TABLE DES VENDEURS                                                          
#    RTGV31L  : NAME=RSGV31L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31L /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -d SHR -g ${G_A2} FPV100 ${DATA}/PTEM/PV100LAD.BPV108ML
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00L /dev/null
#  SORTIE FICHIER PRIMES VENDEURS AGREGES PAR RUBRIQUE                         
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV102 ${DATA}/PTEM/PV100LAG.BPV102AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV102 
       JUMP_LABEL=PV100LAH
       ;;
(PV100LAH)
       m_CondExec 04,GE,PV100LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV103 MAJ DE LA TABLE HV32                   
#  POUR PGM BPV103                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAJ
       ;;
(PV100LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/PV100LAG.BPV102AL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100LAJ.BPV103AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100LAK
       ;;
(PV100LAK)
       m_CondExec 00,EQ,PV100LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV103  MAJ DE  LA TABLE HV32                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAM
       ;;
(PV100LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTGV31L  : NAME=RSGV31L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31L /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A4} FPV103 ${DATA}/PTEM/PV100LAJ.BPV103AL
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  MAJ DE LA TABLE RTHV32                                                      
#    RSHV32L  : NAME=RSHV32L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV32L /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00L /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV103 
       JUMP_LABEL=PV100LAN
       ;;
(PV100LAN)
       m_CondExec 04,GE,PV100LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV104                                        
#  POUR PGM BPV104                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAQ
       ;;
(PV100LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/PV100LAA.BPV108EL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100LAQ.BPV104AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /DERIVEDFIELD CST_3_21 "     "
 /FIELDS FLD_CH_42_7 42 CH 7
 /FIELDS FLD_CH_96_5 96 CH 5
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_171_5 171 CH 5
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_87_4 87 CH 4
 /CONDITION CND_2 FLD_CH_67_1 EQ CST_1_17 OR FLD_CH_171_5 EQ CST_3_21 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_171_5 ASCENDING,
   FLD_CH_42_7 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100LAR
       ;;
(PV100LAR)
       m_CondExec 00,EQ,PV100LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV104  MAJ DE  LA TABLE HV26                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAT
       ;;
(PV100LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE HISTORIQUE DES VENTES DE PSE                                          
#    RTGV26L  : NAME=RSGV26L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV26L /dev/null
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A6} FPV104 ${DATA}/PTEM/PV100LAQ.BPV104AL
#  MAJ DE LA TABLE RTHV26 HISTORIQUE VENTE PSE                                 
#    RSHV26L  : NAME=RSHV26L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV26L /dev/null
#  SORTIE TABLES ANOMALIES                                                     
#    RTAN00L  : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTAN00L /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV104 
       JUMP_LABEL=PV100LAU
       ;;
(PV100LAU)
       m_CondExec 04,GE,PV100LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI PREPARATOIRE POUR FICHIER FPV107 MAJ DE LA TABLE HV33                   
#  POUR PGM BPV107                                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=PV100LAX
       ;;
(PV100LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/PV100LAG.BPV102AL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/PV100LAX.BPV107AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_17 "G"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_5 19 CH 5
 /FIELDS FLD_CH_76_4 76 CH 4
 /FIELDS FLD_CH_13_6 13 CH 6
 /FIELDS FLD_CH_80_4 80 CH 4
 /FIELDS FLD_CH_66_1 66 CH 1
 /FIELDS FLD_CH_68_4 68 CH 4
 /FIELDS FLD_CH_84_3 84 CH 3
 /FIELDS FLD_CH_91_5 91 CH 5
 /FIELDS FLD_CH_87_4 87 CH 4
 /FIELDS FLD_CH_7_6 7 CH 6
 /FIELDS FLD_CH_67_1 67 CH 1
 /FIELDS FLD_CH_72_4 72 CH 4
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_96_5 96 CH 5
 /CONDITION CND_2 FLD_CH_67_1 NE CST_1_17 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_6 ASCENDING,
   FLD_CH_13_6 ASCENDING,
   FLD_CH_19_5 ASCENDING,
   FLD_CH_66_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_68_4,
    TOTAL FLD_CH_72_4,
    TOTAL FLD_CH_76_4,
    TOTAL FLD_CH_80_4,
    TOTAL FLD_CH_84_3,
    TOTAL FLD_CH_87_4,
    TOTAL FLD_CH_91_5,
    TOTAL FLD_CH_96_5
 /OMIT CND_2
 /* Record Type = F  Record Length = 250 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=PV100LAY
       ;;
(PV100LAY)
       m_CondExec 00,EQ,PV100LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV107  MAJ DE  LA TABLE HV33                                      
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBA
       ;;
(PV100LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  TABLE DES VENDEURS                                                          
#    RTGV31L  : NAME=RSGV31L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31L /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A8} FPV107 ${DATA}/PTEM/PV100LAX.BPV107AL
#  MAJ DE LA TABLE RTHV32 HISTORIQUE VENTES                                    
#    RSHV33L  : NAME=RSHV33L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV33L /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV107 
       JUMP_LABEL=PV100LBB
       ;;
(PV100LBB)
       m_CondExec 04,GE,PV100LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV110  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBD
       ;;
(PV100LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
#  TABLE GENERALITE SOCIETE                                                    
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE DES LIEUX                                                             
#    RTGV31L  : NAME=RSGV31L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# EDITION DE L ETAT IPV110 REMPLACE PAR L'EDITION DE L'ETAPE SUIVANTE          
       m_OutputAssign -c 9 -w IPV110M IMPRIM
# MPRIM   REPORT SYSOUT=*                                                      
#  FICHIER FORMATTER A DESTINATION DE L EDITION GENERALISE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -t LSEQ -g +1 FEG110 ${DATA}/PXX0/F61.BPV110AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV110 
       JUMP_LABEL=PV100LBE
       ;;
(PV100LBE)
       m_CondExec 04,GE,PV100LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IPV110 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABY      SORT                                                                
# SORTIN   FILE  NAME=BPV110AL,MODE=I                                          
# SORTOUT  REPORT SYSOUT=(9,IPV110M),RECFM=FBA                                 
# SYSIN    DATA  *                                                             
#  MERGE FIELDS=COPY                                                           
#  RECORD TYPE=F,LENGTH=(133)                                                  
#  OUTREC FIELDS=(24,133)                                                      
#          DATAEND                                                             
# ********************************************************************         
#   PROG    BPV111  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBG
       ;;
(PV100LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
#  TABLE                                                                       
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV111                                                    
       m_OutputAssign -c 9 -w IPV111 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV111 
       JUMP_LABEL=PV100LBH
       ;;
(PV100LBH)
       m_CondExec 04,GE,PV100LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV116  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBJ
       ;;
(PV100LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
#  TABLE                                                                       
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE                                                                       
#    RTGA11L  : NAME=RSGA11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA11L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV116                                                    
       m_OutputAssign -c 9 -w IPV116 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV116 
       JUMP_LABEL=PV100LBK
       ;;
(PV100LBK)
       m_CondExec 04,GE,PV100LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BPV105  : EPURATION DU FICHIER HISTORIQUE -2 MOIS                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBM PGM=BPV105     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBM
       ;;
(PV100LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  ENTREE                                                                      
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR -g +0 FPV100H ${DATA}/PEX0/F61.PV100HL.HISTO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 FPV100HN ${DATA}/PEX0/F61.PV100HL.HISTO
       m_ProgramExec BPV105 
# ********************************************************************         
#  PGM BPV113  : PGM D EPURATION DE HV32 + HV33                                
#  ACCES A RTGA01 POUR DELAI DANS LES 2 TABLES CONCERNES ET DELETE POU         
#  LES ENREGISTREMENTS SUPERIEUR A 13 MOIS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBQ
       ;;
(PV100LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES VENDEURS                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#  TABLE DES VENDEURS                                                          
#    RTHV33L  : NAME=RSHV33L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV33L /dev/null
#  TABLE GENERALISEE                                                           
#    RTGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01L /dev/null
#                                                                              
       m_FileAssign -i FMOIS
$FMOIS
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV113 
       JUMP_LABEL=PV100LBR
       ;;
(PV100LBR)
       m_CondExec 04,GE,PV100LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BPV120                                                                
#  ETAT DE PREPARATION DE PAIE                                                 
#  REPRISE: OUI SI FIN ANORMALE                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBT
       ;;
(PV100LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******************************* TABLE GENERALISEE                            
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
# ******************************* TABLE GENERALITE SOCIETE                     
#    RTGA10L  : NAME=RSGA11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
# ******************************* TABLE DES LIEUX                              
#    RTGV31L  : NAME=RSGV31L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV31L /dev/null
# ******************************* TABLE DES FAMILLES                           
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
# ******************************* FICHIER DUMMY POUR PREP PAYE                 
       m_FileAssign -d SHR FPP000 /dev/null
# ******************************* DATE ENTREE                                  
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
# ** EDITION DE L ETAT IPV120                                                  
       m_OutputAssign -c 9 -w IPV120 IPV120
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV120 
       JUMP_LABEL=PV100LBU
       ;;
(PV100LBU)
       m_CondExec 04,GE,PV100LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BPV117  ETAT DE PREPARATION DE PAIE                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP PV100LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=PV100LBX
       ;;
(PV100LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01L  : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01L /dev/null
#  TABLE                                                                       
#    RTGA10L  : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10L /dev/null
#  TABLE DES FAMILLES                                                          
#    RTHV32L  : NAME=RSHV32L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTHV32L /dev/null
#                                                                              
#  ENTREE                                                                      
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FMOIS
$VDATEJ__MMANN
_end
#  EDITION DE L ETAT IPV117                                                    
       m_OutputAssign -c 9 -w IPV117 IMPRIM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPV117 
       JUMP_LABEL=PV100LBY
       ;;
(PV100LBY)
       m_CondExec 04,GE,PV100LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=PV100LZA
       ;;
(PV100LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/PV100LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
