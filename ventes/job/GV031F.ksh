#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GV031F.ksh                       --- VERSION DU 09/10/2016 05:52
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFGV031 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/04 AT 13.55.47 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GV031F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DE LA TABLE RTGV31 DE DRA                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GV031FA
       ;;
(GV031FA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/03/04 AT 13.55.47 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: GV031F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CODE VENDEUR'                          
# *                           APPL...: REPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GV031FAA
       ;;
(GV031FAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
#  DEPENDANCES POUR PLAN :                                                     
#  ---------------------                                                       
# *************************************                                        
#    RTGV31Y  : NAME=RSGV31Y,MODE=I - DYNAM=YES                                
#    RTGV31D  : NAME=RSGV31D,MODE=I - DYNAM=YES                                
#    RTGV31M  : NAME=RSGV31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/GV031FAA.UNGV31Y
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/GV031FAA.UNGV31D
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC03 ${DATA}/PTEM/GV031FAA.UNGV31M
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV031FAA.sysin
       
       m_DBHpuUnload -f SYSIN
# *******************************************************************          
#  MERGE DES FICHIERS D'UNLOAD                                                 
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP GV031FAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GV031FAD
       ;;
(GV031FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/GV031FAA.UNGV31Y
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/GV031FAA.UNGV31D
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/GV031FAA.UNGV31M
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.UNGV31F
# *********************************************************                    
#  ENVOI FTP SUR LA GATEWAY DU FICHIER UNGV31F                                 
#  REPRISE : OUI                                                               
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=GV031FAE
       ;;
(GV031FAE)
       m_CondExec 00,EQ,GV031FAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP GV031FAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=GV031FAG
       ;;
(GV031FAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/GV031FAG.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=GV031FZA
       ;;
(GV031FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GV031FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
