#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HV015M.ksh                       --- VERSION DU 08/10/2016 12:48
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMHV015 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/03/20 AT 10.15.52 BY BURTECB                      
#    STANDARDS: P  JOBSET: HV015M                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  QUIESCE DE TABLESPACES RSHV20M RSHV25M AVANT MAJ                            
#  SI PROBLEME DS LA CHAINE PRENDRE LA VALEUR DU RBA DE CE STEP                
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HV015MA
       ;;
(HV015MA)
#
#HV015MAA
#HV015MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#HV015MAA
#
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       EXAAP=${EXAAP:-0}
       G_A2=${G_A2:-'+1'}
       EXAAU=${EXAAU:-0}
       G_A3=${G_A3:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=HV015MAD
       ;;
(HV015MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGV0/F89.BHV000AM
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PXX0/HV015MAD.BHV015AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_34_5 34 CH 5
 /FIELDS FLD_CH_48_5 48 CH 5
 /FIELDS FLD_CH_17_8 17 CH 8
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_48_5 ASCENDING,
   FLD_CH_1_13 ASCENDING,
   FLD_CH_17_8 ASCENDING,
   FLD_CH_34_5 ASCENDING
 /* Record Type = F  Record Length = 52 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015MAE
       ;;
(HV015MAE)
       m_CondExec 00,EQ,HV015MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV015  : GENERATION DU FICHIER VENTES PAR MAGASINS,GROUPE                  
#            DE STATISTIQUE,COMMUNE POUR UN MOIS DE VENTE                      
#  REPRISE :OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015MAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015MAG
       ;;
(HV015MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES (GENERALITES ARTICLES)                                      
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  FAMILLE (EDITION DES ETATS)                                          
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******  RAYONS (COMPOSANTS DU RAYON)                                         
#    RSGA21M  : NAME=RSGA21M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA21M /dev/null
# ******  QUOTAS DE LIVRAISON (POPULATION MARCHE/DONNEES DE BASE INSEE         
#    RSGQ01M  : NAME=RSGQ01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGQ01M /dev/null
#                                                                              
# ******  FIC DES VENTES DU MOIS TRI�                                          
       m_FileAssign -d SHR -g ${G_A1} FHV000 ${DATA}/PXX0/HV015MAD.BHV015AM
# ******  FIC DES STATS PAR MAGS/GRPE DE STAT/COMMUNE                          
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 FHV015 ${DATA}/PXX0/HV015MAG.BHV015BM
#                                                                              
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV015 
       JUMP_LABEL=HV015MAH
       ;;
(HV015MAH)
       m_CondExec 04,GE,HV015MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER SEQUENTIEL BHV015BM                                          
#  CODE RAMON  1,11 ; CODE COMMUNE  32,5                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015MAJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=HV015MAJ
       ;;
(HV015MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/HV015MAG.BHV015BM
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -g +1 SORTOUT ${DATA}/PXX0/HV015MAJ.BHV015CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_25_7 25 PD 7
 /FIELDS FLD_CH_32_5 32 CH 05
 /FIELDS FLD_PD_20_5 20 PD 5
 /FIELDS FLD_CH_1_11 1 CH 11
 /KEYS
   FLD_CH_1_11 ASCENDING,
   FLD_CH_32_5 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_20_5,
    TOTAL FLD_PD_25_7
 /* Record Type = F  Record Length = 50 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HV015MAK
       ;;
(HV015MAK)
       m_CondExec 00,EQ,HV015MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHV020 : MISE A JOUR DES TABLES HISTORIQUES DES VENTES PAR MOIS             
#           ET HISTORIQUE DES VENTES PAR GROUPES DE MOIS                       
#  REPRISE: OUI SI FIN ANORMALE                                                
#           NON SI FIN NORMALE.FAIRE UN RECOVER DE RSHV20M RSHV25M AVE         
#           LA VALEUR DU RBA DU PREMIER STEP.UTILISEZ LA CHAINE DB2RBM         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HV015MAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=HV015MAM
       ;;
(HV015MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  FIC DES STATS PAR MAGS/GROUPE DE STATS/COMMUNE                       
       m_FileAssign -d SHR -g ${G_A3} FHV020 ${DATA}/PXX0/HV015MAJ.BHV015CM
#                                                                              
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/MOIS)                              
#    RSHV20M  : NAME=RSHV20M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV20M /dev/null
# ******  HISTO VENTES (MAGAS/RAYON/COMMUNE/GROUPE MOIS)                       
#    RSHV25M  : NAME=RSHV25M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSHV25M /dev/null
#                                                                              
# ******  DATE DU DERNIER JOUR DU MOIS A TRAITER                               
       m_FileAssign -i FDATE
$FMOISJ
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV020 
       JUMP_LABEL=HV015MAN
       ;;
(HV015MAN)
       m_CondExec 04,GE,HV015MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=HV015MZA
       ;;
(HV015MZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HV015MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
