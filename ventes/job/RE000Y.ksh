#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RE000Y.ksh                       --- VERSION DU 17/10/2016 18:38
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYRE000 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 11/08/26 AT 10.52.39 BY BURTECA                      
#    STANDARDS: P  JOBSET: RE000Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#   REPRISE: NON BACKOUT JOBSET                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=RE000YA
       ;;
(RE000YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=RE000YAA
       ;;
(RE000YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BGS105AY
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -g +1 SORTOUT ${DATA}/PTEM/RE000YAA.BRE200AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_81_9 81 CH 9
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING,
   FLD_CH_81_9 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YAB
       ;;
(RE000YAB)
       m_CondExec 00,EQ,RE000YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BRE200   EXTRACTION DES VENTES DU FICHIER CAISSES 36 VENTE-EMPORTEE         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAD PGM=BRE200     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAD
       ;;
(RE000YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  PARAMETRE PGM: SI 'N' TRAITEMENT NCG                                 
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/RE000YAD
#                                                                              
# ******  FICHIER VENTES EMPORTEES TRI�                                        
       m_FileAssign -d SHR -g ${G_A1} FEMPORT ${DATA}/PTEM/RE000YAA.BRE200AY
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000YAD.BRE200BY
       m_ProgramExec BRE200 
# ********************************************************************         
#  BRE205   EXTRACTION DES VENTES DU FICHIER CAISSES AS400 NEM                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAG PGM=BRE205     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAG
       ;;
(RE000YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******  FICHIER VENTES AS400 LG 107 ISSU DE NM009Y                           
       m_FileAssign -d SHR -g +0 FTS057 ${DATA}/PXX0/F45.BNM157AY
# ******  FICHIER SEQUENTIEL D EXTRACTION S/36                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FRE200 ${DATA}/PTEM/RE000YAG.BRE205AY
       m_ProgramExec BRE205 
# ********************************************************************         
#   SORT DU FICHIER CAISSE                                                     
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAJ
       ;;
(RE000YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/RE000YAD.BRE200BY
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/RE000YAG.BRE205AY
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/RE000YAJ.BRE100AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_21_6 21 CH 6
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_7_3 7 CH 3
 /FIELDS FLD_CH_10_4 10 CH 4
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_3 ASCENDING,
   FLD_CH_10_4 ASCENDING,
   FLD_CH_21_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YAK
       ;;
(RE000YAK)
       m_CondExec 00,EQ,RE000YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROG    BRE001 : PROG D EXTRACTION (DEPENDANCE AVEC BGV135=>DSTAT)         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAM
       ;;
(RE000YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE FAMILLES                                                              
#    RTGA14   : NAME=RSGA14Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA14 /dev/null
#  TABLE ARTICLES                                                              
#    RTGA00   : NAME=RSGA00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#  TABLE                                                                       
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE                                                                       
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES VENTES                                                            
#    RTGV11   : NAME=RSGV11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV11 /dev/null
#  TABLE DES VENTES : ADRESSES                                                 
#    RTGV02   : NAME=RSGV02Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGV02 /dev/null
#  TABLE DES GAMMES : PRIX EXCEPTIONNELS                                       
#    RTGG20   : NAME=RSGG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG20 /dev/null
#                                                                              
#  ENTREE: FICHIER SOCIETE                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#  ENTREE: TRI DU FICHIER PRECEDEND                                            
       m_FileAssign -d SHR -g ${G_A4} FRE000 ${DATA}/PTEM/RE000YAJ.BRE100AY
#  FICHIER EN ENTREE : FDATE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
#  FICHIER EN SORTIE : FEXTRAC DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FEXTRAC ${DATA}/PTEM/RE000YAM.BRE001AY
#  FICHIER EN SORTIE : FRE002  DE 512 DE LONG MENSUEL                          
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE002 ${DATA}/PTEM/RE000YAM.BRE002JY
#  FICHIER EN SORTIE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FRE003 ${DATA}/PTEM/RE000YAM.BRE003JY
#  FICHIER EN SORTIE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 RTRE00 ${DATA}/PTEM/RE000YAM.BRE010JY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BRE001 
       JUMP_LABEL=RE000YAN
       ;;
(RE000YAN)
       m_CondExec 04,GE,RE000YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (BRE001AP)                                           
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAQ
       ;;
(RE000YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/RE000YAM.BRE001AY
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000YAQ.BRE001BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_10_3 10 PD 3
 /FIELDS FLD_BI_13_5 13 CH 5
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_PD_10_3 ASCENDING,
   FLD_BI_13_5 ASCENDING,
   FLD_PD_18_3 DESCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YAR
       ;;
(RE000YAR)
       m_CondExec 00,EQ,RE000YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS BRE001CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAT
       ;;
(RE000YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A6} FEXTRAC ${DATA}/PTEM/RE000YAQ.BRE001BY
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PTEM/RE000YAT.BRE001CY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=RE000YAU
       ;;
(RE000YAU)
       m_CondExec 04,GE,RE000YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=RE000YAX
       ;;
(RE000YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/RE000YAT.BRE001CY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PTEM/RE000YAX.FRE001AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YAY
       ;;
(RE000YAY)
       m_CondExec 00,EQ,RE000YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IRE001                                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBA
       ;;
(RE000YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A8} FEXTRAC ${DATA}/PTEM/RE000YAQ.BRE001BY
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FCUMULS ${DATA}/PTEM/RE000YAX.FRE001AY
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d NEW,CATLG,DELETE -r 156 -g +1 FEG132 ${DATA}/PXX0/F45.BRE065AY
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c Z FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=RE000YBB
       ;;
(RE000YBB)
       m_CondExec 04,GE,RE000YBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EDITION IRE001 POUR EOS                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBD
       ;;
(RE000YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/F45.BRE065AY
       m_OutputAssign -c 9 -w IRE001 SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_24_133 24 CH 133
 /COPY
 /* Record Type = F  Record Length = (133) */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_24_133
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=RE000YBE
       ;;
(RE000YBE)
       m_CondExec 00,EQ,RE000YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL DU FRE002                                 
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBG
       ;;
(RE000YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE002  DE 512 DE LONG (JOURNALIER)                     
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/RE000YAM.BRE002JY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BRE002MY.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F45.BRE002MY.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_7_2 7 CH 2
 /FIELDS FLD_PD_12_3 12 PD 3
 /FIELDS FLD_BI_9_3 9 CH 3
 /FIELDS FLD_BI_15_5 15 CH 5
 /KEYS
   FLD_BI_7_2 ASCENDING,
   FLD_BI_9_3 ASCENDING,
   FLD_PD_12_3 ASCENDING,
   FLD_BI_15_5 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YBH
       ;;
(RE000YBH)
       m_CondExec 00,EQ,RE000YBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER MENSUEL ET CUMUL POUR HISTO VENDEUR (RTRE00)               
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBJ
       ;;
(RE000YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : RTRE00  DE 048 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/RE000YAM.BRE010JY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BRE010MY.MENSUEL
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -g +1 SORTOUT ${DATA}/PXX0/F45.BRE010MY.MENSUEL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_13_6 13 CH 6
 /FIELDS FLD_BI_1_3 1 CH 3
 /FIELDS FLD_BI_7_6 7 CH 6
 /FIELDS FLD_BI_4_3 4 CH 3
 /KEYS
   FLD_BI_1_3 ASCENDING,
   FLD_BI_4_3 ASCENDING,
   FLD_BI_7_6 ASCENDING,
   FLD_BI_13_6 ASCENDING
 /* Record Type = F  Record Length = 048 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YBK
       ;;
(RE000YBK)
       m_CondExec 00,EQ,RE000YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   CUMUL FICHIER JOURNALIER FRE003 SUR L HEBDO                                
#  REPRISE: NON BACKOUT JOBSET                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBM
       ;;
(RE000YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER EN ENTREE : FRE003  DE 512 DE LONG                                  
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/RE000YAM.BRE003JY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BRE003HY.HEBDO
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/F45.BRE003HY.HEBDO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_10_2 10 CH 2
 /FIELDS FLD_BI_12_3 12 CH 3
 /FIELDS FLD_BI_15_1 15 CH 1
 /FIELDS FLD_BI_7_3 7 CH 3
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_2 ASCENDING,
   FLD_BI_12_3 ASCENDING,
   FLD_BI_15_1 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=RE000YBN
       ;;
(RE000YBN)
       m_CondExec 00,EQ,RE000YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DU FICHIER DE REPRISE CREE EN CAS DE PLANTAGE                        
#  REPRISE FORCEE AU DEBUT                                                     
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RE000YBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=RE000YBQ
       ;;
(RE000YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC DE CUMUL DES INTERFACES                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER CUMUL REMIS A ZERO                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -g +1 OUT1 ${DATA}/PXX0/F45.BNM157AY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000YBQ.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=RE000YBR
       ;;
(RE000YBR)
       m_CondExec 16,NE,RE000YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RE000YZA
       ;;
(RE000YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RE000YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
