      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSHV41 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSHV41-IDENTIFICATEUR.                                       00000050
           05  TSHV41-TRANSID               PIC X(04) VALUE 'HV41'.     00000060
           05  TSHV41-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSHV41-ITEM.                                                 00000090
      *    05  TSHV41-LONGUEUR              PIC S9(4) VALUE +1320.      00000100
      * AJOUT NFLAG X 11                                                00000100
      *    05  TSHV41-LONGUEUR              PIC S9(4) VALUE +1331.      00000100
      * AJOUT TYPVTE X 11                                               00000100
           05  TSHV41-LONGUEUR              PIC S9(4) VALUE +1342.      00000100
           05  TSHV41-DATAS.                                            00000110
               10  TSHV41-LIGNE             OCCURS 11.                  00000120
      *      PREMIERE PARTIE = 122 OCTETS PAR OCCURENCE               * 00000200
                   15  TSHV41-NFLAG            PIC X(01).               00000130
                   15  TSHV41-NCLUSTER         PIC X(13).               00000130
                   15  TSHV41-NMAG             PIC X(03).               00000130
                   15  TSHV41-NVENTE           PIC X(07).               00000140
                   15  TSHV41-DVENTE           PIC X(08).               00000140
                   15  TSHV41-LNOM             PIC X(25).               00000150
                   15  TSHV41-NTEL             PIC X(10).               00000170
                   15  TSHV41-WEXPTAXE         PIC X(01).                       
                   15  TSHV41-TYPVTE           PIC X(01).                       
                   15  TSHV41-DONNEES-CLIENT-HV41.                      00000160
                       20  TSHV41-FLAG-DONNEES-CLIENT PIC X.            00000160
                           88 TSHV41-ACCES-HV41-PAS-EFFECTUE VALUE ' '.         
                           88 TSHV41-ACCES-HV41-EFFECTUE     VALUE 'O'.         
                       20  TSHV41-LPRENOM      PIC X(15).               00000160
                       20  TSHV41-CPOSTAL      PIC X(05).               00000180
                       20  TSHV41-LCOMMUNE     PIC X(32).               00000190
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
