      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *********************************************************                 
      *   COPY DE LA TABLE RVBA3000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA3000                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVBA3000.                                                            
           02  BA30-CTIERSBA                                                    
               PIC X(0006).                                                     
           02  BA30-NSOCGL                                                      
               PIC X(0005).                                                     
           02  BA30-NETAB                                                       
               PIC X(0003).                                                     
           02  BA30-NAUX                                                        
               PIC X(0003).                                                     
           02  BA30-NTIERSCV                                                    
               PIC X(0006).                                                     
           02  BA30-WELIBELLE                                                   
               PIC X(0001).                                                     
           02  BA30-WEFACTURE                                                   
               PIC X(0001).                                                     
           02  BA30-QTAUXREM                                                    
               PIC S9(3)V9(0002) COMP-3.                                        
           02  BA30-WTYPREM                                                     
               PIC X(0001).                                                     
           02  BA30-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA3000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA3000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-CTIERSBA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-CTIERSBA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-NSOCGL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-NSOCGL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-NETAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-NETAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-NAUX-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-NAUX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-NTIERSCV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-NTIERSCV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-WELIBELLE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-WELIBELLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-WEFACTURE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-WEFACTURE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-QTAUXREM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-QTAUXREM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-WTYPREM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-WTYPREM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA30-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA30-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
