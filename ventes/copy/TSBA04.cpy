      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSBA04 = TS D'EDITION DE LA FACTURE/AVOIR                  *         
      *                                                               *         
      *    CETTE TS CONTIENT LES SEQUENCES DE BA PAR MONTANT          *         
      *                                                               *         
      *    LONGUEUR TOTALE = 1177                                     *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS04                      PIC S9(05) COMP-3 VALUE  +0.              
       01  ITS04-MAX                  PIC S9(05) COMP-3 VALUE +39.              
      *                                                               *         
       01  TS04-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'BA04'.             
           05 TS04-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *01  TS04-LONG                  PIC S9(04) COMP VALUE +897.               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS04-LONG                  PIC S9(04) COMP VALUE +1177.              
      *--                                                                       
       01  TS04-LONG                  PIC S9(04) COMP-5 VALUE +1177.            
      *}                                                                        
      *                                                               *         
       01  TS04-DONNEES.                                                        
           05 TS04-NPOSTE             OCCURS 39.                                
              10 TS04-QBA             PIC 9(04).                                
              10 TS04-PBA             PIC S9(05)V99.                            
              10 TS04-NBADEB          PIC 9(06).                                
              10 TS04-NBAFIN          PIC 9(06).                                
              10 TS04-PBA2            PIC S9(05)V99.                            
           05 TS04-WEMISEURO          PIC X(01).                                
           05 TS04-PTAUX              PIC S9(05)V9(05) COMP-3.                  
                                                                                
