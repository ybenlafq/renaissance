      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL12   ESL12                                              00000020
      ***************************************************************** 00000030
       01   ESL12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDETAILL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDETAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDETAILF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDETAILI  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEEL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATEEF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATEEI   PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATERL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDATERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDATERF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDATERI   PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSOCCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCCREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSOCCREI  PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUCREL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLIEUCREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUCREF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLIEUCREI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTYPEI    PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNUMDOCI  PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODOPL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODOPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODOPF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODOPI   PIC X(5).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSOCOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCOF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSOCOI    PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIEUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUOF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIEUOI   PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSOCDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCDF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSOCDI    PIC X(3).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLIEUDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUDF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLIEUDI   PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNCODICI  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCFAMI    PIC X(5).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCMARQI   PIC X(5).                                       00000810
           02 MLIGD OCCURS   15 TIMES .                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGL   COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MLIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIGF   PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MLIGI   PIC X(78).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: ESL12   ESL12                                              00001080
      ***************************************************************** 00001090
       01   ESL12O REDEFINES ESL12I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MPAGEA    PIC X.                                          00001270
           02 MPAGEC    PIC X.                                          00001280
           02 MPAGEP    PIC X.                                          00001290
           02 MPAGEH    PIC X.                                          00001300
           02 MPAGEV    PIC X.                                          00001310
           02 MPAGEO    PIC X(3).                                       00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MPAGEMA   PIC X.                                          00001340
           02 MPAGEMC   PIC X.                                          00001350
           02 MPAGEMP   PIC X.                                          00001360
           02 MPAGEMH   PIC X.                                          00001370
           02 MPAGEMV   PIC X.                                          00001380
           02 MPAGEMO   PIC X(3).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MDETAILA  PIC X.                                          00001410
           02 MDETAILC  PIC X.                                          00001420
           02 MDETAILP  PIC X.                                          00001430
           02 MDETAILH  PIC X.                                          00001440
           02 MDETAILV  PIC X.                                          00001450
           02 MDETAILO  PIC X.                                          00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MDATEEA   PIC X.                                          00001480
           02 MDATEEC   PIC X.                                          00001490
           02 MDATEEP   PIC X.                                          00001500
           02 MDATEEH   PIC X.                                          00001510
           02 MDATEEV   PIC X.                                          00001520
           02 MDATEEO   PIC X(8).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MDATERA   PIC X.                                          00001550
           02 MDATERC   PIC X.                                          00001560
           02 MDATERP   PIC X.                                          00001570
           02 MDATERH   PIC X.                                          00001580
           02 MDATERV   PIC X.                                          00001590
           02 MDATERO   PIC X(8).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSOCCREA  PIC X.                                          00001620
           02 MSOCCREC  PIC X.                                          00001630
           02 MSOCCREP  PIC X.                                          00001640
           02 MSOCCREH  PIC X.                                          00001650
           02 MSOCCREV  PIC X.                                          00001660
           02 MSOCCREO  PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLIEUCREA      PIC X.                                     00001690
           02 MLIEUCREC PIC X.                                          00001700
           02 MLIEUCREP PIC X.                                          00001710
           02 MLIEUCREH PIC X.                                          00001720
           02 MLIEUCREV PIC X.                                          00001730
           02 MLIEUCREO      PIC X(3).                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MTYPEA    PIC X.                                          00001760
           02 MTYPEC    PIC X.                                          00001770
           02 MTYPEP    PIC X.                                          00001780
           02 MTYPEH    PIC X.                                          00001790
           02 MTYPEV    PIC X.                                          00001800
           02 MTYPEO    PIC X(2).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNUMDOCA  PIC X.                                          00001830
           02 MNUMDOCC  PIC X.                                          00001840
           02 MNUMDOCP  PIC X.                                          00001850
           02 MNUMDOCH  PIC X.                                          00001860
           02 MNUMDOCV  PIC X.                                          00001870
           02 MNUMDOCO  PIC X(7).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCODOPA   PIC X.                                          00001900
           02 MCODOPC   PIC X.                                          00001910
           02 MCODOPP   PIC X.                                          00001920
           02 MCODOPH   PIC X.                                          00001930
           02 MCODOPV   PIC X.                                          00001940
           02 MCODOPO   PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MSOCOA    PIC X.                                          00001970
           02 MSOCOC    PIC X.                                          00001980
           02 MSOCOP    PIC X.                                          00001990
           02 MSOCOH    PIC X.                                          00002000
           02 MSOCOV    PIC X.                                          00002010
           02 MSOCOO    PIC X(3).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLIEUOA   PIC X.                                          00002040
           02 MLIEUOC   PIC X.                                          00002050
           02 MLIEUOP   PIC X.                                          00002060
           02 MLIEUOH   PIC X.                                          00002070
           02 MLIEUOV   PIC X.                                          00002080
           02 MLIEUOO   PIC X(3).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MSOCDA    PIC X.                                          00002110
           02 MSOCDC    PIC X.                                          00002120
           02 MSOCDP    PIC X.                                          00002130
           02 MSOCDH    PIC X.                                          00002140
           02 MSOCDV    PIC X.                                          00002150
           02 MSOCDO    PIC X(3).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MLIEUDA   PIC X.                                          00002180
           02 MLIEUDC   PIC X.                                          00002190
           02 MLIEUDP   PIC X.                                          00002200
           02 MLIEUDH   PIC X.                                          00002210
           02 MLIEUDV   PIC X.                                          00002220
           02 MLIEUDO   PIC X(3).                                       00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MNCODICA  PIC X.                                          00002250
           02 MNCODICC  PIC X.                                          00002260
           02 MNCODICP  PIC X.                                          00002270
           02 MNCODICH  PIC X.                                          00002280
           02 MNCODICV  PIC X.                                          00002290
           02 MNCODICO  PIC X(7).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MCFAMA    PIC X.                                          00002320
           02 MCFAMC    PIC X.                                          00002330
           02 MCFAMP    PIC X.                                          00002340
           02 MCFAMH    PIC X.                                          00002350
           02 MCFAMV    PIC X.                                          00002360
           02 MCFAMO    PIC X(5).                                       00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCMARQA   PIC X.                                          00002390
           02 MCMARQC   PIC X.                                          00002400
           02 MCMARQP   PIC X.                                          00002410
           02 MCMARQH   PIC X.                                          00002420
           02 MCMARQV   PIC X.                                          00002430
           02 MCMARQO   PIC X(5).                                       00002440
           02 DFHMS1 OCCURS   15 TIMES .                                00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MLIGA   PIC X.                                          00002470
             03 MLIGC   PIC X.                                          00002480
             03 MLIGP   PIC X.                                          00002490
             03 MLIGH   PIC X.                                          00002500
             03 MLIGV   PIC X.                                          00002510
             03 MLIGO   PIC X(78).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
