      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV55   EGV55                                              00000020
      ***************************************************************** 00000030
       01   EGV91I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTYPEI    PIC X(35).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNSOCI    PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMAGI    PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNVENTEI  PIC X(7).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLNOML   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCLNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLNOMF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCLNOMI   PIC X(25).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCPOSTALL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCCPOSTALL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCCPOSTALF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCCPOSTALI     PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCNTELL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCNTELL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCNTELF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCNTELI   PIC X(10).                                      00000530
           02 MLIGNEI OCCURS   10 TIMES .                               00000540
      * ZONE DE SELECTION                                               00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MSELECTI   PIC X.                                     00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNSOC1L    COMP PIC S9(4).                            00000600
      *--                                                                       
               04 MNSOC1L COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MNSOC1F    PIC X.                                     00000610
               04 FILLER     PIC X(4).                                  00000620
               04 MNSOC1I    PIC X(3).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNMAG1L    COMP PIC S9(4).                            00000640
      *--                                                                       
               04 MNMAG1L COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MNMAG1F    PIC X.                                     00000650
               04 FILLER     PIC X(4).                                  00000660
               04 MNMAG1I    PIC X(3).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNVENTE1L  COMP PIC S9(4).                            00000680
      *--                                                                       
               04 MNVENTE1L COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MNVENTE1F  PIC X.                                     00000690
               04 FILLER     PIC X(4).                                  00000700
               04 MNVENTE1I  PIC X(7).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLNOML     COMP PIC S9(4).                            00000720
      *--                                                                       
               04 MLNOML COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MLNOMF     PIC X.                                     00000730
               04 FILLER     PIC X(4).                                  00000740
               04 MLNOMI     PIC X(20).                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLPRENOML  COMP PIC S9(4).                            00000760
      *--                                                                       
               04 MLPRENOML COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MLPRENOMF  PIC X.                                     00000770
               04 FILLER     PIC X(4).                                  00000780
               04 MLPRENOMI  PIC X(8).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNTELL     COMP PIC S9(4).                            00000800
      *--                                                                       
               04 MNTELL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNTELF     PIC X.                                     00000810
               04 FILLER     PIC X(4).                                  00000820
               04 MNTELI     PIC X(10).                                 00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCPOSTALL  COMP PIC S9(4).                            00000840
      *--                                                                       
               04 MCPOSTALL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MCPOSTALF  PIC X.                                     00000850
               04 FILLER     PIC X(4).                                  00000860
               04 MCPOSTALI  PIC X(5).                                  00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLCOMMUNEL      COMP PIC S9(4).                       00000880
      *--                                                                       
               04 MLCOMMUNEL COMP-5 PIC S9(4).                                  
      *}                                                                        
               04 MLCOMMUNEF      PIC X.                                00000890
               04 FILLER     PIC X(4).                                  00000900
               04 MLCOMMUNEI      PIC X(11).                            00000910
      * MESSAGE ERREUR                                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MLIBERRI  PIC X(78).                                      00000960
      * CODE TRANSACTION                                                00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCODTRAI  PIC X(4).                                       00001010
      * CICS DE TRAVAIL                                                 00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      * NETNAME                                                         00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MNETNAMI  PIC X(8).                                       00001110
      * CODE TERMINAL                                                   00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MSCREENI  PIC X(5).                                       00001160
      ***************************************************************** 00001170
      * SDF: EGV55   EGV55                                              00001180
      ***************************************************************** 00001190
       01   EGV91O REDEFINES EGV91I.                                    00001200
           02 FILLER    PIC X(12).                                      00001210
      * DATE DU JOUR                                                    00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MDATJOUA  PIC X.                                          00001240
           02 MDATJOUC  PIC X.                                          00001250
           02 MDATJOUP  PIC X.                                          00001260
           02 MDATJOUH  PIC X.                                          00001270
           02 MDATJOUV  PIC X.                                          00001280
           02 MDATJOUO  PIC X(10).                                      00001290
      * HEURE                                                           00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MTIMJOUA  PIC X.                                          00001320
           02 MTIMJOUC  PIC X.                                          00001330
           02 MTIMJOUP  PIC X.                                          00001340
           02 MTIMJOUH  PIC X.                                          00001350
           02 MTIMJOUV  PIC X.                                          00001360
           02 MTIMJOUO  PIC X(5).                                       00001370
      * NUMERO DE PAGE COURANTE                                         00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MNOPAGEA  PIC X.                                          00001400
           02 MNOPAGEC  PIC X.                                          00001410
           02 MNOPAGEP  PIC X.                                          00001420
           02 MNOPAGEH  PIC X.                                          00001430
           02 MNOPAGEV  PIC X.                                          00001440
           02 MNOPAGEO  PIC X(3).                                       00001450
      * NOMBRE TOTAL DE PAGES                                           00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MNBPAGEA  PIC X.                                          00001480
           02 MNBPAGEC  PIC X.                                          00001490
           02 MNBPAGEP  PIC X.                                          00001500
           02 MNBPAGEH  PIC X.                                          00001510
           02 MNBPAGEV  PIC X.                                          00001520
           02 MNBPAGEO  PIC X(3).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTYPEA    PIC X.                                          00001550
           02 MTYPEC    PIC X.                                          00001560
           02 MTYPEP    PIC X.                                          00001570
           02 MTYPEH    PIC X.                                          00001580
           02 MTYPEV    PIC X.                                          00001590
           02 MTYPEO    PIC X(35).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MNSOCA    PIC X.                                          00001620
           02 MNSOCC    PIC X.                                          00001630
           02 MNSOCP    PIC X.                                          00001640
           02 MNSOCH    PIC X.                                          00001650
           02 MNSOCV    PIC X.                                          00001660
           02 MNSOCO    PIC X(3).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNMAGA    PIC X.                                          00001690
           02 MNMAGC    PIC X.                                          00001700
           02 MNMAGP    PIC X.                                          00001710
           02 MNMAGH    PIC X.                                          00001720
           02 MNMAGV    PIC X.                                          00001730
           02 MNMAGO    PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNVENTEA  PIC X.                                          00001760
           02 MNVENTEC  PIC X.                                          00001770
           02 MNVENTEP  PIC X.                                          00001780
           02 MNVENTEH  PIC X.                                          00001790
           02 MNVENTEV  PIC X.                                          00001800
           02 MNVENTEO  PIC X(7).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MCLNOMA   PIC X.                                          00001830
           02 MCLNOMC   PIC X.                                          00001840
           02 MCLNOMP   PIC X.                                          00001850
           02 MCLNOMH   PIC X.                                          00001860
           02 MCLNOMV   PIC X.                                          00001870
           02 MCLNOMO   PIC X(25).                                      00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCCPOSTALA     PIC X.                                     00001900
           02 MCCPOSTALC     PIC X.                                     00001910
           02 MCCPOSTALP     PIC X.                                     00001920
           02 MCCPOSTALH     PIC X.                                     00001930
           02 MCCPOSTALV     PIC X.                                     00001940
           02 MCCPOSTALO     PIC X(5).                                  00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCNTELA   PIC X.                                          00001970
           02 MCNTELC   PIC X.                                          00001980
           02 MCNTELP   PIC X.                                          00001990
           02 MCNTELH   PIC X.                                          00002000
           02 MCNTELV   PIC X.                                          00002010
           02 MCNTELO   PIC X(10).                                      00002020
           02 MLIGNEO OCCURS   10 TIMES .                               00002030
      * ZONE DE SELECTION                                               00002040
               04 FILLER     PIC X(2).                                  00002050
               04 MSELECTA   PIC X.                                     00002060
               04 MSELECTC   PIC X.                                     00002070
               04 MSELECTP   PIC X.                                     00002080
               04 MSELECTH   PIC X.                                     00002090
               04 MSELECTV   PIC X.                                     00002100
               04 MSELECTO   PIC X.                                     00002110
               04 FILLER     PIC X(2).                                  00002120
               04 MNSOC1A    PIC X.                                     00002130
               04 MNSOC1C    PIC X.                                     00002140
               04 MNSOC1P    PIC X.                                     00002150
               04 MNSOC1H    PIC X.                                     00002160
               04 MNSOC1V    PIC X.                                     00002170
               04 MNSOC1O    PIC X(3).                                  00002180
               04 FILLER     PIC X(2).                                  00002190
               04 MNMAG1A    PIC X.                                     00002200
               04 MNMAG1C    PIC X.                                     00002210
               04 MNMAG1P    PIC X.                                     00002220
               04 MNMAG1H    PIC X.                                     00002230
               04 MNMAG1V    PIC X.                                     00002240
               04 MNMAG1O    PIC X(3).                                  00002250
               04 FILLER     PIC X(2).                                  00002260
               04 MNVENTE1A  PIC X.                                     00002270
               04 MNVENTE1C  PIC X.                                     00002280
               04 MNVENTE1P  PIC X.                                     00002290
               04 MNVENTE1H  PIC X.                                     00002300
               04 MNVENTE1V  PIC X.                                     00002310
               04 MNVENTE1O  PIC X(7).                                  00002320
               04 FILLER     PIC X(2).                                  00002330
               04 MLNOMA     PIC X.                                     00002340
               04 MLNOMC     PIC X.                                     00002350
               04 MLNOMP     PIC X.                                     00002360
               04 MLNOMH     PIC X.                                     00002370
               04 MLNOMV     PIC X.                                     00002380
               04 MLNOMO     PIC X(20).                                 00002390
               04 FILLER     PIC X(2).                                  00002400
               04 MLPRENOMA  PIC X.                                     00002410
               04 MLPRENOMC  PIC X.                                     00002420
               04 MLPRENOMP  PIC X.                                     00002430
               04 MLPRENOMH  PIC X.                                     00002440
               04 MLPRENOMV  PIC X.                                     00002450
               04 MLPRENOMO  PIC X(8).                                  00002460
               04 FILLER     PIC X(2).                                  00002470
               04 MNTELA     PIC X.                                     00002480
               04 MNTELC     PIC X.                                     00002490
               04 MNTELP     PIC X.                                     00002500
               04 MNTELH     PIC X.                                     00002510
               04 MNTELV     PIC X.                                     00002520
               04 MNTELO     PIC X(10).                                 00002530
               04 FILLER     PIC X(2).                                  00002540
               04 MCPOSTALA  PIC X.                                     00002550
               04 MCPOSTALC  PIC X.                                     00002560
               04 MCPOSTALP  PIC X.                                     00002570
               04 MCPOSTALH  PIC X.                                     00002580
               04 MCPOSTALV  PIC X.                                     00002590
               04 MCPOSTALO  PIC X(5).                                  00002600
               04 FILLER     PIC X(2).                                  00002610
               04 MLCOMMUNEA      PIC X.                                00002620
               04 MLCOMMUNEC PIC X.                                     00002630
               04 MLCOMMUNEP PIC X.                                     00002640
               04 MLCOMMUNEH PIC X.                                     00002650
               04 MLCOMMUNEV PIC X.                                     00002660
               04 MLCOMMUNEO      PIC X(11).                            00002670
      * MESSAGE ERREUR                                                  00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLIBERRA  PIC X.                                          00002700
           02 MLIBERRC  PIC X.                                          00002710
           02 MLIBERRP  PIC X.                                          00002720
           02 MLIBERRH  PIC X.                                          00002730
           02 MLIBERRV  PIC X.                                          00002740
           02 MLIBERRO  PIC X(78).                                      00002750
      * CODE TRANSACTION                                                00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCODTRAA  PIC X.                                          00002780
           02 MCODTRAC  PIC X.                                          00002790
           02 MCODTRAP  PIC X.                                          00002800
           02 MCODTRAH  PIC X.                                          00002810
           02 MCODTRAV  PIC X.                                          00002820
           02 MCODTRAO  PIC X(4).                                       00002830
      * CICS DE TRAVAIL                                                 00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCICSA    PIC X.                                          00002860
           02 MCICSC    PIC X.                                          00002870
           02 MCICSP    PIC X.                                          00002880
           02 MCICSH    PIC X.                                          00002890
           02 MCICSV    PIC X.                                          00002900
           02 MCICSO    PIC X(5).                                       00002910
      * NETNAME                                                         00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNETNAMA  PIC X.                                          00002940
           02 MNETNAMC  PIC X.                                          00002950
           02 MNETNAMP  PIC X.                                          00002960
           02 MNETNAMH  PIC X.                                          00002970
           02 MNETNAMV  PIC X.                                          00002980
           02 MNETNAMO  PIC X(8).                                       00002990
      * CODE TERMINAL                                                   00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MSCREENA  PIC X.                                          00003020
           02 MSCREENC  PIC X.                                          00003030
           02 MSCREENP  PIC X.                                          00003040
           02 MSCREENH  PIC X.                                          00003050
           02 MSCREENV  PIC X.                                          00003060
           02 MSCREENO  PIC X(5).                                       00003070
                                                                                
