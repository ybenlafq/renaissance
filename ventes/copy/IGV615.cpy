      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV615 AU 10/01/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,01,BI,A,                          *        
      *                           08,03,BI,A,                          *        
      *                           11,08,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,07,BI,A,                          *        
      *                           29,07,BI,A,                          *        
      *                           36,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV615.                                                        
            05 NOMETAT-IGV615           PIC X(6) VALUE 'IGV615'.                
            05 RUPTURES-IGV615.                                                 
           10 IGV615-WDISPATCH          PIC X(01).                      007  001
           10 IGV615-NLIEU              PIC X(03).                      008  003
           10 IGV615-DCREATION          PIC X(08).                      011  008
           10 IGV615-CMODDEL            PIC X(03).                      019  003
           10 IGV615-NVENTE             PIC X(07).                      022  007
           10 IGV615-NCODIC             PIC X(07).                      029  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV615-SEQUENCE           PIC S9(04) COMP.                036  002
      *--                                                                       
           10 IGV615-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV615.                                                   
           10 IGV615-LLIEU              PIC X(20).                      038  020
           10 IGV615-NLIEUMODIF         PIC X(03).                      058  003
           10 IGV615-NSOCIETE           PIC X(03).                      061  003
           10 IGV615-NSOCMODIF          PIC X(03).                      064  003
           10 IGV615-QVENDUE            PIC X(03).                      067  003
           10 IGV615-TITRE              PIC X(15).                      070  015
           10 IGV615-DATETR             PIC X(08).                      085  008
            05 FILLER                      PIC X(420).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGV615-LONG           PIC S9(4)   COMP  VALUE +092.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGV615-LONG           PIC S9(4) COMP-5  VALUE +092.           
                                                                                
      *}                                                                        
