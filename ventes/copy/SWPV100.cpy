      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      * LG = 210                                                                
      * LG = 250 AJOUT PRIME BRUTE + FILLER                                     
      *                                                                         
       01  FPV100-DSECT.                                                        
           03  FPV100-CLEF.                                                     
>003           05  FPV100-NSOCIETE         PIC  X(03).                          
>006           05  FPV100-NMAG             PIC  X(03).                          
>012           05  FPV100-CVENDEUR         PIC  X(06).                          
>018           05  FPV100-DMVENTE          PIC  X(06).                          
>023           05  FPV100-AGREGAT          PIC  9(05).                          
      *                                                                         
               05  FPV100-AGREG1.                                               
>031            10 FPV100-DCREATION        PIC  X(08).                          
>041            10 FPV100-LVEND10          PIC  X(10).                          
>048            10 FPV100-NCODIC           PIC  X(07).                          
>053            10 FPV100-CFAM             PIC  X(05).                          
>058            10 FPV100-CMARQ            PIC  X(05).                          
>065            10 FPV100-NVENTE           PIC  X(07).                          
      *                                                                         
               05  FPV100-AGREG2 REDEFINES FPV100-AGREG1.                       
>028            10 FPV100-CRAYONFAM        PIC  X(05).                          
>048            10 FPV100-LAGREGATED       PIC  X(20).                          
>065            10 FILLER                  PIC  X(17).                          
      *                                                                         
>066           05  FPV100-REPRISE          PIC  X(01).                          
>067           05  FPV100-GROUPE           PIC  X(01).                          
      *                                                                         
>071       03  FPV100-QVENDUE              PIC S9(05)V99  COMP-3.               
>075       03  FPV100-QVENDUEEP            PIC S9(05)V99  COMP-3.               
>079       03  FPV100-QVENDUESV            PIC S9(05)V99  COMP-3.               
>083       03  FPV100-QVENDUECM            PIC S9(05)V99  COMP-3.               
>086       03  FPV100-QNBPSE               PIC S9(05)     COMP-3.               
>090       03  FPV100-QNBPSAB              PIC S9(05)V99  COMP-3.               
      *                                                                         
>095       03  FPV100-PCA                  PIC S9(07)V99  COMP-3.               
>100       03  FPV100-PCAEP                PIC S9(07)V99  COMP-3.               
>105       03  FPV100-PCASV                PIC S9(07)V99  COMP-3.               
>110       03  FPV100-PCACM                PIC S9(07)V99  COMP-3.               
>115       03  FPV100-PCAPSE               PIC S9(07)V99  COMP-3.               
>120       03  FPV100-PRMP                 PIC S9(07)V99  COMP-3.               
>125       03  FPV100-PRMPEP               PIC S9(07)V99  COMP-3.               
>130       03  FPV100-PRMPSV               PIC S9(07)V99  COMP-3.               
>135       03  FPV100-PRMPCM               PIC S9(07)V99  COMP-3.               
>140       03  FPV100-MTREM                PIC S9(07)V99  COMP-3.               
>145       03  FPV100-MTREMEP              PIC S9(07)V99  COMP-3.               
>150       03  FPV100-PMTVOL               PIC S9(07)V99  COMP-3.               
>155       03  FPV100-PMTJAUNE             PIC S9(07)V99  COMP-3.               
>160       03  FPV100-PMTGRISE             PIC S9(07)V99  COMP-3.               
>165       03  FPV100-PMTEP                PIC S9(07)V99  COMP-3.               
>170       03  FPV100-PMTPSE               PIC S9(07)V99  COMP-3.               
>175       03  FPV100-CGARANTIE            PIC  X(05).                          
>180       03  FPV100-MTVA                 PIC S9(07)V99  COMP-3.               
>185       03  FPV100-MTVAEP               PIC S9(07)V99  COMP-3.               
>190       03  FPV100-MTVASV               PIC S9(07)V99  COMP-3.               
>195       03  FPV100-MTVACM               PIC S9(07)V99  COMP-3.               
>200       03  FPV100-MTVAPSE              PIC S9(07)V99  COMP-3.               
      *                                                                         
>208       03  FPV100-DMVT                 PIC  X(08).                          
>213       03  FPV100-PMTBRUTE             PIC S9(07)V99  COMP-3.               
>216       03  FPV100-NSOCV                PIC  X(03).                          
>219       03  FPV100-NLIEUV               PIC  X(03).                          
>220       03  FPV100-WRETRO               PIC  X(01).                          
>221       03  FPV100-CODFIL               PIC  X(01).                          
>250       03  FILLER                      PIC  X(29).                          
      *                                                                         
                                                                                
