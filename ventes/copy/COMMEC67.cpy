      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TRANSACTION: GV00                                              *        
      * TITRE      : COMMAREA D'APPEL MEC67, MODULE INTERROGATION      *        
      *              DES STOCKS DARTY POUR L'EXPEDIION (EMX)           *        
      * LONGUEUR   : ????                                              *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
        01 COMM-EC67-APPLI.                                                     
           05 COMM-EC67-ZIN.                                                    
               10 COMM-EC67-CAPPEL           PIC X(01).                         
      *{ remove-comma-in-dde 1.5                                                
      *           88 COMM-EC67-CONTROLE      VALUE 'D', 'M', '4', 'C'.          
      *--                                                                       
                  88 COMM-EC67-CONTROLE      VALUE 'D'  'M'  '4'  'C'.          
      *}                                                                        
                  88 COMM-EC67-CTRL-MGV64    VALUE '4'.                         
                  88 COMM-EC67-NEW-APPEL     VALUE ' '.                         
               10 COMM-EC67-NEW-MODE.                                           
                  15 COMM-EC67-MAJ-STOCK      PIC X(01).                        
                  15 COMM-EC67-INTERNET       PIC X(01).                        
LAVOIR         10 COMM-EC67-TRANS             PIC X(01).                        
LAVOIR            88 COMM-EC67-TRANS-GV       VALUE 'G'.                        
LAVOIR            88 COMM-EC67-TRANS-MECC     VALUE 'M'.                        
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-EC67-SUFFIX           PIC X(01).                         
               10 COMM-EC67-NSOCIETE         PIC X(03).                         
               10 COMM-EC67-NLIEU            PIC X(03).                         
               10 COMM-EC67-MDLIV-NSOC       PIC X(03).                         
               10 COMM-EC67-MDLIV-NLIEU      PIC X(03).                         
               10 COMM-EC67-NVENTE           PIC X(07).                         
               10 COMM-EC67-NCODIC           PIC X(07).                         
               10 COMM-EC67-NSEQNQ           PIC S9(5) COMP-3.                  
               10 COMM-EC67-WCQERESF         PIC X(01).                         
                  88 COMM-EC67-CMD-FNR                 VALUE 'F'.               
               10 COMM-EC67-DDELIV           PIC X(08).                         
               10 COMM-EC67-SSAAMMJJ         PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-EC67-QTE              PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-EC67-QTE              PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
               10 COMM-EC67-FL05.                                               
                   15 COMM-EC67-FL05-DEPOT     OCCURS 5.                        
                      20 COMM-EC67-FL05-NSOCDEPOT           PIC X(03).          
                      20 COMM-EC67-FL05-NDEPOT              PIC X(03).          
               10 COMM-EC67-WEMPORTE        PIC X(01).                          
               10 COMM-EC67-NORDRE          PIC X(05).                          
               10 COMM-EC67-NCDEWC          PIC S9(15) PACKED-DECIMAL.          
               10 COMM-EC67-GA00-QHAUTEUR     PIC S9(3) COMP-3.                 
               10 COMM-EC67-GA00-QPROFONDEUR  PIC S9(3) COMP-3.                 
               10 COMM-EC67-GA00-QLARGEUR     PIC S9(3) COMP-3.                 
               10 FILLER                    PIC X(14).                          
      * ->                                                                      
           05 COMM-EC67-ZOUT.                                                   
               10 COMM-EC67-CODRET           PIC X(04).                         
               10 COMM-EC67-LMESSAGE         PIC X(53).                         
               10 COMM-EC67-LIEU-STOCK.                                         
                  15 COMM-EC67-NSOCDEPOT     PIC X(03).                         
                  15 COMM-EC67-NDEPOT        PIC X(03).                         
               10 COMM-EC67-ETAT-RESERVATION PIC 9(01).                         
                  88 COMM-EC67-AUCUN-STOCK-DISPO       VALUE 0.                 
                  88 COMM-EC67-STOCK-DISPONIBLE        VALUE 1.                 
                  88 COMM-EC67-STOCK-DACEM             VALUE 2.                 
               10 COMM-EC67-ETAT-FOURNISSEUR PIC 9(01).                         
                  88 COMM-EC67-STOCK-FOURN-NON         VALUE 0.                 
                  88 COMM-EC67-STOCK-FOURN-OUI         VALUE 1.                 
               10 COMM-EC67-CSTATUT          PIC X(01).                         
               10 FILLER                     PIC X(179).                        
                                                                                
