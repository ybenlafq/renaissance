      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:59 >
      *****************************************************************
      *   CCOPY MESSAGE MQ AVOIRS, BONS D'ACHAT, ACOMPTES DACEM,
      *                   LISTE DE MARIAGE ...
      *
      *****************************************************************
      * AUTEUR : L.ARMANT                                             *
      * OBJET  : AJOUT DES ZONES COMPLEMENT D'ADRESSE                 *
      * DATE   : 04/02/2013                                           *
      *****************************************************************
      *
       01  WS-MQ10.
         02  WS-QUEUE.
               10   MQ10-CORRELID PIC    X(24).
         02  WS-CODRET            PIC    XX.
         02  WS-MESSAGE.
            05  MESS-ENTETE.
               10   MES-TYPE      PIC    X(3).
               10   MES-NSOCMSG   PIC    X(3).
               10   MES-NLIEUMSG  PIC    X(3).
               10   MES-NSOCDST   PIC    X(3).
               10   MES-NLIEUDST  PIC    X(3).
               10   MES-NORD      PIC    9(8).
               10   MES-LPROG     PIC    X(10).
               10   MES-DJOUR     PIC    X(8).
               10   MES-WSID      PIC    X(10).
               10   MES-USER      PIC    X(10).
               10   MES-CHRONO    PIC    9(7).
               10   MES-NBRMSG    PIC    9(7).
               10   MES-FILLER    PIC    X(30).
            05  MESS-DATA                    PIC X(2100).
            05  MESS-DATA-AV3 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR3           PIC X(3).
               10  MESS-LIEU-AVOIR3          PIC X(3).
               10  MESS-NUMERO-AVOIR3        PIC X(8).
               10  MESS-DEVISE-AVOIR3        PIC X(3).
               10  MESS-MONTANT-AVOIR3       PIC 9(7)V99.
               10  FILLER                    PIC X(2074).
            05  MESS-DATA-AV2 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR2           PIC X(3).
               10  MESS-LIEU-AVOIR2          PIC X(3).
               10  MESS-NUMERO-AVOIR2        PIC X(8).
               10  FILLER                    PIC X(2086).
            05  MESS-REP-AV2 REDEFINES MESS-DATA.
               10  MESS-DEVISE-AVOIR2        PIC X(3).
               10  MESS-MONTANT-AVOIR2       PIC 9(7)V99.
               10  MESS-ETAT-AVOIR           PIC X.
               10  MESS-DATEVALID-AVOIR      PIC X(8).
               10  FILLER                    PIC X(2079).
            05  MESS-DATA-AV4 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR4           PIC X(3).
               10  MESS-LIEU-AVOIR4          PIC X(3).
               10  MESS-NUMERO-AVOIR4        PIC X(8).
               10  FILLER                    PIC X(2086).
            05  MESS-DATA-AV5 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR-CRE        PIC X(3).
               10  MESS-LIEU-AVOIR-CRE       PIC X(3).
               10  MESS-NUMERO-AVOIR-CRE     PIC X(8).
               10  MESS-MONTANT-AVOIR-CRE    PIC 9(7)V99.
               10  MESS-DEVISE-AVOIR-CRE     PIC X(3).
      *        10  MESS-DATE-VALID-AVOIR-CRE PIC X(8).
               10  MESS-TYPEUTIL-AVOIR-CRE   PIC X(5).
               10  FILLER                    PIC X(2069).
            05  MESS-DATA-AV6 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR-ANU        PIC X(3).
               10  MESS-LIEU-AVOIR-ANU       PIC X(3).
               10  MESS-NUMERO-AVOIR-ANU     PIC X(8).
               10  FILLER                    PIC X(2086).
            05  MESS-DATA-AC3 REDEFINES MESS-DATA.
               10  MESS-SOC-ACPT3            PIC X(3).
               10  MESS-LIEU-ACPT3           PIC X(3).
               10  MESS-NUMERO-ACPT3         PIC X(10).
               10  MESS-DEVISE-ACPT3         PIC X(3).
               10  MESS-MONTANT-ACPT3        PIC 9(7)V99.
               10  FILLER                    PIC X(2072).
            05  MESS-DATA-AC2 REDEFINES MESS-DATA.
               10  MESS-SOC-ACPT2            PIC X(3).
               10  MESS-LIEU-ACPT2           PIC X(3).
               10  MESS-NUMERO-ACPT2         PIC X(10).
               10  FILLER                    PIC X(2084).
            05  MESS-REP-AC2 REDEFINES MESS-DATA.
               10  MESS-DEVISE-ACPT2         PIC X(3).
               10  MESS-MONTANT-ACPT2        PIC 9(7)V99.
               10  MESS-ETAT-ACPT            PIC X.
               10  MESS-DATEVALID-ACPT       PIC X(8).
               10  FILLER                    PIC X(2079).
            05  MESS-DATA-AC4 REDEFINES MESS-DATA.
               10  MESS-SOC-ACPT4            PIC X(3).
               10  MESS-LIEU-ACPT4           PIC X(3).
               10  MESS-NUMERO-ACPT4         PIC X(10).
               10  FILLER                    PIC X(2084).
            05  MESS-DATA-AC5 REDEFINES MESS-DATA.
               10  MESS-SOC-ACPT-CRE         PIC X(3).
               10  MESS-LIEU-ACPT-CRE        PIC X(3).
               10  MESS-NUMERO-ACPT-CRE      PIC X(10).
               10  MESS-MONTANT-ACPT-CRE     PIC 9(7)V99.
               10  MESS-DEVISE-ACPT-CRE      PIC X(3).
      *        10  MESS-DATE-VALID-ACPT-CRE  PIC X(8).
               10  FILLER                    PIC X(2072).
            05  MESS-DATA-AC6 REDEFINES MESS-DATA.
               10  MESS-SOC-ACPT-ANU         PIC X(3).
               10  MESS-LIEU-ACPT-ANU        PIC X(3).
               10  MESS-NUMERO-ACPT-ANU      PIC X(10).
               10  FILLER                    PIC X(2084).
            05  MESS-DATA-BA3 REDEFINES MESS-DATA.
               10  MESS-SOC-BA3              PIC X(3).
               10  MESS-LIEU-BA3             PIC X(3).
               10  MESS-NUMERO-BA3           PIC X(6).
               10  MESS-DEVISE-BA3           PIC X(3).
               10  MESS-MONTANT-BA3          PIC 9(7)V99.
      *        10  MESS-DJOUR-BA3            PIC X(8).
               10  FILLER                    PIC X(2076).
            05  MESS-DATA-BA2 REDEFINES MESS-DATA.
               10  FILLER OCCURS 99.
                  15  MESS-SOC-BA2              PIC X(3).
                  15  MESS-LIEU-BA2             PIC X(3).
                  15  MESS-NUMERO-BA2           PIC X(6).
                  15  MESS-DJOUR-FILLER         PIC X(9).
               10  MESS-NBBA-BA2             PIC 999.
               10  FILLER                    PIC X(18).
            05  MESS-REP-BA2 REDEFINES MESS-DATA.
               10  FILLER OCCURS 99.
                 15  MESS-DEVISE-BA2           PIC X(3).
                 15  MESS-MONTANT-BA2          PIC 9(7)V99.
                 15  MESS-ETAT-BA2             PIC X.
                 15  MESS-DATEVALID-BA2        PIC X(8).
               10  FILLER                    PIC X(21).
            05  MESS-DATA-BA4 REDEFINES MESS-DATA.
               10  MESS-SOC-BA-ACH           PIC X(3).
               10  MESS-LIEU-BA-ACH          PIC X(3).
               10  MESS-NUMERO-BA-ACH        PIC X(6).
               10  MESS-DEVISE-BA-ACH        PIC X(3).
               10  MESS-MONTANT-BA-ACH       PIC 9(7)V99.
               10  MESS-DJOUR-BA-ACH         PIC X(08).
               10  FILLER                    PIC X(2068).
            05  MESS-DATA-BA6 REDEFINES MESS-DATA.
               10  MESS-SOC-BA-ANU           PIC X(3).
               10  MESS-LIEU-BA-ANU          PIC X(3).
               10  MESS-NUMERO-BA-ANU        PIC X(6).
      *        10  MESS-DJOUR-BA-ANU         PIC X(8).
               10  FILLER                    PIC X(2088).
            05  MESS-DATA-LM2 REDEFINES MESS-DATA.
               10  MESS-SOC-LM               PIC X(3).
               10  MESS-LIEU-LM              PIC X(3).
               10  MESS-NUMERO-LM            PIC X(7).
               10  FILLER                    PIC X(2087).
            05  MESS-REP-LM2 REDEFINES MESS-DATA.
               10  MESS-EXISTENCE-LM         PIC X(1).
               10  FILLER                    PIC X(2099).
            05  MESS-REP-ERREUR REDEFINES MESS-DATA.
               10  MESS-CODRET               PIC X(1).
                   88  MESS-CODRET-OK        VALUE ' '.
                   88  MESS-CODRET-ERREUR    VALUE '1'.
               10  MESS-LIBERR               PIC X(99).
               10  FILLER                    PIC X(2000).
            05  MESS-DATA-AV7 REDEFINES MESS-DATA.
               10  MESS-SOC-AVOIR7           PIC X(3).
               10  MESS-LIEU-AVOIR7          PIC X(3).
               10  MESS-NUMERO-AVOIR7        PIC X(8).
      *        10  FILLER                    PIC X(2086).
               10  AV7-CTITRENOM             PIC X(05).
               10  AV7-LNOM                  PIC X(25).
               10  AV7-LPRENOM               PIC X(15).
               10  AV7-CVOIE                 PIC X(05).
               10  AV7-CTVOIE                PIC X(04).
               10  AV7-LNOMVOIE              PIC X(21).
               10  AV7-CPOSTAL               PIC X(05).
               10  AV7-LCOMMUNE              PIC X(32).
               10  AV7-LBUREAU               PIC X(26).
               10  AV7-LCMPAD1               PIC X(32).
               10  AV7-LCMPAD2               PIC X(32).
               10  AV7-LBATIMENT             PIC X(03).
               10  AV7-LESCALIER             PIC X(03).
               10  AV7-LETAGE                PIC X(03).
               10  AV7-LPORTE                PIC X(03).
      *        10  FILLER                    PIC X(1948).
               10  FILLER                    PIC X(1872).
      
