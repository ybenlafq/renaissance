      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM050 AU 27/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,08,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,03,PD,A,                          *        
      *                           24,05,BI,A,                          *        
      *                           29,20,BI,A,                          *        
      *                           49,07,BI,A,                          *        
      *                           56,03,PD,A,                          *        
      *                           59,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISM050.                                                        
            05 NOMETAT-ISM050           PIC X(6) VALUE 'ISM050'.                
            05 RUPTURES-ISM050.                                                 
           10 ISM050-FDATE              PIC X(08).                      007  008
           10 ISM050-NSOCIETE           PIC X(03).                      015  003
           10 ISM050-NLIEU              PIC X(03).                      018  003
           10 ISM050-WSEQFAM            PIC S9(05)      COMP-3.         021  003
           10 ISM050-CMARQ              PIC X(05).                      024  005
           10 ISM050-LREFFOURN          PIC X(20).                      029  020
           10 ISM050-NCODIC             PIC X(07).                      049  007
           10 ISM050-QSTOCK             PIC S9(05)      COMP-3.         056  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM050-SEQUENCE           PIC S9(04) COMP.                059  002
      *--                                                                       
           10 ISM050-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISM050.                                                   
           10 ISM050-CFAM               PIC X(05).                      061  005
           10 ISM050-LFAM               PIC X(20).                      066  020
           10 ISM050-LLIEU              PIC X(20).                      086  020
           10 ISM050-LSOC               PIC X(20).                      106  020
           10 ISM050-DMAJSTOCK          PIC X(08).                      126  008
            05 FILLER                      PIC X(379).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISM050-LONG           PIC S9(4)   COMP  VALUE +133.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISM050-LONG           PIC S9(4) COMP-5  VALUE +133.           
                                                                                
      *}                                                                        
