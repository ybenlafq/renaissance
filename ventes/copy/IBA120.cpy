      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA120 AU 14/03/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,10,BI,A,                          *        
      *                           17,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA120.                                                        
            05 NOMETAT-IBA120           PIC X(6) VALUE 'IBA120'.                
            05 RUPTURES-IBA120.                                                 
           10 IBA120-NTIERSSAP          PIC X(10).                      007  010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA120-SEQUENCE           PIC S9(04) COMP.                017  002
      *--                                                                       
           10 IBA120-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA120.                                                   
           10 IBA120-CTIERSBA           PIC X(06).                      019  006
           10 IBA120-DANNEE             PIC X(04).                      025  004
           10 IBA120-DANNEE-1           PIC X(04).                      029  004
           10 IBA120-DANNEE-2           PIC X(04).                      033  004
           10 IBA120-DANNEE-3           PIC X(04).                      037  004
           10 IBA120-DANNEE-4           PIC X(04).                      041  004
           10 IBA120-PCAREEL            PIC S9(09)V9(2) COMP-3.         045  006
           10 IBA120-PCAREEL-1          PIC S9(09)V9(2) COMP-3.         051  006
           10 IBA120-PCAREEL-2          PIC S9(09)V9(2) COMP-3.         057  006
           10 IBA120-PCAREEL-3          PIC S9(09)V9(2) COMP-3.         063  006
           10 IBA120-PCAREEL-4          PIC S9(09)V9(2) COMP-3.         069  006
           10 IBA120-PCAREEL-5          PIC S9(09)V9(2) COMP-3.         075  006
           10 IBA120-PCATHEO            PIC S9(09)V9(2) COMP-3.         081  006
           10 IBA120-PCATHEO-1          PIC S9(09)V9(2) COMP-3.         087  006
           10 IBA120-PCATHEO-2          PIC S9(09)V9(2) COMP-3.         093  006
           10 IBA120-PCATHEO-3          PIC S9(09)V9(2) COMP-3.         099  006
           10 IBA120-PCATHEO-4          PIC S9(09)V9(2) COMP-3.         105  006
           10 IBA120-PCATHEO-5          PIC S9(09)V9(2) COMP-3.         111  006
           10 IBA120-QTAUXREM           PIC S9(03)V9(2) COMP-3.         117  003
            05 FILLER                      PIC X(393).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA120-LONG           PIC S9(4)   COMP  VALUE +119.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA120-LONG           PIC S9(4) COMP-5  VALUE +119.           
                                                                                
      *}                                                                        
