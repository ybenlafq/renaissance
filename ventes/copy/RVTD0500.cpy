      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE .RVTD0500                          *        
      ******************************************************************        
       01  RVTD0500.                                                            
           10 TD05-CAPPLI               PIC X(5).                               
           10 TD05-CMARQ                PIC X(5).                               
           10 TD05-CLESUPP              PIC X(15).                              
           10 TD05-DRGLT                PIC X(8).                               
           10 TD05-NSOCIETE             PIC X(3).                               
           10 TD05-NLIEU                PIC X(3).                               
           10 TD05-NVENTE               PIC X(7).                               
           10 TD05-NDOSSIER             PIC S9(3)V USAGE COMP-3.                
           10 TD05-CTYPPREST            PIC X(5).                               
           10 TD05-PRGLT                PIC S9(7)V9(2) USAGE COMP-3.            
           10 TD05-CODREM               PIC X(5).                               
           10 TD05-GAMME                PIC X(5).                               
           10 TD05-POINTVTE             PIC X(15).                              
           10 TD05-NOMTITULAIRE         PIC X(30).                              
           10 TD05-DSOUSCRIPTION        PIC X(8).                               
           10 TD05-DRESILIATION         PIC X(8).                               
           10 TD05-COFFRE               PIC X(10).                              
           10 TD05-LOFFRE               PIC X(30).                              
           10 TD05-TYPREM               PIC X(10).                              
           10 TD05-REMREPR              PIC X(4).                               
           10 TD05-CREM1                PIC X(10).                              
           10 TD05-CREM2                PIC X(10).                              
           10 TD05-CRGESTION            PIC X(10).                              
           10 TD05-DSYST                PIC S9(13)V USAGE COMP-3.               
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  IRVTD0500.                                                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP OCCURS 24 TIMES.          
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5 OCCURS 24 TIMES.              
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 24      *        
      ******************************************************************        
                                                                                
