      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVGV0500                                     00020005
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV0500                 00060005
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0500.                                                    00090005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0500.                                                            
      *}                                                                        
           02  GV05-NSOCIETE                                            00100004
               PIC X(0003).                                             00110004
           02  GV05-NLIEU                                               00120004
               PIC X(0003).                                             00130004
           02  GV05-NVENTE                                              00140004
               PIC X(0007).                                             00150004
           02  GV05-NDOSSIER                                            00160004
               PIC S9(3) COMP-3.                                        00170006
           02  GV05-CDOSSIER                                            00180004
               PIC X(0005).                                             00190001
           02  GV05-CTYPENT                                             00200004
               PIC X(0002).                                             00210001
           02  GV05-NENTITE                                             00220004
               PIC X(0007).                                             00230001
           02  GV05-NSEQENS                                             00240004
               PIC S9(3) COMP-3.                                        00250001
           02  GV05-CAGENT                                              00260004
               PIC X(0002).                                             00270001
           02  GV05-DCREATION                                           00280004
               PIC X(0008).                                             00290001
           02  GV05-DMODIF                                              00300004
               PIC X(0008).                                             00310001
           02  GV05-DANNUL                                              00320004
               PIC X(0008).                                             00330001
           02  GV05-DSYST                                               00340004
               PIC S9(13) COMP-3.                                       00350004
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00360000
      *---------------------------------------------------------        00370000
      *   LISTE DES FLAGS DE LA TABLE RVGV0500                          00380005
      *---------------------------------------------------------        00390000
      *                                                                 00400000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0500-FLAGS.                                              00410005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NSOCIETE-F                                          00420004
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  GV05-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NLIEU-F                                             00440004
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  GV05-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NVENTE-F                                            00460004
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  GV05-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NDOSSIER-F                                          00480004
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  GV05-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-CDOSSIER-F                                          00500004
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  GV05-CDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-CTYPENT-F                                           00520004
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  GV05-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NENTITE-F                                           00540004
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  GV05-NENTITE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-NSEQENS-F                                           00560004
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  GV05-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-CAGENT-F                                            00580004
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  GV05-CAGENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-DCREATION-F                                         00600004
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  GV05-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-DMODIF-F                                            00620004
      *        PIC S9(4) COMP.                                          00630000
      *--                                                                       
           02  GV05-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-DANNUL-F                                            00640004
      *        PIC S9(4) COMP.                                          00650000
      *--                                                                       
           02  GV05-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV05-DSYST-F                                             00660004
      *        PIC S9(4) COMP.                                          00670004
      *--                                                                       
           02  GV05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00680000
                                                                                
