      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA VUE RVAV1000                                               
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RTAV10                           
      **********************************************************                
      *                                                                         
       01  RVAV1000.                                                            
           10 AV10-NSOCIETE        PIC X(3).                                    
           10 AV10-NLIEU           PIC X(3).                                    
           10 AV10-NAVOIRTOT       PIC X(12).                                   
           10 AV10-NIDENT          PIC X(20).                                   
           10 AV10-PMONTANT        PIC S9(5)V9(2) USAGE COMP-3.                 
           10 AV10-DEMIS           PIC X(8).                                    
           10 AV10-DFINVALID       PIC X(8).                                    
           10 AV10-NCONTRAT        PIC X(11).                                   
           10 AV10-NSOCUTIL        PIC X(3).                                    
           10 AV10-NLIEUUTIL       PIC X(3).                                    
           10 AV10-NIDENTUTIL      PIC X(20).                                   
           10 AV10-DUTIL           PIC X(8).                                    
           10 AV10-CANNULATION     PIC X(1).                                    
           10 AV10-DANNULATION     PIC X(8).                                    
           10 AV10-NSEQIDENT       PIC X(8).                                    
           10 AV10-NAVOIR          PIC X(7).                                    
           10 AV10-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAV1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV1000-FLAGS.                                                      
           10 AV10-NSOCIETE-F      PIC X(3).                                    
           10 AV10-NLIEU-F         PIC X(3).                                    
           10 AV10-NAVOIRTOT-F     PIC X(12).                                   
           10 AV10-NIDENT-F        PIC X(20).                                   
           10 AV10-PMONTANT-F      PIC S9(5)V9(2) USAGE COMP-3.                 
           10 AV10-DEMIS-F         PIC X(8).                                    
           10 AV10-DFINVALID-F     PIC X(8).                                    
           10 AV10-NCONTRAT-F      PIC X(11).                                   
           10 AV10-NSOCUTIL-F      PIC X(3).                                    
           10 AV10-NLIEUUTIL-F     PIC X(3).                                    
           10 AV10-NIDENTUTIL-F    PIC X(20).                                   
           10 AV10-DUTIL-F         PIC X(8).                                    
           10 AV10-CANNULATION-F   PIC X(1).                                    
           10 AV10-DANNULATION-F   PIC X(8).                                    
           10 AV10-NSEQIDENT-F     PIC X(8).                                    
           10 AV10-NAVOIR-F        PIC X(7).                                    
           10 AV10-DSYST-F         PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 17      *        
      ******************************************************************        
                                                                                
