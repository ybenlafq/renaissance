      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      **********************************************************                
      *   COPY DE LA TABLE RVPP0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPP0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPP0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPP0100.                                                            
      *}                                                                        
           02  PP01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  PP01-NLIEU                                                       
               PIC X(0003).                                                     
           02  PP01-DMOISPAYE                                                   
               PIC X(0006).                                                     
           02  PP01-CPRIME                                                      
               PIC X(0005).                                                     
           02  PP01-NRUB                                                        
               PIC X(0002).                                                     
           02  PP01-PMONTANTZIN                                                 
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP01-PMONTANTZ1                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP01-PMONTANTZ2                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP01-PMONTANTZ3                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP01-PRESULTAT                                                   
               PIC S9(11)V9(0002) COMP-3.                                       
           02  PP01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPP0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPP0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPP0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-DMOISPAYE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-DMOISPAYE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-CPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-CPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-NRUB-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-NRUB-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-PMONTANTZIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-PMONTANTZIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-PMONTANTZ1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-PMONTANTZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-PMONTANTZ2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-PMONTANTZ2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-PMONTANTZ3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-PMONTANTZ3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-PRESULTAT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-PRESULTAT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
