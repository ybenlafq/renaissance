      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * TD00 : ECRAN GENERAL                                            00000020
      ***************************************************************** 00000030
       01   ETD00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTD00L   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWTD00L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWTD00F   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWTD00I   PIC X.                                          00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPLIL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCAPPLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPLIF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCAPPLII  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOSSL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MTYPDOSSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPDOSSF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MTYPDOSSI      PIC X(5).                                  00000250
           02 MNOPTIONI OCCURS   15 TIMES .                             00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPTL  COMP PIC S9(4).                                 00000270
      *--                                                                       
             03 MNOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNOPTF  PIC X.                                          00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNOPTI  PIC X(2).                                       00000300
           02 MLOPTIONI OCCURS   15 TIMES .                             00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPTL  COMP PIC S9(4).                                 00000320
      *--                                                                       
             03 MLOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLOPTF  PIC X.                                          00000330
             03 FILLER  PIC X(4).                                       00000340
             03 MLOPTI  PIC X(20).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MZONCMDI  PIC X(2).                                       00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNSOCIETEI     PIC X(3).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLSOCIETEI     PIC X(20).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNLIEUI   PIC X(3).                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLLIEUI   PIC X(20).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNVENTEI  PIC X(7).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCTYPEI   PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCMARQI   PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDOSSIERL     COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MCDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCDOSSIERF     PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCDOSSIERI     PIC X(5).                                  00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODRECHL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MCODRECHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCODRECHF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MCODRECHI      PIC X(5).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVALRECHL      COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MVALRECHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MVALRECHF      PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MVALRECHI      PIC X(30).                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MDATDEBI  PIC X(10).                                      00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MDATFINI  PIC X(10).                                      00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLIBERRI  PIC X(78).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MCODTRAI  PIC X(4).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MNETNAMI  PIC X(8).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MSCREENI  PIC X(4).                                       00001070
      ***************************************************************** 00001080
      * TD00 : ECRAN GENERAL                                            00001090
      ***************************************************************** 00001100
       01   ETD00O REDEFINES ETD00I.                                    00001110
           02 FILLER    PIC X(12).                                      00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MDATJOUA  PIC X.                                          00001140
           02 MDATJOUC  PIC X.                                          00001150
           02 MDATJOUP  PIC X.                                          00001160
           02 MDATJOUH  PIC X.                                          00001170
           02 MDATJOUV  PIC X.                                          00001180
           02 MDATJOUO  PIC X(10).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MTIMJOUA  PIC X.                                          00001210
           02 MTIMJOUC  PIC X.                                          00001220
           02 MTIMJOUP  PIC X.                                          00001230
           02 MTIMJOUH  PIC X.                                          00001240
           02 MTIMJOUV  PIC X.                                          00001250
           02 MTIMJOUO  PIC X(5).                                       00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MWTD00A   PIC X.                                          00001280
           02 MWTD00C   PIC X.                                          00001290
           02 MWTD00P   PIC X.                                          00001300
           02 MWTD00H   PIC X.                                          00001310
           02 MWTD00V   PIC X.                                          00001320
           02 MWTD00O   PIC X.                                          00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCAPPLIA  PIC X.                                          00001350
           02 MCAPPLIC  PIC X.                                          00001360
           02 MCAPPLIP  PIC X.                                          00001370
           02 MCAPPLIH  PIC X.                                          00001380
           02 MCAPPLIV  PIC X.                                          00001390
           02 MCAPPLIO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTYPDOSSA      PIC X.                                     00001420
           02 MTYPDOSSC PIC X.                                          00001430
           02 MTYPDOSSP PIC X.                                          00001440
           02 MTYPDOSSH PIC X.                                          00001450
           02 MTYPDOSSV PIC X.                                          00001460
           02 MTYPDOSSO      PIC X(5).                                  00001470
           02 MNOPTIONO OCCURS   15 TIMES .                             00001480
             03 FILLER       PIC X(2).                                  00001490
             03 MNOPTA  PIC X.                                          00001500
             03 MNOPTC  PIC X.                                          00001510
             03 MNOPTP  PIC X.                                          00001520
             03 MNOPTH  PIC X.                                          00001530
             03 MNOPTV  PIC X.                                          00001540
             03 MNOPTO  PIC X(2).                                       00001550
           02 MLOPTIONO OCCURS   15 TIMES .                             00001560
             03 FILLER       PIC X(2).                                  00001570
             03 MLOPTA  PIC X.                                          00001580
             03 MLOPTC  PIC X.                                          00001590
             03 MLOPTP  PIC X.                                          00001600
             03 MLOPTH  PIC X.                                          00001610
             03 MLOPTV  PIC X.                                          00001620
             03 MLOPTO  PIC X(20).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MZONCMDA  PIC X.                                          00001650
           02 MZONCMDC  PIC X.                                          00001660
           02 MZONCMDP  PIC X.                                          00001670
           02 MZONCMDH  PIC X.                                          00001680
           02 MZONCMDV  PIC X.                                          00001690
           02 MZONCMDO  PIC X(2).                                       00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MNSOCIETEA     PIC X.                                     00001720
           02 MNSOCIETEC     PIC X.                                     00001730
           02 MNSOCIETEP     PIC X.                                     00001740
           02 MNSOCIETEH     PIC X.                                     00001750
           02 MNSOCIETEV     PIC X.                                     00001760
           02 MNSOCIETEO     PIC X(3).                                  00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MLSOCIETEA     PIC X.                                     00001790
           02 MLSOCIETEC     PIC X.                                     00001800
           02 MLSOCIETEP     PIC X.                                     00001810
           02 MLSOCIETEH     PIC X.                                     00001820
           02 MLSOCIETEV     PIC X.                                     00001830
           02 MLSOCIETEO     PIC X(20).                                 00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNLIEUA   PIC X.                                          00001860
           02 MNLIEUC   PIC X.                                          00001870
           02 MNLIEUP   PIC X.                                          00001880
           02 MNLIEUH   PIC X.                                          00001890
           02 MNLIEUV   PIC X.                                          00001900
           02 MNLIEUO   PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MLLIEUA   PIC X.                                          00001930
           02 MLLIEUC   PIC X.                                          00001940
           02 MLLIEUP   PIC X.                                          00001950
           02 MLLIEUH   PIC X.                                          00001960
           02 MLLIEUV   PIC X.                                          00001970
           02 MLLIEUO   PIC X(20).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNVENTEA  PIC X.                                          00002000
           02 MNVENTEC  PIC X.                                          00002010
           02 MNVENTEP  PIC X.                                          00002020
           02 MNVENTEH  PIC X.                                          00002030
           02 MNVENTEV  PIC X.                                          00002040
           02 MNVENTEO  PIC X(7).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MCTYPEA   PIC X.                                          00002070
           02 MCTYPEC   PIC X.                                          00002080
           02 MCTYPEP   PIC X.                                          00002090
           02 MCTYPEH   PIC X.                                          00002100
           02 MCTYPEV   PIC X.                                          00002110
           02 MCTYPEO   PIC X(5).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MCMARQA   PIC X.                                          00002140
           02 MCMARQC   PIC X.                                          00002150
           02 MCMARQP   PIC X.                                          00002160
           02 MCMARQH   PIC X.                                          00002170
           02 MCMARQV   PIC X.                                          00002180
           02 MCMARQO   PIC X(5).                                       00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MCDOSSIERA     PIC X.                                     00002210
           02 MCDOSSIERC     PIC X.                                     00002220
           02 MCDOSSIERP     PIC X.                                     00002230
           02 MCDOSSIERH     PIC X.                                     00002240
           02 MCDOSSIERV     PIC X.                                     00002250
           02 MCDOSSIERO     PIC X(5).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCODRECHA      PIC X.                                     00002280
           02 MCODRECHC PIC X.                                          00002290
           02 MCODRECHP PIC X.                                          00002300
           02 MCODRECHH PIC X.                                          00002310
           02 MCODRECHV PIC X.                                          00002320
           02 MCODRECHO      PIC X(5).                                  00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MVALRECHA      PIC X.                                     00002350
           02 MVALRECHC PIC X.                                          00002360
           02 MVALRECHP PIC X.                                          00002370
           02 MVALRECHH PIC X.                                          00002380
           02 MVALRECHV PIC X.                                          00002390
           02 MVALRECHO      PIC X(30).                                 00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MDATDEBA  PIC X.                                          00002420
           02 MDATDEBC  PIC X.                                          00002430
           02 MDATDEBP  PIC X.                                          00002440
           02 MDATDEBH  PIC X.                                          00002450
           02 MDATDEBV  PIC X.                                          00002460
           02 MDATDEBO  PIC X(10).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MDATFINA  PIC X.                                          00002490
           02 MDATFINC  PIC X.                                          00002500
           02 MDATFINP  PIC X.                                          00002510
           02 MDATFINH  PIC X.                                          00002520
           02 MDATFINV  PIC X.                                          00002530
           02 MDATFINO  PIC X(10).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MLIBERRA  PIC X.                                          00002560
           02 MLIBERRC  PIC X.                                          00002570
           02 MLIBERRP  PIC X.                                          00002580
           02 MLIBERRH  PIC X.                                          00002590
           02 MLIBERRV  PIC X.                                          00002600
           02 MLIBERRO  PIC X(78).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MCODTRAA  PIC X.                                          00002630
           02 MCODTRAC  PIC X.                                          00002640
           02 MCODTRAP  PIC X.                                          00002650
           02 MCODTRAH  PIC X.                                          00002660
           02 MCODTRAV  PIC X.                                          00002670
           02 MCODTRAO  PIC X(4).                                       00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MCICSA    PIC X.                                          00002700
           02 MCICSC    PIC X.                                          00002710
           02 MCICSP    PIC X.                                          00002720
           02 MCICSH    PIC X.                                          00002730
           02 MCICSV    PIC X.                                          00002740
           02 MCICSO    PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MSCREENA  PIC X.                                          00002840
           02 MSCREENC  PIC X.                                          00002850
           02 MSCREENP  PIC X.                                          00002860
           02 MSCREENH  PIC X.                                          00002870
           02 MSCREENV  PIC X.                                          00002880
           02 MSCREENO  PIC X(4).                                       00002890
                                                                                
