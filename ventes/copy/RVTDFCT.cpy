      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDFCT CODE FAMILLE DE CONTROLE         *        
      *----------------------------------------------------------------*        
       01  RVTDFCT.                                                             
           05  TDFCT-CTABLEG2    PIC X(15).                                     
           05  TDFCT-CTABLEG2-REDEF REDEFINES TDFCT-CTABLEG2.                   
               10  TDFCT-FAMCTR          PIC X(05).                             
           05  TDFCT-WTABLEG     PIC X(80).                                     
           05  TDFCT-WTABLEG-REDEF  REDEFINES TDFCT-WTABLEG.                    
               10  TDFCT-LFAMCTR         PIC X(20).                             
               10  TDFCT-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDFCT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDFCT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDFCT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDFCT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDFCT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
