      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVMO0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVMO0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMO0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMO0100.                                                            
      *}                                                                        
           02  MO01-TYPOFFRE                                                    
               PIC X(0005).                                                     
           02  MO01-NSOC                                                        
               PIC X(0003).                                                     
           02  MO01-NLIEU                                                       
               PIC X(0003).                                                     
           02  MO01-NVENTE                                                      
               PIC X(0007).                                                     
           02  MO01-CPRESTA                                                     
               PIC X(0005).                                                     
           02  MO01-MONTTOT                                                     
               PIC S9(07)V9(0002) COMP-3.                                       
           02  MO01-DVENTE                                                      
               PIC X(0008).                                                     
           02  MO01-DENGAG                                                      
               PIC S9(03) COMP-3.                                               
           02  MO01-NBMOIS                                                      
               PIC S9(03) COMP-3.                                               
           02  MO01-MTMENSUEL                                                   
               PIC S9(07)V9(0002) COMP-3.                                       
           02  MO01-MTDEVERSE                                                   
               PIC S9(07)V9(0002) COMP-3.                                       
           02  MO01-PRORATA                                                     
               PIC S9(07)V9(0002) COMP-3.                                       
           02  MO01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVMO0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMO0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMO0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-TYPOFFRE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-TYPOFFRE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-CPRESTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-CPRESTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-MONTTOT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-MONTTOT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-DENGAG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-DENGAG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-NBMOIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-NBMOIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-MTMENSUEL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-MTMENSUEL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-MTDEVERSE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-MTDEVERSE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-PRORATA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-PRORATA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  MO01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  MO01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
