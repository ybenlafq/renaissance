      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      * COBOL DECLARATION FOR TABLE PNEM.RVTD0200                      *        
      *----------------------------------------------------------------*        
       01  RVTD0200.                                                            
           10 TD02-NSOCIETE        PIC X(3).                                    
           10 TD02-NLIEU           PIC X(3).                                    
           10 TD02-NVENTE          PIC X(7).                                    
           10 TD02-NDOSSIER        PIC S9(3)V USAGE COMP-3.                     
           10 TD02-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
           10 TD02-NSEQ            PIC S9(3)V USAGE COMP-3.                     
           10 TD02-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 TD02-CTYPENREG       PIC X(1).                                    
           10 TD02-NCODICGRP       PIC X(7).                                    
           10 TD02-NCODIC          PIC X(7).                                    
           10 TD02-CENREG          PIC X(5).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
                                                                                
