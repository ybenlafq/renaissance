      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *................................................................         
      * TS REGLEMENTS => A UNE LIGNE REGLEMENT CORRESPOND UN ITEM TS  *         
      *................................................................         
      * LONGUEUR TOTALE = 469                                         *         
      *                                                               *         
       01  TS-LONG                 PIC S9(4)  VALUE 576.                        
      *                                                               *         
       01  TS-DATA.                                                             
           05 TS-LIGNE   OCCURS 7.                                              
              10 TS-ETAT-LIGNE     PIC X(1).                                    
                 88 TS-ETAT-ACTIVE     VALUE 'A'.                               
                 88 TS-ETAT-INACTIVE   VALUE 'I'.                               
              10 TS-ACTION-LIGNE   PIC X(1).                                    
                 88 TS-ACTION-INSERT   VALUE 'I'.                               
                 88 TS-ACTION-UPDATE   VALUE 'U'.                               
                 88 TS-ACTION-DELETE   VALUE 'D'.                               
              10 TS-DSAISIE        PIC X(8).                                    
              10 TS-NSEQ           PIC X(2).                                    
              10 TS-CMODPAIMT      PIC X(5).                                    
              10 TS-LMODPAIMT      PIC X(10).                                   
              10 TS-PREGLTVTE      PIC S9(7)V99.                                
              10 TS-NREGLTVTE      PIC X(7).                                    
              10 TS-DREMISE        PIC X(7).                                    
              10 TS-DREGLTVTE      PIC X(8).                                    
              10 TS-WREGLTVTE      PIC X(1).                                    
              10 TS-NLIEUTRAN      PIC X(16).                                   
              10 TS-DCOMPTA        PIC X(8).                                    
      * TS-ETAT-LIGNE ----------> ACTIVE / INACTIVE   A OU I                    
      * TS-ACTION-LIGNE --------> UPDATE / INSERT / DELETE   U, I OU D          
                                                                                
