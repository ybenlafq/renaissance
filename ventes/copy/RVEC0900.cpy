      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE RTEC09                             *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC0900.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC0900.                                                            
      *}                                                                        
           10 EC09-NSOCIETE             PIC X(3).                               
           10 EC09-NLIEU                PIC X(3).                               
           10 EC09-NVENTE               PIC X(7).                               
           10 EC09-DDELIV               PIC X(10).                              
           10 EC09-CMODDEL              PIC X(4).                               
           10 EC09-TRACK-NUMBER         PIC X(26).                              
           10 EC09-INDEX-KEY            PIC X(2).                               
           10 EC09-STATE                PIC X(10).                              
           10 EC09-EXT-ID               PIC X(50).                              
           10 EC09-NSEQNQ               PIC S9(5)V USAGE COMP-3.                
           10 EC09-NENTITE              PIC X(7).                               
           10 EC09-IDMESSAGE            PIC X(5).                               
           10 EC09-SENDING-DATE         PIC X(26).                              
           10 EC09-TIMEST-CREAT         PIC X(26).                              
           10 EC09-TIMEST-UPDATE        PIC X(26).                              
           10 EC09-NGSM                 PIC X(10).                              
           10 EC09-TELDOM               PIC X(10).                              
           10 EC09-TELBUR               PIC X(10).                              
           10 EC09-CTITRENOM            PIC X(5).                               
           10 EC09-LNOM                 PIC X(25).                              
           10 EC09-LPRENOM              PIC X(15).                              
           10 EC09-IDCLIENT             PIC X(8).                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           10 EC09-ADD-TEXT             PIC X(255).                             
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 23      *        
      ******************************************************************        
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC0900-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC0900-FLAGS.                                                      
      *}                                                                        
           10 EC09-NSOCIETE-F           PIC X(3).                               
           10 EC09-NLIEU-F              PIC X(3).                               
           10 EC09-NVENTE-F             PIC X(7).                               
           10 EC09-DDELIV-F             PIC X(10).                              
           10 EC09-CMODDEL-F            PIC X(4).                               
           10 EC09-TRACK-NUMBER-F       PIC X(26).                              
           10 EC09-INDEX-KEY-F          PIC X(2).                               
           10 EC09-STATE-F              PIC X(10).                              
           10 EC09-EXT-ID-F             PIC X(50).                              
           10 EC09-NSEQNQ-F             PIC S9(5)V USAGE COMP-3.                
           10 EC09-NENTITE-F            PIC X(7).                               
           10 EC09-IDMESSAGE-F          PIC X(5).                               
           10 EC09-SENDING-DATE-F       PIC X(26).                              
           10 EC09-TIMEST-CREAT-F       PIC X(26).                              
           10 EC09-TIMEST-UPDATE-F      PIC X(26).                              
           10 EC09-NGSM-F               PIC X(10).                              
           10 EC09-TELDOM-F             PIC X(10).                              
           10 EC09-TELBUR-F             PIC X(10).                              
           10 EC09-CTITRENOM-F          PIC X(5).                               
           10 EC09-LNOM-F               PIC X(25).                              
           10 EC09-LPRENOM-F            PIC X(15).                              
           10 EC09-IDCLIENT-F           PIC X(8).                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
           10 EC09-ADD-TEXT-F           PIC X(255).                             
                                                                                
