      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC0204                      *        
      ******************************************************************        
       01  RVEC0205.                                                            
           10 EC02-NSOCIETE        PIC X(3).                                    
           10 EC02-NLIEU           PIC X(3).                                    
           10 EC02-NVENTE          PIC X(7).                                    
           10 EC02-DVENTE          PIC X(8).                                    
           10 EC02-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC02-WTPCMD          PIC X(5).                                    
           10 EC02-CPAYM           PIC X(5).                                    
           10 EC02-WPAYM           PIC X(1).                                    
           10 EC02-MTPAYM          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 EC02-LCADEAU.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC02-LCADEAU-LEN  PIC S9(4) USAGE COMP.                        
      *--                                                                       
              49 EC02-LCADEAU-LEN  PIC S9(4) COMP-5.                            
      *}                                                                        
              49 EC02-LCADEAU-TEXT  PIC X(255).                                 
           10 EC02-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC02-DVALP           PIC X(8).                                    
           10 EC02-WVALP           PIC X(1).                                    
           10 EC02-WPAYDEM         PIC X(1).                                    
           10 EC02-CCPSWD          PIC X(10).                                   
           10 EC02-WCC             PIC X(1).                                    
           10 EC02-CCEVENT         PIC X(4).                                    
           10 EC02-DATPREPA        PIC X(8).                                    
           10 EC02-HEURPREPA       PIC X(4).                                    
      *   POUR SAVOIR SI LA VENTE A ETE PURGEE PAR LE BATCH DU SOIR =P          
           10 EC02-ETAT            PIC X(15).                                   
           10 EC02-CANAL           PIC X(10).                                   
                                                                                
