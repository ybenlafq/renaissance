      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE500      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,05,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE500.                                                        
            05 NOMETAT-IRE500           PIC X(6) VALUE 'IRE500'.                
            05 RUPTURES-IRE500.                                                 
           10 IRE500-NSOCIETE           PIC X(03).                      007  003
           10 IRE500-TYPE-ENR           PIC X(01).                      010  001
           10 IRE500-TLM-ELA            PIC X(05).                      011  005
           10 IRE500-CGRPMAG            PIC X(02).                      016  002
           10 IRE500-NLIEU              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE500-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE500-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE500.                                                   
           10 IRE500-LIBELLE            PIC X(20).                      023  020
           10 IRE500-LMOISENC           PIC X(09).                      043  009
           10 IRE500-LMOIS01            PIC X(09).                      052  009
           10 IRE500-LMOIS02            PIC X(09).                      061  009
           10 IRE500-LMOIS03            PIC X(09).                      070  009
           10 IRE500-LMOIS04            PIC X(09).                      079  009
           10 IRE500-LMOIS05            PIC X(09).                      088  009
           10 IRE500-LMOIS06            PIC X(09).                      097  009
           10 IRE500-LMOIS07            PIC X(09).                      106  009
           10 IRE500-LMOIS08            PIC X(09).                      115  009
           10 IRE500-LMOIS09            PIC X(09).                      124  009
           10 IRE500-LMOIS10            PIC X(09).                      133  009
           10 IRE500-LMOIS11            PIC X(09).                      142  009
           10 IRE500-LMOIS12            PIC X(09).                      151  009
           10 IRE500-CA01               PIC S9(13)V9(2) COMP-3.         160  008
           10 IRE500-CA02               PIC S9(13)V9(2) COMP-3.         168  008
           10 IRE500-CA03               PIC S9(13)V9(2) COMP-3.         176  008
           10 IRE500-CA04               PIC S9(13)V9(2) COMP-3.         184  008
           10 IRE500-CA05               PIC S9(13)V9(2) COMP-3.         192  008
           10 IRE500-CA06               PIC S9(13)V9(2) COMP-3.         200  008
           10 IRE500-CA07               PIC S9(13)V9(2) COMP-3.         208  008
           10 IRE500-CA08               PIC S9(13)V9(2) COMP-3.         216  008
           10 IRE500-CA09               PIC S9(13)V9(2) COMP-3.         224  008
           10 IRE500-CA10               PIC S9(13)V9(2) COMP-3.         232  008
           10 IRE500-CA11               PIC S9(13)V9(2) COMP-3.         240  008
           10 IRE500-CA12               PIC S9(13)V9(2) COMP-3.         248  008
           10 IRE500-REM01              PIC S9(13)V9(2) COMP-3.         256  008
           10 IRE500-REM02              PIC S9(13)V9(2) COMP-3.         264  008
           10 IRE500-REM03              PIC S9(13)V9(2) COMP-3.         272  008
           10 IRE500-REM04              PIC S9(13)V9(2) COMP-3.         280  008
           10 IRE500-REM05              PIC S9(13)V9(2) COMP-3.         288  008
           10 IRE500-REM06              PIC S9(13)V9(2) COMP-3.         296  008
           10 IRE500-REM07              PIC S9(13)V9(2) COMP-3.         304  008
           10 IRE500-REM08              PIC S9(13)V9(2) COMP-3.         312  008
           10 IRE500-REM09              PIC S9(13)V9(2) COMP-3.         320  008
           10 IRE500-REM10              PIC S9(13)V9(2) COMP-3.         328  008
           10 IRE500-REM11              PIC S9(13)V9(2) COMP-3.         336  008
           10 IRE500-REM12              PIC S9(13)V9(2) COMP-3.         344  008
            05 FILLER                      PIC X(161).                          
                                                                                
