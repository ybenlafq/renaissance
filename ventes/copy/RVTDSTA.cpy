      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDSTA �CRAN DE STAT ETD05              *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVTDSTA.                                                             
           05  TDSTA-CTABLEG2    PIC X(15).                                     
           05  TDSTA-CTABLEG2-REDEF REDEFINES TDSTA-CTABLEG2.                   
               10  TDSTA-TYPDOSS         PIC X(05).                             
               10  TDSTA-CSTAT           PIC X(05).                             
           05  TDSTA-WTABLEG     PIC X(80).                                     
           05  TDSTA-WTABLEG-REDEF  REDEFINES TDSTA-WTABLEG.                    
               10  TDSTA-LSTAT           PIC X(34).                             
               10  TDSTA-NAFFICHE        PIC X(02).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDSTA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDSTA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDSTA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDSTA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDSTA-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
