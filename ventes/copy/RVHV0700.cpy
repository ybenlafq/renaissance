      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV0700                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0700                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV0700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV0700.                                                            
      *}                                                                        
           02  HV07-CETAT                                                       
               PIC X(0010).                                                     
           02  HV07-CFAM                                                        
               PIC X(0005).                                                     
           02  HV07-WPRIXRENTA                                                  
               PIC X(0001).                                                     
           02  HV07-PBORNE01                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE02                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE03                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE04                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE05                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE06                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE07                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE08                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE09                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-PBORNE10                                                    
               PIC S9(5) COMP-3.                                                
           02  HV07-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV0700                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV0700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV0700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-WPRIXRENTA-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-WPRIXRENTA-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE01-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE01-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE02-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE02-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE03-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE03-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE04-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE04-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE05-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE05-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE06-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE06-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE07-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE07-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE08-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE08-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE09-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE09-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-PBORNE10-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV07-PBORNE10-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV07-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  HV07-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
