      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *-------------------------------------------------------------            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-EC10-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-EC10-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 1   PIC X(1).                               
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
           02 COMM-EC10.                                                        
              03  COMM-EC10-NBP       PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC10-PAGEA     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC10-RSTAT     PIC X(01).                                
              03  COMM-EC10-ROPER     PIC X(04).                                
              03  COMM-EC10-RNCDEWC   PIC X(08).                                
              03  COMM-EC10-RNSOCIETE PIC X(03).                                
              03  COMM-EC10-RNLIEU    PIC X(03).                                
              03  COMM-EC10-RNVENTE   PIC X(07).                                
              03  COMM-EC10-RNOM      PIC X(24).                                
              03  COMM-EC10-RDCRE     PIC X(08).                                
              03  COMM-EC10-CPT-E     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC10-CPT-R     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC10-CPT-I     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC10-CPT-T     PIC S9(5) PACKED-DECIMAL.                 
      *       03  COMM-EC10-CPT-Z     PIC S9(5) PACKED-DECIMAL.                 
           02 COMM-EC11.                                                        
              03  COMM-EC11-NBP       PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC11-PAGEA     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC11-STAT      PIC X(01).                                
              03  COMM-EC11-OPER      PIC X(05).                                
              03  COMM-EC11-NCDEWC    PIC S9(15) PACKED-DECIMAL.                
              03  COMM-EC11-NSOCIETE  PIC X(03).                                
              03  COMM-EC11-NLIEU     PIC X(03).                                
              03  COMM-EC11-NVENTE    PIC X(07).                                
              03  COMM-EC11-NOM       PIC X(24).                                
              03  COMM-EC11-DCRE      PIC X(08).                                
              03  COMM-EC11-HCRE      PIC X(04).                                
      *    02 COMM-EC12.                                                        
      *       03  COMM-EC12-TIMESTP   PIC X(26).                                
      *       03  COMM-EC12-I-TS      PIC  9(5).                                
                                                                                
