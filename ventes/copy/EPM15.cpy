      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPF22   EPF22                                              00000020
      ***************************************************************** 00000030
       01   EPM15I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIBELL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIBELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIBELF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIBELI  PIC X(20).                                      00000250
           02 MNLIGNE1I OCCURS   14 TIMES .                             00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MNLIEUI      PIC X(3).                                  00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MLIBELI      PIC X(20).                                 00000340
           02 MNLIGNE2I OCCURS   14 TIMES .                             00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATUTL     COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MSTATUTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSTATUTF     PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MSTATUTI     PIC X.                                     00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEL  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MDATEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDATEF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MDATEI  PIC X(8).                                       00000430
           02 MNLIGNE3I OCCURS   14 TIMES .                             00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEML      COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MDATEML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDATEMF      PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MDATEMI      PIC X(8).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTIMEL  COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MTIMEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTIMEF  PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MTIMEI  PIC X(5).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(79).                                      00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCODTRAI  PIC X(4).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MCICSI    PIC X(5).                                       00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MNETNAMI  PIC X(8).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MSCREENI  PIC X(4).                                       00000720
      ***************************************************************** 00000730
      * SDF: EPF22   EPF22                                              00000740
      ***************************************************************** 00000750
       01   EPM15O REDEFINES EPM15I.                                    00000760
           02 FILLER    PIC X(12).                                      00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MDATJOUA  PIC X.                                          00000790
           02 MDATJOUC  PIC X.                                          00000800
           02 MDATJOUP  PIC X.                                          00000810
           02 MDATJOUH  PIC X.                                          00000820
           02 MDATJOUV  PIC X.                                          00000830
           02 MDATJOUO  PIC X(10).                                      00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MTIMJOUA  PIC X.                                          00000860
           02 MTIMJOUC  PIC X.                                          00000870
           02 MTIMJOUP  PIC X.                                          00000880
           02 MTIMJOUH  PIC X.                                          00000890
           02 MTIMJOUV  PIC X.                                          00000900
           02 MTIMJOUO  PIC X(5).                                       00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MPAGEA    PIC X.                                          00000930
           02 MPAGEC    PIC X.                                          00000940
           02 MPAGEP    PIC X.                                          00000950
           02 MPAGEH    PIC X.                                          00000960
           02 MPAGEV    PIC X.                                          00000970
           02 MPAGEO    PIC X(3).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MNSOCA    PIC X.                                          00001000
           02 MNSOCC    PIC X.                                          00001010
           02 MNSOCP    PIC X.                                          00001020
           02 MNSOCH    PIC X.                                          00001030
           02 MNSOCV    PIC X.                                          00001040
           02 MNSOCO    PIC X(3).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MNLIBELA  PIC X.                                          00001070
           02 MNLIBELC  PIC X.                                          00001080
           02 MNLIBELP  PIC X.                                          00001090
           02 MNLIBELH  PIC X.                                          00001100
           02 MNLIBELV  PIC X.                                          00001110
           02 MNLIBELO  PIC X(20).                                      00001120
           02 MNLIGNE1O OCCURS   14 TIMES .                             00001130
             03 FILLER       PIC X(2).                                  00001140
             03 MNLIEUA      PIC X.                                     00001150
             03 MNLIEUC PIC X.                                          00001160
             03 MNLIEUP PIC X.                                          00001170
             03 MNLIEUH PIC X.                                          00001180
             03 MNLIEUV PIC X.                                          00001190
             03 MNLIEUO      PIC X(3).                                  00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MLIBELA      PIC X.                                     00001220
             03 MLIBELC PIC X.                                          00001230
             03 MLIBELP PIC X.                                          00001240
             03 MLIBELH PIC X.                                          00001250
             03 MLIBELV PIC X.                                          00001260
             03 MLIBELO      PIC X(20).                                 00001270
           02 MNLIGNE2O OCCURS   14 TIMES .                             00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MSTATUTA     PIC X.                                     00001300
             03 MSTATUTC     PIC X.                                     00001310
             03 MSTATUTP     PIC X.                                     00001320
             03 MSTATUTH     PIC X.                                     00001330
             03 MSTATUTV     PIC X.                                     00001340
             03 MSTATUTO     PIC X.                                     00001350
             03 FILLER       PIC X(2).                                  00001360
             03 MDATEA  PIC X.                                          00001370
             03 MDATEC  PIC X.                                          00001380
             03 MDATEP  PIC X.                                          00001390
             03 MDATEH  PIC X.                                          00001400
             03 MDATEV  PIC X.                                          00001410
             03 MDATEO  PIC X(8).                                       00001420
           02 MNLIGNE3O OCCURS   14 TIMES .                             00001430
             03 FILLER       PIC X(2).                                  00001440
             03 MDATEMA      PIC X.                                     00001450
             03 MDATEMC PIC X.                                          00001460
             03 MDATEMP PIC X.                                          00001470
             03 MDATEMH PIC X.                                          00001480
             03 MDATEMV PIC X.                                          00001490
             03 MDATEMO      PIC X(8).                                  00001500
             03 FILLER       PIC X(2).                                  00001510
             03 MTIMEA  PIC X.                                          00001520
             03 MTIMEC  PIC X.                                          00001530
             03 MTIMEP  PIC X.                                          00001540
             03 MTIMEH  PIC X.                                          00001550
             03 MTIMEV  PIC X.                                          00001560
             03 MTIMEO  PIC X(5).                                       00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MLIBERRA  PIC X.                                          00001590
           02 MLIBERRC  PIC X.                                          00001600
           02 MLIBERRP  PIC X.                                          00001610
           02 MLIBERRH  PIC X.                                          00001620
           02 MLIBERRV  PIC X.                                          00001630
           02 MLIBERRO  PIC X(79).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MCODTRAA  PIC X.                                          00001660
           02 MCODTRAC  PIC X.                                          00001670
           02 MCODTRAP  PIC X.                                          00001680
           02 MCODTRAH  PIC X.                                          00001690
           02 MCODTRAV  PIC X.                                          00001700
           02 MCODTRAO  PIC X(4).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCICSA    PIC X.                                          00001730
           02 MCICSC    PIC X.                                          00001740
           02 MCICSP    PIC X.                                          00001750
           02 MCICSH    PIC X.                                          00001760
           02 MCICSV    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNETNAMA  PIC X.                                          00001800
           02 MNETNAMC  PIC X.                                          00001810
           02 MNETNAMP  PIC X.                                          00001820
           02 MNETNAMH  PIC X.                                          00001830
           02 MNETNAMV  PIC X.                                          00001840
           02 MNETNAMO  PIC X(8).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MSCREENA  PIC X.                                          00001870
           02 MSCREENC  PIC X.                                          00001880
           02 MSCREENP  PIC X.                                          00001890
           02 MSCREENH  PIC X.                                          00001900
           02 MSCREENV  PIC X.                                          00001910
           02 MSCREENO  PIC X(4).                                       00001920
                                                                                
