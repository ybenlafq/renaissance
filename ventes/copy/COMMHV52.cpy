      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          01 COMM-HV52-DONNEES.                                         00000010
             10 COMM-HV52-NB-POSTES               PIC S9(09) COMP-3.    00000070
             10 COMM-HV52-CODRET                  PIC 9(03) COMP-3.     00000070
                88 COMM-HV52-ERREUR-MANIP         VALUE 999.            00000070
             10 COMM-HV52-LIBERR                  PIC X(25).            00000070
             10 COMM-HV52-CRITERES.                                     00000020
                20 COMM-HV52-NSOCIETE             PIC X(03).            00000020
                20 COMM-HV52-NLIEU                PIC X(03).            00000030
                20 COMM-HV52-DVENTE               PIC X(08).            00000020
                20 COMM-HV52-LNOM                 PIC X(25).            00000020
                20 COMM-HV52-CMARQ                PIC X(05).            00000040
                20 COMM-HV52-CFAM                 PIC X(05).            00000030
                20 COMM-HV52-DDEBUT               PIC X(08).            00000020
                20 COMM-HV52-DFIN                 PIC X(08).            00000020
                20 COMM-HV52-NCAISSE              PIC X(03).            00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         20 COMM-HV52-MONTANT              PIC S9(7)V99 COMP.    00000030
      *--                                                                       
                20 COMM-HV52-MONTANT              PIC S9(7)V99 COMP-5.          
      *}                                                                        
                20 COMM-HV52-TPAIE                PIC X(01).            00000040
                20 COMM-HV52-TTRANS               PIC X(01).            00000030
                20 COMM-HV52-NCODIC               PIC X(07).            00000030
JD              20 COMM-HV52-CDEVISE              PIC X(01).            00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JD    *         20 COMM-HV52-MTREGLEM             PIC S9(7)V99 COMP.    00000030
      *--                                                                       
                20 COMM-HV52-MTREGLEM             PIC S9(7)V99 COMP-5.          
      *}                                                                        
             10 COMM-HV52-TYPE-RECHERCHE          PIC X.                00000070
                88 COMM-HV52-PAS-RECH                  VALUE ' '.               
                88 COMM-HV52-RECH-DVENTE               VALUE '1'.               
                88 COMM-HV52-RECH-NOM-MARQUE           VALUE '2'.               
                88 COMM-HV52-RECH-NOM-FAMILLE          VALUE '3'.               
                88 COMM-HV52-RECH-NOM-MARQ-FAM         VALUE '4'.               
                88 COMM-HV52-RECH-NOM               VALUE '2' '3' '4'.          
                88 COMM-HV52-RECH-NTRANS               VALUE 'A'.               
                88 COMM-HV52-RECH-TTRANS               VALUE 'B'.               
                88 COMM-HV52-RECH-TTRANS-MONTANT       VALUE 'C'.               
                88 COMM-HV52-RECH-TPAIE-MONTANT        VALUE 'D'.               
                88 COMM-HV52-RECH-NCODIC               VALUE 'E'.               
JD              88 COMM-HV52-RECH-TPAIE-CDEV           VALUE 'F'.               
JD              88 COMM-HV52-RECH-TPAI-CDEV-PREGL      VALUE 'G'.               
                                                                                
