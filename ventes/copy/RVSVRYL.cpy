      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SVRYL SVRYL ARBORESCENCE DES RAYONS    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVRYL.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVRYL.                                                             
      *}                                                                        
           05  SVRYL-CTABLEG2    PIC X(15).                                     
           05  SVRYL-CTABLEG2-REDEF REDEFINES SVRYL-CTABLEG2.                   
               10  SVRYL-APPLICAT        PIC X(08).                             
               10  SVRYL-CRAYCIB         PIC X(05).                             
               10  SVRYL-NSEQED          PIC X(02).                             
           05  SVRYL-WTABLEG     PIC X(80).                                     
           05  SVRYL-WTABLEG-REDEF  REDEFINES SVRYL-WTABLEG.                    
               10  SVRYL-CRAYMGIF        PIC X(05).                             
               10  SVRYL-COMMENT         PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVRYL-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVRYL-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVRYL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SVRYL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVRYL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SVRYL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
