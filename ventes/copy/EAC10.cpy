      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * R�sultats par Soci�t�                                           00000020
      ***************************************************************** 00000030
       01   EAC10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPAGEAL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTPAGEAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPAGEAF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTPAGEAI  PIC X(4).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MPAGEAI   PIC X(2).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MTNBPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBPF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MTNBPI    PIC X(2).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNBPI     PIC X(2).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDDEBUTI  PIC X(10).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MJDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJDEBUTF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MJDEBUTI  PIC X(10).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MDFINI    PIC X(10).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJFINL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MJFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJFINF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MJFINI    PIC X(10).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE1L     COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MTE1L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE1F     PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MTE1I     PIC X(28).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDDEBUTL      COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MEDDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEDDEBUTF      PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MEDDEBUTI      PIC X(10).                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJDEBUTL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MEJDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEJDEBUTF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MEJDEBUTI      PIC X(10).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE2L     COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MTE2L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE2F     PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MTE2I     PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDFINL   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MEDFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDFINF   PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MEDFINI   PIC X(10).                                      00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJFINL   COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MEJFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEJFINF   PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MEJFINI   PIC X(10).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSURFACEL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MSURFACEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSURFACEF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSURFACEI      PIC X(18).                                 00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTXCR0L  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MTTXCR0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTXCR0F  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MTTXCR0I  PIC X(4).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPANM0L  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MTPANM0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPANM0F  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MTPANM0I  PIC X(4).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTSOCL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MTSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTSOCF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MTSOCI    PIC X(3).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCATLML  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MTCATLML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCATLMF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MTCATLMI  PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCAELAL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MTCAELAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCAELAF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MTCAELAI  PIC X(3).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCADACL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MTCADACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCADACF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MTCADACI  PIC X(4).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCATOTL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MTCATOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCATOTF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MTCATOTI  PIC X(5).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPCEL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MTNBPCEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBPCEF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MTNBPCEI  PIC X(6).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBTRNL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MTNBTRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBTRNF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MTNBTRNI  PIC X(6).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBENTL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MTNBENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBENTF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MTNBENTI  PIC X(6).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTXCR1L  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MTTXCR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTXCR1F  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MTTXCR1I  PIC X(4).                                       00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPANM1L  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MTPANM1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPANM1F  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MTPANM1I  PIC X(4).                                       00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCAL     COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MTCAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTCAF     PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MTCAI     PIC X(38).                                      00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBRL    COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MTNBRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBRF    PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MTNBRI    PIC X(21).                                      00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTPL     COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MTTPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTTPF     PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MTTPI     PIC X(9).                                       00001350
           02 MLIGNEI OCCURS   10 TIMES .                               00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00001370
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00001380
             03 FILLER  PIC X(4).                                       00001390
             03 MXI     PIC X.                                          00001400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00001410
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00001420
             03 FILLER  PIC X(4).                                       00001430
             03 MNSOCI  PIC X(3).                                       00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATLML      COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MCATLML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATLMF      PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MCATLMI      PIC X(9).                                  00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAELAL      COMP PIC S9(4).                            00001490
      *--                                                                       
             03 MCAELAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAELAF      PIC X.                                     00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MCAELAI      PIC X(9).                                  00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCADACL      COMP PIC S9(4).                            00001530
      *--                                                                       
             03 MCADACL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCADACF      PIC X.                                     00001540
             03 FILLER  PIC X(4).                                       00001550
             03 MCADACI      PIC X(8).                                  00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATOTL      COMP PIC S9(4).                            00001570
      *--                                                                       
             03 MCATOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATOTF      PIC X.                                     00001580
             03 FILLER  PIC X(4).                                       00001590
             03 MCATOTI      PIC X(9).                                  00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPCEL      COMP PIC S9(4).                            00001610
      *--                                                                       
             03 MNBPCEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBPCEF      PIC X.                                     00001620
             03 FILLER  PIC X(4).                                       00001630
             03 MNBPCEI      PIC X(6).                                  00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBTRNL      COMP PIC S9(4).                            00001650
      *--                                                                       
             03 MNBTRNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBTRNF      PIC X.                                     00001660
             03 FILLER  PIC X(4).                                       00001670
             03 MNBTRNI      PIC X(6).                                  00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBENTL      COMP PIC S9(4).                            00001690
      *--                                                                       
             03 MNBENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBENTF      PIC X.                                     00001700
             03 FILLER  PIC X(4).                                       00001710
             03 MNBENTI      PIC X(7).                                  00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTXCRL  COMP PIC S9(4).                                 00001730
      *--                                                                       
             03 MTXCRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTXCRF  PIC X.                                          00001740
             03 FILLER  PIC X(4).                                       00001750
             03 MTXCRI  PIC X(4).                                       00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPANML  COMP PIC S9(4).                                 00001770
      *--                                                                       
             03 MPANML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPANMF  PIC X.                                          00001780
             03 FILLER  PIC X(4).                                       00001790
             03 MPANMI  PIC X(4).                                       00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATLMTL  COMP PIC S9(4).                                 00001810
      *--                                                                       
           02 MCATLMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCATLMTF  PIC X.                                          00001820
           02 FILLER    PIC X(4).                                       00001830
           02 MCATLMTI  PIC X(9).                                       00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAELATL  COMP PIC S9(4).                                 00001850
      *--                                                                       
           02 MCAELATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAELATF  PIC X.                                          00001860
           02 FILLER    PIC X(4).                                       00001870
           02 MCAELATI  PIC X(9).                                       00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCADACTL  COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MCADACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCADACTF  PIC X.                                          00001900
           02 FILLER    PIC X(4).                                       00001910
           02 MCADACTI  PIC X(8).                                       00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATOTTL  COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MCATOTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCATOTTF  PIC X.                                          00001940
           02 FILLER    PIC X(4).                                       00001950
           02 MCATOTTI  PIC X(9).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPCETL  COMP PIC S9(4).                                 00001970
      *--                                                                       
           02 MNBPCETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPCETF  PIC X.                                          00001980
           02 FILLER    PIC X(4).                                       00001990
           02 MNBPCETI  PIC X(6).                                       00002000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTRNTL  COMP PIC S9(4).                                 00002010
      *--                                                                       
           02 MNBTRNTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBTRNTF  PIC X.                                          00002020
           02 FILLER    PIC X(4).                                       00002030
           02 MNBTRNTI  PIC X(6).                                       00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBENTTL  COMP PIC S9(4).                                 00002050
      *--                                                                       
           02 MNBENTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBENTTF  PIC X.                                          00002060
           02 FILLER    PIC X(4).                                       00002070
           02 MNBENTTI  PIC X(7).                                       00002080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTXCRTL   COMP PIC S9(4).                                 00002090
      *--                                                                       
           02 MTXCRTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTXCRTF   PIC X.                                          00002100
           02 FILLER    PIC X(4).                                       00002110
           02 MTXCRTI   PIC X(4).                                       00002120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPANMTL   COMP PIC S9(4).                                 00002130
      *--                                                                       
           02 MPANMTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPANMTF   PIC X.                                          00002140
           02 FILLER    PIC X(4).                                       00002150
           02 MPANMTI   PIC X(4).                                       00002160
           02 MFD OCCURS   3 TIMES .                                    00002170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFL     COMP PIC S9(4).                                 00002180
      *--                                                                       
             03 MFL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MFF     PIC X.                                          00002190
             03 FILLER  PIC X(4).                                       00002200
             03 MFI     PIC X(79).                                      00002210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002220
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002230
           02 FILLER    PIC X(4).                                       00002240
           02 MLIBERRI  PIC X(79).                                      00002250
      * CODE TRANSACTION                                                00002260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002280
           02 FILLER    PIC X(4).                                       00002290
           02 MCODTRAI  PIC X(4).                                       00002300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002310
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002320
           02 FILLER    PIC X(4).                                       00002330
           02 MZONCMDI  PIC X(15).                                      00002340
      * NOM DU CICS                                                     00002350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002360
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002370
           02 FILLER    PIC X(4).                                       00002380
           02 MCICSI    PIC X(5).                                       00002390
      * NETNAME                                                         00002400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002410
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002420
           02 FILLER    PIC X(4).                                       00002430
           02 MNETNAMI  PIC X(8).                                       00002440
      * CODE TERMINAL                                                   00002450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002460
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002470
           02 FILLER    PIC X(4).                                       00002480
           02 MSCREENI  PIC X(4).                                       00002490
      ***************************************************************** 00002500
      * R�sultats par Soci�t�                                           00002510
      ***************************************************************** 00002520
       01   EAC10O REDEFINES EAC10I.                                    00002530
           02 FILLER    PIC X(12).                                      00002540
      * DATE DU JOUR                                                    00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MDATJOUA  PIC X.                                          00002570
           02 MDATJOUC  PIC X.                                          00002580
           02 MDATJOUP  PIC X.                                          00002590
           02 MDATJOUH  PIC X.                                          00002600
           02 MDATJOUV  PIC X.                                          00002610
           02 MDATJOUO  PIC X(10).                                      00002620
      * HEURE                                                           00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MTIMJOUA  PIC X.                                          00002650
           02 MTIMJOUC  PIC X.                                          00002660
           02 MTIMJOUP  PIC X.                                          00002670
           02 MTIMJOUH  PIC X.                                          00002680
           02 MTIMJOUV  PIC X.                                          00002690
           02 MTIMJOUO  PIC X(5).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MTPAGEAA  PIC X.                                          00002720
           02 MTPAGEAC  PIC X.                                          00002730
           02 MTPAGEAP  PIC X.                                          00002740
           02 MTPAGEAH  PIC X.                                          00002750
           02 MTPAGEAV  PIC X.                                          00002760
           02 MTPAGEAO  PIC X(4).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MPAGEAA   PIC X.                                          00002790
           02 MPAGEAC   PIC X.                                          00002800
           02 MPAGEAP   PIC X.                                          00002810
           02 MPAGEAH   PIC X.                                          00002820
           02 MPAGEAV   PIC X.                                          00002830
           02 MPAGEAO   PIC Z9.                                         00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MTNBPA    PIC X.                                          00002860
           02 MTNBPC    PIC X.                                          00002870
           02 MTNBPP    PIC X.                                          00002880
           02 MTNBPH    PIC X.                                          00002890
           02 MTNBPV    PIC X.                                          00002900
           02 MTNBPO    PIC X(2).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNBPA     PIC X.                                          00002930
           02 MNBPC     PIC X.                                          00002940
           02 MNBPP     PIC X.                                          00002950
           02 MNBPH     PIC X.                                          00002960
           02 MNBPV     PIC X.                                          00002970
           02 MNBPO     PIC Z9.                                         00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MDDEBUTA  PIC X.                                          00003000
           02 MDDEBUTC  PIC X.                                          00003010
           02 MDDEBUTP  PIC X.                                          00003020
           02 MDDEBUTH  PIC X.                                          00003030
           02 MDDEBUTV  PIC X.                                          00003040
           02 MDDEBUTO  PIC X(10).                                      00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MJDEBUTA  PIC X.                                          00003070
           02 MJDEBUTC  PIC X.                                          00003080
           02 MJDEBUTP  PIC X.                                          00003090
           02 MJDEBUTH  PIC X.                                          00003100
           02 MJDEBUTV  PIC X.                                          00003110
           02 MJDEBUTO  PIC X(10).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MDFINA    PIC X.                                          00003140
           02 MDFINC    PIC X.                                          00003150
           02 MDFINP    PIC X.                                          00003160
           02 MDFINH    PIC X.                                          00003170
           02 MDFINV    PIC X.                                          00003180
           02 MDFINO    PIC X(10).                                      00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MJFINA    PIC X.                                          00003210
           02 MJFINC    PIC X.                                          00003220
           02 MJFINP    PIC X.                                          00003230
           02 MJFINH    PIC X.                                          00003240
           02 MJFINV    PIC X.                                          00003250
           02 MJFINO    PIC X(10).                                      00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MTE1A     PIC X.                                          00003280
           02 MTE1C     PIC X.                                          00003290
           02 MTE1P     PIC X.                                          00003300
           02 MTE1H     PIC X.                                          00003310
           02 MTE1V     PIC X.                                          00003320
           02 MTE1O     PIC X(28).                                      00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MEDDEBUTA      PIC X.                                     00003350
           02 MEDDEBUTC PIC X.                                          00003360
           02 MEDDEBUTP PIC X.                                          00003370
           02 MEDDEBUTH PIC X.                                          00003380
           02 MEDDEBUTV PIC X.                                          00003390
           02 MEDDEBUTO      PIC X(10).                                 00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MEJDEBUTA      PIC X.                                     00003420
           02 MEJDEBUTC PIC X.                                          00003430
           02 MEJDEBUTP PIC X.                                          00003440
           02 MEJDEBUTH PIC X.                                          00003450
           02 MEJDEBUTV PIC X.                                          00003460
           02 MEJDEBUTO      PIC X(10).                                 00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MTE2A     PIC X.                                          00003490
           02 MTE2C     PIC X.                                          00003500
           02 MTE2P     PIC X.                                          00003510
           02 MTE2H     PIC X.                                          00003520
           02 MTE2V     PIC X.                                          00003530
           02 MTE2O     PIC X(4).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MEDFINA   PIC X.                                          00003560
           02 MEDFINC   PIC X.                                          00003570
           02 MEDFINP   PIC X.                                          00003580
           02 MEDFINH   PIC X.                                          00003590
           02 MEDFINV   PIC X.                                          00003600
           02 MEDFINO   PIC X(10).                                      00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MEJFINA   PIC X.                                          00003630
           02 MEJFINC   PIC X.                                          00003640
           02 MEJFINP   PIC X.                                          00003650
           02 MEJFINH   PIC X.                                          00003660
           02 MEJFINV   PIC X.                                          00003670
           02 MEJFINO   PIC X(10).                                      00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSURFACEA      PIC X.                                     00003700
           02 MSURFACEC PIC X.                                          00003710
           02 MSURFACEP PIC X.                                          00003720
           02 MSURFACEH PIC X.                                          00003730
           02 MSURFACEV PIC X.                                          00003740
           02 MSURFACEO      PIC X(18).                                 00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MTTXCR0A  PIC X.                                          00003770
           02 MTTXCR0C  PIC X.                                          00003780
           02 MTTXCR0P  PIC X.                                          00003790
           02 MTTXCR0H  PIC X.                                          00003800
           02 MTTXCR0V  PIC X.                                          00003810
           02 MTTXCR0O  PIC X(4).                                       00003820
           02 FILLER    PIC X(2).                                       00003830
           02 MTPANM0A  PIC X.                                          00003840
           02 MTPANM0C  PIC X.                                          00003850
           02 MTPANM0P  PIC X.                                          00003860
           02 MTPANM0H  PIC X.                                          00003870
           02 MTPANM0V  PIC X.                                          00003880
           02 MTPANM0O  PIC X(4).                                       00003890
           02 FILLER    PIC X(2).                                       00003900
           02 MTSOCA    PIC X.                                          00003910
           02 MTSOCC    PIC X.                                          00003920
           02 MTSOCP    PIC X.                                          00003930
           02 MTSOCH    PIC X.                                          00003940
           02 MTSOCV    PIC X.                                          00003950
           02 MTSOCO    PIC X(3).                                       00003960
           02 FILLER    PIC X(2).                                       00003970
           02 MTCATLMA  PIC X.                                          00003980
           02 MTCATLMC  PIC X.                                          00003990
           02 MTCATLMP  PIC X.                                          00004000
           02 MTCATLMH  PIC X.                                          00004010
           02 MTCATLMV  PIC X.                                          00004020
           02 MTCATLMO  PIC X(3).                                       00004030
           02 FILLER    PIC X(2).                                       00004040
           02 MTCAELAA  PIC X.                                          00004050
           02 MTCAELAC  PIC X.                                          00004060
           02 MTCAELAP  PIC X.                                          00004070
           02 MTCAELAH  PIC X.                                          00004080
           02 MTCAELAV  PIC X.                                          00004090
           02 MTCAELAO  PIC X(3).                                       00004100
           02 FILLER    PIC X(2).                                       00004110
           02 MTCADACA  PIC X.                                          00004120
           02 MTCADACC  PIC X.                                          00004130
           02 MTCADACP  PIC X.                                          00004140
           02 MTCADACH  PIC X.                                          00004150
           02 MTCADACV  PIC X.                                          00004160
           02 MTCADACO  PIC X(4).                                       00004170
           02 FILLER    PIC X(2).                                       00004180
           02 MTCATOTA  PIC X.                                          00004190
           02 MTCATOTC  PIC X.                                          00004200
           02 MTCATOTP  PIC X.                                          00004210
           02 MTCATOTH  PIC X.                                          00004220
           02 MTCATOTV  PIC X.                                          00004230
           02 MTCATOTO  PIC X(5).                                       00004240
           02 FILLER    PIC X(2).                                       00004250
           02 MTNBPCEA  PIC X.                                          00004260
           02 MTNBPCEC  PIC X.                                          00004270
           02 MTNBPCEP  PIC X.                                          00004280
           02 MTNBPCEH  PIC X.                                          00004290
           02 MTNBPCEV  PIC X.                                          00004300
           02 MTNBPCEO  PIC X(6).                                       00004310
           02 FILLER    PIC X(2).                                       00004320
           02 MTNBTRNA  PIC X.                                          00004330
           02 MTNBTRNC  PIC X.                                          00004340
           02 MTNBTRNP  PIC X.                                          00004350
           02 MTNBTRNH  PIC X.                                          00004360
           02 MTNBTRNV  PIC X.                                          00004370
           02 MTNBTRNO  PIC X(6).                                       00004380
           02 FILLER    PIC X(2).                                       00004390
           02 MTNBENTA  PIC X.                                          00004400
           02 MTNBENTC  PIC X.                                          00004410
           02 MTNBENTP  PIC X.                                          00004420
           02 MTNBENTH  PIC X.                                          00004430
           02 MTNBENTV  PIC X.                                          00004440
           02 MTNBENTO  PIC X(6).                                       00004450
           02 FILLER    PIC X(2).                                       00004460
           02 MTTXCR1A  PIC X.                                          00004470
           02 MTTXCR1C  PIC X.                                          00004480
           02 MTTXCR1P  PIC X.                                          00004490
           02 MTTXCR1H  PIC X.                                          00004500
           02 MTTXCR1V  PIC X.                                          00004510
           02 MTTXCR1O  PIC X(4).                                       00004520
           02 FILLER    PIC X(2).                                       00004530
           02 MTPANM1A  PIC X.                                          00004540
           02 MTPANM1C  PIC X.                                          00004550
           02 MTPANM1P  PIC X.                                          00004560
           02 MTPANM1H  PIC X.                                          00004570
           02 MTPANM1V  PIC X.                                          00004580
           02 MTPANM1O  PIC X(4).                                       00004590
           02 FILLER    PIC X(2).                                       00004600
           02 MTCAA     PIC X.                                          00004610
           02 MTCAC     PIC X.                                          00004620
           02 MTCAP     PIC X.                                          00004630
           02 MTCAH     PIC X.                                          00004640
           02 MTCAV     PIC X.                                          00004650
           02 MTCAO     PIC X(38).                                      00004660
           02 FILLER    PIC X(2).                                       00004670
           02 MTNBRA    PIC X.                                          00004680
           02 MTNBRC    PIC X.                                          00004690
           02 MTNBRP    PIC X.                                          00004700
           02 MTNBRH    PIC X.                                          00004710
           02 MTNBRV    PIC X.                                          00004720
           02 MTNBRO    PIC X(21).                                      00004730
           02 FILLER    PIC X(2).                                       00004740
           02 MTTPA     PIC X.                                          00004750
           02 MTTPC     PIC X.                                          00004760
           02 MTTPP     PIC X.                                          00004770
           02 MTTPH     PIC X.                                          00004780
           02 MTTPV     PIC X.                                          00004790
           02 MTTPO     PIC X(9).                                       00004800
           02 MLIGNEO OCCURS   10 TIMES .                               00004810
             03 FILLER       PIC X(2).                                  00004820
             03 MXA     PIC X.                                          00004830
             03 MXC     PIC X.                                          00004840
             03 MXP     PIC X.                                          00004850
             03 MXH     PIC X.                                          00004860
             03 MXV     PIC X.                                          00004870
             03 MXO     PIC X.                                          00004880
             03 FILLER       PIC X(2).                                  00004890
             03 MNSOCA  PIC X.                                          00004900
             03 MNSOCC  PIC X.                                          00004910
             03 MNSOCP  PIC X.                                          00004920
             03 MNSOCH  PIC X.                                          00004930
             03 MNSOCV  PIC X.                                          00004940
             03 MNSOCO  PIC X(3).                                       00004950
             03 FILLER       PIC X(2).                                  00004960
             03 MCATLMA      PIC X.                                     00004970
             03 MCATLMC PIC X.                                          00004980
             03 MCATLMP PIC X.                                          00004990
             03 MCATLMH PIC X.                                          00005000
             03 MCATLMV PIC X.                                          00005010
             03 MCATLMO      PIC --------9.                             00005020
             03 FILLER       PIC X(2).                                  00005030
             03 MCAELAA      PIC X.                                     00005040
             03 MCAELAC PIC X.                                          00005050
             03 MCAELAP PIC X.                                          00005060
             03 MCAELAH PIC X.                                          00005070
             03 MCAELAV PIC X.                                          00005080
             03 MCAELAO      PIC --------9.                             00005090
             03 FILLER       PIC X(2).                                  00005100
             03 MCADACA      PIC X.                                     00005110
             03 MCADACC PIC X.                                          00005120
             03 MCADACP PIC X.                                          00005130
             03 MCADACH PIC X.                                          00005140
             03 MCADACV PIC X.                                          00005150
             03 MCADACO      PIC -------9.                              00005160
             03 FILLER       PIC X(2).                                  00005170
             03 MCATOTA      PIC X.                                     00005180
             03 MCATOTC PIC X.                                          00005190
             03 MCATOTP PIC X.                                          00005200
             03 MCATOTH PIC X.                                          00005210
             03 MCATOTV PIC X.                                          00005220
             03 MCATOTO      PIC --------9.                             00005230
             03 FILLER       PIC X(2).                                  00005240
             03 MNBPCEA      PIC X.                                     00005250
             03 MNBPCEC PIC X.                                          00005260
             03 MNBPCEP PIC X.                                          00005270
             03 MNBPCEH PIC X.                                          00005280
             03 MNBPCEV PIC X.                                          00005290
             03 MNBPCEO      PIC -----9.                                00005300
             03 FILLER       PIC X(2).                                  00005310
             03 MNBTRNA      PIC X.                                     00005320
             03 MNBTRNC PIC X.                                          00005330
             03 MNBTRNP PIC X.                                          00005340
             03 MNBTRNH PIC X.                                          00005350
             03 MNBTRNV PIC X.                                          00005360
             03 MNBTRNO      PIC -----9.                                00005370
             03 FILLER       PIC X(2).                                  00005380
             03 MNBENTA      PIC X.                                     00005390
             03 MNBENTC PIC X.                                          00005400
             03 MNBENTP PIC X.                                          00005410
             03 MNBENTH PIC X.                                          00005420
             03 MNBENTV PIC X.                                          00005430
             03 MNBENTO      PIC ------9.                               00005440
             03 FILLER       PIC X(2).                                  00005450
             03 MTXCRA  PIC X.                                          00005460
             03 MTXCRC  PIC X.                                          00005470
             03 MTXCRP  PIC X.                                          00005480
             03 MTXCRH  PIC X.                                          00005490
             03 MTXCRV  PIC X.                                          00005500
             03 MTXCRO  PIC Z9,9.                                       00005510
             03 FILLER       PIC X(2).                                  00005520
             03 MPANMA  PIC X.                                          00005530
             03 MPANMC  PIC X.                                          00005540
             03 MPANMP  PIC X.                                          00005550
             03 MPANMH  PIC X.                                          00005560
             03 MPANMV  PIC X.                                          00005570
             03 MPANMO  PIC ---9.                                       00005580
           02 FILLER    PIC X(2).                                       00005590
           02 MCATLMTA  PIC X.                                          00005600
           02 MCATLMTC  PIC X.                                          00005610
           02 MCATLMTP  PIC X.                                          00005620
           02 MCATLMTH  PIC X.                                          00005630
           02 MCATLMTV  PIC X.                                          00005640
           02 MCATLMTO  PIC --------9.                                  00005650
           02 FILLER    PIC X(2).                                       00005660
           02 MCAELATA  PIC X.                                          00005670
           02 MCAELATC  PIC X.                                          00005680
           02 MCAELATP  PIC X.                                          00005690
           02 MCAELATH  PIC X.                                          00005700
           02 MCAELATV  PIC X.                                          00005710
           02 MCAELATO  PIC --------9.                                  00005720
           02 FILLER    PIC X(2).                                       00005730
           02 MCADACTA  PIC X.                                          00005740
           02 MCADACTC  PIC X.                                          00005750
           02 MCADACTP  PIC X.                                          00005760
           02 MCADACTH  PIC X.                                          00005770
           02 MCADACTV  PIC X.                                          00005780
           02 MCADACTO  PIC -------9.                                   00005790
           02 FILLER    PIC X(2).                                       00005800
           02 MCATOTTA  PIC X.                                          00005810
           02 MCATOTTC  PIC X.                                          00005820
           02 MCATOTTP  PIC X.                                          00005830
           02 MCATOTTH  PIC X.                                          00005840
           02 MCATOTTV  PIC X.                                          00005850
           02 MCATOTTO  PIC --------9.                                  00005860
           02 FILLER    PIC X(2).                                       00005870
           02 MNBPCETA  PIC X.                                          00005880
           02 MNBPCETC  PIC X.                                          00005890
           02 MNBPCETP  PIC X.                                          00005900
           02 MNBPCETH  PIC X.                                          00005910
           02 MNBPCETV  PIC X.                                          00005920
           02 MNBPCETO  PIC -----9.                                     00005930
           02 FILLER    PIC X(2).                                       00005940
           02 MNBTRNTA  PIC X.                                          00005950
           02 MNBTRNTC  PIC X.                                          00005960
           02 MNBTRNTP  PIC X.                                          00005970
           02 MNBTRNTH  PIC X.                                          00005980
           02 MNBTRNTV  PIC X.                                          00005990
           02 MNBTRNTO  PIC -----9.                                     00006000
           02 FILLER    PIC X(2).                                       00006010
           02 MNBENTTA  PIC X.                                          00006020
           02 MNBENTTC  PIC X.                                          00006030
           02 MNBENTTP  PIC X.                                          00006040
           02 MNBENTTH  PIC X.                                          00006050
           02 MNBENTTV  PIC X.                                          00006060
           02 MNBENTTO  PIC ------9.                                    00006070
           02 FILLER    PIC X(2).                                       00006080
           02 MTXCRTA   PIC X.                                          00006090
           02 MTXCRTC   PIC X.                                          00006100
           02 MTXCRTP   PIC X.                                          00006110
           02 MTXCRTH   PIC X.                                          00006120
           02 MTXCRTV   PIC X.                                          00006130
           02 MTXCRTO   PIC Z9,9.                                       00006140
           02 FILLER    PIC X(2).                                       00006150
           02 MPANMTA   PIC X.                                          00006160
           02 MPANMTC   PIC X.                                          00006170
           02 MPANMTP   PIC X.                                          00006180
           02 MPANMTH   PIC X.                                          00006190
           02 MPANMTV   PIC X.                                          00006200
           02 MPANMTO   PIC ---9.                                       00006210
           02 DFHMS1 OCCURS   3 TIMES .                                 00006220
             03 FILLER       PIC X(2).                                  00006230
             03 MFA     PIC X.                                          00006240
             03 MFC     PIC X.                                          00006250
             03 MFP     PIC X.                                          00006260
             03 MFH     PIC X.                                          00006270
             03 MFV     PIC X.                                          00006280
             03 MFO     PIC X(79).                                      00006290
           02 FILLER    PIC X(2).                                       00006300
           02 MLIBERRA  PIC X.                                          00006310
           02 MLIBERRC  PIC X.                                          00006320
           02 MLIBERRP  PIC X.                                          00006330
           02 MLIBERRH  PIC X.                                          00006340
           02 MLIBERRV  PIC X.                                          00006350
           02 MLIBERRO  PIC X(79).                                      00006360
      * CODE TRANSACTION                                                00006370
           02 FILLER    PIC X(2).                                       00006380
           02 MCODTRAA  PIC X.                                          00006390
           02 MCODTRAC  PIC X.                                          00006400
           02 MCODTRAP  PIC X.                                          00006410
           02 MCODTRAH  PIC X.                                          00006420
           02 MCODTRAV  PIC X.                                          00006430
           02 MCODTRAO  PIC X(4).                                       00006440
           02 FILLER    PIC X(2).                                       00006450
           02 MZONCMDA  PIC X.                                          00006460
           02 MZONCMDC  PIC X.                                          00006470
           02 MZONCMDP  PIC X.                                          00006480
           02 MZONCMDH  PIC X.                                          00006490
           02 MZONCMDV  PIC X.                                          00006500
           02 MZONCMDO  PIC X(15).                                      00006510
      * NOM DU CICS                                                     00006520
           02 FILLER    PIC X(2).                                       00006530
           02 MCICSA    PIC X.                                          00006540
           02 MCICSC    PIC X.                                          00006550
           02 MCICSP    PIC X.                                          00006560
           02 MCICSH    PIC X.                                          00006570
           02 MCICSV    PIC X.                                          00006580
           02 MCICSO    PIC X(5).                                       00006590
      * NETNAME                                                         00006600
           02 FILLER    PIC X(2).                                       00006610
           02 MNETNAMA  PIC X.                                          00006620
           02 MNETNAMC  PIC X.                                          00006630
           02 MNETNAMP  PIC X.                                          00006640
           02 MNETNAMH  PIC X.                                          00006650
           02 MNETNAMV  PIC X.                                          00006660
           02 MNETNAMO  PIC X(8).                                       00006670
      * CODE TERMINAL                                                   00006680
           02 FILLER    PIC X(2).                                       00006690
           02 MSCREENA  PIC X.                                          00006700
           02 MSCREENC  PIC X.                                          00006710
           02 MSCREENP  PIC X.                                          00006720
           02 MSCREENH  PIC X.                                          00006730
           02 MSCREENV  PIC X.                                          00006740
           02 MSCREENO  PIC X(4).                                       00006750
                                                                                
