      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAV0300                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAV0300                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0300.                                                            
           02  AV03-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  AV03-NLIEU                                                       
               PIC X(0003).                                                     
           02  AV03-NAVOIR                                                      
               PIC X(0007).                                                     
           02  AV03-DMAJ                                                        
               PIC X(0008).                                                     
           02  AV03-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAV0300                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0300-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV03-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV03-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV03-NAVOIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV03-NAVOIR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV03-DMAJ                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV03-DMAJ                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV03-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV03-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
