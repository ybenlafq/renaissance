      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      **************************************************************            
      *         MAQUETTE COMMAREA STANDARD AIDA COBOL2             *            
      **************************************************************            
      *                                                                         
      * XXXX DANS LE NOM DES ZONES COM-XXXX-LONG-COMMAREA ET                    
      *      COMM-XXXX-APPLI EST LE SUFFIXE DE LA COMMAREA UTILISATEUR          
      *      DONNE PAR LE PROGRAMMEUR LORS DE LA GENERATION DU                  
      *      PROGRAMME (ETAPE CHOIX DES RESSOURCES).                            
      *                                                                         
      * COM-XXXX-LONG-COMMAREA NE DOIT PAS EXEDER UNE VALUE DE +4096            
      * COMPRENANT :                                                            
      * 1 - LES ZONES RESERVEES A AIDA                                          
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS                           
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT                         
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP                              
      * 5 - LES ZONES RESERVEES APPLICATIVES                                    
      *                                                                         
      * COM-XXX-LONG-COMMAREA ET Z-COMMAREA SONT DES NOMS IMPOSES               
      * PAR AIDA                                                                
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SV00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-SV00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA -----------------------------------------        
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS --------------------------        
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------------        
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP -----------------------------        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 180 PIC X(1).                               
      *                                                                         
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>> ZONES RESERVEES APPLICATIVES <<<<<<<<<<<<<<<<<*        
      *>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*        
      *                                                                         
      *----------------------------------------------------------------*        
      * MENU GENERAL DE L'APPLICATION (PROGRAMME TSV00)                *        
      *----------------------------------------------------------------*        
           02 COMM-SV00-MENU.                                                   
      *----------------------------------------------------------------*        
              03 COMM-SV00-SV00.                                                
                 04 COMM-SV00-CFAM1          PIC X(5).                          
                 04 FILLER                   PIC X(195).                        
              03 COMM-SV00-APPLI             PIC X(3000).                       
      *.....................................................                    
              03 COMM-SV00-SV01   REDEFINES  COMM-SV00-APPLI.                   
                 04 COMM-SV01-ETAPE              PIC X.                         
                 04 COMM-SV01-NSOC               PIC X(3).                      
                 04 COMM-SV01.                                                  
                    05 COMM-SV01-PAGE            PIC 99.                        
                    05 COMM-SV01-PAGET           PIC 99.                        
                    05 COMM-SV01-NMAG            PIC X(3).                      
                    05 COMM-SV01-CTPSAV          PIC X(5).                      
                    05 COMM-SV01-LIGNE               OCCURS 10.                 
                       10 COMM-SV01-DEFFET       PIC X(8).                      
                       10 COMM-SV01-COL              OCCURS 20.                 
                          15 COMM-SV01-NSOCSAV   PIC X(3).                      
                          15 COMM-SV01-WTAUXSA   PIC 9(3).                      
      *.....................................................                    
              03 COMM-SV00-SV02   REDEFINES  COMM-SV00-APPLI.                   
                 04 COMM-SV02-PAGE               PIC 999.                       
                 04 COMM-SV02-WSEQFAM-SUIV       PIC S9(5) COMP-3.              
                 04 COMM-SV02-FIN-GA14           PIC X(1).                      
                 04 COMM-SV02-TAB-LIGNE.                                        
                    05 COMM-SV02-LIGNE OCCURS  15.                              
                       10 COMM-SV02-CFAM         PIC X(5).                      
                       10 COMM-SV02-LFAM         PIC X(20).                     
                       10 COMM-SV02-WSEQFAM      PIC S9(5) COMP-3.              
                       10 COMM-SV02-CSECGAS      PIC X(5).                      
                       10 COMM-SV02-CAFFGAS      PIC X(1).                      
                       10 COMM-SV02-CGRP         PIC X(5).                      
                       10 COMM-SV02-CSECT1       PIC X(5).                      
                       10 COMM-SV02-CAFFEC1      PIC X(1).                      
                       10 COMM-SV02-CSECT2       PIC X(5).                      
                       10 COMM-SV02-FLAG-PREM    PIC X(1).                      
                 04 COMM-SV02-TAB-PREM.                                         
                    05 COMM-SV02-WSEQFAM-PREM OCCURS 50                         
                                                 PIC S9(5) COMP-3.              
      *.....................................................                    
              03 COMM-SV00-SV03   REDEFINES  COMM-SV00-APPLI.                   
                 04 COMM-SV03-PAGE               PIC 999.                       
                 04 COMM-SV03-PAGET              PIC 999.                       
                 04 COMM-SV03-CGROUPE            PIC X(5).                      
                 04 COMM-SV03-SECTEUR1           PIC X(5).                      
                 04 COMM-SV03-SECTEUR2           PIC X(5).                      
                                                                                
