      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMAS10.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *      COMMAREA DE LA TRANSACTION AS00 ASILAGE CEN               *        
      *                                                                *        
      *      PARTIES APPLICATIVES COMMUNES AUX TRANSACTION :           *        
      *      AS10                                                      *        
      *                                                                *        
      *      LONGUEUR = 1 - ZONES COMMUNES               372 C         *        
      *                 2 - ZONES APPLICATIVES PM       3724 C         *        
      *                     TOTAL                       4096           *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      ******************************************************************        
      *    ZONES APPLICATIVES DES TRANSACTION AS00,AS10,     --- 199 C *        
      ******************************************************************        
      *                                                                         
           02 COMM-AS00-ZONES   REDEFINES  COMM-AS00-APPLI.                     
      *                                    NOM DU MENU APPELANT                 
              05 COMM-AS00-CMENU           PIC X(05).                           
      *                                    MAGASIN DE RECEPTION                 
              05 COMM-AS00-CCAMPAGNE       PIC X(04).                           
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DES DONNEES PARAMETRES   - TAS10 - 171 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-AS10-APPLI.                                               
                 10 COMM-AS10-CFONCT          PIC X(03).                00008601
                 10 COMM-AS10-CCAMPAGNE       PIC X(04).                        
                 10 COMM-AS10-CODE-RETOUR.                                      
                    15 COMM-AS10-CODRET       PIC X(01).                        
                    15 COMM-AS10-LIBERR       PIC X(58).                        
      *                                                                         
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 2918 C*         
      ******************************************************************        
      *                                                                         
              05 COMM-AS00-FILLER          PIC X(2918).                         
      *                                                                         
          02 COMM-AS25-APPLI REDEFINES COMM-AS00-APPLI.                         
             03 COMM-AS25-PAGE           PIC 9(2).                              
             03 COMM-AS25-PAGE-MAX       PIC 9(2).                              
             03 COMM-AS25-FILLER         PIC X(3691).                           
                                                                                
