      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV611 AU 18/01/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,06,BI,A,                          *        
      *                           19,03,BI,A,                          *        
      *                           22,03,PD,A,                          *        
      *                           25,02,BI,A,                          *        
      *                           27,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV611.                                                        
            05 NOMETAT-IPV611           PIC X(6) VALUE 'IPV611'.                
            05 RUPTURES-IPV611.                                                 
           10 IPV611-NSOCIETE           PIC X(03).                      007  003
           10 IPV611-NLIEU              PIC X(03).                      010  003
           10 IPV611-CVENDEUR           PIC X(06).                      013  006
           10 IPV611-LIEUVTE            PIC X(03).                      019  003
           10 IPV611-WSEQED             PIC S9(05)      COMP-3.         022  003
           10 IPV611-NAGREGATED         PIC X(02).                      025  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV611-SEQUENCE           PIC S9(04) COMP.                027  002
      *--                                                                       
           10 IPV611-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV611.                                                   
           10 IPV611-LVENDEUR           PIC X(20).                      029  020
           10 IPV611-RUBRIQUE           PIC X(20).                      049  020
           10 IPV611-SOCVTE             PIC X(03).                      069  003
           10 IPV611-WEDIT              PIC X(01).                      072  001
           10 IPV611-FOR                PIC S9(07)V9(2) COMP-3.         073  005
           10 IPV611-MBU                PIC S9(07)V9(2) COMP-3.         078  005
           10 IPV611-PCA                PIC S9(07)V9(2) COMP-3.         083  005
           10 IPV611-REM                PIC S9(07)V9(2) COMP-3.         088  005
           10 IPV611-VOL                PIC S9(08)      COMP-3.         093  005
           10 IPV611-DEBPERIODE         PIC X(08).                      098  008
           10 IPV611-FINPERIODE         PIC X(08).                      106  008
            05 FILLER                      PIC X(399).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV611-LONG           PIC S9(4)   COMP  VALUE +113.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV611-LONG           PIC S9(4) COMP-5  VALUE +113.           
                                                                                
      *}                                                                        
