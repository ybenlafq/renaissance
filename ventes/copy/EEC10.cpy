      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Contr�le Commandes WebSph�re 1/2                                00000020
      ***************************************************************** 00000030
       01   EEC10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPAGEAL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTPAGEAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPAGEAF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTPAGEAI  PIC X(4).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEAI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MTNBPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBPF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MTNBPI    PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNBPI     PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCPTL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MCPTF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCPTI     PIC X(79).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSTATL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MRSTATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRSTATF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MRSTATI   PIC X.                                          00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROPERL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MROPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MROPERF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MROPERI   PIC X(4).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNCDEWCL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MRNCDEWCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRNCDEWCF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MRNCDEWCI      PIC X(8).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNSOCIETEL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MRNSOCIETEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MRNSOCIETEF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MRNSOCIETEI    PIC X(3).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNLIEUL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MRNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRNLIEUF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MRNLIEUI  PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNVENTEL      COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MRNVENTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRNVENTEF      PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MRNVENTEI      PIC X(7).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNOML    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MRNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRNOMF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MRNOMI    PIC X(24).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRDCREL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MRDCREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRDCREF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MRDCREI   PIC X(8).                                       00000650
           02 MXD OCCURS   8 TIMES .                                    00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MXL     COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MXL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MXF     PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MXI     PIC X.                                          00000700
           02 MSTATD OCCURS   8 TIMES .                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATL  COMP PIC S9(4).                                 00000720
      *--                                                                       
             03 MSTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTATF  PIC X.                                          00000730
             03 FILLER  PIC X(4).                                       00000740
             03 MSTATI  PIC X(7).                                       00000750
           02 MOPERD OCCURS   8 TIMES .                                 00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MOPERL  COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MOPERL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MOPERF  PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MOPERI  PIC X(4).                                       00000800
           02 MNCDEWCD OCCURS   8 TIMES .                               00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCDEWCL     COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MNCDEWCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCDEWCF     PIC X.                                     00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MNCDEWCI     PIC X(8).                                  00000850
           02 MNSOCIETED OCCURS   8 TIMES .                             00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MNSOCIETEI   PIC X(3).                                  00000900
           02 MNLIEUD OCCURS   8 TIMES .                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MNLIEUI      PIC X(3).                                  00000950
           02 MNVENTED OCCURS   8 TIMES .                               00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNVENTEL     COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MNVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNVENTEF     PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MNVENTEI     PIC X(7).                                  00001000
           02 MNOMD OCCURS   8 TIMES .                                  00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOML   COMP PIC S9(4).                                 00001020
      *--                                                                       
             03 MNOML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNOMF   PIC X.                                          00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MNOMI   PIC X(24).                                      00001050
           02 MDCRED OCCURS   8 TIMES .                                 00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREL  COMP PIC S9(4).                                 00001070
      *--                                                                       
             03 MDCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDCREF  PIC X.                                          00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDCREI  PIC X(8).                                       00001100
           02 MHCRED OCCURS   8 TIMES .                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCREL  COMP PIC S9(4).                                 00001120
      *--                                                                       
             03 MHCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MHCREF  PIC X.                                          00001130
             03 FILLER  PIC X(4).                                       00001140
             03 MHCREI  PIC X(5).                                       00001150
           02 MSTATTD OCCURS   8 TIMES .                                00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATTL      COMP PIC S9(4).                            00001170
      *--                                                                       
             03 MSTATTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MSTATTF      PIC X.                                     00001180
             03 FILLER  PIC X(4).                                       00001190
             03 MSTATTI      PIC X.                                     00001200
           02 MTEXTETD OCCURS   8 TIMES .                               00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTEXTETL     COMP PIC S9(4).                            00001220
      *--                                                                       
             03 MTEXTETL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MTEXTETF     PIC X.                                     00001230
             03 FILLER  PIC X(4).                                       00001240
             03 MTEXTETI     PIC X(60).                                 00001250
           02 MDCRETD OCCURS   8 TIMES .                                00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCRETL      COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MDCRETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDCRETF      PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MDCRETI      PIC X(8).                                  00001300
           02 MHCRETD OCCURS   8 TIMES .                                00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCRETL      COMP PIC S9(4).                            00001320
      *--                                                                       
             03 MHCRETL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MHCRETF      PIC X.                                     00001330
             03 FILLER  PIC X(4).                                       00001340
             03 MHCRETI      PIC X(5).                                  00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MLIBERRI  PIC X(79).                                      00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MCODTRAI  PIC X(4).                                       00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001440
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MZONCMDI  PIC X(15).                                      00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MCICSI    PIC X(5).                                       00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MNETNAMI  PIC X(8).                                       00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MSCREENI  PIC X(4).                                       00001590
      ***************************************************************** 00001600
      * Contr�le Commandes WebSph�re 1/2                                00001610
      ***************************************************************** 00001620
       01   EEC10O REDEFINES EEC10I.                                    00001630
           02 FILLER    PIC X(12).                                      00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MDATJOUA  PIC X.                                          00001660
           02 MDATJOUC  PIC X.                                          00001670
           02 MDATJOUP  PIC X.                                          00001680
           02 MDATJOUH  PIC X.                                          00001690
           02 MDATJOUV  PIC X.                                          00001700
           02 MDATJOUO  PIC X(10).                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MTIMJOUA  PIC X.                                          00001730
           02 MTIMJOUC  PIC X.                                          00001740
           02 MTIMJOUP  PIC X.                                          00001750
           02 MTIMJOUH  PIC X.                                          00001760
           02 MTIMJOUV  PIC X.                                          00001770
           02 MTIMJOUO  PIC X(5).                                       00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MTPAGEAA  PIC X.                                          00001800
           02 MTPAGEAC  PIC X.                                          00001810
           02 MTPAGEAP  PIC X.                                          00001820
           02 MTPAGEAH  PIC X.                                          00001830
           02 MTPAGEAV  PIC X.                                          00001840
           02 MTPAGEAO  PIC X(4).                                       00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MPAGEAA   PIC X.                                          00001870
           02 MPAGEAC   PIC X.                                          00001880
           02 MPAGEAP   PIC X.                                          00001890
           02 MPAGEAH   PIC X.                                          00001900
           02 MPAGEAV   PIC X.                                          00001910
           02 MPAGEAO   PIC Z9.                                         00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MTNBPA    PIC X.                                          00001940
           02 MTNBPC    PIC X.                                          00001950
           02 MTNBPP    PIC X.                                          00001960
           02 MTNBPH    PIC X.                                          00001970
           02 MTNBPV    PIC X.                                          00001980
           02 MTNBPO    PIC X(2).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNBPA     PIC X.                                          00002010
           02 MNBPC     PIC X.                                          00002020
           02 MNBPP     PIC X.                                          00002030
           02 MNBPH     PIC X.                                          00002040
           02 MNBPV     PIC X.                                          00002050
           02 MNBPO     PIC Z9.                                         00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MCPTA     PIC X.                                          00002080
           02 MCPTC     PIC X.                                          00002090
           02 MCPTP     PIC X.                                          00002100
           02 MCPTH     PIC X.                                          00002110
           02 MCPTV     PIC X.                                          00002120
           02 MCPTO     PIC X(79).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MRSTATA   PIC X.                                          00002150
           02 MRSTATC   PIC X.                                          00002160
           02 MRSTATP   PIC X.                                          00002170
           02 MRSTATH   PIC X.                                          00002180
           02 MRSTATV   PIC X.                                          00002190
           02 MRSTATO   PIC X.                                          00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MROPERA   PIC X.                                          00002220
           02 MROPERC   PIC X.                                          00002230
           02 MROPERP   PIC X.                                          00002240
           02 MROPERH   PIC X.                                          00002250
           02 MROPERV   PIC X.                                          00002260
           02 MROPERO   PIC X(4).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MRNCDEWCA      PIC X.                                     00002290
           02 MRNCDEWCC PIC X.                                          00002300
           02 MRNCDEWCP PIC X.                                          00002310
           02 MRNCDEWCH PIC X.                                          00002320
           02 MRNCDEWCV PIC X.                                          00002330
           02 MRNCDEWCO      PIC X(8).                                  00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MRNSOCIETEA    PIC X.                                     00002360
           02 MRNSOCIETEC    PIC X.                                     00002370
           02 MRNSOCIETEP    PIC X.                                     00002380
           02 MRNSOCIETEH    PIC X.                                     00002390
           02 MRNSOCIETEV    PIC X.                                     00002400
           02 MRNSOCIETEO    PIC X(3).                                  00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MRNLIEUA  PIC X.                                          00002430
           02 MRNLIEUC  PIC X.                                          00002440
           02 MRNLIEUP  PIC X.                                          00002450
           02 MRNLIEUH  PIC X.                                          00002460
           02 MRNLIEUV  PIC X.                                          00002470
           02 MRNLIEUO  PIC X(3).                                       00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MRNVENTEA      PIC X.                                     00002500
           02 MRNVENTEC PIC X.                                          00002510
           02 MRNVENTEP PIC X.                                          00002520
           02 MRNVENTEH PIC X.                                          00002530
           02 MRNVENTEV PIC X.                                          00002540
           02 MRNVENTEO      PIC X(7).                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MRNOMA    PIC X.                                          00002570
           02 MRNOMC    PIC X.                                          00002580
           02 MRNOMP    PIC X.                                          00002590
           02 MRNOMH    PIC X.                                          00002600
           02 MRNOMV    PIC X.                                          00002610
           02 MRNOMO    PIC X(24).                                      00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MRDCREA   PIC X.                                          00002640
           02 MRDCREC   PIC X.                                          00002650
           02 MRDCREP   PIC X.                                          00002660
           02 MRDCREH   PIC X.                                          00002670
           02 MRDCREV   PIC X.                                          00002680
           02 MRDCREO   PIC X(8).                                       00002690
           02 DFHMS1 OCCURS   8 TIMES .                                 00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MXA     PIC X.                                          00002720
             03 MXC     PIC X.                                          00002730
             03 MXP     PIC X.                                          00002740
             03 MXH     PIC X.                                          00002750
             03 MXV     PIC X.                                          00002760
             03 MXO     PIC X.                                          00002770
           02 DFHMS2 OCCURS   8 TIMES .                                 00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MSTATA  PIC X.                                          00002800
             03 MSTATC  PIC X.                                          00002810
             03 MSTATP  PIC X.                                          00002820
             03 MSTATH  PIC X.                                          00002830
             03 MSTATV  PIC X.                                          00002840
             03 MSTATO  PIC X(7).                                       00002850
           02 DFHMS3 OCCURS   8 TIMES .                                 00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MOPERA  PIC X.                                          00002880
             03 MOPERC  PIC X.                                          00002890
             03 MOPERP  PIC X.                                          00002900
             03 MOPERH  PIC X.                                          00002910
             03 MOPERV  PIC X.                                          00002920
             03 MOPERO  PIC X(4).                                       00002930
           02 DFHMS4 OCCURS   8 TIMES .                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MNCDEWCA     PIC X.                                     00002960
             03 MNCDEWCC     PIC X.                                     00002970
             03 MNCDEWCP     PIC X.                                     00002980
             03 MNCDEWCH     PIC X.                                     00002990
             03 MNCDEWCV     PIC X.                                     00003000
             03 MNCDEWCO     PIC Z(7)9.                                 00003010
           02 DFHMS5 OCCURS   8 TIMES .                                 00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MNSOCIETEA   PIC X.                                     00003040
             03 MNSOCIETEC   PIC X.                                     00003050
             03 MNSOCIETEP   PIC X.                                     00003060
             03 MNSOCIETEH   PIC X.                                     00003070
             03 MNSOCIETEV   PIC X.                                     00003080
             03 MNSOCIETEO   PIC X(3).                                  00003090
           02 DFHMS6 OCCURS   8 TIMES .                                 00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MNLIEUA      PIC X.                                     00003120
             03 MNLIEUC PIC X.                                          00003130
             03 MNLIEUP PIC X.                                          00003140
             03 MNLIEUH PIC X.                                          00003150
             03 MNLIEUV PIC X.                                          00003160
             03 MNLIEUO      PIC X(3).                                  00003170
           02 DFHMS7 OCCURS   8 TIMES .                                 00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MNVENTEA     PIC X.                                     00003200
             03 MNVENTEC     PIC X.                                     00003210
             03 MNVENTEP     PIC X.                                     00003220
             03 MNVENTEH     PIC X.                                     00003230
             03 MNVENTEV     PIC X.                                     00003240
             03 MNVENTEO     PIC X(7).                                  00003250
           02 DFHMS8 OCCURS   8 TIMES .                                 00003260
             03 FILLER       PIC X(2).                                  00003270
             03 MNOMA   PIC X.                                          00003280
             03 MNOMC   PIC X.                                          00003290
             03 MNOMP   PIC X.                                          00003300
             03 MNOMH   PIC X.                                          00003310
             03 MNOMV   PIC X.                                          00003320
             03 MNOMO   PIC X(24).                                      00003330
           02 DFHMS9 OCCURS   8 TIMES .                                 00003340
             03 FILLER       PIC X(2).                                  00003350
             03 MDCREA  PIC X.                                          00003360
             03 MDCREC  PIC X.                                          00003370
             03 MDCREP  PIC X.                                          00003380
             03 MDCREH  PIC X.                                          00003390
             03 MDCREV  PIC X.                                          00003400
             03 MDCREO  PIC X(8).                                       00003410
           02 DFHMS10 OCCURS   8 TIMES .                                00003420
             03 FILLER       PIC X(2).                                  00003430
             03 MHCREA  PIC X.                                          00003440
             03 MHCREC  PIC X.                                          00003450
             03 MHCREP  PIC X.                                          00003460
             03 MHCREH  PIC X.                                          00003470
             03 MHCREV  PIC X.                                          00003480
             03 MHCREO  PIC X(5).                                       00003490
           02 DFHMS11 OCCURS   8 TIMES .                                00003500
             03 FILLER       PIC X(2).                                  00003510
             03 MSTATTA      PIC X.                                     00003520
             03 MSTATTC PIC X.                                          00003530
             03 MSTATTP PIC X.                                          00003540
             03 MSTATTH PIC X.                                          00003550
             03 MSTATTV PIC X.                                          00003560
             03 MSTATTO      PIC X.                                     00003570
           02 DFHMS12 OCCURS   8 TIMES .                                00003580
             03 FILLER       PIC X(2).                                  00003590
             03 MTEXTETA     PIC X.                                     00003600
             03 MTEXTETC     PIC X.                                     00003610
             03 MTEXTETP     PIC X.                                     00003620
             03 MTEXTETH     PIC X.                                     00003630
             03 MTEXTETV     PIC X.                                     00003640
             03 MTEXTETO     PIC X(60).                                 00003650
           02 DFHMS13 OCCURS   8 TIMES .                                00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MDCRETA      PIC X.                                     00003680
             03 MDCRETC PIC X.                                          00003690
             03 MDCRETP PIC X.                                          00003700
             03 MDCRETH PIC X.                                          00003710
             03 MDCRETV PIC X.                                          00003720
             03 MDCRETO      PIC X(8).                                  00003730
           02 DFHMS14 OCCURS   8 TIMES .                                00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MHCRETA      PIC X.                                     00003760
             03 MHCRETC PIC X.                                          00003770
             03 MHCRETP PIC X.                                          00003780
             03 MHCRETH PIC X.                                          00003790
             03 MHCRETV PIC X.                                          00003800
             03 MHCRETO      PIC X(5).                                  00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MLIBERRA  PIC X.                                          00003830
           02 MLIBERRC  PIC X.                                          00003840
           02 MLIBERRP  PIC X.                                          00003850
           02 MLIBERRH  PIC X.                                          00003860
           02 MLIBERRV  PIC X.                                          00003870
           02 MLIBERRO  PIC X(79).                                      00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MCODTRAA  PIC X.                                          00003900
           02 MCODTRAC  PIC X.                                          00003910
           02 MCODTRAP  PIC X.                                          00003920
           02 MCODTRAH  PIC X.                                          00003930
           02 MCODTRAV  PIC X.                                          00003940
           02 MCODTRAO  PIC X(4).                                       00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MZONCMDA  PIC X.                                          00003970
           02 MZONCMDC  PIC X.                                          00003980
           02 MZONCMDP  PIC X.                                          00003990
           02 MZONCMDH  PIC X.                                          00004000
           02 MZONCMDV  PIC X.                                          00004010
           02 MZONCMDO  PIC X(15).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCICSA    PIC X.                                          00004040
           02 MCICSC    PIC X.                                          00004050
           02 MCICSP    PIC X.                                          00004060
           02 MCICSH    PIC X.                                          00004070
           02 MCICSV    PIC X.                                          00004080
           02 MCICSO    PIC X(5).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MNETNAMA  PIC X.                                          00004110
           02 MNETNAMC  PIC X.                                          00004120
           02 MNETNAMP  PIC X.                                          00004130
           02 MNETNAMH  PIC X.                                          00004140
           02 MNETNAMV  PIC X.                                          00004150
           02 MNETNAMO  PIC X(8).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MSCREENA  PIC X.                                          00004180
           02 MSCREENC  PIC X.                                          00004190
           02 MSCREENP  PIC X.                                          00004200
           02 MSCREENH  PIC X.                                          00004210
           02 MSCREENV  PIC X.                                          00004220
           02 MSCREENO  PIC X(4).                                       00004230
                                                                                
