      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMPM10.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *      COMMAREA DE LA TRANSACTION PM00 PARAMETRES MAGASIN        *        
      *                                                                *        
      *      PARTIES APPLICATIVES COMMUNES AUX TRANSACTION :           *        
      *      PM10                                                      *        
      *                                                                *        
      *      LONGUEUR = 1 - ZONES COMMUNES               372 C         *        
      *                 2 - ZONES APPLICATIVES PM       3724 C         *        
      *                     TOTAL                       4096           *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      ******************************************************************        
      *    ZONES APPLICATIVES DES TRANSACTION PM00,PM10,     --- 199 C *        
      ******************************************************************        
      *                                                                         
           02 COMM-PM00-ZONES   REDEFINES  COMM-PM00-APPLI.                     
      *                                    NOM DU MENU APPELANT                 
              05 COMM-PM00-CMENU           PIC X(05).                           
      *                                    MAGASIN DE RECEPTION                 
              05 COMM-PM00-NSOCIETE        PIC X(03).                           
              05 COMM-PM00-NLIEU           PIC X(03).                           
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DES DONNEES PARAMETRES   - TPM10 - 171 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-PM10-APPLI.                                               
                 10 COMM-PM10-CFONCT          PIC X(03).                00008601
                 10 COMM-PM10-NSOCIETE        PIC X(03).                        
                 10 COMM-PM10-NLIEU           PIC X(03).                00008101
                 10 COMM-PM10-LSOCIETE        PIC X(20).                00008101
                 10 COMM-PM10-LLIEU           PIC X(20).                00008101
                 10 COMM-PM10-CODE-RETOUR.                                      
                    15 COMM-PM10-CODRET       PIC X(01).                        
                    15 COMM-PM10-LIBERR       PIC X(58).                        
      *                                                                         
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 2918 C*         
      ******************************************************************        
      *                                                                         
              05 COMM-PM00-FILLER          PIC X(2918).                         
      *                                                                         
                                                                                
