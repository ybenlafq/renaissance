      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAC21   EAC21                                              00000020
      ***************************************************************** 00000030
       01   EAC21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCL      COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MLCL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MLCF      PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MLCI      PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MJOURI    PIC X(10).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MHMAJL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MHMAJL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MHMAJF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MHMAJI    PIC X(6).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMAGL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MCMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCMAGF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MCMAGI    PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLMAGI    PIC X(21).                                      00000350
      * chiffe affaires ttc tlm                                         00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATLML   COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCATLML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCATLMF   PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCATLMI   PIC X(7).                                       00000400
      * ca ttc ela                                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAELAL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCAELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCAELAF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCAELAI   PIC X(7).                                       00000450
      * ca dacem                                                        00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCADACL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MCADACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCADACF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MCADACI   PIC X(6).                                       00000500
      * chiffre affaire total ela+tlm+dacem                             00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATOTL   COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MCATOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCATOTF   PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MCATOTI   PIC X(7).                                       00000550
      * nombre piece tlm                                                00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTLML   COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MNBTLML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBTLMF   PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MNBTLMI   PIC X(4).                                       00000600
      * nombre piece ela                                                00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBELAL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNBELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBELAF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNBELAI   PIC X(4).                                       00000650
      * nombre piece dacem                                              00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBDACL   COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MNBDACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBDACF   PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MNBDACI   PIC X(4).                                       00000700
      * nombre transaction darty                                        00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRDARL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MTRDARL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTRDARF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MTRDARI   PIC X(4).                                       00000750
      * nombre transaction dacem                                        00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRDACL   COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MTRDACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTRDACF   PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MTRDACI   PIC X(4).                                       00000800
      * nbre entree client                                              00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBENTL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNBENTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBENTF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNBENTI   PIC X(4).                                       00000850
      * taux concretisation                                             00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTXCRL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MTXCRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTXCRF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MTXCRI    PIC X(5).                                       00000900
      * prix vente moyen                                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPVML     COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MPVML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MPVMF     PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MPVMI     PIC X(4).                                       00000950
      * nombre jour vendeur                                             00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBVDL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MNBVDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBVDF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNBVDI    PIC X(4).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCN1L    COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MLCN1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLCN1F    PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MLCN1I    PIC X(3).                                       00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURN1L  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MJOURN1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJOURN1F  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MJOURN1I  PIC X(10).                                      00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECATLML  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MECATLML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECATLMF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MECATLMI  PIC X(7).                                       00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECAELAL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MECAELAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECAELAF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MECAELAI  PIC X(7).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECADACL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MECADACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECADACF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MECADACI  PIC X(7).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECATOTL  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MECATOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MECATOTF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MECATOTI  PIC X(7).                                       00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENBENTL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MENBENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MENBENTF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MENBENTI  PIC X(6).                                       00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METXCRL   COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 METXCRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 METXCRF   PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 METXCRI   PIC X(5).                                       00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MZONCMDI  PIC X(15).                                      00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MLIBERRI  PIC X(54).                                      00001400
      * CODE TRANSACTION                                                00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCODTRAI  PIC X(4).                                       00001450
      * NOM DU CICS                                                     00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      * NETNAME                                                         00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MNETNAMI  PIC X(8).                                       00001550
      * CODE TERMINAL                                                   00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001570
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001580
           02 FILLER    PIC X(4).                                       00001590
           02 MSCREENI  PIC X(5).                                       00001600
      ***************************************************************** 00001610
      * SDF: EAC21   EAC21                                              00001620
      ***************************************************************** 00001630
       01   EAC21O REDEFINES EAC21I.                                    00001640
           02 FILLER    PIC X(12).                                      00001650
      * DATE DU JOUR                                                    00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MDATJOUA  PIC X.                                          00001680
           02 MDATJOUC  PIC X.                                          00001690
           02 MDATJOUP  PIC X.                                          00001700
           02 MDATJOUH  PIC X.                                          00001710
           02 MDATJOUV  PIC X.                                          00001720
           02 MDATJOUO  PIC X(10).                                      00001730
      * HEURE                                                           00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MTIMJOUA  PIC X.                                          00001760
           02 MTIMJOUC  PIC X.                                          00001770
           02 MTIMJOUP  PIC X.                                          00001780
           02 MTIMJOUH  PIC X.                                          00001790
           02 MTIMJOUV  PIC X.                                          00001800
           02 MTIMJOUO  PIC X(5).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLCA      PIC X.                                          00001830
           02 MLCC      PIC X.                                          00001840
           02 MLCP      PIC X.                                          00001850
           02 MLCH      PIC X.                                          00001860
           02 MLCV      PIC X.                                          00001870
           02 MLCO      PIC X(3).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MJOURA    PIC X.                                          00001900
           02 MJOURC    PIC X.                                          00001910
           02 MJOURP    PIC X.                                          00001920
           02 MJOURH    PIC X.                                          00001930
           02 MJOURV    PIC X.                                          00001940
           02 MJOURO    PIC X(10).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MHMAJA    PIC X.                                          00001970
           02 MHMAJC    PIC X.                                          00001980
           02 MHMAJP    PIC X.                                          00001990
           02 MHMAJH    PIC X.                                          00002000
           02 MHMAJV    PIC X.                                          00002010
           02 MHMAJO    PIC X(6).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MCMAGA    PIC X.                                          00002040
           02 MCMAGC    PIC X.                                          00002050
           02 MCMAGP    PIC X.                                          00002060
           02 MCMAGH    PIC X.                                          00002070
           02 MCMAGV    PIC X.                                          00002080
           02 MCMAGO    PIC X(3).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MLMAGA    PIC X.                                          00002110
           02 MLMAGC    PIC X.                                          00002120
           02 MLMAGP    PIC X.                                          00002130
           02 MLMAGH    PIC X.                                          00002140
           02 MLMAGV    PIC X.                                          00002150
           02 MLMAGO    PIC X(21).                                      00002160
      * chiffe affaires ttc tlm                                         00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCATLMA   PIC X.                                          00002190
           02 MCATLMC   PIC X.                                          00002200
           02 MCATLMP   PIC X.                                          00002210
           02 MCATLMH   PIC X.                                          00002220
           02 MCATLMV   PIC X.                                          00002230
           02 MCATLMO   PIC ZZZZZZ9.                                    00002240
      * ca ttc ela                                                      00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MCAELAA   PIC X.                                          00002270
           02 MCAELAC   PIC X.                                          00002280
           02 MCAELAP   PIC X.                                          00002290
           02 MCAELAH   PIC X.                                          00002300
           02 MCAELAV   PIC X.                                          00002310
           02 MCAELAO   PIC ZZZZZZ9.                                    00002320
      * ca dacem                                                        00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCADACA   PIC X.                                          00002350
           02 MCADACC   PIC X.                                          00002360
           02 MCADACP   PIC X.                                          00002370
           02 MCADACH   PIC X.                                          00002380
           02 MCADACV   PIC X.                                          00002390
           02 MCADACO   PIC ZZZZZ9.                                     00002400
      * chiffre affaire total ela+tlm+dacem                             00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCATOTA   PIC X.                                          00002430
           02 MCATOTC   PIC X.                                          00002440
           02 MCATOTP   PIC X.                                          00002450
           02 MCATOTH   PIC X.                                          00002460
           02 MCATOTV   PIC X.                                          00002470
           02 MCATOTO   PIC ZZZZZZ9.                                    00002480
      * nombre piece tlm                                                00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNBTLMA   PIC X.                                          00002510
           02 MNBTLMC   PIC X.                                          00002520
           02 MNBTLMP   PIC X.                                          00002530
           02 MNBTLMH   PIC X.                                          00002540
           02 MNBTLMV   PIC X.                                          00002550
           02 MNBTLMO   PIC ZZZ9.                                       00002560
      * nombre piece ela                                                00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MNBELAA   PIC X.                                          00002590
           02 MNBELAC   PIC X.                                          00002600
           02 MNBELAP   PIC X.                                          00002610
           02 MNBELAH   PIC X.                                          00002620
           02 MNBELAV   PIC X.                                          00002630
           02 MNBELAO   PIC ZZZ9.                                       00002640
      * nombre piece dacem                                              00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MNBDACA   PIC X.                                          00002670
           02 MNBDACC   PIC X.                                          00002680
           02 MNBDACP   PIC X.                                          00002690
           02 MNBDACH   PIC X.                                          00002700
           02 MNBDACV   PIC X.                                          00002710
           02 MNBDACO   PIC ZZZ9.                                       00002720
      * nombre transaction darty                                        00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MTRDARA   PIC X.                                          00002750
           02 MTRDARC   PIC X.                                          00002760
           02 MTRDARP   PIC X.                                          00002770
           02 MTRDARH   PIC X.                                          00002780
           02 MTRDARV   PIC X.                                          00002790
           02 MTRDARO   PIC ZZZ9.                                       00002800
      * nombre transaction dacem                                        00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MTRDACA   PIC X.                                          00002830
           02 MTRDACC   PIC X.                                          00002840
           02 MTRDACP   PIC X.                                          00002850
           02 MTRDACH   PIC X.                                          00002860
           02 MTRDACV   PIC X.                                          00002870
           02 MTRDACO   PIC ZZZ9.                                       00002880
      * nbre entree client                                              00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MNBENTA   PIC X.                                          00002910
           02 MNBENTC   PIC X.                                          00002920
           02 MNBENTP   PIC X.                                          00002930
           02 MNBENTH   PIC X.                                          00002940
           02 MNBENTV   PIC X.                                          00002950
           02 MNBENTO   PIC ZZZ9.                                       00002960
      * taux concretisation                                             00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MTXCRA    PIC X.                                          00002990
           02 MTXCRC    PIC X.                                          00003000
           02 MTXCRP    PIC X.                                          00003010
           02 MTXCRH    PIC X.                                          00003020
           02 MTXCRV    PIC X.                                          00003030
           02 MTXCRO    PIC ZZ9,9.                                      00003040
      * prix vente moyen                                                00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MPVMA     PIC X.                                          00003070
           02 MPVMC     PIC X.                                          00003080
           02 MPVMP     PIC X.                                          00003090
           02 MPVMH     PIC X.                                          00003100
           02 MPVMV     PIC X.                                          00003110
           02 MPVMO     PIC ZZZ9.                                       00003120
      * nombre jour vendeur                                             00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MNBVDA    PIC X.                                          00003150
           02 MNBVDC    PIC X.                                          00003160
           02 MNBVDP    PIC X.                                          00003170
           02 MNBVDH    PIC X.                                          00003180
           02 MNBVDV    PIC X.                                          00003190
           02 MNBVDO    PIC Z9,9.                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MLCN1A    PIC X.                                          00003220
           02 MLCN1C    PIC X.                                          00003230
           02 MLCN1P    PIC X.                                          00003240
           02 MLCN1H    PIC X.                                          00003250
           02 MLCN1V    PIC X.                                          00003260
           02 MLCN1O    PIC X(3).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MJOURN1A  PIC X.                                          00003290
           02 MJOURN1C  PIC X.                                          00003300
           02 MJOURN1P  PIC X.                                          00003310
           02 MJOURN1H  PIC X.                                          00003320
           02 MJOURN1V  PIC X.                                          00003330
           02 MJOURN1O  PIC X(10).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MECATLMA  PIC X.                                          00003360
           02 MECATLMC  PIC X.                                          00003370
           02 MECATLMP  PIC X.                                          00003380
           02 MECATLMH  PIC X.                                          00003390
           02 MECATLMV  PIC X.                                          00003400
           02 MECATLMO  PIC X(7).                                       00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MECAELAA  PIC X.                                          00003430
           02 MECAELAC  PIC X.                                          00003440
           02 MECAELAP  PIC X.                                          00003450
           02 MECAELAH  PIC X.                                          00003460
           02 MECAELAV  PIC X.                                          00003470
           02 MECAELAO  PIC X(7).                                       00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MECADACA  PIC X.                                          00003500
           02 MECADACC  PIC X.                                          00003510
           02 MECADACP  PIC X.                                          00003520
           02 MECADACH  PIC X.                                          00003530
           02 MECADACV  PIC X.                                          00003540
           02 MECADACO  PIC X(7).                                       00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MECATOTA  PIC X.                                          00003570
           02 MECATOTC  PIC X.                                          00003580
           02 MECATOTP  PIC X.                                          00003590
           02 MECATOTH  PIC X.                                          00003600
           02 MECATOTV  PIC X.                                          00003610
           02 MECATOTO  PIC X(7).                                       00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MENBENTA  PIC X.                                          00003640
           02 MENBENTC  PIC X.                                          00003650
           02 MENBENTP  PIC X.                                          00003660
           02 MENBENTH  PIC X.                                          00003670
           02 MENBENTV  PIC X.                                          00003680
           02 MENBENTO  PIC X(6).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 METXCRA   PIC X.                                          00003710
           02 METXCRC   PIC X.                                          00003720
           02 METXCRP   PIC X.                                          00003730
           02 METXCRH   PIC X.                                          00003740
           02 METXCRV   PIC X.                                          00003750
           02 METXCRO   PIC X(5).                                       00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MZONCMDA  PIC X.                                          00003780
           02 MZONCMDC  PIC X.                                          00003790
           02 MZONCMDP  PIC X.                                          00003800
           02 MZONCMDH  PIC X.                                          00003810
           02 MZONCMDV  PIC X.                                          00003820
           02 MZONCMDO  PIC X(15).                                      00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MLIBERRA  PIC X.                                          00003850
           02 MLIBERRC  PIC X.                                          00003860
           02 MLIBERRP  PIC X.                                          00003870
           02 MLIBERRH  PIC X.                                          00003880
           02 MLIBERRV  PIC X.                                          00003890
           02 MLIBERRO  PIC X(54).                                      00003900
      * CODE TRANSACTION                                                00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MCODTRAA  PIC X.                                          00003930
           02 MCODTRAC  PIC X.                                          00003940
           02 MCODTRAP  PIC X.                                          00003950
           02 MCODTRAH  PIC X.                                          00003960
           02 MCODTRAV  PIC X.                                          00003970
           02 MCODTRAO  PIC X(4).                                       00003980
      * NOM DU CICS                                                     00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MCICSA    PIC X.                                          00004010
           02 MCICSC    PIC X.                                          00004020
           02 MCICSP    PIC X.                                          00004030
           02 MCICSH    PIC X.                                          00004040
           02 MCICSV    PIC X.                                          00004050
           02 MCICSO    PIC X(5).                                       00004060
      * NETNAME                                                         00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MNETNAMA  PIC X.                                          00004090
           02 MNETNAMC  PIC X.                                          00004100
           02 MNETNAMP  PIC X.                                          00004110
           02 MNETNAMH  PIC X.                                          00004120
           02 MNETNAMV  PIC X.                                          00004130
           02 MNETNAMO  PIC X(8).                                       00004140
      * CODE TERMINAL                                                   00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MSCREENA  PIC X.                                          00004170
           02 MSCREENC  PIC X.                                          00004180
           02 MSCREENP  PIC X.                                          00004190
           02 MSCREENH  PIC X.                                          00004200
           02 MSCREENV  PIC X.                                          00004210
           02 MSCREENO  PIC X(5).                                       00004220
                                                                                
