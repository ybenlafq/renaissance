      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVLT0200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVLT0200                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLT0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLT0200.                                                            
      *}                                                                        
           02  LT02-NLETTRE                                                     
               PIC X(0006).                                                     
           02  LT02-NPAGE                                                       
               PIC X(0002).                                                     
           02  LT02-NLIGNE                                                      
               PIC X(0002).                                                     
           02  LT02-INDVAR                                                      
               PIC X(0002).                                                     
           02  LT02-NUMVAR                                                      
               PIC X(0004).                                                     
           02  LT02-POSVAR                                                      
               PIC X(0002).                                                     
           02  LT02-LGVAR                                                       
               PIC X(0002).                                                     
           02  LT02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVLT0200                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVLT0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVLT0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-NLETTRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-NLETTRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-NPAGE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-NPAGE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-INDVAR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-INDVAR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-NUMVAR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-NUMVAR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-POSVAR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-POSVAR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-LGVAR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-LGVAR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  LT02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  LT02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
