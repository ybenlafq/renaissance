      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMMAREA AC30 - RESULTATS                                               
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-AC00-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
       01  Z-COMMAREA.                                                          
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
          02 FILLER-COM-AIDA      PIC X(100).                                   
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
          02 COMM-CICS-APPLID     PIC X(8).                                     
          02 COMM-CICS-NETNAM     PIC X(8).                                     
          02 COMM-CICS-TRANSA     PIC X(4).                                     
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
          02 COMM-DATE-SIECLE     PIC XX.                                       
          02 COMM-DATE-ANNEE      PIC XX.                                       
          02 COMM-DATE-MOIS       PIC XX.                                       
          02 COMM-DATE-JOUR       PIC XX.                                       
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
          02 COMM-DATE-QNTA       PIC 999.                                      
          02 COMM-DATE-QNT0       PIC 99999.                                    
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
          02 COMM-DATE-BISX       PIC 9.                                        
      *   JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                   
          02 COMM-DATE-JSM        PIC 9.                                        
      *   LIBELLES DU JOUR COURT - LONG                                         
          02 COMM-DATE-JSM-LC     PIC XXX.                                      
          02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                 
      *   LIBELLES DU MOIS COURT - LONG                                         
          02 COMM-DATE-MOIS-LC    PIC XXX.                                      
          02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                 
      *   DIFFERENTES FORMES DE DATE                                            
          02 COMM-DATE-SSAAMMJJ   PIC X(8).                                     
          02 COMM-DATE-AAMMJJ     PIC X(6).                                     
          02 COMM-DATE-JJMMSSAA   PIC X(8).                                     
          02 COMM-DATE-JJMMAA     PIC X(6).                                     
          02 COMM-DATE-JJ-MM-AA   PIC X(8).                                     
          02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                    
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
          02 COMM-DATE-WEEK.                                                    
             05 COMM-DATE-SEMSS   PIC 9(02).                                    
             05 COMM-DATE-SEMAA   PIC 9(02).                                    
             05 COMM-DATE-SEMNU   PIC 9(02).                                    
          02 COMM-DATE-FILLER     PIC X(08).                                    
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                      
      *--                                                                       
          02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                    
      *}                                                                        
          02 COMM-SWAP-ATTR OCCURS 200 PIC X(1).                                
      ******************************************************************        
      * APPLI AC30                                                              
      ******************************************************************        
          02 COMM-AC30-APPLI.                                                   
             03 COMM-AC30-CACID          PIC X(8).                              
             03 COMM-AC30-DATEM          PIC X.                                 
             03 COMM-AC30-NSOCIETE       PIC X(3).                              
             03 COMM-AC30-NLIEU          PIC X(3).                              
             03 COMM-AC30-LLIEU          PIC X(20).                             
             03 COMM-AC30-PAGE           PIC 9(2).                              
             03 COMM-AC30-NBP            PIC 9(2).                              
             03 COMM-AC30-NORDRE-TS      PIC S9(4) COMP-3.                      
             03 COMM-AC30-DDEBUT         PIC X(8).                              
             03 COMM-AC30-JDEBUT         PIC X(8).                              
             03 COMM-AC30-DFIN           PIC X(8).                              
             03  COMM-AC30-DELAI         PIC 9(3).                              
             03 COMM-AC30-ATT-EN-COURS   PIC X(10).                             
             03 COMM-AC30-MODE           PIC X.                                 
                88 COMM-AC30-MODE-ATT     VALUE 'C'.                            
                88 COMM-AC30-MODE-VENDEUR VALUE 'V'.                            
             03 COMM-AC30-ATT    OCCURS 99.                                     
                10 COMM-AC30-ATT-LIEU          PIC X(10).                       
                10 COMM-AC30-ATT-CACID         PIC X(8).                        
      *      12 ATT MAX MAIS 15 POSTES POUR COLLER A L'ECRAN                    
             03 COMM-AC30-ATT    OCCURS 15.                                     
                10 COMM-AC30-ATT-ATT           PIC X(10).                       
                10 COMM-AC30-ATT-CATPM   PIC S9(7) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-CAACC   PIC S9(7) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-CADAC   PIC S9(7) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-CATOT   PIC S9(7) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-PSE     PIC S9(5)V99 PACKED-DECIMAL.           
                10 COMM-AC30-ATT-REM     PIC S9(5)V99 PACKED-DECIMAL.           
                10 COMM-AC30-ATT-VOL     PIC S9(5) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-VOL-PSA PIC S9(5) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-NBVT    PIC S9(7) PACKED-DECIMAL.              
                10 COMM-AC30-ATT-NBVTC   PIC S9(7) PACKED-DECIMAL.              
                                                                                
