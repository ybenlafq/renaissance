      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:13 >
      
      *****************************************************************
      *   COPY MESSAGES MQ RECU PAR LE HOST
      *   PAR LE PROGRAMME MSL12 TAILLE COPIE 30179 MESSAGE 30157
      *****************************************************************
      *     M O D I F I C A T I O N S  /  M A I N T E N A N C E S      *
      ******************************************************************
      *  SSAAMMJJ / AUTEUR / REPAIRE / DESCRIPTIF MAINTENANCE          *
      *  -------- / ------ / ------- / ------------------------------- *
      *  20030922 / DSA010 / LP01    / MODIFICATIONS POUR PASSAGE MQHI *
      ******************************************************************
      *
      * ENTETE QUEUE (CORRELID) + ENTETE STANDARD              26 + 105
      *    COPY MESSLENT.
      *****************************************************************
      *   COPY ENTETE DES MESSAGES MQ RECUPERES PAR LE HOST
      *****************************************************************
      *
       01  WS-MQ10.
      * ZONES D'ECHANGE AVEC MQHI (IDENTIFIANT MSG / CODE RET TRT)
         02 WS-QUEUE.
               10   MQ10-MSGID       PIC  X(24).
               10   MQ10-CORRELID    PIC  X(24).
         02 WS-CODRET                PIC  XX.
      * DESCRIPTION DU MSG
         02 MESSL.
      *    ENTETE STANDARD                                       LG=105C
           05  MESSL-ENTETE.
               10   MESSL-TYPE       PIC  X(03).
               10   MESSL-NSOCMSG    PIC  X(03).
               10   MESSL-NLIEUMSG   PIC  X(03).
               10   MESSL-NSOCDST    PIC  X(03).
               10   MESSL-NLIEUDST   PIC  X(03).
               10   MESSL-NORD       PIC  9(08).
               10   MESSL-LPROG      PIC  X(10).
               10   MESSL-DJOUR      PIC  X(08).
               10   MESSL-WSID       PIC  X(10).
               10   MESSL-USER       PIC  X(10).
               10   MESSL-CHRONO     PIC  9(07).
               10   MESSL-NBRMSG     PIC  9(07).
               10   MESSL-NBRENR     PIC  9(05).
               10   MESSL-TAILLE     PIC  9(05).
               10   MESSL-VERSION    PIC  X(02).
               10   MESSL-DSYST      PIC S9(13).
               10   MESSL-FILLER     PIC  X(05).
      *
      *---- FIN COPIE
           05  MESSL12-DATA.
      * ENTETE SL                                                    48
               10   MESSL12-W-NSOCCRE   PIC X(03).
               10   MESSL12-W-NLIEUCRE  PIC X(03).
               10   MESSL12-W-CTYPDOC   PIC X(02).
               10   MESSL12-W-NUMDOC    PIC 9(07).
               10   MESSL12-W-DOPERREC  PIC X(08).
               10   MESSL12-W-DHOPERREC PIC X(08).
               10   MESSL12-W-CACIDREC  PIC X(10).
      *        CODE ACTION
      *        TRAITEMENT : E, R OU A
      *        LIGNES : N --> ON TRAITE TOUT LE DOCUMENT ET LES LIGNES
      *                       EVENTUELLEMENT EN ECART (DANS -W-DATA)
      *                 O --> ON NEGLIGE L'ENTETE ET ON NE TRAITE
      *                       QUE LES LIGNES DE -W-DATA, EN FONCTION
      *                       DUCODE TRAITEMENT
               10  MESSL12-W-ACTION.
                  15 MESSL12-W-TRAIT       PIC X(01).
                  15 MESSL12-W-LIGNES      PIC X(01).
                  15 MESSL12-W-COPAR       PIC X(05).
      * LIGNES DE FEMSL10                                          30000
               10   MESSL12-W-DATA PIC X(30000).
      
