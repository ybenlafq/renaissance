      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION STOCK LOCAL : REPONSE DU LOCAL         
      *                                                                         
      *1213 DE02009 PROJET EMPORT� SUR STOCK PTF                                
      *****************************************************************         
      * VERSION POUR MGV48 AVEC PUT ET GET EXTERNES                             
      *                                                                         
           10  WS-MESSAGE-RECU REDEFINES COMM-MQ12-MESSAGE.                     
      *---                                                                      
           17  MESR-ENTETE.                                                     
               20   MESR-TYPE     PIC    X(3).                                  
               20   MESR-NSOCMSG  PIC    X(3).                                  
               20   MESR-NLIEUMSG PIC    X(3).                                  
               20   MESR-NSOCDST  PIC    X(3).                                  
               20   MESR-NLIEUDST PIC    X(3).                                  
               20   MESR-NORD     PIC    9(8).                                  
               20   MESR-LPROG    PIC    X(10).                                 
               20   MESR-DJOUR    PIC    X(8).                                  
               20   MESR-WSID     PIC    X(10).                                 
               20   MESR-USER     PIC    X(10).                                 
               20   MESR-CHRONO   PIC    9(7).                                  
               20   MESR-NBRMSG   PIC    9(7).                                  
               20   MESR-FILLER   PIC    X(30).                                 
           17  MESR-QSTOCK-LOCAL OCCURS 40.                                     
               20   MESR-NCODIC    PIC    X(7).                                 
               20   MESR-QSTOCK    PIC    S9(5).                                
               20   MESR-NLIGNE    PIC    X(2).                                 
      *                                                                         
 1213 * PARTIE REPONSE POUR LA R�SERVATION DE STOCK                             
  "   *                                                                         
  "        10  WS-MESS-R  REDEFINES COMM-MQ12-MESSAGE.                          
  "   * =========   ENTETE DU MESSAGE:   ===========                            
  "            15 MESS-R-ENTETE        PIC X(105).                              
  "   * =========   CORPS DU MESSAGE:   ===========                             
  "            15 TABL-R OCCURS 40.                                             
  "   *      CODIC                                                              
  "               20 MESS-R-NCODIC     PIC X(7).                                
  "   *      N� LIGNE HOST                                                      
  "               20 MESS-R-NSEQNQ     PIC 9(5).                                
  "   *      ERREUR MESSAGE                                                     
  "               20 MESS-R-ERRMSG     PIC X(60).                               
  "   *      STATUT                                                             
  "            15 MESS-R-STATUT     PIC 9(01).                                  
  "              88 MESS-R-STATUT-OK VALUE 0.                                   
  "              88 MESS-R-STATUT-KO VALUE 1.                                   
      *                                                                         
                                                                                
