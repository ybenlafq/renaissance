      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTSV04                        *        
      ******************************************************************        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVSV0400.                                                            
      *    *************************************************************        
      *                       CODE PRODUIT                                      
           10 SV04-NCODIC          PIC X(7).                                    
      *    *************************************************************        
      *                       CODE CHASSIS                                      
           10 SV04-CCHASSIS        PIC X(7).                                    
      *    *************************************************************        
      *                       DATE SYSTEME                                      
           10 SV04-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *        
      ******************************************************************        
       01  RVSV0400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV04-NCODIC-F   PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV04-NCODIC-F   PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV04-CCHASSIS-F PIC S9(4) COMP.                                   
      *--                                                                       
           10 SV04-CCHASSIS-F PIC S9(4) COMP-5.                                 
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SV04-DSYST-F    PIC S9(4) COMP.                                   
      *                                                                         
      *--                                                                       
           10 SV04-DSYST-F    PIC S9(4) COMP-5.                                 
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
