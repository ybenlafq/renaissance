      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:11 >
      
          01 COMM-HV48-DONNEES.
             10 COMM-HV48-NB-POSTES               PIC S9(09) COMP-3.
             10 COMM-HV48-CODRET                  PIC 9(03) COMP-3.
                88 COMM-HV48-ERREUR-MANIP         VALUE 999.
             10 COMM-HV48-LIBERR                  PIC X(25).
             10 COMM-HV48-CRITERES.
                20 COMM-HV48-NSOCIETE             PIC X(03).
                20 COMM-HV48-NLIEU                PIC X(03).
                20 COMM-HV48-DVENTE               PIC X(08).
                20 COMM-HV48-LNOM                 PIC X(25).
                20 COMM-HV48-CMARQ                PIC X(05).
                20 COMM-HV48-CFAM                 PIC X(05).
                20 COMM-HV48-DDEBUT               PIC X(08).
                20 COMM-HV48-DFIN                 PIC X(08).
                20 COMM-HV48-NCAISSE              PIC X(03).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *         20 COMM-HV48-MONTANT              PIC S9(7)V99 COMP.
      *--
                20 COMM-HV48-MONTANT              PIC S9(7)V99 COMP-5.
      *}
                20 COMM-HV48-TPAIE                PIC X(01).
                20 COMM-HV48-TTRANS               PIC X(01).
                20 COMM-HV48-NCODIC               PIC X(07).
             10 COMM-HV48-TYPE-RECHERCHE          PIC X.
                88 COMM-HV48-PAS-RECH                  VALUE ' '.
                88 COMM-HV48-RECH-DVENTE               VALUE '1'.
                88 COMM-HV48-RECH-NOM-MARQUE           VALUE '2'.
                88 COMM-HV48-RECH-NOM-FAMILLE          VALUE '3'.
                88 COMM-HV48-RECH-NOM-MARQ-FAM         VALUE '4'.
                88 COMM-HV48-RECH-NOM               VALUE '2' '3' '4'.
                88 COMM-HV48-RECH-NTRANS               VALUE 'A'.
                88 COMM-HV48-RECH-TTRANS               VALUE 'B'.
                88 COMM-HV48-RECH-TTRANS-MONTANT       VALUE 'C'.
                88 COMM-HV48-RECH-TPAIE-MONTANT        VALUE 'D'.
                88 COMM-HV48-RECH-NCODIC               VALUE 'E'.
      
