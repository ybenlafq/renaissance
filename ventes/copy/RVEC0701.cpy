      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVEC0701                    *        
      ******************************************************************        
       01  RVEC0701.                                                            
           10 EC07-NSOCENTR        PIC X(3).                                    
           10 EC07-NDEPOT          PIC X(3).                                    
           10 EC07-NSOCIETE        PIC X(3).                                    
           10 EC07-NLIEU           PIC X(3).                                    
           10 EC07-DMUTATION       PIC X(8).                                    
           10 EC07-VERSION         PIC X(1).                                    
           10 EC07-TYPE-MUTATION   PIC X(5).                                    
           10 EC07-CSELART         PIC X(5).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
