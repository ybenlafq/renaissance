      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      * COBOL DECLARATION FOR TABLE PNEM.RVTD0300                      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTD0300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTD0300.                                                            
      *}                                                                        
           10 TD03-NSOCIETE             PIC X(3).                               
           10 TD03-NLIEU                PIC X(3).                               
           10 TD03-NVENTE               PIC X(7).                               
           10 TD03-NDOSSIER             PIC S9(3)V USAGE COMP-3.                
           10 TD03-NSEQ                 PIC S9(3)V USAGE COMP-3.                
           10 TD03-CODANO               PIC X(5).                               
           10 TD03-CODACT               PIC X(5).                               
           10 TD03-CODPRE               PIC X(5).                               
           10 TD03-LIEUACT              PIC X(1).                               
           10 TD03-WCONTROLE            PIC X(5).                               
           10 TD03-DCREATION            PIC X(8).                               
           10 TD03-DCORRECTION          PIC X(8).                               
           10 TD03-LCORRECTION          PIC X(1).                               
           10 TD03-PROVENANCE           PIC X(5).                               
           10 TD03-DSYST                PIC S9(13)V USAGE COMP-3.               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
