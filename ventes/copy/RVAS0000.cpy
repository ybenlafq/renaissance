      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAS0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAS0000                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAS0000.                                                            
           02  AS00-CCAMPAGNE                                                   
               PIC X(0004).                                                     
           02  AS00-LIBCAMP                                                     
               PIC X(0030).                                                     
           02  AS00-DDEBUT                                                      
               PIC X(0008).                                                     
           02  AS00-DFIN                                                        
               PIC X(0008).                                                     
           02  AS00-CMONO                                                       
               PIC X(0001).                                                     
           02  AS00-CMULTI                                                      
               PIC X(0001).                                                     
           02  AS00-DMINI                                                       
               PIC S9(7)V99 COMP-3.                                             
           02  AS00-DMAXI                                                       
               PIC S9(7)V99 COMP-3.                                             
           02  AS00-CTRANS1                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS2                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS3                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS4                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS5                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS6                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS7                                                     
               PIC X(0001).                                                     
           02  AS00-CTRANS8                                                     
               PIC X(0001).                                                     
           02  AS00-CMAG                                                        
               PIC X(0001).                                                     
           02  AS00-CDCOM                                                       
               PIC X(0001).                                                     
           02  AS00-CBTOB                                                       
               PIC X(0001).                                                     
           02  AS00-CMGD                                                        
               PIC X(0001).                                                     
           02  AS00-DCREATION                                                   
               PIC X(0008).                                                     
           02  AS00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAS0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAS0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CCAMPAGNE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CCAMPAGNE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-LIBCAMP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-LIBCAMP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DDEBUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DDEBUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DFIN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DFIN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CMONO-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CMONO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CMULTI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CMULTI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DMINI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DMINI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DMAXI-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DMAXI-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS3-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS3-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS4-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS4-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS5-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS5-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS6-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS6-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS7-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS7-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CTRANS8-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CTRANS8-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CDCOM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CDCOM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CBTOB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CBTOB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-CMGD-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-CMGD-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
