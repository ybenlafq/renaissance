      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL1600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL1600                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL1600.                                                            
           02 SL16-COPSL            PIC X(5).                                   
           02 SL16-LOPSL            PIC X(20).                                  
           02 SL16-CTYPDOC          PIC X(2).                                   
           02 SL16-WCONF            PIC X(1).                                   
           02 SL16-CTYPRS           PIC X(2).                                   
           02 SL16-WSUPPRS          PIC X(1).                                   
           02 SL16-WWVND            PIC X(1).                                   
           02 SL16-WWHS             PIC X(1).                                   
           02 SL16-WWUT             PIC X(1).                                   
           02 SL16-WREG             PIC X(1).                                   
           02 SL16-DSYST            PIC S9(13) COMP-3.                          
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVSL1600                                  
      *---------------------------------------------------------                
       01  RVSL1600-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-COPSL-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-COPSL-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-LOPSL-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-LOPSL-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-CTYPDOC-F        PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-CTYPDOC-F        PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WCONF-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WCONF-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-CTYPRS-F         PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-CTYPRS-F         PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WSUPPRS-F        PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WSUPPRS-F        PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WWVND-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WWVND-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WWHS-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WWHS-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WWUT-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WWUT-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-WREG-F           PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-WREG-F           PIC S9(04) COMP-5.                          
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 SL16-DSYST-F          PIC S9(04) COMP.                            
      *--                                                                       
           02 SL16-DSYST-F          PIC S9(04) COMP-5.                          
      *}                                                                        
      *                                                                         
                                                                                
