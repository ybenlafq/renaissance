      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPP20   EPP20                                              00000020
      ***************************************************************** 00000030
       01   EPP20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPTIONL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MLOPTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLOPTIONF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLOPTIONI      PIC X(20).                                 00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUORIGI    PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMOISPAYEL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDMOISPAYEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDMOISPAYEF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDMOISPAYEI    PIC X(6).                                  00000370
           02 M1LIGNEI OCCURS   10 TIMES .                              00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1NRUBL      COMP PIC S9(4).                            00000390
      *--                                                                       
             03 M1NRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M1NRUBF      PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 M1NRUBI      PIC X(2).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1LRUBL      COMP PIC S9(4).                            00000430
      *--                                                                       
             03 M1LRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M1LRUBF      PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 M1LRUBI      PIC X(19).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTANTZINL    COMP PIC S9(4).                       00000470
      *--                                                                       
             03 M1PMONTANTZINL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 M1PMONTANTZINF    PIC X.                                00000480
             03 FILLER  PIC X(4).                                       00000490
             03 M1PMONTANTZINI    PIC X(9).                             00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT1L  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 M1CPARENT1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT1F  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 M1CPARENT1I  PIC X.                                     00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTANTZ1L     COMP PIC S9(4).                       00000550
      *--                                                                       
             03 M1PMONTANTZ1L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M1PMONTANTZ1F     PIC X.                                00000560
             03 FILLER  PIC X(4).                                       00000570
             03 M1PMONTANTZ1I     PIC X(9).                             00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1COPER1L    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 M1COPER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M1COPER1F    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 M1COPER1I    PIC X(2).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTANTZ2L     COMP PIC S9(4).                       00000630
      *--                                                                       
             03 M1PMONTANTZ2L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M1PMONTANTZ2F     PIC X.                                00000640
             03 FILLER  PIC X(4).                                       00000650
             03 M1PMONTANTZ2I     PIC X(6).                             00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1COPER2L    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 M1COPER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M1COPER2F    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 M1COPER2I    PIC X(2).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PMONTANTZ3L     COMP PIC S9(4).                       00000710
      *--                                                                       
             03 M1PMONTANTZ3L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M1PMONTANTZ3F     PIC X.                                00000720
             03 FILLER  PIC X(4).                                       00000730
             03 M1PMONTANTZ3I     PIC X(6).                             00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1CPARENT2L  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 M1CPARENT2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M1CPARENT2F  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 M1CPARENT2I  PIC X(2).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M1PRESULTATL      COMP PIC S9(4).                       00000790
      *--                                                                       
             03 M1PRESULTATL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 M1PRESULTATF      PIC X.                                00000800
             03 FILLER  PIC X(4).                                       00000810
             03 M1PRESULTATI      PIC X(11).                            00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M1LTYPRUBL     COMP PIC S9(4).                            00000830
      *--                                                                       
           02 M1LTYPRUBL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 M1LTYPRUBF     PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 M1LTYPRUBI     PIC X(30).                                 00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M1PTOTALL      COMP PIC S9(4).                            00000870
      *--                                                                       
           02 M1PTOTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 M1PTOTALF      PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 M1PTOTALI      PIC X(11).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2LRUB1L  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 M2LRUB1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 M2LRUB1F  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 M2LRUB1I  PIC X(30).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2LRUB2L  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 M2LRUB2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 M2LRUB2F  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 M2LRUB2I  PIC X(30).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PMONTANTZ11L      COMP PIC S9(4).                       00000990
      *--                                                                       
           02 M2PMONTANTZ11L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2PMONTANTZ11F      PIC X.                                00001000
           02 FILLER    PIC X(4).                                       00001010
           02 M2PMONTANTZ11I      PIC X(9).                             00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2COPERATEUR1L      COMP PIC S9(4).                       00001030
      *--                                                                       
           02 M2COPERATEUR1L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2COPERATEUR1F      PIC X.                                00001040
           02 FILLER    PIC X(4).                                       00001050
           02 M2COPERATEUR1I      PIC X.                                00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PMONTANTZ21L      COMP PIC S9(4).                       00001070
      *--                                                                       
           02 M2PMONTANTZ21L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2PMONTANTZ21F      PIC X.                                00001080
           02 FILLER    PIC X(4).                                       00001090
           02 M2PMONTANTZ21I      PIC X(6).                             00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PRESULTAT1L  COMP PIC S9(4).                            00001110
      *--                                                                       
           02 M2PRESULTAT1L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 M2PRESULTAT1F  PIC X.                                     00001120
           02 FILLER    PIC X(4).                                       00001130
           02 M2PRESULTAT1I  PIC X(11).                                 00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PMONTANTZ12L      COMP PIC S9(4).                       00001150
      *--                                                                       
           02 M2PMONTANTZ12L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2PMONTANTZ12F      PIC X.                                00001160
           02 FILLER    PIC X(4).                                       00001170
           02 M2PMONTANTZ12I      PIC X(11).                            00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2COPERATEUR2L      COMP PIC S9(4).                       00001190
      *--                                                                       
           02 M2COPERATEUR2L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2COPERATEUR2F      PIC X.                                00001200
           02 FILLER    PIC X(4).                                       00001210
           02 M2COPERATEUR2I      PIC X.                                00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PMONTANTZ22L      COMP PIC S9(4).                       00001230
      *--                                                                       
           02 M2PMONTANTZ22L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 M2PMONTANTZ22F      PIC X.                                00001240
           02 FILLER    PIC X(4).                                       00001250
           02 M2PMONTANTZ22I      PIC X(6).                             00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PRESULTAT2L  COMP PIC S9(4).                            00001270
      *--                                                                       
           02 M2PRESULTAT2L COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 M2PRESULTAT2F  PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 M2PRESULTAT2I  PIC X(11).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M2PTOTALL      COMP PIC S9(4).                            00001310
      *--                                                                       
           02 M2PTOTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 M2PTOTALF      PIC X.                                     00001320
           02 FILLER    PIC X(4).                                       00001330
           02 M2PTOTALI      PIC X(11).                                 00001340
           02 M3LIGNEI OCCURS   2 TIMES .                               00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3NRUBL      COMP PIC S9(4).                            00001360
      *--                                                                       
             03 M3NRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M3NRUBF      PIC X.                                     00001370
             03 FILLER  PIC X(4).                                       00001380
             03 M3NRUBI      PIC X(2).                                  00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3LRUBL      COMP PIC S9(4).                            00001400
      *--                                                                       
             03 M3LRUBL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 M3LRUBF      PIC X.                                     00001410
             03 FILLER  PIC X(4).                                       00001420
             03 M3LRUBI      PIC X(19).                                 00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTANTZINL    COMP PIC S9(4).                       00001440
      *--                                                                       
             03 M3PMONTANTZINL COMP-5 PIC S9(4).                                
      *}                                                                        
             03 M3PMONTANTZINF    PIC X.                                00001450
             03 FILLER  PIC X(4).                                       00001460
             03 M3PMONTANTZINI    PIC X(9).                             00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT1L  COMP PIC S9(4).                            00001480
      *--                                                                       
             03 M3CPARENT1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT1F  PIC X.                                     00001490
             03 FILLER  PIC X(4).                                       00001500
             03 M3CPARENT1I  PIC X.                                     00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTANTZ1L     COMP PIC S9(4).                       00001520
      *--                                                                       
             03 M3PMONTANTZ1L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M3PMONTANTZ1F     PIC X.                                00001530
             03 FILLER  PIC X(4).                                       00001540
             03 M3PMONTANTZ1I     PIC X(9).                             00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3COPER1L    COMP PIC S9(4).                            00001560
      *--                                                                       
             03 M3COPER1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M3COPER1F    PIC X.                                     00001570
             03 FILLER  PIC X(4).                                       00001580
             03 M3COPER1I    PIC X(2).                                  00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTANTZ2L     COMP PIC S9(4).                       00001600
      *--                                                                       
             03 M3PMONTANTZ2L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M3PMONTANTZ2F     PIC X.                                00001610
             03 FILLER  PIC X(4).                                       00001620
             03 M3PMONTANTZ2I     PIC X(6).                             00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3COPER2L    COMP PIC S9(4).                            00001640
      *--                                                                       
             03 M3COPER2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 M3COPER2F    PIC X.                                     00001650
             03 FILLER  PIC X(4).                                       00001660
             03 M3COPER2I    PIC X(2).                                  00001670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PMONTANTZ3L     COMP PIC S9(4).                       00001680
      *--                                                                       
             03 M3PMONTANTZ3L COMP-5 PIC S9(4).                                 
      *}                                                                        
             03 M3PMONTANTZ3F     PIC X.                                00001690
             03 FILLER  PIC X(4).                                       00001700
             03 M3PMONTANTZ3I     PIC X(6).                             00001710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3CPARENT2L  COMP PIC S9(4).                            00001720
      *--                                                                       
             03 M3CPARENT2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 M3CPARENT2F  PIC X.                                     00001730
             03 FILLER  PIC X(4).                                       00001740
             03 M3CPARENT2I  PIC X(2).                                  00001750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 M3PRESULTATL      COMP PIC S9(4).                       00001760
      *--                                                                       
             03 M3PRESULTATL COMP-5 PIC S9(4).                                  
      *}                                                                        
             03 M3PRESULTATF      PIC X.                                00001770
             03 FILLER  PIC X(4).                                       00001780
             03 M3PRESULTATI      PIC X(11).                            00001790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M3LTYPRUBL     COMP PIC S9(4).                            00001800
      *--                                                                       
           02 M3LTYPRUBL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 M3LTYPRUBF     PIC X.                                     00001810
           02 FILLER    PIC X(4).                                       00001820
           02 M3LTYPRUBI     PIC X(30).                                 00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 M3PENVELOPPEL  COMP PIC S9(4).                            00001840
      *--                                                                       
           02 M3PENVELOPPEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 M3PENVELOPPEF  PIC X.                                     00001850
           02 FILLER    PIC X(4).                                       00001860
           02 M3PENVELOPPEI  PIC X(11).                                 00001870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001880
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001890
           02 FILLER    PIC X(4).                                       00001900
           02 MZONCMDI  PIC X(15).                                      00001910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001920
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001930
           02 FILLER    PIC X(4).                                       00001940
           02 MLIBERRI  PIC X(58).                                      00001950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001960
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001970
           02 FILLER    PIC X(4).                                       00001980
           02 MCODTRAI  PIC X(4).                                       00001990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002010
           02 FILLER    PIC X(4).                                       00002020
           02 MCICSI    PIC X(5).                                       00002030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002040
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002050
           02 FILLER    PIC X(4).                                       00002060
           02 MNETNAMI  PIC X(8).                                       00002070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002080
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002090
           02 FILLER    PIC X(4).                                       00002100
           02 MSCREENI  PIC X(4).                                       00002110
      ***************************************************************** 00002120
      * SDF: EPP20   EPP20                                              00002130
      ***************************************************************** 00002140
       01   EPP20O REDEFINES EPP20I.                                    00002150
           02 FILLER    PIC X(12).                                      00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MDATJOUA  PIC X.                                          00002180
           02 MDATJOUC  PIC X.                                          00002190
           02 MDATJOUP  PIC X.                                          00002200
           02 MDATJOUH  PIC X.                                          00002210
           02 MDATJOUV  PIC X.                                          00002220
           02 MDATJOUO  PIC X(10).                                      00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MTIMJOUA  PIC X.                                          00002250
           02 MTIMJOUC  PIC X.                                          00002260
           02 MTIMJOUP  PIC X.                                          00002270
           02 MTIMJOUH  PIC X.                                          00002280
           02 MTIMJOUV  PIC X.                                          00002290
           02 MTIMJOUO  PIC X(5).                                       00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLOPTIONA      PIC X.                                     00002320
           02 MLOPTIONC PIC X.                                          00002330
           02 MLOPTIONP PIC X.                                          00002340
           02 MLOPTIONH PIC X.                                          00002350
           02 MLOPTIONV PIC X.                                          00002360
           02 MLOPTIONO      PIC X(20).                                 00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MNSOCIETEA     PIC X.                                     00002390
           02 MNSOCIETEC     PIC X.                                     00002400
           02 MNSOCIETEP     PIC X.                                     00002410
           02 MNSOCIETEH     PIC X.                                     00002420
           02 MNSOCIETEV     PIC X.                                     00002430
           02 MNSOCIETEO     PIC X(3).                                  00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MNLIEUORIGA    PIC X.                                     00002460
           02 MNLIEUORIGC    PIC X.                                     00002470
           02 MNLIEUORIGP    PIC X.                                     00002480
           02 MNLIEUORIGH    PIC X.                                     00002490
           02 MNLIEUORIGV    PIC X.                                     00002500
           02 MNLIEUORIGO    PIC X(3).                                  00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNLIEUA   PIC X.                                          00002530
           02 MNLIEUC   PIC X.                                          00002540
           02 MNLIEUP   PIC X.                                          00002550
           02 MNLIEUH   PIC X.                                          00002560
           02 MNLIEUV   PIC X.                                          00002570
           02 MNLIEUO   PIC X(3).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MLLIEUA   PIC X.                                          00002600
           02 MLLIEUC   PIC X.                                          00002610
           02 MLLIEUP   PIC X.                                          00002620
           02 MLLIEUH   PIC X.                                          00002630
           02 MLLIEUV   PIC X.                                          00002640
           02 MLLIEUO   PIC X(20).                                      00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MDMOISPAYEA    PIC X.                                     00002670
           02 MDMOISPAYEC    PIC X.                                     00002680
           02 MDMOISPAYEP    PIC X.                                     00002690
           02 MDMOISPAYEH    PIC X.                                     00002700
           02 MDMOISPAYEV    PIC X.                                     00002710
           02 MDMOISPAYEO    PIC X(6).                                  00002720
           02 M1LIGNEO OCCURS   10 TIMES .                              00002730
             03 FILLER       PIC X(2).                                  00002740
             03 M1NRUBA      PIC X.                                     00002750
             03 M1NRUBC PIC X.                                          00002760
             03 M1NRUBP PIC X.                                          00002770
             03 M1NRUBH PIC X.                                          00002780
             03 M1NRUBV PIC X.                                          00002790
             03 M1NRUBO      PIC X(2).                                  00002800
             03 FILLER       PIC X(2).                                  00002810
             03 M1LRUBA      PIC X.                                     00002820
             03 M1LRUBC PIC X.                                          00002830
             03 M1LRUBP PIC X.                                          00002840
             03 M1LRUBH PIC X.                                          00002850
             03 M1LRUBV PIC X.                                          00002860
             03 M1LRUBO      PIC X(19).                                 00002870
             03 FILLER       PIC X(2).                                  00002880
             03 M1PMONTANTZINA    PIC X.                                00002890
             03 M1PMONTANTZINC    PIC X.                                00002900
             03 M1PMONTANTZINP    PIC X.                                00002910
             03 M1PMONTANTZINH    PIC X.                                00002920
             03 M1PMONTANTZINV    PIC X.                                00002930
             03 M1PMONTANTZINO    PIC X(9).                             00002940
             03 FILLER       PIC X(2).                                  00002950
             03 M1CPARENT1A  PIC X.                                     00002960
             03 M1CPARENT1C  PIC X.                                     00002970
             03 M1CPARENT1P  PIC X.                                     00002980
             03 M1CPARENT1H  PIC X.                                     00002990
             03 M1CPARENT1V  PIC X.                                     00003000
             03 M1CPARENT1O  PIC X.                                     00003010
             03 FILLER       PIC X(2).                                  00003020
             03 M1PMONTANTZ1A     PIC X.                                00003030
             03 M1PMONTANTZ1C     PIC X.                                00003040
             03 M1PMONTANTZ1P     PIC X.                                00003050
             03 M1PMONTANTZ1H     PIC X.                                00003060
             03 M1PMONTANTZ1V     PIC X.                                00003070
             03 M1PMONTANTZ1O     PIC X(9).                             00003080
             03 FILLER       PIC X(2).                                  00003090
             03 M1COPER1A    PIC X.                                     00003100
             03 M1COPER1C    PIC X.                                     00003110
             03 M1COPER1P    PIC X.                                     00003120
             03 M1COPER1H    PIC X.                                     00003130
             03 M1COPER1V    PIC X.                                     00003140
             03 M1COPER1O    PIC X(2).                                  00003150
             03 FILLER       PIC X(2).                                  00003160
             03 M1PMONTANTZ2A     PIC X.                                00003170
             03 M1PMONTANTZ2C     PIC X.                                00003180
             03 M1PMONTANTZ2P     PIC X.                                00003190
             03 M1PMONTANTZ2H     PIC X.                                00003200
             03 M1PMONTANTZ2V     PIC X.                                00003210
             03 M1PMONTANTZ2O     PIC X(6).                             00003220
             03 FILLER       PIC X(2).                                  00003230
             03 M1COPER2A    PIC X.                                     00003240
             03 M1COPER2C    PIC X.                                     00003250
             03 M1COPER2P    PIC X.                                     00003260
             03 M1COPER2H    PIC X.                                     00003270
             03 M1COPER2V    PIC X.                                     00003280
             03 M1COPER2O    PIC X(2).                                  00003290
             03 FILLER       PIC X(2).                                  00003300
             03 M1PMONTANTZ3A     PIC X.                                00003310
             03 M1PMONTANTZ3C     PIC X.                                00003320
             03 M1PMONTANTZ3P     PIC X.                                00003330
             03 M1PMONTANTZ3H     PIC X.                                00003340
             03 M1PMONTANTZ3V     PIC X.                                00003350
             03 M1PMONTANTZ3O     PIC X(6).                             00003360
             03 FILLER       PIC X(2).                                  00003370
             03 M1CPARENT2A  PIC X.                                     00003380
             03 M1CPARENT2C  PIC X.                                     00003390
             03 M1CPARENT2P  PIC X.                                     00003400
             03 M1CPARENT2H  PIC X.                                     00003410
             03 M1CPARENT2V  PIC X.                                     00003420
             03 M1CPARENT2O  PIC X(2).                                  00003430
             03 FILLER       PIC X(2).                                  00003440
             03 M1PRESULTATA      PIC X.                                00003450
             03 M1PRESULTATC PIC X.                                     00003460
             03 M1PRESULTATP PIC X.                                     00003470
             03 M1PRESULTATH PIC X.                                     00003480
             03 M1PRESULTATV PIC X.                                     00003490
             03 M1PRESULTATO      PIC X(11).                            00003500
           02 FILLER    PIC X(2).                                       00003510
           02 M1LTYPRUBA     PIC X.                                     00003520
           02 M1LTYPRUBC     PIC X.                                     00003530
           02 M1LTYPRUBP     PIC X.                                     00003540
           02 M1LTYPRUBH     PIC X.                                     00003550
           02 M1LTYPRUBV     PIC X.                                     00003560
           02 M1LTYPRUBO     PIC X(30).                                 00003570
           02 FILLER    PIC X(2).                                       00003580
           02 M1PTOTALA      PIC X.                                     00003590
           02 M1PTOTALC PIC X.                                          00003600
           02 M1PTOTALP PIC X.                                          00003610
           02 M1PTOTALH PIC X.                                          00003620
           02 M1PTOTALV PIC X.                                          00003630
           02 M1PTOTALO      PIC X(11).                                 00003640
           02 FILLER    PIC X(2).                                       00003650
           02 M2LRUB1A  PIC X.                                          00003660
           02 M2LRUB1C  PIC X.                                          00003670
           02 M2LRUB1P  PIC X.                                          00003680
           02 M2LRUB1H  PIC X.                                          00003690
           02 M2LRUB1V  PIC X.                                          00003700
           02 M2LRUB1O  PIC X(30).                                      00003710
           02 FILLER    PIC X(2).                                       00003720
           02 M2LRUB2A  PIC X.                                          00003730
           02 M2LRUB2C  PIC X.                                          00003740
           02 M2LRUB2P  PIC X.                                          00003750
           02 M2LRUB2H  PIC X.                                          00003760
           02 M2LRUB2V  PIC X.                                          00003770
           02 M2LRUB2O  PIC X(30).                                      00003780
           02 FILLER    PIC X(2).                                       00003790
           02 M2PMONTANTZ11A      PIC X.                                00003800
           02 M2PMONTANTZ11C PIC X.                                     00003810
           02 M2PMONTANTZ11P PIC X.                                     00003820
           02 M2PMONTANTZ11H PIC X.                                     00003830
           02 M2PMONTANTZ11V PIC X.                                     00003840
           02 M2PMONTANTZ11O      PIC X(9).                             00003850
           02 FILLER    PIC X(2).                                       00003860
           02 M2COPERATEUR1A      PIC X.                                00003870
           02 M2COPERATEUR1C PIC X.                                     00003880
           02 M2COPERATEUR1P PIC X.                                     00003890
           02 M2COPERATEUR1H PIC X.                                     00003900
           02 M2COPERATEUR1V PIC X.                                     00003910
           02 M2COPERATEUR1O      PIC X.                                00003920
           02 FILLER    PIC X(2).                                       00003930
           02 M2PMONTANTZ21A      PIC X.                                00003940
           02 M2PMONTANTZ21C PIC X.                                     00003950
           02 M2PMONTANTZ21P PIC X.                                     00003960
           02 M2PMONTANTZ21H PIC X.                                     00003970
           02 M2PMONTANTZ21V PIC X.                                     00003980
           02 M2PMONTANTZ21O      PIC X(6).                             00003990
           02 FILLER    PIC X(2).                                       00004000
           02 M2PRESULTAT1A  PIC X.                                     00004010
           02 M2PRESULTAT1C  PIC X.                                     00004020
           02 M2PRESULTAT1P  PIC X.                                     00004030
           02 M2PRESULTAT1H  PIC X.                                     00004040
           02 M2PRESULTAT1V  PIC X.                                     00004050
           02 M2PRESULTAT1O  PIC X(11).                                 00004060
           02 FILLER    PIC X(2).                                       00004070
           02 M2PMONTANTZ12A      PIC X.                                00004080
           02 M2PMONTANTZ12C PIC X.                                     00004090
           02 M2PMONTANTZ12P PIC X.                                     00004100
           02 M2PMONTANTZ12H PIC X.                                     00004110
           02 M2PMONTANTZ12V PIC X.                                     00004120
           02 M2PMONTANTZ12O      PIC X(11).                            00004130
           02 FILLER    PIC X(2).                                       00004140
           02 M2COPERATEUR2A      PIC X.                                00004150
           02 M2COPERATEUR2C PIC X.                                     00004160
           02 M2COPERATEUR2P PIC X.                                     00004170
           02 M2COPERATEUR2H PIC X.                                     00004180
           02 M2COPERATEUR2V PIC X.                                     00004190
           02 M2COPERATEUR2O      PIC X.                                00004200
           02 FILLER    PIC X(2).                                       00004210
           02 M2PMONTANTZ22A      PIC X.                                00004220
           02 M2PMONTANTZ22C PIC X.                                     00004230
           02 M2PMONTANTZ22P PIC X.                                     00004240
           02 M2PMONTANTZ22H PIC X.                                     00004250
           02 M2PMONTANTZ22V PIC X.                                     00004260
           02 M2PMONTANTZ22O      PIC X(6).                             00004270
           02 FILLER    PIC X(2).                                       00004280
           02 M2PRESULTAT2A  PIC X.                                     00004290
           02 M2PRESULTAT2C  PIC X.                                     00004300
           02 M2PRESULTAT2P  PIC X.                                     00004310
           02 M2PRESULTAT2H  PIC X.                                     00004320
           02 M2PRESULTAT2V  PIC X.                                     00004330
           02 M2PRESULTAT2O  PIC X(11).                                 00004340
           02 FILLER    PIC X(2).                                       00004350
           02 M2PTOTALA      PIC X.                                     00004360
           02 M2PTOTALC PIC X.                                          00004370
           02 M2PTOTALP PIC X.                                          00004380
           02 M2PTOTALH PIC X.                                          00004390
           02 M2PTOTALV PIC X.                                          00004400
           02 M2PTOTALO      PIC X(11).                                 00004410
           02 M3LIGNEO OCCURS   2 TIMES .                               00004420
             03 FILLER       PIC X(2).                                  00004430
             03 M3NRUBA      PIC X.                                     00004440
             03 M3NRUBC PIC X.                                          00004450
             03 M3NRUBP PIC X.                                          00004460
             03 M3NRUBH PIC X.                                          00004470
             03 M3NRUBV PIC X.                                          00004480
             03 M3NRUBO      PIC X(2).                                  00004490
             03 FILLER       PIC X(2).                                  00004500
             03 M3LRUBA      PIC X.                                     00004510
             03 M3LRUBC PIC X.                                          00004520
             03 M3LRUBP PIC X.                                          00004530
             03 M3LRUBH PIC X.                                          00004540
             03 M3LRUBV PIC X.                                          00004550
             03 M3LRUBO      PIC X(19).                                 00004560
             03 FILLER       PIC X(2).                                  00004570
             03 M3PMONTANTZINA    PIC X.                                00004580
             03 M3PMONTANTZINC    PIC X.                                00004590
             03 M3PMONTANTZINP    PIC X.                                00004600
             03 M3PMONTANTZINH    PIC X.                                00004610
             03 M3PMONTANTZINV    PIC X.                                00004620
             03 M3PMONTANTZINO    PIC X(9).                             00004630
             03 FILLER       PIC X(2).                                  00004640
             03 M3CPARENT1A  PIC X.                                     00004650
             03 M3CPARENT1C  PIC X.                                     00004660
             03 M3CPARENT1P  PIC X.                                     00004670
             03 M3CPARENT1H  PIC X.                                     00004680
             03 M3CPARENT1V  PIC X.                                     00004690
             03 M3CPARENT1O  PIC X.                                     00004700
             03 FILLER       PIC X(2).                                  00004710
             03 M3PMONTANTZ1A     PIC X.                                00004720
             03 M3PMONTANTZ1C     PIC X.                                00004730
             03 M3PMONTANTZ1P     PIC X.                                00004740
             03 M3PMONTANTZ1H     PIC X.                                00004750
             03 M3PMONTANTZ1V     PIC X.                                00004760
             03 M3PMONTANTZ1O     PIC X(9).                             00004770
             03 FILLER       PIC X(2).                                  00004780
             03 M3COPER1A    PIC X.                                     00004790
             03 M3COPER1C    PIC X.                                     00004800
             03 M3COPER1P    PIC X.                                     00004810
             03 M3COPER1H    PIC X.                                     00004820
             03 M3COPER1V    PIC X.                                     00004830
             03 M3COPER1O    PIC X(2).                                  00004840
             03 FILLER       PIC X(2).                                  00004850
             03 M3PMONTANTZ2A     PIC X.                                00004860
             03 M3PMONTANTZ2C     PIC X.                                00004870
             03 M3PMONTANTZ2P     PIC X.                                00004880
             03 M3PMONTANTZ2H     PIC X.                                00004890
             03 M3PMONTANTZ2V     PIC X.                                00004900
             03 M3PMONTANTZ2O     PIC X(6).                             00004910
             03 FILLER       PIC X(2).                                  00004920
             03 M3COPER2A    PIC X.                                     00004930
             03 M3COPER2C    PIC X.                                     00004940
             03 M3COPER2P    PIC X.                                     00004950
             03 M3COPER2H    PIC X.                                     00004960
             03 M3COPER2V    PIC X.                                     00004970
             03 M3COPER2O    PIC X(2).                                  00004980
             03 FILLER       PIC X(2).                                  00004990
             03 M3PMONTANTZ3A     PIC X.                                00005000
             03 M3PMONTANTZ3C     PIC X.                                00005010
             03 M3PMONTANTZ3P     PIC X.                                00005020
             03 M3PMONTANTZ3H     PIC X.                                00005030
             03 M3PMONTANTZ3V     PIC X.                                00005040
             03 M3PMONTANTZ3O     PIC X(6).                             00005050
             03 FILLER       PIC X(2).                                  00005060
             03 M3CPARENT2A  PIC X.                                     00005070
             03 M3CPARENT2C  PIC X.                                     00005080
             03 M3CPARENT2P  PIC X.                                     00005090
             03 M3CPARENT2H  PIC X.                                     00005100
             03 M3CPARENT2V  PIC X.                                     00005110
             03 M3CPARENT2O  PIC X(2).                                  00005120
             03 FILLER       PIC X(2).                                  00005130
             03 M3PRESULTATA      PIC X.                                00005140
             03 M3PRESULTATC PIC X.                                     00005150
             03 M3PRESULTATP PIC X.                                     00005160
             03 M3PRESULTATH PIC X.                                     00005170
             03 M3PRESULTATV PIC X.                                     00005180
             03 M3PRESULTATO      PIC X(11).                            00005190
           02 FILLER    PIC X(2).                                       00005200
           02 M3LTYPRUBA     PIC X.                                     00005210
           02 M3LTYPRUBC     PIC X.                                     00005220
           02 M3LTYPRUBP     PIC X.                                     00005230
           02 M3LTYPRUBH     PIC X.                                     00005240
           02 M3LTYPRUBV     PIC X.                                     00005250
           02 M3LTYPRUBO     PIC X(30).                                 00005260
           02 FILLER    PIC X(2).                                       00005270
           02 M3PENVELOPPEA  PIC X.                                     00005280
           02 M3PENVELOPPEC  PIC X.                                     00005290
           02 M3PENVELOPPEP  PIC X.                                     00005300
           02 M3PENVELOPPEH  PIC X.                                     00005310
           02 M3PENVELOPPEV  PIC X.                                     00005320
           02 M3PENVELOPPEO  PIC X(11).                                 00005330
           02 FILLER    PIC X(2).                                       00005340
           02 MZONCMDA  PIC X.                                          00005350
           02 MZONCMDC  PIC X.                                          00005360
           02 MZONCMDP  PIC X.                                          00005370
           02 MZONCMDH  PIC X.                                          00005380
           02 MZONCMDV  PIC X.                                          00005390
           02 MZONCMDO  PIC X(15).                                      00005400
           02 FILLER    PIC X(2).                                       00005410
           02 MLIBERRA  PIC X.                                          00005420
           02 MLIBERRC  PIC X.                                          00005430
           02 MLIBERRP  PIC X.                                          00005440
           02 MLIBERRH  PIC X.                                          00005450
           02 MLIBERRV  PIC X.                                          00005460
           02 MLIBERRO  PIC X(58).                                      00005470
           02 FILLER    PIC X(2).                                       00005480
           02 MCODTRAA  PIC X.                                          00005490
           02 MCODTRAC  PIC X.                                          00005500
           02 MCODTRAP  PIC X.                                          00005510
           02 MCODTRAH  PIC X.                                          00005520
           02 MCODTRAV  PIC X.                                          00005530
           02 MCODTRAO  PIC X(4).                                       00005540
           02 FILLER    PIC X(2).                                       00005550
           02 MCICSA    PIC X.                                          00005560
           02 MCICSC    PIC X.                                          00005570
           02 MCICSP    PIC X.                                          00005580
           02 MCICSH    PIC X.                                          00005590
           02 MCICSV    PIC X.                                          00005600
           02 MCICSO    PIC X(5).                                       00005610
           02 FILLER    PIC X(2).                                       00005620
           02 MNETNAMA  PIC X.                                          00005630
           02 MNETNAMC  PIC X.                                          00005640
           02 MNETNAMP  PIC X.                                          00005650
           02 MNETNAMH  PIC X.                                          00005660
           02 MNETNAMV  PIC X.                                          00005670
           02 MNETNAMO  PIC X(8).                                       00005680
           02 FILLER    PIC X(2).                                       00005690
           02 MSCREENA  PIC X.                                          00005700
           02 MSCREENC  PIC X.                                          00005710
           02 MSCREENP  PIC X.                                          00005720
           02 MSCREENH  PIC X.                                          00005730
           02 MSCREENV  PIC X.                                          00005740
           02 MSCREENO  PIC X(4).                                       00005750
                                                                                
