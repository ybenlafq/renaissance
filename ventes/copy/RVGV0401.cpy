      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RRTGV04                                              
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVGV0401                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0401.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0401.                                                            
      *}                                                                        
           02  GV04-NSOCIETE  PIC X(03).                                        
           02  GV04-NLIEU     PIC X(03).                                        
           02  GV04-NVENTE    PIC X(07).                                        
           02  GV04-DCREATION PIC X(08).                                        
           02  GV04-DMAJ      PIC X(08).                                        
           02  GV04-WACCES    PIC X(01).                                        
           02  GV04-WREPRISE  PIC X(01).                                        
           02  GV04-NBPRDREP  PIC S9(02) COMP-3.                                
           02  GV04-DATENAISS PIC X(08).                                        
           02  GV04-CPNAISS   PIC X(05).                                        
           02  GV04-LIEUNAISS PIC X(32).                                        
           02  GV04-WBTOB     PIC X(01).                                        
           02  GV04-WARF      PIC X(01).                                        
           02  GV04-DSYST     PIC S9(13) COMP-3.                                
           02  GV04-WASCUTIL  PIC X(01).                                        
           02  GV04-WMARCHES  PIC X(01).                                        
           02  GV04-WCOL      PIC X(01).                                        
           02  GV04-WSDEP     PIC X(01).                                        
           02  GV04-WFLAG     PIC X(10).                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVGV0401                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-NSOCIETE-F  PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-NSOCIETE-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-NLIEU-F     PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-NLIEU-F     PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-NVENTE-F    PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-NVENTE-F    PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-DCREATION-F PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-DCREATION-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-DMAJ-F      PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-DMAJ-F      PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WACCES-F    PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WACCES-F    PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WREPRISE-F  PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WREPRISE-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-NBPRDREP-F  PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-NBPRDREP-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-DATENAISS-F PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-DATENAISS-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-CPNAISS-F   PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-CPNAISS-F   PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-LIEUNAISS-F PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-LIEUNAISS-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WBTOB-F     PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WBTOB-F     PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WARF-F      PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WARF-F      PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-DSYST-F     PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-DSYST-F     PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WASCUTIL-F  PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WASCUTIL-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WMARCHES-F  PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WMARCHES-F  PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WCOL-F      PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WCOL-F      PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WSDEP-F     PIC S9(4) COMP.                                 
      *--                                                                       
           02  GV04-WSDEP-F     PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV04-WFLAG-F     PIC S9(4) COMP.                                 
      *                                                                         
      *--                                                                       
           02  GV04-WFLAG-F     PIC S9(4) COMP-5.                               
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
