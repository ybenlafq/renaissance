      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV411 AU 30/01/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV411.                                                        
            05 NOMETAT-IPV411           PIC X(6) VALUE 'IPV411'.                
            05 RUPTURES-IPV411.                                                 
           10 IPV411-NSOCIETE           PIC X(03).                      007  003
           10 IPV411-NLIEU              PIC X(03).                      010  003
           10 IPV411-WSEQED             PIC S9(05)      COMP-3.         013  003
           10 IPV411-NAGREGATED         PIC X(02).                      016  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV411-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IPV411-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV411.                                                   
           10 IPV411-RUBRIQUE           PIC X(20).                      020  020
           10 IPV411-PCA                PIC S9(08)      COMP-3.         040  005
           10 IPV411-PMTADD             PIC S9(07)V9(2) COMP-3.         045  005
           10 IPV411-PMTEP              PIC S9(07)V9(2) COMP-3.         050  005
           10 IPV411-PMTJAUNE           PIC S9(07)V9(2) COMP-3.         055  005
           10 IPV411-PMTPSE             PIC S9(07)V9(2) COMP-3.         060  005
           10 IPV411-PMTTOT             PIC S9(07)V9(2) COMP-3.         065  005
           10 IPV411-PMTVOL             PIC S9(07)V9(2) COMP-3.         070  005
           10 IPV411-QVENDUE            PIC S9(08)      COMP-3.         075  005
           10 IPV411-DEBPERIODE         PIC X(08).                      080  008
           10 IPV411-FINPERIODE         PIC X(08).                      088  008
            05 FILLER                      PIC X(417).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV411-LONG           PIC S9(4)   COMP  VALUE +095.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV411-LONG           PIC S9(4) COMP-5  VALUE +095.           
                                                                                
      *}                                                                        
