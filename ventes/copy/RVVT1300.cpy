      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT1300                                     00020002
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1300                 00060002
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT1300.                                                    00090002
           02  VT13-NFLAG                                               00100002
               PIC X(0001).                                             00110002
           02  VT13-NCLUSTER                                            00120002
               PIC X(0013).                                             00130002
           02  VT13-NSOCIETE                                            00140002
               PIC X(0003).                                             00150002
           02  VT13-NLIGNE                                              00160002
               PIC X(0003).                                             00170001
           02  VT13-NLCODIC                                             00180002
               PIC X(0003).                                             00190001
           02  VT13-CENREG                                              00200002
               PIC X(0005).                                             00210001
           02  VT13-DDELIV                                              00220002
               PIC X(0008).                                             00230001
           02  VT13-QVENDUE                                             00240002
               PIC S9(5) COMP-3.                                        00250001
           02  VT13-PVUNIT                                              00260002
               PIC S9(7)V9(0002) COMP-3.                                00270001
           02  VT13-PVTOTAL                                             00280002
               PIC S9(7)V9(0002) COMP-3.                                00290001
           02  VT13-TAUXTVA                                             00300002
               PIC S9(3)V9(0002) COMP-3.                                00310001
           02  VT13-NCONTRAT                                            00320002
               PIC X(0010).                                             00330001
           02  VT13-CANNULREP                                           00340002
               PIC X(0001).                                             00350001
           02  VT13-CVENDEUR                                            00360002
               PIC X(0007).                                             00370001
           02  VT13-NAUTORM                                             00371003
               PIC X(0005).                                             00372003
           02  VT13-NSEQNQ                                              00373003
               PIC S9(005)  COMP-3.                                     00374003
           02  VT13-NSEQREF                                             00375003
               PIC S9(005)  COMP-3.                                     00376003
           02  VT13-CTYPENT                                             00377004
               PIC X(002).                                              00378005
           02  VT13-NLIEN                                               00379003
               PIC S9(005)  COMP-3.                                     00379103
           02  VT13-NACTVTE                                             00379203
               PIC S9(005)  COMP-3.                                     00379303
           02  VT13-NSEQENS                                             00379403
               PIC S9(005)  COMP-3.                                     00379503
           02  VT13-MPRIMECLI                                           00379603
               PIC S9(7)V9(0002) COMP-3.                                00379703
      *                                                                 00380001
      *---------------------------------------------------------        00390001
      *   LISTE DES FLAGS DE LA TABLE RVVT1300                          00400002
      *---------------------------------------------------------        00410001
      *                                                                 00420001
       01  RVVT1300-FLAGS.                                              00430002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NFLAG-F                                             00440002
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT13-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NCLUSTER-F                                          00460002
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT13-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NSOCIETE-F                                          00480002
      *        PIC S9(4) COMP.                                          00490002
      *--                                                                       
           02  VT13-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NLIGNE-F                                            00500002
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT13-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NLCODIC-F                                           00520002
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT13-NLCODIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-CENREG-F                                            00540002
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT13-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-DDELIV-F                                            00560002
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT13-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-QVENDUE-F                                           00580002
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT13-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-PVUNIT-F                                            00600002
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT13-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-PVTOTAL-F                                           00620002
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT13-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-TAUXTVA-F                                           00640002
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  VT13-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NCONTRAT-F                                          00660002
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  VT13-NCONTRAT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-CANNULREP-F                                         00680002
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  VT13-CANNULREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-CVENDEUR-F                                          00700002
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  VT13-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NAUTORM-F                                           00711003
      *        PIC S9(4) COMP.                                          00712003
      *--                                                                       
           02  VT13-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NSEQNQ-F                                            00713003
      *        PIC S9(4) COMP.                                          00714003
      *--                                                                       
           02  VT13-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NSEQREF-F                                           00715003
      *        PIC S9(4) COMP.                                          00716003
      *--                                                                       
           02  VT13-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-CTYPENT-F                                           00717004
      *        PIC S9(4) COMP.                                          00718003
      *--                                                                       
           02  VT13-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NLIEN-F                                             00719003
      *        PIC S9(4) COMP.                                          00719103
      *--                                                                       
           02  VT13-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NACTVTE-F                                           00719203
      *        PIC S9(4) COMP.                                          00719303
      *--                                                                       
           02  VT13-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-NSEQENS-F                                           00719403
      *        PIC S9(4) COMP.                                          00719503
      *--                                                                       
           02  VT13-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT13-MPRIMECLI-F                                         00719603
      *        PIC S9(4) COMP.                                          00719703
      *--                                                                       
           02  VT13-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00720001
                                                                                
