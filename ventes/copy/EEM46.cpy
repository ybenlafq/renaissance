      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EEM46   EEM46                                              00000020
      ***************************************************************** 00000030
       01   EEM46I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      * NUMERO SOCIETE                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000270
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNSOCI    PIC X(3).                                       00000300
      * NUMERO MAGASIN                                                  00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNMAGI    PIC X(3).                                       00000350
      * DATE DEBUT PERIODE(JJ/MM/SSAA)                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBL    COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MDDEBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDDEBF    PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MDDEBI    PIC X(10).                                      00000400
      * DATE FIN PERIODE(JJ/MM/SSAA)                                    00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDFINI    PIC X(10).                                      00000450
      * NUMERO CAISSE                                                   00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNCAISI   PIC X(3).                                       00000500
      * NUMERO TRANSACTION                                              00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNTRANSI  PIC X(8).                                       00000550
      * TYPE TRANSACTION                                                00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRANSL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MTTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTRANSF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MTTRANSI  PIC X(3).                                       00000600
      * MONTANT TRANS.                                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MPMONTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPMONTF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MPMONTI   PIC X(10).                                      00000650
      * LIBELLE 'MODE PAIEMENT'                                         00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPAIL    COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MLPAIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLPAIF    PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MLPAII    PIC X(16).                                      00000700
      * MODE PAIEMENT                                                   00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMPAIL    COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MMPAIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMPAIF    PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MMPAII    PIC X.                                          00000750
      * LIBELLE 'DEVISE PAIEMENT'                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000770
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MLDEVISEI      PIC X(17).                                 00000800
      * DEVISE PAIEMENT                                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCDEVISEI      PIC X.                                     00000850
      * LIBELLE 'N� DE LA VENTE'                                        00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENTEL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVENTEF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLVENTEI  PIC X(19).                                      00000900
      * N� DE LA VENTE                                                  00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNVENTEI  PIC X(7).                                       00000950
      * LIBELLE 'N� CODIC'                                              00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCODICL  COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MLCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCODICF  PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MLCODICI  PIC X(12).                                      00001000
      * N� CODIC                                                        00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNCODICI  PIC X(7).                                       00001050
      * LIBELLE 'MODE DELIVRANCE'                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDELL    COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MLDELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLDELF    PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLDELI    PIC X(17).                                      00001100
      * MODE DELIVRANCE                                                 00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDELL     COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MDELL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MDELF     PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MDELI     PIC X(3).                                       00001150
      * libelle 'n� trans'                                              00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNTRANS1L     COMP PIC S9(4).                            00001170
      *--                                                                       
           02 MLNTRANS1L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNTRANS1F     PIC X.                                     00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MLNTRANS1I     PIC X(2).                                  00001200
      * libelle du type                                                 00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPE1L  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MLTYPE1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTYPE1F  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLTYPE1I  PIC X(4).                                       00001250
      * libelle                                                         00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPER1L  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MLOPER1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLOPER1F  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MLOPER1I  PIC X(8).                                       00001300
      * libelle 'n� trans'                                              00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNTRANS2L     COMP PIC S9(4).                            00001320
      *--                                                                       
           02 MLNTRANS2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNTRANS2F     PIC X.                                     00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MLNTRANS2I     PIC X(8).                                  00001350
      * libelle du type                                                 00001360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPE2L  COMP PIC S9(4).                                 00001370
      *--                                                                       
           02 MLTYPE2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTYPE2F  PIC X.                                          00001380
           02 FILLER    PIC X(4).                                       00001390
           02 MLTYPE2I  PIC X(5).                                       00001400
      * libelle                                                         00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPER2L  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MLOPER2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLOPER2F  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MLOPER2I  PIC X(8).                                       00001450
      * ENTETE REGLEMENT                                                00001460
           02 MLIGNEI OCCURS   12 TIMES .                               00001470
      * ZONE DE SELECTION                                               00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTSELECTL  COMP PIC S9(4).                            00001490
      *--                                                                       
               04 MTSELECTL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MTSELECTF  PIC X.                                     00001500
               04 FILLER     PIC X(4).                                  00001510
               04 MTSELECTI  PIC X.                                     00001520
      * DATE VENTE (JJ/MM/SSAA)                                         00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDVENTEL   COMP PIC S9(4).                            00001540
      *--                                                                       
               04 MDVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDVENTEF   PIC X.                                     00001550
               04 FILLER     PIC X(4).                                  00001560
               04 MDVENTEI   PIC X(10).                                 00001570
      * HEURE DE VENTE                                                  00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTIMVTEL   COMP PIC S9(4).                            00001590
      *--                                                                       
               04 MTIMVTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MTIMVTEF   PIC X.                                     00001600
               04 FILLER     PIC X(4).                                  00001610
               04 MTIMVTEI   PIC X(5).                                  00001620
      * N� CAISSE LIGNE                                                 00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNCAISSEL  COMP PIC S9(4).                            00001640
      *--                                                                       
               04 MNCAISSEL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MNCAISSEF  PIC X.                                     00001650
               04 FILLER     PIC X(4).                                  00001660
               04 MNCAISSEI  PIC X(3).                                  00001670
      * N� TRANSACTION LIGNE                                            00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNTRSL     COMP PIC S9(4).                            00001690
      *--                                                                       
               04 MNTRSL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNTRSF     PIC X.                                     00001700
               04 FILLER     PIC X(4).                                  00001710
               04 MNTRSI     PIC X(8).                                  00001720
      * TYPE TRANSACTION LIGNE                                          00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MTTRSL     COMP PIC S9(4).                            00001740
      *--                                                                       
               04 MTTRSL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MTTRSF     PIC X.                                     00001750
               04 FILLER     PIC X(4).                                  00001760
               04 MTTRSI     PIC X(3).                                  00001770
      * N� OPERATEUR LIGNE                                              00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNOPERL    COMP PIC S9(4).                            00001790
      *--                                                                       
               04 MNOPERL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MNOPERF    PIC X.                                     00001800
               04 FILLER     PIC X(4).                                  00001810
               04 MNOPERI    PIC X(8).                                  00001820
      * CODE DEVISE LIGNE                                               00001830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCDEVL     COMP PIC S9(4).                            00001840
      *--                                                                       
               04 MCDEVL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MCDEVF     PIC X.                                     00001850
               04 FILLER     PIC X(4).                                  00001860
               04 MCDEVI     PIC X.                                     00001870
      * MONTANT LIGNE                                                   00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MPMONTOPL  COMP PIC S9(4).                            00001890
      *--                                                                       
               04 MPMONTOPL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MPMONTOPF  PIC X.                                     00001900
               04 FILLER     PIC X(4).                                  00001910
               04 MPMONTOPI  PIC X(10).                                 00001920
      * MESSAGE ERREUR                                                  00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001940
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MLIBERRI  PIC X(78).                                      00001970
      * CODE TRANSACTION                                                00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MCODTRAI  PIC X(4).                                       00002020
      * CICS DE TRAVAIL                                                 00002030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002050
           02 FILLER    PIC X(4).                                       00002060
           02 MCICSI    PIC X(5).                                       00002070
      * NETNAME                                                         00002080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002090
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002100
           02 FILLER    PIC X(4).                                       00002110
           02 MNETNAMI  PIC X(8).                                       00002120
      * CODE TERMINAL                                                   00002130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002150
           02 FILLER    PIC X(4).                                       00002160
           02 MSCREENI  PIC X(5).                                       00002170
      ***************************************************************** 00002180
      * SDF: EEM46   EEM46                                              00002190
      ***************************************************************** 00002200
       01   EEM46O REDEFINES EEM46I.                                    00002210
           02 FILLER    PIC X(12).                                      00002220
      * DATE DU JOUR                                                    00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MDATJOUA  PIC X.                                          00002250
           02 MDATJOUC  PIC X.                                          00002260
           02 MDATJOUP  PIC X.                                          00002270
           02 MDATJOUH  PIC X.                                          00002280
           02 MDATJOUV  PIC X.                                          00002290
           02 MDATJOUO  PIC X(10).                                      00002300
      * HEURE                                                           00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MTIMJOUA  PIC X.                                          00002330
           02 MTIMJOUC  PIC X.                                          00002340
           02 MTIMJOUP  PIC X.                                          00002350
           02 MTIMJOUH  PIC X.                                          00002360
           02 MTIMJOUV  PIC X.                                          00002370
           02 MTIMJOUO  PIC X(5).                                       00002380
      * NUMERO DE PAGE COURANTE                                         00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNOPAGEA  PIC X.                                          00002410
           02 MNOPAGEC  PIC X.                                          00002420
           02 MNOPAGEP  PIC X.                                          00002430
           02 MNOPAGEH  PIC X.                                          00002440
           02 MNOPAGEV  PIC X.                                          00002450
           02 MNOPAGEO  PIC X(3).                                       00002460
      * NOMBRE TOTAL DE PAGES                                           00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNBPAGEA  PIC X.                                          00002490
           02 MNBPAGEC  PIC X.                                          00002500
           02 MNBPAGEP  PIC X.                                          00002510
           02 MNBPAGEH  PIC X.                                          00002520
           02 MNBPAGEV  PIC X.                                          00002530
           02 MNBPAGEO  PIC X(3).                                       00002540
      * NUMERO SOCIETE                                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNSOCA    PIC X.                                          00002570
           02 MNSOCC    PIC X.                                          00002580
           02 MNSOCP    PIC X.                                          00002590
           02 MNSOCH    PIC X.                                          00002600
           02 MNSOCV    PIC X.                                          00002610
           02 MNSOCO    PIC X(3).                                       00002620
      * NUMERO MAGASIN                                                  00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MNMAGA    PIC X.                                          00002650
           02 MNMAGC    PIC X.                                          00002660
           02 MNMAGP    PIC X.                                          00002670
           02 MNMAGH    PIC X.                                          00002680
           02 MNMAGV    PIC X.                                          00002690
           02 MNMAGO    PIC X(3).                                       00002700
      * DATE DEBUT PERIODE(JJ/MM/SSAA)                                  00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MDDEBA    PIC X.                                          00002730
           02 MDDEBC    PIC X.                                          00002740
           02 MDDEBP    PIC X.                                          00002750
           02 MDDEBH    PIC X.                                          00002760
           02 MDDEBV    PIC X.                                          00002770
           02 MDDEBO    PIC X(10).                                      00002780
      * DATE FIN PERIODE(JJ/MM/SSAA)                                    00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MDFINA    PIC X.                                          00002810
           02 MDFINC    PIC X.                                          00002820
           02 MDFINP    PIC X.                                          00002830
           02 MDFINH    PIC X.                                          00002840
           02 MDFINV    PIC X.                                          00002850
           02 MDFINO    PIC X(10).                                      00002860
      * NUMERO CAISSE                                                   00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MNCAISA   PIC X.                                          00002890
           02 MNCAISC   PIC X.                                          00002900
           02 MNCAISP   PIC X.                                          00002910
           02 MNCAISH   PIC X.                                          00002920
           02 MNCAISV   PIC X.                                          00002930
           02 MNCAISO   PIC X(3).                                       00002940
      * NUMERO TRANSACTION                                              00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MNTRANSA  PIC X.                                          00002970
           02 MNTRANSC  PIC X.                                          00002980
           02 MNTRANSP  PIC X.                                          00002990
           02 MNTRANSH  PIC X.                                          00003000
           02 MNTRANSV  PIC X.                                          00003010
           02 MNTRANSO  PIC X(8).                                       00003020
      * TYPE TRANSACTION                                                00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MTTRANSA  PIC X.                                          00003050
           02 MTTRANSC  PIC X.                                          00003060
           02 MTTRANSP  PIC X.                                          00003070
           02 MTTRANSH  PIC X.                                          00003080
           02 MTTRANSV  PIC X.                                          00003090
           02 MTTRANSO  PIC X(3).                                       00003100
      * MONTANT TRANS.                                                  00003110
           02 FILLER    PIC X(2).                                       00003120
           02 MPMONTA   PIC X.                                          00003130
           02 MPMONTC   PIC X.                                          00003140
           02 MPMONTP   PIC X.                                          00003150
           02 MPMONTH   PIC X.                                          00003160
           02 MPMONTV   PIC X.                                          00003170
           02 MPMONTO   PIC X(10).                                      00003180
      * LIBELLE 'MODE PAIEMENT'                                         00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MLPAIA    PIC X.                                          00003210
           02 MLPAIC    PIC X.                                          00003220
           02 MLPAIP    PIC X.                                          00003230
           02 MLPAIH    PIC X.                                          00003240
           02 MLPAIV    PIC X.                                          00003250
           02 MLPAIO    PIC X(16).                                      00003260
      * MODE PAIEMENT                                                   00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MMPAIA    PIC X.                                          00003290
           02 MMPAIC    PIC X.                                          00003300
           02 MMPAIP    PIC X.                                          00003310
           02 MMPAIH    PIC X.                                          00003320
           02 MMPAIV    PIC X.                                          00003330
           02 MMPAIO    PIC X.                                          00003340
      * LIBELLE 'DEVISE PAIEMENT'                                       00003350
           02 FILLER    PIC X(2).                                       00003360
           02 MLDEVISEA      PIC X.                                     00003370
           02 MLDEVISEC PIC X.                                          00003380
           02 MLDEVISEP PIC X.                                          00003390
           02 MLDEVISEH PIC X.                                          00003400
           02 MLDEVISEV PIC X.                                          00003410
           02 MLDEVISEO      PIC X(17).                                 00003420
      * DEVISE PAIEMENT                                                 00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCDEVISEA      PIC X.                                     00003450
           02 MCDEVISEC PIC X.                                          00003460
           02 MCDEVISEP PIC X.                                          00003470
           02 MCDEVISEH PIC X.                                          00003480
           02 MCDEVISEV PIC X.                                          00003490
           02 MCDEVISEO      PIC X.                                     00003500
      * LIBELLE 'N� DE LA VENTE'                                        00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLVENTEA  PIC X.                                          00003530
           02 MLVENTEC  PIC X.                                          00003540
           02 MLVENTEP  PIC X.                                          00003550
           02 MLVENTEH  PIC X.                                          00003560
           02 MLVENTEV  PIC X.                                          00003570
           02 MLVENTEO  PIC X(19).                                      00003580
      * N� DE LA VENTE                                                  00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MNVENTEA  PIC X.                                          00003610
           02 MNVENTEC  PIC X.                                          00003620
           02 MNVENTEP  PIC X.                                          00003630
           02 MNVENTEH  PIC X.                                          00003640
           02 MNVENTEV  PIC X.                                          00003650
           02 MNVENTEO  PIC X(7).                                       00003660
      * LIBELLE 'N� CODIC'                                              00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MLCODICA  PIC X.                                          00003690
           02 MLCODICC  PIC X.                                          00003700
           02 MLCODICP  PIC X.                                          00003710
           02 MLCODICH  PIC X.                                          00003720
           02 MLCODICV  PIC X.                                          00003730
           02 MLCODICO  PIC X(12).                                      00003740
      * N� CODIC                                                        00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MNCODICA  PIC X.                                          00003770
           02 MNCODICC  PIC X.                                          00003780
           02 MNCODICP  PIC X.                                          00003790
           02 MNCODICH  PIC X.                                          00003800
           02 MNCODICV  PIC X.                                          00003810
           02 MNCODICO  PIC X(7).                                       00003820
      * LIBELLE 'MODE DELIVRANCE'                                       00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MLDELA    PIC X.                                          00003850
           02 MLDELC    PIC X.                                          00003860
           02 MLDELP    PIC X.                                          00003870
           02 MLDELH    PIC X.                                          00003880
           02 MLDELV    PIC X.                                          00003890
           02 MLDELO    PIC X(17).                                      00003900
      * MODE DELIVRANCE                                                 00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MDELA     PIC X.                                          00003930
           02 MDELC     PIC X.                                          00003940
           02 MDELP     PIC X.                                          00003950
           02 MDELH     PIC X.                                          00003960
           02 MDELV     PIC X.                                          00003970
           02 MDELO     PIC X(3).                                       00003980
      * libelle 'n� trans'                                              00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MLNTRANS1A     PIC X.                                     00004010
           02 MLNTRANS1C     PIC X.                                     00004020
           02 MLNTRANS1P     PIC X.                                     00004030
           02 MLNTRANS1H     PIC X.                                     00004040
           02 MLNTRANS1V     PIC X.                                     00004050
           02 MLNTRANS1O     PIC X(2).                                  00004060
      * libelle du type                                                 00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MLTYPE1A  PIC X.                                          00004090
           02 MLTYPE1C  PIC X.                                          00004100
           02 MLTYPE1P  PIC X.                                          00004110
           02 MLTYPE1H  PIC X.                                          00004120
           02 MLTYPE1V  PIC X.                                          00004130
           02 MLTYPE1O  PIC X(4).                                       00004140
      * libelle                                                         00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MLOPER1A  PIC X.                                          00004170
           02 MLOPER1C  PIC X.                                          00004180
           02 MLOPER1P  PIC X.                                          00004190
           02 MLOPER1H  PIC X.                                          00004200
           02 MLOPER1V  PIC X.                                          00004210
           02 MLOPER1O  PIC X(8).                                       00004220
      * libelle 'n� trans'                                              00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MLNTRANS2A     PIC X.                                     00004250
           02 MLNTRANS2C     PIC X.                                     00004260
           02 MLNTRANS2P     PIC X.                                     00004270
           02 MLNTRANS2H     PIC X.                                     00004280
           02 MLNTRANS2V     PIC X.                                     00004290
           02 MLNTRANS2O     PIC X(8).                                  00004300
      * libelle du type                                                 00004310
           02 FILLER    PIC X(2).                                       00004320
           02 MLTYPE2A  PIC X.                                          00004330
           02 MLTYPE2C  PIC X.                                          00004340
           02 MLTYPE2P  PIC X.                                          00004350
           02 MLTYPE2H  PIC X.                                          00004360
           02 MLTYPE2V  PIC X.                                          00004370
           02 MLTYPE2O  PIC X(5).                                       00004380
      * libelle                                                         00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MLOPER2A  PIC X.                                          00004410
           02 MLOPER2C  PIC X.                                          00004420
           02 MLOPER2P  PIC X.                                          00004430
           02 MLOPER2H  PIC X.                                          00004440
           02 MLOPER2V  PIC X.                                          00004450
           02 MLOPER2O  PIC X(8).                                       00004460
      * ENTETE REGLEMENT                                                00004470
           02 MLIGNEO OCCURS   12 TIMES .                               00004480
      * ZONE DE SELECTION                                               00004490
               04 FILLER     PIC X(2).                                  00004500
               04 MTSELECTA  PIC X.                                     00004510
               04 MTSELECTC  PIC X.                                     00004520
               04 MTSELECTP  PIC X.                                     00004530
               04 MTSELECTH  PIC X.                                     00004540
               04 MTSELECTV  PIC X.                                     00004550
               04 MTSELECTO  PIC X.                                     00004560
      * DATE VENTE (JJ/MM/SSAA)                                         00004570
               04 FILLER     PIC X(2).                                  00004580
               04 MDVENTEA   PIC X.                                     00004590
               04 MDVENTEC   PIC X.                                     00004600
               04 MDVENTEP   PIC X.                                     00004610
               04 MDVENTEH   PIC X.                                     00004620
               04 MDVENTEV   PIC X.                                     00004630
               04 MDVENTEO   PIC X(10).                                 00004640
      * HEURE DE VENTE                                                  00004650
               04 FILLER     PIC X(2).                                  00004660
               04 MTIMVTEA   PIC X.                                     00004670
               04 MTIMVTEC   PIC X.                                     00004680
               04 MTIMVTEP   PIC X.                                     00004690
               04 MTIMVTEH   PIC X.                                     00004700
               04 MTIMVTEV   PIC X.                                     00004710
               04 MTIMVTEO   PIC X(5).                                  00004720
      * N� CAISSE LIGNE                                                 00004730
               04 FILLER     PIC X(2).                                  00004740
               04 MNCAISSEA  PIC X.                                     00004750
               04 MNCAISSEC  PIC X.                                     00004760
               04 MNCAISSEP  PIC X.                                     00004770
               04 MNCAISSEH  PIC X.                                     00004780
               04 MNCAISSEV  PIC X.                                     00004790
               04 MNCAISSEO  PIC X(3).                                  00004800
      * N� TRANSACTION LIGNE                                            00004810
               04 FILLER     PIC X(2).                                  00004820
               04 MNTRSA     PIC X.                                     00004830
               04 MNTRSC     PIC X.                                     00004840
               04 MNTRSP     PIC X.                                     00004850
               04 MNTRSH     PIC X.                                     00004860
               04 MNTRSV     PIC X.                                     00004870
               04 MNTRSO     PIC X(8).                                  00004880
      * TYPE TRANSACTION LIGNE                                          00004890
               04 FILLER     PIC X(2).                                  00004900
               04 MTTRSA     PIC X.                                     00004910
               04 MTTRSC     PIC X.                                     00004920
               04 MTTRSP     PIC X.                                     00004930
               04 MTTRSH     PIC X.                                     00004940
               04 MTTRSV     PIC X.                                     00004950
               04 MTTRSO     PIC X(3).                                  00004960
      * N� OPERATEUR LIGNE                                              00004970
               04 FILLER     PIC X(2).                                  00004980
               04 MNOPERA    PIC X.                                     00004990
               04 MNOPERC    PIC X.                                     00005000
               04 MNOPERP    PIC X.                                     00005010
               04 MNOPERH    PIC X.                                     00005020
               04 MNOPERV    PIC X.                                     00005030
               04 MNOPERO    PIC X(8).                                  00005040
      * CODE DEVISE LIGNE                                               00005050
               04 FILLER     PIC X(2).                                  00005060
               04 MCDEVA     PIC X.                                     00005070
               04 MCDEVC     PIC X.                                     00005080
               04 MCDEVP     PIC X.                                     00005090
               04 MCDEVH     PIC X.                                     00005100
               04 MCDEVV     PIC X.                                     00005110
               04 MCDEVO     PIC X.                                     00005120
      * MONTANT LIGNE                                                   00005130
               04 FILLER     PIC X(2).                                  00005140
               04 MPMONTOPA  PIC X.                                     00005150
               04 MPMONTOPC  PIC X.                                     00005160
               04 MPMONTOPP  PIC X.                                     00005170
               04 MPMONTOPH  PIC X.                                     00005180
               04 MPMONTOPV  PIC X.                                     00005190
               04 MPMONTOPO  PIC X(10).                                 00005200
      * MESSAGE ERREUR                                                  00005210
           02 FILLER    PIC X(2).                                       00005220
           02 MLIBERRA  PIC X.                                          00005230
           02 MLIBERRC  PIC X.                                          00005240
           02 MLIBERRP  PIC X.                                          00005250
           02 MLIBERRH  PIC X.                                          00005260
           02 MLIBERRV  PIC X.                                          00005270
           02 MLIBERRO  PIC X(78).                                      00005280
      * CODE TRANSACTION                                                00005290
           02 FILLER    PIC X(2).                                       00005300
           02 MCODTRAA  PIC X.                                          00005310
           02 MCODTRAC  PIC X.                                          00005320
           02 MCODTRAP  PIC X.                                          00005330
           02 MCODTRAH  PIC X.                                          00005340
           02 MCODTRAV  PIC X.                                          00005350
           02 MCODTRAO  PIC X(4).                                       00005360
      * CICS DE TRAVAIL                                                 00005370
           02 FILLER    PIC X(2).                                       00005380
           02 MCICSA    PIC X.                                          00005390
           02 MCICSC    PIC X.                                          00005400
           02 MCICSP    PIC X.                                          00005410
           02 MCICSH    PIC X.                                          00005420
           02 MCICSV    PIC X.                                          00005430
           02 MCICSO    PIC X(5).                                       00005440
      * NETNAME                                                         00005450
           02 FILLER    PIC X(2).                                       00005460
           02 MNETNAMA  PIC X.                                          00005470
           02 MNETNAMC  PIC X.                                          00005480
           02 MNETNAMP  PIC X.                                          00005490
           02 MNETNAMH  PIC X.                                          00005500
           02 MNETNAMV  PIC X.                                          00005510
           02 MNETNAMO  PIC X(8).                                       00005520
      * CODE TERMINAL                                                   00005530
           02 FILLER    PIC X(2).                                       00005540
           02 MSCREENA  PIC X.                                          00005550
           02 MSCREENC  PIC X.                                          00005560
           02 MSCREENP  PIC X.                                          00005570
           02 MSCREENH  PIC X.                                          00005580
           02 MSCREENV  PIC X.                                          00005590
           02 MSCREENO  PIC X(5).                                       00005600
                                                                                
