      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * POUR GARDER UNE VENTE ENTIERE DECODEE EN FRANCAIS                       
V6.5  * METTRE LES VARIABLES DANS L'ORDRE DE LA EC002                           
LD0310* 22/03/2010 MODIFICATION MESSAGE D.COM .                                 
V41R0 * 03/03/2011 MODIFICATION MESSAGE D.COM . KIALA + MONOEQUIPAGE            
V43R0 * 08/07/2011 MODIFICATION MESSAGE D.COM . ajout date reprise              
V50R0 * 20/01/2012 MODIFICATION MESSAGE D.COM . ajout post-pse                  
M23862* 25/07/2013 MODIFICATION MESSAGE D.COM . PSE ASSURANTIELLE               
V6.5  * 21/08/2013 MODIFICATION MESSAGE D.COM . CONSIGNES                       
MGD14 * 15/01/2015 Ajout du type de vente                                       
1000  * 26/03/2015 ajout date et heure de prise de commande client              
promo * 25/08/2015 ajout des zones pour les Cartes cadeaux (moteur promo        
      ******************************************************************        
           02  W-CC-NSOC-ECOM              PIC X(003).                          
           02  W-CC-NLIEU-ECOM             PIC X(003).                          
           02  W-CC-NSOC-LIVR              PIC X(003).                          
           02  W-CC-CELEMEN                PIC X(005).                          
           02  W-CC-NSOC-EN-TRT            PIC X(003).                          
           02  W-CC-DATE-DU-JOUR           PIC X(008).                          
           02  W-CC-DATE-DU-LUNDI          PIC X(008).                          
           02  W-CC-CDEVISE                PIC X(003).                          
MGD14      02  W-CC-TYPVTE                 PIC X.                               
MGD14      02  W-CC-CANAL                  PIC X(10).                           
MGD14 *    02  filler                      PIC X(10).                           
promo      02  W-CC-V-P1-MAX               PIC 9(02) VALUE 10.                  
promo      02  W-CC-V-P2-MAX               PIC 9(02) VALUE 20.                  
promo      02  filler                      PIC X(06).                           
           02  W-CC-FLAG-LIVRAISONS        PIC X.                               
       88  W-CC-IL-Y-A-DES-LIVRAISONS    VALUE 'O'.                             
       88  W-CC-YA-PAS-DE-LIVRAISONS     VALUE 'N'.                             
           02  W-CC-FLAG-CLICK-COLLECT     PIC X.                               
       88  W-CC-VENTE-CLICK-COLLECT      VALUE 'O'.                             
       88  W-CC-PAS-CLICK-COLLECT        VALUE 'N'.                             
           02  W-CC-RETOUR                 PIC 9(1).                            
           02  W-CC-MESSAGE                PIC X(58).                           
           02  W-COMMANDE-CLIENT.                                               
               03  W-CC-ENTETE.                                                 
                   04  W-CC-E-NUM-CMD-DC   PIC S9(15) PACKED-DECIMAL.           
                   04  W-CC-E-NOM-LIV      PIC X(025).                          
                   04  W-CC-E-PNOM-LIV     PIC X(015).                          
                   04  W-CC-E-ADR1-LIV     PIC X(032).                          
                   04  W-CC-E-ADR2-LIV     PIC X(032).                          
                   04  W-CC-E-CPT-ADR-LIV  PIC X(032).                          
                   04  W-CC-E-EST-MAIS     PIC X(001).                          
                   04  W-CC-E-BMENT        PIC X(003).                          
                   04  W-CC-E-ESCAL        PIC X(003).                          
                   04  W-CC-E-ETAGE        PIC X(003).                          
                   04  W-CC-E-PTE          PIC X(003).                          
                   04  W-CC-E-CODE         PIC X(010).                          
                   04  W-CC-E-C-PAYS-LIV   PIC X(002).                          
                   04  W-CC-E-C-POST-LIV   PIC X(005).                          
                   04  W-CC-E-C-INSEE-LIV  PIC X(005).                          
                   04  W-CC-E-CMUNE-LIV    PIC X(032).                          
                   04  W-CC-E-NOM-FACT     PIC X(025).                          
                   04  W-CC-E-PNOM-FACT    PIC X(015).                          
                   04  W-CC-E-ADR1-FACT    PIC X(032).                          
                   04  W-CC-E-ADR2-FACT    PIC X(032).                          
                   04  W-CC-E-CPT-ADR-FACT PIC X(032).                          
                   04  W-CC-E-C-PAYS-FACT  PIC X(002).                          
                   04  W-CC-E-C-POST-FACT  PIC X(005).                          
                   04  W-CC-E-C-INSEE-FACT PIC X(005).                          
                   04  W-CC-E-CMUNE-FACT   PIC X(032).                          
                   04  W-CC-E-CIVIL        PIC X(005).                          
                   04  W-CC-E-TP-CMD       PIC X(005).                          
                   04  W-CC-E-TEL-FIXE     PIC X(010).                          
                   04  W-CC-E-TEL-PORT     PIC X(010).                          
                   04  W-CC-E-CMENT-VT     PIC X(130).                          
                   04  W-CC-E-CMENT-LIV    PIC X(170).                          
                   04  W-CC-E-CMENT-ACC    PIC X(255).                          
                   04  W-CC-E-REPR-MAT     PIC X(001).                          
V41R0              04  W-CC-E-NB-REPRISE   PIC X(001).                          
    |              04  W-CC-E-ACCES-LOG    PIC X(001).                          
    |              04  W-CC-E-PDT-VOLUMIQ  PIC X(001).                          
                   04  W-CC-E-MT-PMENT-SI  PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-CC-E-MT-SHIP      PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-CC-E-MT-AJUST     PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-CC-E-STAT         PIC X(001).                          
                   04  W-CC-E-C-RET        PIC X(002).                          
                   04  W-CC-E-TIMESTP      PIC S9(15) PACKED-DECIMAL.           
LD0310*            04  W-CC-E-EMAIL        PIC X(063).                          
Ld0310*            04  W-CC-E-CC-PASSWORD  PIC X(010).                          
                   04  W-CC-E-EVENT        PIC X(010).                          
                   04  W-CC-E-IDENT-CLIENT PIC X(008).                          
LD0310             04  W-CC-E-DATE-NAISS   PIC X(008).                          
LD0310             04  W-CC-E-CODE-NAISS   PIC X(005).                          
LD0310             04  W-CC-E-VILLE-NAISS  PIC X(128).                          
V43R0              04  W-CC-E-C-VEND       PIC X(06).                           
V6.5               04  W-CC-E-CC-TYPE         PIC X(010).                       
V6.5               04  W-CC-E-CC-EVENT-TYPE-ID   PIC X(015).                    
V6.5               04  W-CC-E-CC-EVENT-DATE-TIME PIC X(026).                    
V6.5               04  W-CC-E-CC-DELIVERY-CODE   PIC X(010).                    
V6.5               04  W-CC-E-CC-COLLECTION-CODE PIC X(010).                    
V6.5               04  W-CC-E-CC-SITE-NAME       PIC X(020).                    
V6.5               04  W-CC-E-CC-DOOR-NAME       PIC X(010).                    
V6.5               04  W-CC-E-CC-EMPLOYEE-NAME   PIC X(010).                    
      *            AJOUTER LES NOUVEAUX CODES GV10 ICI AVANT EMAIL              
LD0310             04  W-CC-E-EMAIL        PIC X(256).                          
LD0310             04  W-CC-E-CC-PASSWORD  PIC X(010).                          
      * tableau des r�glements (indice, utilis�, max)                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-CC-R-I                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-R-I                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-CC-R-U                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-R-U                PIC S9(4) COMP-5.                    
      *}                                                                        
      *        03  W-CC-R-MAX � modifier dans COMMECCL                          
               03  W-CC-REGLEMENT       OCCURS 10.                              
                   04  W-CC-R-NUM-CMD-SI   PIC X(007).                          
                   04  W-CC-R-L-PMENT-SI   PIC X(002).                          
                   04  W-CC-R-MD-PMENT     PIC X(005).                          
                   04  W-CC-R-ID-PMENT     PIC X(015).                          
                   04  W-CC-R-NUM-CAISSE   PIC X(005).                          
                   04  W-CC-R-NUM-ESSEMT   PIC X(007).                          
                   04  W-CC-R-DT-ESSEMT    PIC X(008).                          
                   04  W-CC-R-MT           PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-CC-R-STAT         PIC X(001).                          
                   04  W-CC-R-C-RET        PIC X(002).                          
      * a ne pas transmettre a eCom, pour les besoins du traitement             
                   04  W-CC-R-MT-PRIS      PIC S9(7)V99 PACKED-DECIMAL.         
                   04  W-CC-R-C-SOC        PIC X(003).                          
                   04  W-CC-R-C-LIEU       PIC X(003).                          
      * tableau des ventes (indice, utilis�, max)                               
      *    max attention: le tableau a toujours un poste de plus que            
      *    celui indiqu� dans W-CC-V-MAX, pour pouvoir trier le tableau         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-CC-V-I                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-V-I                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  W-CC-V-U                PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-V-U                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
promo *        03  W-CC-V-P1               PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-V-P1               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
promo *        03  W-CC-V-P2               PIC S9(4) BINARY.                    
      *--                                                                       
               03  W-CC-V-P2               PIC S9(4) COMP-5.                    
      *}                                                                        
      *        03  W-CC-V-MAX � modifier dans COMMECCL                          
               03  W-CC-VENTE           OCCURS 51.                              
                   04 W-CC-V-C-SOC        PIC X(003).                           
                   04 W-CC-V-C-LIEU       PIC X(003).                           
                   04 W-CC-V-NUM-CMD-SI   PIC X(007).                           
                   04 W-CC-V-CTYPENREG    PIC X(001).                           
                   04 W-CC-V-CENREG       PIC X(005).                           
                   04 W-CC-V-L-CMD-SI     PIC S9(5) PACKED-DECIMAL.             
                   04 W-CC-V-CODIC        PIC X(007).                           
                   04 W-CC-V-QTY          PIC S9(3) PACKED-DECIMAL.             
                   04 W-CC-V-PX-VT        PIC S9(7)V99 PACKED-DECIMAL.          
                   04 W-CC-V-MODE-DELIV   PIC X(005).                           
                   04 W-CC-V-DT-LIV       PIC X(008).                           
V5.1.P             04 W-CC-V-DT-LIV-MAX   PIC X(008).                           
                   04 W-CC-V-CREN-LIV     PIC X(005).                           
LD0610*            04 W-CC-V-NUM-COLIS    PIC X(015).                           
LD0610             04 W-CC-V-NUM-COLIS    PIC X(026).                           
                   04 W-CC-V-CARSPEC      PIC X(015).                           
                   04 W-CC-V-STAT         PIC X(001).                           
                   04 W-CC-V-C-RET        PIC X(002).                           
                   04 W-CC-V-DELIVERY     PIC X(012).                           
                   04 W-CC-V-NSOC-CC      PIC X(003).                           
                   04 W-CC-V-NLIEU-CC     PIC X(003).                           
LD0310             04 W-CC-V-DT-TOPE      PIC X(008).                           
LD0310             04 W-CC-V-NCODICGRP    PIC X(007).                           
LD0310             04 W-CC-V-IS-PRECO     PIC X(001).                           
LD0310             04 W-CC-V-T-TVA        PIC X(005).                           
V41R0              04 W-CC-V-RELAIS-ID    PIC X(010).                           
V43R0              04 W-CC-V-DT-REPRISE   PIC X(008).                           
v5.1.p             04 W-CC-CREN-HDEB      PIC X(004).                           
v5.1.p             04 W-CC-CREN-Hfin      PIC X(004).                           
v5.1.p             04 W-CC-V-REASON       pic x(10).                            
      * a ne pas transmettre a eCom, pour les besoins du traitement             
                   04 W-CC-V-MODE-DELIV-L PIC X(005).                           
                   04 W-CC-V-NB-SERVICES  PIC S9(3) PACKED-DECIMAL.             
LD0310*            04 W-CC-V-NCODICGRP    PIC X(007).                           
                   04 W-CC-V-NSOCLIVR     PIC X(003).                           
                   04 W-CC-V-NDEPOT       PIC X(003).                           
LD0310             04 W-CC-V-WEMPORTE     PIC X(008).                           
V41R0              04 W-CC-V-PX-VT-UNIT   PIC S9(7)V9(4) PACKED-DECIMAL.        
V50R0              04 W-CC-V-POST-PSE     PIC X(001).                           
M23862             04 W-CC-V-NUM-PS       PIC X(008).                           
promo          03  W-CC-CARTEKDO.                                               
promo              04 W-CC-CARTE-KDO       OCCURS 10.                           
promo                05 W-CC-V-RANG        PIC X(010).                          
promo                05 W-CC-POSTE-KDO     OCCURS 20.                           
promo                   07 W-CC-V-NOM      PIC X(020).                          
promo                   07 W-CC-V-VALEUR   PIC X(020).                          
               03 w-cc-user-data.                                               
                   04 W-CC-U-asc           pic x(01).                           
                   04 W-CC-U-esc           pic x(01).                           
                   04 W-CC-U-asc-u         pic x(01).                           
                   04 W-CC-U-marches       pic x(01).                           
                   04 W-CC-U-haut-col      pic x(01).                           
1000           03 w-cc-v-date-creat        pic x(08).                           
               03 w-cc-v-heure-creat       pic x(06).                           
promo *        03 filler                   pic x(086).                          
                                                                                
