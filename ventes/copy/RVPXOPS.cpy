      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PXOPS PRIX OPéRATIONS SPéCIALES        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPXOPS.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPXOPS.                                                             
      *}                                                                        
           05  PXOPS-CTABLEG2    PIC X(15).                                     
           05  PXOPS-CTABLEG2-REDEF REDEFINES PXOPS-CTABLEG2.                   
               10  PXOPS-COPS            PIC X(07).                             
               10  PXOPS-DOPS            PIC X(08).                             
           05  PXOPS-WTABLEG     PIC X(80).                                     
           05  PXOPS-WTABLEG-REDEF  REDEFINES PXOPS-WTABLEG.                    
               10  PXOPS-OPATTR          PIC X(10).                             
               10  PXOPS-LIEU            PIC X(11).                             
               10  PXOPS-PXTYPE          PIC X(01).                             
               10  PXOPS-PXATTR          PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPXOPS-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPXOPS-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PXOPS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PXOPS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PXOPS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PXOPS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
