      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPP1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPP1000                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPP1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPP1000.                                                            
      *}                                                                        
           02  PP10-DMOISPAYE                                                   
               PIC X(0006).                                                     
           02  PP10-CPRIME                                                      
               PIC X(0005).                                                     
           02  PP10-NLIGNE                                                      
               PIC X(0002).                                                     
           02  PP10-NRUB                                                        
               PIC X(0002).                                                     
           02  PP10-LRUB                                                        
               PIC X(0030).                                                     
           02  PP10-CORIGINEZIN                                                 
               PIC X(0002).                                                     
           02  PP10-PMONTANTZIN                                                 
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP10-CORIGINEZ1                                                  
               PIC X(0002).                                                     
           02  PP10-PMONTANTZ1                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP10-CORIGINEZ2                                                  
               PIC X(0002).                                                     
           02  PP10-PMONTANTZ2                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP10-CORIGINEZ3                                                  
               PIC X(0002).                                                     
           02  PP10-PMONTANTZ3                                                  
               PIC S9(11)V9(0004) COMP-3.                                       
           02  PP10-COPERATEUR1                                                 
               PIC X(0001).                                                     
           02  PP10-COPERATEUR2                                                 
               PIC X(0001).                                                     
           02  PP10-CPRIORITE                                                   
               PIC X(0001).                                                     
           02  PP10-CCALCUL                                                     
               PIC X(0001).                                                     
           02  PP10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPP1000                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPP1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPP1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-DMOISPAYE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-DMOISPAYE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-NRUB-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-NRUB-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-LRUB-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-LRUB-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CORIGINEZIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CORIGINEZIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-PMONTANTZIN-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-PMONTANTZIN-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CORIGINEZ1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CORIGINEZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-PMONTANTZ1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-PMONTANTZ1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CORIGINEZ2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CORIGINEZ2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-PMONTANTZ2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-PMONTANTZ2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CORIGINEZ3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CORIGINEZ3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-PMONTANTZ3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-PMONTANTZ3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-COPERATEUR1-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-COPERATEUR1-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-COPERATEUR2-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-COPERATEUR2-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CPRIORITE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CPRIORITE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-CCALCUL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-CCALCUL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PP10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PP10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
