      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA002 AU 13/03/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,01,BI,A,                          *        
      *                           08,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA002.                                                        
            05 NOMETAT-IBA002           PIC X(6) VALUE 'IBA002'.                
            05 RUPTURES-IBA002.                                                 
           10 IBA002-WANNUL             PIC X(01).                      007  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA002-SEQUENCE           PIC S9(04) COMP.                008  002
      *--                                                                       
           10 IBA002-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA002.                                                   
           10 IBA002-NBA                PIC X(06).                      010  006
           10 IBA002-NFACTBA            PIC X(10).                      016  010
           10 IBA002-NFACTREM           PIC X(10).                      026  010
           10 IBA002-NTIERSSAP          PIC X(10).                      036  010
           10 IBA002-SOULIGNE           PIC X(22).                      046  022
           10 IBA002-TYPEDEBA           PIC X(22).                      068  022
           10 IBA002-PBA                PIC S9(06)V9(2) COMP-3.         090  005
           10 IBA002-DEMIS              PIC X(08).                      095  008
           10 IBA002-DICEMIS            PIC X(08).                      103  008
            05 FILLER                      PIC X(402).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA002-LONG           PIC S9(4)   COMP  VALUE +110.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA002-LONG           PIC S9(4) COMP-5  VALUE +110.           
                                                                                
      *}                                                                        
