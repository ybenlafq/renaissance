      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET      : EDITION DE FICHES                               *        
      *  TS          : TSCS55                                          *        
      *  TITRE       : TS CONTENANT LES DONNEES SPECIFIQUES            *        
      *                DE LA FICHE A EDITER                            *        
      *  LONGUEUR    : 177                                             *        
      *                                                                *        
      ******************************************************************        
       01  TSCS55-IDENT.                                                        
           05 FILLER                 PIC X(04)       VALUE 'CS55'.              
           05 TSCS55-EIBTRMID        PIC X(04)       VALUE SPACES.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSCS55-LONG               PIC S9(04) COMP VALUE +177.                
      *--                                                                       
       01  TSCS55-LONG               PIC S9(04) COMP-5 VALUE +177.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TSCS55-RANG               PIC S9(04) COMP VALUE +0.                  
      *--                                                                       
       01  TSCS55-RANG               PIC S9(04) COMP-5 VALUE +0.                
      *}                                                                        
       01  TSCS55-DSECT.                                                        
           05 TSCS55-RUPTURES.                                                  
              10 TSCS55-PAVE.                                                   
                 15 TSCS55-FICHE.                                               
                    20 TSCS55-NFICHE PIC X(07).                                 
                 15 TSCS55-NPAVE     PIC X(01).                                 
              10 TSCS55-NDONNEE      PIC X(05).                                 
           05 TSCS55-CHAMPS.                                                    
              10 TSCS55-LNG          PIC 9(03).                                 
              10 TSCS55-LIG          PIC X(132).                                
      *       COORDONNEE RELATIVE                                               
              10 TSCS55-ABC-R        PIC 9(04).                                 
              10 TSCS55-ORD-R        PIC 9(04).                                 
              10 TSCS55-POL          PIC 9(03).                                 
              10 TSCS55-COULEUR.                                                
                 15 TSCS55-COU          PIC X(05).                              
                 15 TSCS55-CUT          PIC X(01).                              
              10 TSCS55-LGR          PIC 9(04).                                 
              10 TSCS55-HTR          PIC 9(04).                                 
              10 TSCS55-RAY          PIC 9(04).                                 
                                                                                
