      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV11   EAV11                                              00000020
      ***************************************************************** 00000030
       01   EAV11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n� de societe sais                                              00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n� de lieu saisie                                               00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libell� du lieu                                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
      * n� de lieu                                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUI   PIC X(3).                                       00000330
      * libelle lieu                                                    00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLLIEUI   PIC X(20).                                      00000380
      * n� d'avoir                                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNAVOIRI  PIC X(8).                                       00000430
      * code type avoir                                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000450
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCTYPUTILI     PIC X(5).                                  00000480
      * libelle type avoir                                              00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLTYPUTILI     PIC X(20).                                 00000530
      * code type d'utilisation                                         00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCMOTIFI  PIC X(5).                                       00000580
      * libelle type d'utilisation                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLMOTIFI  PIC X(20).                                      00000630
      * montant de l'avoir                                              00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAVOIRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MPAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAVOIRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MPAVOIRI  PIC X(10).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000690
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCDEVISEI      PIC X(3).                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000730
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLDEVISEI      PIC X(5).                                  00000760
      * titre du nom                                                    00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCTITRENOMI    PIC X(5).                                  00000810
      * nom du client                                                   00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLNOMI    PIC X(25).                                      00000860
      * date d'emission                                                 00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MDEMISI   PIC X(10).                                      00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLIBERRI  PIC X(78).                                      00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCODTRAI  PIC X(4).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCICSI    PIC X(5).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MNETNAMI  PIC X(8).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MSCREENI  PIC X(4).                                       00001110
      ***************************************************************** 00001120
      * SDF: EAV11   EAV11                                              00001130
      ***************************************************************** 00001140
       01   EAV11O REDEFINES EAV11I.                                    00001150
           02 FILLER    PIC X(12).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDATJOUA  PIC X.                                          00001180
           02 MDATJOUC  PIC X.                                          00001190
           02 MDATJOUP  PIC X.                                          00001200
           02 MDATJOUH  PIC X.                                          00001210
           02 MDATJOUV  PIC X.                                          00001220
           02 MDATJOUO  PIC X(10).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MTIMJOUA  PIC X.                                          00001250
           02 MTIMJOUC  PIC X.                                          00001260
           02 MTIMJOUP  PIC X.                                          00001270
           02 MTIMJOUH  PIC X.                                          00001280
           02 MTIMJOUV  PIC X.                                          00001290
           02 MTIMJOUO  PIC X(5).                                       00001300
      * n� de societe sais                                              00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNSOCSAISIEA   PIC X.                                     00001330
           02 MNSOCSAISIEC   PIC X.                                     00001340
           02 MNSOCSAISIEP   PIC X.                                     00001350
           02 MNSOCSAISIEH   PIC X.                                     00001360
           02 MNSOCSAISIEV   PIC X.                                     00001370
           02 MNSOCSAISIEO   PIC X(3).                                  00001380
      * n� de lieu saisie                                               00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MNLIEUSAISIEA  PIC X.                                     00001410
           02 MNLIEUSAISIEC  PIC X.                                     00001420
           02 MNLIEUSAISIEP  PIC X.                                     00001430
           02 MNLIEUSAISIEH  PIC X.                                     00001440
           02 MNLIEUSAISIEV  PIC X.                                     00001450
           02 MNLIEUSAISIEO  PIC X(3).                                  00001460
      * libell� du lieu                                                 00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MLLIEUSAISIEA  PIC X.                                     00001490
           02 MLLIEUSAISIEC  PIC X.                                     00001500
           02 MLLIEUSAISIEP  PIC X.                                     00001510
           02 MLLIEUSAISIEH  PIC X.                                     00001520
           02 MLLIEUSAISIEV  PIC X.                                     00001530
           02 MLLIEUSAISIEO  PIC X(20).                                 00001540
      * n� de lieu                                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MNLIEUA   PIC X.                                          00001570
           02 MNLIEUC   PIC X.                                          00001580
           02 MNLIEUP   PIC X.                                          00001590
           02 MNLIEUH   PIC X.                                          00001600
           02 MNLIEUV   PIC X.                                          00001610
           02 MNLIEUO   PIC X(3).                                       00001620
      * libelle lieu                                                    00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MLLIEUA   PIC X.                                          00001650
           02 MLLIEUC   PIC X.                                          00001660
           02 MLLIEUP   PIC X.                                          00001670
           02 MLLIEUH   PIC X.                                          00001680
           02 MLLIEUV   PIC X.                                          00001690
           02 MLLIEUO   PIC X(20).                                      00001700
      * n� d'avoir                                                      00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNAVOIRA  PIC X.                                          00001730
           02 MNAVOIRC  PIC X.                                          00001740
           02 MNAVOIRP  PIC X.                                          00001750
           02 MNAVOIRH  PIC X.                                          00001760
           02 MNAVOIRV  PIC X.                                          00001770
           02 MNAVOIRO  PIC X(8).                                       00001780
      * code type avoir                                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCTYPUTILA     PIC X.                                     00001810
           02 MCTYPUTILC     PIC X.                                     00001820
           02 MCTYPUTILP     PIC X.                                     00001830
           02 MCTYPUTILH     PIC X.                                     00001840
           02 MCTYPUTILV     PIC X.                                     00001850
           02 MCTYPUTILO     PIC X(5).                                  00001860
      * libelle type avoir                                              00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MLTYPUTILA     PIC X.                                     00001890
           02 MLTYPUTILC     PIC X.                                     00001900
           02 MLTYPUTILP     PIC X.                                     00001910
           02 MLTYPUTILH     PIC X.                                     00001920
           02 MLTYPUTILV     PIC X.                                     00001930
           02 MLTYPUTILO     PIC X(20).                                 00001940
      * code type d'utilisation                                         00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCMOTIFA  PIC X.                                          00001970
           02 MCMOTIFC  PIC X.                                          00001980
           02 MCMOTIFP  PIC X.                                          00001990
           02 MCMOTIFH  PIC X.                                          00002000
           02 MCMOTIFV  PIC X.                                          00002010
           02 MCMOTIFO  PIC X(5).                                       00002020
      * libelle type d'utilisation                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLMOTIFA  PIC X.                                          00002050
           02 MLMOTIFC  PIC X.                                          00002060
           02 MLMOTIFP  PIC X.                                          00002070
           02 MLMOTIFH  PIC X.                                          00002080
           02 MLMOTIFV  PIC X.                                          00002090
           02 MLMOTIFO  PIC X(20).                                      00002100
      * montant de l'avoir                                              00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MPAVOIRA  PIC X.                                          00002130
           02 MPAVOIRC  PIC X.                                          00002140
           02 MPAVOIRP  PIC X.                                          00002150
           02 MPAVOIRH  PIC X.                                          00002160
           02 MPAVOIRV  PIC X.                                          00002170
           02 MPAVOIRO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MCDEVISEA      PIC X.                                     00002200
           02 MCDEVISEC PIC X.                                          00002210
           02 MCDEVISEP PIC X.                                          00002220
           02 MCDEVISEH PIC X.                                          00002230
           02 MCDEVISEV PIC X.                                          00002240
           02 MCDEVISEO      PIC X(3).                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLDEVISEA      PIC X.                                     00002270
           02 MLDEVISEC PIC X.                                          00002280
           02 MLDEVISEP PIC X.                                          00002290
           02 MLDEVISEH PIC X.                                          00002300
           02 MLDEVISEV PIC X.                                          00002310
           02 MLDEVISEO      PIC X(5).                                  00002320
      * titre du nom                                                    00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCTITRENOMA    PIC X.                                     00002350
           02 MCTITRENOMC    PIC X.                                     00002360
           02 MCTITRENOMP    PIC X.                                     00002370
           02 MCTITRENOMH    PIC X.                                     00002380
           02 MCTITRENOMV    PIC X.                                     00002390
           02 MCTITRENOMO    PIC X(5).                                  00002400
      * nom du client                                                   00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLNOMA    PIC X.                                          00002430
           02 MLNOMC    PIC X.                                          00002440
           02 MLNOMP    PIC X.                                          00002450
           02 MLNOMH    PIC X.                                          00002460
           02 MLNOMV    PIC X.                                          00002470
           02 MLNOMO    PIC X(25).                                      00002480
      * date d'emission                                                 00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MDEMISA   PIC X.                                          00002510
           02 MDEMISC   PIC X.                                          00002520
           02 MDEMISP   PIC X.                                          00002530
           02 MDEMISH   PIC X.                                          00002540
           02 MDEMISV   PIC X.                                          00002550
           02 MDEMISO   PIC X(10).                                      00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MLIBERRA  PIC X.                                          00002580
           02 MLIBERRC  PIC X.                                          00002590
           02 MLIBERRP  PIC X.                                          00002600
           02 MLIBERRH  PIC X.                                          00002610
           02 MLIBERRV  PIC X.                                          00002620
           02 MLIBERRO  PIC X(78).                                      00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCODTRAA  PIC X.                                          00002650
           02 MCODTRAC  PIC X.                                          00002660
           02 MCODTRAP  PIC X.                                          00002670
           02 MCODTRAH  PIC X.                                          00002680
           02 MCODTRAV  PIC X.                                          00002690
           02 MCODTRAO  PIC X(4).                                       00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCICSA    PIC X.                                          00002720
           02 MCICSC    PIC X.                                          00002730
           02 MCICSP    PIC X.                                          00002740
           02 MCICSH    PIC X.                                          00002750
           02 MCICSV    PIC X.                                          00002760
           02 MCICSO    PIC X(5).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MNETNAMA  PIC X.                                          00002790
           02 MNETNAMC  PIC X.                                          00002800
           02 MNETNAMP  PIC X.                                          00002810
           02 MNETNAMH  PIC X.                                          00002820
           02 MNETNAMV  PIC X.                                          00002830
           02 MNETNAMO  PIC X(8).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MSCREENA  PIC X.                                          00002860
           02 MSCREENC  PIC X.                                          00002870
           02 MSCREENP  PIC X.                                          00002880
           02 MSCREENH  PIC X.                                          00002890
           02 MSCREENV  PIC X.                                          00002900
           02 MSCREENO  PIC X(4).                                       00002910
                                                                                
