      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESV01   ESV01                                              00000020
      ***************************************************************** 00000030
       01   ESV01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGETL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGETL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGETF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGETI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGI    PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTPSAVL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCTPSAVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTPSAVF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCTPSAVI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTPSAVL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLTPSAVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTPSAVF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLTPSAVI  PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEFFETL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDEFFETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDEFFETF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDEFFETI  PIC X(10).                                      00000450
           02 MNSOCSAVD OCCURS   20 TIMES .                             00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCSAVL    COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNSOCSAVL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNSOCSAVF    PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNSOCSAVI    PIC X(3).                                  00000500
           02 MWTAUXSAD OCCURS   20 TIMES .                             00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWTAUXSAL    COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MWTAUXSAL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWTAUXSAF    PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MWTAUXSAI    PIC X(3).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(78).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: ESV01   ESV01                                              00000770
      ***************************************************************** 00000780
       01   ESV01O REDEFINES ESV01I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MPAGEA    PIC X.                                          00000960
           02 MPAGEC    PIC X.                                          00000970
           02 MPAGEP    PIC X.                                          00000980
           02 MPAGEH    PIC X.                                          00000990
           02 MPAGEV    PIC X.                                          00001000
           02 MPAGEO    PIC Z9.                                         00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPAGETA   PIC X.                                          00001030
           02 MPAGETC   PIC X.                                          00001040
           02 MPAGETP   PIC X.                                          00001050
           02 MPAGETH   PIC X.                                          00001060
           02 MPAGETV   PIC X.                                          00001070
           02 MPAGETO   PIC Z9.                                         00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MNSOCA    PIC X.                                          00001100
           02 MNSOCC    PIC X.                                          00001110
           02 MNSOCP    PIC X.                                          00001120
           02 MNSOCH    PIC X.                                          00001130
           02 MNSOCV    PIC X.                                          00001140
           02 MNSOCO    PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNMAGA    PIC X.                                          00001170
           02 MNMAGC    PIC X.                                          00001180
           02 MNMAGP    PIC X.                                          00001190
           02 MNMAGH    PIC X.                                          00001200
           02 MNMAGV    PIC X.                                          00001210
           02 MNMAGO    PIC X(3).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLLIEUA   PIC X.                                          00001240
           02 MLLIEUC   PIC X.                                          00001250
           02 MLLIEUP   PIC X.                                          00001260
           02 MLLIEUH   PIC X.                                          00001270
           02 MLLIEUV   PIC X.                                          00001280
           02 MLLIEUO   PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCTPSAVA  PIC X.                                          00001310
           02 MCTPSAVC  PIC X.                                          00001320
           02 MCTPSAVP  PIC X.                                          00001330
           02 MCTPSAVH  PIC X.                                          00001340
           02 MCTPSAVV  PIC X.                                          00001350
           02 MCTPSAVO  PIC X(5).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MLTPSAVA  PIC X.                                          00001380
           02 MLTPSAVC  PIC X.                                          00001390
           02 MLTPSAVP  PIC X.                                          00001400
           02 MLTPSAVH  PIC X.                                          00001410
           02 MLTPSAVV  PIC X.                                          00001420
           02 MLTPSAVO  PIC X(20).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MDEFFETA  PIC X.                                          00001450
           02 MDEFFETC  PIC X.                                          00001460
           02 MDEFFETP  PIC X.                                          00001470
           02 MDEFFETH  PIC X.                                          00001480
           02 MDEFFETV  PIC X.                                          00001490
           02 MDEFFETO  PIC X(10).                                      00001500
           02 DFHMS1 OCCURS   20 TIMES .                                00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MNSOCSAVA    PIC X.                                     00001530
             03 MNSOCSAVC    PIC X.                                     00001540
             03 MNSOCSAVP    PIC X.                                     00001550
             03 MNSOCSAVH    PIC X.                                     00001560
             03 MNSOCSAVV    PIC X.                                     00001570
             03 MNSOCSAVO    PIC X(3).                                  00001580
           02 DFHMS2 OCCURS   20 TIMES .                                00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MWTAUXSAA    PIC X.                                     00001610
             03 MWTAUXSAC    PIC X.                                     00001620
             03 MWTAUXSAP    PIC X.                                     00001630
             03 MWTAUXSAH    PIC X.                                     00001640
             03 MWTAUXSAV    PIC X.                                     00001650
             03 MWTAUXSAO    PIC ZZZ.                                   00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBERRA  PIC X.                                          00001680
           02 MLIBERRC  PIC X.                                          00001690
           02 MLIBERRP  PIC X.                                          00001700
           02 MLIBERRH  PIC X.                                          00001710
           02 MLIBERRV  PIC X.                                          00001720
           02 MLIBERRO  PIC X(78).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCODTRAA  PIC X.                                          00001750
           02 MCODTRAC  PIC X.                                          00001760
           02 MCODTRAP  PIC X.                                          00001770
           02 MCODTRAH  PIC X.                                          00001780
           02 MCODTRAV  PIC X.                                          00001790
           02 MCODTRAO  PIC X(4).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCICSA    PIC X.                                          00001820
           02 MCICSC    PIC X.                                          00001830
           02 MCICSP    PIC X.                                          00001840
           02 MCICSH    PIC X.                                          00001850
           02 MCICSV    PIC X.                                          00001860
           02 MCICSO    PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSCREENA  PIC X.                                          00001960
           02 MSCREENC  PIC X.                                          00001970
           02 MSCREENP  PIC X.                                          00001980
           02 MSCREENH  PIC X.                                          00001990
           02 MSCREENV  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
