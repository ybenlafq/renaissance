      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC008 CORR MOD PAIMNT DARTY.COM->NCG   *        
      *----------------------------------------------------------------*        
       01  RVEC008.                                                             
           05  EC008-CTABLEG2    PIC X(15).                                     
           05  EC008-CTABLEG2-REDEF REDEFINES EC008-CTABLEG2.                   
               10  EC008-MOPAIDC         PIC X(05).                             
           05  EC008-WTABLEG     PIC X(80).                                     
           05  EC008-WTABLEG-REDEF  REDEFINES EC008-WTABLEG.                    
               10  EC008-MOPAISI         PIC X(05).                             
               10  EC008-WENLIGNE        PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC008-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC008-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC008-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC008-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC008-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
