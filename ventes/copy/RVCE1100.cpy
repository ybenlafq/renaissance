      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVCE1100                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVCE1100                 00000060
      *   CLE UNIQUE : NSOCIETE � CFORMAT                               00000070
      *---------------------------------------------------------        00000080
      *                                                                 00000090
       01  RVCE1100.                                                    00000100
           10 CE11-NSOCIETE        PIC X(3).                            00000110
           10 CE11-NLIEU           PIC X(3).                            00000120
           10 CE11-DEFFET          PIC X(8).                            00000130
           10 CE11-NCODIC          PIC X(7).                            00000140
           10 CE11-CFORMAT         PIC X(5).                            00000150
           10 CE11-WETIQUETTE      PIC X(1).                            00000160
           10 CE11-DSYST           PIC S9(13) COMP-3.                   00000170
      *                                                                 00000180
      *---------------------------------------------------------        00000190
      *   LISTE DES FLAGS DE LA TABLE RVCE1100                          00000200
      *---------------------------------------------------------        00000210
      *                                                                 00000220
       01  RVCE1100-FLAGS.                                              00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-NSOCIETE-F      PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 CE11-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-NLIEU-F         PIC S9(4) COMP.                      00000250
      *--                                                                       
           10 CE11-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-DEFFET-F        PIC S9(4) COMP.                      00000260
      *--                                                                       
           10 CE11-DEFFET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-NCODIC-F        PIC S9(4) COMP.                      00000270
      *--                                                                       
           10 CE11-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-CFORMAT-F       PIC S9(4) COMP.                      00000280
      *--                                                                       
           10 CE11-CFORMAT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-WETIQUETTE-F    PIC S9(4) COMP.                      00000290
      *--                                                                       
           10 CE11-WETIQUETTE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE11-DSYST-F         PIC S9(4) COMP.                      00000300
      *                                                                         
      *--                                                                       
           10 CE11-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
