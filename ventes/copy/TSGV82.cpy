      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS SPECIFIQUE DU PROGRAMME TGV82                         * 00000030
      *      1 ITEM PAR PAGE ET PAR MAGASIN                           * 00000050
      *       5 BONS D'ENLEVEMENTS PAR ITEM                           * 00000050
      *         2 LIGNES PAR BONS                                     * 00000051
      *            --->  10 LIGNES PAR TS                             * 00000052
      *                                                               * 00000052
      *      TR : GV80  GESTION DES BE                                * 00000060
      *      PG : TGV82 SELECTION DES BE A ENLEVER POUR UN MAGASIN    * 00000070
      *                                                               * 00000080
      *      NOM: 'GV82' + EIBTRMID                                   * 00000090
      *      LG : 775 C                                               * 00000091
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV82.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
           05 TS-GV82-LONG               PIC S9(5) COMP-3 VALUE +775.   00000097
      ********************************** DONNEES                        00000098
           05 TS-GV82-DONNEES.                                          00000099
              10 TS-GV82-NMAGASIN      PIC  X(3).                       00000100
              10 TS-GV82-NBLIGNES      PIC  9(2).                       00000100
              10 TS-GV82-LIGNE           OCCURS 10.                     00000100
                 15 TS-GV82-BONS       PIC  X(76) .                             
                 15 TS-GV82-PLIGNE     PIC  X.                                  
                                                                                
