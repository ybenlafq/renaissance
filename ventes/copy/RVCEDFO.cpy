      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CEDFO ASSOCIATION FORMAT OBJETS        *        
      *----------------------------------------------------------------*        
       01  RVCEDFO.                                                             
           05  CEDFO-CTABLEG2    PIC X(15).                                     
           05  CEDFO-CTABLEG2-REDEF REDEFINES CEDFO-CTABLEG2.                   
               10  CEDFO-CFORMAT         PIC X(05).                             
               10  CEDFO-TOBJET          PIC X(01).                             
               10  CEDFO-COBJET          PIC X(05).                             
           05  CEDFO-WTABLEG     PIC X(80).                                     
           05  CEDFO-WTABLEG-REDEF  REDEFINES CEDFO-WTABLEG.                    
               10  CEDFO-WACTIF          PIC X(01).                             
               10  CEDFO-NSEQ            PIC X(03).                             
               10  CEDFO-NSEQ-N         REDEFINES CEDFO-NSEQ                    
                                         PIC 9(03).                             
               10  CEDFO-WDATA           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCEDFO-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEDFO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CEDFO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEDFO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CEDFO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
