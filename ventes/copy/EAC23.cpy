      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Saisie des entr�es au 1/4 heure                                 00000020
      ***************************************************************** 00000030
       01   EAC23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MJOURI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MDATEI    PIC X(10).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNSOCI    PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MNLIEUI   PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLLIEUI   PIC X(20).                                      00000350
           02 MLIGNEI OCCURS   14 TIMES .                               00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENTJOUL    COMP PIC S9(4).                            00000370
      *--                                                                       
             03 MQENTJOUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQENTJOUF    PIC X.                                     00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MQENTJOUI    PIC X(6).                                  00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQBOUCHON1L  COMP PIC S9(4).                            00000410
      *--                                                                       
             03 MQBOUCHON1L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQBOUCHON1F  PIC X.                                     00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MQBOUCHON1I  PIC X(2).                                  00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWMODIF1L    COMP PIC S9(4).                            00000450
      *--                                                                       
             03 MWMODIF1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWMODIF1F    PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MWMODIF1I    PIC X.                                     00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENT1L      COMP PIC S9(4).                            00000490
      *--                                                                       
             03 MQENT1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQENT1F      PIC X.                                     00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MQENT1I      PIC X(3).                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQBOUCHON2L  COMP PIC S9(4).                            00000530
      *--                                                                       
             03 MQBOUCHON2L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQBOUCHON2F  PIC X.                                     00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MQBOUCHON2I  PIC X(2).                                  00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWMODIF2L    COMP PIC S9(4).                            00000570
      *--                                                                       
             03 MWMODIF2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWMODIF2F    PIC X.                                     00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MWMODIF2I    PIC X.                                     00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENT2L      COMP PIC S9(4).                            00000610
      *--                                                                       
             03 MQENT2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQENT2F      PIC X.                                     00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MQENT2I      PIC X(3).                                  00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQBOUCHON3L  COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MQBOUCHON3L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQBOUCHON3F  PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MQBOUCHON3I  PIC X(2).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWMODIF3L    COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MWMODIF3L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWMODIF3F    PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MWMODIF3I    PIC X.                                     00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENT3L      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MQENT3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQENT3F      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MQENT3I      PIC X(3).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQBOUCHON4L  COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MQBOUCHON4L COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQBOUCHON4F  PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MQBOUCHON4I  PIC X(2).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWMODIF4L    COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MWMODIF4L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWMODIF4F    PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MWMODIF4I    PIC X.                                     00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENT4L      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 MQENT4L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MQENT4F      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 MQENT4I      PIC X(3).                                  00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQENTTOTL      COMP PIC S9(4).                            00000890
      *--                                                                       
           02 MQENTTOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQENTTOTF      PIC X.                                     00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MQENTTOTI      PIC X(6).                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQENTESTIML    COMP PIC S9(4).                            00000930
      *--                                                                       
           02 MQENTESTIML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MQENTESTIMF    PIC X.                                     00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MQENTESTIMI    PIC X(6).                                  00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBQUARTL      COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MNBQUARTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBQUARTF      PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNBQUARTI      PIC X(2).                                  00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQENTREPL      COMP PIC S9(4).                            00001010
      *--                                                                       
           02 MQENTREPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQENTREPF      PIC X.                                     00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MQENTREPI      PIC X(3).                                  00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MLIBERRI  PIC X(79).                                      00001080
      * CODE TRANSACTION                                                00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MZONCMDI  PIC X(15).                                      00001170
      * NOM DU CICS                                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      * NETNAME                                                         00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MNETNAMI  PIC X(8).                                       00001270
      * CODE TERMINAL                                                   00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MSCREENI  PIC X(4).                                       00001320
      ***************************************************************** 00001330
      * Saisie des entr�es au 1/4 heure                                 00001340
      ***************************************************************** 00001350
       01   EAC23O REDEFINES EAC23I.                                    00001360
           02 FILLER    PIC X(12).                                      00001370
      * DATE DU JOUR                                                    00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MDATJOUA  PIC X.                                          00001400
           02 MDATJOUC  PIC X.                                          00001410
           02 MDATJOUP  PIC X.                                          00001420
           02 MDATJOUH  PIC X.                                          00001430
           02 MDATJOUV  PIC X.                                          00001440
           02 MDATJOUO  PIC X(10).                                      00001450
      * HEURE                                                           00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTIMJOUA  PIC X.                                          00001480
           02 MTIMJOUC  PIC X.                                          00001490
           02 MTIMJOUP  PIC X.                                          00001500
           02 MTIMJOUH  PIC X.                                          00001510
           02 MTIMJOUV  PIC X.                                          00001520
           02 MTIMJOUO  PIC X(5).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MJOURA    PIC X.                                          00001550
           02 MJOURC    PIC X.                                          00001560
           02 MJOURP    PIC X.                                          00001570
           02 MJOURH    PIC X.                                          00001580
           02 MJOURV    PIC X.                                          00001590
           02 MJOURO    PIC X(3).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MDATEA    PIC X.                                          00001620
           02 MDATEC    PIC X.                                          00001630
           02 MDATEP    PIC X.                                          00001640
           02 MDATEH    PIC X.                                          00001650
           02 MDATEV    PIC X.                                          00001660
           02 MDATEO    PIC X(10).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MNSOCA    PIC X.                                          00001690
           02 MNSOCC    PIC X.                                          00001700
           02 MNSOCP    PIC X.                                          00001710
           02 MNSOCH    PIC X.                                          00001720
           02 MNSOCV    PIC X.                                          00001730
           02 MNSOCO    PIC X(3).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNLIEUA   PIC X.                                          00001760
           02 MNLIEUC   PIC X.                                          00001770
           02 MNLIEUP   PIC X.                                          00001780
           02 MNLIEUH   PIC X.                                          00001790
           02 MNLIEUV   PIC X.                                          00001800
           02 MNLIEUO   PIC X(3).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MLLIEUA   PIC X.                                          00001830
           02 MLLIEUC   PIC X.                                          00001840
           02 MLLIEUP   PIC X.                                          00001850
           02 MLLIEUH   PIC X.                                          00001860
           02 MLLIEUV   PIC X.                                          00001870
           02 MLLIEUO   PIC X(20).                                      00001880
           02 MLIGNEO OCCURS   14 TIMES .                               00001890
             03 FILLER       PIC X(2).                                  00001900
             03 MQENTJOUA    PIC X.                                     00001910
             03 MQENTJOUC    PIC X.                                     00001920
             03 MQENTJOUP    PIC X.                                     00001930
             03 MQENTJOUH    PIC X.                                     00001940
             03 MQENTJOUV    PIC X.                                     00001950
             03 MQENTJOUO    PIC -----9.                                00001960
             03 FILLER       PIC X(2).                                  00001970
             03 MQBOUCHON1A  PIC X.                                     00001980
             03 MQBOUCHON1C  PIC X.                                     00001990
             03 MQBOUCHON1P  PIC X.                                     00002000
             03 MQBOUCHON1H  PIC X.                                     00002010
             03 MQBOUCHON1V  PIC X.                                     00002020
             03 MQBOUCHON1O  PIC X(2).                                  00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MWMODIF1A    PIC X.                                     00002050
             03 MWMODIF1C    PIC X.                                     00002060
             03 MWMODIF1P    PIC X.                                     00002070
             03 MWMODIF1H    PIC X.                                     00002080
             03 MWMODIF1V    PIC X.                                     00002090
             03 MWMODIF1O    PIC X.                                     00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MQENT1A      PIC X.                                     00002120
             03 MQENT1C PIC X.                                          00002130
             03 MQENT1P PIC X.                                          00002140
             03 MQENT1H PIC X.                                          00002150
             03 MQENT1V PIC X.                                          00002160
             03 MQENT1O      PIC X(3).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MQBOUCHON2A  PIC X.                                     00002190
             03 MQBOUCHON2C  PIC X.                                     00002200
             03 MQBOUCHON2P  PIC X.                                     00002210
             03 MQBOUCHON2H  PIC X.                                     00002220
             03 MQBOUCHON2V  PIC X.                                     00002230
             03 MQBOUCHON2O  PIC X(2).                                  00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MWMODIF2A    PIC X.                                     00002260
             03 MWMODIF2C    PIC X.                                     00002270
             03 MWMODIF2P    PIC X.                                     00002280
             03 MWMODIF2H    PIC X.                                     00002290
             03 MWMODIF2V    PIC X.                                     00002300
             03 MWMODIF2O    PIC X.                                     00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MQENT2A      PIC X.                                     00002330
             03 MQENT2C PIC X.                                          00002340
             03 MQENT2P PIC X.                                          00002350
             03 MQENT2H PIC X.                                          00002360
             03 MQENT2V PIC X.                                          00002370
             03 MQENT2O      PIC X(3).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MQBOUCHON3A  PIC X.                                     00002400
             03 MQBOUCHON3C  PIC X.                                     00002410
             03 MQBOUCHON3P  PIC X.                                     00002420
             03 MQBOUCHON3H  PIC X.                                     00002430
             03 MQBOUCHON3V  PIC X.                                     00002440
             03 MQBOUCHON3O  PIC X(2).                                  00002450
             03 FILLER       PIC X(2).                                  00002460
             03 MWMODIF3A    PIC X.                                     00002470
             03 MWMODIF3C    PIC X.                                     00002480
             03 MWMODIF3P    PIC X.                                     00002490
             03 MWMODIF3H    PIC X.                                     00002500
             03 MWMODIF3V    PIC X.                                     00002510
             03 MWMODIF3O    PIC X.                                     00002520
             03 FILLER       PIC X(2).                                  00002530
             03 MQENT3A      PIC X.                                     00002540
             03 MQENT3C PIC X.                                          00002550
             03 MQENT3P PIC X.                                          00002560
             03 MQENT3H PIC X.                                          00002570
             03 MQENT3V PIC X.                                          00002580
             03 MQENT3O      PIC X(3).                                  00002590
             03 FILLER       PIC X(2).                                  00002600
             03 MQBOUCHON4A  PIC X.                                     00002610
             03 MQBOUCHON4C  PIC X.                                     00002620
             03 MQBOUCHON4P  PIC X.                                     00002630
             03 MQBOUCHON4H  PIC X.                                     00002640
             03 MQBOUCHON4V  PIC X.                                     00002650
             03 MQBOUCHON4O  PIC X(2).                                  00002660
             03 FILLER       PIC X(2).                                  00002670
             03 MWMODIF4A    PIC X.                                     00002680
             03 MWMODIF4C    PIC X.                                     00002690
             03 MWMODIF4P    PIC X.                                     00002700
             03 MWMODIF4H    PIC X.                                     00002710
             03 MWMODIF4V    PIC X.                                     00002720
             03 MWMODIF4O    PIC X.                                     00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MQENT4A      PIC X.                                     00002750
             03 MQENT4C PIC X.                                          00002760
             03 MQENT4P PIC X.                                          00002770
             03 MQENT4H PIC X.                                          00002780
             03 MQENT4V PIC X.                                          00002790
             03 MQENT4O      PIC X(3).                                  00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MQENTTOTA      PIC X.                                     00002820
           02 MQENTTOTC PIC X.                                          00002830
           02 MQENTTOTP PIC X.                                          00002840
           02 MQENTTOTH PIC X.                                          00002850
           02 MQENTTOTV PIC X.                                          00002860
           02 MQENTTOTO      PIC -----9.                                00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MQENTESTIMA    PIC X.                                     00002890
           02 MQENTESTIMC    PIC X.                                     00002900
           02 MQENTESTIMP    PIC X.                                     00002910
           02 MQENTESTIMH    PIC X.                                     00002920
           02 MQENTESTIMV    PIC X.                                     00002930
           02 MQENTESTIMO    PIC X(6).                                  00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MNBQUARTA      PIC X.                                     00002960
           02 MNBQUARTC PIC X.                                          00002970
           02 MNBQUARTP PIC X.                                          00002980
           02 MNBQUARTH PIC X.                                          00002990
           02 MNBQUARTV PIC X.                                          00003000
           02 MNBQUARTO      PIC X(2).                                  00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MQENTREPA      PIC X.                                     00003030
           02 MQENTREPC PIC X.                                          00003040
           02 MQENTREPP PIC X.                                          00003050
           02 MQENTREPH PIC X.                                          00003060
           02 MQENTREPV PIC X.                                          00003070
           02 MQENTREPO      PIC X(3).                                  00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MLIBERRA  PIC X.                                          00003100
           02 MLIBERRC  PIC X.                                          00003110
           02 MLIBERRP  PIC X.                                          00003120
           02 MLIBERRH  PIC X.                                          00003130
           02 MLIBERRV  PIC X.                                          00003140
           02 MLIBERRO  PIC X(79).                                      00003150
      * CODE TRANSACTION                                                00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MCODTRAA  PIC X.                                          00003180
           02 MCODTRAC  PIC X.                                          00003190
           02 MCODTRAP  PIC X.                                          00003200
           02 MCODTRAH  PIC X.                                          00003210
           02 MCODTRAV  PIC X.                                          00003220
           02 MCODTRAO  PIC X(4).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MZONCMDA  PIC X.                                          00003250
           02 MZONCMDC  PIC X.                                          00003260
           02 MZONCMDP  PIC X.                                          00003270
           02 MZONCMDH  PIC X.                                          00003280
           02 MZONCMDV  PIC X.                                          00003290
           02 MZONCMDO  PIC X(15).                                      00003300
      * NOM DU CICS                                                     00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MCICSA    PIC X.                                          00003330
           02 MCICSC    PIC X.                                          00003340
           02 MCICSP    PIC X.                                          00003350
           02 MCICSH    PIC X.                                          00003360
           02 MCICSV    PIC X.                                          00003370
           02 MCICSO    PIC X(5).                                       00003380
      * NETNAME                                                         00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MNETNAMA  PIC X.                                          00003410
           02 MNETNAMC  PIC X.                                          00003420
           02 MNETNAMP  PIC X.                                          00003430
           02 MNETNAMH  PIC X.                                          00003440
           02 MNETNAMV  PIC X.                                          00003450
           02 MNETNAMO  PIC X(8).                                       00003460
      * CODE TERMINAL                                                   00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MSCREENA  PIC X.                                          00003490
           02 MSCREENC  PIC X.                                          00003500
           02 MSCREENP  PIC X.                                          00003510
           02 MSCREENH  PIC X.                                          00003520
           02 MSCREENV  PIC X.                                          00003530
           02 MSCREENO  PIC X(4).                                       00003540
                                                                                
