      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * log ventes darty.com                                            00000020
      ***************************************************************** 00000030
       01   EEC00O.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
           02 FILLER    PIC X(2).                                       00000060
           02 MDATJOUA  PIC X.                                          00000070
           02 MDATJOUC  PIC X.                                          00000080
           02 MDATJOUP  PIC X.                                          00000090
           02 MDATJOUH  PIC X.                                          00000100
           02 MDATJOUV  PIC X.                                          00000110
           02 MDATJOUO  PIC X(10).                                      00000120
           02 MLIGNEO OCCURS   23 TIMES .                               00000130
             03 FILLER       PIC X(2).                                  00000140
             03 MHMSA   PIC X.                                          00000150
             03 MHMSC   PIC X.                                          00000160
             03 MHMSP   PIC X.                                          00000170
             03 MHMSH   PIC X.                                          00000180
             03 MHMSV   PIC X.                                          00000190
             03 MHMSO   PIC X(8).                                       00000200
             03 FILLER       PIC X(2).                                  00000210
             03 MTEXTA  PIC X.                                          00000220
             03 MTEXTC  PIC X.                                          00000230
             03 MTEXTP  PIC X.                                          00000240
             03 MTEXTH  PIC X.                                          00000250
             03 MTEXTV  PIC X.                                          00000260
             03 MTEXTO  PIC X(69).                                      00000270
                                                                                
