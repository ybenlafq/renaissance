      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5500                         
      *   CLE NSOCIETE A NTRANS + NSEQREGL  LG = 73                             
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5500.                                                            
      *}                                                                        
1          02  EM55-NSOCIETE                                                    
               PIC X(0003).                                                     
4          02  EM55-NLIEU                                                       
               PIC X(0003).                                                     
7          02  EM55-DCAISSE                                                     
               PIC X(0008).                                                     
15         02  EM55-NCAISSE                                                     
               PIC X(0003).                                                     
18         02  EM55-NTRANS                                                      
               PIC X(0008).                                                     
26         02  EM55-NSEQREGL                                                    
               PIC X(0003).                                                     
29         02  EM55-DHVENTE                                                     
               PIC X(0002).                                                     
31         02  EM55-DMVENTE                                                     
               PIC X(0002).                                                     
33         02  EM55-NTYPEREGL                                                   
               PIC X(0003).                                                     
36         02  EM55-NOPERATEUR                                                  
               PIC X(0007).                                                     
43         02  EM55-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
48         02  EM55-PMONTATTN                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
53         02  EM55-CMODREG                                                     
               PIC X(0005).                                                     
58         02  EM55-LMODREG                                                     
               PIC X(0015).                                                     
73         02  EM55-DEVCUMUL                                                    
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEM5500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NSEQREGL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NSEQREGL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NTYPEREGL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NTYPEREGL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-PMONTATTN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-PMONTATTN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-CMODREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-CMODREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-LMODREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-LMODREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM55-DEVCUMUL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM55-DEVCUMUL-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
