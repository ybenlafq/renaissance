      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPM0600                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPM0600                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPM0600.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPM0600.                                                            
      *}                                                                        
           02  PM06-CMOPAI                                                      
               PIC X(0005).                                                     
           02  PM06-WTPRGT                                                      
               PIC X(0001).                                                     
           02  PM06-CMPANC                                                      
               PIC X(0001).                                                     
           02  PM06-LMDPAI                                                      
               PIC X(0015).                                                     
           02  PM06-WINFCP                                                      
               PIC X(0001).                                                     
           02  PM06-WTCTRL                                                      
               PIC X(0002).                                                     
           02  PM06-WSEQED                                                      
               PIC X(0002).                                                     
           02  PM06-LMDPRB                                                      
               PIC X(0015).                                                     
           02  PM06-WDCMPT                                                      
               PIC X(0001).                                                     
           02  PM06-WMCTRL                                                      
               PIC X(0010).                                                     
           02  PM06-WCTFDC                                                      
               PIC X(0001).                                                     
           02  PM06-WRCOFF                                                      
               PIC X(0001).                                                     
           02  PM06-CMOPAC                                                      
               PIC X(0005).                                                     
           02  PM06-LVTIPA                                                      
               PIC X(0010).                                                     
           02  PM06-LRTIPA                                                      
               PIC X(0010).                                                     
           02  PM06-LUTIPA                                                      
               PIC X(0010).                                                     
           02  PM06-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPM0600                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPM0600-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPM0600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-CMOPAI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-CMOPAI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WTPRGT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WTPRGT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-CMPANC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-CMPANC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-LMDPAI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-LMDPAI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WINFCP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WINFCP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WTCTRL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WTCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-LMDPRB-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-LMDPRB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WDCMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WDCMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WMCTRL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WMCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WCTFDC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WCTFDC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-WRCOFF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-WRCOFF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-CMOPAC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-CMOPAC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-LVTIPA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-LVTIPA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-LRTIPA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-LRTIPA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-LUTIPA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-LUTIPA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM06-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
