      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TRANSACTION: GV00                                              *        
      * TITRE      : COMMAREA D'APPEL MEC68, MODULE INTERROGATION      *        
      *              DES STOCKS          DACEM POUR L'EXPEDIION (EMX)  *        
      * LONGUEUR   : ????                                              *        
      *                                                                *        
      * -------------------------------------------------------------- *        
        01 COMM-EC68-APPLI.                                                     
           05 COMM-EC68-ZIN.                                                    
               10 COMM-EC68-CAPPEL           PIC X(01).                         
      *{ remove-comma-in-dde 1.5                                                
      *           88 COMM-EC68-CONTROLE      VALUE 'D', 'M', '4', 'C'.          
      *--                                                                       
                  88 COMM-EC68-CONTROLE      VALUE 'D'  'M'  '4'  'C'.          
      *}                                                                        
                  88 COMM-EC68-CTRL-MGV64    VALUE '4'.                         
                  88 COMM-EC68-NEW-APPEL     VALUE ' '.                         
               10 COMM-EC68-NEW-MODE.                                           
                  15 COMM-EC68-ACT-DACEM      PIC X(01).                        
                     88 COMM-EC68-CREATION-DACEM   VALUE 'C'.                   
                     88 COMM-EC68-MODIF-DACEM      VALUE 'M'.                   
                     88 COMM-EC68-SUP-DACEM        VALUE 'S'.                   
                  15 COMM-EC68-INTERNET       PIC X(01).                        
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-EC68-SUFFIX           PIC X(01).                         
               10 COMM-EC68-NSOCIETE         PIC X(03).                         
               10 COMM-EC68-NLIEU            PIC X(03).                         
               10 COMM-EC68-MDLIV-DEPOT.                                        
                  15 COMM-EC68-MDLIV-NSOC       PIC X(03).                      
                  15 COMM-EC68-MDLIV-NLIEU      PIC X(03).                      
               10 COMM-EC68-NVENTE           PIC X(07).                         
               10 COMM-EC68-NCODIC           PIC X(07).                         
               10 COMM-EC68-NSEQNQ           PIC S9(5) COMP-3.                  
               10 COMM-EC68-DDELIV           PIC X(08).                         
               10 COMM-EC68-SSAAMMJJ         PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-EC68-QTE              PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-EC68-QTE              PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
      * -> PARAM�TRES SP�CIAUX                                                  
               10 COMM-EC68-TYPMUT          PIC X(01).                          
                  88 COMM-EC68-MUT-UNIQUE               VALUE 'U'.              
                  88 COMM-EC68-MUT-MULTI                VALUE 'M'.              
               10 COMM-EC68-WEMPORTE        PIC X(01).                          
               10 COMM-EC68-RESA-A-J        PIC X(01).                          
               10 COMM-EC68-NORDRE          PIC X(05).                          
               10 COMM-EC68-DMUT            PIC X(08).                          
               10 COMM-EC68-FILIERE-IN      PIC X(02).                          
               10 COMM-EC68-SUP-FILIERE     PIC X(02).                          
               10 COMM-EC68-MUT-SL300       PIC X(01).                          
               10 FILLER                    PIC X(31).                          
      * ->                                                                      
           05 COMM-EC68-ZOUT.                                                   
               10 COMM-EC68-CODRET           PIC X(04).                         
               10 COMM-EC68-LMESSAGE         PIC X(53).                         
               10 COMM-EC68-NMUTATION        PIC X(07).                         
               10 COMM-EC68-NMUTTEMP         PIC X(07).                         
               10 COMM-EC68-ETAT-RESERVATION PIC 9(01).                         
                  88 COMM-EC68-AUCUN-STOCK-DISPO       VALUE 0.                 
                  88 COMM-EC68-STOCK-DACEM             VALUE 1.                 
               10 COMM-EC68-DATE-DISPO       PIC X(08).                         
               10 COMM-EC68-PRECO            PIC 9(01).                         
                  88 EC68-PAS-COMMANDE-PRECO      VALUE 0.                      
                  88 EC68-COMMANDE-PRECO          VALUE 1.                      
               10 COMM-EC68-FILIERE          PIC X(04).                         
               10 FILLER                     PIC X(092).                        
                                                                                
