      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC010 MUTS NON PRISES EN COMPTE DCOM   *        
      *----------------------------------------------------------------*        
       01  RVEC010.                                                             
           05  EC010-CTABLEG2    PIC X(15).                                     
           05  EC010-CTABLEG2-REDEF REDEFINES EC010-CTABLEG2.                   
               10  EC010-NSOCENTR        PIC X(03).                             
               10  EC010-NDEPOT          PIC X(03).                             
               10  EC010-NSOCIETE        PIC X(03).                             
               10  EC010-NLIEU           PIC X(03).                             
           05  EC010-WTABLEG     PIC X(80).                                     
           05  EC010-WTABLEG-REDEF  REDEFINES EC010-WTABLEG.                    
               10  EC010-CSELART         PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC010-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC010-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC010-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC010-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC010-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
