      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:49 >
      
      *****************************************************************
      * SDF: EIT10   EIT10
      *****************************************************************
       01   EIT10I.
           02 FILLER    PIC X(12).
      * DATE DU JOUR
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MDATJOUL  COMP PIC S9(4).
      *--
           02 MDATJOUL COMP-5 PIC S9(4).
      *}
           02 MDATJOUF  PIC X.
           02 FILLER    PIC X(4).
           02 MDATJOUI  PIC X(10).
      * HEURE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MTIMJOUL  COMP PIC S9(4).
      *--
           02 MTIMJOUL COMP-5 PIC S9(4).
      *}
           02 MTIMJOUF  PIC X.
           02 FILLER    PIC X(4).
           02 MTIMJOUI  PIC X(8).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MNINVENTAIREL  COMP PIC S9(4).
      *--
           02 MNINVENTAIREL COMP-5 PIC S9(4).
      *}
           02 MNINVENTAIREF  PIC X.
           02 FILLER    PIC X(4).
           02 MNINVENTAIREI  PIC X(5).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MNSOCL    COMP PIC S9(4).
      *--
           02 MNSOCL COMP-5 PIC S9(4).
      *}
           02 MNSOCF    PIC X.
           02 FILLER    PIC X(4).
           02 MNSOCI    PIC X(3).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MLSOCL    COMP PIC S9(4).
      *--
           02 MLSOCL COMP-5 PIC S9(4).
      *}
           02 MLSOCF    PIC X.
           02 FILLER    PIC X(4).
           02 MLSOCI    PIC X(20).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MNLIEUL   COMP PIC S9(4).
      *--
           02 MNLIEUL COMP-5 PIC S9(4).
      *}
           02 MNLIEUF   PIC X.
           02 FILLER    PIC X(4).
           02 MNLIEUI   PIC X(3).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MLLIEUL   COMP PIC S9(4).
      *--
           02 MLLIEUL COMP-5 PIC S9(4).
      *}
           02 MLLIEUF   PIC X.
           02 FILLER    PIC X(4).
           02 MLLIEUI   PIC X(20).
           02 M47I OCCURS   14 TIMES .
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 CODICL  COMP PIC S9(4).
      *--
             03 CODICL COMP-5 PIC S9(4).
      *}
             03 CODICF  PIC X.
             03 FILLER  PIC X(4).
             03 CODICI  PIC X(7).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 QTEL    COMP PIC S9(4).
      *--
             03 QTEL COMP-5 PIC S9(4).
      *}
             03 QTEF    PIC X.
             03 FILLER  PIC X(4).
             03 QTEI    PIC X(5).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 FAML    COMP PIC S9(4).
      *--
             03 FAML COMP-5 PIC S9(4).
      *}
             03 FAMF    PIC X.
             03 FILLER  PIC X(4).
             03 FAMI    PIC X(5).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 MARL    COMP PIC S9(4).
      *--
             03 MARL COMP-5 PIC S9(4).
      *}
             03 MARF    PIC X.
             03 FILLER  PIC X(4).
             03 MARI    PIC X(5).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 REFL    COMP PIC S9(4).
      *--
             03 REFL COMP-5 PIC S9(4).
      *}
             03 REFF    PIC X.
             03 FILLER  PIC X(4).
             03 REFI    PIC X(20).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *      03 MSGL    COMP PIC S9(4).
      *--
             03 MSGL COMP-5 PIC S9(4).
      *}
             03 MSGF    PIC X.
             03 FILLER  PIC X(4).
             03 MSGI    PIC X(24).
      * MESSAGE ERREUR
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MLIBERRL  COMP PIC S9(4).
      *--
           02 MLIBERRL COMP-5 PIC S9(4).
      *}
           02 MLIBERRF  PIC X.
           02 FILLER    PIC X(4).
           02 MLIBERRI  PIC X(78).
      * CODE TRANSACTION
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MCODTRAL  COMP PIC S9(4).
      *--
           02 MCODTRAL COMP-5 PIC S9(4).
      *}
           02 MCODTRAF  PIC X.
           02 FILLER    PIC X(4).
           02 MCODTRAI  PIC X(4).
      * CICS DE TRAVAIL
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MCICSL    COMP PIC S9(4).
      *--
           02 MCICSL COMP-5 PIC S9(4).
      *}
           02 MCICSF    PIC X.
           02 FILLER    PIC X(4).
           02 MCICSI    PIC X(5).
      * NETNAME
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MNETNAML  COMP PIC S9(4).
      *--
           02 MNETNAML COMP-5 PIC S9(4).
      *}
           02 MNETNAMF  PIC X.
           02 FILLER    PIC X(4).
           02 MNETNAMI  PIC X(8).
      * CODE TERMINAL
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MSCREENL  COMP PIC S9(4).
      *--
           02 MSCREENL COMP-5 PIC S9(4).
      *}
           02 MSCREENF  PIC X.
           02 FILLER    PIC X(4).
           02 MSCREENI  PIC X(5).
      *****************************************************************
      * SDF: EIT10   EIT10
      *****************************************************************
       01   EIT10O REDEFINES EIT10I.
           02 FILLER    PIC X(12).
      * DATE DU JOUR
           02 FILLER    PIC X(2).
           02 MDATJOUA  PIC X.
           02 MDATJOUC  PIC X.
           02 MDATJOUP  PIC X.
           02 MDATJOUH  PIC X.
           02 MDATJOUV  PIC X.
           02 MDATJOUO  PIC X(10).
      * HEURE
           02 FILLER    PIC X(2).
           02 MTIMJOUA  PIC X.
           02 MTIMJOUC  PIC X.
           02 MTIMJOUP  PIC X.
           02 MTIMJOUH  PIC X.
           02 MTIMJOUV  PIC X.
           02 MTIMJOUO  PIC X(8).
           02 FILLER    PIC X(2).
           02 MNINVENTAIREA  PIC X.
           02 MNINVENTAIREC  PIC X.
           02 MNINVENTAIREP  PIC X.
           02 MNINVENTAIREH  PIC X.
           02 MNINVENTAIREV  PIC X.
           02 MNINVENTAIREO  PIC X(5).
           02 FILLER    PIC X(2).
           02 MNSOCA    PIC X.
           02 MNSOCC    PIC X.
           02 MNSOCP    PIC X.
           02 MNSOCH    PIC X.
           02 MNSOCV    PIC X.
           02 MNSOCO    PIC X(3).
           02 FILLER    PIC X(2).
           02 MLSOCA    PIC X.
           02 MLSOCC    PIC X.
           02 MLSOCP    PIC X.
           02 MLSOCH    PIC X.
           02 MLSOCV    PIC X.
           02 MLSOCO    PIC X(20).
           02 FILLER    PIC X(2).
           02 MNLIEUA   PIC X.
           02 MNLIEUC   PIC X.
           02 MNLIEUP   PIC X.
           02 MNLIEUH   PIC X.
           02 MNLIEUV   PIC X.
           02 MNLIEUO   PIC X(3).
           02 FILLER    PIC X(2).
           02 MLLIEUA   PIC X.
           02 MLLIEUC   PIC X.
           02 MLLIEUP   PIC X.
           02 MLLIEUH   PIC X.
           02 MLLIEUV   PIC X.
           02 MLLIEUO   PIC X(20).
           02 M47O OCCURS   14 TIMES .
             03 FILLER       PIC X(2).
             03 CODICA  PIC X.
             03 CODICC  PIC X.
             03 CODICP  PIC X.
             03 CODICH  PIC X.
             03 CODICV  PIC X.
             03 CODICO  PIC X(7).
             03 FILLER       PIC X(2).
             03 QTEA    PIC X.
             03 QTEC    PIC X.
             03 QTEP    PIC X.
             03 QTEH    PIC X.
             03 QTEV    PIC X.
             03 QTEO    PIC ZZZZ9.
             03 FILLER       PIC X(2).
             03 FAMA    PIC X.
             03 FAMC    PIC X.
             03 FAMP    PIC X.
             03 FAMH    PIC X.
             03 FAMV    PIC X.
             03 FAMO    PIC X(5).
             03 FILLER       PIC X(2).
             03 MARA    PIC X.
             03 MARC    PIC X.
             03 MARP    PIC X.
             03 MARH    PIC X.
             03 MARV    PIC X.
             03 MARO    PIC X(5).
             03 FILLER       PIC X(2).
             03 REFA    PIC X.
             03 REFC    PIC X.
             03 REFP    PIC X.
             03 REFH    PIC X.
             03 REFV    PIC X.
             03 REFO    PIC X(20).
             03 FILLER       PIC X(2).
             03 MSGA    PIC X.
             03 MSGC    PIC X.
             03 MSGP    PIC X.
             03 MSGH    PIC X.
             03 MSGV    PIC X.
             03 MSGO    PIC X(24).
      * MESSAGE ERREUR
           02 FILLER    PIC X(2).
           02 MLIBERRA  PIC X.
           02 MLIBERRC  PIC X.
           02 MLIBERRP  PIC X.
           02 MLIBERRH  PIC X.
           02 MLIBERRV  PIC X.
           02 MLIBERRO  PIC X(78).
      * CODE TRANSACTION
           02 FILLER    PIC X(2).
           02 MCODTRAA  PIC X.
           02 MCODTRAC  PIC X.
           02 MCODTRAP  PIC X.
           02 MCODTRAH  PIC X.
           02 MCODTRAV  PIC X.
           02 MCODTRAO  PIC X(4).
      * CICS DE TRAVAIL
           02 FILLER    PIC X(2).
           02 MCICSA    PIC X.
           02 MCICSC    PIC X.
           02 MCICSP    PIC X.
           02 MCICSH    PIC X.
           02 MCICSV    PIC X.
           02 MCICSO    PIC X(5).
      * NETNAME
           02 FILLER    PIC X(2).
           02 MNETNAMA  PIC X.
           02 MNETNAMC  PIC X.
           02 MNETNAMP  PIC X.
           02 MNETNAMH  PIC X.
           02 MNETNAMV  PIC X.
           02 MNETNAMO  PIC X(8).
      * CODE TERMINAL
           02 FILLER    PIC X(2).
           02 MSCREENA  PIC X.
           02 MSCREENC  PIC X.
           02 MSCREENP  PIC X.
           02 MSCREENH  PIC X.
           02 MSCREENV  PIC X.
           02 MSCREENO  PIC X(5).
      
