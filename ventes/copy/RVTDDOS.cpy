      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDDOS TYPE DE DOSSIER POUR TD00        *        
      *----------------------------------------------------------------*        
       01  RVTDDOS.                                                             
           05  TDDOS-CTABLEG2    PIC X(15).                                     
           05  TDDOS-CTABLEG2-REDEF REDEFINES TDDOS-CTABLEG2.                   
               10  TDDOS-TYPDOSS         PIC X(05).                             
           05  TDDOS-WTABLEG     PIC X(80).                                     
           05  TDDOS-WTABLEG-REDEF  REDEFINES TDDOS-WTABLEG.                    
               10  TDDOS-LIBDOSS         PIC X(20).                             
               10  TDDOS-WMTD00          PIC X(01).                             
               10  TDDOS-DMTD00          PIC X(08).                             
               10  TDDOS-DMTD00-N       REDEFINES TDDOS-DMTD00                  
                                         PIC 9(08).                             
               10  TDDOS-WTD00           PIC X(01).                             
               10  TDDOS-DTD00           PIC X(08).                             
               10  TDDOS-DTD00-N        REDEFINES TDDOS-DTD00                   
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDDOS-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDDOS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDDOS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDDOS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDDOS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
