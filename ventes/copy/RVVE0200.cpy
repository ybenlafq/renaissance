      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA VUE RVVE0200                                               
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVVE0200                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE0200.                                                            
      *}                                                                        
           02  VE02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  VE02-NLIEU                                                       
               PIC X(0003).                                                     
           02  VE02-NORDRE                                                      
               PIC X(0005).                                                     
           02  VE02-NVENTE                                                      
               PIC X(0007).                                                     
           02  VE02-WTYPEADR                                                    
               PIC X(0001).                                                     
           02  VE02-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  VE02-LNOM                                                        
               PIC X(0025).                                                     
           02  VE02-LPRENOM                                                     
               PIC X(0015).                                                     
           02  VE02-LBATIMENT                                                   
               PIC X(0003).                                                     
           02  VE02-LESCALIER                                                   
               PIC X(0003).                                                     
           02  VE02-LETAGE                                                      
               PIC X(0003).                                                     
           02  VE02-LPORTE                                                      
               PIC X(0003).                                                     
           02  VE02-LCMPAD1                                                     
               PIC X(0032).                                                     
           02  VE02-LCMPAD2                                                     
               PIC X(0032).                                                     
           02  VE02-CVOIE                                                       
               PIC X(0005).                                                     
           02  VE02-CTVOIE                                                      
               PIC X(0004).                                                     
           02  VE02-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  VE02-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  VE02-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  VE02-LBUREAU                                                     
               PIC X(0026).                                                     
           02  VE02-TELDOM                                                      
               PIC X(0010).                                                     
           02  VE02-TELBUR                                                      
               PIC X(0010).                                                     
           02  VE02-LPOSTEBUR                                                   
               PIC X(0005).                                                     
           02  VE02-LCOMLIV1                                                    
               PIC X(0078).                                                     
           02  VE02-LCOMLIV2                                                    
               PIC X(0078).                                                     
           02  VE02-WETRANGER                                                   
               PIC X(0001).                                                     
           02  VE02-CZONLIV                                                     
               PIC X(0005).                                                     
           02  VE02-CINSEE                                                      
               PIC X(0005).                                                     
           02  VE02-WCONTRA                                                     
               PIC X(0001).                                                     
           02  VE02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  VE02-CILOT                                                       
               PIC X(0008).                                                     
           02  VE02-IDCLIENT                                                    
               PIC X(0008).                                                     
           02  VE02-CPAYS                                                       
               PIC X(0003).                                                     
           02  VE02-NGSM                                                        
               PIC X(0015).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV0202                                  
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVVE0200                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-WTYPEADR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-WTYPEADR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LBATIMENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LESCALIER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LETAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LPORTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LCMPAD1-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LCMPAD2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LCMPAD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-TELDOM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-TELDOM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-TELBUR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-TELBUR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LPOSTEBUR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LPOSTEBUR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LCOMLIV1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LCOMLIV1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-LCOMLIV2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-LCOMLIV2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-WETRANGER-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-WETRANGER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CZONLIV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CZONLIV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-WCONTRA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-WCONTRA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CILOT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CILOT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-IDCLIENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-IDCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-CPAYS-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-CPAYS-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE02-NGSM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE02-NGSM-F                                                      
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
