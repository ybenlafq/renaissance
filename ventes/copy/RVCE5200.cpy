      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************00000010
      * COBOL DECLARATION FOR TABLE DSA000.RTCE52                      *00000020
      ******************************************************************00000030
       01  RVCE5200.                                                    00000040
           10 CE52-NCODIC1         PIC X(7).                            00000050
           10 CE52-COBJET          PIC X(5).                            00000060
           10 CE52-VERSION         PIC X(1).                            00000070
           10 CE52-NCODIC2         PIC X(7).                            00000080
           10 CE52-NCODICS         PIC X(7).                            00000090
           10 CE52-PRIX2           PIC X(1).                            00000100
           10 CE52-DSYST           PIC S9(13)V USAGE COMP-3.            00000110
      ******************************************************************00000120
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *00000130
      ******************************************************************00000140
      ******************************************************************00000150
      * INDICATOR VARIABLE STRUCTURE                                   *00000160
      ******************************************************************00000170
       01  RVCE5200-FLAGS.                                              00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-NCODIC1-F       PIC S9(4) COMP.                      00000190
      *--                                                                       
           10 CE52-NCODIC1-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-COBJET-F        PIC S9(4) COMP.                      00000200
      *--                                                                       
           10 CE52-COBJET-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-VERSION-F       PIC S9(4) COMP.                      00000210
      *--                                                                       
           10 CE52-VERSION-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-NCODIC2-F       PIC S9(4) COMP.                      00000220
      *--                                                                       
           10 CE52-NCODIC2-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-NCODICS-F       PIC S9(4) COMP.                      00000230
      *--                                                                       
           10 CE52-NCODICS-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-PRIX2-F         PIC S9(4) COMP.                      00000240
      *--                                                                       
           10 CE52-PRIX2-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 CE52-DSYST-F         PIC S9(4) COMP.                      00000250
      *                                                                         
      *--                                                                       
           10 CE52-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
