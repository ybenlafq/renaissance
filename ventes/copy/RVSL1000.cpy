      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL1000                         
      *   TABLE D'ECHANGE NMD STOCK LOCAL                                       
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVSL1000.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVSL1000.                                                             
      *}                                                                        
          02  SL10-NSOCCRE                                                      
              PIC   X(03).                                                      
          02  SL10-NLIEUCRE                                                     
              PIC   X(03).                                                      
          02  SL10-CTYPDOC                                                      
              PIC   X(02).                                                      
          02  SL10-NUMDOC                                                       
              PIC   S9(07) COMP-3.                                              
          02  SL10-NLDOC                                                        
              PIC   S9(05) COMP-3.                                              
          02  SL10-NCOLIS                                                       
              PIC   S9(09) COMP-3.                                              
          02  SL10-NCODIC                                                       
              PIC   X(07).                                                      
          02  SL10-CETAT                                                        
              PIC   X(02).                                                      
          02  SL10-NSERIE                                                       
              PIC   X(20).                                                      
          02  SL10-NSOCIDT                                                      
              PIC   X(03).                                                      
          02  SL10-NLIEUIDT                                                     
              PIC   X(03).                                                      
          02  SL10-CTYPDOCIDT                                                   
              PIC   X(02).                                                      
          02  SL10-NUMIDT                                                       
              PIC   X(07).                                                      
          02  SL10-NSOCO                                                        
              PIC   X(03).                                                      
          02  SL10-NLIEUO                                                       
              PIC   X(03).                                                      
          02  SL10-NSSLIEUO                                                     
              PIC   X(03).                                                      
          02  SL10-CLIEUTRTO                                                    
              PIC   X(05).                                                      
          02  SL10-NSOCD                                                        
              PIC   X(03).                                                      
          02  SL10-NLIEUD                                                       
              PIC   X(03).                                                      
          02  SL10-NSSLIEUD                                                     
              PIC   X(03).                                                      
          02  SL10-CLIEUTRTD                                                    
              PIC   X(05).                                                      
          02  SL10-CAUXD                                                        
              PIC   X(6).                                                       
          02  SL10-COPER                                                        
              PIC   X(10).                                                      
          02  SL10-COPAR                                                        
              PIC   X(05).                                                      
          02  SL10-CPROG                                                        
              PIC   X(05).                                                      
          02  SL10-NSOCDREF                                                     
              PIC   X(03).                                                      
          02  SL10-NLIEUDREF                                                    
              PIC   X(03).                                                      
          02  SL10-CTYPDREF                                                     
              PIC   X(02).                                                      
          02  SL10-NUMDREF                                                      
              PIC   S9(07) COMP-3.                                              
          02  SL10-NLDREF                                                       
              PIC   S9(05) COMP-3.                                              
          02  SL10-QEXP                                                         
              PIC   S9(05) COMP-3.                                              
          02  SL10-DOPEREXP                                                     
              PIC   X(08).                                                      
          02  SL10-DMVTEXP                                                      
              PIC   X(08).                                                      
          02  SL10-DHOPEREXP                                                    
              PIC   X(06).                                                      
          02  SL10-CACIDEXP                                                     
              PIC   X(10).                                                      
          02  SL10-QREC                                                         
              PIC   S9(05) COMP-3.                                              
          02  SL10-DOPERREC                                                     
              PIC   X(08).                                                      
          02  SL10-DMVTREC                                                      
              PIC   X(08).                                                      
          02  SL10-DHOPERREC                                                    
              PIC   X(06).                                                      
          02  SL10-CACIDREC                                                     
              PIC   X(10).                                                      
          02  SL10-NCOMM                                                        
              PIC   S9(09) COMP-3.                                              
          02  SL10-WARB                                                         
              PIC   X(01).                                                      
          02  SL10-QARB                                                         
              PIC   S9(05) COMP-3.                                              
          02  SL10-DOPERARB                                                     
              PIC   X(08).                                                      
          02  SL10-DMVTARB                                                      
              PIC   X(08).                                                      
          02  SL10-DHOPERARB                                                    
              PIC   X(06).                                                      
          02  SL10-CACIDARB                                                     
              PIC   X(10).                                                      
          02  SL10-IMAJ                                                         
              PIC   S9(07) COMP-3.                                              
          02  SL10-DSYST                                                        
              PIC   S9(13) COMP-3.                                              
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVSL1000                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01 RVSL1000-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01 RVSL1000-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSOCCRE-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSOCCRE-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLIEUCRE-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLIEUCRE-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CTYPDOC-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CTYPDOC-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NUMDOC-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NUMDOC-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLDOC-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLDOC-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NCOLIS-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NCOLIS-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NCODIC-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NCODIC-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CETAT-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CETAT-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSERIE-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSERIE-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSOCIDT-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSOCIDT-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLIEUIDT-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLIEUIDT-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CTYPDOCIDT-F                                                 
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CTYPDOCIDT-F                                                 
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NUMIDT-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NUMIDT-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSOCO-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSOCO-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLIEUO-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLIEUO-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSSLIEUQ-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSSLIEUQ-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CLIEUTRTO-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CLIEUTRTO-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSOCD-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSOCD-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLIEUD-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLIEUD-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSSLIEUD-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSSLIEUD-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CLIEUTRTD-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CLIEUTRTD-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CAUXD-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CAUXD-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-COPER-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-COPER-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-COPAR-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-COPAR-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CPROG-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CPROG-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NSOCDREF-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NSOCDREF-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLIEUDREF-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLIEUDREF-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CTYPDREF-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CTYPDREF-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NUMDREF-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NUMDREF-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NLDREF-F                                                     
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NLDREF-F                                                     
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-QEXP-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-QEXP-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DOPEREXP-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DOPEREXP-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DMVTEXP-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DMVTEXP-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DHOPEREXP-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DHOPEREXP-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CACIDEXP-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CACIDEXP-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-QREC-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-QREC-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DOPERREC-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DOPERREC-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DMVTREC-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DMVTREC-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DHOPERREC-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DHOPERREC-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CACIDREC-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CACIDREC-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-NCOMM-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-NCOMM-F                                                      
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-WARB-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-WARB-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-QARB-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-QARB-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DOPERARB-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DOPERARB-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DMVTARB-F                                                    
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DMVTARB-F                                                    
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DHOPERARB-F                                                  
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-DHOPERARB-F                                                  
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-CACIDARB-F                                                   
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-CACIDARB-F                                                   
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-IMAJ-F                                                       
      *       PIC   S9(4) COMP.                                                 
      *--                                                                       
          02  SL10-IMAJ-F                                                       
              PIC   S9(4) COMP-5.                                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL10-DSYST-F                                                      
      *       PIC   S9(4) COMP.                                                 
      *                                                                         
      *--                                                                       
          02  SL10-DSYST-F                                                      
              PIC   S9(4) COMP-5.                                               
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
