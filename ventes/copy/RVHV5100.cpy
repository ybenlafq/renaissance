      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5100.                                                            
           02  HV51-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV51-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV51-DCAISSE                                                     
               PIC X(0008).                                                     
           02  HV51-NCAISSE                                                     
               PIC X(0003).                                                     
           02  HV51-NTRANS                                                      
               PIC X(0004).                                                     
           02  HV51-NTYPETRANS                                                  
               PIC X(0001).                                                     
           02  HV51-CANNULATION                                                 
               PIC X(0001).                                                     
           02  HV51-NTRANANUL                                                   
               PIC X(0004).                                                     
           02  HV51-NOPERATEUR                                                  
               PIC X(0004).                                                     
           02  HV51-NVENDEUR                                                    
               PIC X(0004).                                                     
           02  HV51-NLIEUVENTE                                                  
               PIC X(0003).                                                     
           02  HV51-DVENTE                                                      
               PIC X(0008).                                                     
           02  HV51-NVENTE                                                      
               PIC X(0007).                                                     
           02  HV51-NFACTURE                                                    
               PIC X(0006).                                                     
           02  HV51-NEMPORT                                                     
               PIC X(0008).                                                     
           02  HV51-DHVENTE                                                     
               PIC X(0002).                                                     
           02  HV51-DMVENTE                                                     
               PIC X(0002).                                                     
           02  HV51-PTTVENTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PDEDUCT                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PVERSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PCOMPT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PLIVR                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PDIFFERE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PRFACT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-PREMVTE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV51-CORGORED                                                    
               PIC X(0005).                                                     
           02  HV51-CMODPAIMT1                                                  
               PIC X(0001).                                                     
           02  HV51-CMODPAIMT2                                                  
               PIC X(0001).                                                     
           02  HV51-CMODPAIMT3                                                  
               PIC X(0001).                                                     
           02  HV51-CMODPAIMT4                                                  
               PIC X(0001).                                                     
           02  HV51-CMODPAIMT5                                                  
               PIC X(0001).                                                     
           02  HV51-CMODPAIMT6                                                  
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NTYPETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NTYPETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NTRANANUL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NTRANANUL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NLIEUVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NLIEUVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-NEMPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-NEMPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PTTVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PDEDUCT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PDEDUCT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PVERSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PVERSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PDIFFERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PRFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-PREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-PREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CORGORED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT5-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT5-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV51-CMODPAIMT6-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV51-CMODPAIMT6-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
