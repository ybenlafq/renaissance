      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPM0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPM0000                         
      *---------------------------------------------------------                
      *                                                                         
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVPM0000.                                                            
           02  PM00-NSOC                                                        
               PIC X(0003).                                                     
           02  PM00-NLIEU                                                       
               PIC X(0003).                                                     
           02  PM00-LMAGED                                                      
               PIC X(0015).                                                     
           02  PM00-LORDRE                                                      
               PIC X(0015).                                                     
           02  PM00-WMAGP                                                       
               PIC X(0001).                                                     
           02  PM00-WNOBCD                                                      
               PIC X(0001).                                                     
           02  PM00-WNOPSE                                                      
               PIC X(0001).                                                     
           02  PM00-WDEROG                                                      
               PIC X(0001).                                                     
           02  PM00-WMDGRD                                                      
               PIC X(0001).                                                     
           02  PM00-WAUTMG                                                      
               PIC X(0001).                                                     
           02  PM00-CVENDF                                                      
               PIC X(0007).                                                     
           02  PM00-COPERF                                                      
               PIC X(0007).                                                     
           02  PM00-WSTOCK                                                      
               PIC X(0001).                                                     
           02  PM00-CVENLS                                                      
               PIC X(0007).                                                     
           02  PM00-CVSECR                                                      
               PIC X(0007).                                                     
           02  PM00-CTOMAG                                                      
               PIC X(0001).                                                     
           02  PM00-CSSAV                                                       
               PIC X(0003).                                                     
           02  PM00-CLSAV                                                       
               PIC X(0003).                                                     
           02  PM00-CSDACM                                                      
               PIC X(0003).                                                     
           02  PM00-CLDACM                                                      
               PIC X(0003).                                                     
           02  PM00-DSEM01                                                      
               PIC X(0008).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPM0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPM0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-LMAGED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-LMAGED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-LORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-LORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WMAGP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WMAGP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WNOBCD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WNOBCD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WNOPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WNOPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WDEROG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WDEROG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WMDGRD-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WMDGRD-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WAUTMG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WAUTMG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CVENDF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CVENDF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-COPERF-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-COPERF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-WSTOCK-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-WSTOCK-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CVENLS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CVENLS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CVSECR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CVSECR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CTOMAG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CTOMAG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CSSAV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CSSAV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CLSAV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CLSAV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CSDACM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CSDACM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-CLDACM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-CLDACM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM00-DSEM01-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM00-DSEM01-F                                                    
               PIC S9(4) COMP-5.                                                
           EXEC SQL END DECLARE SECTION END-EXEC.
      *}                                                                        
       EJECT                                                                    
                                                                                
