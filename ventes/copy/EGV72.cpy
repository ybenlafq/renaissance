      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV72   EGV72                                              00000020
      ***************************************************************** 00000030
       01   EGV72I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDTRIL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCVENDTRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCVENDTRIF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCVENDTRII     PIC X(6).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENDTRIL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLVENDTRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLVENDTRIF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLVENDTRII     PIC X(15).                                 00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGTRIL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNMAGTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMAGTRIF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNMAGTRII      PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATRIL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDCREATRIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDCREATRIF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDCREATRII     PIC X(10).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMATTRIL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNMATTRIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNMATTRIF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMATTRII      PIC X(7).                                  00000370
           02 M128I OCCURS   15 TIMES .                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCVENDEURL   COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MCVENDEURL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCVENDEURF   PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MCVENDEURI   PIC X(6).                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLVENDEURL   COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLVENDEURL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLVENDEURF   PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLVENDEURI   PIC X(15).                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMAGL  COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MNMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNMAGF  PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNMAGI  PIC X(3).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATIONL  COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDCREATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MDCREATIONF  PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDCREATIONI  PIC X(10).                                 00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNMATSIGAL   COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MNMATSIGAL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNMATSIGAF   PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MNMATSIGAI   PIC X(7).                                  00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPRIMEL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MWPRIMEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MWPRIMEF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MWPRIMEI     PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWPRIMETL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MWPRIMETL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWPRIMETF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MWPRIMETI    PIC X(5).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MZONCMDI  PIC X(15).                                      00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MLIBERRI  PIC X(58).                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MCODTRAI  PIC X(4).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCICSI    PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MNETNAMI  PIC X(8).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MSCREENI  PIC X(4).                                       00000900
      ***************************************************************** 00000910
      * SDF: EGV72   EGV72                                              00000920
      ***************************************************************** 00000930
       01   EGV72O REDEFINES EGV72I.                                    00000940
           02 FILLER    PIC X(12).                                      00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MDATJOUA  PIC X.                                          00000970
           02 MDATJOUC  PIC X.                                          00000980
           02 MDATJOUP  PIC X.                                          00000990
           02 MDATJOUH  PIC X.                                          00001000
           02 MDATJOUV  PIC X.                                          00001010
           02 MDATJOUO  PIC X(10).                                      00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MTIMJOUA  PIC X.                                          00001040
           02 MTIMJOUC  PIC X.                                          00001050
           02 MTIMJOUP  PIC X.                                          00001060
           02 MTIMJOUH  PIC X.                                          00001070
           02 MTIMJOUV  PIC X.                                          00001080
           02 MTIMJOUO  PIC X(5).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MWPAGEA   PIC X.                                          00001110
           02 MWPAGEC   PIC X.                                          00001120
           02 MWPAGEP   PIC X.                                          00001130
           02 MWPAGEH   PIC X.                                          00001140
           02 MWPAGEV   PIC X.                                          00001150
           02 MWPAGEO   PIC X(3).                                       00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCVENDTRIA     PIC X.                                     00001180
           02 MCVENDTRIC     PIC X.                                     00001190
           02 MCVENDTRIP     PIC X.                                     00001200
           02 MCVENDTRIH     PIC X.                                     00001210
           02 MCVENDTRIV     PIC X.                                     00001220
           02 MCVENDTRIO     PIC X(6).                                  00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MLVENDTRIA     PIC X.                                     00001250
           02 MLVENDTRIC     PIC X.                                     00001260
           02 MLVENDTRIP     PIC X.                                     00001270
           02 MLVENDTRIH     PIC X.                                     00001280
           02 MLVENDTRIV     PIC X.                                     00001290
           02 MLVENDTRIO     PIC X(15).                                 00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNMAGTRIA      PIC X.                                     00001320
           02 MNMAGTRIC PIC X.                                          00001330
           02 MNMAGTRIP PIC X.                                          00001340
           02 MNMAGTRIH PIC X.                                          00001350
           02 MNMAGTRIV PIC X.                                          00001360
           02 MNMAGTRIO      PIC X(3).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MDCREATRIA     PIC X.                                     00001390
           02 MDCREATRIC     PIC X.                                     00001400
           02 MDCREATRIP     PIC X.                                     00001410
           02 MDCREATRIH     PIC X.                                     00001420
           02 MDCREATRIV     PIC X.                                     00001430
           02 MDCREATRIO     PIC X(10).                                 00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNMATTRIA      PIC X.                                     00001460
           02 MNMATTRIC PIC X.                                          00001470
           02 MNMATTRIP PIC X.                                          00001480
           02 MNMATTRIH PIC X.                                          00001490
           02 MNMATTRIV PIC X.                                          00001500
           02 MNMATTRIO      PIC X(7).                                  00001510
           02 M128O OCCURS   15 TIMES .                                 00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MCVENDEURA   PIC X.                                     00001540
             03 MCVENDEURC   PIC X.                                     00001550
             03 MCVENDEURP   PIC X.                                     00001560
             03 MCVENDEURH   PIC X.                                     00001570
             03 MCVENDEURV   PIC X.                                     00001580
             03 MCVENDEURO   PIC X(6).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MLVENDEURA   PIC X.                                     00001610
             03 MLVENDEURC   PIC X.                                     00001620
             03 MLVENDEURP   PIC X.                                     00001630
             03 MLVENDEURH   PIC X.                                     00001640
             03 MLVENDEURV   PIC X.                                     00001650
             03 MLVENDEURO   PIC X(15).                                 00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MNMAGA  PIC X.                                          00001680
             03 MNMAGC  PIC X.                                          00001690
             03 MNMAGP  PIC X.                                          00001700
             03 MNMAGH  PIC X.                                          00001710
             03 MNMAGV  PIC X.                                          00001720
             03 MNMAGO  PIC X(3).                                       00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MDCREATIONA  PIC X.                                     00001750
             03 MDCREATIONC  PIC X.                                     00001760
             03 MDCREATIONP  PIC X.                                     00001770
             03 MDCREATIONH  PIC X.                                     00001780
             03 MDCREATIONV  PIC X.                                     00001790
             03 MDCREATIONO  PIC X(10).                                 00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MNMATSIGAA   PIC X.                                     00001820
             03 MNMATSIGAC   PIC X.                                     00001830
             03 MNMATSIGAP   PIC X.                                     00001840
             03 MNMATSIGAH   PIC X.                                     00001850
             03 MNMATSIGAV   PIC X.                                     00001860
             03 MNMATSIGAO   PIC X(7).                                  00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MWPRIMEA     PIC X.                                     00001890
             03 MWPRIMEC     PIC X.                                     00001900
             03 MWPRIMEP     PIC X.                                     00001910
             03 MWPRIMEH     PIC X.                                     00001920
             03 MWPRIMEV     PIC X.                                     00001930
             03 MWPRIMEO     PIC X.                                     00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MWPRIMETA    PIC X.                                     00001960
             03 MWPRIMETC    PIC X.                                     00001970
             03 MWPRIMETP    PIC X.                                     00001980
             03 MWPRIMETH    PIC X.                                     00001990
             03 MWPRIMETV    PIC X.                                     00002000
             03 MWPRIMETO    PIC X(5).                                  00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MZONCMDA  PIC X.                                          00002030
           02 MZONCMDC  PIC X.                                          00002040
           02 MZONCMDP  PIC X.                                          00002050
           02 MZONCMDH  PIC X.                                          00002060
           02 MZONCMDV  PIC X.                                          00002070
           02 MZONCMDO  PIC X(15).                                      00002080
           02 FILLER    PIC X(2).                                       00002090
           02 MLIBERRA  PIC X.                                          00002100
           02 MLIBERRC  PIC X.                                          00002110
           02 MLIBERRP  PIC X.                                          00002120
           02 MLIBERRH  PIC X.                                          00002130
           02 MLIBERRV  PIC X.                                          00002140
           02 MLIBERRO  PIC X(58).                                      00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCODTRAA  PIC X.                                          00002170
           02 MCODTRAC  PIC X.                                          00002180
           02 MCODTRAP  PIC X.                                          00002190
           02 MCODTRAH  PIC X.                                          00002200
           02 MCODTRAV  PIC X.                                          00002210
           02 MCODTRAO  PIC X(4).                                       00002220
           02 FILLER    PIC X(2).                                       00002230
           02 MCICSA    PIC X.                                          00002240
           02 MCICSC    PIC X.                                          00002250
           02 MCICSP    PIC X.                                          00002260
           02 MCICSH    PIC X.                                          00002270
           02 MCICSV    PIC X.                                          00002280
           02 MCICSO    PIC X(5).                                       00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MNETNAMA  PIC X.                                          00002310
           02 MNETNAMC  PIC X.                                          00002320
           02 MNETNAMP  PIC X.                                          00002330
           02 MNETNAMH  PIC X.                                          00002340
           02 MNETNAMV  PIC X.                                          00002350
           02 MNETNAMO  PIC X(8).                                       00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MSCREENA  PIC X.                                          00002380
           02 MSCREENC  PIC X.                                          00002390
           02 MSCREENP  PIC X.                                          00002400
           02 MSCREENH  PIC X.                                          00002410
           02 MSCREENV  PIC X.                                          00002420
           02 MSCREENO  PIC X(4).                                       00002430
                                                                                
