      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGV19                                            *        
      *  TITRE      : COMMAREA DU MODULE MGV19 (APPELE PAR TGV02/09)   *        
      *               CONTROLE DE L'ORTHOGRAPHE DE LA VOIE             *        
      *               POUR LES COMMUNES LOGITRANS                      *        
      *  LONGUEUR   : 150 C                                            *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MGV19-APPLI.                                                    
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 39        
           05 COMM-GV19-DONNEES-ENTREE.                                         
              10 COMM-MGV19-CINSEE         PIC X(05).                           
              10 COMM-MGV19-CVOIE          PIC X(05).                           
              10 COMM-MGV19-CTVOIE         PIC X(04).                           
              10 COMM-MGV19-LNOMVOIE       PIC X(21).                           
              10 COMM-MGV19-CTL-CILOT      PIC X(01).                           
                 88 COMM-MGV19-CTL-EFFECTUE        VALUE 'O'.                   
                 88 COMM-MGV19-CTL-PAS-EFFECTUE    VALUE SPACE.                 
              10 COMM-MGV19-SAISIE-FORCEE  PIC X(01).                           
                 88 COMM-MGV19-PF12-ACTIVEE        VALUE 'O'.                   
                 88 COMM-MGV19-PF12-NON-ACTIVEE    VALUE SPACE.                 
              10 COMM-MGV19-HISTO-PF12     PIC X(01).                           
                 88 COMM-MGV19-PF12-EFFECTUEE      VALUE 'O'.                   
                 88 COMM-MGV19-PF12-PAS-EFFECTUEE  VALUE SPACE.                 
              10 COMM-MGV19-ERREUR-CTL-ADR PIC X(01).                           
                 88 COMM-MGV19-ERREUR-CTL          VALUE 'O'.                   
                 88 COMM-MGV19-PAS-ERREUR-CTL      VALUE SPACE.                 
      *--- DONNEES EN SORTIE DU MODULE ------------------------------ 67        
           05 COMM-MGV19-DONNEES-SORTIE.                                        
              10 COMM-MGV19-MESSAGE.                                            
                 15 COMM-MGV19-CODRET          PIC X(01).                       
                    88 COMM-MGV19-CODRET-OK                 VALUE ' '.          
                    88 COMM-MGV19-CODRET-ERREUR             VALUE '1'.          
                 15 COMM-MGV19-LIBERR.                                          
                    20 COMM-MGV19-NSEQERR         PIC X(04).                    
                    20 COMM-MGV19-ERRFIL          PIC X(01).                    
                    20 COMM-MGV19-LMESSAGE        PIC X(53).                    
              10 COMM-MGV19-CILOT           PIC X(08).                          
      *--- RESERVE -------------------------------------------------- 44        
           05 COMM-MGV19-FILLER          PIC X(44).                             
                                                                                
