      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-CL16-LONG            PIC  S9(4)  COMP-3  VALUE  +35.              
       01  TS-CL16-RECORD.                                                      
           02  TS-CL16-COPSL       PIC  X(05).                                  
           02  TS-CL16-LOPSL       PIC  X(20).                                  
           02  TS-CL16-CTYPDOC     PIC  X(02).                                  
           02  TS-CL16-WCONF       PIC  X(01).                                  
           02  TS-CL16-CTYPRS      PIC  X(02).                                  
           02  TS-CL16-WSUPPRS     PIC  X(01).                                  
           02  TS-CL16-WWVND       PIC  X(01).                                  
           02  TS-CL16-WWHS        PIC  X(01).                                  
           02  TS-CL16-WWUT        PIC  X(01).                                  
           02  TS-CL16-REG         PIC  X(01).                                  
                                                                                
