      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVGV1499                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1499.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1499.                                                            
      *}                                                                        
      *                       NSOCIETE                                          
           10 GV14-NSOCIETE        PIC X(3).                                    
      *                       NLIEU                                             
           10 GV14-NLIEU           PIC X(3).                                    
      *                       NORDRE                                            
           10 GV14-NORDRE          PIC X(5).                                    
      *                       NVENTE                                            
           10 GV14-NVENTE          PIC X(7).                                    
      *                       DSAISIE                                           
           10 GV14-DSAISIE         PIC X(8).                                    
      *                       NSEQ                                              
           10 GV14-NSEQ            PIC X(2).                                    
      *                       CMODPAIMT                                         
           10 GV14-CMODPAIMT       PIC X(5).                                    
      *                       PREGLTVTE                                         
           10 GV14-PREGLTVTE       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       NREGLTVTE                                         
           10 GV14-NREGLTVTE       PIC X(7).                                    
      *                       DREGLTVTE                                         
           10 GV14-DREGLTVTE       PIC X(8).                                    
      *                       WREGLTVTE                                         
           10 GV14-WREGLTVTE       PIC X(1).                                    
      *                       DCOMPTA                                           
           10 GV14-DCOMPTA         PIC X(8).                                    
      *                       DSYST                                             
           10 GV14-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       CPROTOUR                                          
           10 GV14-CPROTOUR        PIC X(5).                                    
      *                       CTOURNEE                                          
           10 GV14-CTOURNEE        PIC X(3).                                    
      *                       CDEVISE                                           
           10 GV14-CDEVISE         PIC X(3).                                    
      *                       MDEVISE                                           
           10 GV14-MDEVISE         PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       MECART                                            
           10 GV14-MECART          PIC S9(1)V9(2) USAGE COMP-3.                 
      *                       CDEV                                              
           10 GV14-CDEV            PIC X(3).                                    
      *                       NSOCP                                             
           10 GV14-NSOCP           PIC X(3).                                    
      *                       NLIEUP                                            
           10 GV14-NLIEUP          PIC X(3).                                    
      *                       NTRANS                                            
           10 GV14-NTRANS          PIC S9(8)V USAGE COMP-3.                     
      *                       PMODPAIMT                                         
           10 GV14-PMODPAIMT       PIC S9(7)V9(2) USAGE COMP-3.                 
      *                       CVENDEUR                                          
           10 GV14-CVENDEUR        PIC X(6).                                    
      *                       CFCRED                                            
           10 GV14-CFCRED          PIC X(5).                                    
      *                       NCREDI                                            
           10 GV14-NCREDI          PIC X(14).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 26      *        
      ******************************************************************        
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV1404                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1404-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1404-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-PREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-WREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-WREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-MDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MECART-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-MECART-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-PMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-PMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CFCRED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NCREDI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV14-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
