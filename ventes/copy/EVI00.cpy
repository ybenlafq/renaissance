      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * EVI00 : ventes inter filiale                                    00000020
      ***************************************************************** 00000030
       01   EVI00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTYPEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTALL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPOSTALL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPOSTALF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPOSTALI  PIC X(5).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCUISL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCUISL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCUISF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCUISI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVENDF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCVENDI   PIC X(6).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENDL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLVENDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLVENDF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLVENDI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDOSSIERL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MDOSSIERL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDOSSIERF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDOSSIERI      PIC X(22).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MECRANL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MECRANL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MECRANF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MECRANI   PIC X(45).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBELLEL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLIBELLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBELLEF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLIBELLEI      PIC X(79).                                 00000450
           02 MSELD OCCURS   12 TIMES .                                 00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000470
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MSELI   PIC X.                                          00000500
           02 MTABD OCCURS   12 TIMES .                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTABL   COMP PIC S9(4).                                 00000520
      *--                                                                       
             03 MTABL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MTABF   PIC X.                                          00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MTABI   PIC X(77).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(74).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * EVI00 : ventes inter filiale                                    00000770
      ***************************************************************** 00000780
       01   EVI00O REDEFINES EVI00I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MTYPEA    PIC X.                                          00000960
           02 MTYPEC    PIC X.                                          00000970
           02 MTYPEP    PIC X.                                          00000980
           02 MTYPEH    PIC X.                                          00000990
           02 MTYPEV    PIC X.                                          00001000
           02 MTYPEO    PIC X(3).                                       00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MPOSTALA  PIC X.                                          00001030
           02 MPOSTALC  PIC X.                                          00001040
           02 MPOSTALP  PIC X.                                          00001050
           02 MPOSTALH  PIC X.                                          00001060
           02 MPOSTALV  PIC X.                                          00001070
           02 MPOSTALO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MCUISA    PIC X.                                          00001100
           02 MCUISC    PIC X.                                          00001110
           02 MCUISP    PIC X.                                          00001120
           02 MCUISH    PIC X.                                          00001130
           02 MCUISV    PIC X.                                          00001140
           02 MCUISO    PIC X(3).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MCVENDA   PIC X.                                          00001170
           02 MCVENDC   PIC X.                                          00001180
           02 MCVENDP   PIC X.                                          00001190
           02 MCVENDH   PIC X.                                          00001200
           02 MCVENDV   PIC X.                                          00001210
           02 MCVENDO   PIC X(6).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLVENDA   PIC X.                                          00001240
           02 MLVENDC   PIC X.                                          00001250
           02 MLVENDP   PIC X.                                          00001260
           02 MLVENDH   PIC X.                                          00001270
           02 MLVENDV   PIC X.                                          00001280
           02 MLVENDO   PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MDOSSIERA      PIC X.                                     00001310
           02 MDOSSIERC PIC X.                                          00001320
           02 MDOSSIERP PIC X.                                          00001330
           02 MDOSSIERH PIC X.                                          00001340
           02 MDOSSIERV PIC X.                                          00001350
           02 MDOSSIERO      PIC X(22).                                 00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MECRANA   PIC X.                                          00001380
           02 MECRANC   PIC X.                                          00001390
           02 MECRANP   PIC X.                                          00001400
           02 MECRANH   PIC X.                                          00001410
           02 MECRANV   PIC X.                                          00001420
           02 MECRANO   PIC X(45).                                      00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLIBELLEA      PIC X.                                     00001450
           02 MLIBELLEC PIC X.                                          00001460
           02 MLIBELLEP PIC X.                                          00001470
           02 MLIBELLEH PIC X.                                          00001480
           02 MLIBELLEV PIC X.                                          00001490
           02 MLIBELLEO      PIC X(79).                                 00001500
           02 DFHMS1 OCCURS   12 TIMES .                                00001510
             03 FILLER       PIC X(2).                                  00001520
             03 MSELA   PIC X.                                          00001530
             03 MSELC   PIC X.                                          00001540
             03 MSELP   PIC X.                                          00001550
             03 MSELH   PIC X.                                          00001560
             03 MSELV   PIC X.                                          00001570
             03 MSELO   PIC X.                                          00001580
           02 DFHMS2 OCCURS   12 TIMES .                                00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MTABA   PIC X.                                          00001610
             03 MTABC   PIC X.                                          00001620
             03 MTABP   PIC X.                                          00001630
             03 MTABH   PIC X.                                          00001640
             03 MTABV   PIC X.                                          00001650
             03 MTABO   PIC X(77).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBERRA  PIC X.                                          00001680
           02 MLIBERRC  PIC X.                                          00001690
           02 MLIBERRP  PIC X.                                          00001700
           02 MLIBERRH  PIC X.                                          00001710
           02 MLIBERRV  PIC X.                                          00001720
           02 MLIBERRO  PIC X(74).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCODTRAA  PIC X.                                          00001750
           02 MCODTRAC  PIC X.                                          00001760
           02 MCODTRAP  PIC X.                                          00001770
           02 MCODTRAH  PIC X.                                          00001780
           02 MCODTRAV  PIC X.                                          00001790
           02 MCODTRAO  PIC X(4).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCICSA    PIC X.                                          00001820
           02 MCICSC    PIC X.                                          00001830
           02 MCICSP    PIC X.                                          00001840
           02 MCICSH    PIC X.                                          00001850
           02 MCICSV    PIC X.                                          00001860
           02 MCICSO    PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSCREENA  PIC X.                                          00001960
           02 MSCREENC  PIC X.                                          00001970
           02 MSCREENP  PIC X.                                          00001980
           02 MSCREENH  PIC X.                                          00001990
           02 MSCREENV  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
