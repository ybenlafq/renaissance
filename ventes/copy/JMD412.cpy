      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD412 AU 22/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,05,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,05,BI,A,                          *        
      *                           22,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD412.                                                        
            05 NOMETAT-JMD412           PIC X(6) VALUE 'JMD412'.                
            05 RUPTURES-JMD412.                                                 
           10 JMD412-COPER              PIC X(05).                      007  005
           10 JMD412-CFAM               PIC X(05).                      012  005
           10 JMD412-WSEQFAM            PIC 9(05).                      017  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD412-SEQUENCE           PIC S9(04) COMP.                022  002
      *--                                                                       
           10 JMD412-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD412.                                                   
           10 JMD412-CMARQ              PIC X(05).                      024  005
           10 JMD412-LFAM               PIC X(20).                      029  020
           10 JMD412-LOPER              PIC X(20).                      049  020
           10 JMD412-LREFFOURN          PIC X(20).                      069  020
           10 JMD412-NCODIC             PIC X(07).                      089  007
           10 JMD412-NMAG               PIC X(03).                      096  003
           10 JMD412-NSOC               PIC X(03).                      099  003
           10 JMD412-PCESSIONCUM        PIC S9(11)V9(2) COMP-3.         102  007
           10 JMD412-PCESSIONCUM-EXC    PIC S9(11)V9(2) COMP-3.         109  007
           10 JMD412-PRMPCUM            PIC S9(11)V9(2) COMP-3.         116  007
           10 JMD412-PRMPCUM-EXC        PIC S9(09)V9(2) COMP-3.         123  006
           10 JMD412-QTECUM             PIC S9(06)      COMP-3.         129  004
           10 JMD412-QTECUM-EXC         PIC S9(06)      COMP-3.         133  004
           10 JMD412-DDEB               PIC X(08).                      137  008
           10 JMD412-DFIN               PIC X(08).                      145  008
            05 FILLER                      PIC X(360).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD412-LONG           PIC S9(4)   COMP  VALUE +152.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD412-LONG           PIC S9(4) COMP-5  VALUE +152.           
                                                                                
      *}                                                                        
