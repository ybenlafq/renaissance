      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE GVCAD ASSOCIATION CADRE/ SOUS-TABLE    *        
      *----------------------------------------------------------------*        
       01  RVGVCAD.                                                             
           05  GVCAD-CTABLEG2    PIC X(15).                                     
           05  GVCAD-CTABLEG2-REDEF REDEFINES GVCAD-CTABLEG2.                   
               10  GVCAD-CADRE           PIC X(05).                             
           05  GVCAD-WTABLEG     PIC X(80).                                     
           05  GVCAD-WTABLEG-REDEF  REDEFINES GVCAD-WTABLEG.                    
               10  GVCAD-TCADRE          PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVGVCAD-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCAD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  GVCAD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  GVCAD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  GVCAD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
