      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC2001                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC2001.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC2001.                                                            
      *}                                                                        
           10 EC20-CFAM            PIC X(5).                                    
           10 EC20-SMAG            PIC S9(5)V USAGE COMP-3.                     
           10 EC20-SART            PIC S9(5)V USAGE COMP-3.                     
           10 EC20-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC20-SDEPOT          PIC S9(5)V USAGE COMP-3.                     
           10 EC20-WCC             PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 6       *        
      ******************************************************************        
      **********************************************************                
M20448*   LISTE DES FLAGS DE LA TABLE RVEC2001                                  
  "   **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
  "   *01  RVEC2001-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC2001-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-CFAM-F                                                      
  "   *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC20-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-SMAG-F                                                      
  "   *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC20-SMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-SART-F                                                      
  "   *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC20-SART-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-DSYST-F                                                     
  "   *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-SDEPOT-F                                                    
  "   *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC20-SDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
  "   *    02  EC20-WCC-F                                                       
  "   *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  EC20-WCC-F                                                       
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
