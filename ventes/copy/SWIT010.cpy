      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * FICHIER DE TRAITEMENT DANS CREATION TABLE IT015                 00000010
      * ET EDITION DES SUPPORTS DE COMPTAGE                             00000020
      *                                                                 00000030
       01  FIT010-DSECT.                                                00000040
           05  FIT010-CLEF.                                             00000050
               10  FIT010-NINVENTAIRE      PIC   X(05).                 00000060
               10  FIT010-NLIEU            PIC   X(03).                 00000070
               10  FIT010-NSOCIETE         PIC   X(03).                 00000080
               10  FIT010-NSSLIEU          PIC   X(03).                 00000090
               10  FIT010-WFLAG            PIC   X(01).                 00000100
               10  FIT010-NCODIC           PIC   X(07).                 00000110
           05      FIT010-QSTOCKTHEO       PIC  S9(05) COMP-3.          00000120
           05      FIT010-QSTOCKINVT       PIC  S9(05) COMP-3.          00000130
           05      FIT010-DSYST            PIC  S9(13) COMP-3.          00000140
      *                                                       --> 035   00000150
           05      FIT010-WSEQFAM          PIC  S9(05) COMP-3.          00000160
           05      FIT010-CFAM             PIC   X(05).                 00000170
           05      FIT010-CMARQ            PIC   X(05).                 00000180
           05      FIT010-LREFFOURN        PIC   X(20).                 00000190
           05      FIT010-DSTOCKTHEO       PIC   X(08).                 00000200
           05      FIT010-DCOMPTAGE        PIC   X(08).                 00000210
      *                                                                 00000220
      *--- LE CODE PERMET DE CHOISIR LE TYPE DE SUPPORT DE COMPTAGE     00000230
      *    1 POUR IIT011     2 POUR IIT031                              00000240
      *                                                                 00000250
           05      FIT010-CODE             PIC   X(01).                 00000260
           05      FILLER                  PIC   X(05).                 00000270
      *                                                       --> 090   00000280
                                                                                
