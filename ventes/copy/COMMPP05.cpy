      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  PROGRAMMES : MPP05 ET MPP005                                  *        
      *  TITRE      : COMMAREA DES MODULES (TP ET BATCH) DE CALCUL     *        
      *               DE LA PAMM D'UN MAGASIN                          *        
      *  LONGUEUR   : 6635 C                                           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-PP05-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-PP05-DONNEES-ENTREE.                                         
              10 COMM-PP05-NSOCIETE         PIC X(03).                          
              10 COMM-PP05-NLIEU            PIC X(03).                          
              10 COMM-PP05-DMOISPAYE        PIC X(06).                          
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-PP05-MESSAGE.                                                
              10 COMM-PP05-CODRET           PIC X(01).                          
                 88 COMM-PP05-CODRET-OK                  VALUE ' '.             
                 88 COMM-PP05-CODRET-ERREUR              VALUE '1'.             
              10 COMM-PP05-LIBERR           PIC X(58).                          
      *--- MONTANTS PAMM, CA ET VOLUME DU MAGASIN ------------------- 61        
           05 COMM-PP05-PMONTANTS-MAG.                                          
              10 COMM-PP05-PMONTANTS-PAMM-POSTE  OCCURS 3.                      
      *--------- POSTE 1 = MONTANT PAMM VOLUME                                  
      *--------- POSTE 2 = MONTANT PAMM C.A                                     
      *--------- POSTE 3 = MONTANT PAMM TOTAL                                   
                 15 COMM-PP05-PPAMM-OBJECTIF   PIC S9(07)V99  COMP-3.           
                 15 COMM-PP05-PPAMM-AFFECTE    PIC S9(07)     COMP-3.           
                 15 COMM-PP05-PPAMM-ECART      PIC S9(07)     COMP-3.           
                 15 COMM-PP05-PPAMM-NONAFF     PIC S9(07)     COMP-3.           
              10 COMM-PP05-QPIECEMAG        PIC S9(07)     COMP-3.              
              10 COMM-PP05-PMTCAMAG         PIC S9(11)     COMP-3.              
      *--- TABLEAU DES VENDEURS ----(100 * 65 C) + 3 -------------- 6503        
           05 COMM-PP05-IT-MAX          PIC S9(05) COMP-3.                      
           05 COMM-PP05-TAB-VENDEUR     OCCURS 100.                             
              10 COMM-PP05-NSOCMAG          PIC X(03).                          
              10 COMM-PP05-NMAG             PIC X(03).                          
              10 COMM-PP05-CVENDEUR         PIC X(06).                          
              10 COMM-PP05-DONNEES-VENDEUR.                                     
                 15 COMM-PP05-LVENDEUR         PIC X(15).                       
                 15 COMM-PP05-NMATSIGA         PIC X(07).                       
                 15 COMM-PP05-WPRIME           PIC X(01).                       
              10 COMM-PP05-DONNEES-PRIME.                                       
                 15 COMM-PP05-QPIECEVOL        PIC S9(07)     COMP-3.           
                 15 COMM-PP05-PMTCA            PIC S9(11)     COMP-3.           
                 15 COMM-PP05-QPOURCVOL        PIC S9(3)V9(3) COMP-3.           
                 15 COMM-PP05-QPOURCCA         PIC S9(3)V9(3) COMP-3.           
                 15 COMM-PP05-PPAMM            OCCURS 3                         
                                               PIC S9(07)     COMP-3.           
                                                                                
