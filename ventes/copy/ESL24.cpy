      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL24   ESL24                                              00000020
      ***************************************************************** 00000030
       01   ESL24I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGESI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCCL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCCF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCCI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUCL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUCF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUCI  PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTYPDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPDOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTYPDOCI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNUMDOCI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDDEBUTI  PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDFINI    PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOL    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSOCOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCOF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSOCOI    PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIEUOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUOF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIEUOI   PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCO1L   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MSOCO1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCO1F   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MSOCO1I   PIC X(3).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUO1L  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIEUO1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUO1F  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIEUO1I  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUOL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MLLIEUOL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUOF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLLIEUOI  PIC X(15).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSOCDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSOCDF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSOCDI    PIC X(3).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDL   COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIEUDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIEUDF   PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIEUDI   PIC X(3).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUDL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLLIEUDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLLIEUDF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLLIEUDI  PIC X(15).                                      00000770
           02 MNSOCD OCCURS   13 TIMES .                                00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNSOCI  PIC X(3).                                       00000820
           02 MNLIEUD OCCURS   13 TIMES .                               00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000840
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MNLIEUI      PIC X(3).                                  00000870
           02 MTYPED OCCURS   13 TIMES .                                00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTYPEL  COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MTYPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTYPEF  PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MTYPEI  PIC X(2).                                       00000920
           02 MNUMD OCCURS   13 TIMES .                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNUML   COMP PIC S9(4).                                 00000940
      *--                                                                       
             03 MNUML COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MNUMF   PIC X.                                          00000950
             03 FILLER  PIC X(4).                                       00000960
             03 MNUMI   PIC X(7).                                       00000970
           02 MCFAMD OCCURS   13 TIMES .                                00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MCFAMI  PIC X(5).                                       00001020
           02 MCMARQD OCCURS   13 TIMES .                               00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MCMARQI      PIC X(5).                                  00001070
           02 MNCODICD OCCURS   13 TIMES .                              00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MNCODICI     PIC X(7).                                  00001120
           02 MNSOCOD OCCURS   13 TIMES .                               00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCOL      COMP PIC S9(4).                            00001140
      *--                                                                       
             03 MNSOCOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNSOCOF      PIC X.                                     00001150
             03 FILLER  PIC X(4).                                       00001160
             03 MNSOCOI      PIC X(3).                                  00001170
           02 MNLIEUOD OCCURS   13 TIMES .                              00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUOL     COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MNLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNLIEUOF     PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MNLIEUOI     PIC X(3).                                  00001220
           02 MDATEEXPD OCCURS   13 TIMES .                             00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDATEEXPL    COMP PIC S9(4).                            00001240
      *--                                                                       
             03 MDATEEXPL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDATEEXPF    PIC X.                                     00001250
             03 FILLER  PIC X(4).                                       00001260
             03 MDATEEXPI    PIC X(5).                                  00001270
           02 MQEXPD OCCURS   13 TIMES .                                00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQEXPL  COMP PIC S9(4).                                 00001290
      *--                                                                       
             03 MQEXPL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQEXPF  PIC X.                                          00001300
             03 FILLER  PIC X(4).                                       00001310
             03 MQEXPI  PIC X(5).                                       00001320
           02 MQRECD OCCURS   13 TIMES .                                00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQRECL  COMP PIC S9(4).                                 00001340
      *--                                                                       
             03 MQRECL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MQRECF  PIC X.                                          00001350
             03 FILLER  PIC X(4).                                       00001360
             03 MQRECI  PIC X(5).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MLIBERRI  PIC X(78).                                      00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCODTRAI  PIC X(4).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MCICSI    PIC X(5).                                       00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001500
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MNETNAMI  PIC X(8).                                       00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MSCREENI  PIC X(4).                                       00001570
      ***************************************************************** 00001580
      * SDF: ESL24   ESL24                                              00001590
      ***************************************************************** 00001600
       01   ESL24O REDEFINES ESL24I.                                    00001610
           02 FILLER    PIC X(12).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MDATJOUA  PIC X.                                          00001640
           02 MDATJOUC  PIC X.                                          00001650
           02 MDATJOUP  PIC X.                                          00001660
           02 MDATJOUH  PIC X.                                          00001670
           02 MDATJOUV  PIC X.                                          00001680
           02 MDATJOUO  PIC X(10).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MTIMJOUA  PIC X.                                          00001710
           02 MTIMJOUC  PIC X.                                          00001720
           02 MTIMJOUP  PIC X.                                          00001730
           02 MTIMJOUH  PIC X.                                          00001740
           02 MTIMJOUV  PIC X.                                          00001750
           02 MTIMJOUO  PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNPAGEA   PIC X.                                          00001780
           02 MNPAGEC   PIC X.                                          00001790
           02 MNPAGEP   PIC X.                                          00001800
           02 MNPAGEH   PIC X.                                          00001810
           02 MNPAGEV   PIC X.                                          00001820
           02 MNPAGEO   PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNBPAGESA      PIC X.                                     00001850
           02 MNBPAGESC PIC X.                                          00001860
           02 MNBPAGESP PIC X.                                          00001870
           02 MNBPAGESH PIC X.                                          00001880
           02 MNBPAGESV PIC X.                                          00001890
           02 MNBPAGESO      PIC X(3).                                  00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MNSOCCA   PIC X.                                          00001920
           02 MNSOCCC   PIC X.                                          00001930
           02 MNSOCCP   PIC X.                                          00001940
           02 MNSOCCH   PIC X.                                          00001950
           02 MNSOCCV   PIC X.                                          00001960
           02 MNSOCCO   PIC X(3).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNLIEUCA  PIC X.                                          00001990
           02 MNLIEUCC  PIC X.                                          00002000
           02 MNLIEUCP  PIC X.                                          00002010
           02 MNLIEUCH  PIC X.                                          00002020
           02 MNLIEUCV  PIC X.                                          00002030
           02 MNLIEUCO  PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MTYPDOCA  PIC X.                                          00002060
           02 MTYPDOCC  PIC X.                                          00002070
           02 MTYPDOCP  PIC X.                                          00002080
           02 MTYPDOCH  PIC X.                                          00002090
           02 MTYPDOCV  PIC X.                                          00002100
           02 MTYPDOCO  PIC X(2).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNUMDOCA  PIC X.                                          00002130
           02 MNUMDOCC  PIC X.                                          00002140
           02 MNUMDOCP  PIC X.                                          00002150
           02 MNUMDOCH  PIC X.                                          00002160
           02 MNUMDOCV  PIC X.                                          00002170
           02 MNUMDOCO  PIC X(7).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MDDEBUTA  PIC X.                                          00002200
           02 MDDEBUTC  PIC X.                                          00002210
           02 MDDEBUTP  PIC X.                                          00002220
           02 MDDEBUTH  PIC X.                                          00002230
           02 MDDEBUTV  PIC X.                                          00002240
           02 MDDEBUTO  PIC X(6).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDFINA    PIC X.                                          00002270
           02 MDFINC    PIC X.                                          00002280
           02 MDFINP    PIC X.                                          00002290
           02 MDFINH    PIC X.                                          00002300
           02 MDFINV    PIC X.                                          00002310
           02 MDFINO    PIC X(6).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MSOCOA    PIC X.                                          00002340
           02 MSOCOC    PIC X.                                          00002350
           02 MSOCOP    PIC X.                                          00002360
           02 MSOCOH    PIC X.                                          00002370
           02 MSOCOV    PIC X.                                          00002380
           02 MSOCOO    PIC X(3).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLIEUOA   PIC X.                                          00002410
           02 MLIEUOC   PIC X.                                          00002420
           02 MLIEUOP   PIC X.                                          00002430
           02 MLIEUOH   PIC X.                                          00002440
           02 MLIEUOV   PIC X.                                          00002450
           02 MLIEUOO   PIC X(3).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MSOCO1A   PIC X.                                          00002480
           02 MSOCO1C   PIC X.                                          00002490
           02 MSOCO1P   PIC X.                                          00002500
           02 MSOCO1H   PIC X.                                          00002510
           02 MSOCO1V   PIC X.                                          00002520
           02 MSOCO1O   PIC X(3).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MLIEUO1A  PIC X.                                          00002550
           02 MLIEUO1C  PIC X.                                          00002560
           02 MLIEUO1P  PIC X.                                          00002570
           02 MLIEUO1H  PIC X.                                          00002580
           02 MLIEUO1V  PIC X.                                          00002590
           02 MLIEUO1O  PIC X(3).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLLIEUOA  PIC X.                                          00002620
           02 MLLIEUOC  PIC X.                                          00002630
           02 MLLIEUOP  PIC X.                                          00002640
           02 MLLIEUOH  PIC X.                                          00002650
           02 MLLIEUOV  PIC X.                                          00002660
           02 MLLIEUOO  PIC X(15).                                      00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSOCDA    PIC X.                                          00002690
           02 MSOCDC    PIC X.                                          00002700
           02 MSOCDP    PIC X.                                          00002710
           02 MSOCDH    PIC X.                                          00002720
           02 MSOCDV    PIC X.                                          00002730
           02 MSOCDO    PIC X(3).                                       00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLIEUDA   PIC X.                                          00002760
           02 MLIEUDC   PIC X.                                          00002770
           02 MLIEUDP   PIC X.                                          00002780
           02 MLIEUDH   PIC X.                                          00002790
           02 MLIEUDV   PIC X.                                          00002800
           02 MLIEUDO   PIC X(3).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MLLIEUDA  PIC X.                                          00002830
           02 MLLIEUDC  PIC X.                                          00002840
           02 MLLIEUDP  PIC X.                                          00002850
           02 MLLIEUDH  PIC X.                                          00002860
           02 MLLIEUDV  PIC X.                                          00002870
           02 MLLIEUDO  PIC X(15).                                      00002880
           02 DFHMS1 OCCURS   13 TIMES .                                00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MNSOCA  PIC X.                                          00002910
             03 MNSOCC  PIC X.                                          00002920
             03 MNSOCP  PIC X.                                          00002930
             03 MNSOCH  PIC X.                                          00002940
             03 MNSOCV  PIC X.                                          00002950
             03 MNSOCO  PIC X(3).                                       00002960
           02 DFHMS2 OCCURS   13 TIMES .                                00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MNLIEUA      PIC X.                                     00002990
             03 MNLIEUC PIC X.                                          00003000
             03 MNLIEUP PIC X.                                          00003010
             03 MNLIEUH PIC X.                                          00003020
             03 MNLIEUV PIC X.                                          00003030
             03 MNLIEUO      PIC X(3).                                  00003040
           02 DFHMS3 OCCURS   13 TIMES .                                00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MTYPEA  PIC X.                                          00003070
             03 MTYPEC  PIC X.                                          00003080
             03 MTYPEP  PIC X.                                          00003090
             03 MTYPEH  PIC X.                                          00003100
             03 MTYPEV  PIC X.                                          00003110
             03 MTYPEO  PIC X(2).                                       00003120
           02 DFHMS4 OCCURS   13 TIMES .                                00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MNUMA   PIC X.                                          00003150
             03 MNUMC   PIC X.                                          00003160
             03 MNUMP   PIC X.                                          00003170
             03 MNUMH   PIC X.                                          00003180
             03 MNUMV   PIC X.                                          00003190
             03 MNUMO   PIC X(7).                                       00003200
           02 DFHMS5 OCCURS   13 TIMES .                                00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MCFAMA  PIC X.                                          00003230
             03 MCFAMC  PIC X.                                          00003240
             03 MCFAMP  PIC X.                                          00003250
             03 MCFAMH  PIC X.                                          00003260
             03 MCFAMV  PIC X.                                          00003270
             03 MCFAMO  PIC X(5).                                       00003280
           02 DFHMS6 OCCURS   13 TIMES .                                00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MCMARQA      PIC X.                                     00003310
             03 MCMARQC PIC X.                                          00003320
             03 MCMARQP PIC X.                                          00003330
             03 MCMARQH PIC X.                                          00003340
             03 MCMARQV PIC X.                                          00003350
             03 MCMARQO      PIC X(5).                                  00003360
           02 DFHMS7 OCCURS   13 TIMES .                                00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MNCODICA     PIC X.                                     00003390
             03 MNCODICC     PIC X.                                     00003400
             03 MNCODICP     PIC X.                                     00003410
             03 MNCODICH     PIC X.                                     00003420
             03 MNCODICV     PIC X.                                     00003430
             03 MNCODICO     PIC X(7).                                  00003440
           02 DFHMS8 OCCURS   13 TIMES .                                00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MNSOCOA      PIC X.                                     00003470
             03 MNSOCOC PIC X.                                          00003480
             03 MNSOCOP PIC X.                                          00003490
             03 MNSOCOH PIC X.                                          00003500
             03 MNSOCOV PIC X.                                          00003510
             03 MNSOCOO      PIC X(3).                                  00003520
           02 DFHMS9 OCCURS   13 TIMES .                                00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MNLIEUOA     PIC X.                                     00003550
             03 MNLIEUOC     PIC X.                                     00003560
             03 MNLIEUOP     PIC X.                                     00003570
             03 MNLIEUOH     PIC X.                                     00003580
             03 MNLIEUOV     PIC X.                                     00003590
             03 MNLIEUOO     PIC X(3).                                  00003600
           02 DFHMS10 OCCURS   13 TIMES .                               00003610
             03 FILLER       PIC X(2).                                  00003620
             03 MDATEEXPA    PIC X.                                     00003630
             03 MDATEEXPC    PIC X.                                     00003640
             03 MDATEEXPP    PIC X.                                     00003650
             03 MDATEEXPH    PIC X.                                     00003660
             03 MDATEEXPV    PIC X.                                     00003670
             03 MDATEEXPO    PIC X(5).                                  00003680
           02 DFHMS11 OCCURS   13 TIMES .                               00003690
             03 FILLER       PIC X(2).                                  00003700
             03 MQEXPA  PIC X.                                          00003710
             03 MQEXPC  PIC X.                                          00003720
             03 MQEXPP  PIC X.                                          00003730
             03 MQEXPH  PIC X.                                          00003740
             03 MQEXPV  PIC X.                                          00003750
             03 MQEXPO  PIC X(5).                                       00003760
           02 DFHMS12 OCCURS   13 TIMES .                               00003770
             03 FILLER       PIC X(2).                                  00003780
             03 MQRECA  PIC X.                                          00003790
             03 MQRECC  PIC X.                                          00003800
             03 MQRECP  PIC X.                                          00003810
             03 MQRECH  PIC X.                                          00003820
             03 MQRECV  PIC X.                                          00003830
             03 MQRECO  PIC X(5).                                       00003840
           02 FILLER    PIC X(2).                                       00003850
           02 MLIBERRA  PIC X.                                          00003860
           02 MLIBERRC  PIC X.                                          00003870
           02 MLIBERRP  PIC X.                                          00003880
           02 MLIBERRH  PIC X.                                          00003890
           02 MLIBERRV  PIC X.                                          00003900
           02 MLIBERRO  PIC X(78).                                      00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MCODTRAA  PIC X.                                          00003930
           02 MCODTRAC  PIC X.                                          00003940
           02 MCODTRAP  PIC X.                                          00003950
           02 MCODTRAH  PIC X.                                          00003960
           02 MCODTRAV  PIC X.                                          00003970
           02 MCODTRAO  PIC X(4).                                       00003980
           02 FILLER    PIC X(2).                                       00003990
           02 MCICSA    PIC X.                                          00004000
           02 MCICSC    PIC X.                                          00004010
           02 MCICSP    PIC X.                                          00004020
           02 MCICSH    PIC X.                                          00004030
           02 MCICSV    PIC X.                                          00004040
           02 MCICSO    PIC X(5).                                       00004050
           02 FILLER    PIC X(2).                                       00004060
           02 MNETNAMA  PIC X.                                          00004070
           02 MNETNAMC  PIC X.                                          00004080
           02 MNETNAMP  PIC X.                                          00004090
           02 MNETNAMH  PIC X.                                          00004100
           02 MNETNAMV  PIC X.                                          00004110
           02 MNETNAMO  PIC X(8).                                       00004120
           02 FILLER    PIC X(2).                                       00004130
           02 MSCREENA  PIC X.                                          00004140
           02 MSCREENC  PIC X.                                          00004150
           02 MSCREENP  PIC X.                                          00004160
           02 MSCREENH  PIC X.                                          00004170
           02 MSCREENV  PIC X.                                          00004180
           02 MSCREENO  PIC X(4).                                       00004190
                                                                                
