      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * TD00 : RESULTAT RECHERCHE                                       00000020
      ***************************************************************** 00000030
       01   ETD01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEAI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLSOCIETEI     PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUI   PIC X(3).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLLIEUI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREATIONL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCREATIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCREATIONF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCREATIONI     PIC X(28).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPTIONL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLOPTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLOPTIONF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLOPTIONI      PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLGNL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBLGNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBLGNF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBLGNI  PIC X(77).                                      00000490
           02 MTABI OCCURS   13 TIMES .                                 00000500
             03 MXD OCCURS   2 TIMES .                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MXL   COMP PIC S9(4).                                 00000520
      *--                                                                       
               04 MXL COMP-5 PIC S9(4).                                         
      *}                                                                        
               04 MXF   PIC X.                                          00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MXI   PIC X.                                          00000550
             03 MLIGNED OCCURS   2 TIMES .                              00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLIGNEL    COMP PIC S9(4).                            00000570
      *--                                                                       
               04 MLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MLIGNEF    PIC X.                                     00000580
               04 FILLER     PIC X(4).                                  00000590
               04 MLIGNEI    PIC X(36).                                 00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MLIBERRI  PIC X(78).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCODTRAI  PIC X(4).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCICSI    PIC X(5).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MNETNAMI  PIC X(8).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MSCREENI  PIC X(4).                                       00000800
      ***************************************************************** 00000810
      * TD00 : RESULTAT RECHERCHE                                       00000820
      ***************************************************************** 00000830
       01   ETD01O REDEFINES ETD01I.                                    00000840
           02 FILLER    PIC X(12).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MDATJOUA  PIC X.                                          00000870
           02 MDATJOUC  PIC X.                                          00000880
           02 MDATJOUP  PIC X.                                          00000890
           02 MDATJOUH  PIC X.                                          00000900
           02 MDATJOUV  PIC X.                                          00000910
           02 MDATJOUO  PIC X(10).                                      00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MTIMJOUA  PIC X.                                          00000940
           02 MTIMJOUC  PIC X.                                          00000950
           02 MTIMJOUP  PIC X.                                          00000960
           02 MTIMJOUH  PIC X.                                          00000970
           02 MTIMJOUV  PIC X.                                          00000980
           02 MTIMJOUO  PIC X(5).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MPAGEAA   PIC X.                                          00001010
           02 MPAGEAC   PIC X.                                          00001020
           02 MPAGEAP   PIC X.                                          00001030
           02 MPAGEAH   PIC X.                                          00001040
           02 MPAGEAV   PIC X.                                          00001050
           02 MPAGEAO   PIC Z9.                                         00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNBPA     PIC X.                                          00001080
           02 MNBPC     PIC X.                                          00001090
           02 MNBPP     PIC X.                                          00001100
           02 MNBPH     PIC X.                                          00001110
           02 MNBPV     PIC X.                                          00001120
           02 MNBPO     PIC Z9.                                         00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MNSOCIETEA     PIC X.                                     00001150
           02 MNSOCIETEC     PIC X.                                     00001160
           02 MNSOCIETEP     PIC X.                                     00001170
           02 MNSOCIETEH     PIC X.                                     00001180
           02 MNSOCIETEV     PIC X.                                     00001190
           02 MNSOCIETEO     PIC X(3).                                  00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MLSOCIETEA     PIC X.                                     00001220
           02 MLSOCIETEC     PIC X.                                     00001230
           02 MLSOCIETEP     PIC X.                                     00001240
           02 MLSOCIETEH     PIC X.                                     00001250
           02 MLSOCIETEV     PIC X.                                     00001260
           02 MLSOCIETEO     PIC X(20).                                 00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNLIEUA   PIC X.                                          00001290
           02 MNLIEUC   PIC X.                                          00001300
           02 MNLIEUP   PIC X.                                          00001310
           02 MNLIEUH   PIC X.                                          00001320
           02 MNLIEUV   PIC X.                                          00001330
           02 MNLIEUO   PIC X(3).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MLLIEUA   PIC X.                                          00001360
           02 MLLIEUC   PIC X.                                          00001370
           02 MLLIEUP   PIC X.                                          00001380
           02 MLLIEUH   PIC X.                                          00001390
           02 MLLIEUV   PIC X.                                          00001400
           02 MLLIEUO   PIC X(20).                                      00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MCREATIONA     PIC X.                                     00001430
           02 MCREATIONC     PIC X.                                     00001440
           02 MCREATIONP     PIC X.                                     00001450
           02 MCREATIONH     PIC X.                                     00001460
           02 MCREATIONV     PIC X.                                     00001470
           02 MCREATIONO     PIC X(28).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MLOPTIONA      PIC X.                                     00001500
           02 MLOPTIONC PIC X.                                          00001510
           02 MLOPTIONP PIC X.                                          00001520
           02 MLOPTIONH PIC X.                                          00001530
           02 MLOPTIONV PIC X.                                          00001540
           02 MLOPTIONO      PIC X(20).                                 00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBLGNA  PIC X.                                          00001570
           02 MLIBLGNC  PIC X.                                          00001580
           02 MLIBLGNP  PIC X.                                          00001590
           02 MLIBLGNH  PIC X.                                          00001600
           02 MLIBLGNV  PIC X.                                          00001610
           02 MLIBLGNO  PIC X(77).                                      00001620
           02 MTABO OCCURS   13 TIMES .                                 00001630
             03 DFHMS1 OCCURS   2 TIMES .                               00001640
               04 FILLER     PIC X(2).                                  00001650
               04 MXA   PIC X.                                          00001660
               04 MXC   PIC X.                                          00001670
               04 MXP   PIC X.                                          00001680
               04 MXH   PIC X.                                          00001690
               04 MXV   PIC X.                                          00001700
               04 MXO   PIC X.                                          00001710
             03 DFHMS2 OCCURS   2 TIMES .                               00001720
               04 FILLER     PIC X(2).                                  00001730
               04 MLIGNEA    PIC X.                                     00001740
               04 MLIGNEC    PIC X.                                     00001750
               04 MLIGNEP    PIC X.                                     00001760
               04 MLIGNEH    PIC X.                                     00001770
               04 MLIGNEV    PIC X.                                     00001780
               04 MLIGNEO    PIC X(36).                                 00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLIBERRA  PIC X.                                          00001810
           02 MLIBERRC  PIC X.                                          00001820
           02 MLIBERRP  PIC X.                                          00001830
           02 MLIBERRH  PIC X.                                          00001840
           02 MLIBERRV  PIC X.                                          00001850
           02 MLIBERRO  PIC X(78).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCODTRAA  PIC X.                                          00001880
           02 MCODTRAC  PIC X.                                          00001890
           02 MCODTRAP  PIC X.                                          00001900
           02 MCODTRAH  PIC X.                                          00001910
           02 MCODTRAV  PIC X.                                          00001920
           02 MCODTRAO  PIC X(4).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MCICSA    PIC X.                                          00001950
           02 MCICSC    PIC X.                                          00001960
           02 MCICSP    PIC X.                                          00001970
           02 MCICSH    PIC X.                                          00001980
           02 MCICSV    PIC X.                                          00001990
           02 MCICSO    PIC X(5).                                       00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNETNAMA  PIC X.                                          00002020
           02 MNETNAMC  PIC X.                                          00002030
           02 MNETNAMP  PIC X.                                          00002040
           02 MNETNAMH  PIC X.                                          00002050
           02 MNETNAMV  PIC X.                                          00002060
           02 MNETNAMO  PIC X(8).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MSCREENA  PIC X.                                          00002090
           02 MSCREENC  PIC X.                                          00002100
           02 MSCREENP  PIC X.                                          00002110
           02 MSCREENH  PIC X.                                          00002120
           02 MSCREENV  PIC X.                                          00002130
           02 MSCREENO  PIC X(4).                                       00002140
                                                                                
