      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE006      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,05,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE006.                                                        
            05 NOMETAT-IRE006           PIC X(6) VALUE 'IRE006'.                
            05 RUPTURES-IRE006.                                                 
           10 IRE006-NSOCIETE           PIC X(03).                      007  003
           10 IRE006-TYPE-ENR           PIC X(01).                      010  001
           10 IRE006-CRAYON             PIC X(05).                      011  005
           10 IRE006-CGRPMAG            PIC X(02).                      016  002
           10 IRE006-NLIEU              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE006-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE006-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE006.                                                   
           10 IRE006-LIBMOIS            PIC X(09).                      023  009
           10 IRE006-PCT1               PIC X(16).                      032  016
           10 IRE006-PCT2               PIC X(16).                      048  016
           10 IRE006-QTE1-C             PIC S9(09)      COMP-3.         064  005
           10 IRE006-QTE1-M             PIC S9(07)      COMP-3.         069  004
           10 IRE006-QTE2-C             PIC S9(09)      COMP-3.         073  005
           10 IRE006-QTE2-M             PIC S9(07)      COMP-3.         078  004
           10 IRE006-TOT-VAL-FORC-M     PIC S9(12)V9(3) COMP-3.         082  008
           10 IRE006-TOT-VAL-REM        PIC S9(12)V9(3) COMP-3.         090  008
           10 IRE006-TOT-VENTES2        PIC S9(12)V9(3) COMP-3.         098  008
           10 IRE006-VAL-FORC-M         PIC S9(12)V9(3) COMP-3.         106  008
           10 IRE006-VAL-REM            PIC S9(12)V9(3) COMP-3.         114  008
           10 IRE006-VENTES1            PIC S9(12)V9(3) COMP-3.         122  008
            05 FILLER                      PIC X(383).                          
                                                                                
