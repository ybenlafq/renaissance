      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Résultats par Magasin                                           00000020
      ***************************************************************** 00000030
       01   EAC11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTITREI   PIC X(39).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPAGEAL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MTPAGEAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPAGEAF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MTPAGEAI  PIC X(4).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MPAGEAI   PIC X.                                          00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MTNBPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBPF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MTNBPI    PIC X(2).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNBPI     PIC X.                                          00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MDDEBUTI  PIC X(10).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MJDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJDEBUTF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MJDEBUTI  PIC X(10).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000440
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MDFINI    PIC X(10).                                      00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJFINL    COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MJFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJFINF    PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MJFINI    PIC X(10).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE1L     COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MTE1L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE1F     PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MTE1I     PIC X(28).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDDEBUTL      COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MEDDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEDDEBUTF      PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MEDDEBUTI      PIC X(10).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJDEBUTL      COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MEJDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEJDEBUTF      PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MEJDEBUTI      PIC X(10).                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE2L     COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MTE2L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE2F     PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MTE2I     PIC X(4).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDFINL   COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MEDFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDFINF   PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MEDFINI   PIC X(10).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJFINL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MEJFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEJFINF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MEJFINI   PIC X(10).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSURFACEL      COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MSURFACEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSURFACEF      PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MSURFACEI      PIC X(18).                                 00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTXCR0L  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MTTXCR0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTXCR0F  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MTTXCR0I  PIC X(4).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPANM0L  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MTPANM0L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPANM0F  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MTPANM0I  PIC X(4).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNLIEUL  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MTNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNLIEUF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MTNLIEUI  PIC X(3).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTLLIEUL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MTLLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTLLIEUF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MTLLIEUI  PIC X(7).                                       00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCATLML  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MTCATLML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCATLMF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MTCATLMI  PIC X(3).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCAELAL  COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MTCAELAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCAELAF  PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MTCAELAI  PIC X(3).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCADACL  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MTCADACL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCADACF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MTCADACI  PIC X(4).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCATOTL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MTCATOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTCATOTF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MTCATOTI  PIC X(5).                                       00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPCEL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MTNBPCEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBPCEF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MTNBPCEI  PIC X(5).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBTRNL  COMP PIC S9(4).                                 00001160
      *--                                                                       
           02 MTNBTRNL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBTRNF  PIC X.                                          00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MTNBTRNI  PIC X(5).                                       00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBENTL  COMP PIC S9(4).                                 00001200
      *--                                                                       
           02 MTNBENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTNBENTF  PIC X.                                          00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MTNBENTI  PIC X(6).                                       00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTXCR1L  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MTTXCR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTXCR1F  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MTTXCR1I  PIC X(4).                                       00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPANM1L  COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MTPANM1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPANM1F  PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MTPANM1I  PIC X(4).                                       00001310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTCAL     COMP PIC S9(4).                                 00001320
      *--                                                                       
           02 MTCAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTCAF     PIC X.                                          00001330
           02 FILLER    PIC X(4).                                       00001340
           02 MTCAI     PIC X(30).                                      00001350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBRL    COMP PIC S9(4).                                 00001360
      *--                                                                       
           02 MTNBRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBRF    PIC X.                                          00001370
           02 FILLER    PIC X(4).                                       00001380
           02 MTNBRI    PIC X(18).                                      00001390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTPL     COMP PIC S9(4).                                 00001400
      *--                                                                       
           02 MTTPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTTPF     PIC X.                                          00001410
           02 FILLER    PIC X(4).                                       00001420
           02 MTTPI     PIC X(9).                                       00001430
           02 MLIGNEI OCCURS   10 TIMES .                               00001440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001450
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001460
             03 FILLER  PIC X(4).                                       00001470
             03 MNLIEUI      PIC X(3).                                  00001480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00001490
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00001500
             03 FILLER  PIC X(4).                                       00001510
             03 MLLIEUI      PIC X(15).                                 00001520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATLML      COMP PIC S9(4).                            00001530
      *--                                                                       
             03 MCATLML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATLMF      PIC X.                                     00001540
             03 FILLER  PIC X(4).                                       00001550
             03 MCATLMI      PIC X(7).                                  00001560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAELAL      COMP PIC S9(4).                            00001570
      *--                                                                       
             03 MCAELAL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAELAF      PIC X.                                     00001580
             03 FILLER  PIC X(4).                                       00001590
             03 MCAELAI      PIC X(7).                                  00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCADACL      COMP PIC S9(4).                            00001610
      *--                                                                       
             03 MCADACL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCADACF      PIC X.                                     00001620
             03 FILLER  PIC X(4).                                       00001630
             03 MCADACI      PIC X(6).                                  00001640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATOTL      COMP PIC S9(4).                            00001650
      *--                                                                       
             03 MCATOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCATOTF      PIC X.                                     00001660
             03 FILLER  PIC X(4).                                       00001670
             03 MCATOTI      PIC X(7).                                  00001680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBPCEL      COMP PIC S9(4).                            00001690
      *--                                                                       
             03 MNBPCEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBPCEF      PIC X.                                     00001700
             03 FILLER  PIC X(4).                                       00001710
             03 MNBPCEI      PIC X(5).                                  00001720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBTRNL      COMP PIC S9(4).                            00001730
      *--                                                                       
             03 MNBTRNL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBTRNF      PIC X.                                     00001740
             03 FILLER  PIC X(4).                                       00001750
             03 MNBTRNI      PIC X(5).                                  00001760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBENTL      COMP PIC S9(4).                            00001770
      *--                                                                       
             03 MNBENTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNBENTF      PIC X.                                     00001780
             03 FILLER  PIC X(4).                                       00001790
             03 MNBENTI      PIC X(6).                                  00001800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTXCRL  COMP PIC S9(4).                                 00001810
      *--                                                                       
             03 MTXCRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTXCRF  PIC X.                                          00001820
             03 FILLER  PIC X(4).                                       00001830
             03 MTXCRI  PIC X(4).                                       00001840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPANML  COMP PIC S9(4).                                 00001850
      *--                                                                       
             03 MPANML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPANMF  PIC X.                                          00001860
             03 FILLER  PIC X(4).                                       00001870
             03 MPANMI  PIC X(4).                                       00001880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATLMTL  COMP PIC S9(4).                                 00001890
      *--                                                                       
           02 MCATLMTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCATLMTF  PIC X.                                          00001900
           02 FILLER    PIC X(4).                                       00001910
           02 MCATLMTI  PIC X(7).                                       00001920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAELATL  COMP PIC S9(4).                                 00001930
      *--                                                                       
           02 MCAELATL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAELATF  PIC X.                                          00001940
           02 FILLER    PIC X(4).                                       00001950
           02 MCAELATI  PIC X(7).                                       00001960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCADACTL  COMP PIC S9(4).                                 00001970
      *--                                                                       
           02 MCADACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCADACTF  PIC X.                                          00001980
           02 FILLER    PIC X(4).                                       00001990
           02 MCADACTI  PIC X(6).                                       00002000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATOTTL  COMP PIC S9(4).                                 00002010
      *--                                                                       
           02 MCATOTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCATOTTF  PIC X.                                          00002020
           02 FILLER    PIC X(4).                                       00002030
           02 MCATOTTI  PIC X(7).                                       00002040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPCETL  COMP PIC S9(4).                                 00002050
      *--                                                                       
           02 MNBPCETL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPCETF  PIC X.                                          00002060
           02 FILLER    PIC X(4).                                       00002070
           02 MNBPCETI  PIC X(5).                                       00002080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTRNTL  COMP PIC S9(4).                                 00002090
      *--                                                                       
           02 MNBTRNTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBTRNTF  PIC X.                                          00002100
           02 FILLER    PIC X(4).                                       00002110
           02 MNBTRNTI  PIC X(5).                                       00002120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBENTTL  COMP PIC S9(4).                                 00002130
      *--                                                                       
           02 MNBENTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBENTTF  PIC X.                                          00002140
           02 FILLER    PIC X(4).                                       00002150
           02 MNBENTTI  PIC X(6).                                       00002160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTXCRTL   COMP PIC S9(4).                                 00002170
      *--                                                                       
           02 MTXCRTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTXCRTF   PIC X.                                          00002180
           02 FILLER    PIC X(4).                                       00002190
           02 MTXCRTI   PIC X(4).                                       00002200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPANMTL   COMP PIC S9(4).                                 00002210
      *--                                                                       
           02 MPANMTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPANMTF   PIC X.                                          00002220
           02 FILLER    PIC X(4).                                       00002230
           02 MPANMTI   PIC X(4).                                       00002240
           02 MFD OCCURS   3 TIMES .                                    00002250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MFL     COMP PIC S9(4).                                 00002260
      *--                                                                       
             03 MFL COMP-5 PIC S9(4).                                           
      *}                                                                        
             03 MFF     PIC X.                                          00002270
             03 FILLER  PIC X(4).                                       00002280
             03 MFI     PIC X(79).                                      00002290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002300
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002310
           02 FILLER    PIC X(4).                                       00002320
           02 MLIBERRI  PIC X(79).                                      00002330
      * CODE TRANSACTION                                                00002340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002360
           02 FILLER    PIC X(4).                                       00002370
           02 MCODTRAI  PIC X(4).                                       00002380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00002390
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00002400
           02 FILLER    PIC X(4).                                       00002410
           02 MZONCMDI  PIC X(15).                                      00002420
      * NOM DU CICS                                                     00002430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002440
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002450
           02 FILLER    PIC X(4).                                       00002460
           02 MCICSI    PIC X(5).                                       00002470
      * NETNAME                                                         00002480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002490
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002500
           02 FILLER    PIC X(4).                                       00002510
           02 MNETNAMI  PIC X(8).                                       00002520
      * CODE TERMINAL                                                   00002530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002540
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002550
           02 FILLER    PIC X(4).                                       00002560
           02 MSCREENI  PIC X(4).                                       00002570
      ***************************************************************** 00002580
      * Résultats par Magasin                                           00002590
      ***************************************************************** 00002600
       01   EAC11O REDEFINES EAC11I.                                    00002610
           02 FILLER    PIC X(12).                                      00002620
      * DATE DU JOUR                                                    00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MDATJOUA  PIC X.                                          00002650
           02 MDATJOUC  PIC X.                                          00002660
           02 MDATJOUP  PIC X.                                          00002670
           02 MDATJOUH  PIC X.                                          00002680
           02 MDATJOUV  PIC X.                                          00002690
           02 MDATJOUO  PIC X(10).                                      00002700
      * HEURE                                                           00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MTIMJOUA  PIC X.                                          00002730
           02 MTIMJOUC  PIC X.                                          00002740
           02 MTIMJOUP  PIC X.                                          00002750
           02 MTIMJOUH  PIC X.                                          00002760
           02 MTIMJOUV  PIC X.                                          00002770
           02 MTIMJOUO  PIC X(5).                                       00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MTITREA   PIC X.                                          00002800
           02 MTITREC   PIC X.                                          00002810
           02 MTITREP   PIC X.                                          00002820
           02 MTITREH   PIC X.                                          00002830
           02 MTITREV   PIC X.                                          00002840
           02 MTITREO   PIC X(39).                                      00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MTPAGEAA  PIC X.                                          00002870
           02 MTPAGEAC  PIC X.                                          00002880
           02 MTPAGEAP  PIC X.                                          00002890
           02 MTPAGEAH  PIC X.                                          00002900
           02 MTPAGEAV  PIC X.                                          00002910
           02 MTPAGEAO  PIC X(4).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MPAGEAA   PIC X.                                          00002940
           02 MPAGEAC   PIC X.                                          00002950
           02 MPAGEAP   PIC X.                                          00002960
           02 MPAGEAH   PIC X.                                          00002970
           02 MPAGEAV   PIC X.                                          00002980
           02 MPAGEAO   PIC 9.                                          00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MTNBPA    PIC X.                                          00003010
           02 MTNBPC    PIC X.                                          00003020
           02 MTNBPP    PIC X.                                          00003030
           02 MTNBPH    PIC X.                                          00003040
           02 MTNBPV    PIC X.                                          00003050
           02 MTNBPO    PIC X(2).                                       00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MNBPA     PIC X.                                          00003080
           02 MNBPC     PIC X.                                          00003090
           02 MNBPP     PIC X.                                          00003100
           02 MNBPH     PIC X.                                          00003110
           02 MNBPV     PIC X.                                          00003120
           02 MNBPO     PIC 9.                                          00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MDDEBUTA  PIC X.                                          00003150
           02 MDDEBUTC  PIC X.                                          00003160
           02 MDDEBUTP  PIC X.                                          00003170
           02 MDDEBUTH  PIC X.                                          00003180
           02 MDDEBUTV  PIC X.                                          00003190
           02 MDDEBUTO  PIC X(10).                                      00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MJDEBUTA  PIC X.                                          00003220
           02 MJDEBUTC  PIC X.                                          00003230
           02 MJDEBUTP  PIC X.                                          00003240
           02 MJDEBUTH  PIC X.                                          00003250
           02 MJDEBUTV  PIC X.                                          00003260
           02 MJDEBUTO  PIC X(10).                                      00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MDFINA    PIC X.                                          00003290
           02 MDFINC    PIC X.                                          00003300
           02 MDFINP    PIC X.                                          00003310
           02 MDFINH    PIC X.                                          00003320
           02 MDFINV    PIC X.                                          00003330
           02 MDFINO    PIC X(10).                                      00003340
           02 FILLER    PIC X(2).                                       00003350
           02 MJFINA    PIC X.                                          00003360
           02 MJFINC    PIC X.                                          00003370
           02 MJFINP    PIC X.                                          00003380
           02 MJFINH    PIC X.                                          00003390
           02 MJFINV    PIC X.                                          00003400
           02 MJFINO    PIC X(10).                                      00003410
           02 FILLER    PIC X(2).                                       00003420
           02 MTE1A     PIC X.                                          00003430
           02 MTE1C     PIC X.                                          00003440
           02 MTE1P     PIC X.                                          00003450
           02 MTE1H     PIC X.                                          00003460
           02 MTE1V     PIC X.                                          00003470
           02 MTE1O     PIC X(28).                                      00003480
           02 FILLER    PIC X(2).                                       00003490
           02 MEDDEBUTA      PIC X.                                     00003500
           02 MEDDEBUTC PIC X.                                          00003510
           02 MEDDEBUTP PIC X.                                          00003520
           02 MEDDEBUTH PIC X.                                          00003530
           02 MEDDEBUTV PIC X.                                          00003540
           02 MEDDEBUTO      PIC X(10).                                 00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MEJDEBUTA      PIC X.                                     00003570
           02 MEJDEBUTC PIC X.                                          00003580
           02 MEJDEBUTP PIC X.                                          00003590
           02 MEJDEBUTH PIC X.                                          00003600
           02 MEJDEBUTV PIC X.                                          00003610
           02 MEJDEBUTO      PIC X(10).                                 00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MTE2A     PIC X.                                          00003640
           02 MTE2C     PIC X.                                          00003650
           02 MTE2P     PIC X.                                          00003660
           02 MTE2H     PIC X.                                          00003670
           02 MTE2V     PIC X.                                          00003680
           02 MTE2O     PIC X(4).                                       00003690
           02 FILLER    PIC X(2).                                       00003700
           02 MEDFINA   PIC X.                                          00003710
           02 MEDFINC   PIC X.                                          00003720
           02 MEDFINP   PIC X.                                          00003730
           02 MEDFINH   PIC X.                                          00003740
           02 MEDFINV   PIC X.                                          00003750
           02 MEDFINO   PIC X(10).                                      00003760
           02 FILLER    PIC X(2).                                       00003770
           02 MEJFINA   PIC X.                                          00003780
           02 MEJFINC   PIC X.                                          00003790
           02 MEJFINP   PIC X.                                          00003800
           02 MEJFINH   PIC X.                                          00003810
           02 MEJFINV   PIC X.                                          00003820
           02 MEJFINO   PIC X(10).                                      00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MSURFACEA      PIC X.                                     00003850
           02 MSURFACEC PIC X.                                          00003860
           02 MSURFACEP PIC X.                                          00003870
           02 MSURFACEH PIC X.                                          00003880
           02 MSURFACEV PIC X.                                          00003890
           02 MSURFACEO      PIC X(18).                                 00003900
           02 FILLER    PIC X(2).                                       00003910
           02 MTTXCR0A  PIC X.                                          00003920
           02 MTTXCR0C  PIC X.                                          00003930
           02 MTTXCR0P  PIC X.                                          00003940
           02 MTTXCR0H  PIC X.                                          00003950
           02 MTTXCR0V  PIC X.                                          00003960
           02 MTTXCR0O  PIC X(4).                                       00003970
           02 FILLER    PIC X(2).                                       00003980
           02 MTPANM0A  PIC X.                                          00003990
           02 MTPANM0C  PIC X.                                          00004000
           02 MTPANM0P  PIC X.                                          00004010
           02 MTPANM0H  PIC X.                                          00004020
           02 MTPANM0V  PIC X.                                          00004030
           02 MTPANM0O  PIC X(4).                                       00004040
           02 FILLER    PIC X(2).                                       00004050
           02 MTNLIEUA  PIC X.                                          00004060
           02 MTNLIEUC  PIC X.                                          00004070
           02 MTNLIEUP  PIC X.                                          00004080
           02 MTNLIEUH  PIC X.                                          00004090
           02 MTNLIEUV  PIC X.                                          00004100
           02 MTNLIEUO  PIC X(3).                                       00004110
           02 FILLER    PIC X(2).                                       00004120
           02 MTLLIEUA  PIC X.                                          00004130
           02 MTLLIEUC  PIC X.                                          00004140
           02 MTLLIEUP  PIC X.                                          00004150
           02 MTLLIEUH  PIC X.                                          00004160
           02 MTLLIEUV  PIC X.                                          00004170
           02 MTLLIEUO  PIC X(7).                                       00004180
           02 FILLER    PIC X(2).                                       00004190
           02 MTCATLMA  PIC X.                                          00004200
           02 MTCATLMC  PIC X.                                          00004210
           02 MTCATLMP  PIC X.                                          00004220
           02 MTCATLMH  PIC X.                                          00004230
           02 MTCATLMV  PIC X.                                          00004240
           02 MTCATLMO  PIC X(3).                                       00004250
           02 FILLER    PIC X(2).                                       00004260
           02 MTCAELAA  PIC X.                                          00004270
           02 MTCAELAC  PIC X.                                          00004280
           02 MTCAELAP  PIC X.                                          00004290
           02 MTCAELAH  PIC X.                                          00004300
           02 MTCAELAV  PIC X.                                          00004310
           02 MTCAELAO  PIC X(3).                                       00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MTCADACA  PIC X.                                          00004340
           02 MTCADACC  PIC X.                                          00004350
           02 MTCADACP  PIC X.                                          00004360
           02 MTCADACH  PIC X.                                          00004370
           02 MTCADACV  PIC X.                                          00004380
           02 MTCADACO  PIC X(4).                                       00004390
           02 FILLER    PIC X(2).                                       00004400
           02 MTCATOTA  PIC X.                                          00004410
           02 MTCATOTC  PIC X.                                          00004420
           02 MTCATOTP  PIC X.                                          00004430
           02 MTCATOTH  PIC X.                                          00004440
           02 MTCATOTV  PIC X.                                          00004450
           02 MTCATOTO  PIC X(5).                                       00004460
           02 FILLER    PIC X(2).                                       00004470
           02 MTNBPCEA  PIC X.                                          00004480
           02 MTNBPCEC  PIC X.                                          00004490
           02 MTNBPCEP  PIC X.                                          00004500
           02 MTNBPCEH  PIC X.                                          00004510
           02 MTNBPCEV  PIC X.                                          00004520
           02 MTNBPCEO  PIC X(5).                                       00004530
           02 FILLER    PIC X(2).                                       00004540
           02 MTNBTRNA  PIC X.                                          00004550
           02 MTNBTRNC  PIC X.                                          00004560
           02 MTNBTRNP  PIC X.                                          00004570
           02 MTNBTRNH  PIC X.                                          00004580
           02 MTNBTRNV  PIC X.                                          00004590
           02 MTNBTRNO  PIC X(5).                                       00004600
           02 FILLER    PIC X(2).                                       00004610
           02 MTNBENTA  PIC X.                                          00004620
           02 MTNBENTC  PIC X.                                          00004630
           02 MTNBENTP  PIC X.                                          00004640
           02 MTNBENTH  PIC X.                                          00004650
           02 MTNBENTV  PIC X.                                          00004660
           02 MTNBENTO  PIC X(6).                                       00004670
           02 FILLER    PIC X(2).                                       00004680
           02 MTTXCR1A  PIC X.                                          00004690
           02 MTTXCR1C  PIC X.                                          00004700
           02 MTTXCR1P  PIC X.                                          00004710
           02 MTTXCR1H  PIC X.                                          00004720
           02 MTTXCR1V  PIC X.                                          00004730
           02 MTTXCR1O  PIC X(4).                                       00004740
           02 FILLER    PIC X(2).                                       00004750
           02 MTPANM1A  PIC X.                                          00004760
           02 MTPANM1C  PIC X.                                          00004770
           02 MTPANM1P  PIC X.                                          00004780
           02 MTPANM1H  PIC X.                                          00004790
           02 MTPANM1V  PIC X.                                          00004800
           02 MTPANM1O  PIC X(4).                                       00004810
           02 FILLER    PIC X(2).                                       00004820
           02 MTCAA     PIC X.                                          00004830
           02 MTCAC     PIC X.                                          00004840
           02 MTCAP     PIC X.                                          00004850
           02 MTCAH     PIC X.                                          00004860
           02 MTCAV     PIC X.                                          00004870
           02 MTCAO     PIC X(30).                                      00004880
           02 FILLER    PIC X(2).                                       00004890
           02 MTNBRA    PIC X.                                          00004900
           02 MTNBRC    PIC X.                                          00004910
           02 MTNBRP    PIC X.                                          00004920
           02 MTNBRH    PIC X.                                          00004930
           02 MTNBRV    PIC X.                                          00004940
           02 MTNBRO    PIC X(18).                                      00004950
           02 FILLER    PIC X(2).                                       00004960
           02 MTTPA     PIC X.                                          00004970
           02 MTTPC     PIC X.                                          00004980
           02 MTTPP     PIC X.                                          00004990
           02 MTTPH     PIC X.                                          00005000
           02 MTTPV     PIC X.                                          00005010
           02 MTTPO     PIC X(9).                                       00005020
           02 MLIGNEO OCCURS   10 TIMES .                               00005030
             03 FILLER       PIC X(2).                                  00005040
             03 MNLIEUA      PIC X.                                     00005050
             03 MNLIEUC PIC X.                                          00005060
             03 MNLIEUP PIC X.                                          00005070
             03 MNLIEUH PIC X.                                          00005080
             03 MNLIEUV PIC X.                                          00005090
             03 MNLIEUO      PIC X(3).                                  00005100
             03 FILLER       PIC X(2).                                  00005110
             03 MLLIEUA      PIC X.                                     00005120
             03 MLLIEUC PIC X.                                          00005130
             03 MLLIEUP PIC X.                                          00005140
             03 MLLIEUH PIC X.                                          00005150
             03 MLLIEUV PIC X.                                          00005160
             03 MLLIEUO      PIC X(15).                                 00005170
             03 FILLER       PIC X(2).                                  00005180
             03 MCATLMA      PIC X.                                     00005190
             03 MCATLMC PIC X.                                          00005200
             03 MCATLMP PIC X.                                          00005210
             03 MCATLMH PIC X.                                          00005220
             03 MCATLMV PIC X.                                          00005230
             03 MCATLMO      PIC ------9.                               00005240
             03 FILLER       PIC X(2).                                  00005250
             03 MCAELAA      PIC X.                                     00005260
             03 MCAELAC PIC X.                                          00005270
             03 MCAELAP PIC X.                                          00005280
             03 MCAELAH PIC X.                                          00005290
             03 MCAELAV PIC X.                                          00005300
             03 MCAELAO      PIC ------9.                               00005310
             03 FILLER       PIC X(2).                                  00005320
             03 MCADACA      PIC X.                                     00005330
             03 MCADACC PIC X.                                          00005340
             03 MCADACP PIC X.                                          00005350
             03 MCADACH PIC X.                                          00005360
             03 MCADACV PIC X.                                          00005370
             03 MCADACO      PIC -----9.                                00005380
             03 FILLER       PIC X(2).                                  00005390
             03 MCATOTA      PIC X.                                     00005400
             03 MCATOTC PIC X.                                          00005410
             03 MCATOTP PIC X.                                          00005420
             03 MCATOTH PIC X.                                          00005430
             03 MCATOTV PIC X.                                          00005440
             03 MCATOTO      PIC ------9.                               00005450
             03 FILLER       PIC X(2).                                  00005460
             03 MNBPCEA      PIC X.                                     00005470
             03 MNBPCEC PIC X.                                          00005480
             03 MNBPCEP PIC X.                                          00005490
             03 MNBPCEH PIC X.                                          00005500
             03 MNBPCEV PIC X.                                          00005510
             03 MNBPCEO      PIC ----9.                                 00005520
             03 FILLER       PIC X(2).                                  00005530
             03 MNBTRNA      PIC X.                                     00005540
             03 MNBTRNC PIC X.                                          00005550
             03 MNBTRNP PIC X.                                          00005560
             03 MNBTRNH PIC X.                                          00005570
             03 MNBTRNV PIC X.                                          00005580
             03 MNBTRNO      PIC ----9.                                 00005590
             03 FILLER       PIC X(2).                                  00005600
             03 MNBENTA      PIC X.                                     00005610
             03 MNBENTC PIC X.                                          00005620
             03 MNBENTP PIC X.                                          00005630
             03 MNBENTH PIC X.                                          00005640
             03 MNBENTV PIC X.                                          00005650
             03 MNBENTO      PIC -----9.                                00005660
             03 FILLER       PIC X(2).                                  00005670
             03 MTXCRA  PIC X.                                          00005680
             03 MTXCRC  PIC X.                                          00005690
             03 MTXCRP  PIC X.                                          00005700
             03 MTXCRH  PIC X.                                          00005710
             03 MTXCRV  PIC X.                                          00005720
             03 MTXCRO  PIC Z9,9.                                       00005730
             03 FILLER       PIC X(2).                                  00005740
             03 MPANMA  PIC X.                                          00005750
             03 MPANMC  PIC X.                                          00005760
             03 MPANMP  PIC X.                                          00005770
             03 MPANMH  PIC X.                                          00005780
             03 MPANMV  PIC X.                                          00005790
             03 MPANMO  PIC ---9.                                       00005800
           02 FILLER    PIC X(2).                                       00005810
           02 MCATLMTA  PIC X.                                          00005820
           02 MCATLMTC  PIC X.                                          00005830
           02 MCATLMTP  PIC X.                                          00005840
           02 MCATLMTH  PIC X.                                          00005850
           02 MCATLMTV  PIC X.                                          00005860
           02 MCATLMTO  PIC ------9.                                    00005870
           02 FILLER    PIC X(2).                                       00005880
           02 MCAELATA  PIC X.                                          00005890
           02 MCAELATC  PIC X.                                          00005900
           02 MCAELATP  PIC X.                                          00005910
           02 MCAELATH  PIC X.                                          00005920
           02 MCAELATV  PIC X.                                          00005930
           02 MCAELATO  PIC ------9.                                    00005940
           02 FILLER    PIC X(2).                                       00005950
           02 MCADACTA  PIC X.                                          00005960
           02 MCADACTC  PIC X.                                          00005970
           02 MCADACTP  PIC X.                                          00005980
           02 MCADACTH  PIC X.                                          00005990
           02 MCADACTV  PIC X.                                          00006000
           02 MCADACTO  PIC -----9.                                     00006010
           02 FILLER    PIC X(2).                                       00006020
           02 MCATOTTA  PIC X.                                          00006030
           02 MCATOTTC  PIC X.                                          00006040
           02 MCATOTTP  PIC X.                                          00006050
           02 MCATOTTH  PIC X.                                          00006060
           02 MCATOTTV  PIC X.                                          00006070
           02 MCATOTTO  PIC ------9.                                    00006080
           02 FILLER    PIC X(2).                                       00006090
           02 MNBPCETA  PIC X.                                          00006100
           02 MNBPCETC  PIC X.                                          00006110
           02 MNBPCETP  PIC X.                                          00006120
           02 MNBPCETH  PIC X.                                          00006130
           02 MNBPCETV  PIC X.                                          00006140
           02 MNBPCETO  PIC ----9.                                      00006150
           02 FILLER    PIC X(2).                                       00006160
           02 MNBTRNTA  PIC X.                                          00006170
           02 MNBTRNTC  PIC X.                                          00006180
           02 MNBTRNTP  PIC X.                                          00006190
           02 MNBTRNTH  PIC X.                                          00006200
           02 MNBTRNTV  PIC X.                                          00006210
           02 MNBTRNTO  PIC ----9.                                      00006220
           02 FILLER    PIC X(2).                                       00006230
           02 MNBENTTA  PIC X.                                          00006240
           02 MNBENTTC  PIC X.                                          00006250
           02 MNBENTTP  PIC X.                                          00006260
           02 MNBENTTH  PIC X.                                          00006270
           02 MNBENTTV  PIC X.                                          00006280
           02 MNBENTTO  PIC -----9.                                     00006290
           02 FILLER    PIC X(2).                                       00006300
           02 MTXCRTA   PIC X.                                          00006310
           02 MTXCRTC   PIC X.                                          00006320
           02 MTXCRTP   PIC X.                                          00006330
           02 MTXCRTH   PIC X.                                          00006340
           02 MTXCRTV   PIC X.                                          00006350
           02 MTXCRTO   PIC Z9,9.                                       00006360
           02 FILLER    PIC X(2).                                       00006370
           02 MPANMTA   PIC X.                                          00006380
           02 MPANMTC   PIC X.                                          00006390
           02 MPANMTP   PIC X.                                          00006400
           02 MPANMTH   PIC X.                                          00006410
           02 MPANMTV   PIC X.                                          00006420
           02 MPANMTO   PIC ---9.                                       00006430
           02 DFHMS1 OCCURS   3 TIMES .                                 00006440
             03 FILLER       PIC X(2).                                  00006450
             03 MFA     PIC X.                                          00006460
             03 MFC     PIC X.                                          00006470
             03 MFP     PIC X.                                          00006480
             03 MFH     PIC X.                                          00006490
             03 MFV     PIC X.                                          00006500
             03 MFO     PIC X(79).                                      00006510
           02 FILLER    PIC X(2).                                       00006520
           02 MLIBERRA  PIC X.                                          00006530
           02 MLIBERRC  PIC X.                                          00006540
           02 MLIBERRP  PIC X.                                          00006550
           02 MLIBERRH  PIC X.                                          00006560
           02 MLIBERRV  PIC X.                                          00006570
           02 MLIBERRO  PIC X(79).                                      00006580
      * CODE TRANSACTION                                                00006590
           02 FILLER    PIC X(2).                                       00006600
           02 MCODTRAA  PIC X.                                          00006610
           02 MCODTRAC  PIC X.                                          00006620
           02 MCODTRAP  PIC X.                                          00006630
           02 MCODTRAH  PIC X.                                          00006640
           02 MCODTRAV  PIC X.                                          00006650
           02 MCODTRAO  PIC X(4).                                       00006660
           02 FILLER    PIC X(2).                                       00006670
           02 MZONCMDA  PIC X.                                          00006680
           02 MZONCMDC  PIC X.                                          00006690
           02 MZONCMDP  PIC X.                                          00006700
           02 MZONCMDH  PIC X.                                          00006710
           02 MZONCMDV  PIC X.                                          00006720
           02 MZONCMDO  PIC X(15).                                      00006730
      * NOM DU CICS                                                     00006740
           02 FILLER    PIC X(2).                                       00006750
           02 MCICSA    PIC X.                                          00006760
           02 MCICSC    PIC X.                                          00006770
           02 MCICSP    PIC X.                                          00006780
           02 MCICSH    PIC X.                                          00006790
           02 MCICSV    PIC X.                                          00006800
           02 MCICSO    PIC X(5).                                       00006810
      * NETNAME                                                         00006820
           02 FILLER    PIC X(2).                                       00006830
           02 MNETNAMA  PIC X.                                          00006840
           02 MNETNAMC  PIC X.                                          00006850
           02 MNETNAMP  PIC X.                                          00006860
           02 MNETNAMH  PIC X.                                          00006870
           02 MNETNAMV  PIC X.                                          00006880
           02 MNETNAMO  PIC X(8).                                       00006890
      * CODE TERMINAL                                                   00006900
           02 FILLER    PIC X(2).                                       00006910
           02 MSCREENA  PIC X.                                          00006920
           02 MSCREENC  PIC X.                                          00006930
           02 MSCREENP  PIC X.                                          00006940
           02 MSCREENH  PIC X.                                          00006950
           02 MSCREENV  PIC X.                                          00006960
           02 MSCREENO  PIC X(4).                                       00006970
                                                                                
