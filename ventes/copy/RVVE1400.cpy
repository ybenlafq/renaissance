      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                        00010005
      **********************************************************        00020005
      *   COPY DE LA TABLE RVVE1400                                     00030006
      **********************************************************        00040005
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVE1400                 00050006
      **********************************************************        00060005
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1400.                                                    00070006
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1400.                                                            
      *}                                                                        
           02  VE14-NSOCIETE                                            00080005
               PIC X(0003).                                             00090005
           02  VE14-NLIEU                                               00100005
               PIC X(0003).                                             00110005
           02  VE14-NORDRE                                              00120005
               PIC X(0005).                                             00130005
           02  VE14-NVENTE                                              00140005
               PIC X(0007).                                             00150005
           02  VE14-DSAISIE                                             00160005
               PIC X(0008).                                             00170005
           02  VE14-NSEQ                                                00180005
               PIC X(0002).                                             00190005
           02  VE14-CMODPAIMT                                           00200005
               PIC X(0005).                                             00210005
           02  VE14-PREGLTVTE                                           00220005
               PIC S9(7)V9(0002) COMP-3.                                00230005
           02  VE14-NREGLTVTE                                           00240005
               PIC X(0007).                                             00250005
           02  VE14-DREGLTVTE                                           00260005
               PIC X(0008).                                             00270005
           02  VE14-WREGLTVTE                                           00280005
               PIC X(0001).                                             00290005
           02  VE14-DCOMPTA                                             00300005
               PIC X(0008).                                             00310005
           02  VE14-DSYST                                               00320005
               PIC S9(13) COMP-3.                                       00330005
           02  VE14-CPROTOUR                                            00340005
               PIC X(0005).                                             00350005
           02  VE14-CTOURNEE                                            00360005
               PIC X(0003).                                             00370005
           02  VE14-CDEVISE                                             00380005
               PIC X(0003).                                             00390005
           02  VE14-MDEVISE                                             00400005
               PIC S9(7)V9(0002) COMP-3.                                00410005
           02  VE14-MECART                                              00420005
               PIC S9V9(0002) COMP-3.                                   00430005
           02  VE14-CDEV                                                00440005
               PIC X(0003).                                             00450005
           02  VE14-NSOCP                                               00460005
               PIC X(0003).                                             00470005
           02  VE14-NLIEUP                                              00480005
               PIC X(0003).                                             00490005
           02  VE14-NTRANS                                              00500005
               PIC S9(8) COMP-3.                                        00510005
           02  VE14-PMODPAIMT                                           00520005
               PIC S9(7)V9(0002) COMP-3.                                00530005
           02  VE14-CVENDEUR                                            00540005
               PIC X(6).                                                00550005
           02  VE14-CFCRED                                              00560005
               PIC X(5).                                                00570005
           02  VE14-NCREDI                                              00580005
               PIC X(14).                                               00590005
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00600005
      *   LISTE DES FLAGS DE LA TABLE RVVE1400                          00610006
      **********************************************************        00620005
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVVE1400-FLAGS.                                              00630006
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVVE1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NSOCIETE-F                                          00640005
      *        PIC S9(4) COMP.                                          00650005
      *--                                                                       
           02  VE14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NLIEU-F                                             00660005
      *        PIC S9(4) COMP.                                          00670005
      *--                                                                       
           02  VE14-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NORDRE-F                                            00680005
      *        PIC S9(4) COMP.                                          00690005
      *--                                                                       
           02  VE14-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NVENTE-F                                            00700005
      *        PIC S9(4) COMP.                                          00710005
      *--                                                                       
           02  VE14-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-DSAISIE-F                                           00720005
      *        PIC S9(4) COMP.                                          00730005
      *--                                                                       
           02  VE14-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NSEQ-F                                              00740005
      *        PIC S9(4) COMP.                                          00750005
      *--                                                                       
           02  VE14-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CMODPAIMT-F                                         00760005
      *        PIC S9(4) COMP.                                          00770005
      *--                                                                       
           02  VE14-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-PREGLTVTE-F                                         00780005
      *        PIC S9(4) COMP.                                          00790005
      *--                                                                       
           02  VE14-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NREGLTVTE-F                                         00800005
      *        PIC S9(4) COMP.                                          00810005
      *--                                                                       
           02  VE14-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-DREGLTVTE-F                                         00820005
      *        PIC S9(4) COMP.                                          00830005
      *--                                                                       
           02  VE14-DREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-WREGLTVTE-F                                         00840005
      *        PIC S9(4) COMP.                                          00850005
      *--                                                                       
           02  VE14-WREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-DCOMPTA-F                                           00860005
      *        PIC S9(4) COMP.                                          00870005
      *--                                                                       
           02  VE14-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-DSYST-F                                             00880005
      *        PIC S9(4) COMP.                                          00890005
      *--                                                                       
           02  VE14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CPROTOUR-F                                          00900005
      *        PIC S9(4) COMP.                                          00910005
      *--                                                                       
           02  VE14-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CTOURNEE-F                                          00920005
      *        PIC S9(4) COMP.                                          00930005
      *--                                                                       
           02  VE14-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CDEVISE-F                                           00940005
      *        PIC S9(4) COMP.                                          00950005
      *--                                                                       
           02  VE14-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-MDEVISE-F                                           00960005
      *        PIC S9(4) COMP.                                          00970005
      *--                                                                       
           02  VE14-MDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-MECART-F                                            00980005
      *        PIC S9(4) COMP.                                          00990005
      *--                                                                       
           02  VE14-MECART-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CDEV-F                                              01000005
      *        PIC S9(4) COMP.                                          01010005
      *--                                                                       
           02  VE14-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NSOCP-F                                             01020005
      *        PIC S9(4) COMP.                                          01030005
      *--                                                                       
           02  VE14-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NLIEUP-F                                            01040005
      *        PIC S9(4) COMP.                                          01050005
      *--                                                                       
           02  VE14-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NTRANS-F                                            01060005
      *        PIC S9(4) COMP.                                          01070005
      *--                                                                       
           02  VE14-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-PMODPAIMT-F                                         01080005
      *        PIC S9(4) COMP.                                          01090005
      *--                                                                       
           02  VE14-PMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CVENDEUR-F                                          01100005
      *        PIC S9(4) COMP.                                          01110005
      *--                                                                       
           02  VE14-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-CFCRED-F                                            01120005
      *        PIC S9(4) COMP.                                          01130005
      *--                                                                       
           02  VE14-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE14-NCREDI-F                                            01140005
      *        PIC S9(4) COMP.                                          01150005
      *                                                                         
      *--                                                                       
           02  VE14-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
