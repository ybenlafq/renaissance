      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV2900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV2900                         
      **********************************************************                
       01  RVHV2900.                                                            
           02  HV29-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV29-WSEQPRO                                                     
               PIC S9(7) COMP-3.                                                
           02  HV29-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV29-DVENTE                                                      
               PIC X(0006).                                                     
           02  HV29-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV29-PCA                                                         
               PIC S9(9) COMP-3.                                                
           02  HV29-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV2900                                  
      **********************************************************                
       01  RVHV2900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-WSEQPRO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-WSEQPRO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV29-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV29-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV29-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
