      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EVT44   EVT44                                              00000020
      ***************************************************************** 00000030
       01   EVT44I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDVENTEI  PIC X(8).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMODIFVTEL    COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MDMODIFVTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDMODIFVTEF    PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDMODIFVTEI    PIC X(8).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTTVENTEL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPTTVENTEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPTTVENTEF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPTTVENTEI     PIC X(9).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPCOMPTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MPCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPCOMPTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MPCOMPTI  PIC X(9).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPLIVRL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MPLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPLIVRF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPLIVRI   PIC X(9).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPDIFFEREL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MPDIFFEREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPDIFFEREF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPDIFFEREI     PIC X(9).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPRFACTL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MPRFACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPRFACTF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MPRFACTI  PIC X(9).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUTORL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNAUTORL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAUTORF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNAUTORI  PIC X(20).                                      00000570
           02 MTABLEI OCCURS   7 TIMES .                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDSAISIEL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MDSAISIEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MDSAISIEF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MDSAISIEI    PIC X(8).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLMODPAIMTL  COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLMODPAIMTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLMODPAIMTF  PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLMODPAIMTI  PIC X(10).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMODPAIMTL  COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCMODPAIMTL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MCMODPAIMTF  PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCMODPAIMTI  PIC X(3).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPREGLTVTEL  COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MPREGLTVTEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MPREGLTVTEF  PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MPREGLTVTEI  PIC X(9).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNREGLTVTEL  COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MNREGLTVTEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNREGLTVTEF  PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MNREGLTVTEI  PIC X(7).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUTRANL  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNLIEUTRANL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MNLIEUTRANF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNLIEUTRANI  PIC X(16).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCORGOREDL     COMP PIC S9(4).                            00000830
      *--                                                                       
           02 MCORGOREDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCORGOREDF     PIC X.                                     00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCORGOREDI     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORGOREDL     COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MLORGOREDL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLORGOREDF     PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLORGOREDI     PIC X(15).                                 00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWFACTUREL     COMP PIC S9(4).                            00000910
      *--                                                                       
           02 MWFACTUREL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWFACTUREF     PIC X.                                     00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MWFACTUREI     PIC X.                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDETAXECL     COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MWDETAXECL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWDETAXECF     PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MWDETAXECI     PIC X.                                     00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVA1L   COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCTVA1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTVA1F   PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCTVA1I   PIC X(5).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTVA1L   COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MPTVA1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPTVA1F   PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MPTVA1I   PIC X(3).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFCREDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MCFCREDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFCREDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MCFCREDI  PIC X(5).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCREDIL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MNCREDIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCREDIF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MNCREDII  PIC X(14).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEXPORTL      COMP PIC S9(4).                            00001150
      *--                                                                       
           02 MWEXPORTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWEXPORTF      PIC X.                                     00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MWEXPORTI      PIC X.                                     00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWBLSPL   COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MWBLSPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWBLSPF   PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MWBLSPI   PIC X.                                          00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVA2L   COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MCTVA2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTVA2F   PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MCTVA2I   PIC X(5).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTVA2L   COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MPTVA2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPTVA2F   PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MPTVA2I   PIC X(3).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODPAIMTVTEL      COMP PIC S9(4).                       00001310
      *--                                                                       
           02 MCMODPAIMTVTEL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MCMODPAIMTVTEF      PIC X.                                00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCMODPAIMTVTEI      PIC X(5).                             00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMODPAIMTVTEL      COMP PIC S9(4).                       00001350
      *--                                                                       
           02 MLMODPAIMTVTEL COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MLMODPAIMTVTEF      PIC X.                                00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLMODPAIMTVTEI      PIC X(15).                            00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVA3L   COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCTVA3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTVA3F   PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCTVA3I   PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPTVA3L   COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MPTVA3L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPTVA3F   PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MPTVA3I   PIC X(9).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDESCRIPTIF1L      COMP PIC S9(4).                       00001470
      *--                                                                       
           02 MLDESCRIPTIF1L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MLDESCRIPTIF1F      PIC X.                                00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MLDESCRIPTIF1I      PIC X(30).                            00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDESCRIPTIF2L      COMP PIC S9(4).                       00001510
      *--                                                                       
           02 MLDESCRIPTIF2L COMP-5 PIC S9(4).                                  
      *}                                                                        
           02 MLDESCRIPTIF2F      PIC X.                                00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MLDESCRIPTIF2I      PIC X(30).                            00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MZONCMDI  PIC X(15).                                      00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MLIBERRI  PIC X(58).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MNETNAMI  PIC X(8).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MSCREENI  PIC X(4).                                       00001780
      ***************************************************************** 00001790
      * SDF: EVT44   EVT44                                              00001800
      ***************************************************************** 00001810
       01   EVT44O REDEFINES EVT44I.                                    00001820
           02 FILLER    PIC X(12).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTIMJOUA  PIC X.                                          00001920
           02 MTIMJOUC  PIC X.                                          00001930
           02 MTIMJOUP  PIC X.                                          00001940
           02 MTIMJOUH  PIC X.                                          00001950
           02 MTIMJOUV  PIC X.                                          00001960
           02 MTIMJOUO  PIC X(5).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MNPAGEA   PIC X.                                          00001990
           02 MNPAGEC   PIC X.                                          00002000
           02 MNPAGEP   PIC X.                                          00002010
           02 MNPAGEH   PIC X.                                          00002020
           02 MNPAGEV   PIC X.                                          00002030
           02 MNPAGEO   PIC X(2).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNLIEUA   PIC X.                                          00002060
           02 MNLIEUC   PIC X.                                          00002070
           02 MNLIEUP   PIC X.                                          00002080
           02 MNLIEUH   PIC X.                                          00002090
           02 MNLIEUV   PIC X.                                          00002100
           02 MNLIEUO   PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNVENTEA  PIC X.                                          00002130
           02 MNVENTEC  PIC X.                                          00002140
           02 MNVENTEP  PIC X.                                          00002150
           02 MNVENTEH  PIC X.                                          00002160
           02 MNVENTEV  PIC X.                                          00002170
           02 MNVENTEO  PIC X(7).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MDVENTEA  PIC X.                                          00002200
           02 MDVENTEC  PIC X.                                          00002210
           02 MDVENTEP  PIC X.                                          00002220
           02 MDVENTEH  PIC X.                                          00002230
           02 MDVENTEV  PIC X.                                          00002240
           02 MDVENTEO  PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MDMODIFVTEA    PIC X.                                     00002270
           02 MDMODIFVTEC    PIC X.                                     00002280
           02 MDMODIFVTEP    PIC X.                                     00002290
           02 MDMODIFVTEH    PIC X.                                     00002300
           02 MDMODIFVTEV    PIC X.                                     00002310
           02 MDMODIFVTEO    PIC X(8).                                  00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MPTTVENTEA     PIC X.                                     00002340
           02 MPTTVENTEC     PIC X.                                     00002350
           02 MPTTVENTEP     PIC X.                                     00002360
           02 MPTTVENTEH     PIC X.                                     00002370
           02 MPTTVENTEV     PIC X.                                     00002380
           02 MPTTVENTEO     PIC X(9).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MPCOMPTA  PIC X.                                          00002410
           02 MPCOMPTC  PIC X.                                          00002420
           02 MPCOMPTP  PIC X.                                          00002430
           02 MPCOMPTH  PIC X.                                          00002440
           02 MPCOMPTV  PIC X.                                          00002450
           02 MPCOMPTO  PIC X(9).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MPLIVRA   PIC X.                                          00002480
           02 MPLIVRC   PIC X.                                          00002490
           02 MPLIVRP   PIC X.                                          00002500
           02 MPLIVRH   PIC X.                                          00002510
           02 MPLIVRV   PIC X.                                          00002520
           02 MPLIVRO   PIC X(9).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MPDIFFEREA     PIC X.                                     00002550
           02 MPDIFFEREC     PIC X.                                     00002560
           02 MPDIFFEREP     PIC X.                                     00002570
           02 MPDIFFEREH     PIC X.                                     00002580
           02 MPDIFFEREV     PIC X.                                     00002590
           02 MPDIFFEREO     PIC X(9).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MPRFACTA  PIC X.                                          00002620
           02 MPRFACTC  PIC X.                                          00002630
           02 MPRFACTP  PIC X.                                          00002640
           02 MPRFACTH  PIC X.                                          00002650
           02 MPRFACTV  PIC X.                                          00002660
           02 MPRFACTO  PIC X(9).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNAUTORA  PIC X.                                          00002690
           02 MNAUTORC  PIC X.                                          00002700
           02 MNAUTORP  PIC X.                                          00002710
           02 MNAUTORH  PIC X.                                          00002720
           02 MNAUTORV  PIC X.                                          00002730
           02 MNAUTORO  PIC X(20).                                      00002740
           02 MTABLEO OCCURS   7 TIMES .                                00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MDSAISIEA    PIC X.                                     00002770
             03 MDSAISIEC    PIC X.                                     00002780
             03 MDSAISIEP    PIC X.                                     00002790
             03 MDSAISIEH    PIC X.                                     00002800
             03 MDSAISIEV    PIC X.                                     00002810
             03 MDSAISIEO    PIC X(8).                                  00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MLMODPAIMTA  PIC X.                                     00002840
             03 MLMODPAIMTC  PIC X.                                     00002850
             03 MLMODPAIMTP  PIC X.                                     00002860
             03 MLMODPAIMTH  PIC X.                                     00002870
             03 MLMODPAIMTV  PIC X.                                     00002880
             03 MLMODPAIMTO  PIC X(10).                                 00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MCMODPAIMTA  PIC X.                                     00002910
             03 MCMODPAIMTC  PIC X.                                     00002920
             03 MCMODPAIMTP  PIC X.                                     00002930
             03 MCMODPAIMTH  PIC X.                                     00002940
             03 MCMODPAIMTV  PIC X.                                     00002950
             03 MCMODPAIMTO  PIC X(3).                                  00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MPREGLTVTEA  PIC X.                                     00002980
             03 MPREGLTVTEC  PIC X.                                     00002990
             03 MPREGLTVTEP  PIC X.                                     00003000
             03 MPREGLTVTEH  PIC X.                                     00003010
             03 MPREGLTVTEV  PIC X.                                     00003020
             03 MPREGLTVTEO  PIC X(9).                                  00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MNREGLTVTEA  PIC X.                                     00003050
             03 MNREGLTVTEC  PIC X.                                     00003060
             03 MNREGLTVTEP  PIC X.                                     00003070
             03 MNREGLTVTEH  PIC X.                                     00003080
             03 MNREGLTVTEV  PIC X.                                     00003090
             03 MNREGLTVTEO  PIC X(7).                                  00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MNLIEUTRANA  PIC X.                                     00003120
             03 MNLIEUTRANC  PIC X.                                     00003130
             03 MNLIEUTRANP  PIC X.                                     00003140
             03 MNLIEUTRANH  PIC X.                                     00003150
             03 MNLIEUTRANV  PIC X.                                     00003160
             03 MNLIEUTRANO  PIC X(16).                                 00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MCORGOREDA     PIC X.                                     00003190
           02 MCORGOREDC     PIC X.                                     00003200
           02 MCORGOREDP     PIC X.                                     00003210
           02 MCORGOREDH     PIC X.                                     00003220
           02 MCORGOREDV     PIC X.                                     00003230
           02 MCORGOREDO     PIC X(5).                                  00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MLORGOREDA     PIC X.                                     00003260
           02 MLORGOREDC     PIC X.                                     00003270
           02 MLORGOREDP     PIC X.                                     00003280
           02 MLORGOREDH     PIC X.                                     00003290
           02 MLORGOREDV     PIC X.                                     00003300
           02 MLORGOREDO     PIC X(15).                                 00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MWFACTUREA     PIC X.                                     00003330
           02 MWFACTUREC     PIC X.                                     00003340
           02 MWFACTUREP     PIC X.                                     00003350
           02 MWFACTUREH     PIC X.                                     00003360
           02 MWFACTUREV     PIC X.                                     00003370
           02 MWFACTUREO     PIC X.                                     00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MWDETAXECA     PIC X.                                     00003400
           02 MWDETAXECC     PIC X.                                     00003410
           02 MWDETAXECP     PIC X.                                     00003420
           02 MWDETAXECH     PIC X.                                     00003430
           02 MWDETAXECV     PIC X.                                     00003440
           02 MWDETAXECO     PIC X.                                     00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MCTVA1A   PIC X.                                          00003470
           02 MCTVA1C   PIC X.                                          00003480
           02 MCTVA1P   PIC X.                                          00003490
           02 MCTVA1H   PIC X.                                          00003500
           02 MCTVA1V   PIC X.                                          00003510
           02 MCTVA1O   PIC X(5).                                       00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MPTVA1A   PIC X.                                          00003540
           02 MPTVA1C   PIC X.                                          00003550
           02 MPTVA1P   PIC X.                                          00003560
           02 MPTVA1H   PIC X.                                          00003570
           02 MPTVA1V   PIC X.                                          00003580
           02 MPTVA1O   PIC X(3).                                       00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MCFCREDA  PIC X.                                          00003610
           02 MCFCREDC  PIC X.                                          00003620
           02 MCFCREDP  PIC X.                                          00003630
           02 MCFCREDH  PIC X.                                          00003640
           02 MCFCREDV  PIC X.                                          00003650
           02 MCFCREDO  PIC X(5).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MNCREDIA  PIC X.                                          00003680
           02 MNCREDIC  PIC X.                                          00003690
           02 MNCREDIP  PIC X.                                          00003700
           02 MNCREDIH  PIC X.                                          00003710
           02 MNCREDIV  PIC X.                                          00003720
           02 MNCREDIO  PIC X(14).                                      00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MWEXPORTA      PIC X.                                     00003750
           02 MWEXPORTC PIC X.                                          00003760
           02 MWEXPORTP PIC X.                                          00003770
           02 MWEXPORTH PIC X.                                          00003780
           02 MWEXPORTV PIC X.                                          00003790
           02 MWEXPORTO      PIC X.                                     00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MWBLSPA   PIC X.                                          00003820
           02 MWBLSPC   PIC X.                                          00003830
           02 MWBLSPP   PIC X.                                          00003840
           02 MWBLSPH   PIC X.                                          00003850
           02 MWBLSPV   PIC X.                                          00003860
           02 MWBLSPO   PIC X.                                          00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCTVA2A   PIC X.                                          00003890
           02 MCTVA2C   PIC X.                                          00003900
           02 MCTVA2P   PIC X.                                          00003910
           02 MCTVA2H   PIC X.                                          00003920
           02 MCTVA2V   PIC X.                                          00003930
           02 MCTVA2O   PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MPTVA2A   PIC X.                                          00003960
           02 MPTVA2C   PIC X.                                          00003970
           02 MPTVA2P   PIC X.                                          00003980
           02 MPTVA2H   PIC X.                                          00003990
           02 MPTVA2V   PIC X.                                          00004000
           02 MPTVA2O   PIC X(3).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MCMODPAIMTVTEA      PIC X.                                00004030
           02 MCMODPAIMTVTEC PIC X.                                     00004040
           02 MCMODPAIMTVTEP PIC X.                                     00004050
           02 MCMODPAIMTVTEH PIC X.                                     00004060
           02 MCMODPAIMTVTEV PIC X.                                     00004070
           02 MCMODPAIMTVTEO      PIC X(5).                             00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MLMODPAIMTVTEA      PIC X.                                00004100
           02 MLMODPAIMTVTEC PIC X.                                     00004110
           02 MLMODPAIMTVTEP PIC X.                                     00004120
           02 MLMODPAIMTVTEH PIC X.                                     00004130
           02 MLMODPAIMTVTEV PIC X.                                     00004140
           02 MLMODPAIMTVTEO      PIC X(15).                            00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MCTVA3A   PIC X.                                          00004170
           02 MCTVA3C   PIC X.                                          00004180
           02 MCTVA3P   PIC X.                                          00004190
           02 MCTVA3H   PIC X.                                          00004200
           02 MCTVA3V   PIC X.                                          00004210
           02 MCTVA3O   PIC X(5).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MPTVA3A   PIC X.                                          00004240
           02 MPTVA3C   PIC X.                                          00004250
           02 MPTVA3P   PIC X.                                          00004260
           02 MPTVA3H   PIC X.                                          00004270
           02 MPTVA3V   PIC X.                                          00004280
           02 MPTVA3O   PIC X(9).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MLDESCRIPTIF1A      PIC X.                                00004310
           02 MLDESCRIPTIF1C PIC X.                                     00004320
           02 MLDESCRIPTIF1P PIC X.                                     00004330
           02 MLDESCRIPTIF1H PIC X.                                     00004340
           02 MLDESCRIPTIF1V PIC X.                                     00004350
           02 MLDESCRIPTIF1O      PIC X(30).                            00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MLDESCRIPTIF2A      PIC X.                                00004380
           02 MLDESCRIPTIF2C PIC X.                                     00004390
           02 MLDESCRIPTIF2P PIC X.                                     00004400
           02 MLDESCRIPTIF2H PIC X.                                     00004410
           02 MLDESCRIPTIF2V PIC X.                                     00004420
           02 MLDESCRIPTIF2O      PIC X(30).                            00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MZONCMDA  PIC X.                                          00004450
           02 MZONCMDC  PIC X.                                          00004460
           02 MZONCMDP  PIC X.                                          00004470
           02 MZONCMDH  PIC X.                                          00004480
           02 MZONCMDV  PIC X.                                          00004490
           02 MZONCMDO  PIC X(15).                                      00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MLIBERRA  PIC X.                                          00004520
           02 MLIBERRC  PIC X.                                          00004530
           02 MLIBERRP  PIC X.                                          00004540
           02 MLIBERRH  PIC X.                                          00004550
           02 MLIBERRV  PIC X.                                          00004560
           02 MLIBERRO  PIC X(58).                                      00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MCODTRAA  PIC X.                                          00004590
           02 MCODTRAC  PIC X.                                          00004600
           02 MCODTRAP  PIC X.                                          00004610
           02 MCODTRAH  PIC X.                                          00004620
           02 MCODTRAV  PIC X.                                          00004630
           02 MCODTRAO  PIC X(4).                                       00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MCICSA    PIC X.                                          00004660
           02 MCICSC    PIC X.                                          00004670
           02 MCICSP    PIC X.                                          00004680
           02 MCICSH    PIC X.                                          00004690
           02 MCICSV    PIC X.                                          00004700
           02 MCICSO    PIC X(5).                                       00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MNETNAMA  PIC X.                                          00004730
           02 MNETNAMC  PIC X.                                          00004740
           02 MNETNAMP  PIC X.                                          00004750
           02 MNETNAMH  PIC X.                                          00004760
           02 MNETNAMV  PIC X.                                          00004770
           02 MNETNAMO  PIC X(8).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MSCREENA  PIC X.                                          00004800
           02 MSCREENC  PIC X.                                          00004810
           02 MSCREENP  PIC X.                                          00004820
           02 MSCREENH  PIC X.                                          00004830
           02 MSCREENV  PIC X.                                          00004840
           02 MSCREENO  PIC X(4).                                       00004850
                                                                                
