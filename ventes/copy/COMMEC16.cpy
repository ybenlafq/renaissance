      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * COMMAREA MEC16X POUR CONSIGNES                                          
           02  W-CONSIGNES-RET             PIC  X(02).                          
           02  W-CONSIGNES-MESSAGE         PIC  X(80).                          
           02  W-CONSIGNES-ENTREE.                                              
               03  W-CS-VENTE-GV.                                               
                   04  W-CS-E-NSOC         PIC  X(03).                          
                   04  W-CS-E-NLIEU        PIC  X(03).                          
                   04  W-CS-E-NVENTE       PIC  X(07).                          
               03  W-CS-DATA.                                                   
                   04  W-CS-E-CRETRAIT     PIC  X(10).                          
                   04  W-CS-E-CASIER       PIC  X(10).                          
                   04  W-CS-E-EMPLOYE      PIC  X(10).                          
               03  W-CS-ACTION.                                                 
                   04  W-CS-A-EVENEMENT    PIC X(020).                          
                   04  W-CS-A-TIMESTP      PIC X(026).                          
                                                                                
