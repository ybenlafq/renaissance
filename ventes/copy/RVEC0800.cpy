      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVEC0800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEC0800                         
      **********************************************************                
       01  RVEC0800.                                                            
           02  EC08-DTDDE                                                       
               PIC X(0010).                                                     
           02  EC08-HRDDE                                                       
               PIC X(0008).                                                     
           02  EC08-TYPE                                                        
               PIC X(0004).                                                     
           02  EC08-DATA                                                        
               PIC X(32696).                                                    
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVEC0800                                  
      **********************************************************                
       01  RVEC0800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EC08-DTDDE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC08-DTDDE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EC08-HRDDE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC08-HRDDE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EC08-TYPE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EC08-TYPE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EC08-DATA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  EC08-DATA-F                                                      
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
