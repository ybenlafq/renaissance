      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS SPECIFIQUE DU PROGRAMME TGV83                         * 00000030
      *      1 ITEM PAR PAGE (POUR UN ENLEVEMENT)                     * 00000050
      *       5 BONS D'ENLEVEMENTS PAR ITEM                           * 00000050
      *         2 LIGNES PAR BONS                                     * 00000051
      *            --->  10 LIGNES PAR TS                             * 00000052
      *                                                               * 00000052
      *      TR : GV80  GESTION DES BE                                * 00000060
      *      PG : TGV83 RECEPTION DES BE                              * 00000070
      *                                                               * 00000080
      *      NOM: 'GV83' + EIBTRMID                                   * 00000090
      *      LG : 802 C                                               * 00000091
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV83.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
           05 TS-GV83-LONG               PIC S9(5) COMP-3 VALUE +802.   00000097
      ********************************** DONNEES                        00000098
           05 TS-GV83-DONNEES.                                          00000099
              10 TS-GV83-NBLIGNES      PIC  9(2).                       00000100
              10 TS-GV83-LIGNE           OCCURS 10.                     00000100
                 15 TS-GV83-BE-DET1    PIC  X(9).                               
                 15 TS-GV83-BE-DET2    PIC  X(64).                              
                 15 TS-GV83-QTE        PIC  9(3).                               
                 15 TS-GV83-QTEINIT    PIC  9(3).                               
                 15 TS-GV83-MESS       PIC  X.                                  
                                                                                
