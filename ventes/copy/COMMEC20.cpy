      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *        MAQUETTE COMMAREA STANDARD AIDA COBOL2              *            
      **************************************************************            
      *-------------------------------------------------------------            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-EC20-LONG-COMMAREA PIC S9(4) COMP VALUE +4096.                   
      *--                                                                       
       01  COM-EC20-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +4096.                 
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *                                                                         
      * ZONES RESERVEES A AIDA ----------------------------------- 100          
           02 FILLER-COM-AIDA      PIC X(100).                                  
      *                                                                         
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020          
           02 COMM-CICS-APPLID     PIC X(8).                                    
           02 COMM-CICS-NETNAM     PIC X(8).                                    
           02 COMM-CICS-TRANSA     PIC X(4).                                    
      *                                                                         
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100          
           02 COMM-DATE-SIECLE     PIC XX.                                      
           02 COMM-DATE-ANNEE      PIC XX.                                      
           02 COMM-DATE-MOIS       PIC XX.                                      
           02 COMM-DATE-JOUR       PIC 99.                                      
      *   QUANTIEMES CALENDAIRE ET STANDARD                                     
           02 COMM-DATE-QNTA       PIC 999.                                     
           02 COMM-DATE-QNT0       PIC 99999.                                   
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                          
           02 COMM-DATE-BISX       PIC 9.                                       
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                                  
           02 COMM-DATE-JSM        PIC 9.                                       
      *   LIBELLES DU JOUR COURT - LONG                                         
           02 COMM-DATE-JSM-LC     PIC XXX.                                     
           02 COMM-DATE-JSM-LL     PIC XXXXXXXX.                                
      *   LIBELLES DU MOIS COURT - LONG                                         
           02 COMM-DATE-MOIS-LC    PIC XXX.                                     
           02 COMM-DATE-MOIS-LL    PIC XXXXXXXX.                                
      *   DIFFERENTES FORMES DE DATE                                            
           02 COMM-DATE-SSAAMMJJ   PIC X(8).                                    
           02 COMM-DATE-AAMMJJ     PIC X(6).                                    
           02 COMM-DATE-JJMMSSAA   PIC X(8).                                    
           02 COMM-DATE-JJMMAA     PIC X(6).                                    
           02 COMM-DATE-JJ-MM-AA   PIC X(8).                                    
           02 COMM-DATE-JJ-MM-SSAA PIC X(10).                                   
      *   TRAITEMENT DU NUMERO DE SEMAINE                                       
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS   PIC 99.                                      
              05 COMM-DATE-SEMAA   PIC 99.                                      
              05 COMM-DATE-SEMNU   PIC 99.                                      
      *                                                                         
           02 COMM-DATE-FILLER     PIC X(08).                                   
      *                                                                         
      * ZONES RESERVEES TRAITEMENT DU SWAP ----------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS       PIC S9(4) COMP VALUE -1.                     
      *--                                                                       
           02 COMM-SWAP-CURS       PIC S9(4) COMP-5 VALUE -1.                   
      *}                                                                        
           02 COMM-SWAP-ATTR OCCURS 1   PIC X(1).                               
      *                                                                         
      * ZONES RESERVEES APPLICATIVES ---------------------------- 3724          
      *                                                                         
      *                                                                         
           02 COMM-EC20.                                                        
              03  COMM-EC20-NCODIC    PIC X(07).                                
              03  COMM-EC20-CFAM      PIC X(05).                                
              03  COMM-EC20-CRAYON    PIC X(05).                                
              03  COMM-EC20-SMAG      PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC20-SART      PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC20-SDEPOT    PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC20-EXPO.                                               
                  04  COMM-EC20-EXPO-OCCURS OCCURS 18.                          
                      05 COMM-EC20-CEXPO  PIC X.                                
                      05 COMM-EC20-WCC    PIC X.                                
                      05 COMM-EC20-LEXPO  PIC X(10).                            
           02 COMM-EC21.                                                        
              03  COMM-EC21-NBP       PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC21-PAGEA     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC21-RCFAM     PIC X(05).                                
              03  COMM-EC21-LRAYON    PIC X(20).                                
AD01          03  FILLER              PIC X(100).                               
           02 COMM-EC22.                                                        
              03  COMM-EC22-NBP       PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC22-PAGEA     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC22-LFAM      PIC X(20).                                
AD01          03  COMM-EC22-WCC       PIC X(01).                                
AD01          03  FILLER              PIC X(100).                               
           02 COMM-EC23.                                                        
              03  COMM-EC23-NBP       PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-PAGEA     PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SMAG      PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SART      PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SDEPOT    PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SMAG-C    PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SART-C    PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-SDEPOT-C  PIC S9(5) PACKED-DECIMAL.                 
              03  COMM-EC23-CFAM      PIC X(05).                                
              03  COMM-EC23-CMARQ     PIC X(05).                                
AD01          03  COMM-EC23-CEXPO     PIC X(05).                                
AD01          03  COMM-EC23-WCC       PIC X(01).                                
AD01          03  COMM-EC23-WDACEM    PIC X(01).                                
              03  COMM-EC23-LREFFOURN PIC X(20).                                
              03  COMM-EC23-MAG       PIC S9(5) PACKED-DECIMAL                  
                                      OCCURS 10.                                
              03  COMM-EC23-ART       PIC S9(5) PACKED-DECIMAL                  
                                      OCCURS 10.                                
              03  COMM-EC23-STOCK.                                              
                  04  COMM-EC23-STOCK-OCCURS OCCURS 2.                          
                      05 COMM-EC23-DEPOT  PIC X(10).                            
                      05 COMM-EC23-QSTOCK PIC S9(5) PACKED-DECIMAL.             
                                                                                
