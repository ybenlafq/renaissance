      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMMAREA PSE POST ACHAT MEC31                                           
      ******************************************************************        
           02  W-MEC31-ENTETE.                                                  
              03  W-MEC31-NSOC                PIC X(003).                       
              03  W-MEC31-NLIEU               PIC X(003).                       
              03  W-MEC31-NVENTE              PIC X(007).                       
              03  W-MEC31-NCDEWC              PIC S9(15) PACKED-DECIMAL.        
              03  W-MEC31-MD-PMENT            PIC X(005).                       
           02  W-MEC31-CODE-RET               PIC X(1).                         
      * tableau des ajouts PSE (indice, utilis�, max)                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  W-PSE-R-I               PIC S9(4) BINARY.                        
      *--                                                                       
           02  W-PSE-R-I               PIC S9(4) COMP-5.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  W-PSE-R-U               PIC S9(4) BINARY.                        
      *--                                                                       
           02  W-PSE-R-U               PIC S9(4) COMP-5.                        
      *}                                                                        
           02  W-MEC31-PSE          OCCURS 30.                                  
               04  W-MEC31-RET       PIC X(002).                                
               04  W-MEC31-C-PSE     PIC X(005).                                
               04  W-MEC31-C-VEND    PIC X(006).                                
               04  W-MEC31-NCODIC    PIC X(007).                                
               04  W-MEC31-NSEQNQ    PIC S9(2) PACKED-DECIMAL.                  
               04  W-MEC31-PMONTANT  PIC S9(5)V99 PACKED-DECIMAL.               
M23862         04  W-MEC31-NUM-PS    PIC X(008).                                
                                                                                
