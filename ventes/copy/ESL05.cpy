      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL05   ESL05                                              00000020
      ***************************************************************** 00000030
       01   ESL05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSLENCL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 NSLENCL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 NSLENCF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 NSLENCI   PIC X(6).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LLIEUENCL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 LLIEUENCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 LLIEUENCF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 LLIEUENCI      PIC X(20).                                 00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DOMAINEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 DOMAINEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 DOMAINEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 DOMAINEI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMPTEURL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 COMPTEURL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 COMPTEURF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 COMPTEURI      PIC X(2).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 REFERENCEL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 REFERENCEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 REFERENCEF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 REFERENCEI     PIC X(15).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DEFFETL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 DEFFETL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 DEFFETF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 DEFFETI   PIC X(10).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 DSAISIEL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 DSAISIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 DSAISIEF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 DSAISIEI  PIC X(10).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 REFERENC2L     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 REFERENC2L COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 REFERENC2F     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 REFERENC2I     PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 NSEQL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 NSEQL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 NSEQF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 NSEQI     PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STELIEUL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 STELIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 STELIEUF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 STELIEUI  PIC X(6).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LLIEUL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 LLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 LLIEUF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 LLIEUI    PIC X(20).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STELIEUSL      COMP PIC S9(4).                            00000580
      *--                                                                       
           02 STELIEUSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 STELIEUSF      PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 STELIEUSI      PIC X(6).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 LLIEUSL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 LLIEUSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 LLIEUSF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 LLIEUSI   PIC X(18).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 PMONTANTL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 PMONTANTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 PMONTANTF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 PMONTANTI      PIC X(10).                                 00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MZONCMDI  PIC X(12).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIBERRI  PIC X(61).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCICSI    PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MNETNAMI  PIC X(8).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MSCREENI  PIC X(4).                                       00000930
      ***************************************************************** 00000940
      * SDF: ESL05   ESL05                                              00000950
      ***************************************************************** 00000960
       01   ESL05O REDEFINES ESL05I.                                    00000970
           02 FILLER    PIC X(12).                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MDATJOUA  PIC X.                                          00001000
           02 MDATJOUC  PIC X.                                          00001010
           02 MDATJOUP  PIC X.                                          00001020
           02 MDATJOUH  PIC X.                                          00001030
           02 MDATJOUV  PIC X.                                          00001040
           02 MDATJOUO  PIC X(10).                                      00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MTIMJOUA  PIC X.                                          00001070
           02 MTIMJOUC  PIC X.                                          00001080
           02 MTIMJOUP  PIC X.                                          00001090
           02 MTIMJOUH  PIC X.                                          00001100
           02 MTIMJOUV  PIC X.                                          00001110
           02 MTIMJOUO  PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 NSLENCA   PIC X.                                          00001140
           02 NSLENCC   PIC X.                                          00001150
           02 NSLENCP   PIC X.                                          00001160
           02 NSLENCH   PIC X.                                          00001170
           02 NSLENCV   PIC X.                                          00001180
           02 NSLENCO   PIC X(6).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 LLIEUENCA      PIC X.                                     00001210
           02 LLIEUENCC PIC X.                                          00001220
           02 LLIEUENCP PIC X.                                          00001230
           02 LLIEUENCH PIC X.                                          00001240
           02 LLIEUENCV PIC X.                                          00001250
           02 LLIEUENCO      PIC X(20).                                 00001260
           02 FILLER    PIC X(2).                                       00001270
           02 DOMAINEA  PIC X.                                          00001280
           02 DOMAINEC  PIC X.                                          00001290
           02 DOMAINEP  PIC X.                                          00001300
           02 DOMAINEH  PIC X.                                          00001310
           02 DOMAINEV  PIC X.                                          00001320
           02 DOMAINEO  PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 COMPTEURA      PIC X.                                     00001350
           02 COMPTEURC PIC X.                                          00001360
           02 COMPTEURP PIC X.                                          00001370
           02 COMPTEURH PIC X.                                          00001380
           02 COMPTEURV PIC X.                                          00001390
           02 COMPTEURO      PIC X(2).                                  00001400
           02 FILLER    PIC X(2).                                       00001410
           02 REFERENCEA     PIC X.                                     00001420
           02 REFERENCEC     PIC X.                                     00001430
           02 REFERENCEP     PIC X.                                     00001440
           02 REFERENCEH     PIC X.                                     00001450
           02 REFERENCEV     PIC X.                                     00001460
           02 REFERENCEO     PIC X(15).                                 00001470
           02 FILLER    PIC X(2).                                       00001480
           02 DEFFETA   PIC X.                                          00001490
           02 DEFFETC   PIC X.                                          00001500
           02 DEFFETP   PIC X.                                          00001510
           02 DEFFETH   PIC X.                                          00001520
           02 DEFFETV   PIC X.                                          00001530
           02 DEFFETO   PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 DSAISIEA  PIC X.                                          00001560
           02 DSAISIEC  PIC X.                                          00001570
           02 DSAISIEP  PIC X.                                          00001580
           02 DSAISIEH  PIC X.                                          00001590
           02 DSAISIEV  PIC X.                                          00001600
           02 DSAISIEO  PIC X(10).                                      00001610
           02 FILLER    PIC X(2).                                       00001620
           02 REFERENC2A     PIC X.                                     00001630
           02 REFERENC2C     PIC X.                                     00001640
           02 REFERENC2P     PIC X.                                     00001650
           02 REFERENC2H     PIC X.                                     00001660
           02 REFERENC2V     PIC X.                                     00001670
           02 REFERENC2O     PIC X(20).                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 NSEQA     PIC X.                                          00001700
           02 NSEQC     PIC X.                                          00001710
           02 NSEQP     PIC X.                                          00001720
           02 NSEQH     PIC X.                                          00001730
           02 NSEQV     PIC X.                                          00001740
           02 NSEQO     PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 STELIEUA  PIC X.                                          00001770
           02 STELIEUC  PIC X.                                          00001780
           02 STELIEUP  PIC X.                                          00001790
           02 STELIEUH  PIC X.                                          00001800
           02 STELIEUV  PIC X.                                          00001810
           02 STELIEUO  PIC X(6).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 LLIEUA    PIC X.                                          00001840
           02 LLIEUC    PIC X.                                          00001850
           02 LLIEUP    PIC X.                                          00001860
           02 LLIEUH    PIC X.                                          00001870
           02 LLIEUV    PIC X.                                          00001880
           02 LLIEUO    PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 STELIEUSA      PIC X.                                     00001910
           02 STELIEUSC PIC X.                                          00001920
           02 STELIEUSP PIC X.                                          00001930
           02 STELIEUSH PIC X.                                          00001940
           02 STELIEUSV PIC X.                                          00001950
           02 STELIEUSO      PIC X(6).                                  00001960
           02 FILLER    PIC X(2).                                       00001970
           02 LLIEUSA   PIC X.                                          00001980
           02 LLIEUSC   PIC X.                                          00001990
           02 LLIEUSP   PIC X.                                          00002000
           02 LLIEUSH   PIC X.                                          00002010
           02 LLIEUSV   PIC X.                                          00002020
           02 LLIEUSO   PIC X(18).                                      00002030
           02 FILLER    PIC X(2).                                       00002040
           02 PMONTANTA      PIC X.                                     00002050
           02 PMONTANTC PIC X.                                          00002060
           02 PMONTANTP PIC X.                                          00002070
           02 PMONTANTH PIC X.                                          00002080
           02 PMONTANTV PIC X.                                          00002090
           02 PMONTANTO      PIC -ZZZZZ9,99.                            00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MZONCMDA  PIC X.                                          00002120
           02 MZONCMDC  PIC X.                                          00002130
           02 MZONCMDP  PIC X.                                          00002140
           02 MZONCMDH  PIC X.                                          00002150
           02 MZONCMDV  PIC X.                                          00002160
           02 MZONCMDO  PIC X(12).                                      00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIBERRA  PIC X.                                          00002190
           02 MLIBERRC  PIC X.                                          00002200
           02 MLIBERRP  PIC X.                                          00002210
           02 MLIBERRH  PIC X.                                          00002220
           02 MLIBERRV  PIC X.                                          00002230
           02 MLIBERRO  PIC X(61).                                      00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MCODTRAA  PIC X.                                          00002260
           02 MCODTRAC  PIC X.                                          00002270
           02 MCODTRAP  PIC X.                                          00002280
           02 MCODTRAH  PIC X.                                          00002290
           02 MCODTRAV  PIC X.                                          00002300
           02 MCODTRAO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MCICSA    PIC X.                                          00002330
           02 MCICSC    PIC X.                                          00002340
           02 MCICSP    PIC X.                                          00002350
           02 MCICSH    PIC X.                                          00002360
           02 MCICSV    PIC X.                                          00002370
           02 MCICSO    PIC X(5).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MNETNAMA  PIC X.                                          00002400
           02 MNETNAMC  PIC X.                                          00002410
           02 MNETNAMP  PIC X.                                          00002420
           02 MNETNAMH  PIC X.                                          00002430
           02 MNETNAMV  PIC X.                                          00002440
           02 MNETNAMO  PIC X(8).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(4).                                       00002520
                                                                                
