      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVPM3500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPM3500                         
      *   TABLE MQ DU LOCAL                                                     
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPM3500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPM3500.                                                            
      *}                                                                        
           02  PM35-CNOMLG                                                      
               PIC X(0015).                                                     
           02  PM35-LNOMLG                                                      
               PIC X(0040).                                                     
           02  PM35-CQALIA                                                      
               PIC X(0015).                                                     
           02  PM35-QEXPIR                                                      
               PIC S9(9) COMP-3.                                                
           02  PM35-WCOA                                                        
               PIC X(0001).                                                     
           02  PM35-WCOD                                                        
               PIC X(0001).                                                     
           02  PM35-WRTQ                                                        
               PIC X(0015).                                                     
           02  PM35-MSGID                                                       
               PIC X(0024).                                                     
           02  PM35-CARRET                                                      
               PIC X(0001).                                                     
           02  PM35-PERSIS                                                      
               PIC X(0001).                                                     
           02  PM35-PRIOR                                                       
               PIC S9(9) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPM3500                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVPM3500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVPM3500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-CNOMLG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-CNOMLG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-LNOMLG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-LNOMLG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-CQALIA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-CQALIA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-QEXPIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-QEXPIR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-WCOA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-WCOA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-WCOD-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-WCOD-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-WRTQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-WRTQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-MSGID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-MSGID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-CARRET-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-CARRET-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-PERSIS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM35-PERSIS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM35-PRIOR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  PM35-PRIOR-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
