      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGV7100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV7100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV7100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV7100.                                                            
      *}                                                                        
           02  GV71-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV71-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV71-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV71-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV71-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV71-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV71-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GV71-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV71-WCQERESF                                                    
               PIC X(0001).                                                     
           02  GV71-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV71-DMUTATION                                                   
               PIC X(0008).                                                     
           02  GV71-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV7100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV7100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV7100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-DMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-DMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV71-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV71-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
