      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV04   EAV04                                              00000020
      ***************************************************************** 00000030
       01   EAV04I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�societe                                                       00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCIETEI     PIC X(3).                                  00000180
      * n� lieu                                                         00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUI   PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MLLIEUI   PIC X(20).                                      00000270
      * n�avoir                                                         00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000290
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000300
           02 FILLER    PIC X(4).                                       00000310
           02 MNAVOIRI  PIC X(7).                                       00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPAVOIRL    COMP PIC S9(4).                            00000330
      *--                                                                       
           02 MCTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPAVOIRF    PIC X.                                     00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MCTYPAVOIRI    PIC X(5).                                  00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPAVOIRL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MLTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPAVOIRF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MLTYPAVOIRI    PIC X(20).                                 00000400
      * indication sur la vente                                         00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMESSVENTEL    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MMESSVENTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MMESSVENTEF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MMESSVENTEI    PIC X(30).                                 00000450
      * n�societe vente d'origine                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVENTEL    COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MNSOCVENTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCVENTEF    PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNSOCVENTEI    PIC X(3).                                  00000500
      * n�magasin   "  "  "  "  "                                       00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVENTEL   COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MNLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNLIEUVENTEF   PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNLIEUVENTEI   PIC X(3).                                  00000550
      * libelle qui va bien                                             00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUVENTEL   COMP PIC S9(4).                            00000570
      *--                                                                       
           02 MLLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLLIEUVENTEF   PIC X.                                     00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MLLIEUVENTEI   PIC X(20).                                 00000600
      * n� bon de commande                                              00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNVENTEI  PIC X(7).                                       00000650
      * date de caisse                                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCAISSEL      COMP PIC S9(4).                            00000670
      *--                                                                       
           02 MDCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCAISSEF      PIC X.                                     00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MDCAISSEI      PIC X(10).                                 00000700
      * num�ro de caisse                                                00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISSEL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MNCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCAISSEF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MNCAISSEI      PIC X(3).                                  00000750
      * n� de transaction                                               00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNTRANSI  PIC X(4).                                       00000800
      * n� CRI                                                          00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCRIL    COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNCRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCRIF    PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNCRII    PIC X(11).                                      00000850
      * code mode de paiement                                           00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODPAIMTL    COMP PIC S9(4).                            00000870
      *--                                                                       
           02 MCMODPAIMTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODPAIMTF    PIC X.                                     00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCMODPAIMTI    PIC X(5).                                  00000900
      * libelle mode de paiement                                        00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMODPAIMTL    COMP PIC S9(4).                            00000920
      *--                                                                       
           02 MLMODPAIMTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLMODPAIMTF    PIC X.                                     00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLMODPAIMTI    PIC X(19).                                 00000950
      * titre du nom                                                    00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCTITRENOMI    PIC X(5).                                  00001000
      * nom                                                             00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MLNOMI    PIC X(25).                                      00001050
      * prenom                                                          00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLPRENOMI      PIC X(15).                                 00001100
      * n�voie                                                          00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVOIEL   COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MCVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVOIEF   PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MCVOIEI   PIC X(5).                                       00001150
      * type de voie                                                    00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVOIEL  COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MCTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTVOIEF  PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MCTVOIEI  PIC X(4).                                       00001200
      * nom de la voie                                                  00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMVOIEL     COMP PIC S9(4).                            00001220
      *--                                                                       
           02 MLNOMVOIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNOMVOIEF     PIC X.                                     00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MLNOMVOIEI     PIC X(21).                                 00001250
      * commune                                                         00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPADRL      COMP PIC S9(4).                            00001270
      *--                                                                       
           02 MCOMPADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMPADRF      PIC X.                                     00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCOMPADRI      PIC X(32).                                 00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBATL     COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MBATL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MBATF     PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MBATI     PIC X(3).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESCL     COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MESCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MESCF     PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MESCI     PIC X(3).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METAGEL   COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 METAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 METAGEF   PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 METAGEI   PIC X(3).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPORTEL   COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MPORTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPORTEF   PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MPORTEI   PIC X(3).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00001470
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCPOSTALI      PIC X(5).                                  00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00001510
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MLCOMMUNEI     PIC X(32).                                 00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBUREAUL      COMP PIC S9(4).                            00001550
      *--                                                                       
           02 MLBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBUREAUF      PIC X.                                     00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MLBUREAUI      PIC X(26).                                 00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MLIBERRI  PIC X(78).                                      00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MCODTRAI  PIC X(4).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCICSI    PIC X(5).                                       00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MNETNAMI  PIC X(8).                                       00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MSCREENI  PIC X(4).                                       00001780
      ***************************************************************** 00001790
      * SDF: EAV04   EAV04                                              00001800
      ***************************************************************** 00001810
       01   EAV04O REDEFINES EAV04I.                                    00001820
           02 FILLER    PIC X(12).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDATJOUA  PIC X.                                          00001850
           02 MDATJOUC  PIC X.                                          00001860
           02 MDATJOUP  PIC X.                                          00001870
           02 MDATJOUH  PIC X.                                          00001880
           02 MDATJOUV  PIC X.                                          00001890
           02 MDATJOUO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MTIMJOUA  PIC X.                                          00001920
           02 MTIMJOUC  PIC X.                                          00001930
           02 MTIMJOUP  PIC X.                                          00001940
           02 MTIMJOUH  PIC X.                                          00001950
           02 MTIMJOUV  PIC X.                                          00001960
           02 MTIMJOUO  PIC X(5).                                       00001970
      * n�societe                                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNSOCIETEA     PIC X.                                     00002000
           02 MNSOCIETEC     PIC X.                                     00002010
           02 MNSOCIETEP     PIC X.                                     00002020
           02 MNSOCIETEH     PIC X.                                     00002030
           02 MNSOCIETEV     PIC X.                                     00002040
           02 MNSOCIETEO     PIC X(3).                                  00002050
      * n� lieu                                                         00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MNLIEUA   PIC X.                                          00002080
           02 MNLIEUC   PIC X.                                          00002090
           02 MNLIEUP   PIC X.                                          00002100
           02 MNLIEUH   PIC X.                                          00002110
           02 MNLIEUV   PIC X.                                          00002120
           02 MNLIEUO   PIC X(3).                                       00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MLLIEUA   PIC X.                                          00002150
           02 MLLIEUC   PIC X.                                          00002160
           02 MLLIEUP   PIC X.                                          00002170
           02 MLLIEUH   PIC X.                                          00002180
           02 MLLIEUV   PIC X.                                          00002190
           02 MLLIEUO   PIC X(20).                                      00002200
      * n�avoir                                                         00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MNAVOIRA  PIC X.                                          00002230
           02 MNAVOIRC  PIC X.                                          00002240
           02 MNAVOIRP  PIC X.                                          00002250
           02 MNAVOIRH  PIC X.                                          00002260
           02 MNAVOIRV  PIC X.                                          00002270
           02 MNAVOIRO  PIC X(7).                                       00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCTYPAVOIRA    PIC X.                                     00002300
           02 MCTYPAVOIRC    PIC X.                                     00002310
           02 MCTYPAVOIRP    PIC X.                                     00002320
           02 MCTYPAVOIRH    PIC X.                                     00002330
           02 MCTYPAVOIRV    PIC X.                                     00002340
           02 MCTYPAVOIRO    PIC X(5).                                  00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MLTYPAVOIRA    PIC X.                                     00002370
           02 MLTYPAVOIRC    PIC X.                                     00002380
           02 MLTYPAVOIRP    PIC X.                                     00002390
           02 MLTYPAVOIRH    PIC X.                                     00002400
           02 MLTYPAVOIRV    PIC X.                                     00002410
           02 MLTYPAVOIRO    PIC X(20).                                 00002420
      * indication sur la vente                                         00002430
           02 FILLER    PIC X(2).                                       00002440
           02 MMESSVENTEA    PIC X.                                     00002450
           02 MMESSVENTEC    PIC X.                                     00002460
           02 MMESSVENTEP    PIC X.                                     00002470
           02 MMESSVENTEH    PIC X.                                     00002480
           02 MMESSVENTEV    PIC X.                                     00002490
           02 MMESSVENTEO    PIC X(30).                                 00002500
      * n�societe vente d'origine                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNSOCVENTEA    PIC X.                                     00002530
           02 MNSOCVENTEC    PIC X.                                     00002540
           02 MNSOCVENTEP    PIC X.                                     00002550
           02 MNSOCVENTEH    PIC X.                                     00002560
           02 MNSOCVENTEV    PIC X.                                     00002570
           02 MNSOCVENTEO    PIC X(3).                                  00002580
      * n�magasin   "  "  "  "  "                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNLIEUVENTEA   PIC X.                                     00002610
           02 MNLIEUVENTEC   PIC X.                                     00002620
           02 MNLIEUVENTEP   PIC X.                                     00002630
           02 MNLIEUVENTEH   PIC X.                                     00002640
           02 MNLIEUVENTEV   PIC X.                                     00002650
           02 MNLIEUVENTEO   PIC X(3).                                  00002660
      * libelle qui va bien                                             00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLLIEUVENTEA   PIC X.                                     00002690
           02 MLLIEUVENTEC   PIC X.                                     00002700
           02 MLLIEUVENTEP   PIC X.                                     00002710
           02 MLLIEUVENTEH   PIC X.                                     00002720
           02 MLLIEUVENTEV   PIC X.                                     00002730
           02 MLLIEUVENTEO   PIC X(20).                                 00002740
      * n� bon de commande                                              00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNVENTEA  PIC X.                                          00002770
           02 MNVENTEC  PIC X.                                          00002780
           02 MNVENTEP  PIC X.                                          00002790
           02 MNVENTEH  PIC X.                                          00002800
           02 MNVENTEV  PIC X.                                          00002810
           02 MNVENTEO  PIC X(7).                                       00002820
      * date de caisse                                                  00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MDCAISSEA      PIC X.                                     00002850
           02 MDCAISSEC PIC X.                                          00002860
           02 MDCAISSEP PIC X.                                          00002870
           02 MDCAISSEH PIC X.                                          00002880
           02 MDCAISSEV PIC X.                                          00002890
           02 MDCAISSEO      PIC X(10).                                 00002900
      * num�ro de caisse                                                00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MNCAISSEA      PIC X.                                     00002930
           02 MNCAISSEC PIC X.                                          00002940
           02 MNCAISSEP PIC X.                                          00002950
           02 MNCAISSEH PIC X.                                          00002960
           02 MNCAISSEV PIC X.                                          00002970
           02 MNCAISSEO      PIC X(3).                                  00002980
      * n� de transaction                                               00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MNTRANSA  PIC X.                                          00003010
           02 MNTRANSC  PIC X.                                          00003020
           02 MNTRANSP  PIC X.                                          00003030
           02 MNTRANSH  PIC X.                                          00003040
           02 MNTRANSV  PIC X.                                          00003050
           02 MNTRANSO  PIC X(4).                                       00003060
      * n� CRI                                                          00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MNCRIA    PIC X.                                          00003090
           02 MNCRIC    PIC X.                                          00003100
           02 MNCRIP    PIC X.                                          00003110
           02 MNCRIH    PIC X.                                          00003120
           02 MNCRIV    PIC X.                                          00003130
           02 MNCRIO    PIC X(11).                                      00003140
      * code mode de paiement                                           00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MCMODPAIMTA    PIC X.                                     00003170
           02 MCMODPAIMTC    PIC X.                                     00003180
           02 MCMODPAIMTP    PIC X.                                     00003190
           02 MCMODPAIMTH    PIC X.                                     00003200
           02 MCMODPAIMTV    PIC X.                                     00003210
           02 MCMODPAIMTO    PIC X(5).                                  00003220
      * libelle mode de paiement                                        00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MLMODPAIMTA    PIC X.                                     00003250
           02 MLMODPAIMTC    PIC X.                                     00003260
           02 MLMODPAIMTP    PIC X.                                     00003270
           02 MLMODPAIMTH    PIC X.                                     00003280
           02 MLMODPAIMTV    PIC X.                                     00003290
           02 MLMODPAIMTO    PIC X(19).                                 00003300
      * titre du nom                                                    00003310
           02 FILLER    PIC X(2).                                       00003320
           02 MCTITRENOMA    PIC X.                                     00003330
           02 MCTITRENOMC    PIC X.                                     00003340
           02 MCTITRENOMP    PIC X.                                     00003350
           02 MCTITRENOMH    PIC X.                                     00003360
           02 MCTITRENOMV    PIC X.                                     00003370
           02 MCTITRENOMO    PIC X(5).                                  00003380
      * nom                                                             00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MLNOMA    PIC X.                                          00003410
           02 MLNOMC    PIC X.                                          00003420
           02 MLNOMP    PIC X.                                          00003430
           02 MLNOMH    PIC X.                                          00003440
           02 MLNOMV    PIC X.                                          00003450
           02 MLNOMO    PIC X(25).                                      00003460
      * prenom                                                          00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MLPRENOMA      PIC X.                                     00003490
           02 MLPRENOMC PIC X.                                          00003500
           02 MLPRENOMP PIC X.                                          00003510
           02 MLPRENOMH PIC X.                                          00003520
           02 MLPRENOMV PIC X.                                          00003530
           02 MLPRENOMO      PIC X(15).                                 00003540
      * n�voie                                                          00003550
           02 FILLER    PIC X(2).                                       00003560
           02 MCVOIEA   PIC X.                                          00003570
           02 MCVOIEC   PIC X.                                          00003580
           02 MCVOIEP   PIC X.                                          00003590
           02 MCVOIEH   PIC X.                                          00003600
           02 MCVOIEV   PIC X.                                          00003610
           02 MCVOIEO   PIC X(5).                                       00003620
      * type de voie                                                    00003630
           02 FILLER    PIC X(2).                                       00003640
           02 MCTVOIEA  PIC X.                                          00003650
           02 MCTVOIEC  PIC X.                                          00003660
           02 MCTVOIEP  PIC X.                                          00003670
           02 MCTVOIEH  PIC X.                                          00003680
           02 MCTVOIEV  PIC X.                                          00003690
           02 MCTVOIEO  PIC X(4).                                       00003700
      * nom de la voie                                                  00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MLNOMVOIEA     PIC X.                                     00003730
           02 MLNOMVOIEC     PIC X.                                     00003740
           02 MLNOMVOIEP     PIC X.                                     00003750
           02 MLNOMVOIEH     PIC X.                                     00003760
           02 MLNOMVOIEV     PIC X.                                     00003770
           02 MLNOMVOIEO     PIC X(21).                                 00003780
      * commune                                                         00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MCOMPADRA      PIC X.                                     00003810
           02 MCOMPADRC PIC X.                                          00003820
           02 MCOMPADRP PIC X.                                          00003830
           02 MCOMPADRH PIC X.                                          00003840
           02 MCOMPADRV PIC X.                                          00003850
           02 MCOMPADRO      PIC X(32).                                 00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MBATA     PIC X.                                          00003880
           02 MBATC     PIC X.                                          00003890
           02 MBATP     PIC X.                                          00003900
           02 MBATH     PIC X.                                          00003910
           02 MBATV     PIC X.                                          00003920
           02 MBATO     PIC X(3).                                       00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MESCA     PIC X.                                          00003950
           02 MESCC     PIC X.                                          00003960
           02 MESCP     PIC X.                                          00003970
           02 MESCH     PIC X.                                          00003980
           02 MESCV     PIC X.                                          00003990
           02 MESCO     PIC X(3).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 METAGEA   PIC X.                                          00004020
           02 METAGEC   PIC X.                                          00004030
           02 METAGEP   PIC X.                                          00004040
           02 METAGEH   PIC X.                                          00004050
           02 METAGEV   PIC X.                                          00004060
           02 METAGEO   PIC X(3).                                       00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MPORTEA   PIC X.                                          00004090
           02 MPORTEC   PIC X.                                          00004100
           02 MPORTEP   PIC X.                                          00004110
           02 MPORTEH   PIC X.                                          00004120
           02 MPORTEV   PIC X.                                          00004130
           02 MPORTEO   PIC X(3).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MCPOSTALA      PIC X.                                     00004160
           02 MCPOSTALC PIC X.                                          00004170
           02 MCPOSTALP PIC X.                                          00004180
           02 MCPOSTALH PIC X.                                          00004190
           02 MCPOSTALV PIC X.                                          00004200
           02 MCPOSTALO      PIC X(5).                                  00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MLCOMMUNEA     PIC X.                                     00004230
           02 MLCOMMUNEC     PIC X.                                     00004240
           02 MLCOMMUNEP     PIC X.                                     00004250
           02 MLCOMMUNEH     PIC X.                                     00004260
           02 MLCOMMUNEV     PIC X.                                     00004270
           02 MLCOMMUNEO     PIC X(32).                                 00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MLBUREAUA      PIC X.                                     00004300
           02 MLBUREAUC PIC X.                                          00004310
           02 MLBUREAUP PIC X.                                          00004320
           02 MLBUREAUH PIC X.                                          00004330
           02 MLBUREAUV PIC X.                                          00004340
           02 MLBUREAUO      PIC X(26).                                 00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MLIBERRA  PIC X.                                          00004370
           02 MLIBERRC  PIC X.                                          00004380
           02 MLIBERRP  PIC X.                                          00004390
           02 MLIBERRH  PIC X.                                          00004400
           02 MLIBERRV  PIC X.                                          00004410
           02 MLIBERRO  PIC X(78).                                      00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MCODTRAA  PIC X.                                          00004440
           02 MCODTRAC  PIC X.                                          00004450
           02 MCODTRAP  PIC X.                                          00004460
           02 MCODTRAH  PIC X.                                          00004470
           02 MCODTRAV  PIC X.                                          00004480
           02 MCODTRAO  PIC X(4).                                       00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MCICSA    PIC X.                                          00004510
           02 MCICSC    PIC X.                                          00004520
           02 MCICSP    PIC X.                                          00004530
           02 MCICSH    PIC X.                                          00004540
           02 MCICSV    PIC X.                                          00004550
           02 MCICSO    PIC X(5).                                       00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MNETNAMA  PIC X.                                          00004580
           02 MNETNAMC  PIC X.                                          00004590
           02 MNETNAMP  PIC X.                                          00004600
           02 MNETNAMH  PIC X.                                          00004610
           02 MNETNAMV  PIC X.                                          00004620
           02 MNETNAMO  PIC X(8).                                       00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MSCREENA  PIC X.                                          00004650
           02 MSCREENC  PIC X.                                          00004660
           02 MSCREENP  PIC X.                                          00004670
           02 MSCREENH  PIC X.                                          00004680
           02 MSCREENV  PIC X.                                          00004690
           02 MSCREENO  PIC X(4).                                       00004700
                                                                                
