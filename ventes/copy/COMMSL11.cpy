      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *AUTHOR.DSA049 DIT "DENIS COIFFIER"  LE 01/03/01                          
      * COMMAREA PASSEE A MSL11                                                 
      *  LONGUEUR TOTALE 300                                                    
       01 COMM-MSL11-APPLI.                                                     
      * DESCRIPTION DES LIGNES DE RVGS4300                                      
          05 COMMSL11-GS43-LIGNE              PIC X(91).                        
      * EVENEMENT E/R/A                                                         
          05 COMMSL11-EVENT                   PIC X(01).                        
      * SENES ARBITRAGE 1 - EXPEDITEUR 2 - DESTINATAIRE 0 PAS ARBITR�E          
          05 COMMSL11-NSEQ                    PIC X(01).                        
      * PARAMETRE APPLI-SL N/O/X (CODE ACTION = PARAMETRE EN SORTIE)            
          05 COMMSL11-APPLI-SL                PIC X(01) VALUE SPACES.           
      * NOMBRE DE LIGNES INSEREES DANS RVSL1000                                 
          05 COMMSL11-NB-LIGNES-SL10          PIC 999 VALUE ZERO.               
      * DOCUMENT                                                                
          05 COMMSL11-SL10-NSOCCRE            PIC X(03) VALUE SPACES.           
          05 COMMSL11-SL10-NLIEUCRE           PIC X(03) VALUE SPACES.           
          05 COMMSL11-SL10-CTYPDOC            PIC X(02) VALUE SPACES.           
          05 COMMSL11-SL10-NUMDOC             PIC S9(07) VALUE ZERO.            
          05 COMMSL11-SL10-NLDOC              PIC S9(05) VALUE ZERO.            
      * CODE-ERREUR                                                             
          05 COMMSL11-CODRET                  PIC X(02).                        
             88 COMMSL11-CODRET-OK                 VALUE '  '.                  
             88 COMMSL11-CODRET-ERREUR             VALUE '01'.                  
          05 COMMSL11-ERREUR                  PIC X(52).                        
      * LIEU MIS A JOUR                                                         
          05 COMMSL11-NSOCMAJ                 PIC X(03).                        
          05 COMMSL11-NLIEUMAJ                PIC X(03).                        
      * CODE ARBITRAGE (EXEMPLE CORRECTION POST MUTATION)                       
          05 COMMSL11-WARB                    PIC X(01).                        
      * ZONES UTILES POUR LA RECEPTION DACEM                                    
JA        05 COMMSL11-SL-NSOCCRE              PIC X(03)  VALUE SPACES.          
JA        05 COMMSL11-SL-NLIEUCRE             PIC X(03)  VALUE SPACES.          
JA        05 COMMSL11-SL-CTYPDOC              PIC X(02)  VALUE SPACES.          
JA        05 COMMSL11-SL-NUMDOC               PIC S9(07) VALUE ZERO.            
JA        05 COMMSL11-SL-NLDOC                PIC S9(05) VALUE ZERO.            
JA        05 COMMSL11-SL-NSOCDREF             PIC X(03)  VALUE SPACES.          
JA        05 COMMSL11-SL-NLIEUDREF            PIC X(03)  VALUE SPACES.          
JA        05 COMMSL11-SL-CTYPDREF             PIC X(02)  VALUE SPACES.          
JA        05 COMMSL11-SL-NUMDREF              PIC S9(07) VALUE ZERO.            
JA        05 COMMSL11-SL-NLDREF               PIC S9(05) VALUE ZERO.            
          05 FILLER                           PIC X(082).                       
                                                                                
