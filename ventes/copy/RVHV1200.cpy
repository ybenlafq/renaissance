      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHV1203                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV1203                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV1200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV1200.                                                            
      *}                                                                        
           02  HV12-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV12-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV12-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV12-NCODICLIE                                                   
               PIC X(0007).                                                     
           02  HV12-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  HV12-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV12-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV12-QPIECESEMP                                                  
               PIC S9(5) COMP-3.                                                
           02  HV12-PCAEMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTACHATSEMP                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTPRIMVOL                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-QPIECESEXC                                                  
               PIC S9(5) COMP-3.                                                
           02  HV12-PCAEXC                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTACHATSEXC                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-QPIECESEXE                                                  
               PIC S9(5) COMP-3.                                                
           02  HV12-PCAEXE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV12-PMTACHATSEXE                                                
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV1203                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV1200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV1200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-NCODICLIE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-NCODICLIE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-QPIECESEMP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PCAEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTACHATSEMP-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTPRIMVOL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-QPIECESEXC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-QPIECESEXC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PCAEXC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PCAEXC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTACHATSEXC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTACHATSEXC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-QPIECESEXE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-QPIECESEXE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PCAEXE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PCAEXE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV12-PMTACHATSEXE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV12-PMTACHATSEXE-F                                              
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
