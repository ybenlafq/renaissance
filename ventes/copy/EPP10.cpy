      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPP10   EPP10                                              00000020
      ***************************************************************** 00000030
       01   EPP10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCIETEI     PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUORIGI    PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLLIEUI   PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMOISL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDMOISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDMOISF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDMOISI   PIC X(6).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTVOLL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MPMONTVOLL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPMONTVOLF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MPMONTVOLI     PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTCAL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MPMONTCAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPMONTCAF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MPMONTCAI      PIC X(7).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTTOTL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MPMONTTOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPMONTTOTF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MPMONTTOTI     PIC X(8).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MZONCMDI  PIC X(15).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIBERRI  PIC X(58).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCODTRAI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCICSI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNETNAMI  PIC X(8).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MSCREENI  PIC X(4).                                       00000690
      ***************************************************************** 00000700
      * SDF: EPP10   EPP10                                              00000710
      ***************************************************************** 00000720
       01   EPP10O REDEFINES EPP10I.                                    00000730
           02 FILLER    PIC X(12).                                      00000740
           02 FILLER    PIC X(2).                                       00000750
           02 MDATJOUA  PIC X.                                          00000760
           02 MDATJOUC  PIC X.                                          00000770
           02 MDATJOUP  PIC X.                                          00000780
           02 MDATJOUH  PIC X.                                          00000790
           02 MDATJOUV  PIC X.                                          00000800
           02 MDATJOUO  PIC X(10).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MTIMJOUA  PIC X.                                          00000830
           02 MTIMJOUC  PIC X.                                          00000840
           02 MTIMJOUP  PIC X.                                          00000850
           02 MTIMJOUH  PIC X.                                          00000860
           02 MTIMJOUV  PIC X.                                          00000870
           02 MTIMJOUO  PIC X(5).                                       00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MNSOCIETEA     PIC X.                                     00000900
           02 MNSOCIETEC     PIC X.                                     00000910
           02 MNSOCIETEP     PIC X.                                     00000920
           02 MNSOCIETEH     PIC X.                                     00000930
           02 MNSOCIETEV     PIC X.                                     00000940
           02 MNSOCIETEO     PIC X(3).                                  00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MNLIEUORIGA    PIC X.                                     00000970
           02 MNLIEUORIGC    PIC X.                                     00000980
           02 MNLIEUORIGP    PIC X.                                     00000990
           02 MNLIEUORIGH    PIC X.                                     00001000
           02 MNLIEUORIGV    PIC X.                                     00001010
           02 MNLIEUORIGO    PIC X(3).                                  00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MNLIEUA   PIC X.                                          00001040
           02 MNLIEUC   PIC X.                                          00001050
           02 MNLIEUP   PIC X.                                          00001060
           02 MNLIEUH   PIC X.                                          00001070
           02 MNLIEUV   PIC X.                                          00001080
           02 MNLIEUO   PIC X(3).                                       00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MLLIEUA   PIC X.                                          00001110
           02 MLLIEUC   PIC X.                                          00001120
           02 MLLIEUP   PIC X.                                          00001130
           02 MLLIEUH   PIC X.                                          00001140
           02 MLLIEUV   PIC X.                                          00001150
           02 MLLIEUO   PIC X(20).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDMOISA   PIC X.                                          00001180
           02 MDMOISC   PIC X.                                          00001190
           02 MDMOISP   PIC X.                                          00001200
           02 MDMOISH   PIC X.                                          00001210
           02 MDMOISV   PIC X.                                          00001220
           02 MDMOISO   PIC X(6).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MPMONTVOLA     PIC X.                                     00001250
           02 MPMONTVOLC     PIC X.                                     00001260
           02 MPMONTVOLP     PIC X.                                     00001270
           02 MPMONTVOLH     PIC X.                                     00001280
           02 MPMONTVOLV     PIC X.                                     00001290
           02 MPMONTVOLO     PIC X(7).                                  00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MPMONTCAA      PIC X.                                     00001320
           02 MPMONTCAC PIC X.                                          00001330
           02 MPMONTCAP PIC X.                                          00001340
           02 MPMONTCAH PIC X.                                          00001350
           02 MPMONTCAV PIC X.                                          00001360
           02 MPMONTCAO      PIC X(7).                                  00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MPMONTTOTA     PIC X.                                     00001390
           02 MPMONTTOTC     PIC X.                                     00001400
           02 MPMONTTOTP     PIC X.                                     00001410
           02 MPMONTTOTH     PIC X.                                     00001420
           02 MPMONTTOTV     PIC X.                                     00001430
           02 MPMONTTOTO     PIC X(8).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MZONCMDA  PIC X.                                          00001460
           02 MZONCMDC  PIC X.                                          00001470
           02 MZONCMDP  PIC X.                                          00001480
           02 MZONCMDH  PIC X.                                          00001490
           02 MZONCMDV  PIC X.                                          00001500
           02 MZONCMDO  PIC X(15).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLIBERRA  PIC X.                                          00001530
           02 MLIBERRC  PIC X.                                          00001540
           02 MLIBERRP  PIC X.                                          00001550
           02 MLIBERRH  PIC X.                                          00001560
           02 MLIBERRV  PIC X.                                          00001570
           02 MLIBERRO  PIC X(58).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCODTRAA  PIC X.                                          00001600
           02 MCODTRAC  PIC X.                                          00001610
           02 MCODTRAP  PIC X.                                          00001620
           02 MCODTRAH  PIC X.                                          00001630
           02 MCODTRAV  PIC X.                                          00001640
           02 MCODTRAO  PIC X(4).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MCICSA    PIC X.                                          00001670
           02 MCICSC    PIC X.                                          00001680
           02 MCICSP    PIC X.                                          00001690
           02 MCICSH    PIC X.                                          00001700
           02 MCICSV    PIC X.                                          00001710
           02 MCICSO    PIC X(5).                                       00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNETNAMA  PIC X.                                          00001740
           02 MNETNAMC  PIC X.                                          00001750
           02 MNETNAMP  PIC X.                                          00001760
           02 MNETNAMH  PIC X.                                          00001770
           02 MNETNAMV  PIC X.                                          00001780
           02 MNETNAMO  PIC X(8).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MSCREENA  PIC X.                                          00001810
           02 MSCREENC  PIC X.                                          00001820
           02 MSCREENP  PIC X.                                          00001830
           02 MSCREENH  PIC X.                                          00001840
           02 MSCREENV  PIC X.                                          00001850
           02 MSCREENO  PIC X(4).                                       00001860
                                                                                
