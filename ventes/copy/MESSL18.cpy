      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:09 >
      
      *****************************************************************
      *   COPY MESSAGES MQ   local --> HOST (MODULE MSL18)            *
      *                                                               *
      *   <<ATTENTION>> MAINTENIR CETTE COPIE EN PHASE AVEC CELLE     *
      *                 DE L'AS/400 (CCBMDSL034)                      *
      *****************************************************************
      *
      * Entete queue (correlid)                                lg=  50 c
       01  WS-MQ10.
           05 WS-QUEUE.
              10 MQ10-MSGID             PIC X(24).
              10 MQ10-CORRELID          PIC X(24).
           05 WS-CODRET                 PIC X(02).
      *
      * Message
           05 WS-MESSAGE.
      * Entete standard darty                                  lg= 105 c
              10 MES-ENTETE.
                 15 MES-TYPE            PIC X(03).
                 15 MES-NSOCMSG         PIC X(03).
                 15 MES-NLIEUMSG        PIC X(03).
                 15 MES-NSOCDST         PIC X(03).
                 15 MES-NLIEUDST        PIC X(03).
                 15 MES-NORD            PIC 9(08).
                 15 MES-LPROG           PIC X(10).
                 15 MES-DJOUR           PIC X(08).
                 15 MES-WSID            PIC X(10).
                 15 MES-USER            PIC X(10).
                 15 MES-CHRONO          PIC 9(07).
                 15 MES-NBRMSG          PIC 9(07).
                 15 MES-NBRENR          PIC 9(05).
                 15 MES-TAILLE          PIC 9(05).
                 15 MES-FILLER          PIC X(20).
      *
      * Donnees applicatives                                   lg= 100 c
              10 MES-DATA.
      * Parametres pour envoi d'une demande de modif de vente
      *          Code fonction (issu du cprog msl11 dans mqput)
                 15 MESSL18-CFONCT         PIC X(03).
      *          Identifiant du document
                 15 MESSL18-NSOCCRE        PIC X(03).
                 15 MESSL18-NLIEUCRE       PIC X(03).
                 15 MESSL18-CTYPDOC        PIC X(02).
                 15 MESSL18-NUMDOC         PIC 9(07).
      *          Date �mission du message
                 15 MESSL18-DEMIS          PIC X(08).
      *          Heure �mission du message
                 15 MESSL18-DHEMIS         PIC X(06).
                 15 MESSL18-FILLER         PIC X(68).
      
