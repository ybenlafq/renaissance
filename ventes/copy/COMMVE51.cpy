      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : STATISTIQUES NMD                                 *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MVE51         *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MVE51-APPLI.                                            00260000
         02 COMM-MVE51-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MVE51-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MVE51-NLIEU          PIC X(03).                      00330000
           05 COMM-MVE51-NVENTE         PIC X(07).                      00340000
           05 COMM-MVE51-DJOUR          PIC X(08).                      00340000
           05 COMM-MVE51-PHOTO          PIC X(01).                      00340000
         02 COMM-MVE51-SORTIE.                                          00260000
           05 COMM-MVE51-CODE-RETOUR    PIC X(01).                      00340000
           05 COMM-MVE51-MESSAGE.                                               
                10 COMM-MVE51-CODRET         PIC X.                             
                   88  COMM-MVE51-OK         VALUE ' '.                         
                   88  COMM-MVE51-ERR        VALUE '1'.                         
                10 COMM-MVE51-LIBERR         PIC X(58).                         
         02 FILLER                    PIC X(005).                       00340000
                                                                                
