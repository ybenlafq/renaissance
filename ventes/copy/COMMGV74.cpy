      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * ----------------------------------------------------------- *           
      * - ZONE DE COMMUNICATION APPEL MODULE MGV74.               - *           
      * -                                                         - *           
      * - VALEURS DU CODE RETOUR :                                - *           
      *   TYPERR ! CODRET  !                                      - *           
      *   00     ! 00      ! OK                                   - *           
      *   10     ! 99      ! PROBL�ME DB2 BLOQUANT + ZONE LIBELLE - *           
      *   20     !         ! PB CICS BLOQUANT      + ZONE LIBELLE - *           
      *   30     !         ! PROBL�ME MQ  BLOQUANT + ZONE LIBELLE - *           
      *   40     ! 01 � 99 ! PROBL�ME APPLICATIF + ZONE LIBELLE   - *           
      *   50     ! 01 � XX ! PB APPEL MODULE SEC + ZONE LIBELLE   - *           
      * ----------------------------------------------------------- *           
        01 COMM-GV74-APPLI.                                                     
           05 COMM-GV74-ZIN.                                                    
              10 COMM-GV74-CPT-ZIN                 PIC 99.                      
              10 COMM-GV74-ZIN-TAB                 OCCURS 10.                   
                 15 COMM-GV74-NSOCIETE             PIC X(03).                   
                 15 COMM-GV74-NLIEU                PIC X(03).                   
                 15 COMM-GV74-NVENTE               PIC X(07).                   
                 15 COMM-GV74-VALCTRL              PIC X(50).                   
           05 COMM-GV74-ZOUT.                                                   
              10 COMM-GV74-ZCODRET.                                             
                 15 COMM-GV74-TYPERR               PIC X(02).                   
                 15 COMM-GV74-CODRET               PIC X(02).                   
              10 COMM-GV74-LIBERR                  PIC X(100).                  
                                                                                
