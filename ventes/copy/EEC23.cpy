      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Contr�le Commandes WebSph�re 1/2                                00000020
      ***************************************************************** 00000030
       01   EEC23I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEUILL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSEUILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSEUILF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSEUILI   PIC X(19).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEAI   PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLREFFOURNI    PIC X(20).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELIGIBLEML    COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MELIGIBLEML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MELIGIBLEMF    PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MELIGIBLEMI    PIC X(15).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MELIGIBLEDL    COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MELIGIBLEDL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MELIGIBLEDF    PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MELIGIBLEDI    PIC X(14).                                 00000490
           02 MDEPOTD OCCURS   2 TIMES .                                00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEPOTL      COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MDEPOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEPOTF      PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MDEPOTI      PIC X(7).                                  00000540
           02 MQSTOCKD OCCURS   2 TIMES .                               00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSTOCKL     COMP PIC S9(4).                            00000560
      *--                                                                       
             03 MQSTOCKL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQSTOCKF     PIC X.                                     00000570
             03 FILLER  PIC X(4).                                       00000580
             03 MQSTOCKI     PIC X(5).                                  00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSARTL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MSARTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSARTF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MSARTI    PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSARTTL   COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MSARTTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSARTTF   PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MSARTTI   PIC X(7).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDEPOTL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MSDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDEPOTF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MSDEPOTI  PIC X(4).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDEPOTTL      COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MSDEPOTTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MSDEPOTTF      PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSDEPOTTI      PIC X(5).                                  00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSEUIL1L  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MSEUIL1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSEUIL1F  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MSEUIL1I  PIC X(5).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSMAGL    COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MSMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSMAGF    PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MSMAGI    PIC X(4).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSMAGTL   COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MSMAGTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSMAGTF   PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MSMAGTI   PIC X(7).                                       00000870
           02 MARTD OCCURS   10 TIMES .                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MARTL   COMP PIC S9(4).                                 00000890
      *--                                                                       
             03 MARTL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MARTF   PIC X.                                          00000900
             03 FILLER  PIC X(4).                                       00000910
             03 MARTI   PIC X(3).                                       00000920
           02 MMAGD OCCURS   10 TIMES .                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMAGL   COMP PIC S9(4).                                 00000940
      *--                                                                       
             03 MMAGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMAGF   PIC X.                                          00000950
             03 FILLER  PIC X(4).                                       00000960
             03 MMAGI   PIC X(3).                                       00000970
           02 MNSOCIETED OCCURS   24 TIMES .                            00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCIETEL   COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MNSOCIETEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MNSOCIETEF   PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MNSOCIETEI   PIC X(3).                                  00001020
           02 MNLIEUD OCCURS   24 TIMES .                               00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00001040
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00001050
             03 FILLER  PIC X(4).                                       00001060
             03 MNLIEUI      PIC X(3).                                  00001070
           02 MLLIEUD OCCURS   24 TIMES .                               00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00001090
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00001100
             03 FILLER  PIC X(4).                                       00001110
             03 MLLIEUI      PIC X(20).                                 00001120
           02 MQUANTITED OCCURS   24 TIMES .                            00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQUANTITEL   COMP PIC S9(4).                            00001140
      *--                                                                       
             03 MQUANTITEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MQUANTITEF   PIC X.                                     00001150
             03 FILLER  PIC X(4).                                       00001160
             03 MQUANTITEI   PIC X(4).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MLIBERRI  PIC X(79).                                      00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MCODTRAI  PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MZONCMDI  PIC X(15).                                      00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MCICSI    PIC X(5).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MNETNAMI  PIC X(8).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MSCREENI  PIC X(4).                                       00001410
      ***************************************************************** 00001420
      * Contr�le Commandes WebSph�re 1/2                                00001430
      ***************************************************************** 00001440
       01   EEC23O REDEFINES EEC23I.                                    00001450
           02 FILLER    PIC X(12).                                      00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MDATJOUA  PIC X.                                          00001480
           02 MDATJOUC  PIC X.                                          00001490
           02 MDATJOUP  PIC X.                                          00001500
           02 MDATJOUH  PIC X.                                          00001510
           02 MDATJOUV  PIC X.                                          00001520
           02 MDATJOUO  PIC X(10).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTIMJOUA  PIC X.                                          00001550
           02 MTIMJOUC  PIC X.                                          00001560
           02 MTIMJOUP  PIC X.                                          00001570
           02 MTIMJOUH  PIC X.                                          00001580
           02 MTIMJOUV  PIC X.                                          00001590
           02 MTIMJOUO  PIC X(5).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MSEUILA   PIC X.                                          00001620
           02 MSEUILC   PIC X.                                          00001630
           02 MSEUILP   PIC X.                                          00001640
           02 MSEUILH   PIC X.                                          00001650
           02 MSEUILV   PIC X.                                          00001660
           02 MSEUILO   PIC X(19).                                      00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MPAGEAA   PIC X.                                          00001690
           02 MPAGEAC   PIC X.                                          00001700
           02 MPAGEAP   PIC X.                                          00001710
           02 MPAGEAH   PIC X.                                          00001720
           02 MPAGEAV   PIC X.                                          00001730
           02 MPAGEAO   PIC Z9.                                         00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNBPA     PIC X.                                          00001760
           02 MNBPC     PIC X.                                          00001770
           02 MNBPP     PIC X.                                          00001780
           02 MNBPH     PIC X.                                          00001790
           02 MNBPV     PIC X.                                          00001800
           02 MNBPO     PIC Z9.                                         00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNCODICA  PIC X.                                          00001830
           02 MNCODICC  PIC X.                                          00001840
           02 MNCODICP  PIC X.                                          00001850
           02 MNCODICH  PIC X.                                          00001860
           02 MNCODICV  PIC X.                                          00001870
           02 MNCODICO  PIC X(7).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MCFAMA    PIC X.                                          00001900
           02 MCFAMC    PIC X.                                          00001910
           02 MCFAMP    PIC X.                                          00001920
           02 MCFAMH    PIC X.                                          00001930
           02 MCFAMV    PIC X.                                          00001940
           02 MCFAMO    PIC X(5).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MCMARQA   PIC X.                                          00001970
           02 MCMARQC   PIC X.                                          00001980
           02 MCMARQP   PIC X.                                          00001990
           02 MCMARQH   PIC X.                                          00002000
           02 MCMARQV   PIC X.                                          00002010
           02 MCMARQO   PIC X(5).                                       00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MLREFFOURNA    PIC X.                                     00002040
           02 MLREFFOURNC    PIC X.                                     00002050
           02 MLREFFOURNP    PIC X.                                     00002060
           02 MLREFFOURNH    PIC X.                                     00002070
           02 MLREFFOURNV    PIC X.                                     00002080
           02 MLREFFOURNO    PIC X(20).                                 00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MELIGIBLEMA    PIC X.                                     00002110
           02 MELIGIBLEMC    PIC X.                                     00002120
           02 MELIGIBLEMP    PIC X.                                     00002130
           02 MELIGIBLEMH    PIC X.                                     00002140
           02 MELIGIBLEMV    PIC X.                                     00002150
           02 MELIGIBLEMO    PIC X(15).                                 00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MELIGIBLEDA    PIC X.                                     00002180
           02 MELIGIBLEDC    PIC X.                                     00002190
           02 MELIGIBLEDP    PIC X.                                     00002200
           02 MELIGIBLEDH    PIC X.                                     00002210
           02 MELIGIBLEDV    PIC X.                                     00002220
           02 MELIGIBLEDO    PIC X(14).                                 00002230
           02 DFHMS1 OCCURS   2 TIMES .                                 00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MDEPOTA      PIC X.                                     00002260
             03 MDEPOTC PIC X.                                          00002270
             03 MDEPOTP PIC X.                                          00002280
             03 MDEPOTH PIC X.                                          00002290
             03 MDEPOTV PIC X.                                          00002300
             03 MDEPOTO      PIC X(7).                                  00002310
           02 DFHMS2 OCCURS   2 TIMES .                                 00002320
             03 FILLER       PIC X(2).                                  00002330
             03 MQSTOCKA     PIC X.                                     00002340
             03 MQSTOCKC     PIC X.                                     00002350
             03 MQSTOCKP     PIC X.                                     00002360
             03 MQSTOCKH     PIC X.                                     00002370
             03 MQSTOCKV     PIC X.                                     00002380
             03 MQSTOCKO     PIC ----9.                                 00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MSARTA    PIC X.                                          00002410
           02 MSARTC    PIC X.                                          00002420
           02 MSARTP    PIC X.                                          00002430
           02 MSARTH    PIC X.                                          00002440
           02 MSARTV    PIC X.                                          00002450
           02 MSARTO    PIC ---9.                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MSARTTA   PIC X.                                          00002480
           02 MSARTTC   PIC X.                                          00002490
           02 MSARTTP   PIC X.                                          00002500
           02 MSARTTH   PIC X.                                          00002510
           02 MSARTTV   PIC X.                                          00002520
           02 MSARTTO   PIC X(7).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MSDEPOTA  PIC X.                                          00002550
           02 MSDEPOTC  PIC X.                                          00002560
           02 MSDEPOTP  PIC X.                                          00002570
           02 MSDEPOTH  PIC X.                                          00002580
           02 MSDEPOTV  PIC X.                                          00002590
           02 MSDEPOTO  PIC ---9.                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MSDEPOTTA      PIC X.                                     00002620
           02 MSDEPOTTC PIC X.                                          00002630
           02 MSDEPOTTP PIC X.                                          00002640
           02 MSDEPOTTH PIC X.                                          00002650
           02 MSDEPOTTV PIC X.                                          00002660
           02 MSDEPOTTO      PIC X(5).                                  00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MSEUIL1A  PIC X.                                          00002690
           02 MSEUIL1C  PIC X.                                          00002700
           02 MSEUIL1P  PIC X.                                          00002710
           02 MSEUIL1H  PIC X.                                          00002720
           02 MSEUIL1V  PIC X.                                          00002730
           02 MSEUIL1O  PIC ----9.                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MSMAGA    PIC X.                                          00002760
           02 MSMAGC    PIC X.                                          00002770
           02 MSMAGP    PIC X.                                          00002780
           02 MSMAGH    PIC X.                                          00002790
           02 MSMAGV    PIC X.                                          00002800
           02 MSMAGO    PIC ---9.                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MSMAGTA   PIC X.                                          00002830
           02 MSMAGTC   PIC X.                                          00002840
           02 MSMAGTP   PIC X.                                          00002850
           02 MSMAGTH   PIC X.                                          00002860
           02 MSMAGTV   PIC X.                                          00002870
           02 MSMAGTO   PIC X(7).                                       00002880
           02 DFHMS3 OCCURS   10 TIMES .                                00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MARTA   PIC X.                                          00002910
             03 MARTC   PIC X.                                          00002920
             03 MARTP   PIC X.                                          00002930
             03 MARTH   PIC X.                                          00002940
             03 MARTV   PIC X.                                          00002950
             03 MARTO   PIC ZZ9.                                        00002960
           02 DFHMS4 OCCURS   10 TIMES .                                00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MMAGA   PIC X.                                          00002990
             03 MMAGC   PIC X.                                          00003000
             03 MMAGP   PIC X.                                          00003010
             03 MMAGH   PIC X.                                          00003020
             03 MMAGV   PIC X.                                          00003030
             03 MMAGO   PIC ZZ9.                                        00003040
           02 DFHMS5 OCCURS   24 TIMES .                                00003050
             03 FILLER       PIC X(2).                                  00003060
             03 MNSOCIETEA   PIC X.                                     00003070
             03 MNSOCIETEC   PIC X.                                     00003080
             03 MNSOCIETEP   PIC X.                                     00003090
             03 MNSOCIETEH   PIC X.                                     00003100
             03 MNSOCIETEV   PIC X.                                     00003110
             03 MNSOCIETEO   PIC X(3).                                  00003120
           02 DFHMS6 OCCURS   24 TIMES .                                00003130
             03 FILLER       PIC X(2).                                  00003140
             03 MNLIEUA      PIC X.                                     00003150
             03 MNLIEUC PIC X.                                          00003160
             03 MNLIEUP PIC X.                                          00003170
             03 MNLIEUH PIC X.                                          00003180
             03 MNLIEUV PIC X.                                          00003190
             03 MNLIEUO      PIC X(3).                                  00003200
           02 DFHMS7 OCCURS   24 TIMES .                                00003210
             03 FILLER       PIC X(2).                                  00003220
             03 MLLIEUA      PIC X.                                     00003230
             03 MLLIEUC PIC X.                                          00003240
             03 MLLIEUP PIC X.                                          00003250
             03 MLLIEUH PIC X.                                          00003260
             03 MLLIEUV PIC X.                                          00003270
             03 MLLIEUO      PIC X(20).                                 00003280
           02 DFHMS8 OCCURS   24 TIMES .                                00003290
             03 FILLER       PIC X(2).                                  00003300
             03 MQUANTITEA   PIC X.                                     00003310
             03 MQUANTITEC   PIC X.                                     00003320
             03 MQUANTITEP   PIC X.                                     00003330
             03 MQUANTITEH   PIC X.                                     00003340
             03 MQUANTITEV   PIC X.                                     00003350
             03 MQUANTITEO   PIC ---9.                                  00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MLIBERRA  PIC X.                                          00003380
           02 MLIBERRC  PIC X.                                          00003390
           02 MLIBERRP  PIC X.                                          00003400
           02 MLIBERRH  PIC X.                                          00003410
           02 MLIBERRV  PIC X.                                          00003420
           02 MLIBERRO  PIC X(79).                                      00003430
           02 FILLER    PIC X(2).                                       00003440
           02 MCODTRAA  PIC X.                                          00003450
           02 MCODTRAC  PIC X.                                          00003460
           02 MCODTRAP  PIC X.                                          00003470
           02 MCODTRAH  PIC X.                                          00003480
           02 MCODTRAV  PIC X.                                          00003490
           02 MCODTRAO  PIC X(4).                                       00003500
           02 FILLER    PIC X(2).                                       00003510
           02 MZONCMDA  PIC X.                                          00003520
           02 MZONCMDC  PIC X.                                          00003530
           02 MZONCMDP  PIC X.                                          00003540
           02 MZONCMDH  PIC X.                                          00003550
           02 MZONCMDV  PIC X.                                          00003560
           02 MZONCMDO  PIC X(15).                                      00003570
           02 FILLER    PIC X(2).                                       00003580
           02 MCICSA    PIC X.                                          00003590
           02 MCICSC    PIC X.                                          00003600
           02 MCICSP    PIC X.                                          00003610
           02 MCICSH    PIC X.                                          00003620
           02 MCICSV    PIC X.                                          00003630
           02 MCICSO    PIC X(5).                                       00003640
           02 FILLER    PIC X(2).                                       00003650
           02 MNETNAMA  PIC X.                                          00003660
           02 MNETNAMC  PIC X.                                          00003670
           02 MNETNAMP  PIC X.                                          00003680
           02 MNETNAMH  PIC X.                                          00003690
           02 MNETNAMV  PIC X.                                          00003700
           02 MNETNAMO  PIC X(8).                                       00003710
           02 FILLER    PIC X(2).                                       00003720
           02 MSCREENA  PIC X.                                          00003730
           02 MSCREENC  PIC X.                                          00003740
           02 MSCREENP  PIC X.                                          00003750
           02 MSCREENH  PIC X.                                          00003760
           02 MSCREENV  PIC X.                                          00003770
           02 MSCREENO  PIC X(4).                                       00003780
                                                                                
