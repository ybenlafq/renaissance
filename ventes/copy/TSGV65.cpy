      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS POUR LA GESTION DES BES SPECIFIQUE A TGV65            * 00000030
      *      NOM: 'GV65' + SOCIETE                                    * 00000090
      *      LG :  15  C                                              * 00000091
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV65-IDENTIFICATEUR.                                              
           05  TS-GV65-NOM          PIC    X(4)  VALUE  'GV65'.                 
           05  TS-GV65-NSOC         PIC    X(3).                                
      *                                                               * 00000094
       01  TS-GV65.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
           05 TS-GV65-LONG               PIC S9(5) COMP-3 VALUE +15.    00000097
      ********************************** DONNEES                        00000098
           05 TS-GV65-DONNEES.                                          00000099
              10 TS-GV65-DATCREAT        PIC X(8).                      00000100
              10 TS-GV65-MAXNBE          PIC 9(7).                      00000100
                                                                                
