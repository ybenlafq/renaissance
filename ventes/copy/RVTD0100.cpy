      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      * COBOL DECLARATION FOR TABLE RVTD0100                           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTD0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTD0100.                                                            
      *}                                                                        
           10 TD01-NSOCIETE        PIC X(3).                                    
           10 TD01-NLIEU           PIC X(3).                                    
           10 TD01-NVENTE          PIC X(7).                                    
           10 TD01-NDOSSIER        PIC S9(3)V USAGE COMP-3.                     
           10 TD01-NSEQ            PIC S9(3)V USAGE COMP-3.                     
           10 TD01-CCTRL           PIC X(5).                                    
           10 TD01-LGCTRL          PIC S9(3)V USAGE COMP-3.                     
           10 TD01-VALCTRL         PIC X(50).                                   
           10 TD01-CAUTOR          PIC X(5).                                    
           10 TD01-CJUSTIF         PIC X(5).                                    
           10 TD01-DMODIF          PIC X(8).                                    
           10 TD01-DSYST           PIC S9(13)V USAGE COMP-3.                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 12      *        
      ******************************************************************        
                                                                                
