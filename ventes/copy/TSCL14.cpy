      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-CL14-LONG            PIC  S9(4)  COMP-3  VALUE  +98.              
       01  TS-CL14-RECORD.                                                      
           02  TS-CL14-CTYPDOC1    PIC  X(02).                                  
           02  TS-CL14-COPAR1      PIC  X(05).                                  
           02  TS-CL14-LOPER       PIC  X(30).                                  
           02  TS-CL14-CPROG       PIC  X(10).                                  
           02  TS-CL14-COPER       PIC  X(10).                                  
           02  TS-CL14-CTYPDOC     PIC  X(02).                                  
           02  TS-CL14-COPAR       PIC  X(10).                                  
           02  TS-CL14-EVENT       PIC  X(01).                                  
           02  TS-CL14-NSEQ        PIC  X(01).                                  
           02  TS-CL14-WHOST       PIC  X(01).                                  
           02  TS-CL14-WEXIST      PIC  X(01).                                  
           02  TS-CL14-NSOCO       PIC  X(03).                                  
           02  TS-CL14-NLIEUO      PIC  X(03).                                  
           02  TS-CL14-NSLIEUO     PIC  X(03).                                  
           02  TS-CL14-CLITRTO     PIC  X(03).                                  
           02  TS-CL14-NSOCD       PIC  X(03).                                  
           02  TS-CL14-NLIEUD      PIC  X(03).                                  
           02  TS-CL14-NSLIEUD     PIC  X(03).                                  
           02  TS-CL14-CLITRTD     PIC  X(03).                                  
           02  TS-CL14-CDSUPP      PIC  X(01).                                  
                                                                                
