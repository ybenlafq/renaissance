      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM145 AU 29/12/2004   *       
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,20,BI,A,                          *        
      *                           45,20,BI,A,                          *        
      *                           65,20,BI,A,                          *        
      *                           85,20,BI,A,                          *        
      *                           05,02,PD,A,                          *        
      *                           07,01,PD,A,                          *        
      *                           08,07,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-:ISM145:.                                                      
            05 NOMETAT-:ISM145:         PIC X(6) VALUE 'ISM145'.                
            05 RUPTURES-:ISM145:.                                       001  006
           10 :ISM145:-NSOCIETE         PIC X(03).                      007  003
           10 :ISM145:-GRP              PIC X(02).                      010  002
           10 :ISM145:-CHEFPROD         PIC X(05).                      012  005
           10 :ISM145:-WSEQFAM          PIC S9(05)      COMP-3.         017  003
           10 :ISM145:-CFAM             PIC X(05).                      020  005
           10 :ISM145:-LFAM             PIC X(20).                      025  020
           10 :ISM145:-LVMARKET1        PIC X(20).                      045  020
           10 :ISM145:-LVMARKET2        PIC X(20).                      065  020
           10 :ISM145:-LVMARKET3        PIC X(20).                      085  020
           10 :ISM145:-RANGGPE          PIC S9(03)      COMP-3.         105  002
           10 :ISM145:-ORDRE            PIC S9(01)      COMP-3.         107  001
           10 :ISM145:-NCODICGRP        PIC X(07).                      108  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM145-SEQUENCE         PIC S9(04) COMP.                          
      *--                                                                       
           10 :ISM145:-SEQUENCE         PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-:ISM145:.                                                 
           10 :ISM145:-CMARQ            PIC X(05).                      117  005
           10 :ISM145:-LIBDATE          PIC X(12).                      122  012
           10 :ISM145:-LIBELLE          PIC X(10).                      134  010
           10 :ISM145:-LREFFOURN        PIC X(20).                      144  020
           10 :ISM145:-LSTATCOMP        PIC X(03).                      164  003
           10 :ISM145:-LSTATCOMP2       PIC X(03).                      167  003
            07 ISM145-GAMME.                                                    
           10 :ISM145:-GAMME1           PIC X(05).                      170  005
           10 :ISM145:-GAMME2           PIC X(05).                      175  005
           10 :ISM145:-GAMME3           PIC X(05).                      180  005
           10 :ISM145:-GAMME4           PIC X(05).                      185  005
           10 :ISM145:-GAMME5           PIC X(05).                      190  005
           10 :ISM145:-GAMME6           PIC X(05).                      195  005
           10 :ISM145:-GAMME7           PIC X(05).                      200  005
           10 :ISM145:-GAMME8           PIC X(05).                      205  005
           10 :ISM145:-GAMME9           PIC X(05).                      210  005
           10 :ISM145:-GAMME10          PIC X(05).                      215  005
           10 :ISM145:-GAMME11          PIC X(05).                      220  005
           10 :ISM145:-GAMME12          PIC X(05).                      225  005
           10 :ISM145:-GAMME13          PIC X(05).                      230  005
           10 :ISM145:-GAMME14          PIC X(05).                      235  005
           10 :ISM145:-GAMME15          PIC X(05).                      240  005
           10 :ISM145:-GAMME16          PIC X(05).                      245  005
           10 :ISM145:-GAMME17          PIC X(05).                      250  005
           10 :ISM145:-GAMME18          PIC X(05).                      255  005
           10 :ISM145:-GAMME19          PIC X(05).                      260  005
            07 ISM145-TGAMME REDEFINES ISM145-GAMME.                            
               10 ISM145-GAM            PIC X(05) OCCURS 19.                    
            07 ISM145-MAG.                                                      
           10 :ISM145:-MAG1             PIC X(03).                      265  003
           10 :ISM145:-MAG2             PIC X(03).                      268  003
           10 :ISM145:-MAG3             PIC X(03).                      271  003
           10 :ISM145:-MAG4             PIC X(03).                      274  003
           10 :ISM145:-MAG5             PIC X(03).                      277  003
           10 :ISM145:-MAG6             PIC X(03).                      280  003
           10 :ISM145:-MAG7             PIC X(03).                      283  003
           10 :ISM145:-MAG8             PIC X(03).                      286  003
           10 :ISM145:-MAG9             PIC X(03).                      289  003
           10 :ISM145:-MAG10            PIC X(03).                      292  003
           10 :ISM145:-MAG11            PIC X(03).                      295  003
           10 :ISM145:-MAG12            PIC X(03).                      298  003
           10 :ISM145:-MAG13            PIC X(03).                      301  003
           10 :ISM145:-MAG14            PIC X(03).                      304  003
           10 :ISM145:-MAG15            PIC X(03).                      307  003
           10 :ISM145:-MAG16            PIC X(03).                      310  003
           10 :ISM145:-MAG17            PIC X(03).                      313  003
           10 :ISM145:-MAG18            PIC X(03).                      316  003
           10 :ISM145:-MAG19            PIC X(03).                      319  003
            07 ISM145-TMAG   REDEFINES ISM145-MAG.                              
               10 ISM145-MG             PIC X(03) OCCURS 19.                    
            07 ISM145-QSTOCKMAG.                                                
           10 :ISM145:-QSTOCKMAG1       PIC S9(04)      COMP-3.         322  003
           10 :ISM145:-QSTOCKMAG2       PIC S9(04)      COMP-3.         325  003
           10 :ISM145:-QSTOCKMAG3       PIC S9(04)      COMP-3.         328  003
           10 :ISM145:-QSTOCKMAG4       PIC S9(04)      COMP-3.         331  003
           10 :ISM145:-QSTOCKMAG5       PIC S9(04)      COMP-3.         334  003
           10 :ISM145:-QSTOCKMAG6       PIC S9(04)      COMP-3.         337  003
           10 :ISM145:-QSTOCKMAG7       PIC S9(04)      COMP-3.         340  003
           10 :ISM145:-QSTOCKMAG8       PIC S9(04)      COMP-3.         343  003
           10 :ISM145:-QSTOCKMAG9       PIC S9(04)      COMP-3.         346  003
           10 :ISM145:-QSTOCKMAG10      PIC S9(04)      COMP-3.         349  003
           10 :ISM145:-QSTOCKMAG11      PIC S9(04)      COMP-3.         352  003
           10 :ISM145:-QSTOCKMAG12      PIC S9(04)      COMP-3.         355  003
           10 :ISM145:-QSTOCKMAG13      PIC S9(04)      COMP-3.         358  003
           10 :ISM145:-QSTOCKMAG14      PIC S9(04)      COMP-3.         361  003
           10 :ISM145:-QSTOCKMAG15      PIC S9(04)      COMP-3.         364  003
           10 :ISM145:-QSTOCKMAG16      PIC S9(04)      COMP-3.         367  003
           10 :ISM145:-QSTOCKMAG17      PIC S9(04)      COMP-3.         370  003
           10 :ISM145:-QSTOCKMAG18      PIC S9(04)      COMP-3.         373  003
           10 :ISM145:-QSTOCKMAG19      PIC S9(04)      COMP-3.         376  003
            07 ISM145-TQSTOCKMAG REDEFINES ISM145-QSTOCKMAG.                    
               10 ISM145-QSTOCK       PIC S9(04)      COMP-3 OCCURS 19.         
            07 ISM145-QVTE.                                                     
           10 :ISM145:-QVTE1            PIC S9(04)      COMP-3.         379  003
           10 :ISM145:-QVTE2            PIC S9(04)      COMP-3.         382  003
           10 :ISM145:-QVTE3            PIC S9(04)      COMP-3.         385  003
           10 :ISM145:-QVTE4            PIC S9(04)      COMP-3.         388  003
           10 :ISM145:-QVTE5            PIC S9(04)      COMP-3.         391  003
           10 :ISM145:-QVTE6            PIC S9(04)      COMP-3.         394  003
           10 :ISM145:-QVTE7            PIC S9(04)      COMP-3.         397  003
           10 :ISM145:-QVTE8            PIC S9(04)      COMP-3.         400  003
           10 :ISM145:-QVTE9            PIC S9(04)      COMP-3.         403  003
           10 :ISM145:-QVTE10           PIC S9(04)      COMP-3.         406  003
           10 :ISM145:-QVTE11           PIC S9(04)      COMP-3.         409  003
           10 :ISM145:-QVTE12           PIC S9(04)      COMP-3.         412  003
           10 :ISM145:-QVTE13           PIC S9(04)      COMP-3.         415  003
           10 :ISM145:-QVTE14           PIC S9(04)      COMP-3.         418  003
           10 :ISM145:-QVTE15           PIC S9(04)      COMP-3.         421  003
           10 :ISM145:-QVTE16           PIC S9(04)      COMP-3.         424  003
           10 :ISM145:-QVTE17           PIC S9(04)      COMP-3.         427  003
           10 :ISM145:-QVTE18           PIC S9(04)      COMP-3.         430  003
           10 :ISM145:-QVTE19           PIC S9(04)      COMP-3.         433  003
            07 ISM145-TQVTE      REDEFINES ISM145-QVTE.                         
             10 ISM145-VTE            PIC S9(04)      COMP-3 OCCURS 19.         
            07 ISM145-AUTRES.                                                   
           10 :ISM145:-NCODIC           PIC X(07).                      436  007
           10 :ISM145:-NZONPRIX         PIC X(02).                      443  002
           10 :ISM145:-RANGGPEAFF       PIC X(03).                      445  003
           10 :ISM145:-RANGSOCAFF       PIC X(03).                      448  003
           10 :ISM145:-WCDE             PIC X(01).                      451  001
           10 :ISM145:-WOA              PIC X(01).                      452  001
           10 :ISM145:-WSENSAPPRO       PIC X(01).                      453  001
           10 :ISM145:-LGDRAYON         PIC X(02).                      454  002
           10 :ISM145:-PRIME            PIC S9(04)V9(2) COMP-3.         456  004
           10 :ISM145:-PSTDTTC          PIC S9(06)V9(2) COMP-3.         460  005
           10 :ISM145:-PVREFNAT         PIC S9(06)V9(2) COMP-3.         465  005
           10 :ISM145:-QCDE             PIC S9(05)      COMP-3.         470  003
           10 :ISM145:-QSTOCKDEP        PIC S9(04)      COMP-3.         473  003
           10 :ISM145:-QSTOCKMAGTOT     PIC S9(05)      COMP-3.         476  003
           10 :ISM145:-QVTETOT          PIC S9(05)      COMP-3.         479  003
AL1008     10 :ISM145:-MARGE            PIC S9(03)V9    COMP-3.         479  003
AL1008     10 :ISM145:-TXMARGE          PIC S9(03)V9    COMP-3.         479  003
            05 FILLER                      PIC X(025).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISM145-LONG         PIC S9(4)   COMP  VALUE +487.             
      *                                                                         
      *--                                                                       
        01  DSECT-:ISM145:-LONG         PIC S9(4) COMP-5  VALUE +487.           
                                                                                
      *}                                                                        
