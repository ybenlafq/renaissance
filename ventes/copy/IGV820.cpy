      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV820 AU 28/01/1997  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,07,BI,A,                          *        
      *                           19,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV820.                                                        
            05 NOMETAT-IGV820           PIC X(6) VALUE 'IGV820'.                
            05 RUPTURES-IGV820.                                                 
           10 IGV820-NLIEU              PIC X(03).                      007  003
           10 IGV820-WCATEG-ACOMPTE     PIC X(02).                      010  002
           10 IGV820-NVENTE             PIC X(07).                      012  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV820-SEQUENCE           PIC S9(04) COMP.                019  002
      *--                                                                       
           10 IGV820-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV820.                                                   
           10 IGV820-CAPPRO             PIC X(05).                      021  005
           10 IGV820-CFAM               PIC X(05).                      026  005
           10 IGV820-CMARQ              PIC X(05).                      031  005
           10 IGV820-CMODDEL            PIC X(03).                      036  003
           10 IGV820-NCODIC             PIC X(07).                      039  007
           10 IGV820-NSOCIETE           PIC X(03).                      046  003
           10 IGV820-POUMAX             PIC 9(03)     .                 049  003
           10 IGV820-POUMIN             PIC 9(03)     .                 052  003
           10 IGV820-POURC-ACOMPTE      PIC 9(03)V9(2).                 055  005
           10 IGV820-PCOMPT             PIC S9(07)V9(2) COMP-3.         060  005
           10 IGV820-PTTVENTE           PIC S9(07)V9(2) COMP-3.         065  005
           10 IGV820-QVENDUE            PIC S9(05)      COMP-3.         070  003
           10 IGV820-DDELIV             PIC X(08).                      073  008
            05 FILLER                      PIC X(432).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGV820-LONG           PIC S9(4)   COMP  VALUE +080.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGV820-LONG           PIC S9(4) COMP-5  VALUE +080.           
                                                                                
      *}                                                                        
