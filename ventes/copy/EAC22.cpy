      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAC22   EAC22                                              00000020
      ***************************************************************** 00000030
       01   EAC22I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCL      COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MLCL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MLCF      PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MLCI      PIC X(3).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJOURL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MJOURL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJOURF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MJOURI    PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MCSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSOCF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MCSOCI    PIC X(3).                                       00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MLSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSOCF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MLSOCI    PIC X(21).                                      00000350
           02 FILLER  OCCURS   10 TIMES .                               00000360
      * code magasin                                                    00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMCMAGL      COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MMCMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMCMAGF      PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MMCMAGI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMLMAGL      COMP PIC S9(4).                            00000420
      *--                                                                       
             03 MMLMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMLMAGF      PIC X.                                     00000430
             03 FILLER  PIC X(4).                                       00000440
             03 MMLMAGI      PIC X(9).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMCATLML     COMP PIC S9(4).                            00000460
      *--                                                                       
             03 MMCATLML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMCATLMF     PIC X.                                     00000470
             03 FILLER  PIC X(4).                                       00000480
             03 MMCATLMI     PIC X(7).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMCAELAL     COMP PIC S9(4).                            00000500
      *--                                                                       
             03 MMCAELAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMCAELAF     PIC X.                                     00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MMCAELAI     PIC X(7).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMCADACL     COMP PIC S9(4).                            00000540
      *--                                                                       
             03 MMCADACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMCADACF     PIC X.                                     00000550
             03 FILLER  PIC X(4).                                       00000560
             03 MMCADACI     PIC X(6).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMCATOTL     COMP PIC S9(4).                            00000580
      *--                                                                       
             03 MMCATOTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMCATOTF     PIC X.                                     00000590
             03 FILLER  PIC X(4).                                       00000600
             03 MMCATOTI     PIC X(7).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNBTLML     COMP PIC S9(4).                            00000620
      *--                                                                       
             03 MMNBTLML COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMNBTLMF     PIC X.                                     00000630
             03 FILLER  PIC X(4).                                       00000640
             03 MMNBTLMI     PIC X(4).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNBELAL     COMP PIC S9(4).                            00000660
      *--                                                                       
             03 MMNBELAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMNBELAF     PIC X.                                     00000670
             03 FILLER  PIC X(4).                                       00000680
             03 MMNBELAI     PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNBDACL     COMP PIC S9(4).                            00000700
      *--                                                                       
             03 MMNBDACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMNBDACF     PIC X.                                     00000710
             03 FILLER  PIC X(4).                                       00000720
             03 MMNBDACI     PIC X(4).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTRDARL     COMP PIC S9(4).                            00000740
      *--                                                                       
             03 MMTRDARL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMTRDARF     PIC X.                                     00000750
             03 FILLER  PIC X(4).                                       00000760
             03 MMTRDARI     PIC X(4).                                  00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMTRDACL     COMP PIC S9(4).                            00000780
      *--                                                                       
             03 MMTRDACL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMTRDACF     PIC X.                                     00000790
             03 FILLER  PIC X(4).                                       00000800
             03 MMTRDACI     PIC X(4).                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNBENTL     COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MMNBENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMNBENTF     PIC X.                                     00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MMNBENTI     PIC X(4).                                  00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMNBVDL      COMP PIC S9(4).                            00000860
      *--                                                                       
             03 MMNBVDL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMNBVDF      PIC X.                                     00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MMNBVDI      PIC X(4).                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMAGL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCMAGF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCMAGI    PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGL    COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MLMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLMAGF    PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MLMAGI    PIC X(9).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATLML   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCATLML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCATLMF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCATLMI   PIC X(7).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAELAL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MCAELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCAELAF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MCAELAI   PIC X(7).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCADACL   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MCADACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCADACF   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MCADACI   PIC X(6).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCATOTL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCATOTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCATOTF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCATOTI   PIC X(7).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBTLML   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MNBTLML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBTLMF   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MNBTLMI   PIC X(4).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBELAL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNBELAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBELAF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNBELAI   PIC X(4).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBDACL   COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MNBDACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBDACF   PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MNBDACI   PIC X(4).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRDARL   COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MTRDARL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTRDARF   PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MTRDARI   PIC X(4).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTRDACL   COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MTRDACL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTRDACF   PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MTRDACI   PIC X(4).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBENTL   COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MNBENTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBENTF   PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MNBENTI   PIC X(4).                                       00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBVDL    COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MNBVDL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNBVDF    PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNBVDI    PIC X(4).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MZONCMDI  PIC X(15).                                      00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLIBERRI  PIC X(54).                                      00001490
      * CODE TRANSACTION                                                00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MCODTRAI  PIC X(4).                                       00001540
      * NOM DU CICS                                                     00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MCICSI    PIC X(5).                                       00001590
      * NETNAME                                                         00001600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001610
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001620
           02 FILLER    PIC X(4).                                       00001630
           02 MNETNAMI  PIC X(8).                                       00001640
      * CODE TERMINAL                                                   00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MSCREENI  PIC X(5).                                       00001690
      ***************************************************************** 00001700
      * SDF: EAC22   EAC22                                              00001710
      ***************************************************************** 00001720
       01   EAC22O REDEFINES EAC22I.                                    00001730
           02 FILLER    PIC X(12).                                      00001740
      * DATE DU JOUR                                                    00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDATJOUA  PIC X.                                          00001770
           02 MDATJOUC  PIC X.                                          00001780
           02 MDATJOUP  PIC X.                                          00001790
           02 MDATJOUH  PIC X.                                          00001800
           02 MDATJOUV  PIC X.                                          00001810
           02 MDATJOUO  PIC X(10).                                      00001820
      * HEURE                                                           00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MTIMJOUA  PIC X.                                          00001850
           02 MTIMJOUC  PIC X.                                          00001860
           02 MTIMJOUP  PIC X.                                          00001870
           02 MTIMJOUH  PIC X.                                          00001880
           02 MTIMJOUV  PIC X.                                          00001890
           02 MTIMJOUO  PIC X(5).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MPAGEA    PIC X.                                          00001920
           02 MPAGEC    PIC X.                                          00001930
           02 MPAGEP    PIC X.                                          00001940
           02 MPAGEH    PIC X.                                          00001950
           02 MPAGEV    PIC X.                                          00001960
           02 MPAGEO    PIC X(3).                                       00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLCA      PIC X.                                          00001990
           02 MLCC      PIC X.                                          00002000
           02 MLCP      PIC X.                                          00002010
           02 MLCH      PIC X.                                          00002020
           02 MLCV      PIC X.                                          00002030
           02 MLCO      PIC X(3).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MJOURA    PIC X.                                          00002060
           02 MJOURC    PIC X.                                          00002070
           02 MJOURP    PIC X.                                          00002080
           02 MJOURH    PIC X.                                          00002090
           02 MJOURV    PIC X.                                          00002100
           02 MJOURO    PIC X(10).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MCSOCA    PIC X.                                          00002130
           02 MCSOCC    PIC X.                                          00002140
           02 MCSOCP    PIC X.                                          00002150
           02 MCSOCH    PIC X.                                          00002160
           02 MCSOCV    PIC X.                                          00002170
           02 MCSOCO    PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLSOCA    PIC X.                                          00002200
           02 MLSOCC    PIC X.                                          00002210
           02 MLSOCP    PIC X.                                          00002220
           02 MLSOCH    PIC X.                                          00002230
           02 MLSOCV    PIC X.                                          00002240
           02 MLSOCO    PIC X(21).                                      00002250
           02 FILLER  OCCURS   10 TIMES .                               00002260
      * code magasin                                                    00002270
             03 FILLER       PIC X(2).                                  00002280
             03 MMCMAGA      PIC X.                                     00002290
             03 MMCMAGC PIC X.                                          00002300
             03 MMCMAGP PIC X.                                          00002310
             03 MMCMAGH PIC X.                                          00002320
             03 MMCMAGV PIC X.                                          00002330
             03 MMCMAGO      PIC X(3).                                  00002340
             03 FILLER       PIC X(2).                                  00002350
             03 MMLMAGA      PIC X.                                     00002360
             03 MMLMAGC PIC X.                                          00002370
             03 MMLMAGP PIC X.                                          00002380
             03 MMLMAGH PIC X.                                          00002390
             03 MMLMAGV PIC X.                                          00002400
             03 MMLMAGO      PIC X(9).                                  00002410
             03 FILLER       PIC X(2).                                  00002420
             03 MMCATLMA     PIC X.                                     00002430
             03 MMCATLMC     PIC X.                                     00002440
             03 MMCATLMP     PIC X.                                     00002450
             03 MMCATLMH     PIC X.                                     00002460
             03 MMCATLMV     PIC X.                                     00002470
             03 MMCATLMO     PIC ZZZZZZ9.                               00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MMCAELAA     PIC X.                                     00002500
             03 MMCAELAC     PIC X.                                     00002510
             03 MMCAELAP     PIC X.                                     00002520
             03 MMCAELAH     PIC X.                                     00002530
             03 MMCAELAV     PIC X.                                     00002540
             03 MMCAELAO     PIC ZZZZZZ9.                               00002550
             03 FILLER       PIC X(2).                                  00002560
             03 MMCADACA     PIC X.                                     00002570
             03 MMCADACC     PIC X.                                     00002580
             03 MMCADACP     PIC X.                                     00002590
             03 MMCADACH     PIC X.                                     00002600
             03 MMCADACV     PIC X.                                     00002610
             03 MMCADACO     PIC ZZZZZ9.                                00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MMCATOTA     PIC X.                                     00002640
             03 MMCATOTC     PIC X.                                     00002650
             03 MMCATOTP     PIC X.                                     00002660
             03 MMCATOTH     PIC X.                                     00002670
             03 MMCATOTV     PIC X.                                     00002680
             03 MMCATOTO     PIC ZZZZZZ9.                               00002690
             03 FILLER       PIC X(2).                                  00002700
             03 MMNBTLMA     PIC X.                                     00002710
             03 MMNBTLMC     PIC X.                                     00002720
             03 MMNBTLMP     PIC X.                                     00002730
             03 MMNBTLMH     PIC X.                                     00002740
             03 MMNBTLMV     PIC X.                                     00002750
             03 MMNBTLMO     PIC ZZZ9.                                  00002760
             03 FILLER       PIC X(2).                                  00002770
             03 MMNBELAA     PIC X.                                     00002780
             03 MMNBELAC     PIC X.                                     00002790
             03 MMNBELAP     PIC X.                                     00002800
             03 MMNBELAH     PIC X.                                     00002810
             03 MMNBELAV     PIC X.                                     00002820
             03 MMNBELAO     PIC ZZZ9.                                  00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MMNBDACA     PIC X.                                     00002850
             03 MMNBDACC     PIC X.                                     00002860
             03 MMNBDACP     PIC X.                                     00002870
             03 MMNBDACH     PIC X.                                     00002880
             03 MMNBDACV     PIC X.                                     00002890
             03 MMNBDACO     PIC ZZZ9.                                  00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MMTRDARA     PIC X.                                     00002920
             03 MMTRDARC     PIC X.                                     00002930
             03 MMTRDARP     PIC X.                                     00002940
             03 MMTRDARH     PIC X.                                     00002950
             03 MMTRDARV     PIC X.                                     00002960
             03 MMTRDARO     PIC ZZZ9.                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MMTRDACA     PIC X.                                     00002990
             03 MMTRDACC     PIC X.                                     00003000
             03 MMTRDACP     PIC X.                                     00003010
             03 MMTRDACH     PIC X.                                     00003020
             03 MMTRDACV     PIC X.                                     00003030
             03 MMTRDACO     PIC ZZZ9.                                  00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MMNBENTA     PIC X.                                     00003060
             03 MMNBENTC     PIC X.                                     00003070
             03 MMNBENTP     PIC X.                                     00003080
             03 MMNBENTH     PIC X.                                     00003090
             03 MMNBENTV     PIC X.                                     00003100
             03 MMNBENTO     PIC ZZZ9.                                  00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MMNBVDA      PIC X.                                     00003130
             03 MMNBVDC PIC X.                                          00003140
             03 MMNBVDP PIC X.                                          00003150
             03 MMNBVDH PIC X.                                          00003160
             03 MMNBVDV PIC X.                                          00003170
             03 MMNBVDO      PIC Z9,9.                                  00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MCMAGA    PIC X.                                          00003200
           02 MCMAGC    PIC X.                                          00003210
           02 MCMAGP    PIC X.                                          00003220
           02 MCMAGH    PIC X.                                          00003230
           02 MCMAGV    PIC X.                                          00003240
           02 MCMAGO    PIC X(3).                                       00003250
           02 FILLER    PIC X(2).                                       00003260
           02 MLMAGA    PIC X.                                          00003270
           02 MLMAGC    PIC X.                                          00003280
           02 MLMAGP    PIC X.                                          00003290
           02 MLMAGH    PIC X.                                          00003300
           02 MLMAGV    PIC X.                                          00003310
           02 MLMAGO    PIC X(9).                                       00003320
           02 FILLER    PIC X(2).                                       00003330
           02 MCATLMA   PIC X.                                          00003340
           02 MCATLMC   PIC X.                                          00003350
           02 MCATLMP   PIC X.                                          00003360
           02 MCATLMH   PIC X.                                          00003370
           02 MCATLMV   PIC X.                                          00003380
           02 MCATLMO   PIC ZZZZZZ9.                                    00003390
           02 FILLER    PIC X(2).                                       00003400
           02 MCAELAA   PIC X.                                          00003410
           02 MCAELAC   PIC X.                                          00003420
           02 MCAELAP   PIC X.                                          00003430
           02 MCAELAH   PIC X.                                          00003440
           02 MCAELAV   PIC X.                                          00003450
           02 MCAELAO   PIC ZZZZZZ9.                                    00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MCADACA   PIC X.                                          00003480
           02 MCADACC   PIC X.                                          00003490
           02 MCADACP   PIC X.                                          00003500
           02 MCADACH   PIC X.                                          00003510
           02 MCADACV   PIC X.                                          00003520
           02 MCADACO   PIC ZZZZZ9.                                     00003530
           02 FILLER    PIC X(2).                                       00003540
           02 MCATOTA   PIC X.                                          00003550
           02 MCATOTC   PIC X.                                          00003560
           02 MCATOTP   PIC X.                                          00003570
           02 MCATOTH   PIC X.                                          00003580
           02 MCATOTV   PIC X.                                          00003590
           02 MCATOTO   PIC ZZZZZZ9.                                    00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MNBTLMA   PIC X.                                          00003620
           02 MNBTLMC   PIC X.                                          00003630
           02 MNBTLMP   PIC X.                                          00003640
           02 MNBTLMH   PIC X.                                          00003650
           02 MNBTLMV   PIC X.                                          00003660
           02 MNBTLMO   PIC ZZZ9.                                       00003670
           02 FILLER    PIC X(2).                                       00003680
           02 MNBELAA   PIC X.                                          00003690
           02 MNBELAC   PIC X.                                          00003700
           02 MNBELAP   PIC X.                                          00003710
           02 MNBELAH   PIC X.                                          00003720
           02 MNBELAV   PIC X.                                          00003730
           02 MNBELAO   PIC ZZZ9.                                       00003740
           02 FILLER    PIC X(2).                                       00003750
           02 MNBDACA   PIC X.                                          00003760
           02 MNBDACC   PIC X.                                          00003770
           02 MNBDACP   PIC X.                                          00003780
           02 MNBDACH   PIC X.                                          00003790
           02 MNBDACV   PIC X.                                          00003800
           02 MNBDACO   PIC ZZZ9.                                       00003810
           02 FILLER    PIC X(2).                                       00003820
           02 MTRDARA   PIC X.                                          00003830
           02 MTRDARC   PIC X.                                          00003840
           02 MTRDARP   PIC X.                                          00003850
           02 MTRDARH   PIC X.                                          00003860
           02 MTRDARV   PIC X.                                          00003870
           02 MTRDARO   PIC ZZZ9.                                       00003880
           02 FILLER    PIC X(2).                                       00003890
           02 MTRDACA   PIC X.                                          00003900
           02 MTRDACC   PIC X.                                          00003910
           02 MTRDACP   PIC X.                                          00003920
           02 MTRDACH   PIC X.                                          00003930
           02 MTRDACV   PIC X.                                          00003940
           02 MTRDACO   PIC ZZZ9.                                       00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MNBENTA   PIC X.                                          00003970
           02 MNBENTC   PIC X.                                          00003980
           02 MNBENTP   PIC X.                                          00003990
           02 MNBENTH   PIC X.                                          00004000
           02 MNBENTV   PIC X.                                          00004010
           02 MNBENTO   PIC ZZZ9.                                       00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MNBVDA    PIC X.                                          00004040
           02 MNBVDC    PIC X.                                          00004050
           02 MNBVDP    PIC X.                                          00004060
           02 MNBVDH    PIC X.                                          00004070
           02 MNBVDV    PIC X.                                          00004080
           02 MNBVDO    PIC Z9,9.                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MZONCMDA  PIC X.                                          00004110
           02 MZONCMDC  PIC X.                                          00004120
           02 MZONCMDP  PIC X.                                          00004130
           02 MZONCMDH  PIC X.                                          00004140
           02 MZONCMDV  PIC X.                                          00004150
           02 MZONCMDO  PIC X(15).                                      00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MLIBERRA  PIC X.                                          00004180
           02 MLIBERRC  PIC X.                                          00004190
           02 MLIBERRP  PIC X.                                          00004200
           02 MLIBERRH  PIC X.                                          00004210
           02 MLIBERRV  PIC X.                                          00004220
           02 MLIBERRO  PIC X(54).                                      00004230
      * CODE TRANSACTION                                                00004240
           02 FILLER    PIC X(2).                                       00004250
           02 MCODTRAA  PIC X.                                          00004260
           02 MCODTRAC  PIC X.                                          00004270
           02 MCODTRAP  PIC X.                                          00004280
           02 MCODTRAH  PIC X.                                          00004290
           02 MCODTRAV  PIC X.                                          00004300
           02 MCODTRAO  PIC X(4).                                       00004310
      * NOM DU CICS                                                     00004320
           02 FILLER    PIC X(2).                                       00004330
           02 MCICSA    PIC X.                                          00004340
           02 MCICSC    PIC X.                                          00004350
           02 MCICSP    PIC X.                                          00004360
           02 MCICSH    PIC X.                                          00004370
           02 MCICSV    PIC X.                                          00004380
           02 MCICSO    PIC X(5).                                       00004390
      * NETNAME                                                         00004400
           02 FILLER    PIC X(2).                                       00004410
           02 MNETNAMA  PIC X.                                          00004420
           02 MNETNAMC  PIC X.                                          00004430
           02 MNETNAMP  PIC X.                                          00004440
           02 MNETNAMH  PIC X.                                          00004450
           02 MNETNAMV  PIC X.                                          00004460
           02 MNETNAMO  PIC X(8).                                       00004470
      * CODE TERMINAL                                                   00004480
           02 FILLER    PIC X(2).                                       00004490
           02 MSCREENA  PIC X.                                          00004500
           02 MSCREENC  PIC X.                                          00004510
           02 MSCREENP  PIC X.                                          00004520
           02 MSCREENH  PIC X.                                          00004530
           02 MSCREENV  PIC X.                                          00004540
           02 MSCREENO  PIC X(5).                                       00004550
                                                                                
