      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * DESCRIPTION FICHIER FSM140                                              
      *                            LONGUEUR ENREG. = 314                        
      *                                                                         
       01 ENR-FSM140.                                                           
          05  FSM140-NSOC                        PIC X(03).                     
          05  FSM140-CGRP                        PIC X(02).                     
          05  FSM140-TAB-LIEU     OCCURS 20.                                    
              10  FSM140-NLIEU                   PIC X(03).                     
              10  FSM140-NSEQ                    PIC X(02).                     
          05  FSM140-TAB-VTE-STK  OCCURS 20.                                    
              10  FSM140-QVTE                    PIC S9(5) COMP-3.              
              10  FSM140-QSTK                    PIC S9(5) COMP-3.              
          05  FSM140-TOTVTE                      PIC S9(7) COMP-3.              
          05  FSM140-TOTSTK                      PIC S9(7) COMP-3.              
          05  FSM140-NCODIC                      PIC X(07).                     
          05  FSM140-CFAM                        PIC X(05).                     
          05  FSM140-CMARQ                       PIC X(05).                     
          05  FSM140-LREFFOURN                   PIC X(20).                     
          05  FSM140-WSENSVTE                    PIC X(01).                     
          05  FSM140-LSTATCOMP                   PIC X(03).                     
          05  FSM140-LREFDARTY                   PIC X(20).                     
          05  FSM140-PRIX                        PIC S9(7)V9(2) COMP-3.         
          05  FSM140-PCOMM                       PIC S9(5)V9(2) COMP-3.         
          05  FSM140-DCDE                        PIC X(08).                     
          05  FSM140-QCDE                        PIC S9(5) COMP-3.              
                                                                                
