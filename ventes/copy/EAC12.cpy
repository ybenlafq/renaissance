      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Résultats par Magasin                                           00000020
      ***************************************************************** 00000030
       01   EAC12I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(56).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MDDEBUTI  PIC X(10).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MJDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJDEBUTF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MJDEBUTI  PIC X(10).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDFINI    PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJFINL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MJFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJFINF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MJFINI    PIC X(10).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBHL    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLIBHL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIBHF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLIBHI    PIC X(28).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTHL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MDDEBUTHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDDEBUTHF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDDEBUTHI      PIC X(10).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTHL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MJDEBUTHL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MJDEBUTHF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MJDEBUTHI      PIC X(10).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBH2L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBH2L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBH2F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBH2I   PIC X(4).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINHL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDFINHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFINHF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDFINHI   PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJFINHL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MJFINHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MJFINHF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MJFINHI   PIC X(10).                                      00000570
           02 MLIGNEI OCCURS   14 TIMES .                               00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENTTRL     COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MQENTTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQENTTRF     PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MQENTTRI     PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATRL  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCATRL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCATRF  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCATRI  PIC X(10).                                      00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCRTTRL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MCCRTTRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCCRTTRF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCCRTTRI     PIC X(4).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENTCUML    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MQENTCUML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQENTCUMF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MQENTCUMI    PIC X(6).                                  00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCACUML      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCACUML COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCACUMF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCACUMI      PIC X(10).                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCCRTCUML    COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MCCRTCUML COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCCRTCUMF    PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MCCRTCUMI    PIC X(4).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQENTPDTL    COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQENTPDTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQENTPDTF    PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQENTPDTI    PIC X(4).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPDTL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCAPDTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAPDTF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCAPDTI      PIC X(4).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MF11L     COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MF11L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MF11F     PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MF11I     PIC X(14).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MF12L     COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MF12L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MF12F     PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MF12I     PIC X(14).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLIBERRI  PIC X(79).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCODTRAI  PIC X(4).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MZONCMDI  PIC X(15).                                      00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCICSI    PIC X(5).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MNETNAMI  PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MSCREENI  PIC X(4).                                       00001220
      ***************************************************************** 00001230
      * Résultats par Magasin                                           00001240
      ***************************************************************** 00001250
       01   EAC12O REDEFINES EAC12I.                                    00001260
           02 FILLER    PIC X(12).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MDATJOUA  PIC X.                                          00001290
           02 MDATJOUC  PIC X.                                          00001300
           02 MDATJOUP  PIC X.                                          00001310
           02 MDATJOUH  PIC X.                                          00001320
           02 MDATJOUV  PIC X.                                          00001330
           02 MDATJOUO  PIC X(10).                                      00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MTIMJOUA  PIC X.                                          00001360
           02 MTIMJOUC  PIC X.                                          00001370
           02 MTIMJOUP  PIC X.                                          00001380
           02 MTIMJOUH  PIC X.                                          00001390
           02 MTIMJOUV  PIC X.                                          00001400
           02 MTIMJOUO  PIC X(5).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MTITREA   PIC X.                                          00001430
           02 MTITREC   PIC X.                                          00001440
           02 MTITREP   PIC X.                                          00001450
           02 MTITREH   PIC X.                                          00001460
           02 MTITREV   PIC X.                                          00001470
           02 MTITREO   PIC X(56).                                      00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MDDEBUTA  PIC X.                                          00001500
           02 MDDEBUTC  PIC X.                                          00001510
           02 MDDEBUTP  PIC X.                                          00001520
           02 MDDEBUTH  PIC X.                                          00001530
           02 MDDEBUTV  PIC X.                                          00001540
           02 MDDEBUTO  PIC X(10).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MJDEBUTA  PIC X.                                          00001570
           02 MJDEBUTC  PIC X.                                          00001580
           02 MJDEBUTP  PIC X.                                          00001590
           02 MJDEBUTH  PIC X.                                          00001600
           02 MJDEBUTV  PIC X.                                          00001610
           02 MJDEBUTO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MDFINA    PIC X.                                          00001640
           02 MDFINC    PIC X.                                          00001650
           02 MDFINP    PIC X.                                          00001660
           02 MDFINH    PIC X.                                          00001670
           02 MDFINV    PIC X.                                          00001680
           02 MDFINO    PIC X(10).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MJFINA    PIC X.                                          00001710
           02 MJFINC    PIC X.                                          00001720
           02 MJFINP    PIC X.                                          00001730
           02 MJFINH    PIC X.                                          00001740
           02 MJFINV    PIC X.                                          00001750
           02 MJFINO    PIC X(10).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MLIBHA    PIC X.                                          00001780
           02 MLIBHC    PIC X.                                          00001790
           02 MLIBHP    PIC X.                                          00001800
           02 MLIBHH    PIC X.                                          00001810
           02 MLIBHV    PIC X.                                          00001820
           02 MLIBHO    PIC X(28).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MDDEBUTHA      PIC X.                                     00001850
           02 MDDEBUTHC PIC X.                                          00001860
           02 MDDEBUTHP PIC X.                                          00001870
           02 MDDEBUTHH PIC X.                                          00001880
           02 MDDEBUTHV PIC X.                                          00001890
           02 MDDEBUTHO      PIC X(10).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MJDEBUTHA      PIC X.                                     00001920
           02 MJDEBUTHC PIC X.                                          00001930
           02 MJDEBUTHP PIC X.                                          00001940
           02 MJDEBUTHH PIC X.                                          00001950
           02 MJDEBUTHV PIC X.                                          00001960
           02 MJDEBUTHO      PIC X(10).                                 00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MLIBH2A   PIC X.                                          00001990
           02 MLIBH2C   PIC X.                                          00002000
           02 MLIBH2P   PIC X.                                          00002010
           02 MLIBH2H   PIC X.                                          00002020
           02 MLIBH2V   PIC X.                                          00002030
           02 MLIBH2O   PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MDFINHA   PIC X.                                          00002060
           02 MDFINHC   PIC X.                                          00002070
           02 MDFINHP   PIC X.                                          00002080
           02 MDFINHH   PIC X.                                          00002090
           02 MDFINHV   PIC X.                                          00002100
           02 MDFINHO   PIC X(10).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MJFINHA   PIC X.                                          00002130
           02 MJFINHC   PIC X.                                          00002140
           02 MJFINHP   PIC X.                                          00002150
           02 MJFINHH   PIC X.                                          00002160
           02 MJFINHV   PIC X.                                          00002170
           02 MJFINHO   PIC X(10).                                      00002180
           02 MLIGNEO OCCURS   14 TIMES .                               00002190
             03 FILLER       PIC X(2).                                  00002200
             03 MQENTTRA     PIC X.                                     00002210
             03 MQENTTRC     PIC X.                                     00002220
             03 MQENTTRP     PIC X.                                     00002230
             03 MQENTTRH     PIC X.                                     00002240
             03 MQENTTRV     PIC X.                                     00002250
             03 MQENTTRO     PIC -----9.                                00002260
             03 FILLER       PIC X(2).                                  00002270
             03 MCATRA  PIC X.                                          00002280
             03 MCATRC  PIC X.                                          00002290
             03 MCATRP  PIC X.                                          00002300
             03 MCATRH  PIC X.                                          00002310
             03 MCATRV  PIC X.                                          00002320
             03 MCATRO  PIC ---------9.                                 00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MCCRTTRA     PIC X.                                     00002350
             03 MCCRTTRC     PIC X.                                     00002360
             03 MCCRTTRP     PIC X.                                     00002370
             03 MCCRTTRH     PIC X.                                     00002380
             03 MCCRTTRV     PIC X.                                     00002390
             03 MCCRTTRO     PIC X(4).                                  00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MQENTCUMA    PIC X.                                     00002420
             03 MQENTCUMC    PIC X.                                     00002430
             03 MQENTCUMP    PIC X.                                     00002440
             03 MQENTCUMH    PIC X.                                     00002450
             03 MQENTCUMV    PIC X.                                     00002460
             03 MQENTCUMO    PIC -----9.                                00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MCACUMA      PIC X.                                     00002490
             03 MCACUMC PIC X.                                          00002500
             03 MCACUMP PIC X.                                          00002510
             03 MCACUMH PIC X.                                          00002520
             03 MCACUMV PIC X.                                          00002530
             03 MCACUMO      PIC ---------9.                            00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MCCRTCUMA    PIC X.                                     00002560
             03 MCCRTCUMC    PIC X.                                     00002570
             03 MCCRTCUMP    PIC X.                                     00002580
             03 MCCRTCUMH    PIC X.                                     00002590
             03 MCCRTCUMV    PIC X.                                     00002600
             03 MCCRTCUMO    PIC X(4).                                  00002610
             03 FILLER       PIC X(2).                                  00002620
             03 MQENTPDTA    PIC X.                                     00002630
             03 MQENTPDTC    PIC X.                                     00002640
             03 MQENTPDTP    PIC X.                                     00002650
             03 MQENTPDTH    PIC X.                                     00002660
             03 MQENTPDTV    PIC X.                                     00002670
             03 MQENTPDTO    PIC X(4).                                  00002680
             03 FILLER       PIC X(2).                                  00002690
             03 MCAPDTA      PIC X.                                     00002700
             03 MCAPDTC PIC X.                                          00002710
             03 MCAPDTP PIC X.                                          00002720
             03 MCAPDTH PIC X.                                          00002730
             03 MCAPDTV PIC X.                                          00002740
             03 MCAPDTO      PIC X(4).                                  00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MF11A     PIC X.                                          00002770
           02 MF11C     PIC X.                                          00002780
           02 MF11P     PIC X.                                          00002790
           02 MF11H     PIC X.                                          00002800
           02 MF11V     PIC X.                                          00002810
           02 MF11O     PIC X(14).                                      00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MF12A     PIC X.                                          00002840
           02 MF12C     PIC X.                                          00002850
           02 MF12P     PIC X.                                          00002860
           02 MF12H     PIC X.                                          00002870
           02 MF12V     PIC X.                                          00002880
           02 MF12O     PIC X(14).                                      00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MLIBERRA  PIC X.                                          00002910
           02 MLIBERRC  PIC X.                                          00002920
           02 MLIBERRP  PIC X.                                          00002930
           02 MLIBERRH  PIC X.                                          00002940
           02 MLIBERRV  PIC X.                                          00002950
           02 MLIBERRO  PIC X(79).                                      00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MCODTRAA  PIC X.                                          00002980
           02 MCODTRAC  PIC X.                                          00002990
           02 MCODTRAP  PIC X.                                          00003000
           02 MCODTRAH  PIC X.                                          00003010
           02 MCODTRAV  PIC X.                                          00003020
           02 MCODTRAO  PIC X(4).                                       00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MZONCMDA  PIC X.                                          00003050
           02 MZONCMDC  PIC X.                                          00003060
           02 MZONCMDP  PIC X.                                          00003070
           02 MZONCMDH  PIC X.                                          00003080
           02 MZONCMDV  PIC X.                                          00003090
           02 MZONCMDO  PIC X(15).                                      00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(4).                                       00003310
                                                                                
