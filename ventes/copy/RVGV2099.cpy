      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGV2099                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV2099                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV2099.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV2099.                                                            
      *}                                                                        
           02  GV20-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV20-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV20-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV20-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV20-NCLIENT                                                     
               PIC X(0009).                                                     
           02  GV20-DVENTE                                                      
               PIC X(0008).                                                     
           02  GV20-DHVENTE                                                     
               PIC X(0002).                                                     
           02  GV20-DMVENTE                                                     
               PIC X(0002).                                                     
           02  GV20-PTTVENTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PVERSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PCOMPT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PLIVR                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PDIFFERE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PRFACT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-CREMVTE                                                     
               PIC X(0005).                                                     
           02  GV20-PREMVTE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-LCOMVTE1                                                    
               PIC X(0030).                                                     
           02  GV20-LCOMVTE2                                                    
               PIC X(0030).                                                     
           02  GV20-LCOMVTE3                                                    
               PIC X(0030).                                                     
           02  GV20-LCOMVTE4                                                    
               PIC X(0030).                                                     
           02  GV20-DMODIFVTE                                                   
               PIC X(0008).                                                     
           02  GV20-WFACTURE                                                    
               PIC X(0001).                                                     
           02  GV20-WEXPORT                                                     
               PIC X(0001).                                                     
           02  GV20-WDETAXEC                                                    
               PIC X(0001).                                                     
           02  GV20-WDETAXEHC                                                   
               PIC X(0001).                                                     
           02  GV20-CORGORED                                                    
               PIC X(0005).                                                     
           02  GV20-CMODPAIMTI                                                  
               PIC X(0005).                                                     
           02  GV20-LDESCRIPTIF1                                                
               PIC X(0030).                                                     
           02  GV20-LDESCRIPTIF2                                                
               PIC X(0030).                                                     
           02  GV20-DLIVRBL                                                     
               PIC X(0008).                                                     
           02  GV20-NFOLIOBL                                                    
               PIC X(0003).                                                     
           02  GV20-LAUTORM                                                     
               PIC X(0005).                                                     
           02  GV20-NAUTORD                                                     
               PIC X(0005).                                                     
           02  GV20-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV20-DSTAT                                                       
               PIC X(0004).                                                     
           02  GV20-PTTVENTE1                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PVERSE1                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PCOMPT1                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PLIVR1                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PDIFFERE1                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-PRFACT1                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV20-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GV20-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV20-NETNAME                                                     
               PIC X(0008).                                                     
           02  GV20-CACID                                                       
               PIC X(0008).                                                     
           02  GV20-DFACTURE                                                    
               PIC X(08).                                                       
           02  GV20-NSOCMODIF                                                   
               PIC X(03).                                                       
           02  GV20-NLIEUMODIF                                                  
               PIC X(03).                                                       
           02  GV20-NSOCP                                                       
               PIC X(03).                                                       
           02  GV20-NLIEUP                                                      
               PIC X(03).                                                       
           02  GV20-CDEV                                                        
               PIC X(03).                                                       
           02  GV20-CFCRED                                                      
               PIC X(05).                                                       
           02  GV20-NCREDI                                                      
               PIC X(14).                                                       
           02  GV20-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV2099                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV2099-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV2099-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NCLIENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NCLIENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PTTVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PVERSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PVERSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PDIFFERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PRFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-PREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-PREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LCOMVTE1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LCOMVTE1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LCOMVTE2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LCOMVTE2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LCOMVTE3-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LCOMVTE3-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LCOMVTE4-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LCOMVTE4-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DMODIFVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DMODIFVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-WFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-WFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-WEXPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-WEXPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-WDETAXEC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-WDETAXEC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-WDETAXEHC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-WDETAXEHC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CORGORED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CMODPAIMTI-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CMODPAIMTI-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LDESCRIPTIF1-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LDESCRIPTIF1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LDESCRIPTIF2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LDESCRIPTIF2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DLIVRBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DLIVRBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NFOLIOBL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NFOLIOBL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-LAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-LAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NETNAME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NETNAME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CACID-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CACID-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-DFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-DFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NSOCMODIF-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NSOCMODIF-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NLIEUMODIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NLIEUMODIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NSOCP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NLIEUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CDEV-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-CFCRED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NCREDI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV20-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV20-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
