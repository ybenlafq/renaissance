      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * 1 LIGNE DE DONNEES CORRESPOND A UNE LIGNE D'ECRAN             *         
      * LONGUEUR TOTALE = 441                                         *         
      *****************************************************************         
      * 26/01/10 - PROJET INNOVENTE V3.                               *         
      * MARQUE AL2601 AJOUT PRIME CLIENT POUR GESTION VENTES B&S.     *         
      * LONGUEUR :                                                    *         
      * ------------------------------------------------------------- *         
      * 27/06/2013 : M21401 CVENDEUR ARCHIVAGE POUR ECHANGE           *         
      * ------------------------------------------------------------- *         
      * 11/12/2013 : STOCK AVANCE SUR PLATEFORME MARQUE DE1213        *         
      *****************************************************************         
      *                                                                         
       01  TS01-DONNEES.                                                        
      *--- DONNEES LIGNE DE LA TABLE (LIGNE INITIALE)                           
           05 TS01-LIGNE-INITIALE.                                              
              10 TS01-TYPE-LIGNE-INITIAL         PIC X(02).                     
              10 TS01-LIGNE-VENTE-INITIAL.                                      
                 15 TS01-CMARQ-INITIAL           PIC X(05).                     
                 15 TS01-LREFFOURN-INITIAL       PIC X(20).                     
                 15 TS01-NCODIC-INITIAL          PIC X(07).                     
                 15 TS01-MQTE-INITIAL            PIC 9(03).                     
                 15 TS01-MTYPE-INITIAL           PIC X.                         
                 15 TS01-MPVUNIT-INITIAL         PIC S9(7)V99.                  
                 15 TS01-MPVUNIT-A-INITIAL       REDEFINES                      
                    TS01-MPVUNIT-INITIAL         PIC X(9).                      
                 15 TS01-MPVUNITF-INITIAL        PIC S9(7)V99.                  
                 15 TS01-MPVTOT-INITIAL          PIC S9(7)V99.                  
AL2601           15 TS01-MPRIMECLI-INITIAL       PIC S9(7)V99.                  
                 15 TS01-CMODDEL-INITIAL         PIC X(03).                     
                 15 TS01-MRESERV-INITIAL         PIC X.                         
                 15 TS01-MDATE-INITIAL           PIC X(06).                     
                 15 TS01-DDELIV-INITIAL          PIC X(08).                     
                 15 TS01-MPL-INITIAL             PIC X(02).                     
                 15 TS01-MNVEND-INITIAL          PIC 9.                         
                 15 TS01-QCONDT-INITIAL          PIC S9(5) COMP-3.              
                 15 TS01-DCREATION-INITIAL       PIC X(08).                     
              10 TS01-COMMENTAIRES-INITIAL       PIC X(35).                     
              10 TS01-NSEQNQ-INITIAL             PIC S9(5) COMP-3.              
AL2601        10 TS01-NSEQENS-INITIAL         PIC S9(5) COMP-3.                 
              10 TS01-FILLER-INITIAL             PIC X(02).                     
              10 TS01-CARACTERISTIQUES-INITAL    PIC X(50).                     
      *-      DONNEES RESERVATION STOCK                                         
              10 TS01-RESERVATION-STOCK-INITIAL.                                
                 15 TS01-WCQERESF-INITIAL     PIC X.                            
                 15 TS01-NSOCLIVR-INITIAL     PIC X(03).                        
                 15 TS01-NDEPOT-INITIAL       PIC X(03).                        
                 15 TS01-NMUTATION-INITIAL    PIC X(07).                        
              10 TS01-NSEQREF-INITIAL         PIC S9(5) COMP-3.                 
              10 TS01-NMODIF-INITIAL          PIC S9(5) COMP-3.                 
AL2601        10 TS01-SUPPLEMENT-INITIAL.                                       
     |           15 TS01-NSEQREF-INITIAL      PIC S9(5) COMP-3.                 
     |           15 TS01-NMODIF-INITIAL       PIC S9(5) COMP-3.                 
     |           15 TS01-CTYPENREG-INITIAL    PIC X(0001).                      
     |           15 TS01-NCODICGRP-INITIAL    PIC X(0007).                      
     |           15 TS01-CENREG-INITIAL       PIC X(0005).                      
     |           15 TS01-CEQUIPE-INITIAL      PIC X(0005).                      
     |           15 TS01-CPROTOUR-INITIAL     PIC X(0005).                      
     |           15 TS01-CADRTOUR-INITIAL     PIC X(0001).                      
     |           15 TS01-CPOSTAL-INITIAL      PIC X(0005).                      
     |           15 TS01-NAUTORM-INITIAL      PIC X(0005).                      
     |           15 TS01-WARTINEX-INITIAL     PIC X(0001).                      
     |           15 TS01-WEDITBL-INITIAL      PIC X(0001).                      
     |           15 TS01-CTOURNEE-INITIAL     PIC X(0008).                      
     |           15 TS01-DCOMPTA-INITIAL      PIC X(0008).                      
     |           15 TS01-HCREATION-INITIAL    PIC X(0004).                      
     |           15 TS01-DSTAT-INITIAL        PIC X(0004).                      
     |           15 TS01-NSOCMODIF-INITIAL    PIC X(0003).                      
     |           15 TS01-NLIEUMODIF-INITIAL   PIC X(0003).                      
     |           15 TS01-DATENC-INITIAL       PIC X(0008).                      
     |           15 TS01-CDEV-INITIAL         PIC X(0003).                      
     |           15 TS01-WUNITR-INITIAL       PIC X(0020).                      
     |           15 TS01-LRCMMT-INITIAL       PIC X(0010).                      
     |           15 TS01-NTRANS-INITIAL       PIC S9(8) COMP-3.                 
     |           15 TS01-NSOCORIG-INITIAL     PIC X(0003).                      
     |           15 TS01-NLIEUORIG-INITIAL    PIC X(0003).                      
     |           15 TS01-DTOPE-INITIAL        PIC X(0008).                      
     |           15 TS01-NSOCGEST-INITIAL     PIC X(0003).                      
     |           15 TS01-NLIEUGEST-INITIAL    PIC X(0003).                      
     |           15 TS01-NSOCDEPLIV-INITIAL   PIC X(0003).                      
     |           15 TS01-NLIEUDEPLIV-INITIAL  PIC X(0003).                      
     |           15 TS01-CTYPENT-INITIAL      PIC X(0002).                      
     |           15 TS01-NLIEN-INITIAL        PIC S9(5) COMP-3.                 
     |           15 TS01-NACTVTE-INITIAL      PIC S9(5) COMP-3.                 
     |           15 TS01-PVCODIG-INITIAL      PIC S9(7)V9(0002) COMP-3.         
     |           15 TS01-QTCODIG-INITIAL      PIC S9(5) COMP-3.                 
     |           15 TS01-DPRLG-INITIAL        PIC X(08).                        
     |           15 TS01-CALERTE-INITIAL      PIC X(05).                        
AL2601           15 TS01-CREPRISE-INITIAL     PIC X(05).                        
      *                                                                         
      *--- DONNEES LIGNE DE L' ECRAN (LIGNE MODIFIEE)                           
           05 TS01-LIGNE-MODIFIEE.                                              
              10 TS01-TYPE-LIGNE             PIC X(02).                         
                 88 TS01-LIGNE-VIERGE            VALUE SPACE.                   
                 88 TS01-LIGNE-PRESTATION        VALUE 'PR'.                    
                 88 TS01-LIGNE-VENTE             VALUE 'AV'.                    
                 88 TS01-LIGNE-REMISE            VALUE 'AR'.                    
                 88 TS01-LIGNE-GARANTIE          VALUE 'AG'.                    
                 88 TS01-LIGNE-PRESTA-RATTACHEE  VALUE 'AP'.                    
                 88 TS01-LIGNE-REPRISE           VALUE 'RE'.                    
                 88 TS01-LIGNE-RACHAT-PSE        VALUE 'RP'.                    
      *{ remove-comma-in-dde 1.5                                                
      *          88 TS01-LIGNE-DEPENDANTE                                       
      *             VALUE  'AR' , 'AG' , 'AP' , 'RE' , 'RP'.                    
      *--                                                                       
                 88 TS01-LIGNE-DEPENDANTE                                       
                    VALUE  'AR'   'AG'   'AP'   'RE'   'RP'.                    
      *}                                                                        
              10 TS01-LIGNE-DE-VENTE.                                           
                 15 TS01-CMARQ                PIC X(05).                        
                 15 TS01-LREFFOURN            PIC X(20).                        
                 15 TS01-NCODIC               PIC X(07).                        
                 15 TS01-MQTE                 PIC 9(03).                        
                 15 TS01-MTYPE                PIC X.                            
                 15 TS01-MPVUNIT              PIC S9(7)V99.                     
                 15 TS01-MPVUNIT-X            REDEFINES TS01-MPVUNIT            
                                              PIC X(9).                         
                 15 TS01-MPVUNITF             PIC S9(7)V99.                     
                 15 TS01-MPVTOT               PIC S9(7)V99.                     
AL2601           15 TS01-MPRIMECLI            PIC S9(7)V99.                     
                 15 TS01-CMODDEL.                                               
                    20 TS01-MODE-DELIVRANCE   PIC X.                            
                       88 TS01-ARTICLE-EMPORTE    VALUE 'E'.                    
                       88 TS01-ARTICLE-LIVRE      VALUE 'L'.                    
                       88 TS01-ARTICLE-REPRIS     VALUE 'R'.                    
                    20 TS01-LIEU-DELIVRANCE   PIC X.                            
                       88 TS01-DELIVRANCE-MAGASIN  VALUE 'M'.                   
                       88 TS01-DELIVRANCE-PLTF     VALUE 'P'.                   
                       88 TS01-DELIVRANCE-ENTREPOT VALUE 'D'.                   
                       88 TS01-DELIVRANCE-RX       VALUE 'X'.                   
                    20 TS01-LIEU-DE-STOCKAGE  PIC X.                            
                       88 TS01-PAS-EQUIPE          VALUE  ' '.                  
                       88 TS01-TYPE-DE-LIVRAISON   VALUE '1' THRU '9'.          
                       88 TS01-STOCKAGE-MAGASIN    VALUE 'M'.                   
                       88 TS01-STOCKAGE-ENTREPOT   VALUE 'D'.                   
                       88 TS01-MUTATION            VALUE 'M'  'D'.              
                       88 TS01-STOCK-MAGASIN       VALUE 'M'  'E'  'R'.         
                       88 TS01-STOCK-ENTREPOT      VALUE 'D'  '1'  '9'.         
                 15 TS01-MRESERV              PIC X.                            
                    88 TS01-LIGNE-LIVREE          VALUE 'T'.                    
                    88 TS01-EN-LIVRAISON          VALUE 'L'.                    
                    88 TS01-DIFFERE               VALUE 'D'.                    
                 15 TS01-MDATE                PIC X(06).                        
                 15 TS01-DDELIV               PIC X(08).                        
                 15 TS01-MPL                  PIC X(02).                        
                 15 TS01-MNVEND               PIC 9.                            
                    88 TS01-CODE-VENDEUR-VALIDE   VALUE 1 THRU 4.               
                 15 TS01-MNVEND-ALPHA         REDEFINES TS01-MNVEND             
                                              PIC X.                            
                 15 TS01-QCONDT               PIC S9(5) COMP-3.                 
                 15 TS01-DCREATION            PIC X(08).                        
              10 TS01-COMMENTAIRES.                                             
                 15 TS01-COM-CODE-FAMILLE     PIC X(05).                        
                 15 TS01-FILLER1              PIC X.                            
                 15 TS01-COM-CODE-MARQUE      PIC X(05).                        
                 15 TS01-FILLER2              PIC X.                            
                 15 TS01-COM-REFERENCE        PIC X(20).                        
                 15 TS01-FILLER3              PIC X(03).                        
              10 TS01-NSEQNQ                  PIC S9(5) COMP-3.                 
AL2601        10 TS01-NSEQENS                 PIC S9(5) COMP-3.                 
              10 TS01-FILLER                  PIC X(02).                        
              10 TS01-CARACTERISTIQUES        OCCURS 5.                         
                 15 TS01-POSTE-CARACTERISTIQUE   PIC X(05).                     
                 15 TS01-CODE-CARACTERISTIQUE    PIC X(05).                     
      *-      DONNEES RESERVATION STOCK                                         
              10 TS01-RESERVATION-STOCK.                                        
                 15 TS01-WCQERESF             PIC X.                            
                    88 TS01-RESER-DISPONIBLE      VALUE ' '.                    
                    88 TS01-RESER-FOURNISSEUR     VALUE 'F'.                    
                    88 TS01-RESER-CONTREMARQUE    VALUE 'Q'.                    
                    88 TS01-RESER-NON-AFFECTEE    VALUE 'Z'.                    
                 15 TS01-NSOCLIVR             PIC X(03).                        
                 15 TS01-NDEPOT               PIC X(03).                        
                 15 TS01-NMUTATION            PIC X(07).                        
AL2601        10 TS01-SUPPLEMENT.                                               
     |           15 TS01-NSEQREF              PIC S9(5) COMP-3.                 
     |           15 TS01-NMODIF               PIC S9(5) COMP-3.                 
     |           15 TS01-CTYPENREG            PIC X(0001).                      
     |           15 TS01-NCODICGRP            PIC X(0007).                      
     |           15 TS01-CENREG               PIC X(0005).                      
     |           15 TS01-CEQUIPE              PIC X(0005).                      
     |           15 TS01-CPROTOUR             PIC X(0005).                      
     |           15 TS01-CADRTOUR             PIC X(0001).                      
     |           15 TS01-CPOSTAL              PIC X(0005).                      
     |           15 TS01-NAUTORM              PIC X(0005).                      
     |           15 TS01-WARTINEX             PIC X(0001).                      
     |           15 TS01-WEDITBL              PIC X(0001).                      
     |           15 TS01-CTOURNEE             PIC X(0008).                      
     |           15 TS01-DCOMPTA              PIC X(0008).                      
     |           15 TS01-HCREATION            PIC X(0004).                      
     |           15 TS01-DSTAT                PIC X(0004).                      
     |           15 TS01-NSOCMODIF            PIC X(0003).                      
     |           15 TS01-NLIEUMODIF           PIC X(0003).                      
     |           15 TS01-DATENC               PIC X(0008).                      
     |           15 TS01-CDEV                 PIC X(0003).                      
     |           15 TS01-WUNITR               PIC X(0020).                      
     |           15 TS01-LRCMMT               PIC X(0010).                      
     |           15 TS01-NTRANS               PIC S9(8) COMP-3.                 
     |           15 TS01-NSOCORIG             PIC X(0003).                      
     |           15 TS01-NLIEUORIG            PIC X(0003).                      
     |           15 TS01-DTOPE                PIC X(0008).                      
     |           15 TS01-NSOCGEST             PIC X(0003).                      
     |           15 TS01-NLIEUGEST            PIC X(0003).                      
     |           15 TS01-NSOCDEPLIV           PIC X(0003).                      
     |           15 TS01-NLIEUDEPLIV          PIC X(0003).                      
     |           15 TS01-CTYPENT              PIC X(0002).                      
     |           15 TS01-NLIEN                PIC S9(5) COMP-3.                 
     |           15 TS01-NACTVTE              PIC S9(5) COMP-3.                 
     |           15 TS01-PVCODIG              PIC S9(7)V9(0002) COMP-3.         
     |           15 TS01-QTCODIG              PIC S9(5) COMP-3.                 
     |           15 TS01-DPRLG                PIC X(08).                        
     |           15 TS01-CALERTE              PIC X(05).                        
AL2601           15 TS01-CREPRISE             PIC X(05).                        
      *                                                                         
      *--- DONNEES DU CODIC                                           *         
           05 TS01-DONNEES-CODIC.                                               
              10 TS01-CFAM                 PIC X(05).                           
              10 TS01-CTAUXTVA             PIC X(05).                           
              10 TS01-CPRESTGRP            PIC X(05).                           
              10 TS01-FILLER4              PIC 9(03).                           
      *--------- SUPPRESSION DE CTYPCONDT (REMPLACER PAR CGRPGEO)               
              10 TS01-CGRPGEO              PIC X(05).                           
              10 TS01-WLCONF               PIC X.                               
              10 TS01-CSIGNE     REDEFINES TS01-WLCONF PIC X.                   
              10 TS01-FILLER5              PIC X.                               
              10 TS01-QTYPLIEN             PIC S9(3) COMP-3.                    
      *                                                               *         
      * ------------                                                  *         
                                                                                
