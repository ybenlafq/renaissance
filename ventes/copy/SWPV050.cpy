      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      * LG = 180                                                                
      *                                                                         
       01      FPV050-DSECT.                                                    
           03  FPV050-CLEF.                                                     
>003        05 FPV050-NMAG               PIC  X(03).                            
>009        05 FPV050-CVENDEUR           PIC  X(06).                            
>012        05 FPV050-NAGREGATED         PIC S9(05)    COMP-3.                  
>032       03  FPV050-LMAG               PIC  X(20).                            
>052       03  FPV050-LVENDEUR           PIC  X(20).                            
>072       03  FPV050-LAGREGATED         PIC  X(20).                            
           03  FPV050-TAB.                                                      
            05  FPV050-TAB-VENTES OCCURS 2 TIMES.                               
>076         07 FPV050-QVENDUE            PIC S9(07)    COMP-3.                 
>080         07 FPV050-QVENDUESV          PIC S9(07)    COMP-3.                 
>086         07 FPV050-PMTCA              PIC S9(09)V99 COMP-3.                 
>092         07 FPV050-MTREM              PIC S9(09)V99 COMP-3.                 
>098         07 FPV050-MTCOMM             PIC S9(09)V99 COMP-3.                 
>102         07 FPV050-QNBPSE             PIC S9(07)    COMP-3.                 
>106         07 FPV050-QNBPSAB            PIC S9(07)    COMP-3.                 
>112         07 FPV050-PMTCAPSE           PIC S9(09)V99 COMP-3.                 
           03  FILLER REDEFINES FPV050-TAB.                                     
>076        05 FPV050-QVENDUE-M          PIC S9(07)    COMP-3.                  
>080        05 FPV050-QVENDUESV-M        PIC S9(07)    COMP-3.                  
>086        05 FPV050-PMTCA-M            PIC S9(09)V99 COMP-3.                  
>092        05 FPV050-MTREM-M            PIC S9(09)V99 COMP-3.                  
>098        05 FPV050-MTCOMM-M           PIC S9(09)V99 COMP-3.                  
>102        05 FPV050-QNBPSE-M           PIC S9(07)    COMP-3.                  
>106        05 FPV050-QNBPSAB-M          PIC S9(07)    COMP-3.                  
>112        05 FPV050-PMTCAPSE-M         PIC S9(09)V99 COMP-3.                  
>116        05 FPV050-QVENDUE-C          PIC S9(07)    COMP-3.                  
>120        05 FPV050-QVENDUESV-C        PIC S9(07)    COMP-3.                  
>126        05 FPV050-PMTCA-C            PIC S9(09)V99 COMP-3.                  
>132        05 FPV050-MTREM-C            PIC S9(09)V99 COMP-3.                  
>138        05 FPV050-MTCOMM-C           PIC S9(09)V99 COMP-3.                  
>142        05 FPV050-QNBPSE-C           PIC S9(07)    COMP-3.                  
>146        05 FPV050-QNBPSAB-C          PIC S9(07)    COMP-3.                  
>152        05 FPV050-PMTCAPSE-C         PIC S9(09)V99 COMP-3.                  
>180       03  FILLER                    PIC  X(28).                            
                                                                                
