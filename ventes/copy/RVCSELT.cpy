      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CSELT SEL ART AUTOR TRANSFERT MUTOU    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCSELT .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCSELT .                                                            
      *}                                                                        
           05  CSELT-CTABLEG2    PIC X(15).                                     
           05  CSELT-CTABLEG2-REDEF REDEFINES CSELT-CTABLEG2.                   
               10  CSELT-CSELA           PIC X(05).                             
           05  CSELT-WTABLEG     PIC X(80).                                     
           05  CSELT-WTABLEG-REDEF  REDEFINES CSELT-WTABLEG.                    
               10  CSELT-WFLAG           PIC X(01).                             
               10  CSELT-CORIG           PIC X(05).                             
               10  CSELT-LCOMMENT        PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVCSELT-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVCSELT-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSELT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CSELT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CSELT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CSELT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
