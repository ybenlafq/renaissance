      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:10 >
      
      *****************************************************************
      *
      *  COPY MESSAGE ECHANGE ENTRE MTL06 ET TCBEMTL010 SUR AS400.
      *
      *  LA DESCRIPTION DE MESSAGE SUR L'AS400 DOIT ETRE STRICTEMENT
      *  IDENTIQUE A CE SOURCE.
      *
      *  DANS LE PROGRAMME, CE COPY DOIT ETRE PRECEDE DE CELUI DE
      *  L'ENTETE STANDARD DARTY (MESSLENT).
      *
      *****************************************************************
      *
           10 MESS-TL06-MESSAGE.
      *
      *       Ent�te : - r�f�rences de la tourn�e de livraison.
      *                - droits du magasin.
              15 MESS-TL06-ENTETE.
                 20 MESS-TL06-PROFIL           PIC    X(05).
                 20 MESS-TL06-DTOUR            PIC    X(08).
                 20 MESS-TL06-NTOUR            PIC    X(03).
                 20 MESS-TL06-AUTO             PIC    X(01).
                    88 MESS-TL06-AUCUNE-AUTO   VALUE ' '.
                    88 MESS-TL06-ENCAISSER     VALUE 'E'.
                    88 MESS-TL06-TOPER         VALUE 'T'.
                    88 MESS-TL06-ENC-ET-TOP    VALUE 'Y'.
                    88 MESS-TL06-ENC-OU-TOP    VALUE 'O'.
      *
      *       Caract�ristiques de la tourn�e de livraison.
              15 MESS-TL06-C-TOURNEE.
      *          Indicateur "Tourn�e OK/Erreur".
                 20 MESS-TL06-INDTOUR          PIC    X(01).
      *             Tourn�e OK.
                    88 MESS-TL06-T-OK          VALUE 'O'.
      *             Tourn�e inexistante.
                    88 MESS-TL06-T-INEXIST     VALUE 'N'.
      *             Tourn�e top�e.
                    88 MESS-TL06-T-TOPEE       VALUE 'T'.
      *             Vente non disponible.
                    88 MESS-TL06-T-VTE-IND     VALUE 'V'.
      *             Retour anticip�.
                    88 MESS-TL06-T-RET-ANT     VALUE 'R'.
      *             Autre(s) erreur(s).
                    88 MESS-TL06-T-AUT-ERR     VALUE 'X'.
      *
                 20 MESS-TL06-CLIVR1           PIC    X(10).
                 20 MESS-TL06-LLIVR1           PIC    X(20).
                 20 MESS-TL06-CLIVR2           PIC    X(10).
                 20 MESS-TL06-LLIVR2           PIC    X(20).
                 20 MESS-TL06-CLIVR3           PIC    X(10).
                 20 MESS-TL06-LLIVR3           PIC    X(20).
                 20 MESS-TL06-NBVTE            PIC    9(03).
                 20 MESS-TL06-NBLIG            PIC    9(03).
      *
      *       Table des lignes de vente de la tourn�e.
              15 MESS-TL06-TABLE.
                 20 MESS-TL06-LIGNE         OCCURS    250
                                           INDEXED    MESS-TL06-I.
                    25 MESS-TL06-NLIEU         PIC    X(03).
                    25 MESS-TL06-NORDV         PIC    X(07).
                    25 MESS-TL06-NCODIC        PIC    X(07).
                    25 MESS-TL06-NSEQNQ        PIC    9(05).
                    25 MESS-TL06-QLIVRE        PIC    9(05).
                    25 MESS-TL06-TYPADR        PIC    X(01).
                    25 MESS-TL06-MTBL          PIC   S9(07)V99.
                    25 MESS-TL06-PLIVRE        PIC   S9(07)V99.
                    25 MESS-TL06-CTYPE         PIC    X(01).
                    25 MESS-TL06-NOMCLI        PIC    X(10).
                    25 MESS-TL06-MESS          PIC    X(35).
                    25 MESS-TL06-CGR           PIC    X(04).
      
