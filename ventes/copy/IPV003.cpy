      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV003 AU 28/06/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,05,BI,A,                          *        
      *                           26,20,BI,A,                          *        
      *                           46,07,BI,A,                          *        
      *                           53,06,PD,A,                          *        
      *                           59,08,BI,A,                          *        
      *                           67,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV003.                                                        
            05 NOMETAT-IPV003           PIC X(6) VALUE 'IPV003'.                
            05 RUPTURES-IPV003.                                                 
           10 IPV003-NSOCIETE           PIC X(03).                      007  003
           10 IPV003-CHEFPROD           PIC X(05).                      010  005
           10 IPV003-NLIEU              PIC X(03).                      015  003
           10 IPV003-WSEQFAM            PIC S9(05)      COMP-3.         018  003
           10 IPV003-CMARQ              PIC X(05).                      021  005
           10 IPV003-LREFFOURN          PIC X(20).                      026  020
           10 IPV003-NCODIC             PIC X(07).                      046  007
           10 IPV003-PEXPTTC            PIC S9(08)V9(2) COMP-3.         053  006
           10 IPV003-DEFFET             PIC X(08).                      059  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV003-SEQUENCE           PIC S9(04) COMP.                067  002
      *--                                                                       
           10 IPV003-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV003.                                                   
           10 IPV003-CFAM               PIC X(05).                      069  005
           10 IPV003-CPROGRAMME         PIC X(05).                      074  005
           10 IPV003-LENSCONC           PIC X(15).                      079  015
           10 IPV003-NCONC              PIC X(04).                      094  004
           10 IPV003-NZONPRIX           PIC X(02).                      098  002
           10 IPV003-PSTDTTC            PIC S9(08)V9(2) COMP-3.         100  006
           10 IPV003-DFINEFFET          PIC X(08).                      106  008
           10 IPV003-FDATE              PIC X(08).                      114  008
           10 IPV003-FSEM               PIC X(08).                      122  008
            05 FILLER                      PIC X(383).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV003-LONG           PIC S9(4)   COMP  VALUE +129.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV003-LONG           PIC S9(4) COMP-5  VALUE +129.           
                                                                                
      *}                                                                        
