      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE TMP03                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 122.             00060010
       01  TS-DONNEES.                                                  00070000
              10 TS-CCAMPAGNE   PIC X(04).                              00080001
              10 TS-LIBCAMP     PIC X(30).                              00090002
              10 TS-DDEBUT      PIC X(08).                              00100001
              10 TS-DFIN        PIC X(08).                              00110001
              10 TS-CTRANS1     PIC X(01).                              00120004
              10 TS-CTRANS2     PIC X(01).                              00130004
              10 TS-CTRANS3     PIC X(01).                              00140004
              10 TS-CTRANS4     PIC X(01).                              00150004
              10 TS-CTRANS5     PIC X(01).                              00160004
              10 TS-CTRANS6     PIC X(01).                              00170004
              10 TS-CTRANS7     PIC X(01).                              00180004
              10 TS-CTRANS8     PIC X(01).                              00190004
              10 TS-DCREATION.                                          00191007
                 15 TS-DAACRE   PIC X(04).                              00192007
                 15 TS-DMMCRE   PIC X(02).                              00193007
                 15 TS-DJJCRE   PIC X(02).                              00194007
              10 TS-FAMILLE     OCCURS 10.                              00200005
                 15 TS-CFAM     PIC X(05).                              00210005
              10 TS-CMONO       PIC X(01).                              00220008
              10 TS-CMULTI      PIC X(01).                              00230008
              10 TS-CMAG        PIC X(01).                              00240009
              10 TS-CDCOM       PIC X(01).                              00250009
              10 TS-CBTOB       PIC X(01).                              00260009
              10 TS-CMGD        PIC X(01).                              00270010
                                                                                
