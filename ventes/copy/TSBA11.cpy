      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSBA11 = TS DES BA RECEPTIONNES                            *         
      *                                                               *         
      *    CETTE TS CONTIENT LES BA RECEPTIONNES POUR UN MAGASIN      *         
      *                                                               *         
      *    LONGUEUR TOTALE = 576 NOUVELLE LONGUEUR 936                *         
      *---------------------------------------------------------------*         
      * 1/06/02 MODIFICATION UTILISATION DE LA DOUCHETTE POUR SAISIE  *         
      * 1/06/02 DES BONS D'ACHAT RECEPTIONNéS DANS UNE AUTRE FILIALE  *         
      *29/02/08 TIERS GCT CHANGé EN TIERS SAP (6 -> 10)               *         
      *****************************************************************         
      *                                                               *         
       01  IT-MAX                    PIC S9(05) COMP-3 VALUE 36.                
      *                                                               *         
       01  TS-IDENT.                                                            
           05 FILLER                 PIC X(04)         VALUE 'BA11'.            
           05 TS-TERM                PIC X(04)         VALUE SPACES.            
      *                                                               *         
0208  *01  TS-LONG                   PIC S9(04) COMP   VALUE +792.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
0208  *01  TS-LONG                   PIC S9(04) COMP   VALUE +936.              
      *--                                                                       
       01  TS-LONG                   PIC S9(04) COMP-5   VALUE +936.            
      *}                                                                        
      *                                                               *         
       01  TS-DATA.                                                             
           05 TS-DONNEES.                                                       
              10 TS-LIGNE            OCCURS 36.                                 
                 15 TS-NBA           PIC X(06).                                 
                 15 TS-NBA-9         REDEFINES TS-NBA     PIC 9(06).            
                 15 TS-NFIL          PIC X(06).                                 
0208  *          15 TS-NTIERS        PIC X(06).                                 
--    *          15 TS-NTIERS-9      REDEFINES TS-NTIERS  PIC 9(06).            
--               15 TS-NTIERSSAP     PIC X(10).                                 
0208             15 TS-NTIERSSAP-9   REDEFINES TS-NTIERSSAP PIC 9(10).          
                 15 TS-PBA           PIC S9(05)V99 COMP-3.                      
                                                                                
