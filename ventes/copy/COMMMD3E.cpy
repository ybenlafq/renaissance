      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MD3E-LONG-COMMAREA         PIC S9(4) COMP VALUE +4096.  00000010
      *--                                                                       
       01  COMM-MD3E-LONG-COMMAREA         PIC S9(4) COMP-5 VALUE +4096.        
      *}                                                                        
       01  COMM-MD3E-APPLI.                                             00000011
           02 COMM-MD3E-ZONES-ENTREE.                                   00000020
              05 COMM-MD3E-CTYPE               PIC X(2).                00000021
              05 COMM-MD3E-CTVA                PIC X(5).                00000030
              05 COMM-MD3E-DEFFET              PIC X(8).                00000040
              05 COMM-MD3E-PRIXIN              PIC S9(9)V9(2) COMP-3.   00000054
      *                                                                         
           02 COMM-MD3E-ZONES-SORTIE.                                   00000100
              05 COMM-MD3E-CODRET              PIC S999.                00000110
              05 COMM-MD3E-LMESS               PIC X(68).               00000120
              05 COMM-MD3E-TAUXTVA             PIC S9(3)V9(2) COMP-3.   00000180
              05 COMM-MD3E-COEFTVA             PIC S9(3)V9(4) COMP-3.   00000180
              05 COMM-MD3E-PRIXOUT             PIC S9(9)V9(2) COMP-3.   00000054
           02 COMM-MD3E-FILLER                 PIC X(3390).             00000100
                                                                                
