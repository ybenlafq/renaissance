      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : PREPARATION DE PAYE                              *        
      *  TRANSACTION: PP00                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION PP00                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *--- NOMBRE DE POSTES MAXIMUM POUR TYPE RUBRIQUE ET RUBRIQUE              
      *---(A METTRE A JOUR A CHAQUE CHANGEMENT DU NOMBRE D' OCCURS)             
       01  COMM-PP00-NRUB-MAX           PIC 9(02)            VALUE 14.          
       01  COMM-PP00-NRUB1-MAX          PIC 9(02)            VALUE 10.          
       01  COMM-PP00-NRUB2-MAX          PIC 9(02)            VALUE 02.          
       01  COMM-PP00-NRUB2-FIN          PIC 9(02)            VALUE 12.          
       01  COMM-PP00-NRUB3-MAX          PIC 9(02)            VALUE 02.          
       01  COMM-PP00-NLIGNE             PIC 9(02).                              
           88 COMM-PP00-LIGNE-MOUVT     VALUE 01 THRU 10.                       
           88 COMM-PP00-LIGNE-INTER     VALUE 11 THRU 12.                       
           88 COMM-PP00-LIGNE-INTER1    VALUE 11.                               
           88 COMM-PP00-LIGNE-INTER2    VALUE 12.                               
           88 COMM-PP00-LIGNE-FRANC     VALUE 13 THRU 14.                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-PP00-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-PP00-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *                                                                         
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS        PIC 9(02).                              
              05 COMM-DATE-SEMAA        PIC 9(02).                              
              05 COMM-DATE-SEMNU        PIC 9(02).                              
           02 COMM-DATE-FILLER          PIC X(07).                              
      *                                                                         
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      *--- ZONES APPLICATIVES PP00 GENERALES --------------------- 724          
           02 COMM-PP00-APPLI.                                                  
      *-      LIEU DE SAISIE                                                    
              05 COMM-PP00-NLIEUORIG       PIC X(03).                           
              05 COMM-PP00-CTYPSOC         PIC X(03).                           
      *-      MAGASIN DE TRAITEMENT                                             
              05 COMM-PP00-MAGASIN.                                             
                 10 COMM-PP00-NSOCIETE        PIC X(03).                        
                 10 COMM-PP00-NLIEU           PIC X(03).                        
                 10 COMM-PP00-LLIEU           PIC X(20).                        
                 10 COMM-PP00-CGRPMAG         PIC X(02).                        
                 10 COMM-PP00-DMOISPAYE       PIC X(06).                        
                 10 COMM-PP00-DMOISENCOURS    PIC X(06).                        
      *-      DONNEES COMMUNES A TOUS LES PROGRAMMES                            
              05 COMM-PP00-DONNEES.                                             
                 10 COMM-PP00-WDEVERS         PIC X(01).                        
                    88 COMM-PP00-DEVERSEMENT-NON-EFF      VALUE ' '.            
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-PP00-DEVERSEMENT-EFFECTUE     VALUE '1','2'.        
      *--                                                                       
                    88 COMM-PP00-DEVERSEMENT-EFFECTUE     VALUE '1' '2'.        
      *}                                                                        
                    88 COMM-PP00-ENVELOPPE-CREEE          VALUE '2'.            
                 10 COMM-PP00-MESSAGE.                                          
                    15 COMM-PP00-CODRET          PIC X(01).                     
                       88 COMM-PP00-CODRET-OK               VALUE ' '.          
                       88 COMM-PP00-CODRET-ERREUR           VALUE '1'.          
                 10 COMM-PP00-LIBERR          PIC X(58).                        
                 10 COMM-PP00-TOP-SORTIE      PIC X(01).                        
                    88 COMM-PP00-SORTIE-NON-CONFIRMEE       VALUE ' '.          
                    88 COMM-PP00-SORTIE-CONFIRMEE           VALUE '1'.          
              05 COMM-PP00-OPTION.                                              
                 10 COMM-PP00-OPTPARAM     PIC X(02).                           
                 10 COMM-PP00-CPRIME       PIC X(05).                           
                 10 COMM-PP00-LOPTION      PIC X(20).                           
              05 COMM-PP00-TAB-NB          PIC 9(02).                           
              05 COMM-PP00-TAB-MENU        OCCURS 16.                           
                 10 COMM-PP00-TAB-COPTION  PIC X(02).                           
                 10 COMM-PP00-TAB-CPRIME   PIC X(05).                           
                 10 COMM-PP00-TAB-LOPTION  PIC X(20).                           
              05 COMM-PP00-DMOISSTAT       PIC X(06).                           
              05 COMM-PP00-FILLER          PIC X(148).                          
      *                                                                         
      *--- ZONES APPLICATIVES PP00 ------------------------------ 3000          
           02 COMM-PP00-REDEFINES          PIC X(3000).                         
      *                                                                         
      *--- ZONES APPLICATIVES PP10 -----------------------------------          
           02 COMM-PP10-APPLI REDEFINES COMM-PP00-REDEFINES.                    
      *-      TABLEAU PRIMES C                                                  
              05 COMM-PP10-TAB-PPPRI       OCCURS 2.                            
                 10 COMM-PP10-CPRIME          PIC X(05).                        
                 10 COMM-PP10-LNOMZONE        PIC X(10).                        
                 10 COMM-PP10-WEXISTENCE      PIC X(01).                        
                    88 COMM-PP10-RTPP00-NON-CREE            VALUE ' '.          
                    88 COMM-PP10-RTPP00-EXISTE              VALUE '1'.          
                 10 COMM-PP10-PENVELOPPE      PIC S9(07)V99 COMP-3.             
      *                                                                         
      *--- ZONES APPLICATIVES PP20 -----------------------------------          
           02 COMM-PP20-APPLI REDEFINES COMM-PP00-REDEFINES PIC X(3000).        
      *                                                                         
      *--- ZONES APPLICATIVES PP30 -----------------------------------          
           02 COMM-PP30-APPLI REDEFINES COMM-PP00-REDEFINES.                    
              05 COMM-PP30-FLAG-PP10       PIC X(01).                           
                 88 COMM-PP30-CREAT-PP10                    VALUE ' '.          
                 88 COMM-PP30-MODIF-PP10                    VALUE '1'.          
              05 COMM-PP30-DB2.                                                 
                 07 COMM-PP30-TABLE           OCCURS 14.                        
                    10 COMM-PP30-NLIGNE          PIC X(02).                     
                    10 COMM-PP30-PART1.                                         
                       15 COMM-PP30-NRUB         PIC X(02).                     
                       15 COMM-PP30-LRUB         PIC X(30).                     
                       15 COMM-PP30-CORIGINEZIN  PIC X(02).                     
                       15 COMM-PP30-CORIGINEZ1   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ1   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-CORIGINEZ2   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ2   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-CORIGINEZ3   PIC X(02).                     
                       15 COMM-PP30-PMONTANTZ3   PIC S9(11)V9999 COMP-3.        
                       15 COMM-PP30-COPERATEUR1  PIC X(01).                     
                       15 COMM-PP30-COPERATEUR2  PIC X(01).                     
                       15 COMM-PP30-CPRIORITE    PIC X(01).                     
                       15 COMM-PP30-CCALCUL      PIC X(01).                     
                    10 COMM-PP30-PART2.                                         
                      15 COMM-PP30-PMONTANTZIN PIC S9(11)V9999 COMP-3.          
                 07 COMM-PP30-LTYPRUB            PIC X(30) OCCURS 02.           
      *                                                                         
                                                                                
