      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....: DECLARATION PRODUIT                             *
      * FICHIER.....: EMISSION COLIS LIGNE STANDARD (MUTATION)        *
      * NOM FICHIER.: ECLSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 107                                             *
      *****************************************************************
      *
       01  ECLSTD6.
      * CODE IDENTIFICATEUR
           05      ECLSTD6-TYP-ENREG       PIC  X(0006).
      * CODE SOCI�T�
           05      ECLSTD6-CSOCIETE        PIC  X(0005).
      * NUM�RO D'OP
           05      ECLSTD6-NOP             PIC  X(0015).
      * NUM�RO DE COLIS
           05      ECLSTD6-NCOLIS          PIC  X(0008).
      * NUM�RO DE LA LIGNE
           05      ECLSTD6-NLIGNE          PIC  9(0005).
      * NUM�RO DE LA SOUS LIGNE
           05      ECLSTD6-NSSLIGNE        PIC  9(0005).
      * CODE ARTICLE
           05      ECLSTD6-CARTICLE        PIC  X(0018).
      * CODE CONDITIONNEMENT
           05      ECLSTD6-CONDITIONNEMENT PIC  X(0005).
      * QUANTIT� R�ELLE
           05      ECLSTD6-QTE-REELLE      PIC  9(0009).
      * QUANTIT� TH�ORIQUE
           05      ECLSTD6-QTE-THEORIQUE   PIC  9(0009).
      * FILLER 30
           05      ECLSTD6-FILLER          PIC  X(0021).
      * FIN
           05      ECLSTD6-FIN             PIC  X(0001).
      
