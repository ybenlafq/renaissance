      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC0203                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC0203.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC0203.                                                            
      *}                                                                        
           10 EC02-NSOCIETE        PIC X(3).                                    
           10 EC02-NLIEU           PIC X(3).                                    
           10 EC02-NVENTE          PIC X(7).                                    
           10 EC02-DVENTE          PIC X(8).                                    
           10 EC02-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC02-WTPCMD          PIC X(5).                                    
           10 EC02-CPAYM           PIC X(5).                                    
           10 EC02-WPAYM           PIC X(1).                                    
           10 EC02-MTPAYM          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 EC02-LCADEAU.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC02-LCADEAU-LEN  PIC S9(4) USAGE COMP.                        
      *--                                                                       
              49 EC02-LCADEAU-LEN  PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
              49 EC02-LCADEAU-TEXT  PIC X(255).                                 
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           10 EC02-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC02-DVALP           PIC X(8).                                    
           10 EC02-WVALP           PIC X(1).                                    
           10 EC02-WPAYDEM         PIC X(1).                                    
           10 EC02-CCPSWD          PIC X(10).                                   
           10 EC02-WCC             PIC X(1).                                    
           10 EC02-CCEVENT         PIC X(4).                                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
