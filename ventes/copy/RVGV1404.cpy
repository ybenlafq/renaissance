      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                        00010000
      **********************************************************        00020000
      *   COPY DE LA TABLE RVGV1404                                     00030001
      **********************************************************        00040000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV1404                 00050001
      **********************************************************        00060000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1404.                                                    00070001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1404.                                                            
      *}                                                                        
           02  GV14-NSOCIETE                                            00080000
               PIC X(0003).                                             00090000
           02  GV14-NLIEU                                               00100000
               PIC X(0003).                                             00110000
           02  GV14-NORDRE                                              00120000
               PIC X(0005).                                             00130000
           02  GV14-NVENTE                                              00140000
               PIC X(0007).                                             00150000
           02  GV14-DSAISIE                                             00160000
               PIC X(0008).                                             00170000
           02  GV14-NSEQ                                                00180000
               PIC X(0002).                                             00190000
           02  GV14-CMODPAIMT                                           00200000
               PIC X(0005).                                             00210000
           02  GV14-PREGLTVTE                                           00220000
               PIC S9(7)V9(0002) COMP-3.                                00230000
           02  GV14-NREGLTVTE                                           00240000
               PIC X(0007).                                             00250000
           02  GV14-DREGLTVTE                                           00260000
               PIC X(0008).                                             00270000
           02  GV14-WREGLTVTE                                           00280000
               PIC X(0001).                                             00290000
           02  GV14-DCOMPTA                                             00300000
               PIC X(0008).                                             00310000
           02  GV14-DSYST                                               00320000
               PIC S9(13) COMP-3.                                       00330000
           02  GV14-CPROTOUR                                            00340000
               PIC X(0005).                                             00350000
           02  GV14-CTOURNEE                                            00360000
               PIC X(0003).                                             00370000
           02  GV14-CDEVISE                                             00380000
               PIC X(0003).                                             00390000
           02  GV14-MDEVISE                                             00400000
               PIC S9(7)V9(0002) COMP-3.                                00410000
           02  GV14-MECART                                              00420000
               PIC S9V9(0002) COMP-3.                                   00430000
           02  GV14-CDEV                                                00440000
               PIC X(0003).                                             00450000
           02  GV14-NSOCP                                               00460000
               PIC X(0003).                                             00470000
           02  GV14-NLIEUP                                              00480000
               PIC X(0003).                                             00490000
           02  GV14-NTRANS                                              00500000
               PIC S9(8) COMP-3.                                        00510000
           02  GV14-PMODPAIMT                                           00520000
               PIC S9(7)V9(0002) COMP-3.                                00530000
           02  GV14-CVENDEUR                                            00540000
               PIC X(6).                                                00550000
           02  GV14-CFCRED                                              00560000
               PIC X(5).                                                00570000
           02  GV14-NCREDI                                              00580000
               PIC X(14).                                               00590000
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************        00600000
      *   LISTE DES FLAGS DE LA TABLE RVGV1404                          00610001
      **********************************************************        00620000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1404-FLAGS.                                              00630001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1404-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSOCIETE-F                                          00640000
      *        PIC S9(4) COMP.                                          00650000
      *--                                                                       
           02  GV14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NLIEU-F                                             00660000
      *        PIC S9(4) COMP.                                          00670000
      *--                                                                       
           02  GV14-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NORDRE-F                                            00680000
      *        PIC S9(4) COMP.                                          00690000
      *--                                                                       
           02  GV14-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NVENTE-F                                            00700000
      *        PIC S9(4) COMP.                                          00710000
      *--                                                                       
           02  GV14-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSAISIE-F                                           00720000
      *        PIC S9(4) COMP.                                          00730000
      *--                                                                       
           02  GV14-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSEQ-F                                              00740000
      *        PIC S9(4) COMP.                                          00750000
      *--                                                                       
           02  GV14-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CMODPAIMT-F                                         00760000
      *        PIC S9(4) COMP.                                          00770000
      *--                                                                       
           02  GV14-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-PREGLTVTE-F                                         00780000
      *        PIC S9(4) COMP.                                          00790000
      *--                                                                       
           02  GV14-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NREGLTVTE-F                                         00800000
      *        PIC S9(4) COMP.                                          00810000
      *--                                                                       
           02  GV14-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DREGLTVTE-F                                         00820000
      *        PIC S9(4) COMP.                                          00830000
      *--                                                                       
           02  GV14-DREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-WREGLTVTE-F                                         00840000
      *        PIC S9(4) COMP.                                          00850000
      *--                                                                       
           02  GV14-WREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DCOMPTA-F                                           00860000
      *        PIC S9(4) COMP.                                          00870000
      *--                                                                       
           02  GV14-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSYST-F                                             00880000
      *        PIC S9(4) COMP.                                          00890000
      *--                                                                       
           02  GV14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CPROTOUR-F                                          00900000
      *        PIC S9(4) COMP.                                          00910000
      *--                                                                       
           02  GV14-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CTOURNEE-F                                          00920000
      *        PIC S9(4) COMP.                                          00930000
      *--                                                                       
           02  GV14-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CDEVISE-F                                           00940000
      *        PIC S9(4) COMP.                                          00950000
      *--                                                                       
           02  GV14-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MDEVISE-F                                           00960000
      *        PIC S9(4) COMP.                                          00970000
      *--                                                                       
           02  GV14-MDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MECART-F                                            00980000
      *        PIC S9(4) COMP.                                          00990000
      *--                                                                       
           02  GV14-MECART-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CDEV-F                                              01000000
      *        PIC S9(4) COMP.                                          01010000
      *--                                                                       
           02  GV14-CDEV-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSOCP-F                                             01020000
      *        PIC S9(4) COMP.                                          01030000
      *--                                                                       
           02  GV14-NSOCP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NLIEUP-F                                            01040000
      *        PIC S9(4) COMP.                                          01050000
      *--                                                                       
           02  GV14-NLIEUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NTRANS-F                                            01060000
      *        PIC S9(4) COMP.                                          01070000
      *--                                                                       
           02  GV14-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-PMODPAIMT-F                                         01080000
      *        PIC S9(4) COMP.                                          01090000
      *--                                                                       
           02  GV14-PMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CVENDEUR-F                                          01100000
      *        PIC S9(4) COMP.                                          01110000
      *--                                                                       
           02  GV14-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CFCRED-F                                            01120000
      *        PIC S9(4) COMP.                                          01130000
      *--                                                                       
           02  GV14-CFCRED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NCREDI-F                                            01140000
      *        PIC S9(4) COMP.                                          01150000
      *                                                                         
      *--                                                                       
           02  GV14-NCREDI-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
