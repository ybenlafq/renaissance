      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: SANS (BATCH MQ)                                  *        
      *  TITRE      : COMMAREA DE L'APPLICATION GV50 GV29              *        
      *  LONGUEUR   : 492                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-GV29-APPLI.                                             00260000
            10  COMM-GV29-DATA-VTE.                                             
               20   COMM-GV29-NOMPGM          PIC X(8).                         
               20   COMM-GV29-CETAT           PIC X(10).                        
               20   FILLER                    PIC X(10).                        
               20   COMM-GV29-NSOC            PIC X(3).                         
               20   FILLER                    PIC X(10).                        
               20   COMM-GV29-NLIEU           PIC X(3).                         
               20   FILLER                    PIC X(10).                        
               20   COMM-GV29-NVENTE          PIC X(7).                         
               20   FILLER                    PIC X(36).                        
            15  COMM-GV29-COMMENTAIRE.                                          
               20   COMM-GV29-LCOMMENT1      PIC X(79).                         
               20   COMM-GV29-LCOMMENT2      PIC X(79).                         
               20   COMM-GV29-LCOMMENT3      PIC X(79).                         
               20   COMM-GV29-LCOMMENT4      PIC X(79).                         
               20   COMM-GV29-LCOMMENT5      PIC X(79).                         
               20   COMM-GV29-LCOMMENT6      PIC X(79).                         
               20   COMM-GV29-MODIFVTE       PIC X(79).                         
               20   COMM-GV29-LCOMMENT7      PIC X(79).                         
                                                                                
