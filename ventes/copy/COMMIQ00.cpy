      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------*  00001000
      *                                                              *  00002000
      *                                                              *  00003000
      *                                                              *  00004000
      *                                                              *  00005000
      * -------------------------------------------------------------*  00006000
       01  Z-COMMAREA.                                                  00007000
                                                                        00008000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00009000
           02 FILLER-IQ00-AIDA      PIC X(100).                         00010000
                                                                        00020000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00030000
           02 COMM-CICS-IQ00-APPLID PIC X(08).                          00040000
           02 COMM-CICS-IQ00-NETNAM PIC X(08).                          00050000
           02 COMM-CICS-IQ00-TRANSA PIC X(04).                          00060000
                                                                        00061000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00062000
           02 COMM-DATE-IQ00-SIECLE PIC X(02).                          00063000
           02 COMM-DATE-IQ00-ANNEE PIC X(02).                           00064000
           02 COMM-DATE-IQ00-MOIS  PIC X(02).                           00065000
           02 COMM-DATE-IQ00-JOUR  PIC 99.                              00066000
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00067000
           02 COMM-DATE-IQ00-QNTA  PIC 999.                             00068000
           02 COMM-DATE-IQ00-QNT0  PIC 99999.                           00069000
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00070000
           02 COMM-DATE-IQ00-BISX            PIC 9.                     00071001
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00072000
           02 COMM-DATE-IQ00-JSM             PIC 9.                     00073001
      *   LIBELLES DU JOUR COURT - LONG                                 00074000
           02 COMM-DATE-IQ00-JSM-LC          PIC X(03).                 00075001
           02 COMM-DATE-IQ00-JSM-LL          PIC X(08).                 00076001
      *   LIBELLES DU MOIS COURT - LONG                                 00077000
           02 COMM-DATE-IQ00-MOIS-LC         PIC X(03).                 00078001
           02 COMM-DATE-IQ00-MOIS-LL         PIC X(08).                 00079001
      *   DIFFERENTES FORMES DE DATE                                    00079100
           02 COMM-DATE-IQ00-SSAAMMJJ        PIC X(08).                 00079201
           02 COMM-DATE-IQ00-AAMMJJ          PIC X(06).                 00079301
           02 COMM-DATE-IQ00-JJMMSSAA        PIC X(08).                 00079401
           02 COMM-DATE-IQ00-JJMMAA          PIC X(06).                 00079501
           02 COMM-DATE-IQ00-JJ-MM-AA        PIC X(08).                 00079601
           02 COMM-DATE-IQ00-JJ-MM-SSAA      PIC X(10).                 00079701
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00079800
           02 COMM-DATE-IQ00-WEEK.                                      00079900
              05 COMM-DATE-IQ00-SEMSS        PIC 99.                    00080001
              05 COMM-DATE-IQ00-SEMAA        PIC 99.                    00081001
              05 COMM-DATE-IQ00-SEMNU        PIC 99.                    00082001
           02 COMM-DATE-IQ00-FILLER          PIC X(08).                 00083001
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00084000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS                 PIC S9(4) COMP VALUE -1.   00085001
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-CURS                 PIC S9(4) COMP-5 VALUE -1.         
                                                                        00086000
      *}                                                                        
           02 COMM-IQ00-APPLI.                                          00087000
              05 COMM-IQ00-LEVEL-MAX         PIC X(06).                 00087101
              05 COMM-IQ00-PAGE              PIC 9(03).                 00087201
              05 COMM-IQ00-PAGE-MAX          PIC 9(03).                 00087301
              05 COMM-IQ00-CONTEXTE.                                    00087501
                 10 COMM-IQ00-TRANS          PIC X(04).                 00087601
                 10 COMM-IQ00-DETROMPEUR     PIC X(04).                 00087701
                 10 COMM-IQ00-FONCTION       PIC X(02).                 00087901
                 10 COMM-IQ00-NSOCIETE       PIC X(03).                 00088001
                 10 COMM-IQ00-NLIEU          PIC X(03).                 00088101
                 10 COMM-IQ00-CPOSTAL        PIC X(05).                 00088501
                 10 COMM-IQ00-NCODIC         PIC X(07).                 00088601
                 10 COMM-IQ00-LDM            PIC X(03).                 00088909
                 10 COMM-IQ00-STKAV          PIC X(01).                 00089009
                 10 FILLER                   PIC X(43).                 00089109
      * -> VISIBILIT� DE CAT�GORIES DE SERVICES...                      00089209
      *    UN JOUR ON ESSAIERA DE FAIRE UN TRUC BIEN...                 00089309
              05 COMM-IQ00-LIVRQ          PIC X(09).                    00089409
              05 FILLER REDEFINES COMM-IQ00-LIVRQ OCCURS 09.            00089509
                    10 COMM-IQ00-LIVRQ-P     PIC X(01).                 00089609
              05 COMM-IQ00-FILTRE.                                      00089709
                 10 COMM-IQ00-CPOSTALF       PIC X(05).                 00089809
                 10 COMM-IQ00-COMMUNEF       PIC X(05).                 00089909
              05 COMM-IQ00-NZONPRIX          PIC X(02).                 00090009
              05 COM-IQ00-STAGE1.                                       00090109
                  10 COM-IQ00-CPOSTAL        PIC X(05).                 00090209
                  10 COM-IQ00-LCOMMUNE       PIC X(32).                 00090309
                  10 COM-IQ00-CINSEE         PIC X(05).                 00090409
                  10 COM-IQ00-CELEMEN        PIC X(05).                 00091001
                  10 COM-IQ00-NCODIC         PIC X(07).                 00100001
                  10 COM-IQ00-QVOLUME        PIC S9(9)V99 COMP-3        00101015
                                             VALUE ZEROES.              00102007
                  10 COM-IQ00-CFAM           PIC X(05).                 00110001
                  10 COMM-IQ00-CROSS-DOCK    PIC X(01).                 00110114
                  10 COM-IQ00-CPT-DISPO      PIC S9(04) COMP-3.         00111013
              05 COM-IQ00-STAGE2.                                       00120001
                 10 COM-SRVCE-AFFICHAGE      PIC 9(01)        VALUE 0.  00130001
                    88 COM-SRVCE-FAM-VIDE                     VALUE 0.  00130101
                    88 COM-SRVCE-FAM-OK                       VALUE 1.  00130201
                 10 COM-SRVCE-CFAMSRVCE      PIC X(05).                 00131111
                 10 COM-SRVCE-LFAMSRVCE      PIC X(20).                 00132001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10 COM-STAGE2-TS-DISPO      PIC S9(4) COMP.            00133010
      *--                                                                       
                 10 COM-STAGE2-TS-DISPO      PIC S9(4) COMP-5.                  
      *}                                                                        
                 10 COM-STAGE2-DDISPO        PIC X(08).                 00140012
                 10 COM-SRVCE-NBSRVCE        PIC S9(3) COMP-3 VALUE 0.  00141012
                 10 COM-TAB1.                                           00150001
                 15 COM-TAB1-LIGNE      OCCURS 100.                     00151001
                    20 COM-SRVCE-SRVCE       PIC X(05).                 00160001
                    20 COM-SRVCE-CMODDEL-S   PIC X(03).                 00170001
                    20 COM-SRVCE-CMODDEL     PIC X(03).                 00170101
                    20 COM-SRVCE-CELEMEN     PIC X(05).                 00171001
                    20 COM-SRVCE-CPERIM      PIC X(05).                 00172001
                    20 COM-SRVCE-CPLAGE      PIC X(02).                 00180001
                    20 COM-SRVCE-CADRE       PIC X(05).                 00190001
                    20 COM-SRVCE-CEQUIPE     PIC X(05).                 00200001
                    20 COM-SRVCE-MINI        PIC 9(02).                 00200101
                    20 COM-SRVCE-MAXI        PIC 9(03).                 00200201
                    20 COM-SRVCE-DDISPO      PIC X(08).                 00200302
                    20 COM-SRVCE-CPLAGES     PIC X(30).                 00200408
      * -> ZONE CONTENANT LES ZONES DE LIVRAISONS DONN�ES PAR MIQ00     00200508
              05 COM-IQ00-STAGE3             PIC X(23200).              00201001
              05 COM-IQ00-DATED              PIC X(08).                 00210001
              05 COM-IQ00-DATED10            PIC X(10).                 00211001
              05 COMM-IQ02-PAGED             PIC 9(03).                 00220001
              05 COMM-IQ02-PAGED-MAX         PIC 9(03).                 00230001
              05 COMM-IQ02-PAGEPL            PIC 9(03).                 00240001
              05 COMM-IQ02-PAGEPL-MAX        PIC 9(03).                 00250001
              05 COMM-IQ02-LIGNE-COUR        PIC 9(03).                 00251001
              05 COMM-IQ02-LIGNE-MAX         PIC 9(03).                 00252001
              05 COMM-IQ02-TRT-CRITERE       PIC X(01).                 00260001
                                                                                
