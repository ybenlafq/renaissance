      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV0900                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0900                         
      **********************************************************                
       01  RVHV0900.                                                            
           02  HV09-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV09-CFAM                                                        
               PIC X(0005).                                                     
           02  HV09-DVENTELIVREE                                                
               PIC X(0006).                                                     
           02  HV09-QPIECES                                                     
               PIC S9(7) COMP-3.                                                
           02  HV09-PCA                                                         
               PIC S9(9)V9(0002) COMP-3.                                        
           02  HV09-PMTACHATS                                                   
               PIC S9(9)V9(0002) COMP-3.                                        
           02  HV09-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV0900                                  
      **********************************************************                
       01  RVHV0900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-DVENTELIVREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-DVENTELIVREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV09-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV09-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  HV09-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
