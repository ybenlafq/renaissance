      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV1500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV1500                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1500.                                                            
      *}                                                                        
           02  GV15-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV15-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV15-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV15-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV15-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV15-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV15-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV15-CARACTSPE1                                                  
               PIC X(0005).                                                     
           02  GV15-CVCARACTSPE1                                                
               PIC X(0005).                                                     
           02  GV15-CARACTSPE2                                                  
               PIC X(0005).                                                     
           02  GV15-CVCARACTSPE2                                                
               PIC X(0005).                                                     
           02  GV15-CARACTSPE3                                                  
               PIC X(0005).                                                     
           02  GV15-CVCARACTSPE3                                                
               PIC X(0005).                                                     
           02  GV15-CARACTSPE4                                                  
               PIC X(0005).                                                     
           02  GV15-CVCARACTSPE4                                                
               PIC X(0005).                                                     
           02  GV15-CARACTSPE5                                                  
               PIC X(0005).                                                     
           02  GV15-CVCARACTSPE5                                                
               PIC X(0005).                                                     
           02  GV15-WANNULCAR                                                   
               PIC X(0001).                                                     
           02  GV15-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV1500                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CARACTSPE1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CARACTSPE1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CVCARACTSPE1-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CVCARACTSPE1-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CARACTSPE2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CARACTSPE2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CVCARACTSPE2-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CVCARACTSPE2-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CARACTSPE3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CARACTSPE3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CVCARACTSPE3-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CVCARACTSPE3-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CARACTSPE4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CARACTSPE4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CVCARACTSPE4-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CVCARACTSPE4-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CARACTSPE5-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CARACTSPE5-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-CVCARACTSPE5-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-CVCARACTSPE5-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-WANNULCAR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV15-WANNULCAR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV15-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GV15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
