      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL11   ESL11                                              00000020
      ***************************************************************** 00000030
       01   ESL11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEML   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEMF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTRIL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCTRIF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCTRII    PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTRIL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLTRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLTRIF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLTRII    PIC X(40).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDETAILL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDETAILL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDETAILF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDETAILI  PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNCODICI  PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCFAMI    PIC X(5).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCMARQI   PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEEDL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDATEEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEEDF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEEDI  PIC X(8).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEEFL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATEEFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEEFF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEEFI  PIC X(8).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWARBL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MWARBL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWARBF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWARBI    PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCCREL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MSOCCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSOCCREF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MSOCCREI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUCREL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLIEUCREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUCREF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLIEUCREI      PIC X(3).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPEL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MTYPEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTYPEF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MTYPEI    PIC X(2).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNUMDOCI  PIC X(7).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATERDL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDATERDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATERDF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDATERDI  PIC X(8).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATERFL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDATERFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATERFF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDATERFI  PIC X(8).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODOPEL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCODOPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODOPEF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCODOPEI  PIC X(5).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCODL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSOCODL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCODF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSOCODI   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUODL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIEUODL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUODF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIEUODI  PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCOFL   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MSOCOFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCOFF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MSOCOFI   PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUOFL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MLIEUOFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUOFF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MLIEUOFI  PIC X(3).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDDL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MSOCDDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCDDF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MSOCDDI   PIC X(3).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDDL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIEUDDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUDDF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIEUDDI  PIC X(3).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCDFL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MSOCDFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MSOCDFF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MSOCDFI   PIC X(3).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUDFL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIEUDFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIEUDFF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIEUDFI  PIC X(3).                                       00001170
           02 MLIGD OCCURS   15 TIMES .                                 00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGL   COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MLIGL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MLIGF   PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MLIGI   PIC X(79).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(79).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: ESL11   ESL11                                              00001440
      ***************************************************************** 00001450
       01   ESL11O REDEFINES ESL11I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MPAGEA    PIC X.                                          00001630
           02 MPAGEC    PIC X.                                          00001640
           02 MPAGEP    PIC X.                                          00001650
           02 MPAGEH    PIC X.                                          00001660
           02 MPAGEV    PIC X.                                          00001670
           02 MPAGEO    PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MPAGEMA   PIC X.                                          00001700
           02 MPAGEMC   PIC X.                                          00001710
           02 MPAGEMP   PIC X.                                          00001720
           02 MPAGEMH   PIC X.                                          00001730
           02 MPAGEMV   PIC X.                                          00001740
           02 MPAGEMO   PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCTRIA    PIC X.                                          00001770
           02 MCTRIC    PIC X.                                          00001780
           02 MCTRIP    PIC X.                                          00001790
           02 MCTRIH    PIC X.                                          00001800
           02 MCTRIV    PIC X.                                          00001810
           02 MCTRIO    PIC X.                                          00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLTRIA    PIC X.                                          00001840
           02 MLTRIC    PIC X.                                          00001850
           02 MLTRIP    PIC X.                                          00001860
           02 MLTRIH    PIC X.                                          00001870
           02 MLTRIV    PIC X.                                          00001880
           02 MLTRIO    PIC X(40).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MDETAILA  PIC X.                                          00001910
           02 MDETAILC  PIC X.                                          00001920
           02 MDETAILP  PIC X.                                          00001930
           02 MDETAILH  PIC X.                                          00001940
           02 MDETAILV  PIC X.                                          00001950
           02 MDETAILO  PIC X.                                          00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MNCODICA  PIC X.                                          00001980
           02 MNCODICC  PIC X.                                          00001990
           02 MNCODICP  PIC X.                                          00002000
           02 MNCODICH  PIC X.                                          00002010
           02 MNCODICV  PIC X.                                          00002020
           02 MNCODICO  PIC X(7).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MCFAMA    PIC X.                                          00002050
           02 MCFAMC    PIC X.                                          00002060
           02 MCFAMP    PIC X.                                          00002070
           02 MCFAMH    PIC X.                                          00002080
           02 MCFAMV    PIC X.                                          00002090
           02 MCFAMO    PIC X(5).                                       00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MCMARQA   PIC X.                                          00002120
           02 MCMARQC   PIC X.                                          00002130
           02 MCMARQP   PIC X.                                          00002140
           02 MCMARQH   PIC X.                                          00002150
           02 MCMARQV   PIC X.                                          00002160
           02 MCMARQO   PIC X(5).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MDATEEDA  PIC X.                                          00002190
           02 MDATEEDC  PIC X.                                          00002200
           02 MDATEEDP  PIC X.                                          00002210
           02 MDATEEDH  PIC X.                                          00002220
           02 MDATEEDV  PIC X.                                          00002230
           02 MDATEEDO  PIC X(8).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDATEEFA  PIC X.                                          00002260
           02 MDATEEFC  PIC X.                                          00002270
           02 MDATEEFP  PIC X.                                          00002280
           02 MDATEEFH  PIC X.                                          00002290
           02 MDATEEFV  PIC X.                                          00002300
           02 MDATEEFO  PIC X(8).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MWARBA    PIC X.                                          00002330
           02 MWARBC    PIC X.                                          00002340
           02 MWARBP    PIC X.                                          00002350
           02 MWARBH    PIC X.                                          00002360
           02 MWARBV    PIC X.                                          00002370
           02 MWARBO    PIC X.                                          00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MSOCCREA  PIC X.                                          00002400
           02 MSOCCREC  PIC X.                                          00002410
           02 MSOCCREP  PIC X.                                          00002420
           02 MSOCCREH  PIC X.                                          00002430
           02 MSOCCREV  PIC X.                                          00002440
           02 MSOCCREO  PIC X(3).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLIEUCREA      PIC X.                                     00002470
           02 MLIEUCREC PIC X.                                          00002480
           02 MLIEUCREP PIC X.                                          00002490
           02 MLIEUCREH PIC X.                                          00002500
           02 MLIEUCREV PIC X.                                          00002510
           02 MLIEUCREO      PIC X(3).                                  00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MTYPEA    PIC X.                                          00002540
           02 MTYPEC    PIC X.                                          00002550
           02 MTYPEP    PIC X.                                          00002560
           02 MTYPEH    PIC X.                                          00002570
           02 MTYPEV    PIC X.                                          00002580
           02 MTYPEO    PIC X(2).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MNUMDOCA  PIC X.                                          00002610
           02 MNUMDOCC  PIC X.                                          00002620
           02 MNUMDOCP  PIC X.                                          00002630
           02 MNUMDOCH  PIC X.                                          00002640
           02 MNUMDOCV  PIC X.                                          00002650
           02 MNUMDOCO  PIC X(7).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MDATERDA  PIC X.                                          00002680
           02 MDATERDC  PIC X.                                          00002690
           02 MDATERDP  PIC X.                                          00002700
           02 MDATERDH  PIC X.                                          00002710
           02 MDATERDV  PIC X.                                          00002720
           02 MDATERDO  PIC X(8).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MDATERFA  PIC X.                                          00002750
           02 MDATERFC  PIC X.                                          00002760
           02 MDATERFP  PIC X.                                          00002770
           02 MDATERFH  PIC X.                                          00002780
           02 MDATERFV  PIC X.                                          00002790
           02 MDATERFO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MCODOPEA  PIC X.                                          00002820
           02 MCODOPEC  PIC X.                                          00002830
           02 MCODOPEP  PIC X.                                          00002840
           02 MCODOPEH  PIC X.                                          00002850
           02 MCODOPEV  PIC X.                                          00002860
           02 MCODOPEO  PIC X(5).                                       00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MSOCODA   PIC X.                                          00002890
           02 MSOCODC   PIC X.                                          00002900
           02 MSOCODP   PIC X.                                          00002910
           02 MSOCODH   PIC X.                                          00002920
           02 MSOCODV   PIC X.                                          00002930
           02 MSOCODO   PIC X(3).                                       00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MLIEUODA  PIC X.                                          00002960
           02 MLIEUODC  PIC X.                                          00002970
           02 MLIEUODP  PIC X.                                          00002980
           02 MLIEUODH  PIC X.                                          00002990
           02 MLIEUODV  PIC X.                                          00003000
           02 MLIEUODO  PIC X(3).                                       00003010
           02 FILLER    PIC X(2).                                       00003020
           02 MSOCOFA   PIC X.                                          00003030
           02 MSOCOFC   PIC X.                                          00003040
           02 MSOCOFP   PIC X.                                          00003050
           02 MSOCOFH   PIC X.                                          00003060
           02 MSOCOFV   PIC X.                                          00003070
           02 MSOCOFO   PIC X(3).                                       00003080
           02 FILLER    PIC X(2).                                       00003090
           02 MLIEUOFA  PIC X.                                          00003100
           02 MLIEUOFC  PIC X.                                          00003110
           02 MLIEUOFP  PIC X.                                          00003120
           02 MLIEUOFH  PIC X.                                          00003130
           02 MLIEUOFV  PIC X.                                          00003140
           02 MLIEUOFO  PIC X(3).                                       00003150
           02 FILLER    PIC X(2).                                       00003160
           02 MSOCDDA   PIC X.                                          00003170
           02 MSOCDDC   PIC X.                                          00003180
           02 MSOCDDP   PIC X.                                          00003190
           02 MSOCDDH   PIC X.                                          00003200
           02 MSOCDDV   PIC X.                                          00003210
           02 MSOCDDO   PIC X(3).                                       00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MLIEUDDA  PIC X.                                          00003240
           02 MLIEUDDC  PIC X.                                          00003250
           02 MLIEUDDP  PIC X.                                          00003260
           02 MLIEUDDH  PIC X.                                          00003270
           02 MLIEUDDV  PIC X.                                          00003280
           02 MLIEUDDO  PIC X(3).                                       00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MSOCDFA   PIC X.                                          00003310
           02 MSOCDFC   PIC X.                                          00003320
           02 MSOCDFP   PIC X.                                          00003330
           02 MSOCDFH   PIC X.                                          00003340
           02 MSOCDFV   PIC X.                                          00003350
           02 MSOCDFO   PIC X(3).                                       00003360
           02 FILLER    PIC X(2).                                       00003370
           02 MLIEUDFA  PIC X.                                          00003380
           02 MLIEUDFC  PIC X.                                          00003390
           02 MLIEUDFP  PIC X.                                          00003400
           02 MLIEUDFH  PIC X.                                          00003410
           02 MLIEUDFV  PIC X.                                          00003420
           02 MLIEUDFO  PIC X(3).                                       00003430
           02 DFHMS1 OCCURS   15 TIMES .                                00003440
             03 FILLER       PIC X(2).                                  00003450
             03 MLIGA   PIC X.                                          00003460
             03 MLIGC   PIC X.                                          00003470
             03 MLIGP   PIC X.                                          00003480
             03 MLIGH   PIC X.                                          00003490
             03 MLIGV   PIC X.                                          00003500
             03 MLIGO   PIC X(79).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(79).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
