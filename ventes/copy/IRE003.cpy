      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE003      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,01,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE003.                                                        
            05 NOMETAT-IRE003           PIC X(6) VALUE 'IRE003'.                
            05 RUPTURES-IRE003.                                                 
           10 IRE003-NSOCIETE           PIC X(03).                      007  003
           10 IRE003-CGRPMAG            PIC X(02).                      010  002
           10 IRE003-NLIEU              PIC X(03).                      012  003
           10 IRE003-NJOUR              PIC X(01).                      015  001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE003-SEQUENCE           PIC S9(04) COMP.                016  002
      *--                                                                       
           10 IRE003-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE003.                                                   
           10 IRE003-JOUR               PIC X(03).                      018  003
           10 IRE003-TOT-VAL-FORC-M     PIC S9(15)      COMP-3.         021  008
           10 IRE003-TOT-VAL-FORC-P     PIC S9(15)      COMP-3.         029  008
           10 IRE003-TOT-VAL-REM        PIC S9(15)      COMP-3.         037  008
           10 IRE003-TOT-VENTES2        PIC S9(15)      COMP-3.         045  008
           10 IRE003-VAL-FORC-M         PIC S9(13)      COMP-3.         053  007
           10 IRE003-VAL-FORC-P         PIC S9(13)      COMP-3.         060  007
           10 IRE003-VAL-REM            PIC S9(13)      COMP-3.         067  007
           10 IRE003-VENTES1            PIC S9(13)      COMP-3.         074  007
           10 IRE003-PERDEB             PIC X(08).                      081  008
           10 IRE003-PERFIN             PIC X(08).                      089  008
            05 FILLER                      PIC X(416).                          
                                                                                
