      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV2300                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV2300                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV2300.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV2300.                                                            
      *}                                                                        
           02  GV23-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV23-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV23-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV23-CTYPENREG                                                   
               PIC X(0001).                                                     
           02  GV23-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV23-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV23-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV23-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV23-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV23-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV23-CENREG                                                      
               PIC X(0005).                                                     
           02  GV23-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV23-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV23-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV23-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV23-CEQUIPE                                                     
               PIC X(0005).                                                     
           02  GV23-NLIGNE                                                      
               PIC X(0002).                                                     
           02  GV23-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GV23-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  GV23-PRMP                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV23-WEMPORTE                                                    
               PIC X(0001).                                                     
           02  GV23-CPLAGE                                                      
               PIC X(0002).                                                     
           02  GV23-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  GV23-CADRTOUR                                                    
               PIC X(0001).                                                     
           02  GV23-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GV23-LCOMMENT                                                    
               PIC X(0035).                                                     
           02  GV23-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GV23-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV23-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV23-NAUTORM                                                     
               PIC X(0005).                                                     
           02  GV23-WARTINEX                                                    
               PIC X(0001).                                                     
           02  GV23-WEDITBL                                                     
               PIC X(0001).                                                     
           02  GV23-WACOMMUTER                                                  
               PIC X(0001).                                                     
           02  GV23-WCQERESF                                                    
               PIC X(0001).                                                     
           02  GV23-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GV23-CTOURNEE                                                    
               PIC X(0008).                                                     
           02  GV23-WTOPELIVRE                                                  
               PIC X(0001).                                                     
           02  GV23-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  GV23-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV23-HCREATION                                                   
               PIC X(0004).                                                     
           02  GV23-DANNULATION                                                 
               PIC X(0008).                                                     
           02  GV23-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GV23-WINTMAJ                                                     
               PIC X(0001).                                                     
           02  GV23-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV2300                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV2300-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV2300-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CTYPENREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CTYPENREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CEQUIPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CEQUIPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WEMPORTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WEMPORTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CADRTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WARTINEX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WARTINEX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WEDITBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WEDITBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WACOMMUTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WACOMMUTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WTOPELIVRE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WTOPELIVRE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-HCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-HCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-WINTMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV23-WINTMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV23-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GV23-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
