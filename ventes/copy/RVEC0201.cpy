      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RVEC0201                    *        
      ******************************************************************        
       01  RVEC1201.                                                            
           10 EC02-NSOCIETE        PIC X(3).                                    
           10 EC02-NLIEU           PIC X(3).                                    
           10 EC02-NVENTE          PIC X(7).                                    
           10 EC02-DVENTE          PIC X(8).                                    
           10 EC02-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC02-WTPCMD          PIC X(5).                                    
           10 EC02-CPAYM           PIC X(5).                                    
           10 EC02-WPAYM           PIC X(1).                                    
           10 EC02-MTPAYM          PIC S9(7)V9(2) USAGE COMP-3.                 
           10 EC02-LCADEAU.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC02-LCADEAU-LEN  PIC S9(4) USAGE COMP.                        
      *--                                                                       
              49 EC02-LCADEAU-LEN  PIC S9(4) COMP-5.                            
      *}                                                                        
              49 EC02-LCADEAU-TEXT  PIC X(255).                                 
           10 EC02-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC02-DVALP           PIC X(8).                                    
           10 EC02-WVALP           PIC X(1).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *        
      ******************************************************************        
                                                                                
