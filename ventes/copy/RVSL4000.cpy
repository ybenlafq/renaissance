      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * DCLGEN TABLE(RVSL4000)                                         *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSL4000                      *        
      ******************************************************************        
       01 RVSL4000.                                                             
           10 SL40-NSOCIETE        PIC X(3).                                    
           10 SL40-CLIEUINV        PIC X(5).                                    
           10 SL40-LLIEUINV        PIC X(20).                                   
           10 SL40-NSEQEDT         PIC S9(3)V USAGE COMP-3.                     
           10 SL40-CEMPL           PIC X(5).                                    
           10 SL40-WEXPO           PIC X(1).                                    
           10 SL40-WGLOTS          PIC X(1).                                    
           10 SL40-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVSL4000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-NSOCIETE-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-CLIEUINV-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-CLIEUINV-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-LLIEUINV-F      PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-LLIEUINV-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-NSEQEDT-F       PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-NSEQEDT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-CEMPL-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-CEMPL-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-WEXPO-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-WEXPO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-WGLOTS-F        PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-WGLOTS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL40-DSYST-F         PIC S9(4) USAGE COMP.                        
      *--                                                                       
           10 SL40-DSYST-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 8       *        
      ******************************************************************        
                                                                                
