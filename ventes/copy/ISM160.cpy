      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM160 AU 26/01/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,05,BI,A,                          *        
      *                           17,03,PD,A,                          *        
      *                           20,05,BI,A,                          *        
      *                           25,20,BI,A,                          *        
      *                           45,20,BI,A,                          *        
      *                           65,20,BI,A,                          *        
      *                           85,20,BI,A,                          *        
      *                           05,03,BI,A,                          *        
      *                           08,02,PD,A,                          *        
      *                           10,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISM160.                                                        
            05 NOMETAT-ISM160           PIC X(6) VALUE 'ISM160'.                
            05 RUPTURES-ISM160.                                                 
           10 ISM160-NSOCIETE           PIC X(03).                      007  003
           10 ISM160-GRP                PIC X(02).                      010  002
           10 ISM160-CHEFPROD           PIC X(05).                      012  005
           10 ISM160-WSEQFAM            PIC S9(05)      COMP-3.         017  003
           10 ISM160-CFAM               PIC X(05).                      020  005
           10 ISM160-LFAM               PIC X(20).                      025  020
           10 ISM160-LVMARKET1          PIC X(20).                      045  020
           10 ISM160-LVMARKET2          PIC X(20).                      065  020
           10 ISM160-LVMARKET3          PIC X(20).                      085  020
           10 ISM160-CAPPRO             PIC X(03).                      105  003
           10 ISM160-TRIEXPO            PIC S9(03)      COMP-3.         108  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM160-SEQUENCE           PIC S9(04) COMP.                110  002
      *--                                                                       
           10 ISM160-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISM160.                                                   
           10 ISM160-COMMENTAIRE        PIC X(04).                      112  004
           10 ISM160-GAMME1             PIC X(05).                      116  005
           10 ISM160-GAMME10            PIC X(05).                      121  005
           10 ISM160-GAMME11            PIC X(05).                      126  005
           10 ISM160-GAMME12            PIC X(05).                      131  005
           10 ISM160-GAMME13            PIC X(05).                      136  005
           10 ISM160-GAMME14            PIC X(05).                      141  005
           10 ISM160-GAMME15            PIC X(05).                      146  005
           10 ISM160-GAMME16            PIC X(05).                      151  005
           10 ISM160-GAMME17            PIC X(05).                      156  005
           10 ISM160-GAMME18            PIC X(05).                      161  005
           10 ISM160-GAMME19            PIC X(05).                      166  005
           10 ISM160-GAMME2             PIC X(05).                      171  005
           10 ISM160-GAMME3             PIC X(05).                      176  005
           10 ISM160-GAMME4             PIC X(05).                      181  005
           10 ISM160-GAMME5             PIC X(05).                      186  005
           10 ISM160-GAMME6             PIC X(05).                      191  005
           10 ISM160-GAMME7             PIC X(05).                      196  005
           10 ISM160-GAMME8             PIC X(05).                      201  005
           10 ISM160-GAMME9             PIC X(05).                      206  005
           10 ISM160-LIBDATE            PIC X(12).                      211  012
           10 ISM160-LIBELLE            PIC X(06).                      223  006
           10 ISM160-MAG1               PIC X(03).                      229  003
           10 ISM160-MAG10              PIC X(03).                      232  003
           10 ISM160-MAG11              PIC X(03).                      235  003
           10 ISM160-MAG12              PIC X(03).                      238  003
           10 ISM160-MAG13              PIC X(03).                      241  003
           10 ISM160-MAG14              PIC X(03).                      244  003
           10 ISM160-MAG15              PIC X(03).                      247  003
           10 ISM160-MAG16              PIC X(03).                      250  003
           10 ISM160-MAG17              PIC X(03).                      253  003
           10 ISM160-MAG18              PIC X(03).                      256  003
           10 ISM160-MAG19              PIC X(03).                      259  003
           10 ISM160-MAG2               PIC X(03).                      262  003
           10 ISM160-MAG3               PIC X(03).                      265  003
           10 ISM160-MAG4               PIC X(03).                      268  003
           10 ISM160-MAG5               PIC X(03).                      271  003
           10 ISM160-MAG6               PIC X(03).                      274  003
           10 ISM160-MAG7               PIC X(03).                      277  003
           10 ISM160-MAG8               PIC X(03).                      280  003
           10 ISM160-MAG9               PIC X(03).                      283  003
           10 ISM160-WEDIT1             PIC X(01).                      286  001
           10 ISM160-WEDIT2             PIC X(01).                      287  001
           10 ISM160-WEDIT3             PIC X(01).                      288  001
           10 ISM160-NBREF              PIC S9(04)      COMP-3.         289  003
           10 ISM160-QNBDEP             PIC S9(04)      COMP-3.         292  003
           10 ISM160-QNBR1              PIC S9(04)      COMP-3.         295  003
           10 ISM160-QNBR10             PIC S9(04)      COMP-3.         298  003
           10 ISM160-QNBR11             PIC S9(04)      COMP-3.         301  003
           10 ISM160-QNBR12             PIC S9(04)      COMP-3.         304  003
           10 ISM160-QNBR13             PIC S9(04)      COMP-3.         307  003
           10 ISM160-QNBR14             PIC S9(04)      COMP-3.         310  003
           10 ISM160-QNBR15             PIC S9(04)      COMP-3.         313  003
           10 ISM160-QNBR16             PIC S9(04)      COMP-3.         316  003
           10 ISM160-QNBR17             PIC S9(04)      COMP-3.         319  003
           10 ISM160-QNBR18             PIC S9(04)      COMP-3.         322  003
           10 ISM160-QNBR19             PIC S9(04)      COMP-3.         325  003
           10 ISM160-QNBR2              PIC S9(04)      COMP-3.         328  003
           10 ISM160-QNBR3              PIC S9(04)      COMP-3.         331  003
           10 ISM160-QNBR4              PIC S9(04)      COMP-3.         334  003
           10 ISM160-QNBR5              PIC S9(04)      COMP-3.         337  003
           10 ISM160-QNBR6              PIC S9(04)      COMP-3.         340  003
           10 ISM160-QNBR7              PIC S9(04)      COMP-3.         343  003
           10 ISM160-QNBR8              PIC S9(04)      COMP-3.         346  003
           10 ISM160-QNBR9              PIC S9(04)      COMP-3.         349  003
           10 ISM160-TAUX               PIC S9(03)      COMP-3.         352  002
            05 FILLER                      PIC X(159).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-ISM160-LONG           PIC S9(4)   COMP  VALUE +353.           
      *                                                                         
      *--                                                                       
        01  DSECT-ISM160-LONG           PIC S9(4) COMP-5  VALUE +353.           
                                                                                
      *}                                                                        
