      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EHV01   EHV01                                              00000020
      ***************************************************************** 00000030
       01   EHV01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSEMAINEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSEMAINEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSEMAINEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSEMAINEI     PIC X(6).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCRAYONI  PIC X(5).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRAYONL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLRAYONF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLRAYONI  PIC X(20).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCINDICL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCINDICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCINDICF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCINDICI  PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCFAMI    PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLFAMI    PIC X(20).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNEEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDANNEEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDANNEEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDANNEEI  PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTYPEI   PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDLUNDIL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDLUNDIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDLUNDIF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDLUNDII  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMARDIL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDMARDIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMARDIF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDMARDII  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMERCREDIL    COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MDMERCREDIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDMERCREDIF    PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDMERCREDII    PIC X(4).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDJEUDIL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MDJEUDIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDJEUDIF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MDJEUDII  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENDREDIL    COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MDVENDREDIL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDVENDREDIF    PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDVENDREDII    PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSAMEDIL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MDSAMEDIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDSAMEDIF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDSAMEDII      PIC X(4).                                  00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDIMANCHEL    COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MDDIMANCHEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDDIMANCHEF    PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDDIMANCHEI    PIC X(4).                                  00000770
           02 M247I OCCURS   13 TIMES .                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLRAYONFAML  COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MLRAYONFAML COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MLRAYONFAMF  PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MLRAYONFAMI  PIC X(20).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQLUNDIL     COMP PIC S9(4).                            00000830
      *--                                                                       
             03 MQLUNDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQLUNDIF     PIC X.                                     00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MQLUNDII     PIC X(5).                                  00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMARDIL     COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MQMARDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQMARDIF     PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MQMARDII     PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQMERCREDIL  COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MQMERCREDIL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQMERCREDIF  PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MQMERCREDII  PIC X(5).                                  00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQJEUDIL     COMP PIC S9(4).                            00000950
      *--                                                                       
             03 MQJEUDIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQJEUDIF     PIC X.                                     00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQJEUDII     PIC X(5).                                  00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQVENDREDIL  COMP PIC S9(4).                            00000990
      *--                                                                       
             03 MQVENDREDIL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQVENDREDIF  PIC X.                                     00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MQVENDREDII  PIC X(5).                                  00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQSAMEDIL    COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQSAMEDIL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MQSAMEDIF    PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQSAMEDII    PIC X(5).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQDIMANCHEL  COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MQDIMANCHEL COMP-5 PIC S9(4).                                   
      *}                                                                        
             03 MQDIMANCHEF  PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MQDIMANCHEI  PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCSEMAINEL   COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MCSEMAINEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCSEMAINEF   PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MCSEMAINEI   PIC X(6).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOISL      COMP PIC S9(4).                            00001150
      *--                                                                       
             03 MCMOISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMOISF      PIC X.                                     00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MCMOISI      PIC X(7).                                  00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MZONCMDI  PIC X(15).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MLIBERRI  PIC X(58).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCODTRAI  PIC X(4).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MCICSI    PIC X(5).                                       00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MNETNAMI  PIC X(8).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MSCREENI  PIC X(4).                                       00001420
      ***************************************************************** 00001430
      * SDF: EHV01   EHV01                                              00001440
      ***************************************************************** 00001450
       01   EHV01O REDEFINES EHV01I.                                    00001460
           02 FILLER    PIC X(12).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MTIMJOUA  PIC X.                                          00001560
           02 MTIMJOUC  PIC X.                                          00001570
           02 MTIMJOUP  PIC X.                                          00001580
           02 MTIMJOUH  PIC X.                                          00001590
           02 MTIMJOUV  PIC X.                                          00001600
           02 MTIMJOUO  PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MWPAGEA   PIC X.                                          00001630
           02 MWPAGEC   PIC X.                                          00001640
           02 MWPAGEP   PIC X.                                          00001650
           02 MWPAGEH   PIC X.                                          00001660
           02 MWPAGEV   PIC X.                                          00001670
           02 MWPAGEO   PIC X(3).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNSEMAINEA     PIC X.                                     00001700
           02 MNSEMAINEC     PIC X.                                     00001710
           02 MNSEMAINEP     PIC X.                                     00001720
           02 MNSEMAINEH     PIC X.                                     00001730
           02 MNSEMAINEV     PIC X.                                     00001740
           02 MNSEMAINEO     PIC X(6).                                  00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MCRAYONA  PIC X.                                          00001770
           02 MCRAYONC  PIC X.                                          00001780
           02 MCRAYONP  PIC X.                                          00001790
           02 MCRAYONH  PIC X.                                          00001800
           02 MCRAYONV  PIC X.                                          00001810
           02 MCRAYONO  PIC X(5).                                       00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MLRAYONA  PIC X.                                          00001840
           02 MLRAYONC  PIC X.                                          00001850
           02 MLRAYONP  PIC X.                                          00001860
           02 MLRAYONH  PIC X.                                          00001870
           02 MLRAYONV  PIC X.                                          00001880
           02 MLRAYONO  PIC X(20).                                      00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MCINDICA  PIC X.                                          00001910
           02 MCINDICC  PIC X.                                          00001920
           02 MCINDICP  PIC X.                                          00001930
           02 MCINDICH  PIC X.                                          00001940
           02 MCINDICV  PIC X.                                          00001950
           02 MCINDICO  PIC X(2).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCFAMA    PIC X.                                          00001980
           02 MCFAMC    PIC X.                                          00001990
           02 MCFAMP    PIC X.                                          00002000
           02 MCFAMH    PIC X.                                          00002010
           02 MCFAMV    PIC X.                                          00002020
           02 MCFAMO    PIC X(5).                                       00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLFAMA    PIC X.                                          00002050
           02 MLFAMC    PIC X.                                          00002060
           02 MLFAMP    PIC X.                                          00002070
           02 MLFAMH    PIC X.                                          00002080
           02 MLFAMV    PIC X.                                          00002090
           02 MLFAMO    PIC X(20).                                      00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDANNEEA  PIC X.                                          00002120
           02 MDANNEEC  PIC X.                                          00002130
           02 MDANNEEP  PIC X.                                          00002140
           02 MDANNEEH  PIC X.                                          00002150
           02 MDANNEEV  PIC X.                                          00002160
           02 MDANNEEO  PIC X(4).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MCTYPEA   PIC X.                                          00002190
           02 MCTYPEC   PIC X.                                          00002200
           02 MCTYPEP   PIC X.                                          00002210
           02 MCTYPEH   PIC X.                                          00002220
           02 MCTYPEV   PIC X.                                          00002230
           02 MCTYPEO   PIC X(2).                                       00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MDLUNDIA  PIC X.                                          00002260
           02 MDLUNDIC  PIC X.                                          00002270
           02 MDLUNDIP  PIC X.                                          00002280
           02 MDLUNDIH  PIC X.                                          00002290
           02 MDLUNDIV  PIC X.                                          00002300
           02 MDLUNDIO  PIC X(4).                                       00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MDMARDIA  PIC X.                                          00002330
           02 MDMARDIC  PIC X.                                          00002340
           02 MDMARDIP  PIC X.                                          00002350
           02 MDMARDIH  PIC X.                                          00002360
           02 MDMARDIV  PIC X.                                          00002370
           02 MDMARDIO  PIC X(4).                                       00002380
           02 FILLER    PIC X(2).                                       00002390
           02 MDMERCREDIA    PIC X.                                     00002400
           02 MDMERCREDIC    PIC X.                                     00002410
           02 MDMERCREDIP    PIC X.                                     00002420
           02 MDMERCREDIH    PIC X.                                     00002430
           02 MDMERCREDIV    PIC X.                                     00002440
           02 MDMERCREDIO    PIC X(4).                                  00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MDJEUDIA  PIC X.                                          00002470
           02 MDJEUDIC  PIC X.                                          00002480
           02 MDJEUDIP  PIC X.                                          00002490
           02 MDJEUDIH  PIC X.                                          00002500
           02 MDJEUDIV  PIC X.                                          00002510
           02 MDJEUDIO  PIC X(4).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MDVENDREDIA    PIC X.                                     00002540
           02 MDVENDREDIC    PIC X.                                     00002550
           02 MDVENDREDIP    PIC X.                                     00002560
           02 MDVENDREDIH    PIC X.                                     00002570
           02 MDVENDREDIV    PIC X.                                     00002580
           02 MDVENDREDIO    PIC X(4).                                  00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MDSAMEDIA      PIC X.                                     00002610
           02 MDSAMEDIC PIC X.                                          00002620
           02 MDSAMEDIP PIC X.                                          00002630
           02 MDSAMEDIH PIC X.                                          00002640
           02 MDSAMEDIV PIC X.                                          00002650
           02 MDSAMEDIO      PIC X(4).                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MDDIMANCHEA    PIC X.                                     00002680
           02 MDDIMANCHEC    PIC X.                                     00002690
           02 MDDIMANCHEP    PIC X.                                     00002700
           02 MDDIMANCHEH    PIC X.                                     00002710
           02 MDDIMANCHEV    PIC X.                                     00002720
           02 MDDIMANCHEO    PIC X(4).                                  00002730
           02 M247O OCCURS   13 TIMES .                                 00002740
             03 FILLER       PIC X(2).                                  00002750
             03 MLRAYONFAMA  PIC X.                                     00002760
             03 MLRAYONFAMC  PIC X.                                     00002770
             03 MLRAYONFAMP  PIC X.                                     00002780
             03 MLRAYONFAMH  PIC X.                                     00002790
             03 MLRAYONFAMV  PIC X.                                     00002800
             03 MLRAYONFAMO  PIC X(20).                                 00002810
             03 FILLER       PIC X(2).                                  00002820
             03 MQLUNDIA     PIC X.                                     00002830
             03 MQLUNDIC     PIC X.                                     00002840
             03 MQLUNDIP     PIC X.                                     00002850
             03 MQLUNDIH     PIC X.                                     00002860
             03 MQLUNDIV     PIC X.                                     00002870
             03 MQLUNDIO     PIC X(5).                                  00002880
             03 FILLER       PIC X(2).                                  00002890
             03 MQMARDIA     PIC X.                                     00002900
             03 MQMARDIC     PIC X.                                     00002910
             03 MQMARDIP     PIC X.                                     00002920
             03 MQMARDIH     PIC X.                                     00002930
             03 MQMARDIV     PIC X.                                     00002940
             03 MQMARDIO     PIC X(5).                                  00002950
             03 FILLER       PIC X(2).                                  00002960
             03 MQMERCREDIA  PIC X.                                     00002970
             03 MQMERCREDIC  PIC X.                                     00002980
             03 MQMERCREDIP  PIC X.                                     00002990
             03 MQMERCREDIH  PIC X.                                     00003000
             03 MQMERCREDIV  PIC X.                                     00003010
             03 MQMERCREDIO  PIC X(5).                                  00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MQJEUDIA     PIC X.                                     00003040
             03 MQJEUDIC     PIC X.                                     00003050
             03 MQJEUDIP     PIC X.                                     00003060
             03 MQJEUDIH     PIC X.                                     00003070
             03 MQJEUDIV     PIC X.                                     00003080
             03 MQJEUDIO     PIC X(5).                                  00003090
             03 FILLER       PIC X(2).                                  00003100
             03 MQVENDREDIA  PIC X.                                     00003110
             03 MQVENDREDIC  PIC X.                                     00003120
             03 MQVENDREDIP  PIC X.                                     00003130
             03 MQVENDREDIH  PIC X.                                     00003140
             03 MQVENDREDIV  PIC X.                                     00003150
             03 MQVENDREDIO  PIC X(5).                                  00003160
             03 FILLER       PIC X(2).                                  00003170
             03 MQSAMEDIA    PIC X.                                     00003180
             03 MQSAMEDIC    PIC X.                                     00003190
             03 MQSAMEDIP    PIC X.                                     00003200
             03 MQSAMEDIH    PIC X.                                     00003210
             03 MQSAMEDIV    PIC X.                                     00003220
             03 MQSAMEDIO    PIC X(5).                                  00003230
             03 FILLER       PIC X(2).                                  00003240
             03 MQDIMANCHEA  PIC X.                                     00003250
             03 MQDIMANCHEC  PIC X.                                     00003260
             03 MQDIMANCHEP  PIC X.                                     00003270
             03 MQDIMANCHEH  PIC X.                                     00003280
             03 MQDIMANCHEV  PIC X.                                     00003290
             03 MQDIMANCHEO  PIC X(5).                                  00003300
             03 FILLER       PIC X(2).                                  00003310
             03 MCSEMAINEA   PIC X.                                     00003320
             03 MCSEMAINEC   PIC X.                                     00003330
             03 MCSEMAINEP   PIC X.                                     00003340
             03 MCSEMAINEH   PIC X.                                     00003350
             03 MCSEMAINEV   PIC X.                                     00003360
             03 MCSEMAINEO   PIC X(6).                                  00003370
             03 FILLER       PIC X(2).                                  00003380
             03 MCMOISA      PIC X.                                     00003390
             03 MCMOISC PIC X.                                          00003400
             03 MCMOISP PIC X.                                          00003410
             03 MCMOISH PIC X.                                          00003420
             03 MCMOISV PIC X.                                          00003430
             03 MCMOISO      PIC X(7).                                  00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCMDA  PIC X.                                          00003460
           02 MZONCMDC  PIC X.                                          00003470
           02 MZONCMDP  PIC X.                                          00003480
           02 MZONCMDH  PIC X.                                          00003490
           02 MZONCMDV  PIC X.                                          00003500
           02 MZONCMDO  PIC X(15).                                      00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MLIBERRA  PIC X.                                          00003530
           02 MLIBERRC  PIC X.                                          00003540
           02 MLIBERRP  PIC X.                                          00003550
           02 MLIBERRH  PIC X.                                          00003560
           02 MLIBERRV  PIC X.                                          00003570
           02 MLIBERRO  PIC X(58).                                      00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MCODTRAA  PIC X.                                          00003600
           02 MCODTRAC  PIC X.                                          00003610
           02 MCODTRAP  PIC X.                                          00003620
           02 MCODTRAH  PIC X.                                          00003630
           02 MCODTRAV  PIC X.                                          00003640
           02 MCODTRAO  PIC X(4).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MCICSA    PIC X.                                          00003670
           02 MCICSC    PIC X.                                          00003680
           02 MCICSP    PIC X.                                          00003690
           02 MCICSH    PIC X.                                          00003700
           02 MCICSV    PIC X.                                          00003710
           02 MCICSO    PIC X(5).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MNETNAMA  PIC X.                                          00003740
           02 MNETNAMC  PIC X.                                          00003750
           02 MNETNAMP  PIC X.                                          00003760
           02 MNETNAMH  PIC X.                                          00003770
           02 MNETNAMV  PIC X.                                          00003780
           02 MNETNAMO  PIC X(8).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MSCREENA  PIC X.                                          00003810
           02 MSCREENC  PIC X.                                          00003820
           02 MSCREENP  PIC X.                                          00003830
           02 MSCREENH  PIC X.                                          00003840
           02 MSCREENV  PIC X.                                          00003850
           02 MSCREENO  PIC X(4).                                       00003860
                                                                                
