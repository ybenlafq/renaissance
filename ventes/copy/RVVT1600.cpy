      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT1600                                     00020003
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1600                 00060003
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT1600.                                                    00090003
           02  VT16-NFLAG                                               00100003
               PIC X(0001).                                             00110003
           02  VT16-NCLUSTER                                            00120003
               PIC X(0013).                                             00130003
           02  VT16-NSOCIETE                                            00140003
               PIC X(0003).                                             00150003
           02  VT16-NLIGNE                                              00160003
               PIC X(0003).                                             00170001
           02  VT16-NLCODIC                                             00180003
               PIC X(0003).                                             00190001
           02  VT16-DDELIV                                              00200003
               PIC X(0008).                                             00210001
           02  VT16-CENREG                                              00220003
               PIC X(0005).                                             00230001
           02  VT16-PVTOTAL                                             00240003
               PIC S9(7)V9(0002) COMP-3.                                00250001
           02  VT16-TAUXTVA                                             00260003
               PIC S9(3)V9(0002) COMP-3.                                00270001
           02  VT16-NPSE                                                00280003
               PIC X(0008).                                             00290001
           02  VT16-CANNULREP                                           00300003
               PIC X(0001).                                             00310001
           02  VT16-CVENDEUR                                            00320003
               PIC X(0007).                                             00330001
           02  VT16-NAUTORM                                             00331004
               PIC X(0005).                                             00332004
           02  VT16-NSEQNQ                                              00333004
               PIC S9(005)  COMP-3.                                     00334004
           02  VT16-NSEQREF                                             00335004
               PIC S9(005)  COMP-3.                                     00336004
           02  VT16-CTYPENT                                             00337005
               PIC X(002).                                              00338005
           02  VT16-NLIEN                                               00339004
               PIC S9(005)  COMP-3.                                     00339104
           02  VT16-NACTVTE                                             00339204
               PIC S9(005)  COMP-3.                                     00339304
           02  VT16-NSEQENS                                             00339404
               PIC S9(005)  COMP-3.                                     00339504
           02  VT16-MPRIMECLI                                           00339604
               PIC S9(7)V9(0002) COMP-3.                                00339704
      *                                                                 00340001
      *---------------------------------------------------------        00350001
      *   LISTE DES FLAGS DE LA TABLE RVVT1600                          00360003
      *---------------------------------------------------------        00370001
      *                                                                 00380001
       01  RVVT1600-FLAGS.                                              00390003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NFLAG-F                                             00400003
      *        PIC S9(4) COMP.                                          00410001
      *--                                                                       
           02  VT16-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NCLUSTER-F                                          00420003
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  VT16-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NSOCIETE-F                                          00440003
      *        PIC S9(4) COMP.                                          00450003
      *--                                                                       
           02  VT16-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NLIGNE-F                                            00460003
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT16-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NLCODIC-F                                           00480003
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT16-NLCODIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-DDELIV-F                                            00500003
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT16-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-CENREG-F                                            00520003
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT16-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-PVTOTAL-F                                           00540003
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT16-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-TAUXTVA-F                                           00560003
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT16-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NPSE-F                                              00580003
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT16-NPSE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-CANNULREP-F                                         00600003
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT16-CANNULREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-CVENDEUR-F                                          00620003
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT16-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NAUTORM-F                                           00631004
      *        PIC S9(4) COMP.                                          00632004
      *--                                                                       
           02  VT16-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NSEQNQ-F                                            00633004
      *        PIC S9(4) COMP.                                          00634004
      *--                                                                       
           02  VT16-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NSEQREF-F                                           00635004
      *        PIC S9(4) COMP.                                          00636004
      *--                                                                       
           02  VT16-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-CTYPENT-F                                           00637005
      *        PIC S9(4) COMP.                                          00638004
      *--                                                                       
           02  VT16-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NLIEN-F                                             00639004
      *        PIC S9(4) COMP.                                          00639104
      *--                                                                       
           02  VT16-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NACTVTE-F                                           00639204
      *        PIC S9(4) COMP.                                          00639304
      *--                                                                       
           02  VT16-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-NSEQENS-F                                           00639404
      *        PIC S9(4) COMP.                                          00639504
      *--                                                                       
           02  VT16-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT16-MPRIMECLI-F                                         00639604
      *        PIC S9(4) COMP.                                          00639704
      *--                                                                       
           02  VT16-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640001
                                                                                
