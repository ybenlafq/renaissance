      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:10 >
      
      *****************************************************************
      *   COPY ENTETE DES MESSAGES MQ RECUPERES PAR LE HOST
      *   UTILISE PAR MESSL12 (MSL12) - MESHE01 (MHE01)
      *
      *-----------------------------------------------------------------
      * 28/12/01 ! FC ! AJOUT MARQUEUR VERSION POUR ASSURER COMPATIBILIT
      *-----------------------------------------------------------------
      *
       01  WS-MQ10.
         02 WS-QUEUE.
               10   MQ10-CORRELID    PIC  X(24).
         02 WS-CODRET                PIC  XX.
         02 MESSL.
      * ENTETE STANDARD                                       LG=105C
           10  MESSL-ENTETE.
               20   MESSL-TYPE       PIC  X(03).
               20   MESSL-NSOCMSG    PIC  X(03).
               20   MESSL-NLIEUMSG   PIC  X(03).
               20   MESSL-NSOCDST    PIC  X(03).
               20   MESSL-NLIEUDST   PIC  X(03).
               20   MESSL-NORD       PIC  9(08).
               20   MESSL-LPROG      PIC  X(10).
               20   MESSL-DJOUR      PIC  X(08).
               20   MESSL-WSID       PIC  X(10).
               20   MESSL-USER       PIC  X(10).
               20   MESSL-CHRONO     PIC  9(07).
               20   MESSL-NBRMSG     PIC  9(07).
               20   MESSL-NBRENR     PIC  9(05).
               20   MESSL-TAILLE     PIC  9(05).
               20   MESSL-VERSION    PIC  X(02).
               20   MESSL-FILLER     PIC  X(18).
      
