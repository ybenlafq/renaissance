      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHV510 AU 24/01/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,02,BI,A,                          *        
      *                           17,03,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHV510.                                                        
            05 NOMETAT-IHV510           PIC X(6) VALUE 'IHV510'.                
            05 RUPTURES-IHV510.                                                 
           10 IHV510-NSOCIETE           PIC X(03).                      007  003
           10 IHV510-CRAYON             PIC X(05).                      010  005
           10 IHV510-CGRPMAG            PIC X(02).                      015  002
           10 IHV510-NMAG               PIC X(03).                      017  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHV510-SEQUENCE           PIC S9(04) COMP.                020  002
      *--                                                                       
           10 IHV510-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHV510.                                                   
           10 IHV510-PCA                PIC S9(11)V9(2) COMP-3.         022  007
           10 IHV510-PCA-PM             PIC S9(11)V9(2) COMP-3.         029  007
           10 IHV510-PMARGE             PIC S9(11)V9(2) COMP-3.         036  007
           10 IHV510-PMARGE-PM          PIC S9(11)V9(2) COMP-3.         043  007
           10 IHV510-QPIECES            PIC S9(09)      COMP-3.         050  005
           10 IHV510-QPIECES-PM         PIC S9(09)      COMP-3.         055  005
            05 FILLER                      PIC X(453).                          
                                                                                
