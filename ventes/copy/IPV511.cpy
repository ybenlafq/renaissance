      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV511 AU 03/12/2004  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV511.                                                        
            05 NOMETAT-IPV511           PIC X(6) VALUE 'IPV511'.                
            05 RUPTURES-IPV511.                                                 
           10 IPV511-NSOCIETE           PIC X(03).                      007  003
           10 IPV511-NLIEU              PIC X(03).                      010  003
           10 IPV511-WSEQED             PIC S9(05)      COMP-3.         013  003
           10 IPV511-NAGREGATED         PIC X(02).                      016  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV511-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IPV511-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV511.                                                   
           10 IPV511-RUBRIQUE           PIC X(20).                      020  020
           10 IPV511-COMM               PIC S9(07)V9(2) COMP-3.         040  005
           10 IPV511-MBU                PIC S9(07)V9(2) COMP-3.         045  005
           10 IPV511-MBUGRP             PIC S9(07)V9(2) COMP-3.         050  005
           10 IPV511-PCA                PIC S9(07)V9(2) COMP-3.         055  005
           10 IPV511-PCAGRP             PIC S9(07)V9(2) COMP-3.         060  005
           10 IPV511-PMT                PIC S9(07)V9(2) COMP-3.         065  005
           10 IPV511-PMTADD             PIC S9(07)V9(2) COMP-3.         070  005
           10 IPV511-PMTEP              PIC S9(07)V9(2) COMP-3.         075  005
           10 IPV511-PMTPSE             PIC S9(07)V9(2) COMP-3.         080  005
           10 IPV511-PMTTOT             PIC S9(07)V9(2) COMP-3.         085  005
           10 IPV511-PMTVOL             PIC S9(07)V9(2) COMP-3.         090  005
           10 IPV511-QVENDUE            PIC S9(05)      COMP-3.         095  003
           10 IPV511-QVENDUEGRP         PIC S9(05)      COMP-3.         098  003
           10 IPV511-DEBPERIODE         PIC X(08).                      101  008
           10 IPV511-FINPERIODE         PIC X(08).                      109  008
            05 FILLER                      PIC X(396).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV511-LONG           PIC S9(4)   COMP  VALUE +116.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV511-LONG           PIC S9(4) COMP-5  VALUE +116.           
                                                                                
      *}                                                                        
