      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL4001                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL4001                         
      *   LIEUX D'INVENTAIRE MAGASIN                                            
      *---------------------------------------------------------                
      *                                                                         
       01 RVSL4001.                                                             
      *           CODE SOCIETRE                                                 
          02 SL40-NSOCIETE         PIC X(3).                                    
      *           CODE LIEU INVENTORIE                                          
          02 SL40-CLIEUINV         PIC X(5).                                    
      *           DESCRIPTION LIEU INVENTORI�                                   
          02 SL40-LLIEUINV         PIC X(20).                                   
      *           N� S�QUENCE �DITION                                           
          02 SL40-NSEQEDT          PIC S9(3)       COMP-3.                      
      *           SOUS LIEU STOCK MAGASIN NMD                                   
          02 SL40-CEMPL            PIC X(5).                                    
      *           EXPO MAGASIN  (O/N)                                           
          02 SL40-WEXPO            PIC X(1).                                    
      *           AUTORIS� GESTION DE LOTS (O/N)                                
          02 SL40-WGLOTS           PIC X(1).                                    
      *           TIMESTAMP                                                     
          02 SL40-DSYST            PIC S9(13)      COMP-3.                      
      *           DERNIER N� DE REGLE D'INVENTAIRE.                             
          02 SL40-NREGL            PIC S9(03)      COMP-3.                      
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVSL4001                                    
      *---------------------------------------------------------                
      *                                                                         
       01 RVSL4001-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-NSOCIETE-F   PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-NSOCIETE-F   PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-CLIEUINV-F   PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-CLIEUINV-F   PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-LLIEUINV-F   PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-LLIEUINV-F   PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-NSEQEDT-F    PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-NSEQEDT-F    PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-CEMPL-F      PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-CEMPL-F      PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-WEXPO-F      PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-WEXPO-F      PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-WGLOTS-F     PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-WGLOTS-F     PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-DSYST-F      PIC   S9(4) COMP.                               
      *--                                                                       
          02  SL40-DSYST-F      PIC   S9(4) COMP-5.                             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02  SL40-NREGL-F      PIC   S9(4) COMP.                               
      *                                                                         
      *--                                                                       
          02  SL40-NREGL-F      PIC   S9(4) COMP-5.                             
                                                                                
      *}                                                                        
