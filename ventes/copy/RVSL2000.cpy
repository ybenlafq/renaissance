      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVSL2000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL2000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVSL2000.                                                            
           02  SL20-NSOCIETE          PIC X(0003).                              
           02  SL20-NLIEU             PIC X(0003).                              
           02  SL20-NVENTE            PIC X(0007).                              
           02  SL20-STATUT            PIC X(0002).                              
           02  SL20-INCOMPLETE        PIC X(0001).                              
           02  SL20-NBLIGNENS         PIC S9(3) COMP-3.                         
           02  SL20-DUPLIQUE          PIC X(0001).                              
           02  SL20-DATERESERVATION   PIC X(0008).                              
           02  SL20-PGMMAJ            PIC X(0008).                              
           02  SL20-DSYST             PIC S9(13) COMP-3.                        
       01  RVSL2000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-NSOCIETE-F        PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-NSOCIETE-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-NLIEU-F           PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-NLIEU-F           PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-NVENTE-F          PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-NVENTE-F          PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-STATUT-F          PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-STATUT-F          PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-INCOMPLETE-F      PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-INCOMPLETE-F      PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-NBLIGNENS-F       PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-NBLIGNENS-F       PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-DUPLIQUE-F        PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-DUPLIQUE-F        PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-DATERESERVATION-F PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-DATERESERVATION-F PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-PGMMAJ-F          PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-PGMMAJ-F          PIC S9(4) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL20-DSYST-F           PIC S9(4) COMP.                           
      *--                                                                       
           02  SL20-DSYST-F           PIC S9(4) COMP-5.                         
      *}                                                                        
       EJECT                                                                    
                                                                                
