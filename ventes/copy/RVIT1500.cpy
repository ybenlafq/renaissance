      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVIT1500                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIT1500                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT1500.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT1500.                                                            
      *}                                                                        
           02  IT15-NINVENTAIRE                                         00000100
               PIC X(0005).                                             00000110
           02  IT15-NLIEU                                               00000120
               PIC X(0003).                                             00000130
           02  IT15-NSOCIETE                                            00000140
               PIC X(0003).                                             00000150
           02  IT15-NSSLIEU                                             00000160
               PIC X(0003).                                             00000170
           02  IT15-WFLAG                                               00000180
               PIC X(0001).                                             00000190
           02  IT15-NCODIC                                              00000200
               PIC X(0007).                                             00000210
           02  IT15-QSTOCKTHEO                                          00000220
               PIC S9(5) COMP-3.                                        00000230
           02  IT15-QSTOCKINVT                                          00000240
               PIC S9(5) COMP-3.                                        00000250
           02  IT15-DSYST                                               00000260
               PIC S9(13) COMP-3.                                       00000270
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000280
      *---------------------------------------------------------        00000290
      *   LISTE DES FLAGS DE LA TABLE RVIT1500                          00000300
      *---------------------------------------------------------        00000310
      *                                                                 00000320
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT1500-FLAGS.                                              00000330
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT1500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-NINVENTAIRE-F                                       00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  IT15-NINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-NLIEU-F                                             00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  IT15-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-NSOCIETE-F                                          00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  IT15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-NSSLIEU-F                                           00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  IT15-NSSLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-WFLAG-F                                             00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  IT15-WFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-NCODIC-F                                            00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  IT15-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-QSTOCKTHEO-F                                        00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  IT15-QSTOCKTHEO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-QSTOCKINVT-F                                        00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  IT15-QSTOCKINVT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT15-DSYST-F                                             00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  IT15-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000520
                                                                                
