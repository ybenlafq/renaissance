      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV02   EAV02                                              00000020
      ***************************************************************** 00000030
       01   EAV02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALSELL   COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MCPOSTALSELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCPOSTALSELF   PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCPOSTALSELI   PIC X(5).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMUNESELL   COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MLCOMUNESELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLCOMUNESELF   PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MLCOMUNESELI   PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTITREL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MLTITREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLTITREF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLTITREI  PIC X(20).                                      00000290
           02 MLIGNEI OCCURS   15 TIMES .                               00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPOSTALL    COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCPOSTALL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCPOSTALF    PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCPOSTALI    PIC X(5).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLBUREAUL    COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLBUREAUL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLBUREAUF    PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLBUREAUI    PIC X(26).                                 00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCOMUNEL    COMP PIC S9(4).                            00000390
      *--                                                                       
             03 MLCOMUNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLCOMUNEF    PIC X.                                     00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MLCOMUNEI    PIC X(32).                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELECTL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MWSELECTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSELECTF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MWSELECTI    PIC X.                                     00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MZONCMDI  PIC X(15).                                      00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MLIBERRI  PIC X(58).                                      00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MCODTRAI  PIC X(4).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCICSI    PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MNETNAMI  PIC X(8).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000670
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000680
           02 FILLER    PIC X(4).                                       00000690
           02 MSCREENI  PIC X(4).                                       00000700
      ***************************************************************** 00000710
      * SDF: EAV02   EAV02                                              00000720
      ***************************************************************** 00000730
       01   EAV02O REDEFINES EAV02I.                                    00000740
           02 FILLER    PIC X(12).                                      00000750
           02 FILLER    PIC X(2).                                       00000760
           02 MDATJOUA  PIC X.                                          00000770
           02 MDATJOUC  PIC X.                                          00000780
           02 MDATJOUP  PIC X.                                          00000790
           02 MDATJOUH  PIC X.                                          00000800
           02 MDATJOUV  PIC X.                                          00000810
           02 MDATJOUO  PIC X(10).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MTIMJOUA  PIC X.                                          00000840
           02 MTIMJOUC  PIC X.                                          00000850
           02 MTIMJOUP  PIC X.                                          00000860
           02 MTIMJOUH  PIC X.                                          00000870
           02 MTIMJOUV  PIC X.                                          00000880
           02 MTIMJOUO  PIC X(5).                                       00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MWPAGEA   PIC X.                                          00000910
           02 MWPAGEC   PIC X.                                          00000920
           02 MWPAGEP   PIC X.                                          00000930
           02 MWPAGEH   PIC X.                                          00000940
           02 MWPAGEV   PIC X.                                          00000950
           02 MWPAGEO   PIC X(3).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MCPOSTALSELA   PIC X.                                     00000980
           02 MCPOSTALSELC   PIC X.                                     00000990
           02 MCPOSTALSELP   PIC X.                                     00001000
           02 MCPOSTALSELH   PIC X.                                     00001010
           02 MCPOSTALSELV   PIC X.                                     00001020
           02 MCPOSTALSELO   PIC X(5).                                  00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MLCOMUNESELA   PIC X.                                     00001050
           02 MLCOMUNESELC   PIC X.                                     00001060
           02 MLCOMUNESELP   PIC X.                                     00001070
           02 MLCOMUNESELH   PIC X.                                     00001080
           02 MLCOMUNESELV   PIC X.                                     00001090
           02 MLCOMUNESELO   PIC X(5).                                  00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MLTITREA  PIC X.                                          00001120
           02 MLTITREC  PIC X.                                          00001130
           02 MLTITREP  PIC X.                                          00001140
           02 MLTITREH  PIC X.                                          00001150
           02 MLTITREV  PIC X.                                          00001160
           02 MLTITREO  PIC X(20).                                      00001170
           02 MLIGNEO OCCURS   15 TIMES .                               00001180
             03 FILLER       PIC X(2).                                  00001190
             03 MCPOSTALA    PIC X.                                     00001200
             03 MCPOSTALC    PIC X.                                     00001210
             03 MCPOSTALP    PIC X.                                     00001220
             03 MCPOSTALH    PIC X.                                     00001230
             03 MCPOSTALV    PIC X.                                     00001240
             03 MCPOSTALO    PIC X(5).                                  00001250
             03 FILLER       PIC X(2).                                  00001260
             03 MLBUREAUA    PIC X.                                     00001270
             03 MLBUREAUC    PIC X.                                     00001280
             03 MLBUREAUP    PIC X.                                     00001290
             03 MLBUREAUH    PIC X.                                     00001300
             03 MLBUREAUV    PIC X.                                     00001310
             03 MLBUREAUO    PIC X(26).                                 00001320
             03 FILLER       PIC X(2).                                  00001330
             03 MLCOMUNEA    PIC X.                                     00001340
             03 MLCOMUNEC    PIC X.                                     00001350
             03 MLCOMUNEP    PIC X.                                     00001360
             03 MLCOMUNEH    PIC X.                                     00001370
             03 MLCOMUNEV    PIC X.                                     00001380
             03 MLCOMUNEO    PIC X(32).                                 00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MWSELECTA    PIC X.                                     00001410
             03 MWSELECTC    PIC X.                                     00001420
             03 MWSELECTP    PIC X.                                     00001430
             03 MWSELECTH    PIC X.                                     00001440
             03 MWSELECTV    PIC X.                                     00001450
             03 MWSELECTO    PIC X.                                     00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MZONCMDA  PIC X.                                          00001480
           02 MZONCMDC  PIC X.                                          00001490
           02 MZONCMDP  PIC X.                                          00001500
           02 MZONCMDH  PIC X.                                          00001510
           02 MZONCMDV  PIC X.                                          00001520
           02 MZONCMDO  PIC X(15).                                      00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MLIBERRA  PIC X.                                          00001550
           02 MLIBERRC  PIC X.                                          00001560
           02 MLIBERRP  PIC X.                                          00001570
           02 MLIBERRH  PIC X.                                          00001580
           02 MLIBERRV  PIC X.                                          00001590
           02 MLIBERRO  PIC X(58).                                      00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MCODTRAA  PIC X.                                          00001620
           02 MCODTRAC  PIC X.                                          00001630
           02 MCODTRAP  PIC X.                                          00001640
           02 MCODTRAH  PIC X.                                          00001650
           02 MCODTRAV  PIC X.                                          00001660
           02 MCODTRAO  PIC X(4).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MCICSA    PIC X.                                          00001690
           02 MCICSC    PIC X.                                          00001700
           02 MCICSP    PIC X.                                          00001710
           02 MCICSH    PIC X.                                          00001720
           02 MCICSV    PIC X.                                          00001730
           02 MCICSO    PIC X(5).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MNETNAMA  PIC X.                                          00001760
           02 MNETNAMC  PIC X.                                          00001770
           02 MNETNAMP  PIC X.                                          00001780
           02 MNETNAMH  PIC X.                                          00001790
           02 MNETNAMV  PIC X.                                          00001800
           02 MNETNAMO  PIC X(8).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MSCREENA  PIC X.                                          00001830
           02 MSCREENC  PIC X.                                          00001840
           02 MSCREENP  PIC X.                                          00001850
           02 MSCREENH  PIC X.                                          00001860
           02 MSCREENV  PIC X.                                          00001870
           02 MSCREENO  PIC X(4).                                       00001880
                                                                                
