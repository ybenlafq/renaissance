      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
          01 COMM-HV53-DONNEES.                                         00000010
             10 COMM-HV53-NB-POSTES               PIC S9(09) COMP-3.    00000070
             10 COMM-HV53-CODRET                  PIC 9(03) COMP-3.     00000070
                88 COMM-HV53-ERREUR-MANIP         VALUE 999.            00000070
             10 COMM-HV53-LIBERR                  PIC X(25).            00000070
             10 COMM-HV53-CRITERES.                                     00000020
                20 COMM-HV53-NSOCIETE             PIC X(03).            00000020
                20 COMM-HV53-NLIEU                PIC X(03).            00000030
                20 COMM-HV53-DVENTE               PIC X(08).            00000020
                20 COMM-HV53-LNOM                 PIC X(25).            00000020
                20 COMM-HV53-CMARQ                PIC X(05).            00000040
                20 COMM-HV53-CFAM                 PIC X(05).            00000030
                20 COMM-HV53-DDEBUT               PIC X(08).            00000020
                20 COMM-HV53-DFIN                 PIC X(08).            00000020
                20 COMM-HV53-NCAISSE              PIC X(03).            00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         20 COMM-HV53-MONTANT              PIC S9(7)V99 COMP.    00000030
      *--                                                                       
                20 COMM-HV53-MONTANT              PIC S9(7)V99 COMP-5.          
      *}                                                                        
                20 COMM-HV53-TPAIE                PIC X(01).            00000040
                20 COMM-HV53-TTRANS               PIC X(01).            00000030
                20 COMM-HV53-NTRANS               PIC X(04).            00000030
                20 COMM-HV53-NCODIC               PIC X(07).            00000030
JD              20 COMM-HV53-CDEVISE              PIC X(01).            00000030
JD              20 COMM-HV53-NVENTE               PIC X(07).            00000030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
JD    *         20 COMM-HV53-MTREGLEM             PIC S9(7)V99 COMP.    00000030
      *--                                                                       
                20 COMM-HV53-MTREGLEM             PIC S9(7)V99 COMP-5.          
      *}                                                                        
             10 COMM-HV53-TYPE-RECHERCHE          PIC X.                00000070
                88 COMM-HV53-PAS-RECH                  VALUE ' '.               
                88 COMM-HV53-RECH-DVENTE               VALUE '1'.               
JD              88 COMM-HV53-RECH-NVENTE               VALUE '2'.               
JD              88 COMM-HV53-RECH-NVENTE-NCAISSE       VALUE '3'.               
                88 COMM-HV53-RECH-NOM-MARQ-FAM         VALUE '4'.               
                88 COMM-HV53-RECH-NOM               VALUE '2' '3' '4'.          
                88 COMM-HV53-RECH-NTRANS               VALUE 'A'.               
                88 COMM-HV53-RECH-TTRANS               VALUE 'B'.               
                88 COMM-HV53-RECH-TTRANS-MONTANT       VALUE 'C'.               
JD              88 COMM-HV53-RECH-NCAISSE              VALUE 'D'.               
                88 COMM-HV53-RECH-NCODIC               VALUE 'E'.               
JD              88 COMM-HV53-RECH-TPAIE-CDEV           VALUE 'F'.               
JD              88 COMM-HV53-RECH-TPAI-CDEV-PREGL      VALUE 'G'.               
                                                                                
