      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV2500                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV2500                         
      **********************************************************                
       01  RVHV2500.                                                            
           02  HV25-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV25-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV25-CRAYON                                                      
               PIC X(0005).                                                     
           02  HV25-CINSEE                                                      
               PIC X(0005).                                                     
           02  HV25-DGRPMOIS                                                    
               PIC X(0008).                                                     
           02  HV25-QMVT                                                        
               PIC S9(9) COMP-3.                                                
           02  HV25-PCA                                                         
               PIC S9(11)V9(0002) COMP-3.                                       
           02  HV25-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV2500                                  
      **********************************************************                
       01  RVHV2500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-CRAYON-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-CRAYON-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-CINSEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-CINSEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-DGRPMOIS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-DGRPMOIS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-QMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV25-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV25-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV25-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
