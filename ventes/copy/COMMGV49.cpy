      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION ISV  APELLEE PAR TGV00 *        
      *                        INTERROGATION STATUT DE VENTE LOCAL     *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MGV49-APPLI.                                            00260000
         02 COMM-MGV49-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MGV49-NSOCP          PIC X(03).                      00320000
           05 COMM-MGV49-NLIEUP         PIC X(03).                      00330000
           05 COMM-MGV49-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MGV49-NLIEU          PIC X(03).                      00330000
           05 COMM-MGV49-NORDRE         PIC X(05).                      00340000
           05 COMM-MGV49-DVENTE         PIC X(08).                      00340000
           05 COMM-MGV49-NVENTE         PIC X(07).                      00340000
         02 COMM-MGV49-SORTIE.                                          00260000
           05 COMM-MGV49-MESSAGE.                                               
                10 COMM-MGV49-CODRET         PIC X.                             
                   88  COMM-MGV49-OK         VALUE ' '.                         
                   88  COMM-MGV49-ERR-BLQ    VALUE '1'.                         
                   88  COMM-MGV49-ERR-NON-BLQ VALUE '2'.                        
                10 COMM-MGV49-LIBERR         PIC X(58).                         
           05 FILLER                    PIC X(09).                      00340000
                                                                                
