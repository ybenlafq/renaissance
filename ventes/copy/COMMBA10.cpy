      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *    COPY  COMMBA10.                                                      
      *                                                                         
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *      COMMAREA DE LA TRANSACTION BA00 BONS D'ACHAT              *        
      *                                                                *        
      *      PARTIES APPLICATIVES COMMUNES AUX TRANSACTION :           *        
      *      BA00 - BA10 - BA20 ET BA50.                               *        
      *                                                                *        
      *      LONGUEUR = 1 - ZONES COMMUNES               372 C         *        
      *                 2 - ZONES APPLICATIVES BA       3724 C         *        
      *                     TOTAL                       4096           *        
      *                                                                *        
      ******************************************************************        
      * MODIFICATION : NOUVEAU BON D'ACHAT                                      
      * MARQUE       : AL00                                                     
      * MODIFICATION : ADAPTATION DU TIERS SUITE A SAP/SIEBEL                   
      * MARQUE       : 0208                                                     
      * MODIFICATION : MODIF DE LA CINEMATIQUE DE LA GESTION BA -               
      *              : AJOUT D'UN INDICATEUR D'ETAT DANS COMM-BA50              
      * MARQUE       : 0408                                                     
      ******************************************************************        
      *    ZONES APPLICATIVES DES TRANSACTION BA00,BA10,BA20---- 199 C *        
      ******************************************************************        
      *                                                                         
           02 COMM-BA00-ZONES   REDEFINES  COMM-BA00-APPLI.                     
      *                                    NOM DU MENU APPELANT                 
              05 COMM-BA00-CMENU           PIC X(05).                           
      *                                    MAGASIN DE RECEPTION                 
              05 COMM-BA00-NSOCIETE        PIC X(03).                           
              05 COMM-BA00-NLIEU           PIC X(03).                           
      *                                    ENTITE                               
              05 COMM-BA00-NENTITE         PIC X(05).                           
      *                                    DATE DE RECEPTION                    
              05 COMM-BA00-DRECEP          PIC X(08).                           
      *                                    DERNIERE DATE EMISSION VALIDE        
              05 COMM-BA00-DVALIDITE       PIC X(08).                           
      *                                    MONTANT MAXIMUM AUTORISE             
      *                                    POUR SIGNATURE AUTOMATIQUE           
              05 COMM-BA00-PMTMAXI         PIC S9(5)V99 COMP-3.                 
      *                                    TABLE DES SIGNATAIRES                
              05 COMM-BA00-BASIG           OCCURS 10.                           
                 10 COMM-BA00-NSIGNAT      PIC X.                               
                 10 COMM-BA00-LSIGNAT      PIC X(15).                           
      *                                    TAUX DE TVA BA                       
              05 COMM-BA00-QTAUXTVA        PIC S9(03)V99 COMP-3.                
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DE LA SAISIE DE COMMANDE - TBA0X - 359 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-BA01-APPLI.                                               
      *                                       TOP FONCTION A EFFECTUER          
                 10 COMM-BA01-FONCTION        PIC X(01).                        
                    88 COMM-BA01-CONTROLER        VALUE SPACE.                  
                    88 COMM-BA01-EDITER-BA        VALUE 'B'.                    
                    88 COMM-BA01-EDITER-FACTURE   VALUE 'F'.                    
      *                                       CODE RETOUR PG EDITION            
                 10 COMM-BA01-CODE-RETOUR.                                      
                    15 COMM-BA01-CODRET       PIC X(01).                        
                    15 COMM-BA01-LIBERR       PIC X(58).                        
      *                                       SIGNATAIRE                        
                 10 COMM-BA01-NSIGNAT         PIC X(01).                        
                 10 COMM-BA01-LSIGNAT         PIC X(15).                        
      *                                       DATE EMISSION                     
                 10 COMM-BA01-DEMIS.                                            
                    15 COMM-BA01-DEMIS-JJ     PIC X(02).                        
                    15 COMM-BA01-DEMIS-MM     PIC X(02).                        
                    15 COMM-BA01-DEMIS-SS     PIC X(02).                        
                    15 COMM-BA01-DEMIS-AA     PIC X(02).                        
                    15 COMM-BA01-DEMIS-LM     PIC X(09).                        
      *                                       DATE VALIDATION                   
                 10 COMM-BA01-DVALID.                                           
                    15 COMM-BA01-DVALID-JJ    PIC X(02).                        
                    15 COMM-BA01-DVALID-MM    PIC X(02).                        
                    15 COMM-BA01-DVALID-SS    PIC X(02).                        
                    15 COMM-BA01-DVALID-AA    PIC X(02).                        
                    15 COMM-BA01-DVALID-LM    PIC X(09).                        
      *                                       DONNEES FACTURE/AVOIR             
                 10 COMM-BA01-LFACTURE.                                         
                    15 COMM-BA01-NFACTBA      PIC X(10).                        
                    15 COMM-BA01-NFACTREM     PIC X(10).                        
                    15 COMM-BA01-WANNUL       PIC X(01).                        
                    15 COMM-BA01-WELIBELLE    PIC X(01).                        
                    15 COMM-BA01-WEFACTURE    PIC X(01).                        
                    15 COMM-BA01-PREMISE      PIC 9(07)V99 COMP-3.              
                    15 COMM-BA01-PFACTURE     PIC 9(07)V99 COMP-3.              
                    15 COMM-BA01-QTAUXREM     PIC 9(03)V99 COMP-3.              
                    15 COMM-BA01-WVENTEMINI   PIC X.                            
                    15 COMM-BA01-PVENTEMINI   PIC 9(07)V99 COMP-3.              
                    15 COMM-BA01-WEMONTANT    PIC X(0001).                      
                    15 COMM-BA01-WEMISEURO    PIC X(0001).                      
                    15 COMM-BA01-CDEVISE      PIC X(0003).                      
                    15 COMM-BA01-CDEVISA      PIC X(0003).                      
                    15 COMM-BA01-PREMISE2     PIC 9(07)V99 COMP-3.              
                    15 COMM-BA01-PFACTURE2    PIC 9(07)V99 COMP-3.              
                    15 COMM-BA01-PTAUX        PIC 9(05)V9(5) COMP-3.            
      *                                       ADRESSE DU TIERS                  
                 10 COMM-BA01-LADRESSE.                                         
0208  *             15 COMM-BA01-NTIERSCV     PIC X(06).                        
--                  15 COMM-BA01-NTIERSSAP    PIC X(10).                        
--    *             15 COMM-BA01-CTITRENOM    PIC X(05).                        
--    *             15 COMM-BA01-LNOM         PIC X(25).                        
--    *             15 COMM-BA01-CVOIE        PIC X(05).                        
--    *             15 COMM-BA01-CTVOIE       PIC X(04).                        
--    *             15 COMM-BA01-LNOMVOIE     PIC X(21).                        
--    *             15 COMM-BA01-LCOMMUNE     PIC X(32).                        
--    *             15 COMM-BA01-CPOSTAL      PIC X(05).                        
--    *             15 COMM-BA01-LBUREAU      PIC X(25).                        
--                  15 COMM-BA01-LRSOCIALE    PIC X(32).                        
--                  15 COMM-BA01-LADRESSE1    PIC X(32).                        
--                  15 COMM-BA01-LADRESSE2    PIC X(32).                        
--                  15 COMM-BA01-CPOSTAL      PIC X(08).                        
0208                15 COMM-BA01-LVILLE       PIC X(32).                        
      *                                       LISTE TIERS TBA02                 
                 10 COMM-BA02-LTIERS.                                           
                    15 COMM-BA02-CTIERSBA     PIC X(06).                        
                    15 COMM-BA02-NPAGE        PIC 9(02).                        
                    15 COMM-BA02-NPGMAX       PIC 9(02).                        
                    15 COMM-BA02-NLGMAX       PIC 9(05).                        
      *                                       ETAT CTL BA EDITES TBA07          
      *                                      (DONNEES DE LA VEILLE)             
                 10 COMM-BA07-LVEILLE.                                          
                    15 COMM-BA07-DEMIS        PIC X(08).                        
                    15 COMM-BA07-NCHRONO-V    PIC 9(07).                        
                    15 COMM-BA07-NCHRONO-J    PIC 9(07).                        
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DE LA RECEPTION DES BA   - TBA1X -- 39 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-BA10-APPLI.                                               
      *                                       PAGINATION RECEPTION BA           
                 10 COMM-BA11-GESTION-TS.                                       
                    15 COMM-BA11-NPAGE        PIC 9(02).                        
                    15 COMM-BA11-NPGMAX       PIC 9(02).                        
                    15 COMM-BA11-NLGMAX       PIC 9(05).                        
      *                                       ECART = BA RECEPT(RTBA01)         
      *                                             - BA ENCAIS(RTBA10)         
                 10 COMM-BA11-PSUMBA01        PIC S9(07)V99 COMP-3.             
                 10 COMM-BA11-PSUMBA10        PIC S9(07)V99 COMP-3.             
      *                                       PAGINATION SAISIE ECART           
                 10 COMM-BA12-GESTION-TS.                                       
                    15 COMM-BA12-NPAGE        PIC 9(02).                        
                    15 COMM-BA12-NPGMAX       PIC 9(02).                        
                    15 COMM-BA12-NLGMAX       PIC 9(05).                        
      *                                       N� ECART REPRISE ANCIEN BA        
                 10 COMM-BA12-NECART          PIC X(02).                        
      *                                     PAGINATION REPRISE ANCIEN BA        
                 10 COMM-BA13-GESTION-TS.                                       
                    15 COMM-BA13-NPAGE        PIC 9(02).                        
                    15 COMM-BA13-NPGMAX       PIC 9(02).                        
                    15 COMM-BA13-NLGMAX       PIC 9(05).                        
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DE L'ANNULATION DE BA    - TBA3X -- 19 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-BA30-APPLI.                                               
      *                                       TOP FONCTION A EFFECTUER          
                 10 COMM-BA30-FONCTION        PIC X(01).                        
                    88 COMM-BA30-CONTROLER    VALUE SPACE.                      
                    88 COMM-BA30-VALIDER      VALUE 'V'.                        
      *                                       NUMERO DU BON D'ACHAT             
                 10 COMM-BA30-NBA             PIC X(06).                        
      *                                       N� BA DEBUT,FIN SEQUENCE          
                 10 COMM-BA30-NBADEB          PIC X(06).                        
                 10 COMM-BA30-NBAFIN          PIC X(06).                        
      *                                                                         
      ******************************************************************        
      *--- ZONES APPLICATIVES DES DONNEES BONS D'ACHAT - TBA5X - 163 C *        
      ******************************************************************        
      *                                                                         
              05 COMM-BA50-APPLI.                                               
                 10 COMM-BA50-CFONCT          PIC X(03).                00008601
                 10 COMM-BA50-NSOCGL          PIC X(05).                        
                 10 COMM-BA50-NETAB           PIC X(03).                00008101
                 10 COMM-BA50-NAUX            PIC X(03).                00010001
0208  *          10 COMM-BA50-NTIERSCV        PIC X(06).                00010001
--               10 COMM-BA50-NTIERSSAP       PIC X(10).                00010001
--    *          10 COMM-BA50-CTITRENOM       PIC X(05).                00009101
--    *          10 COMM-BA50-LNOM            PIC X(25).                00009101
--    *          10 COMM-BA50-LPRENOM         PIC X(15).                00009101
0208             10 COMM-BA50-LRSOCIALE       PIC X(32).                00009101
                 10 COMM-BA50-LAUX            PIC X(40).                00010201
                 10 COMM-BA50-CTIERSBA        PIC X(06).                00010001
                 10 COMM-BA50-ATTR-CTIERSBA   PIC X(01).                        
                    88 COMM-BA50-CTIERSBA-DEPROT  VALUE 'O'.                    
                    88 COMM-BA50-CTIERSBA-PROT    VALUE 'N'.                    
                 10 COMM-BA50-CODE-RETOUR.                                      
                    15 COMM-BA50-CODRET       PIC X(01).                        
                    15 COMM-BA50-LIBERR       PIC X(58).                        
0408             10 COMM-BA50-INDETAT         PIC 9(01).                00010001
      *                                                                         
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 2946 C*         
      ******************************************************************        
      *                                                                         
AL00          05 COMM-BA01-NOMETAT         PIC X(06).                           
              05 COMM-BA00-TEXTE-OPTION    PIC X(0050).                         
0208  *       05 COMM-BA00-FILLER          PIC X(2899).                         
0208          05 COMM-BA00-FILLER          PIC X(2889).                         
      *                                                                         
                                                                                
