      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      * TSGVMG = TS PAR MAGASIN, CLE = 'GV' + SOCIETE + LIEU          *         
      *                                                               *         
      * DESCRIPTION DE LA TS CONTENANT LES INFORMATIONS DES           *         
      * TABLES OU SOUS-TABLES DU MAGASIN DE LA VENTE                  *         
      * TABLES OU SOUS-TABLES RENSEIGNES :                            *         
      *   1- RTGA10 : TYPES DE LIEU                                   *         
      *   2- SRP34  : MAGASINS DEROGATION SRP                         *         
      *   3- LOGISTIQUE GROUPE : SUPPRESSION ENAFA                    *         
      * LONGUEUR TOTALE = 500                                         *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      * NOMBRE DE POSTES MAXIMUM POUR CHAQUE TABLE OU SOUS-TABLE      *         
      *(A METTRE A JOUR A CHAQUE CHANGEMENT DU NOMBRE D' OCCURS)      *         
      *                                                               *         
       01  IND-MAGASIN                     PIC 999  VALUE ZEROES.               
       01  TS-GVMG-DONNEES.                                                     
      *                                                         +   8 C         
           05 TS-GVMG-DCREATION            PIC X(08).                           
      *                                                         +   7 C         
           05 TS-GVMG-GA10.                                                     
              10 TS-GVMG-GA10-CTYPLIEU     PIC X(01).                           
              10 TS-GVMG-GA10-NZONPRIX     PIC X(02).                           
              10 TS-GVMG-GA10-CACID        PIC X(04).                           
      *                                                         +   2 C         
           05 TS-GVMG-SRP34.                                            00008802
              10 TS-GVMG-SRP34-WACTIF      PIC X(01).                           
                 88 TS-GVMG-SRP34-ACTIF                    VALUE 'O'.   00008802
              10 TS-GVMG-SRP34-WDEROG      PIC X(01).                           
                 88 TS-GVMG-SRP34-DEROGATION               VALUE '1'.   00008802
      *                                                         + 120 C         
           05 TS-GVMG-FILLER               PIC X(120).                          
      *                                                         +   2 C         
           05 TS-GVMG-GA10-SUITE.                                               
              10 TS-GVMG-GA10-NZPPREST     PIC X(02).                           
      *                                                         + 361 C         
           05 FILLER                       PIC X(361)      VALUE SPACES.        
                                                                                
