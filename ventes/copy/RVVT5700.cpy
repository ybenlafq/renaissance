      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT5700                                     00020005
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT5700                 00060005
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT5700.                                                    00090005
           02  VT57-NFLAG                                               00100006
               PIC X(0001).                                             00110006
           02  VT57-NVENTE                                              00120005
               PIC X(0007).                                             00130001
           02  VT57-NLIEU                                               00140005
               PIC X(0003).                                             00150004
           02  VT57-NSOCIETE                                            00160005
               PIC X(0003).                                             00170001
           02  VT57-NDOC                                                00180005
               PIC X(0002).                                             00190004
           02  VT57-NSERVICE                                            00200005
               PIC X(0004).                                             00210004
           02  VT57-NBOX                                                00220005
               PIC X(0005).                                             00230004
           02  VT57-NBOITE                                              00240005
               PIC X(0006).                                             00250004
           02  VT57-NFICHE                                              00260005
               PIC X(0003).                                             00270004
           02  VT57-NSEQ                                                00280005
               PIC X(0002).                                             00290005
      *                                                                 00300001
      *---------------------------------------------------------        00310001
      *   LISTE DES FLAGS DE LA TABLE RVVT5700                          00320005
      *---------------------------------------------------------        00330001
      *                                                                 00340001
       01  RVVT5700-FLAGS.                                              00350005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NFLAG-F                                             00360006
      *        PIC S9(4) COMP.                                          00370006
      *--                                                                       
           02  VT57-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NVENTE-F                                            00380005
      *        PIC S9(4) COMP.                                          00390001
      *--                                                                       
           02  VT57-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NLIEU-F                                             00400005
      *        PIC S9(4) COMP.                                          00410001
      *--                                                                       
           02  VT57-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NSOCIETE-F                                          00420005
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  VT57-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NDOC-F                                              00440005
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT57-NDOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NSERVICE-F                                          00460005
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT57-NSERVICE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NBOX-F                                              00480005
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT57-NBOX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NBOITE-F                                            00500005
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT57-NBOITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NFICHE-F                                            00520005
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT57-NFICHE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT57-NSEQ-F                                              00540005
      *        PIC S9(4) COMP.                                          00550005
      *--                                                                       
           02  VT57-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00560001
                                                                                
