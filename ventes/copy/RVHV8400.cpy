      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV8400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV8400                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV8400.                                                            
           02  HV84-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV84-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV84-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV84-DSVENTECIALE                                                
               PIC X(0006).                                                     
           02  HV84-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV84-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV84-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-QPIECESEMP                                                  
               PIC S9(5) COMP-3.                                                
           02  HV84-PCAEMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTACHATSEMP                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTPRIMVOL                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-QPIECESEXC                                                  
               PIC S9(5) COMP-3.                                                
           02  HV84-PCAEXC                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTACHATSEXC                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-QPIECESEXE                                                  
               PIC S9(5) COMP-3.                                                
           02  HV84-PCAEXE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTACHATSEXE                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTPRIMESCORR                                               
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV84-PMTPRIMVOLCORR                                              
               PIC S9(7)V9(0002) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV8400                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV8400-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-DSVENTECIALE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-DSVENTECIALE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-QPIECESEMP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PCAEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTACHATSEMP-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTPRIMVOL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-QPIECESEXC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-QPIECESEXC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PCAEXC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PCAEXC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTACHATSEXC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTACHATSEXC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-QPIECESEXE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-QPIECESEXE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PCAEXE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PCAEXE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTACHATSEXE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTACHATSEXE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTPRIMESCORR-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTPRIMESCORR-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV84-PMTPRIMVOLCORR-F                                            
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV84-PMTPRIMVOLCORR-F                                            
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
