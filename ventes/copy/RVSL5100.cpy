      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL5100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL5100                         
      *   TABLE DES RESULTATS D'INVENTAIRE PAR CEMPL/CODIC                      
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL5100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL5100.                                                            
      *}                                                                        
           02  SL51-DINV                                                        
               PIC   X(08).                                                     
           02  SL51-NSOCIETE                                                    
               PIC   X(03).                                                     
           02  SL51-NLIEU                                                       
               PIC   X(03).                                                     
           02  SL51-CEMPL                                                       
               PIC   X(05).                                                     
           02  SL51-NCODIC                                                      
               PIC   X(07).                                                     
           02  SL51-QSTKT                                                       
               PIC   S9(07) COMP-3.                                             
           02  SL51-ECARTB                                                      
               PIC   S9(07) COMP-3.                                             
           02  SL51-ECARTN                                                      
               PIC   S9(07) COMP-3.                                             
           02  SL51-QSTKR                                                       
               PIC   S9(07) COMP-3.                                             
           02  SL51-DSYST                                                       
               PIC   S9(13) COMP-3.                                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVSL5100                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL5100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL5100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-DINV-F                                                      
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-DINV-F                                                      
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-NSOCIETE-F                                                  
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-NSOCIETE-F                                                  
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-NLIEU-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-NLIEU-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-CEMPL-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-CEMPL-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-NCODIC-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-NCODIC-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-QSTKT-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-QSTKT-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-ECARTB-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-ECARTB-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-ECARTN-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-ECARTN-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-QSTKR-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL51-QSTKR-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL51-DSYST-F                                                     
      *        PIC   S9(4) COMP.                                                
      *                                                                         
      *--                                                                       
           02  SL51-DSYST-F                                                     
               PIC   S9(4) COMP-5.                                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
