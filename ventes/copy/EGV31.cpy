      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV31   EGV31                                              00000020
      ***************************************************************** 00000030
       01   EGV31I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MWPAGEI   PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MMAGI     PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNVENTEI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIENTL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCLIENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLIENTF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCLIENTI  PIC X(9).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDVENTEI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNORDREI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWASCL    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MWASCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWASCF    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWASCI    PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWASCUTILL     COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MWASCUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWASCUTILF     PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWASCUTILI     PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMARCHESL     COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MWMARCHESL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWMARCHESF     PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWMARCHESI     PIC X.                                     00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWCOLL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MWCOLL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MWCOLF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWCOLI    PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBRPRDREPL    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNBRPRDREPL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNBRPRDREPF    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNBRPRDREPI    PIC X(2).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWREPRISEL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MWREPRISEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWREPRISEF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWREPRISEI     PIC X.                                     00000610
           02 MLLIVRI OCCURS   2 TIMES .                                00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENLL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MCOMMENLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENLF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCOMMENLI    PIC X(78).                                 00000660
           02 MLREPRII OCCURS   2 TIMES .                               00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMMENRL    COMP PIC S9(4).                            00000680
      *--                                                                       
             03 MCOMMENRL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCOMMENRF    PIC X.                                     00000690
             03 FILLER  PIC X(4).                                       00000700
             03 MCOMMENRI    PIC X(78).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MZONCMDI  PIC X(15).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MLIBERRI  PIC X(58).                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCODTRAI  PIC X(4).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCICSI    PIC X(5).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MNETNAMI  PIC X(8).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MSCREENI  PIC X(4).                                       00000950
      ***************************************************************** 00000960
      * SDF: EGV31   EGV31                                              00000970
      ***************************************************************** 00000980
       01   EGV31O REDEFINES EGV31I.                                    00000990
           02 FILLER    PIC X(12).                                      00001000
           02 FILLER    PIC X(2).                                       00001010
           02 MDATJOUA  PIC X.                                          00001020
           02 MDATJOUC  PIC X.                                          00001030
           02 MDATJOUP  PIC X.                                          00001040
           02 MDATJOUH  PIC X.                                          00001050
           02 MDATJOUV  PIC X.                                          00001060
           02 MDATJOUO  PIC X(10).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MTIMJOUA  PIC X.                                          00001090
           02 MTIMJOUC  PIC X.                                          00001100
           02 MTIMJOUP  PIC X.                                          00001110
           02 MTIMJOUH  PIC X.                                          00001120
           02 MTIMJOUV  PIC X.                                          00001130
           02 MTIMJOUO  PIC X(5).                                       00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MWPAGEA   PIC X.                                          00001160
           02 MWPAGEC   PIC X.                                          00001170
           02 MWPAGEP   PIC X.                                          00001180
           02 MWPAGEH   PIC X.                                          00001190
           02 MWPAGEV   PIC X.                                          00001200
           02 MWPAGEO   PIC X(3).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MMAGA     PIC X.                                          00001230
           02 MMAGC     PIC X.                                          00001240
           02 MMAGP     PIC X.                                          00001250
           02 MMAGH     PIC X.                                          00001260
           02 MMAGV     PIC X.                                          00001270
           02 MMAGO     PIC X(3).                                       00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MNVENTEA  PIC X.                                          00001300
           02 MNVENTEC  PIC X.                                          00001310
           02 MNVENTEP  PIC X.                                          00001320
           02 MNVENTEH  PIC X.                                          00001330
           02 MNVENTEV  PIC X.                                          00001340
           02 MNVENTEO  PIC X(7).                                       00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MCLIENTA  PIC X.                                          00001370
           02 MCLIENTC  PIC X.                                          00001380
           02 MCLIENTP  PIC X.                                          00001390
           02 MCLIENTH  PIC X.                                          00001400
           02 MCLIENTV  PIC X.                                          00001410
           02 MCLIENTO  PIC X(9).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDVENTEA  PIC X.                                          00001440
           02 MDVENTEC  PIC X.                                          00001450
           02 MDVENTEP  PIC X.                                          00001460
           02 MDVENTEH  PIC X.                                          00001470
           02 MDVENTEV  PIC X.                                          00001480
           02 MDVENTEO  PIC X(8).                                       00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MNORDREA  PIC X.                                          00001510
           02 MNORDREC  PIC X.                                          00001520
           02 MNORDREP  PIC X.                                          00001530
           02 MNORDREH  PIC X.                                          00001540
           02 MNORDREV  PIC X.                                          00001550
           02 MNORDREO  PIC X(5).                                       00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MWASCA    PIC X.                                          00001580
           02 MWASCC    PIC X.                                          00001590
           02 MWASCP    PIC X.                                          00001600
           02 MWASCH    PIC X.                                          00001610
           02 MWASCV    PIC X.                                          00001620
           02 MWASCO    PIC X.                                          00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MWASCUTILA     PIC X.                                     00001650
           02 MWASCUTILC     PIC X.                                     00001660
           02 MWASCUTILP     PIC X.                                     00001670
           02 MWASCUTILH     PIC X.                                     00001680
           02 MWASCUTILV     PIC X.                                     00001690
           02 MWASCUTILO     PIC X.                                     00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MWMARCHESA     PIC X.                                     00001720
           02 MWMARCHESC     PIC X.                                     00001730
           02 MWMARCHESP     PIC X.                                     00001740
           02 MWMARCHESH     PIC X.                                     00001750
           02 MWMARCHESV     PIC X.                                     00001760
           02 MWMARCHESO     PIC X.                                     00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MWCOLA    PIC X.                                          00001790
           02 MWCOLC    PIC X.                                          00001800
           02 MWCOLP    PIC X.                                          00001810
           02 MWCOLH    PIC X.                                          00001820
           02 MWCOLV    PIC X.                                          00001830
           02 MWCOLO    PIC X.                                          00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNBRPRDREPA    PIC X.                                     00001860
           02 MNBRPRDREPC    PIC X.                                     00001870
           02 MNBRPRDREPP    PIC X.                                     00001880
           02 MNBRPRDREPH    PIC X.                                     00001890
           02 MNBRPRDREPV    PIC X.                                     00001900
           02 MNBRPRDREPO    PIC X(2).                                  00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MWREPRISEA     PIC X.                                     00001930
           02 MWREPRISEC     PIC X.                                     00001940
           02 MWREPRISEP     PIC X.                                     00001950
           02 MWREPRISEH     PIC X.                                     00001960
           02 MWREPRISEV     PIC X.                                     00001970
           02 MWREPRISEO     PIC X.                                     00001980
           02 MLLIVRO OCCURS   2 TIMES .                                00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MCOMMENLA    PIC X.                                     00002010
             03 MCOMMENLC    PIC X.                                     00002020
             03 MCOMMENLP    PIC X.                                     00002030
             03 MCOMMENLH    PIC X.                                     00002040
             03 MCOMMENLV    PIC X.                                     00002050
             03 MCOMMENLO    PIC X(78).                                 00002060
           02 MLREPRIO OCCURS   2 TIMES .                               00002070
             03 FILLER       PIC X(2).                                  00002080
             03 MCOMMENRA    PIC X.                                     00002090
             03 MCOMMENRC    PIC X.                                     00002100
             03 MCOMMENRP    PIC X.                                     00002110
             03 MCOMMENRH    PIC X.                                     00002120
             03 MCOMMENRV    PIC X.                                     00002130
             03 MCOMMENRO    PIC X(78).                                 00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MZONCMDA  PIC X.                                          00002160
           02 MZONCMDC  PIC X.                                          00002170
           02 MZONCMDP  PIC X.                                          00002180
           02 MZONCMDH  PIC X.                                          00002190
           02 MZONCMDV  PIC X.                                          00002200
           02 MZONCMDO  PIC X(15).                                      00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MLIBERRA  PIC X.                                          00002230
           02 MLIBERRC  PIC X.                                          00002240
           02 MLIBERRP  PIC X.                                          00002250
           02 MLIBERRH  PIC X.                                          00002260
           02 MLIBERRV  PIC X.                                          00002270
           02 MLIBERRO  PIC X(58).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCODTRAA  PIC X.                                          00002300
           02 MCODTRAC  PIC X.                                          00002310
           02 MCODTRAP  PIC X.                                          00002320
           02 MCODTRAH  PIC X.                                          00002330
           02 MCODTRAV  PIC X.                                          00002340
           02 MCODTRAO  PIC X(4).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCICSA    PIC X.                                          00002370
           02 MCICSC    PIC X.                                          00002380
           02 MCICSP    PIC X.                                          00002390
           02 MCICSH    PIC X.                                          00002400
           02 MCICSV    PIC X.                                          00002410
           02 MCICSO    PIC X(5).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MNETNAMA  PIC X.                                          00002440
           02 MNETNAMC  PIC X.                                          00002450
           02 MNETNAMP  PIC X.                                          00002460
           02 MNETNAMH  PIC X.                                          00002470
           02 MNETNAMV  PIC X.                                          00002480
           02 MNETNAMO  PIC X(8).                                       00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MSCREENA  PIC X.                                          00002510
           02 MSCREENC  PIC X.                                          00002520
           02 MSCREENP  PIC X.                                          00002530
           02 MSCREENH  PIC X.                                          00002540
           02 MSCREENV  PIC X.                                          00002550
           02 MSCREENO  PIC X(4).                                       00002560
                                                                                
