      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV620 AU 28/06/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,01,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                           23,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV620.                                                        
            05 NOMETAT-IGV620           PIC X(6) VALUE 'IGV620'.                
            05 RUPTURES-IGV620.                                                 
           10 IGV620-NSOCIETE           PIC X(03).                      007  003
           10 IGV620-NLIEU              PIC X(03).                      010  003
           10 IGV620-NVENTE             PIC X(07).                      013  007
           10 IGV620-CTYPMAJ            PIC X(01).                      020  001
           10 IGV620-NLIGNE             PIC X(02).                      021  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV620-SEQUENCE           PIC S9(04) COMP.                023  002
      *--                                                                       
           10 IGV620-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV620.                                                   
           10 IGV620-CACID              PIC X(08).                      025  008
           10 IGV620-CFAM               PIC X(05).                      033  005
           10 IGV620-CMARQ              PIC X(05).                      038  005
           10 IGV620-CMODDEL            PIC X(03).                      043  003
           10 IGV620-CMODDEL1           PIC X(03).                      046  003
           10 IGV620-CTYPLIEUORIG       PIC X(07).                      049  007
           10 IGV620-LECART             PIC X(28).                      056  028
           10 IGV620-LLIEU              PIC X(20).                      084  020
           10 IGV620-LREFFOURN          PIC X(20).                      104  020
           10 IGV620-LTOTAL             PIC X(30).                      124  030
           10 IGV620-LTYPMAJ            PIC X(03).                      154  003
           10 IGV620-NCODIC             PIC X(07).                      157  007
           10 IGV620-NETNAME            PIC X(08).                      164  008
           10 IGV620-PVUNIT             PIC X(10).                      172  010
           10 IGV620-PVUNIT1            PIC X(10).                      182  010
           10 IGV620-PVTOTAL            PIC S9(07)V9(2) COMP-3.         192  005
           10 IGV620-PVTOTAL1           PIC S9(07)V9(2) COMP-3.         197  005
           10 IGV620-QVENDUE            PIC S9(05)      COMP-3.         202  003
           10 IGV620-QVENDUE1           PIC S9(05)      COMP-3.         205  003
           10 IGV620-REG-PCOMPT         PIC S9(07)V9(2) COMP-3.         208  005
           10 IGV620-REG-PDIFFERE       PIC S9(07)V9(2) COMP-3.         213  005
           10 IGV620-REG-PLIVR          PIC S9(07)V9(2) COMP-3.         218  005
           10 IGV620-REG-PRFACT         PIC S9(07)V9(2) COMP-3.         223  005
           10 IGV620-REG-PTTVENTE       PIC S9(07)V9(2) COMP-3.         228  005
           10 IGV620-TOT-PVTOTAL        PIC S9(07)V9(2) COMP-3.         233  005
            05 FILLER                      PIC X(275).                          
                                                                                
