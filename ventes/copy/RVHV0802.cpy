      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVHV0802                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0802                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVHV0802.                                                    00000090
           02  HV08-NSOCIETE                                            00000100
               PIC X(0003).                                             00000110
           02  HV08-CFAM                                                00000120
               PIC X(0005).                                             00000130
           02  HV08-DVENTECIALE                                         00000140
               PIC X(0008).                                             00000150
           02  HV08-QPIECES                                             00000160
               PIC S9(5) COMP-3.                                        00000170
           02  HV08-QPIECESCOMM                                         00000180
               PIC S9(5) COMP-3.                                        00000190
           02  HV08-PCA                                                 00000200
               PIC S9(7)V9(0002) COMP-3.                                00000210
           02  HV08-PCACOMM                                             00000220
               PIC S9(7)V9(0002) COMP-3.                                00000230
           02  HV08-PMTACHATS                                           00000240
               PIC S9(7)V9(0002) COMP-3.                                00000250
           02  HV08-PMTACHATSCOMM                                       00000260
               PIC S9(7)V9(0002) COMP-3.                                00000270
           02  HV08-PMTPRIMES                                           00000280
               PIC S9(7)V9(0002) COMP-3.                                00000290
           02  HV08-DSYST                                               00000300
               PIC S9(13) COMP-3.                                       00000310
           02  HV08-QPIECESEMP                                          00000320
               PIC S9(5) COMP-3.                                        00000330
           02  HV08-PCAEMP                                              00000340
               PIC S9(7)V9(0002) COMP-3.                                00000350
           02  HV08-PMTACHATSEMP                                        00000360
               PIC S9(7)V9(0002) COMP-3.                                00000370
           02  HV08-PMTPRIMVOL                                          00000380
               PIC S9(7)V9(0002) COMP-3.                                00000390
      *                                                                 00000400
      *---------------------------------------------------------        00000410
      *   LISTE DES FLAGS DE LA TABLE RVHV0802                          00000420
      *---------------------------------------------------------        00000430
      *                                                                 00000440
       01  RVHV0802-FLAGS.                                              00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-NSOCIETE-F                                          00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  HV08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-CFAM-F                                              00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  HV08-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DVENTECIALE-F                                       00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  HV08-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECES-F                                           00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  HV08-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESCOMM-F                                       00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  HV08-QPIECESCOMM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCA-F                                               00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  HV08-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCACOMM-F                                           00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  HV08-PCACOMM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATS-F                                         00000600
      *        PIC S9(4) COMP.                                          00000610
      *--                                                                       
           02  HV08-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSCOMM-F                                     00000620
      *        PIC S9(4) COMP.                                          00000630
      *--                                                                       
           02  HV08-PMTACHATSCOMM-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTPRIMES-F                                         00000640
      *        PIC S9(4) COMP.                                          00000650
      *--                                                                       
           02  HV08-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DSYST-F                                             00000660
      *        PIC S9(4) COMP.                                          00000670
      *--                                                                       
           02  HV08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESEMP-F                                        00000680
      *        PIC S9(4) COMP.                                          00000690
      *--                                                                       
           02  HV08-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCAEMP-F                                            00000700
      *        PIC S9(4) COMP.                                          00000710
      *--                                                                       
           02  HV08-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSEMP-F                                      00000720
      *        PIC S9(4) COMP.                                          00000730
      *--                                                                       
           02  HV08-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTPRIMVOL-F                                        00000740
      *        PIC S9(4) COMP.                                          00000750
      *--                                                                       
           02  HV08-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000760
                                                                                
