      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV50   EGV50                                              00000020
      ***************************************************************** 00000030
       01   EGV50I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(15).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOL      COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MSOL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MSOF      PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MSOI      PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMGL      COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MMGL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MMGF      PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MMGI      PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MORDRL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MORDRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MORDRF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MORDRI    PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVENTEL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MVENTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVENTEF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MVENTEI   PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAUTORDL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCAUTORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCAUTORDF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCAUTORDI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDADEBL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDADEBL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDADEBF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDADEBI   PIC X(6).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDAFINL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDAFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDAFINF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDAFINI   PIC X(6).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLIBERRI  PIC X(78).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MCODTRAI  PIC X(4).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MCICSI    PIC X(5).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNETNAMI  PIC X(8).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MSCREENI  PIC X(4).                                       00000650
      ***************************************************************** 00000660
      * SDF: EGV50   EGV50                                              00000670
      ***************************************************************** 00000680
       01   EGV50O REDEFINES EGV50I.                                    00000690
           02 FILLER    PIC X(12).                                      00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MDATJOUA  PIC X.                                          00000720
           02 MDATJOUC  PIC X.                                          00000730
           02 MDATJOUP  PIC X.                                          00000740
           02 MDATJOUH  PIC X.                                          00000750
           02 MDATJOUV  PIC X.                                          00000760
           02 MDATJOUO  PIC X(10).                                      00000770
           02 FILLER    PIC X(2).                                       00000780
           02 MTIMJOUA  PIC X.                                          00000790
           02 MTIMJOUC  PIC X.                                          00000800
           02 MTIMJOUP  PIC X.                                          00000810
           02 MTIMJOUH  PIC X.                                          00000820
           02 MTIMJOUV  PIC X.                                          00000830
           02 MTIMJOUO  PIC X(5).                                       00000840
           02 FILLER    PIC X(2).                                       00000850
           02 MZONCMDA  PIC X.                                          00000860
           02 MZONCMDC  PIC X.                                          00000870
           02 MZONCMDP  PIC X.                                          00000880
           02 MZONCMDH  PIC X.                                          00000890
           02 MZONCMDV  PIC X.                                          00000900
           02 MZONCMDO  PIC X(15).                                      00000910
           02 FILLER    PIC X(2).                                       00000920
           02 MSOA      PIC X.                                          00000930
           02 MSOC      PIC X.                                          00000940
           02 MSOP      PIC X.                                          00000950
           02 MSOH      PIC X.                                          00000960
           02 MSOV      PIC X.                                          00000970
           02 MSOO      PIC X(3).                                       00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MMGA      PIC X.                                          00001000
           02 MMGC      PIC X.                                          00001010
           02 MMGP      PIC X.                                          00001020
           02 MMGH      PIC X.                                          00001030
           02 MMGV      PIC X.                                          00001040
           02 MMGO      PIC X(3).                                       00001050
           02 FILLER    PIC X(2).                                       00001060
           02 MORDRA    PIC X.                                          00001070
           02 MORDRC    PIC X.                                          00001080
           02 MORDRP    PIC X.                                          00001090
           02 MORDRH    PIC X.                                          00001100
           02 MORDRV    PIC X.                                          00001110
           02 MORDRO    PIC X(5).                                       00001120
           02 FILLER    PIC X(2).                                       00001130
           02 MVENTEA   PIC X.                                          00001140
           02 MVENTEC   PIC X.                                          00001150
           02 MVENTEP   PIC X.                                          00001160
           02 MVENTEH   PIC X.                                          00001170
           02 MVENTEV   PIC X.                                          00001180
           02 MVENTEO   PIC X(7).                                       00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MCAUTORDA      PIC X.                                     00001210
           02 MCAUTORDC PIC X.                                          00001220
           02 MCAUTORDP PIC X.                                          00001230
           02 MCAUTORDH PIC X.                                          00001240
           02 MCAUTORDV PIC X.                                          00001250
           02 MCAUTORDO      PIC X(5).                                  00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MDADEBA   PIC X.                                          00001280
           02 MDADEBC   PIC X.                                          00001290
           02 MDADEBP   PIC X.                                          00001300
           02 MDADEBH   PIC X.                                          00001310
           02 MDADEBV   PIC X.                                          00001320
           02 MDADEBO   PIC X(6).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDAFINA   PIC X.                                          00001350
           02 MDAFINC   PIC X.                                          00001360
           02 MDAFINP   PIC X.                                          00001370
           02 MDAFINH   PIC X.                                          00001380
           02 MDAFINV   PIC X.                                          00001390
           02 MDAFINO   PIC X(6).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLIBERRA  PIC X.                                          00001420
           02 MLIBERRC  PIC X.                                          00001430
           02 MLIBERRP  PIC X.                                          00001440
           02 MLIBERRH  PIC X.                                          00001450
           02 MLIBERRV  PIC X.                                          00001460
           02 MLIBERRO  PIC X(78).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MCODTRAA  PIC X.                                          00001490
           02 MCODTRAC  PIC X.                                          00001500
           02 MCODTRAP  PIC X.                                          00001510
           02 MCODTRAH  PIC X.                                          00001520
           02 MCODTRAV  PIC X.                                          00001530
           02 MCODTRAO  PIC X(4).                                       00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MCICSA    PIC X.                                          00001560
           02 MCICSC    PIC X.                                          00001570
           02 MCICSP    PIC X.                                          00001580
           02 MCICSH    PIC X.                                          00001590
           02 MCICSV    PIC X.                                          00001600
           02 MCICSO    PIC X(5).                                       00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MNETNAMA  PIC X.                                          00001630
           02 MNETNAMC  PIC X.                                          00001640
           02 MNETNAMP  PIC X.                                          00001650
           02 MNETNAMH  PIC X.                                          00001660
           02 MNETNAMV  PIC X.                                          00001670
           02 MNETNAMO  PIC X(8).                                       00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MSCREENA  PIC X.                                          00001700
           02 MSCREENC  PIC X.                                          00001710
           02 MSCREENP  PIC X.                                          00001720
           02 MSCREENH  PIC X.                                          00001730
           02 MSCREENV  PIC X.                                          00001740
           02 MSCREENO  PIC X(4).                                       00001750
                                                                                
