      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT5000                                     00020003
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT5000                 00060003
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT5000.                                                    00090003
           02  VT50-NFLAG                                               00100005
               PIC X(0001).                                             00110005
           02  VT50-NCLUSTERP                                           00120004
               PIC X(0013).                                             00130002
           02  VT50-DVENTE                                              00140004
               PIC X(0008).                                             00150004
           02  VT50-NVENTE                                              00160004
               PIC X(0007).                                             00170001
           02  VT50-NLIEU                                               00180004
               PIC X(0003).                                             00190004
           02  VT50-NSOCIETE                                            00200004
               PIC X(0003).                                             00210001
           02  VT50-NDOC                                                00220004
               PIC X(0002).                                             00230004
           02  VT50-NSEQ                                                00240004
               PIC X(0002).                                             00250004
           02  VT50-NSERVICE                                            00260004
               PIC X(0004).                                             00270004
           02  VT50-NBOX                                                00280004
               PIC X(0005).                                             00290004
           02  VT50-NBOITE                                              00300004
               PIC X(0006).                                             00310004
           02  VT50-NFICHE                                              00320004
               PIC X(0003).                                             00330004
      *                                                                 00340001
      *---------------------------------------------------------        00350001
      *   LISTE DES FLAGS DE LA TABLE RVVT5000                          00360003
      *---------------------------------------------------------        00370001
      *                                                                 00380001
       01  RVVT5000-FLAGS.                                              00390003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NFLAG-F                                             00400005
      *        PIC S9(4) COMP.                                          00410005
      *--                                                                       
           02  VT50-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NCLUSTER-P                                          00420004
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  VT50-NCLUSTER-P                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-DVENTE-F                                            00440004
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT50-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NVENTE-F                                            00460004
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT50-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NLIEU-F                                             00480004
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT50-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NSOCIETE-F                                          00500004
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT50-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NDOC-F                                              00520004
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT50-NDOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NSEQ-F                                              00540004
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT50-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NSERVICE-F                                          00560004
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT50-NSERVICE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NBOX-F                                              00580004
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT50-NBOX-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NBOITE-F                                            00600004
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT50-NBOITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT50-NFICHE-F                                            00620004
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT50-NFICHE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640001
                                                                                
