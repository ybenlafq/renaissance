      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT1700                                     00020002
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1700                 00060002
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT1700.                                                    00090002
           02  VT17-NFLAG                                               00100002
               PIC X(0001).                                             00110002
           02  VT17-NCLUSTER                                            00120002
               PIC X(0013).                                             00130002
           02  VT17-NSOCIETE                                            00140002
               PIC X(0003).                                             00150002
           02  VT17-NLIGNE                                              00160002
               PIC X(0003).                                             00170001
           02  VT17-NLCODIC                                             00180002
               PIC X(0003).                                             00190001
           02  VT17-NCODIC                                              00200002
               PIC X(0007).                                             00210001
           02  VT17-LCOMMENT                                            00220002
               PIC X(0030).                                             00230001
           02  VT17-CMODDEL                                             00240002
               PIC X(0003).                                             00250001
           02  VT17-DDELIV                                              00260002
               PIC X(0008).                                             00270001
           02  VT17-CENREG                                              00280002
               PIC X(0005).                                             00290001
           02  VT17-PVTOTAL                                             00300002
               PIC S9(7)V9(0002) COMP-3.                                00310001
           02  VT17-TAUXTVA                                             00320002
               PIC S9(3)V9(0002) COMP-3.                                00330001
           02  VT17-CANNULREP                                           00340002
               PIC X(0001).                                             00350001
           02  VT17-CVENDEUR                                            00360002
               PIC X(0007).                                             00370001
           02  VT17-NAUTORM                                             00371004
               PIC X(0005).                                             00372003
           02  VT17-NSEQNQ                                              00373004
               PIC S9(005)  COMP-3.                                     00374003
           02  VT17-NSEQREF                                             00375004
               PIC S9(005)  COMP-3.                                     00376003
           02  VT17-CTYPENT                                             00377005
               PIC X(002).                                              00378005
           02  VT17-NLIEN                                               00379004
               PIC S9(005)  COMP-3.                                     00379103
           02  VT17-NACTVTE                                             00379204
               PIC S9(005)  COMP-3.                                     00379303
           02  VT17-NSEQENS                                             00379404
               PIC S9(005)  COMP-3.                                     00379503
           02  VT17-MPRIMECLI                                           00379604
               PIC S9(7)V9(0002) COMP-3.                                00379703
      *                                                                 00380001
      *---------------------------------------------------------        00390001
      *   LISTE DES FLAGS DE LA TABLE RVVT1700                          00400002
      *---------------------------------------------------------        00410001
      *                                                                 00420001
       01  RVVT1700-FLAGS.                                              00430002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NFLAG-F                                             00440002
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT17-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NCLUSTER-F                                          00460002
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT17-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NSOCIETE-F                                          00480002
      *        PIC S9(4) COMP.                                          00490002
      *--                                                                       
           02  VT17-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NLIGNE-F                                            00500002
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT17-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NLCODIC-F                                           00520002
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT17-NLCODIC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NCODIC-F                                            00540002
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT17-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-LCOMMENT-F                                          00560002
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT17-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-CMODDEL-F                                           00580002
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT17-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-DDELIV-F                                            00600002
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT17-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-CENREG-F                                            00620002
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT17-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-PVTOTAL-F                                           00640002
      *        PIC S9(4) COMP.                                          00650001
      *--                                                                       
           02  VT17-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-TAUXTVA-F                                           00660002
      *        PIC S9(4) COMP.                                          00670001
      *--                                                                       
           02  VT17-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-CANNULREP-F                                         00680002
      *        PIC S9(4) COMP.                                          00690001
      *--                                                                       
           02  VT17-CANNULREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-CVENDEUR-F                                          00700002
      *        PIC S9(4) COMP.                                          00710001
      *--                                                                       
           02  VT17-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NAUTORM-F                                           00711004
      *        PIC S9(4) COMP.                                          00712003
      *--                                                                       
           02  VT17-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NSEQNQ-F                                            00713004
      *        PIC S9(4) COMP.                                          00714003
      *--                                                                       
           02  VT17-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NSEQREF-F                                           00715004
      *        PIC S9(4) COMP.                                          00716003
      *--                                                                       
           02  VT17-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-CTYPENT-F                                           00717005
      *        PIC S9(4) COMP.                                          00718003
      *--                                                                       
           02  VT17-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NLIEN-F                                             00719004
      *        PIC S9(4) COMP.                                          00719103
      *--                                                                       
           02  VT17-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NACTVTE-F                                           00719204
      *        PIC S9(4) COMP.                                          00719303
      *--                                                                       
           02  VT17-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-NSEQENS-F                                           00719404
      *        PIC S9(4) COMP.                                          00719503
      *--                                                                       
           02  VT17-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT17-MPRIMECLI-F                                         00719604
      *        PIC S9(4) COMP.                                          00719703
      *--                                                                       
           02  VT17-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00720001
                                                                                
