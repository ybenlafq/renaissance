      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSHV44 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSHV44-IDENTIFICATEUR.                                       00000050
           05  TSHV44-TRANSID               PIC X(04) VALUE 'HV44'.     00000060
           05  TSHV44-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSHV44-ITEM.                                                 00000090
           05  TSHV44-LONGUEUR              PIC S9(4) VALUE +0074.      00000100
           05  TSHV44-DATAS.                                            00000110
               10  TSHV44-LIGNE             OCCURS 02.                  00000120
      *   DESCRIPTION TRANSACTIONS VENTE 038 OCTETS PAR OCCURENCE     * 00000200
      *                                                               * 00000200
                25 TSHV44-NSOC               PIC X(03).                 00000070
                25 TSHV44-NMAG               PIC X(03).                 00000070
                25 TSHV44-NCAIS              PIC X(03).                 00000070
                25 TSHV44-NTRANS             PIC X(04).                 00000070
                25 TSHV44-CTYPTRANS          PIC X(01).                 00000070
                25 TSHV44-NOPER              PIC X(04).                 00000070
                25 TSHV44-DCAIS              PIC X(10).                 00000070
                25 TSHV44-DHMTRANS           PIC X(05).                 00000070
                25 TSHV44-PTRANS             PIC S9(5)V99 COMP-3.       00000070
      *                                                               * 00000200
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
