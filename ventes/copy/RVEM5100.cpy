      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5100                         
      *   CLE UNIQUE : NSOCIETE A NVENTE LG = 172                               
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5100.                                                            
      *}                                                                        
1          02  EM51-NSOCIETE                                                    
               PIC X(0003).                                                     
4          02  EM51-NLIEU                                                       
               PIC X(0003).                                                     
7          02  EM51-DCAISSE                                                     
               PIC X(0008).                                                     
15         02  EM51-NCAISSE                                                     
               PIC X(0003).                                                     
18         02  EM51-NTRANS                                                      
               PIC X(0008).                                                     
26         02  EM51-NSOCVENTE                                                   
               PIC X(0003).                                                     
29         02  EM51-NLIEUVENTE                                                  
               PIC X(0003).                                                     
32         02  EM51-NTYPVENTE                                                   
               PIC X(0003).                                                     
35         02  EM51-NVENTE                                                      
               PIC X(0007).                                                     
42         02  EM51-NTYPETRANS                                                  
               PIC X(0003).                                                     
45         02  EM51-NOPERATEUR                                                  
               PIC X(0007).                                                     
52         02  EM51-NVENDEUR                                                    
               PIC X(0007).                                                     
59         02  EM51-DVENTE                                                      
               PIC X(0008).                                                     
67         02  EM51-NFACTURE                                                    
               PIC X(0006).                                                     
73         02  EM51-NEMPORT                                                     
               PIC X(0008).                                                     
81         02  EM51-DHVENTE                                                     
               PIC X(0002).                                                     
83         02  EM51-DMVENTE                                                     
               PIC X(0002).                                                     
85         02  EM51-DHPAIMT                                                     
               PIC X(0002).                                                     
87         02  EM51-DMPAIMT                                                     
               PIC X(0002).                                                     
89         02  EM51-PTTTRANS                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
94         02  EM51-PTTVENTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
99         02  EM51-PVERSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
104        02  EM51-PCOMPT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
109        02  EM51-PLIVR                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
114        02  EM51-PDIFFERE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
119        02  EM51-PRFACT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
124        02  EM51-CORGORED                                                    
               PIC X(0005).                                                     
129        02  EM51-CMODPAIMT1                                                  
               PIC X(0005).                                                     
134        02  EM51-CMODPAIMT2                                                  
               PIC X(0005).                                                     
139        02  EM51-CMODPAIMT3                                                  
               PIC X(0005).                                                     
144        02  EM51-CMODPAIMT4                                                  
               PIC X(0005).                                                     
149        02  EM51-CMODPAIMT5                                                  
               PIC X(0005).                                                     
154        02  EM51-CMODPAIMT6                                                  
               PIC X(0005).                                                     
159        02  EM51-DMODPAIMT1                                                  
               PIC X(0001).                                                     
160        02  EM51-DMODPAIMT2                                                  
               PIC X(0001).                                                     
161        02  EM51-DMODPAIMT3                                                  
               PIC X(0001).                                                     
162        02  EM51-DMODPAIMT4                                                  
               PIC X(0001).                                                     
163        02  EM51-DMODPAIMT5                                                  
               PIC X(0001).                                                     
164        02  EM51-DMODPAIMT6                                                  
               PIC X(0001).                                                     
165        02  EM51-DEVENT                                                      
               PIC X(0001).                                                     
166        02  EM51-DEVCRED                                                     
               PIC X(0001).                                                     
167        02  EM51-NSOCFAC                                                     
               PIC X(0003).                                                     
170        02  EM51-NLIEUFAC                                                    
               PIC X(0003).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEM5101                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5101-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5101-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NSOCVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NSOCVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NLIEUVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NLIEUVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NTYPVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NTYPVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NTYPETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NTYPETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NEMPORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NEMPORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DHPAIMT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DHPAIMT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMPAIMT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMPAIMT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PTTTRANS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PTTTRANS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PTTVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PTTVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PVERSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PVERSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PCOMPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PCOMPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PLIVR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PLIVR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PDIFFERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PDIFFERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-PRFACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-PRFACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CORGORED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CORGORED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT5-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT5-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-CMODPAIMT6-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-CMODPAIMT6-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT5-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT5-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DMODPAIMT6-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DMODPAIMT6-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DEVENT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DEVENT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-DEVCRED-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-DEVCRED-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NSOCFAC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NSOCFAC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM51-NLIEUFAC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM51-NLIEUFAC-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
