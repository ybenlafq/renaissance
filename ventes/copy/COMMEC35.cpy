      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMMAREA PSE POST ACHAT MEC35                                           
      ******************************************************************        
           02  W-MEC35-ENTETE.                                                  
              03  W-MEC35-NSOC                PIC X(003).                       
              03  W-MEC35-NLIEU               PIC X(003).                       
              03  W-MEC35-NVENTE              PIC X(007).                       
              03  W-MEC35-NCDEWC              PIC S9(15) PACKED-DECIMAL.        
              03  W-MEC35-MD-PMENT            PIC X(005).                       
      * tableau des ajouts PSE (indice, utilis�, max)                           
           02  W-MEC35-DETAIL.                                                  
               04  W-MEC35-CRET      PIC X(002).                                
               04  W-MEC35-CENREG    PIC X(005).                                
               04  W-MEC35-DT-LIV    PIC X(008).                                
               04  W-MEC35-PX-VT     PIC S9(5)V99 PACKED-DECIMAL.               
               04  W-MEC35-STAT      PIC X(001).                                
               04  W-MEC35-C-RET     PIC X(001).                                
           02  W-MEC35-RETOUR.                                                  
               04  W-MEC35-CODE-RET               PIC X(1).                     
               04  W-MEC35-MES-RET                PIC X(80).                    
                                                                                
