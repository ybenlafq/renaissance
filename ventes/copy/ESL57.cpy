      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EVT41   EVT41                                              00000020
      ***************************************************************** 00000030
       01   ESL57I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNOPAGEI  PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPAGEI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCI    PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNVENTEI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLNOMI    PIC X(25).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLPRENOMI      PIC X(15).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDVENTEI  PIC X(5).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELDOML  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MTELDOML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELDOMF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MTELDOMI  PIC X(10).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNGSML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNGSML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNGSMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNGSMI    PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTELBURL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MTELBURL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTELBURF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MTELBURI  PIC X(10).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPOSTEL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MPOSTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPOSTEF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MPOSTEI   PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPVTEL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MTYPVTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPVTEF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MTYPVTEI  PIC X(25).                                      00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLATOSL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MLATOSL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLATOSF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MLATOSI   PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREC-RESL      COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MREC-RESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MREC-RESF      PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MREC-RESI      PIC X(3).                                  00000730
           02 MTAB1I OCCURS   5 TIMES .                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODACTL     COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCODACTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCODACTF     PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCODACTI     PIC X.                                     00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MNCODICI     PIC X(7).                                  00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCFAML  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 MCFAML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCFAMF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 MCFAMI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMARQL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCMARQL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCMARQF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCMARQI      PIC X(5).                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLREFL  COMP PIC S9(4).                                 00000910
      *--                                                                       
             03 MLREFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLREFF  PIC X.                                          00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MLREFI  PIC X(20).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000950
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000960
             03 FILLER  PIC X(4).                                       00000970
             03 MQTEI   PIC X(3).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDISL   COMP PIC S9(4).                                 00000990
      *--                                                                       
             03 MDISL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDISF   PIC X.                                          00001000
             03 FILLER  PIC X(4).                                       00001010
             03 MDISI   PIC X(3).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTERECL     COMP PIC S9(4).                            00001030
      *--                                                                       
             03 MQTERECL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MQTERECF     PIC X.                                     00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MQTERECI     PIC X(3).                                  00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDLIVL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MDDLIVL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDDLIVF      PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MDDLIVI      PIC X(5).                                  00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTFOURL     COMP PIC S9(4).                            00001110
      *--                                                                       
             03 MDTFOURL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDTFOURF     PIC X.                                     00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MDTFOURI     PIC X(5).                                  00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDTYL   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MDTYL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MDTYF   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MDTYI   PIC X.                                          00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCOMML  COMP PIC S9(4).                                 00001190
      *--                                                                       
             03 MCOMML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCOMMF  PIC X.                                          00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MCOMMI  PIC X.                                          00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLSTATL      COMP PIC S9(4).                            00001230
      *--                                                                       
             03 MLSTATL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLSTATF      PIC X.                                     00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MLSTATI      PIC X(8).                                  00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPPROL     COMP PIC S9(4).                            00001270
      *--                                                                       
             03 MCAPPROL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCAPPROF     PIC X.                                     00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MCAPPROI     PIC X(5).                                  00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDCREL      COMP PIC S9(4).                            00001310
      *--                                                                       
             03 MLDCREL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDCREF      PIC X.                                     00001320
             03 FILLER  PIC X(4).                                       00001330
             03 MLDCREI      PIC X(10).                                 00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREATL     COMP PIC S9(4).                            00001350
      *--                                                                       
             03 MDCREATL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MDCREATF     PIC X.                                     00001360
             03 FILLER  PIC X(4).                                       00001370
             03 MDCREATI     PIC X(5).                                  00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLIBERRI  PIC X(78).                                      00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MCODTRAI  PIC X(4).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MCICSI    PIC X(5).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MNETNAMI  PIC X(8).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MSCREENI  PIC X(4).                                       00001580
      ***************************************************************** 00001590
      * SDF: EVT41   EVT41                                              00001600
      ***************************************************************** 00001610
       01   ESL57O REDEFINES ESL57I.                                    00001620
           02 FILLER    PIC X(12).                                      00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MDATJOUA  PIC X.                                          00001650
           02 MDATJOUC  PIC X.                                          00001660
           02 MDATJOUP  PIC X.                                          00001670
           02 MDATJOUH  PIC X.                                          00001680
           02 MDATJOUV  PIC X.                                          00001690
           02 MDATJOUO  PIC X(10).                                      00001700
           02 FILLER    PIC X(2).                                       00001710
           02 MTIMJOUA  PIC X.                                          00001720
           02 MTIMJOUC  PIC X.                                          00001730
           02 MTIMJOUP  PIC X.                                          00001740
           02 MTIMJOUH  PIC X.                                          00001750
           02 MTIMJOUV  PIC X.                                          00001760
           02 MTIMJOUO  PIC X(5).                                       00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MNOPAGEA  PIC X.                                          00001790
           02 MNOPAGEC  PIC X.                                          00001800
           02 MNOPAGEP  PIC X.                                          00001810
           02 MNOPAGEH  PIC X.                                          00001820
           02 MNOPAGEV  PIC X.                                          00001830
           02 MNOPAGEO  PIC X(3).                                       00001840
           02 FILLER    PIC X(2).                                       00001850
           02 MNBPAGEA  PIC X.                                          00001860
           02 MNBPAGEC  PIC X.                                          00001870
           02 MNBPAGEP  PIC X.                                          00001880
           02 MNBPAGEH  PIC X.                                          00001890
           02 MNBPAGEV  PIC X.                                          00001900
           02 MNBPAGEO  PIC X(3).                                       00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNSOCA    PIC X.                                          00001930
           02 MNSOCC    PIC X.                                          00001940
           02 MNSOCP    PIC X.                                          00001950
           02 MNSOCH    PIC X.                                          00001960
           02 MNSOCV    PIC X.                                          00001970
           02 MNSOCO    PIC X(3).                                       00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNLIEUA   PIC X.                                          00002000
           02 MNLIEUC   PIC X.                                          00002010
           02 MNLIEUP   PIC X.                                          00002020
           02 MNLIEUH   PIC X.                                          00002030
           02 MNLIEUV   PIC X.                                          00002040
           02 MNLIEUO   PIC X(3).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MNVENTEA  PIC X.                                          00002070
           02 MNVENTEC  PIC X.                                          00002080
           02 MNVENTEP  PIC X.                                          00002090
           02 MNVENTEH  PIC X.                                          00002100
           02 MNVENTEV  PIC X.                                          00002110
           02 MNVENTEO  PIC X(7).                                       00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLNOMA    PIC X.                                          00002140
           02 MLNOMC    PIC X.                                          00002150
           02 MLNOMP    PIC X.                                          00002160
           02 MLNOMH    PIC X.                                          00002170
           02 MLNOMV    PIC X.                                          00002180
           02 MLNOMO    PIC X(25).                                      00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MLPRENOMA      PIC X.                                     00002210
           02 MLPRENOMC PIC X.                                          00002220
           02 MLPRENOMP PIC X.                                          00002230
           02 MLPRENOMH PIC X.                                          00002240
           02 MLPRENOMV PIC X.                                          00002250
           02 MLPRENOMO      PIC X(15).                                 00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MDVENTEA  PIC X.                                          00002280
           02 MDVENTEC  PIC X.                                          00002290
           02 MDVENTEP  PIC X.                                          00002300
           02 MDVENTEH  PIC X.                                          00002310
           02 MDVENTEV  PIC X.                                          00002320
           02 MDVENTEO  PIC X(5).                                       00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MTELDOMA  PIC X.                                          00002350
           02 MTELDOMC  PIC X.                                          00002360
           02 MTELDOMP  PIC X.                                          00002370
           02 MTELDOMH  PIC X.                                          00002380
           02 MTELDOMV  PIC X.                                          00002390
           02 MTELDOMO  PIC X(10).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MNGSMA    PIC X.                                          00002420
           02 MNGSMC    PIC X.                                          00002430
           02 MNGSMP    PIC X.                                          00002440
           02 MNGSMH    PIC X.                                          00002450
           02 MNGSMV    PIC X.                                          00002460
           02 MNGSMO    PIC X(10).                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MTELBURA  PIC X.                                          00002490
           02 MTELBURC  PIC X.                                          00002500
           02 MTELBURP  PIC X.                                          00002510
           02 MTELBURH  PIC X.                                          00002520
           02 MTELBURV  PIC X.                                          00002530
           02 MTELBURO  PIC X(10).                                      00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MPOSTEA   PIC X.                                          00002560
           02 MPOSTEC   PIC X.                                          00002570
           02 MPOSTEP   PIC X.                                          00002580
           02 MPOSTEH   PIC X.                                          00002590
           02 MPOSTEV   PIC X.                                          00002600
           02 MPOSTEO   PIC X(5).                                       00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MTYPVTEA  PIC X.                                          00002630
           02 MTYPVTEC  PIC X.                                          00002640
           02 MTYPVTEP  PIC X.                                          00002650
           02 MTYPVTEH  PIC X.                                          00002660
           02 MTYPVTEV  PIC X.                                          00002670
           02 MTYPVTEO  PIC X(25).                                      00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MLATOSA   PIC X.                                          00002700
           02 MLATOSC   PIC X.                                          00002710
           02 MLATOSP   PIC X.                                          00002720
           02 MLATOSH   PIC X.                                          00002730
           02 MLATOSV   PIC X.                                          00002740
           02 MLATOSO   PIC X(5).                                       00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MREC-RESA      PIC X.                                     00002770
           02 MREC-RESC PIC X.                                          00002780
           02 MREC-RESP PIC X.                                          00002790
           02 MREC-RESH PIC X.                                          00002800
           02 MREC-RESV PIC X.                                          00002810
           02 MREC-RESO      PIC X(3).                                  00002820
           02 MTAB1O OCCURS   5 TIMES .                                 00002830
             03 FILLER       PIC X(2).                                  00002840
             03 MCODACTA     PIC X.                                     00002850
             03 MCODACTC     PIC X.                                     00002860
             03 MCODACTP     PIC X.                                     00002870
             03 MCODACTH     PIC X.                                     00002880
             03 MCODACTV     PIC X.                                     00002890
             03 MCODACTO     PIC X.                                     00002900
             03 FILLER       PIC X(2).                                  00002910
             03 MNCODICA     PIC X.                                     00002920
             03 MNCODICC     PIC X.                                     00002930
             03 MNCODICP     PIC X.                                     00002940
             03 MNCODICH     PIC X.                                     00002950
             03 MNCODICV     PIC X.                                     00002960
             03 MNCODICO     PIC X(7).                                  00002970
             03 FILLER       PIC X(2).                                  00002980
             03 MCFAMA  PIC X.                                          00002990
             03 MCFAMC  PIC X.                                          00003000
             03 MCFAMP  PIC X.                                          00003010
             03 MCFAMH  PIC X.                                          00003020
             03 MCFAMV  PIC X.                                          00003030
             03 MCFAMO  PIC X(5).                                       00003040
             03 FILLER       PIC X(2).                                  00003050
             03 MCMARQA      PIC X.                                     00003060
             03 MCMARQC PIC X.                                          00003070
             03 MCMARQP PIC X.                                          00003080
             03 MCMARQH PIC X.                                          00003090
             03 MCMARQV PIC X.                                          00003100
             03 MCMARQO      PIC X(5).                                  00003110
             03 FILLER       PIC X(2).                                  00003120
             03 MLREFA  PIC X.                                          00003130
             03 MLREFC  PIC X.                                          00003140
             03 MLREFP  PIC X.                                          00003150
             03 MLREFH  PIC X.                                          00003160
             03 MLREFV  PIC X.                                          00003170
             03 MLREFO  PIC X(20).                                      00003180
             03 FILLER       PIC X(2).                                  00003190
             03 MQTEA   PIC X.                                          00003200
             03 MQTEC   PIC X.                                          00003210
             03 MQTEP   PIC X.                                          00003220
             03 MQTEH   PIC X.                                          00003230
             03 MQTEV   PIC X.                                          00003240
             03 MQTEO   PIC X(3).                                       00003250
             03 FILLER       PIC X(2).                                  00003260
             03 MDISA   PIC X.                                          00003270
             03 MDISC   PIC X.                                          00003280
             03 MDISP   PIC X.                                          00003290
             03 MDISH   PIC X.                                          00003300
             03 MDISV   PIC X.                                          00003310
             03 MDISO   PIC X(3).                                       00003320
             03 FILLER       PIC X(2).                                  00003330
             03 MQTERECA     PIC X.                                     00003340
             03 MQTERECC     PIC X.                                     00003350
             03 MQTERECP     PIC X.                                     00003360
             03 MQTERECH     PIC X.                                     00003370
             03 MQTERECV     PIC X.                                     00003380
             03 MQTERECO     PIC X(3).                                  00003390
             03 FILLER       PIC X(2).                                  00003400
             03 MDDLIVA      PIC X.                                     00003410
             03 MDDLIVC PIC X.                                          00003420
             03 MDDLIVP PIC X.                                          00003430
             03 MDDLIVH PIC X.                                          00003440
             03 MDDLIVV PIC X.                                          00003450
             03 MDDLIVO      PIC X(5).                                  00003460
             03 FILLER       PIC X(2).                                  00003470
             03 MDTFOURA     PIC X.                                     00003480
             03 MDTFOURC     PIC X.                                     00003490
             03 MDTFOURP     PIC X.                                     00003500
             03 MDTFOURH     PIC X.                                     00003510
             03 MDTFOURV     PIC X.                                     00003520
             03 MDTFOURO     PIC X(5).                                  00003530
             03 FILLER       PIC X(2).                                  00003540
             03 MDTYA   PIC X.                                          00003550
             03 MDTYC   PIC X.                                          00003560
             03 MDTYP   PIC X.                                          00003570
             03 MDTYH   PIC X.                                          00003580
             03 MDTYV   PIC X.                                          00003590
             03 MDTYO   PIC X.                                          00003600
             03 FILLER       PIC X(2).                                  00003610
             03 MCOMMA  PIC X.                                          00003620
             03 MCOMMC  PIC X.                                          00003630
             03 MCOMMP  PIC X.                                          00003640
             03 MCOMMH  PIC X.                                          00003650
             03 MCOMMV  PIC X.                                          00003660
             03 MCOMMO  PIC X.                                          00003670
             03 FILLER       PIC X(2).                                  00003680
             03 MLSTATA      PIC X.                                     00003690
             03 MLSTATC PIC X.                                          00003700
             03 MLSTATP PIC X.                                          00003710
             03 MLSTATH PIC X.                                          00003720
             03 MLSTATV PIC X.                                          00003730
             03 MLSTATO      PIC X(8).                                  00003740
             03 FILLER       PIC X(2).                                  00003750
             03 MCAPPROA     PIC X.                                     00003760
             03 MCAPPROC     PIC X.                                     00003770
             03 MCAPPROP     PIC X.                                     00003780
             03 MCAPPROH     PIC X.                                     00003790
             03 MCAPPROV     PIC X.                                     00003800
             03 MCAPPROO     PIC X(5).                                  00003810
             03 FILLER       PIC X(2).                                  00003820
             03 MLDCREA      PIC X.                                     00003830
             03 MLDCREC PIC X.                                          00003840
             03 MLDCREP PIC X.                                          00003850
             03 MLDCREH PIC X.                                          00003860
             03 MLDCREV PIC X.                                          00003870
             03 MLDCREO      PIC X(10).                                 00003880
             03 FILLER       PIC X(2).                                  00003890
             03 MDCREATA     PIC X.                                     00003900
             03 MDCREATC     PIC X.                                     00003910
             03 MDCREATP     PIC X.                                     00003920
             03 MDCREATH     PIC X.                                     00003930
             03 MDCREATV     PIC X.                                     00003940
             03 MDCREATO     PIC X(5).                                  00003950
           02 FILLER    PIC X(2).                                       00003960
           02 MLIBERRA  PIC X.                                          00003970
           02 MLIBERRC  PIC X.                                          00003980
           02 MLIBERRP  PIC X.                                          00003990
           02 MLIBERRH  PIC X.                                          00004000
           02 MLIBERRV  PIC X.                                          00004010
           02 MLIBERRO  PIC X(78).                                      00004020
           02 FILLER    PIC X(2).                                       00004030
           02 MCODTRAA  PIC X.                                          00004040
           02 MCODTRAC  PIC X.                                          00004050
           02 MCODTRAP  PIC X.                                          00004060
           02 MCODTRAH  PIC X.                                          00004070
           02 MCODTRAV  PIC X.                                          00004080
           02 MCODTRAO  PIC X(4).                                       00004090
           02 FILLER    PIC X(2).                                       00004100
           02 MCICSA    PIC X.                                          00004110
           02 MCICSC    PIC X.                                          00004120
           02 MCICSP    PIC X.                                          00004130
           02 MCICSH    PIC X.                                          00004140
           02 MCICSV    PIC X.                                          00004150
           02 MCICSO    PIC X(5).                                       00004160
           02 FILLER    PIC X(2).                                       00004170
           02 MNETNAMA  PIC X.                                          00004180
           02 MNETNAMC  PIC X.                                          00004190
           02 MNETNAMP  PIC X.                                          00004200
           02 MNETNAMH  PIC X.                                          00004210
           02 MNETNAMV  PIC X.                                          00004220
           02 MNETNAMO  PIC X(8).                                       00004230
           02 FILLER    PIC X(2).                                       00004240
           02 MSCREENA  PIC X.                                          00004250
           02 MSCREENC  PIC X.                                          00004260
           02 MSCREENP  PIC X.                                          00004270
           02 MSCREENH  PIC X.                                          00004280
           02 MSCREENV  PIC X.                                          00004290
           02 MSCREENO  PIC X(4).                                       00004300
                                                                                
