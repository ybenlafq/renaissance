      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAS1000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAS1000                         
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVAS1000.                                                            
           02  AS10-CCAMPAGNE                                                   
               PIC X(0004).                                                     
           02  AS10-CFAM                                                        
               PIC X(0005).                                                     
           02  AS10-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAS1000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAS1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS10-CCAMPAGNE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS10-CCAMPAGNE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS10-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS10-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AS10-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AS10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
