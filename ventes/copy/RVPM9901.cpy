      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVPM9900                      *        
      ******************************************************************        
       01  RVPM9900.                                                            
      *                       NSOC                                              
           10 PM99-NSOC            PIC X(3).                                    
      *                       NLIEU                                             
           10 PM99-NLIEU           PIC X(3).                                    
      *                       DJOUR                                             
           10 PM99-DJOUR           PIC X(8).                                    
      *                       CETAT                                             
           10 PM99-CETAT           PIC X(1).                                    
      *                       WMON                                              
           10 PM99-WMON            PIC X(1).                                    
      *                       DSYST                                             
           10 PM99-DSYST           PIC S9(13)V USAGE COMP-3.                    
      *                       HEURE                                             
           10 PM99-HEURE           PIC X(5).                                    
      *                       DATE MAJ STATUT                                   
CL0610     10 PM99-DMAJ            PIC X(8).                                    
      *                                                                         
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 7       *        
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPM9900                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPM9900-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-DJOUR-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-DJOUR-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-CETAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-CETAT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-WMON-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-WMON-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-HEURE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PM99-HEURE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PM99-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  PM99-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
