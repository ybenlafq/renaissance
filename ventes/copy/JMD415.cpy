      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT JMD415 AU 22/04/2003  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-JMD415.                                                        
            05 NOMETAT-JMD415           PIC X(6) VALUE 'JMD415'.                
            05 RUPTURES-JMD415.                                                 
           10 JMD415-NMAG               PIC X(03).                      007  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 JMD415-SEQUENCE           PIC S9(04) COMP.                010  002
      *--                                                                       
           10 JMD415-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-JMD415.                                                   
           10 JMD415-CFAM               PIC X(05).                      012  005
           10 JMD415-CMARQ              PIC X(05).                      017  005
           10 JMD415-LNMAG              PIC X(20).                      022  020
           10 JMD415-NCODIC             PIC X(07).                      042  007
           10 JMD415-NORIGINE           PIC X(07).                      049  007
           10 JMD415-NSOC               PIC X(03).                      056  003
           10 JMD415-PRMP               PIC S9(09)V9(2) COMP-3.         059  006
           10 JMD415-PRMPFIC            PIC S9(09)V9(2) COMP-3.         065  006
           10 JMD415-QTE                PIC S9(06)      COMP-3.         071  004
           10 JMD415-WSEQFAM            PIC S9(05)      COMP-3.         075  003
           10 JMD415-DDEB               PIC X(08).                      078  008
           10 JMD415-DFIN               PIC X(08).                      086  008
           10 JMD415-DMVT               PIC X(08).                      094  008
            05 FILLER                      PIC X(411).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-JMD415-LONG           PIC S9(4)   COMP  VALUE +101.           
      *                                                                         
      *--                                                                       
        01  DSECT-JMD415-LONG           PIC S9(4) COMP-5  VALUE +101.           
                                                                                
      *}                                                                        
