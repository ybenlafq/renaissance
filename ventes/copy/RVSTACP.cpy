      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE STACP REFERENCEMENT STATUT COMPACTE    *        
      *----------------------------------------------------------------*        
       01  RVSTACP.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  STACP-CTABLEG2    PIC X(15).                                     
           05  STACP-CTABLEG2-REDEF REDEFINES STACP-CTABLEG2.                   
               10  STACP-STATCOMP        PIC X(03).                             
           05  STACP-WTABLEG     PIC X(80).                                     
           05  STACP-WTABLEG-REDEF  REDEFINES STACP-WTABLEG.                    
               10  STACP-WACTIF          PIC X(01).                             
               10  STACP-LIBELLE         PIC X(20).                             
               10  STACP-CAPPRO          PIC X(05).                             
               10  STACP-CEXPO           PIC X(05).                             
               10  STACP-NSEQ            PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  STACP-NSEQ-N         REDEFINES STACP-NSEQ                    
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSTACP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  STACP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  STACP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  STACP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  STACP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
