      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVAC0100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAC0100                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAC0100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAC0100.                                                            
      *}                                                                        
           02  AC01-DVENTE                                                      
               PIC X(0008).                                                     
           02  AC01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  AC01-NLIEU                                                       
               PIC X(0003).                                                     
           02  AC01-QTLM                                                        
               PIC S9(4) COMP-3.                                                
           02  AC01-QELA                                                        
               PIC S9(4) COMP-3.                                                
           02  AC01-QDACEM                                                      
               PIC S9(4) COMP-3.                                                
           02  AC01-CATLM                                                       
               PIC S9(7) COMP-3.                                                
           02  AC01-CAELA                                                       
               PIC S9(7) COMP-3.                                                
           02  AC01-CADACEM                                                     
               PIC S9(7) COMP-3.                                                
           02  AC01-QTRDARTY                                                    
               PIC S9(4) COMP-3.                                                
           02  AC01-QTRDACEM                                                    
               PIC S9(4) COMP-3.                                                
           02  AC01-QENTRE                                                      
               PIC S9(5) COMP-3.                                                
           02  AC01-QVENDEUR                                                    
               PIC S9(2)V9(0001) COMP-3.                                        
           02  AC01-COUVERTN1                                                   
               PIC X(0001).                                                     
           02  AC01-COUVERTJ1                                                   
               PIC X(0001).                                                     
           02  AC01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAC0100                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAC0100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAC0100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QTLM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QTLM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QELA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QELA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-CATLM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-CATLM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-CAELA-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-CAELA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-CADACEM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-CADACEM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QTRDARTY-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QTRDARTY-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QTRDACEM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QTRDACEM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QENTRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QENTRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-QVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-QVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-COUVERTN1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-COUVERTN1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-COUVERTJ1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-COUVERTJ1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AC01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AC01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
