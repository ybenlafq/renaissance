      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTEC05                      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC0500.                                                            
      *}                                                                        
           10 EC05-TIMESTP         PIC X(26).                                   
           10 EC05-WRECENV         PIC X(1).                                    
           10 EC05-XMLDATA.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       49 EC05-XMLDATA-LEN  PIC S9(4) USAGE COMP.                        
      *--                                                                       
              49 EC05-XMLDATA-LEN  PIC S9(4) COMP-5.                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
              49 EC05-XMLDATA-TEXT  PIC X(4034).                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 3       *        
      ******************************************************************        
                                                                                
