      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  W-ENR-FBA800.                                                00010012
           05 FBA800-NSOCIETE        PIC X(3).                          00020012
           05 FILLER                 PIC X.                             00021001
           05 FBA800-NTIERSSAP       PIC X(10).                         00030012
           05 FILLER                 PIC X.                             00031001
           05 FBA800-DEMIS           PIC X(8).                          00040012
           05 FILLER                 PIC X.                             00041001
           05 FBA800-NBA             PIC X(6).                          00050012
           05 FILLER                 PIC X.                             00051001
           05 FBA800-NFACTBA         PIC X(10).                         00060012
           05 FILLER                 PIC X.                             00061001
           05 FBA800-REF             PIC X(14).                         00070012
           05 FILLER                 PIC X.                             00071001
           05 FBA800-PBA             PIC X(9).                          00240012
                                                                                
