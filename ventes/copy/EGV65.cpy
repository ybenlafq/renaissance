      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV65   EGV65                                              00000020
      ***************************************************************** 00000030
       01   EGV65I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000110
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000120
           02 FILLER    PIC X(4).                                       00000130
           02 MTIMJOUI  PIC X(5).                                       00000140
      * MAGASIN CEDANT                                                  00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000160
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MNLIEUORIGI    PIC X(3).                                  00000190
      * LIBELLELIEU CEDANT                                              00000200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUORIGL    COMP PIC S9(4).                            00000210
      *--                                                                       
           02 MLLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLLIEUORIGF    PIC X.                                     00000220
           02 FILLER    PIC X(4).                                       00000230
           02 MLLIEUORIGI    PIC X(20).                                 00000240
      * DATE CREATION                                                   00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREATIONL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDCREATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MDCREATIONF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDCREATIONI    PIC X(8).                                  00000290
      * MAGASIN VENTE                                                   00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVENTEL   COMP PIC S9(4).                            00000310
      *--                                                                       
           02 MNLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNLIEUVENTEF   PIC X.                                     00000320
           02 FILLER    PIC X(4).                                       00000330
           02 MNLIEUVENTEI   PIC X(3).                                  00000340
      * LIBELLE LIEU VENTE                                              00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUVENTEL   COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLLIEUVENTEF   PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLLIEUVENTEI   PIC X(20).                                 00000390
      * DATE DELIVRANCE                                                 00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDLIVL   COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MDDLIVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDLIVF   PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MDDLIVI   PIC X(8).                                       00000440
      * NOM DU CLIENT                                                   00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLNOMI    PIC X(25).                                      00000490
      * NUMERO DE BE                                                    00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBONENLVL     COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MNBONENLVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBONENLVF     PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNBONENLVI     PIC X(7).                                  00000540
      * CODIC                                                           00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNCODICI  PIC X(7).                                       00000590
      * QUANTITE PRE RESERVE                                            00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQVENDUEL      COMP PIC S9(4).                            00000610
      *--                                                                       
           02 MQVENDUEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MQVENDUEF      PIC X.                                     00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MQVENDUEI      PIC X(3).                                  00000640
      * CODE FAMILLE                                                    00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCFAMI    PIC X(5).                                       00000690
      * CODE MARQUE                                                     00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCMARQI   PIC X(5).                                       00000740
      * REFERENCE ARTICLE                                               00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MLREFFOURNI    PIC X(20).                                 00000790
      * SOUS LIEU STOCKAGE                                              00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUORIGL  COMP PIC S9(4).                            00000810
      *--                                                                       
           02 MNSSLIEUORIGL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSSLIEUORIGF  PIC X.                                     00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MNSSLIEUORIGI  PIC X(3).                                  00000840
      * ZONE CMD AIDA                                                   00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MZONCMDI  PIC X(15).                                      00000890
      * MESSAGE ERREUR                                                  00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MLIBERRI  PIC X(58).                                      00000940
      * CODE TRANSACTION                                                00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCODTRAI  PIC X(4).                                       00000990
      * CICS DE TRAVAIL                                                 00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MCICSI    PIC X(5).                                       00001040
      * NETNAME                                                         00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MNETNAMI  PIC X(8).                                       00001090
      * CODE TERMINAL                                                   00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(5).                                       00001140
      ***************************************************************** 00001150
      * SDF: EGV65   EGV65                                              00001160
      ***************************************************************** 00001170
       01   EGV65O REDEFINES EGV65I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
      * DATE DU JOUR                                                    00001200
           02 FILLER    PIC X(2).                                       00001210
           02 MDATJOUA  PIC X.                                          00001220
           02 MDATJOUC  PIC X.                                          00001230
           02 MDATJOUP  PIC X.                                          00001240
           02 MDATJOUH  PIC X.                                          00001250
           02 MDATJOUV  PIC X.                                          00001260
           02 MDATJOUO  PIC X(10).                                      00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
      * MAGASIN CEDANT                                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNLIEUORIGA    PIC X.                                     00001370
           02 MNLIEUORIGC    PIC X.                                     00001380
           02 MNLIEUORIGP    PIC X.                                     00001390
           02 MNLIEUORIGH    PIC X.                                     00001400
           02 MNLIEUORIGV    PIC X.                                     00001410
           02 MNLIEUORIGO    PIC X(3).                                  00001420
      * LIBELLELIEU CEDANT                                              00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MLLIEUORIGA    PIC X.                                     00001450
           02 MLLIEUORIGC    PIC X.                                     00001460
           02 MLLIEUORIGP    PIC X.                                     00001470
           02 MLLIEUORIGH    PIC X.                                     00001480
           02 MLLIEUORIGV    PIC X.                                     00001490
           02 MLLIEUORIGO    PIC X(20).                                 00001500
      * DATE CREATION                                                   00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MDCREATIONA    PIC X.                                     00001530
           02 MDCREATIONC    PIC X.                                     00001540
           02 MDCREATIONP    PIC X.                                     00001550
           02 MDCREATIONH    PIC X.                                     00001560
           02 MDCREATIONV    PIC X.                                     00001570
           02 MDCREATIONO    PIC X(8).                                  00001580
      * MAGASIN VENTE                                                   00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNLIEUVENTEA   PIC X.                                     00001610
           02 MNLIEUVENTEC   PIC X.                                     00001620
           02 MNLIEUVENTEP   PIC X.                                     00001630
           02 MNLIEUVENTEH   PIC X.                                     00001640
           02 MNLIEUVENTEV   PIC X.                                     00001650
           02 MNLIEUVENTEO   PIC X(3).                                  00001660
      * LIBELLE LIEU VENTE                                              00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MLLIEUVENTEA   PIC X.                                     00001690
           02 MLLIEUVENTEC   PIC X.                                     00001700
           02 MLLIEUVENTEP   PIC X.                                     00001710
           02 MLLIEUVENTEH   PIC X.                                     00001720
           02 MLLIEUVENTEV   PIC X.                                     00001730
           02 MLLIEUVENTEO   PIC X(20).                                 00001740
      * DATE DELIVRANCE                                                 00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MDDLIVA   PIC X.                                          00001770
           02 MDDLIVC   PIC X.                                          00001780
           02 MDDLIVP   PIC X.                                          00001790
           02 MDDLIVH   PIC X.                                          00001800
           02 MDDLIVV   PIC X.                                          00001810
           02 MDDLIVO   PIC X(8).                                       00001820
      * NOM DU CLIENT                                                   00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MLNOMA    PIC X.                                          00001850
           02 MLNOMC    PIC X.                                          00001860
           02 MLNOMP    PIC X.                                          00001870
           02 MLNOMH    PIC X.                                          00001880
           02 MLNOMV    PIC X.                                          00001890
           02 MLNOMO    PIC X(25).                                      00001900
      * NUMERO DE BE                                                    00001910
           02 FILLER    PIC X(2).                                       00001920
           02 MNBONENLVA     PIC X.                                     00001930
           02 MNBONENLVC     PIC X.                                     00001940
           02 MNBONENLVP     PIC X.                                     00001950
           02 MNBONENLVH     PIC X.                                     00001960
           02 MNBONENLVV     PIC X.                                     00001970
           02 MNBONENLVO     PIC X(7).                                  00001980
      * CODIC                                                           00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNCODICA  PIC X.                                          00002010
           02 MNCODICC  PIC X.                                          00002020
           02 MNCODICP  PIC X.                                          00002030
           02 MNCODICH  PIC X.                                          00002040
           02 MNCODICV  PIC X.                                          00002050
           02 MNCODICO  PIC X(7).                                       00002060
      * QUANTITE PRE RESERVE                                            00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MQVENDUEA      PIC X.                                     00002090
           02 MQVENDUEC PIC X.                                          00002100
           02 MQVENDUEP PIC X.                                          00002110
           02 MQVENDUEH PIC X.                                          00002120
           02 MQVENDUEV PIC X.                                          00002130
           02 MQVENDUEO      PIC ZZZ.                                   00002140
      * CODE FAMILLE                                                    00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MCFAMA    PIC X.                                          00002170
           02 MCFAMC    PIC X.                                          00002180
           02 MCFAMP    PIC X.                                          00002190
           02 MCFAMH    PIC X.                                          00002200
           02 MCFAMV    PIC X.                                          00002210
           02 MCFAMO    PIC X(5).                                       00002220
      * CODE MARQUE                                                     00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MCMARQA   PIC X.                                          00002250
           02 MCMARQC   PIC X.                                          00002260
           02 MCMARQP   PIC X.                                          00002270
           02 MCMARQH   PIC X.                                          00002280
           02 MCMARQV   PIC X.                                          00002290
           02 MCMARQO   PIC X(5).                                       00002300
      * REFERENCE ARTICLE                                               00002310
           02 FILLER    PIC X(2).                                       00002320
           02 MLREFFOURNA    PIC X.                                     00002330
           02 MLREFFOURNC    PIC X.                                     00002340
           02 MLREFFOURNP    PIC X.                                     00002350
           02 MLREFFOURNH    PIC X.                                     00002360
           02 MLREFFOURNV    PIC X.                                     00002370
           02 MLREFFOURNO    PIC X(20).                                 00002380
      * SOUS LIEU STOCKAGE                                              00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MNSSLIEUORIGA  PIC X.                                     00002410
           02 MNSSLIEUORIGC  PIC X.                                     00002420
           02 MNSSLIEUORIGP  PIC X.                                     00002430
           02 MNSSLIEUORIGH  PIC X.                                     00002440
           02 MNSSLIEUORIGV  PIC X.                                     00002450
           02 MNSSLIEUORIGO  PIC X(3).                                  00002460
      * ZONE CMD AIDA                                                   00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MZONCMDA  PIC X.                                          00002490
           02 MZONCMDC  PIC X.                                          00002500
           02 MZONCMDP  PIC X.                                          00002510
           02 MZONCMDH  PIC X.                                          00002520
           02 MZONCMDV  PIC X.                                          00002530
           02 MZONCMDO  PIC X(15).                                      00002540
      * MESSAGE ERREUR                                                  00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MLIBERRA  PIC X.                                          00002570
           02 MLIBERRC  PIC X.                                          00002580
           02 MLIBERRP  PIC X.                                          00002590
           02 MLIBERRH  PIC X.                                          00002600
           02 MLIBERRV  PIC X.                                          00002610
           02 MLIBERRO  PIC X(58).                                      00002620
      * CODE TRANSACTION                                                00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCODTRAA  PIC X.                                          00002650
           02 MCODTRAC  PIC X.                                          00002660
           02 MCODTRAP  PIC X.                                          00002670
           02 MCODTRAH  PIC X.                                          00002680
           02 MCODTRAV  PIC X.                                          00002690
           02 MCODTRAO  PIC X(4).                                       00002700
      * CICS DE TRAVAIL                                                 00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCICSA    PIC X.                                          00002730
           02 MCICSC    PIC X.                                          00002740
           02 MCICSP    PIC X.                                          00002750
           02 MCICSH    PIC X.                                          00002760
           02 MCICSV    PIC X.                                          00002770
           02 MCICSO    PIC X(5).                                       00002780
      * NETNAME                                                         00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MNETNAMA  PIC X.                                          00002810
           02 MNETNAMC  PIC X.                                          00002820
           02 MNETNAMP  PIC X.                                          00002830
           02 MNETNAMH  PIC X.                                          00002840
           02 MNETNAMV  PIC X.                                          00002850
           02 MNETNAMO  PIC X(8).                                       00002860
      * CODE TERMINAL                                                   00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MSCREENA  PIC X.                                          00002890
           02 MSCREENC  PIC X.                                          00002900
           02 MSCREENP  PIC X.                                          00002910
           02 MSCREENH  PIC X.                                          00002920
           02 MSCREENV  PIC X.                                          00002930
           02 MSCREENO  PIC X(5).                                       00002940
                                                                                
