      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : VENTES                                           *        
      *  TRANSACTION: GV90                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION GV90                   *        
      *  LONGUEUR   : 9096                                             *        
      *                                                                *        
      *  Z-COMMAREA, D' UNE LONGUEUR DE 9096 CARACTERES                *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-GV90-LONG-COMMAREA       PIC S9(4) COMP     VALUE +9096. 00240001
      *--                                                                       
       01  COM-GV90-LONG-COMMAREA       PIC S9(4) COMP-5     VALUE              
                                                                  +9096.        
      *}                                                                        
      *                                                                 00250000
       01  Z-COMMAREA.                                                  00260000
      **** ZONES RESERVEES A AIDA -------------------------------- 100  00280000
           02 FILLER-COM-AIDA           PIC X(100).                     00290000
      *                                                                 00300000
      **** ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020  00310000
           02 COMM-CICS-APPLID          PIC X(08).                      00320000
           02 COMM-CICS-NETNAM          PIC X(08).                      00330000
           02 COMM-CICS-TRANSA          PIC X(04).                      00340000
      *                                                                 00350000
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100  00360000
           02 COMM-DATE-SIECLE          PIC X(02).                      00370000
           02 COMM-DATE-ANNEE           PIC X(02).                      00380000
           02 COMM-DATE-MOIS            PIC X(02).                      00390000
           02 COMM-DATE-JOUR            PIC 9(02).                      00400000
           02 COMM-DATE-QNTA            PIC 9(03).                      00420000
           02 COMM-DATE-QNT0            PIC 9(05).                      00430000
           02 COMM-DATE-BISX            PIC 9(01).                      00450000
           02 COMM-DATE-JSM             PIC 9(01).                      00470000
           02 COMM-DATE-JSM-LC          PIC X(03).                      00490000
           02 COMM-DATE-JSM-LL          PIC X(08).                      00500000
           02 COMM-DATE-MOIS-LC         PIC X(03).                      00520000
           02 COMM-DATE-MOIS-LL         PIC X(08).                      00530000
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                      00550000
           02 COMM-DATE-AAMMJJ          PIC X(06).                      00560000
           02 COMM-DATE-JJMMSSAA        PIC X(08).                      00570000
           02 COMM-DATE-JJMMAA          PIC X(06).                      00580000
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                      00590000
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                      00600000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS              PIC 9(02).                00630000
              05 COMM-DATE-SEMAA              PIC 9(02).                00640000
              05 COMM-DATE-SEMNU              PIC 9(02).                00650000
           02 COMM-DATE-FILLER          PIC X(08).                      00670000
      *                                                                 00680000
      **** ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152  00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.        00700000
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).            00710000
      *                                                                 00720000
      *                                                                 00720000
      *                                                                         
      ****          ZONES APPLICATIVES GV90 -           ************ 505        
           02 COMM-GV90.                                                00000010
              05 COMM-GV90-CPROG           PIC X(5).                    00000100
              05 COMM-GV90-OPT             PIC X(1).                    00000100
              05 COMM-GV90-NSOC            PIC X(3).                    00000100
              05 COMM-GV90-NLIEU           PIC X(3).                    00000100
              05 COMM-GV90-NVENTE          PIC X(7).                    00000100
              05 COMM-GV90-LNOM            PIC X(25).                   00000020
              05 COMM-GV90-CPOSTAL         PIC X(05).                   00000040
              05 COMM-GV90-NTEL            PIC X(10).                   00000030
              05 COMM-GV91.                                                     
                 15 COMM-GV91-NSOC         PIC X(3).                    00000100
                 15 COMM-GV91-NLIEU        PIC X(3).                    00000100
                 15 COMM-GV91-NVENTE       PIC X(7).                    00000100
                 15 COMM-GV91-LNOM         PIC X(25).                   00000020
                 15 COMM-GV91-CPOSTAL      PIC X(05).                   00000040
                 15 COMM-GV91-NTEL         PIC X(10).                   00000030
                 15 COMM-GV91-NOPAGE       PIC 9(03).                   00000050
                 15 COMM-GV91-NBPAGE       PIC 9(03).                   00000060
                 15 COMM-GV91-CODERET      PIC 9(02).                   00000050
                 15 COMM-GV91-ATTR         OCCURS 270                   00000050
                                           PIC X(01).                           
              05 COMM-GV92.                                                     
                 15 COMM-GV92-LNOM         PIC X(25).                   00000020
                 15 COMM-GV92-LPRENOM      PIC X(15).                   00000020
                 15 COMM-GV92-CVOIE        PIC X(05).                   00000020
                 15 COMM-GV92-CTVOIE       PIC X(04).                   00000020
                 15 COMM-GV92-LNOMVOIE     PIC X(21).                   00000020
                 15 COMM-GV92-CPOSTAL      PIC X(05).                   00000040
                 15 COMM-GV92-LCOMMUNE     PIC X(32).                   00000040
                 15 COMM-GV92-NOPAGE       PIC 9(03).                   00000050
                 15 COMM-GV92-NBPAGE       PIC 9(03).                   00000060
                 15 COMM-GV92-CODERET      PIC 9(02).                   00000050
           02 FILLER                       PIC X(8219).                 00000010
      *                                                                         
      *                                                                 00000050
      *                                                                 00000360
      *                                                                 00960000
                                                                                
