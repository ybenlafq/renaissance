      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV01   EAV01                                              00000020
      ***************************************************************** 00000030
       01   EAV01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n� societe                                                      00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n�lieu                                                          00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libelle lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
      * code type utilisation                                           00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPUTILI     PIC X(5).                                  00000330
      * libelle type util                                               00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLTYPUTILI     PIC X(20).                                 00000380
      * code motif d'utilisation                                        00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCMOTIFI  PIC X(5).                                       00000430
      * libelle motif                                                   00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MLMOTIFI  PIC X(20).                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMONTANTL     COMP PIC S9(4).                            00000490
      *--                                                                       
           02 MLMONTANTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLMONTANTF     PIC X.                                     00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MLMONTANTI     PIC X(14).                                 00000520
      * montant avoir                                                   00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAVOIRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MPAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAVOIRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MPAVOIRI  PIC X(10).                                      00000570
      * DEVISE DE REFERENCE                                             00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCDEVISEI      PIC X(3).                                  00000620
      * LIBELLE DEVISE                                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MLDEVISEI      PIC X(5).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAVOIRL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MLAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLAVOIRF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MLAVOIRI  PIC X(26).                                      00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLINFOL   COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MLINFOL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLINFOF   PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MLINFOI   PIC X(12).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINFOL    COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MINFOL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MINFOF    PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MINFOI    PIC X(7).                                       00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMANUL   COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MLMANUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMANUF   PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MLMANUI   PIC X(6).                                       00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMANUL    COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MMANUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MMANUF    PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MMANUI    PIC X(6).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOUVL   COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MLNOUVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOUVF   PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MLNOUVI   PIC X(7).                                       00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOUVL    COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MNOUVL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNOUVF    PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MNOUVI    PIC X(9).                                       00000950
      * n� soc de la vente d'origine                                    00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCVENTEL    COMP PIC S9(4).                            00000970
      *--                                                                       
           02 MNSOCVENTEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNSOCVENTEF    PIC X.                                     00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MNSOCVENTEI    PIC X(3).                                  00001000
      * n�magasin vente d'origine                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUVENTEL   COMP PIC S9(4).                            00001020
      *--                                                                       
           02 MNLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNLIEUVENTEF   PIC X.                                     00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MNLIEUVENTEI   PIC X(3).                                  00001050
      * libelle qui va bien                                             00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUVENTEL   COMP PIC S9(4).                            00001070
      *--                                                                       
           02 MLLIEUVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLLIEUVENTEF   PIC X.                                     00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MLLIEUVENTEI   PIC X(20).                                 00001100
      * n�bon de commande                                               00001110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00001120
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00001130
           02 FILLER    PIC X(4).                                       00001140
           02 MNVENTEI  PIC X(7).                                       00001150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCAISSEL      COMP PIC S9(4).                            00001160
      *--                                                                       
           02 MDCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDCAISSEF      PIC X.                                     00001170
           02 FILLER    PIC X(4).                                       00001180
           02 MDCAISSEI      PIC X(10).                                 00001190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISSEL      COMP PIC S9(4).                            00001200
      *--                                                                       
           02 MNCAISSEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCAISSEF      PIC X.                                     00001210
           02 FILLER    PIC X(4).                                       00001220
           02 MNCAISSEI      PIC X(3).                                  00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00001240
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MNTRANSI  PIC X(8).                                       00001270
      * n� CRI                                                          00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCRIL    COMP PIC S9(4).                                 00001290
      *--                                                                       
           02 MNCRIL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNCRIF    PIC X.                                          00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MNCRII    PIC X(11).                                      00001320
      * code mode de paiement                                           00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMODPAIMTL    COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MCMODPAIMTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCMODPAIMTF    PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MCMODPAIMTI    PIC X(5).                                  00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCMODPAIMTL   COMP PIC S9(4).                            00001380
      *--                                                                       
           02 MLCMODPAIMTL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLCMODPAIMTF   PIC X.                                     00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MLCMODPAIMTI   PIC X(20).                                 00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00001420
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MCTITRENOMI    PIC X(5).                                  00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MLNOMI    PIC X(25).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLPRENOML      COMP PIC S9(4).                            00001500
      *--                                                                       
           02 MLPRENOML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLPRENOMF      PIC X.                                     00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MLPRENOMI      PIC X(15).                                 00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVOIEL   COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MCVOIEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCVOIEF   PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MCVOIEI   PIC X(5).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTVOIEL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCTVOIEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTVOIEF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCTVOIEI  PIC X(4).                                       00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMVOIEL     COMP PIC S9(4).                            00001620
      *--                                                                       
           02 MLNOMVOIEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLNOMVOIEF     PIC X.                                     00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MLNOMVOIEI     PIC X(25).                                 00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOMPADRL      COMP PIC S9(4).                            00001660
      *--                                                                       
           02 MCOMPADRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCOMPADRF      PIC X.                                     00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MCOMPADRI      PIC X(32).                                 00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MBATL     COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MBATL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MBATF     PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MBATI     PIC X(3).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MESCL     COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MESCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MESCF     PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MESCI     PIC X(3).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 METAGEL   COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 METAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 METAGEF   PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 METAGEI   PIC X(3).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPORTEL   COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MPORTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPORTEF   PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MPORTEI   PIC X(3).                                       00001850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00001860
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00001870
           02 FILLER    PIC X(4).                                       00001880
           02 MCPOSTALI      PIC X(5).                                  00001890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00001900
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00001910
           02 FILLER    PIC X(4).                                       00001920
           02 MLCOMMUNEI     PIC X(32).                                 00001930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLBUREAUL      COMP PIC S9(4).                            00001940
      *--                                                                       
           02 MLBUREAUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLBUREAUF      PIC X.                                     00001950
           02 FILLER    PIC X(4).                                       00001960
           02 MLBUREAUI      PIC X(26).                                 00001970
      * TITRE CODIC POUR IMPUT. COMPTA                                  00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITRECODICL   COMP PIC S9(4).                            00001990
      *--                                                                       
           02 MTITRECODICL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MTITRECODICF   PIC X.                                     00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MTITRECODICI   PIC X(39).                                 00002020
      * N� DE CODIC                                                     00002030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODICL   COMP PIC S9(4).                                 00002040
      *--                                                                       
           02 MCODICL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCODICF   PIC X.                                          00002050
           02 FILLER    PIC X(4).                                       00002060
           02 MCODICI   PIC X(7).                                       00002070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00002080
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00002090
           02 FILLER    PIC X(4).                                       00002100
           02 MLIBERRI  PIC X(78).                                      00002110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00002120
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00002130
           02 FILLER    PIC X(4).                                       00002140
           02 MCODTRAI  PIC X(4).                                       00002150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00002160
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00002170
           02 FILLER    PIC X(4).                                       00002180
           02 MCICSI    PIC X(5).                                       00002190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00002200
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002210
           02 FILLER    PIC X(4).                                       00002220
           02 MNETNAMI  PIC X(8).                                       00002230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002240
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002250
           02 FILLER    PIC X(4).                                       00002260
           02 MSCREENI  PIC X(4).                                       00002270
      ***************************************************************** 00002280
      * SDF: EAV01   EAV01                                              00002290
      ***************************************************************** 00002300
       01   EAV01O REDEFINES EAV01I.                                    00002310
           02 FILLER    PIC X(12).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MDATJOUA  PIC X.                                          00002340
           02 MDATJOUC  PIC X.                                          00002350
           02 MDATJOUP  PIC X.                                          00002360
           02 MDATJOUH  PIC X.                                          00002370
           02 MDATJOUV  PIC X.                                          00002380
           02 MDATJOUO  PIC X(10).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MTIMJOUA  PIC X.                                          00002410
           02 MTIMJOUC  PIC X.                                          00002420
           02 MTIMJOUP  PIC X.                                          00002430
           02 MTIMJOUH  PIC X.                                          00002440
           02 MTIMJOUV  PIC X.                                          00002450
           02 MTIMJOUO  PIC X(5).                                       00002460
      * n� societe                                                      00002470
           02 FILLER    PIC X(2).                                       00002480
           02 MNSOCSAISIEA   PIC X.                                     00002490
           02 MNSOCSAISIEC   PIC X.                                     00002500
           02 MNSOCSAISIEP   PIC X.                                     00002510
           02 MNSOCSAISIEH   PIC X.                                     00002520
           02 MNSOCSAISIEV   PIC X.                                     00002530
           02 MNSOCSAISIEO   PIC X(3).                                  00002540
      * n�lieu                                                          00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MNLIEUSAISIEA  PIC X.                                     00002570
           02 MNLIEUSAISIEC  PIC X.                                     00002580
           02 MNLIEUSAISIEP  PIC X.                                     00002590
           02 MNLIEUSAISIEH  PIC X.                                     00002600
           02 MNLIEUSAISIEV  PIC X.                                     00002610
           02 MNLIEUSAISIEO  PIC X(3).                                  00002620
      * libelle lieu                                                    00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MLLIEUSAISIEA  PIC X.                                     00002650
           02 MLLIEUSAISIEC  PIC X.                                     00002660
           02 MLLIEUSAISIEP  PIC X.                                     00002670
           02 MLLIEUSAISIEH  PIC X.                                     00002680
           02 MLLIEUSAISIEV  PIC X.                                     00002690
           02 MLLIEUSAISIEO  PIC X(20).                                 00002700
      * code type utilisation                                           00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MCTYPUTILA     PIC X.                                     00002730
           02 MCTYPUTILC     PIC X.                                     00002740
           02 MCTYPUTILP     PIC X.                                     00002750
           02 MCTYPUTILH     PIC X.                                     00002760
           02 MCTYPUTILV     PIC X.                                     00002770
           02 MCTYPUTILO     PIC X(5).                                  00002780
      * libelle type util                                               00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MLTYPUTILA     PIC X.                                     00002810
           02 MLTYPUTILC     PIC X.                                     00002820
           02 MLTYPUTILP     PIC X.                                     00002830
           02 MLTYPUTILH     PIC X.                                     00002840
           02 MLTYPUTILV     PIC X.                                     00002850
           02 MLTYPUTILO     PIC X(20).                                 00002860
      * code motif d'utilisation                                        00002870
           02 FILLER    PIC X(2).                                       00002880
           02 MCMOTIFA  PIC X.                                          00002890
           02 MCMOTIFC  PIC X.                                          00002900
           02 MCMOTIFP  PIC X.                                          00002910
           02 MCMOTIFH  PIC X.                                          00002920
           02 MCMOTIFV  PIC X.                                          00002930
           02 MCMOTIFO  PIC X(5).                                       00002940
      * libelle motif                                                   00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MLMOTIFA  PIC X.                                          00002970
           02 MLMOTIFC  PIC X.                                          00002980
           02 MLMOTIFP  PIC X.                                          00002990
           02 MLMOTIFH  PIC X.                                          00003000
           02 MLMOTIFV  PIC X.                                          00003010
           02 MLMOTIFO  PIC X(20).                                      00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MLMONTANTA     PIC X.                                     00003040
           02 MLMONTANTC     PIC X.                                     00003050
           02 MLMONTANTP     PIC X.                                     00003060
           02 MLMONTANTH     PIC X.                                     00003070
           02 MLMONTANTV     PIC X.                                     00003080
           02 MLMONTANTO     PIC X(14).                                 00003090
      * montant avoir                                                   00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MPAVOIRA  PIC X.                                          00003120
           02 MPAVOIRC  PIC X.                                          00003130
           02 MPAVOIRP  PIC X.                                          00003140
           02 MPAVOIRH  PIC X.                                          00003150
           02 MPAVOIRV  PIC X.                                          00003160
           02 MPAVOIRO  PIC X(10).                                      00003170
      * DEVISE DE REFERENCE                                             00003180
           02 FILLER    PIC X(2).                                       00003190
           02 MCDEVISEA      PIC X.                                     00003200
           02 MCDEVISEC PIC X.                                          00003210
           02 MCDEVISEP PIC X.                                          00003220
           02 MCDEVISEH PIC X.                                          00003230
           02 MCDEVISEV PIC X.                                          00003240
           02 MCDEVISEO      PIC X(3).                                  00003250
      * LIBELLE DEVISE                                                  00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MLDEVISEA      PIC X.                                     00003280
           02 MLDEVISEC PIC X.                                          00003290
           02 MLDEVISEP PIC X.                                          00003300
           02 MLDEVISEH PIC X.                                          00003310
           02 MLDEVISEV PIC X.                                          00003320
           02 MLDEVISEO      PIC X(5).                                  00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MLAVOIRA  PIC X.                                          00003350
           02 MLAVOIRC  PIC X.                                          00003360
           02 MLAVOIRP  PIC X.                                          00003370
           02 MLAVOIRH  PIC X.                                          00003380
           02 MLAVOIRV  PIC X.                                          00003390
           02 MLAVOIRO  PIC X(26).                                      00003400
           02 FILLER    PIC X(2).                                       00003410
           02 MLINFOA   PIC X.                                          00003420
           02 MLINFOC   PIC X.                                          00003430
           02 MLINFOP   PIC X.                                          00003440
           02 MLINFOH   PIC X.                                          00003450
           02 MLINFOV   PIC X.                                          00003460
           02 MLINFOO   PIC X(12).                                      00003470
           02 FILLER    PIC X(2).                                       00003480
           02 MINFOA    PIC X.                                          00003490
           02 MINFOC    PIC X.                                          00003500
           02 MINFOP    PIC X.                                          00003510
           02 MINFOH    PIC X.                                          00003520
           02 MINFOV    PIC X.                                          00003530
           02 MINFOO    PIC X(7).                                       00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MLMANUA   PIC X.                                          00003560
           02 MLMANUC   PIC X.                                          00003570
           02 MLMANUP   PIC X.                                          00003580
           02 MLMANUH   PIC X.                                          00003590
           02 MLMANUV   PIC X.                                          00003600
           02 MLMANUO   PIC X(6).                                       00003610
           02 FILLER    PIC X(2).                                       00003620
           02 MMANUA    PIC X.                                          00003630
           02 MMANUC    PIC X.                                          00003640
           02 MMANUP    PIC X.                                          00003650
           02 MMANUH    PIC X.                                          00003660
           02 MMANUV    PIC X.                                          00003670
           02 MMANUO    PIC X(6).                                       00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MLNOUVA   PIC X.                                          00003700
           02 MLNOUVC   PIC X.                                          00003710
           02 MLNOUVP   PIC X.                                          00003720
           02 MLNOUVH   PIC X.                                          00003730
           02 MLNOUVV   PIC X.                                          00003740
           02 MLNOUVO   PIC X(7).                                       00003750
           02 FILLER    PIC X(2).                                       00003760
           02 MNOUVA    PIC X.                                          00003770
           02 MNOUVC    PIC X.                                          00003780
           02 MNOUVP    PIC X.                                          00003790
           02 MNOUVH    PIC X.                                          00003800
           02 MNOUVV    PIC X.                                          00003810
           02 MNOUVO    PIC X(9).                                       00003820
      * n� soc de la vente d'origine                                    00003830
           02 FILLER    PIC X(2).                                       00003840
           02 MNSOCVENTEA    PIC X.                                     00003850
           02 MNSOCVENTEC    PIC X.                                     00003860
           02 MNSOCVENTEP    PIC X.                                     00003870
           02 MNSOCVENTEH    PIC X.                                     00003880
           02 MNSOCVENTEV    PIC X.                                     00003890
           02 MNSOCVENTEO    PIC X(3).                                  00003900
      * n�magasin vente d'origine                                       00003910
           02 FILLER    PIC X(2).                                       00003920
           02 MNLIEUVENTEA   PIC X.                                     00003930
           02 MNLIEUVENTEC   PIC X.                                     00003940
           02 MNLIEUVENTEP   PIC X.                                     00003950
           02 MNLIEUVENTEH   PIC X.                                     00003960
           02 MNLIEUVENTEV   PIC X.                                     00003970
           02 MNLIEUVENTEO   PIC X(3).                                  00003980
      * libelle qui va bien                                             00003990
           02 FILLER    PIC X(2).                                       00004000
           02 MLLIEUVENTEA   PIC X.                                     00004010
           02 MLLIEUVENTEC   PIC X.                                     00004020
           02 MLLIEUVENTEP   PIC X.                                     00004030
           02 MLLIEUVENTEH   PIC X.                                     00004040
           02 MLLIEUVENTEV   PIC X.                                     00004050
           02 MLLIEUVENTEO   PIC X(20).                                 00004060
      * n�bon de commande                                               00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MNVENTEA  PIC X.                                          00004090
           02 MNVENTEC  PIC X.                                          00004100
           02 MNVENTEP  PIC X.                                          00004110
           02 MNVENTEH  PIC X.                                          00004120
           02 MNVENTEV  PIC X.                                          00004130
           02 MNVENTEO  PIC X(7).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MDCAISSEA      PIC X.                                     00004160
           02 MDCAISSEC PIC X.                                          00004170
           02 MDCAISSEP PIC X.                                          00004180
           02 MDCAISSEH PIC X.                                          00004190
           02 MDCAISSEV PIC X.                                          00004200
           02 MDCAISSEO      PIC X(10).                                 00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MNCAISSEA      PIC X.                                     00004230
           02 MNCAISSEC PIC X.                                          00004240
           02 MNCAISSEP PIC X.                                          00004250
           02 MNCAISSEH PIC X.                                          00004260
           02 MNCAISSEV PIC X.                                          00004270
           02 MNCAISSEO      PIC X(3).                                  00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MNTRANSA  PIC X.                                          00004300
           02 MNTRANSC  PIC X.                                          00004310
           02 MNTRANSP  PIC X.                                          00004320
           02 MNTRANSH  PIC X.                                          00004330
           02 MNTRANSV  PIC X.                                          00004340
           02 MNTRANSO  PIC X(8).                                       00004350
      * n� CRI                                                          00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MNCRIA    PIC X.                                          00004380
           02 MNCRIC    PIC X.                                          00004390
           02 MNCRIP    PIC X.                                          00004400
           02 MNCRIH    PIC X.                                          00004410
           02 MNCRIV    PIC X.                                          00004420
           02 MNCRIO    PIC X(11).                                      00004430
      * code mode de paiement                                           00004440
           02 FILLER    PIC X(2).                                       00004450
           02 MCMODPAIMTA    PIC X.                                     00004460
           02 MCMODPAIMTC    PIC X.                                     00004470
           02 MCMODPAIMTP    PIC X.                                     00004480
           02 MCMODPAIMTH    PIC X.                                     00004490
           02 MCMODPAIMTV    PIC X.                                     00004500
           02 MCMODPAIMTO    PIC X(5).                                  00004510
           02 FILLER    PIC X(2).                                       00004520
           02 MLCMODPAIMTA   PIC X.                                     00004530
           02 MLCMODPAIMTC   PIC X.                                     00004540
           02 MLCMODPAIMTP   PIC X.                                     00004550
           02 MLCMODPAIMTH   PIC X.                                     00004560
           02 MLCMODPAIMTV   PIC X.                                     00004570
           02 MLCMODPAIMTO   PIC X(20).                                 00004580
           02 FILLER    PIC X(2).                                       00004590
           02 MCTITRENOMA    PIC X.                                     00004600
           02 MCTITRENOMC    PIC X.                                     00004610
           02 MCTITRENOMP    PIC X.                                     00004620
           02 MCTITRENOMH    PIC X.                                     00004630
           02 MCTITRENOMV    PIC X.                                     00004640
           02 MCTITRENOMO    PIC X(5).                                  00004650
           02 FILLER    PIC X(2).                                       00004660
           02 MLNOMA    PIC X.                                          00004670
           02 MLNOMC    PIC X.                                          00004680
           02 MLNOMP    PIC X.                                          00004690
           02 MLNOMH    PIC X.                                          00004700
           02 MLNOMV    PIC X.                                          00004710
           02 MLNOMO    PIC X(25).                                      00004720
           02 FILLER    PIC X(2).                                       00004730
           02 MLPRENOMA      PIC X.                                     00004740
           02 MLPRENOMC PIC X.                                          00004750
           02 MLPRENOMP PIC X.                                          00004760
           02 MLPRENOMH PIC X.                                          00004770
           02 MLPRENOMV PIC X.                                          00004780
           02 MLPRENOMO      PIC X(15).                                 00004790
           02 FILLER    PIC X(2).                                       00004800
           02 MCVOIEA   PIC X.                                          00004810
           02 MCVOIEC   PIC X.                                          00004820
           02 MCVOIEP   PIC X.                                          00004830
           02 MCVOIEH   PIC X.                                          00004840
           02 MCVOIEV   PIC X.                                          00004850
           02 MCVOIEO   PIC X(5).                                       00004860
           02 FILLER    PIC X(2).                                       00004870
           02 MCTVOIEA  PIC X.                                          00004880
           02 MCTVOIEC  PIC X.                                          00004890
           02 MCTVOIEP  PIC X.                                          00004900
           02 MCTVOIEH  PIC X.                                          00004910
           02 MCTVOIEV  PIC X.                                          00004920
           02 MCTVOIEO  PIC X(4).                                       00004930
           02 FILLER    PIC X(2).                                       00004940
           02 MLNOMVOIEA     PIC X.                                     00004950
           02 MLNOMVOIEC     PIC X.                                     00004960
           02 MLNOMVOIEP     PIC X.                                     00004970
           02 MLNOMVOIEH     PIC X.                                     00004980
           02 MLNOMVOIEV     PIC X.                                     00004990
           02 MLNOMVOIEO     PIC X(25).                                 00005000
           02 FILLER    PIC X(2).                                       00005010
           02 MCOMPADRA      PIC X.                                     00005020
           02 MCOMPADRC PIC X.                                          00005030
           02 MCOMPADRP PIC X.                                          00005040
           02 MCOMPADRH PIC X.                                          00005050
           02 MCOMPADRV PIC X.                                          00005060
           02 MCOMPADRO      PIC X(32).                                 00005070
           02 FILLER    PIC X(2).                                       00005080
           02 MBATA     PIC X.                                          00005090
           02 MBATC     PIC X.                                          00005100
           02 MBATP     PIC X.                                          00005110
           02 MBATH     PIC X.                                          00005120
           02 MBATV     PIC X.                                          00005130
           02 MBATO     PIC X(3).                                       00005140
           02 FILLER    PIC X(2).                                       00005150
           02 MESCA     PIC X.                                          00005160
           02 MESCC     PIC X.                                          00005170
           02 MESCP     PIC X.                                          00005180
           02 MESCH     PIC X.                                          00005190
           02 MESCV     PIC X.                                          00005200
           02 MESCO     PIC X(3).                                       00005210
           02 FILLER    PIC X(2).                                       00005220
           02 METAGEA   PIC X.                                          00005230
           02 METAGEC   PIC X.                                          00005240
           02 METAGEP   PIC X.                                          00005250
           02 METAGEH   PIC X.                                          00005260
           02 METAGEV   PIC X.                                          00005270
           02 METAGEO   PIC X(3).                                       00005280
           02 FILLER    PIC X(2).                                       00005290
           02 MPORTEA   PIC X.                                          00005300
           02 MPORTEC   PIC X.                                          00005310
           02 MPORTEP   PIC X.                                          00005320
           02 MPORTEH   PIC X.                                          00005330
           02 MPORTEV   PIC X.                                          00005340
           02 MPORTEO   PIC X(3).                                       00005350
           02 FILLER    PIC X(2).                                       00005360
           02 MCPOSTALA      PIC X.                                     00005370
           02 MCPOSTALC PIC X.                                          00005380
           02 MCPOSTALP PIC X.                                          00005390
           02 MCPOSTALH PIC X.                                          00005400
           02 MCPOSTALV PIC X.                                          00005410
           02 MCPOSTALO      PIC X(5).                                  00005420
           02 FILLER    PIC X(2).                                       00005430
           02 MLCOMMUNEA     PIC X.                                     00005440
           02 MLCOMMUNEC     PIC X.                                     00005450
           02 MLCOMMUNEP     PIC X.                                     00005460
           02 MLCOMMUNEH     PIC X.                                     00005470
           02 MLCOMMUNEV     PIC X.                                     00005480
           02 MLCOMMUNEO     PIC X(32).                                 00005490
           02 FILLER    PIC X(2).                                       00005500
           02 MLBUREAUA      PIC X.                                     00005510
           02 MLBUREAUC PIC X.                                          00005520
           02 MLBUREAUP PIC X.                                          00005530
           02 MLBUREAUH PIC X.                                          00005540
           02 MLBUREAUV PIC X.                                          00005550
           02 MLBUREAUO      PIC X(26).                                 00005560
      * TITRE CODIC POUR IMPUT. COMPTA                                  00005570
           02 FILLER    PIC X(2).                                       00005580
           02 MTITRECODICA   PIC X.                                     00005590
           02 MTITRECODICC   PIC X.                                     00005600
           02 MTITRECODICP   PIC X.                                     00005610
           02 MTITRECODICH   PIC X.                                     00005620
           02 MTITRECODICV   PIC X.                                     00005630
           02 MTITRECODICO   PIC X(39).                                 00005640
      * N� DE CODIC                                                     00005650
           02 FILLER    PIC X(2).                                       00005660
           02 MCODICA   PIC X.                                          00005670
           02 MCODICC   PIC X.                                          00005680
           02 MCODICP   PIC X.                                          00005690
           02 MCODICH   PIC X.                                          00005700
           02 MCODICV   PIC X.                                          00005710
           02 MCODICO   PIC X(7).                                       00005720
           02 FILLER    PIC X(2).                                       00005730
           02 MLIBERRA  PIC X.                                          00005740
           02 MLIBERRC  PIC X.                                          00005750
           02 MLIBERRP  PIC X.                                          00005760
           02 MLIBERRH  PIC X.                                          00005770
           02 MLIBERRV  PIC X.                                          00005780
           02 MLIBERRO  PIC X(78).                                      00005790
           02 FILLER    PIC X(2).                                       00005800
           02 MCODTRAA  PIC X.                                          00005810
           02 MCODTRAC  PIC X.                                          00005820
           02 MCODTRAP  PIC X.                                          00005830
           02 MCODTRAH  PIC X.                                          00005840
           02 MCODTRAV  PIC X.                                          00005850
           02 MCODTRAO  PIC X(4).                                       00005860
           02 FILLER    PIC X(2).                                       00005870
           02 MCICSA    PIC X.                                          00005880
           02 MCICSC    PIC X.                                          00005890
           02 MCICSP    PIC X.                                          00005900
           02 MCICSH    PIC X.                                          00005910
           02 MCICSV    PIC X.                                          00005920
           02 MCICSO    PIC X(5).                                       00005930
           02 FILLER    PIC X(2).                                       00005940
           02 MNETNAMA  PIC X.                                          00005950
           02 MNETNAMC  PIC X.                                          00005960
           02 MNETNAMP  PIC X.                                          00005970
           02 MNETNAMH  PIC X.                                          00005980
           02 MNETNAMV  PIC X.                                          00005990
           02 MNETNAMO  PIC X(8).                                       00006000
           02 FILLER    PIC X(2).                                       00006010
           02 MSCREENA  PIC X.                                          00006020
           02 MSCREENC  PIC X.                                          00006030
           02 MSCREENP  PIC X.                                          00006040
           02 MSCREENH  PIC X.                                          00006050
           02 MSCREENV  PIC X.                                          00006060
           02 MSCREENO  PIC X(4).                                       00006070
                                                                                
