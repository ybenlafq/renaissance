      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INIT DES BR (LORS DU DEMARRAGE)                                 00000020
      ***************************************************************** 00000030
       01   ECL14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CTYPDOC1L      COMP PIC S9(4).                            00000240
      *--                                                                       
           02 CTYPDOC1L COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 CTYPDOC1F      PIC X.                                     00000250
           02 FILLER    PIC X(4).                                       00000260
           02 CTYPDOC1I      PIC X(2).                                  00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COPAR1L   COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 COPAR1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 COPAR1F   PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 COPAR1I   PIC X(5).                                       00000310
           02 MLIGNESI OCCURS   12 TIMES .                              00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 LOPERL  COMP PIC S9(4).                                 00000330
      *--                                                                       
             03 LOPERL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 LOPERF  PIC X.                                          00000340
             03 FILLER  PIC X(4).                                       00000350
             03 LOPERI  PIC X(18).                                      00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CPROGL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 CPROGL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 CPROGF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 CPROGI  PIC X(10).                                      00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COPERL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 COPERL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 COPERF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 COPERI  PIC X(10).                                      00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CTYPDOCL     COMP PIC S9(4).                            00000450
      *--                                                                       
             03 CTYPDOCL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 CTYPDOCF     PIC X.                                     00000460
             03 FILLER  PIC X(4).                                       00000470
             03 CTYPDOCI     PIC X(2).                                  00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 COPARL  COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 COPARL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 COPARF  PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 COPARI  PIC X(5).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 EVENTL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 EVENTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 EVENTF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 EVENTI  PIC X.                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NSEQL   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 NSEQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 NSEQF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 NSEQI   PIC X.                                          00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WHOSTL  COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 WHOSTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 WHOSTF  PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 WHOSTI  PIC X.                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 WEXISTL      COMP PIC S9(4).                            00000650
      *--                                                                       
             03 WEXISTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 WEXISTF      PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 WEXISTI      PIC X.                                     00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NSOCOL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 NSOCOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 NSOCOF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 NSOCOI  PIC X(3).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NLIEUOL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 NLIEUOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 NLIEUOF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 NLIEUOI      PIC X(3).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 NSLIEUOL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 NSLIEUOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 NSLIEUOF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 NSLIEUOI     PIC X(3).                                  00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CLITRTOL     COMP PIC S9(4).                            00000810
      *--                                                                       
             03 CLITRTOL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 CLITRTOF     PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 CLITRTOI     PIC X(3).                                  00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 CDSUPPL      COMP PIC S9(4).                            00000850
      *--                                                                       
             03 CDSUPPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 CDSUPPF      PIC X.                                     00000860
             03 FILLER  PIC X(4).                                       00000870
             03 CDSUPPI      PIC X.                                     00000880
      * MESSAGE ERREUR                                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(79).                                      00000930
      * CODE TRANSACTION                                                00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MZONCMDI  PIC X(15).                                      00001020
      * CICS DE TRAVAIL                                                 00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MCICSI    PIC X(5).                                       00001070
      * NETNAME                                                         00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MNETNAMI  PIC X(8).                                       00001120
      * CODE TERMINAL                                                   00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MSCREENI  PIC X(4).                                       00001170
      ***************************************************************** 00001180
      * INIT DES BR (LORS DU DEMARRAGE)                                 00001190
      ***************************************************************** 00001200
       01   ECL14O REDEFINES ECL14I.                                    00001210
           02 FILLER    PIC X(12).                                      00001220
      * DATE DU JOUR                                                    00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MDATJOUA  PIC X.                                          00001250
           02 MDATJOUC  PIC X.                                          00001260
           02 MDATJOUP  PIC X.                                          00001270
           02 MDATJOUH  PIC X.                                          00001280
           02 MDATJOUV  PIC X.                                          00001290
           02 MDATJOUO  PIC X(10).                                      00001300
      * HEURE                                                           00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MTIMJOUA  PIC X.                                          00001330
           02 MTIMJOUC  PIC X.                                          00001340
           02 MTIMJOUP  PIC X.                                          00001350
           02 MTIMJOUH  PIC X.                                          00001360
           02 MTIMJOUV  PIC X.                                          00001370
           02 MTIMJOUO  PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MPAGEA    PIC X.                                          00001400
           02 MPAGEC    PIC X.                                          00001410
           02 MPAGEP    PIC X.                                          00001420
           02 MPAGEH    PIC X.                                          00001430
           02 MPAGEV    PIC X.                                          00001440
           02 MPAGEO    PIC X(2).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNBPAGESA      PIC X.                                     00001470
           02 MNBPAGESC PIC X.                                          00001480
           02 MNBPAGESP PIC X.                                          00001490
           02 MNBPAGESH PIC X.                                          00001500
           02 MNBPAGESV PIC X.                                          00001510
           02 MNBPAGESO      PIC X(2).                                  00001520
           02 FILLER    PIC X(2).                                       00001530
           02 CTYPDOC1A      PIC X.                                     00001540
           02 CTYPDOC1C PIC X.                                          00001550
           02 CTYPDOC1P PIC X.                                          00001560
           02 CTYPDOC1H PIC X.                                          00001570
           02 CTYPDOC1V PIC X.                                          00001580
           02 CTYPDOC1O      PIC X(2).                                  00001590
           02 FILLER    PIC X(2).                                       00001600
           02 COPAR1A   PIC X.                                          00001610
           02 COPAR1C   PIC X.                                          00001620
           02 COPAR1P   PIC X.                                          00001630
           02 COPAR1H   PIC X.                                          00001640
           02 COPAR1V   PIC X.                                          00001650
           02 COPAR1O   PIC X(5).                                       00001660
           02 MLIGNESO OCCURS   12 TIMES .                              00001670
             03 FILLER       PIC X(2).                                  00001680
             03 LOPERA  PIC X.                                          00001690
             03 LOPERC  PIC X.                                          00001700
             03 LOPERP  PIC X.                                          00001710
             03 LOPERH  PIC X.                                          00001720
             03 LOPERV  PIC X.                                          00001730
             03 LOPERO  PIC X(18).                                      00001740
             03 FILLER       PIC X(2).                                  00001750
             03 CPROGA  PIC X.                                          00001760
             03 CPROGC  PIC X.                                          00001770
             03 CPROGP  PIC X.                                          00001780
             03 CPROGH  PIC X.                                          00001790
             03 CPROGV  PIC X.                                          00001800
             03 CPROGO  PIC X(10).                                      00001810
             03 FILLER       PIC X(2).                                  00001820
             03 COPERA  PIC X.                                          00001830
             03 COPERC  PIC X.                                          00001840
             03 COPERP  PIC X.                                          00001850
             03 COPERH  PIC X.                                          00001860
             03 COPERV  PIC X.                                          00001870
             03 COPERO  PIC X(10).                                      00001880
             03 FILLER       PIC X(2).                                  00001890
             03 CTYPDOCA     PIC X.                                     00001900
             03 CTYPDOCC     PIC X.                                     00001910
             03 CTYPDOCP     PIC X.                                     00001920
             03 CTYPDOCH     PIC X.                                     00001930
             03 CTYPDOCV     PIC X.                                     00001940
             03 CTYPDOCO     PIC X(2).                                  00001950
             03 FILLER       PIC X(2).                                  00001960
             03 COPARA  PIC X.                                          00001970
             03 COPARC  PIC X.                                          00001980
             03 COPARP  PIC X.                                          00001990
             03 COPARH  PIC X.                                          00002000
             03 COPARV  PIC X.                                          00002010
             03 COPARO  PIC X(5).                                       00002020
             03 FILLER       PIC X(2).                                  00002030
             03 EVENTA  PIC X.                                          00002040
             03 EVENTC  PIC X.                                          00002050
             03 EVENTP  PIC X.                                          00002060
             03 EVENTH  PIC X.                                          00002070
             03 EVENTV  PIC X.                                          00002080
             03 EVENTO  PIC X.                                          00002090
             03 FILLER       PIC X(2).                                  00002100
             03 NSEQA   PIC X.                                          00002110
             03 NSEQC   PIC X.                                          00002120
             03 NSEQP   PIC X.                                          00002130
             03 NSEQH   PIC X.                                          00002140
             03 NSEQV   PIC X.                                          00002150
             03 NSEQO   PIC X.                                          00002160
             03 FILLER       PIC X(2).                                  00002170
             03 WHOSTA  PIC X.                                          00002180
             03 WHOSTC  PIC X.                                          00002190
             03 WHOSTP  PIC X.                                          00002200
             03 WHOSTH  PIC X.                                          00002210
             03 WHOSTV  PIC X.                                          00002220
             03 WHOSTO  PIC X.                                          00002230
             03 FILLER       PIC X(2).                                  00002240
             03 WEXISTA      PIC X.                                     00002250
             03 WEXISTC PIC X.                                          00002260
             03 WEXISTP PIC X.                                          00002270
             03 WEXISTH PIC X.                                          00002280
             03 WEXISTV PIC X.                                          00002290
             03 WEXISTO      PIC X.                                     00002300
             03 FILLER       PIC X(2).                                  00002310
             03 NSOCOA  PIC X.                                          00002320
             03 NSOCOC  PIC X.                                          00002330
             03 NSOCOP  PIC X.                                          00002340
             03 NSOCOH  PIC X.                                          00002350
             03 NSOCOV  PIC X.                                          00002360
             03 NSOCOO  PIC X(3).                                       00002370
             03 FILLER       PIC X(2).                                  00002380
             03 NLIEUOA      PIC X.                                     00002390
             03 NLIEUOC PIC X.                                          00002400
             03 NLIEUOP PIC X.                                          00002410
             03 NLIEUOH PIC X.                                          00002420
             03 NLIEUOV PIC X.                                          00002430
             03 NLIEUOO      PIC X(3).                                  00002440
             03 FILLER       PIC X(2).                                  00002450
             03 NSLIEUOA     PIC X.                                     00002460
             03 NSLIEUOC     PIC X.                                     00002470
             03 NSLIEUOP     PIC X.                                     00002480
             03 NSLIEUOH     PIC X.                                     00002490
             03 NSLIEUOV     PIC X.                                     00002500
             03 NSLIEUOO     PIC X(3).                                  00002510
             03 FILLER       PIC X(2).                                  00002520
             03 CLITRTOA     PIC X.                                     00002530
             03 CLITRTOC     PIC X.                                     00002540
             03 CLITRTOP     PIC X.                                     00002550
             03 CLITRTOH     PIC X.                                     00002560
             03 CLITRTOV     PIC X.                                     00002570
             03 CLITRTOO     PIC X(3).                                  00002580
             03 FILLER       PIC X(2).                                  00002590
             03 CDSUPPA      PIC X.                                     00002600
             03 CDSUPPC PIC X.                                          00002610
             03 CDSUPPP PIC X.                                          00002620
             03 CDSUPPH PIC X.                                          00002630
             03 CDSUPPV PIC X.                                          00002640
             03 CDSUPPO      PIC X.                                     00002650
      * MESSAGE ERREUR                                                  00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MLIBERRA  PIC X.                                          00002680
           02 MLIBERRC  PIC X.                                          00002690
           02 MLIBERRP  PIC X.                                          00002700
           02 MLIBERRH  PIC X.                                          00002710
           02 MLIBERRV  PIC X.                                          00002720
           02 MLIBERRO  PIC X(79).                                      00002730
      * CODE TRANSACTION                                                00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MCODTRAA  PIC X.                                          00002760
           02 MCODTRAC  PIC X.                                          00002770
           02 MCODTRAP  PIC X.                                          00002780
           02 MCODTRAH  PIC X.                                          00002790
           02 MCODTRAV  PIC X.                                          00002800
           02 MCODTRAO  PIC X(4).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MZONCMDA  PIC X.                                          00002830
           02 MZONCMDC  PIC X.                                          00002840
           02 MZONCMDP  PIC X.                                          00002850
           02 MZONCMDH  PIC X.                                          00002860
           02 MZONCMDV  PIC X.                                          00002870
           02 MZONCMDO  PIC X(15).                                      00002880
      * CICS DE TRAVAIL                                                 00002890
           02 FILLER    PIC X(2).                                       00002900
           02 MCICSA    PIC X.                                          00002910
           02 MCICSC    PIC X.                                          00002920
           02 MCICSP    PIC X.                                          00002930
           02 MCICSH    PIC X.                                          00002940
           02 MCICSV    PIC X.                                          00002950
           02 MCICSO    PIC X(5).                                       00002960
      * NETNAME                                                         00002970
           02 FILLER    PIC X(2).                                       00002980
           02 MNETNAMA  PIC X.                                          00002990
           02 MNETNAMC  PIC X.                                          00003000
           02 MNETNAMP  PIC X.                                          00003010
           02 MNETNAMH  PIC X.                                          00003020
           02 MNETNAMV  PIC X.                                          00003030
           02 MNETNAMO  PIC X(8).                                       00003040
      * CODE TERMINAL                                                   00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MSCREENA  PIC X.                                          00003070
           02 MSCREENC  PIC X.                                          00003080
           02 MSCREENP  PIC X.                                          00003090
           02 MSCREENH  PIC X.                                          00003100
           02 MSCREENV  PIC X.                                          00003110
           02 MSCREENO  PIC X(4).                                       00003120
                                                                                
