      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV26   EGV26                                              00000020
      ***************************************************************** 00000030
       01   EGV26I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVENTEL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTVENTEF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTVENTEI  PIC X(11).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSURRL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLASSURRL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLASSURRF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLASSURRI      PIC X(8).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MASSURRL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MASSURRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MASSURRF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MASSURRI  PIC X(21).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLASSUREL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLASSUREL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLASSUREF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLASSUREI      PIC X(6).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MASSUREL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MASSUREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MASSUREF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MASSUREI  PIC X(30).                                      00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSINL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLSINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLSINF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLSINI    PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSINL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSINL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSINF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSINI     PIC X(21).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MADRL     COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MADRL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MADRF     PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MADRI     PIC X(30).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDCREAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLDCREAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLDCREAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLDCREAI  PIC X(8).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDCREAL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MDCREAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDCREAF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MDCREAI   PIC X(10).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCPOSTALI      PIC X(5).                                  00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVILLEL   COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MVILLEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVILLEF   PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MVILLEI   PIC X(24).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVENTEL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVENTEF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLVENTEI  PIC X(33).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNVENTEI  PIC X(33).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTORIL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MTOTORIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTOTORIF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MTOTORII  PIC X(11).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTOTGVL   COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MTOTGVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTOTGVF   PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MTOTGVI   PIC X(11).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTORIL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCPTORIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCPTORIF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCPTORII  PIC X(11).                                      00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPTGVL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCPTGVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCPTGVF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCPTGVI   PIC X(11).                                      00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MARFORIL  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MARFORIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MARFORIF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MARFORII  PIC X(11).                                      00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MARFGVL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MARFGVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MARFGVF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MARFGVI   PIC X(11).                                      00001010
           02 MLGVENTI OCCURS   7 TIMES .                               00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MORIL   COMP PIC S9(4).                                 00001030
      *--                                                                       
             03 MORIL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MORIF   PIC X.                                          00001040
             03 FILLER  PIC X(4).                                       00001050
             03 MORII   PIC X(2).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODICL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MCODICL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODICF      PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MCODICI      PIC X(39).                                 00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00001110
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00001120
             03 FILLER  PIC X(4).                                       00001130
             03 MQTEI   PIC X(3).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPVUL   COMP PIC S9(4).                                 00001150
      *--                                                                       
             03 MPVUL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MPVUF   PIC X.                                          00001160
             03 FILLER  PIC X(4).                                       00001170
             03 MPVUI   PIC X(8).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMDDELL      COMP PIC S9(4).                            00001190
      *--                                                                       
             03 MMDDELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MMDDELF      PIC X.                                     00001200
             03 FILLER  PIC X(4).                                       00001210
             03 MMDDELI      PIC X(3).                                  00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDDELL  COMP PIC S9(4).                                 00001230
      *--                                                                       
             03 MDDELL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDDELF  PIC X.                                          00001240
             03 FILLER  PIC X(4).                                       00001250
             03 MDDELI  PIC X(10).                                      00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTOPEL  COMP PIC S9(4).                                 00001270
      *--                                                                       
             03 MTOPEL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MTOPEF  PIC X.                                          00001280
             03 FILLER  PIC X(4).                                       00001290
             03 MTOPEI  PIC X.                                          00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLIBERRI  PIC X(78).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MCODTRAI  PIC X(4).                                       00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001390
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MCICSI    PIC X(5).                                       00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001430
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNETNAMI  PIC X(8).                                       00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MSCREENI  PIC X(4).                                       00001500
      ***************************************************************** 00001510
      * SDF: EGV26   EGV26                                              00001520
      ***************************************************************** 00001530
       01   EGV26O REDEFINES EGV26I.                                    00001540
           02 FILLER    PIC X(12).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MDATJOUA  PIC X.                                          00001570
           02 MDATJOUC  PIC X.                                          00001580
           02 MDATJOUP  PIC X.                                          00001590
           02 MDATJOUH  PIC X.                                          00001600
           02 MDATJOUV  PIC X.                                          00001610
           02 MDATJOUO  PIC X(10).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTIMJOUA  PIC X.                                          00001640
           02 MTIMJOUC  PIC X.                                          00001650
           02 MTIMJOUP  PIC X.                                          00001660
           02 MTIMJOUH  PIC X.                                          00001670
           02 MTIMJOUV  PIC X.                                          00001680
           02 MTIMJOUO  PIC X(5).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MTVENTEA  PIC X.                                          00001710
           02 MTVENTEC  PIC X.                                          00001720
           02 MTVENTEP  PIC X.                                          00001730
           02 MTVENTEH  PIC X.                                          00001740
           02 MTVENTEV  PIC X.                                          00001750
           02 MTVENTEO  PIC X(11).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MPAGEA    PIC X.                                          00001780
           02 MPAGEC    PIC X.                                          00001790
           02 MPAGEP    PIC X.                                          00001800
           02 MPAGEH    PIC X.                                          00001810
           02 MPAGEV    PIC X.                                          00001820
           02 MPAGEO    PIC X(3).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MNBPA     PIC X.                                          00001850
           02 MNBPC     PIC X.                                          00001860
           02 MNBPP     PIC X.                                          00001870
           02 MNBPH     PIC X.                                          00001880
           02 MNBPV     PIC X.                                          00001890
           02 MNBPO     PIC X(3).                                       00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLASSURRA      PIC X.                                     00001920
           02 MLASSURRC PIC X.                                          00001930
           02 MLASSURRP PIC X.                                          00001940
           02 MLASSURRH PIC X.                                          00001950
           02 MLASSURRV PIC X.                                          00001960
           02 MLASSURRO      PIC X(8).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MASSURRA  PIC X.                                          00001990
           02 MASSURRC  PIC X.                                          00002000
           02 MASSURRP  PIC X.                                          00002010
           02 MASSURRH  PIC X.                                          00002020
           02 MASSURRV  PIC X.                                          00002030
           02 MASSURRO  PIC X(21).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLASSUREA      PIC X.                                     00002060
           02 MLASSUREC PIC X.                                          00002070
           02 MLASSUREP PIC X.                                          00002080
           02 MLASSUREH PIC X.                                          00002090
           02 MLASSUREV PIC X.                                          00002100
           02 MLASSUREO      PIC X(6).                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MASSUREA  PIC X.                                          00002130
           02 MASSUREC  PIC X.                                          00002140
           02 MASSUREP  PIC X.                                          00002150
           02 MASSUREH  PIC X.                                          00002160
           02 MASSUREV  PIC X.                                          00002170
           02 MASSUREO  PIC X(30).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLSINA    PIC X.                                          00002200
           02 MLSINC    PIC X.                                          00002210
           02 MLSINP    PIC X.                                          00002220
           02 MLSINH    PIC X.                                          00002230
           02 MLSINV    PIC X.                                          00002240
           02 MLSINO    PIC X(8).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MSINA     PIC X.                                          00002270
           02 MSINC     PIC X.                                          00002280
           02 MSINP     PIC X.                                          00002290
           02 MSINH     PIC X.                                          00002300
           02 MSINV     PIC X.                                          00002310
           02 MSINO     PIC X(21).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MADRA     PIC X.                                          00002340
           02 MADRC     PIC X.                                          00002350
           02 MADRP     PIC X.                                          00002360
           02 MADRH     PIC X.                                          00002370
           02 MADRV     PIC X.                                          00002380
           02 MADRO     PIC X(30).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MLDCREAA  PIC X.                                          00002410
           02 MLDCREAC  PIC X.                                          00002420
           02 MLDCREAP  PIC X.                                          00002430
           02 MLDCREAH  PIC X.                                          00002440
           02 MLDCREAV  PIC X.                                          00002450
           02 MLDCREAO  PIC X(8).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDCREAA   PIC X.                                          00002480
           02 MDCREAC   PIC X.                                          00002490
           02 MDCREAP   PIC X.                                          00002500
           02 MDCREAH   PIC X.                                          00002510
           02 MDCREAV   PIC X.                                          00002520
           02 MDCREAO   PIC X(10).                                      00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MCPOSTALA      PIC X.                                     00002550
           02 MCPOSTALC PIC X.                                          00002560
           02 MCPOSTALP PIC X.                                          00002570
           02 MCPOSTALH PIC X.                                          00002580
           02 MCPOSTALV PIC X.                                          00002590
           02 MCPOSTALO      PIC X(5).                                  00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MVILLEA   PIC X.                                          00002620
           02 MVILLEC   PIC X.                                          00002630
           02 MVILLEP   PIC X.                                          00002640
           02 MVILLEH   PIC X.                                          00002650
           02 MVILLEV   PIC X.                                          00002660
           02 MVILLEO   PIC X(24).                                      00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MLVENTEA  PIC X.                                          00002690
           02 MLVENTEC  PIC X.                                          00002700
           02 MLVENTEP  PIC X.                                          00002710
           02 MLVENTEH  PIC X.                                          00002720
           02 MLVENTEV  PIC X.                                          00002730
           02 MLVENTEO  PIC X(33).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNVENTEA  PIC X.                                          00002760
           02 MNVENTEC  PIC X.                                          00002770
           02 MNVENTEP  PIC X.                                          00002780
           02 MNVENTEH  PIC X.                                          00002790
           02 MNVENTEV  PIC X.                                          00002800
           02 MNVENTEO  PIC X(33).                                      00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MTOTORIA  PIC X.                                          00002830
           02 MTOTORIC  PIC X.                                          00002840
           02 MTOTORIP  PIC X.                                          00002850
           02 MTOTORIH  PIC X.                                          00002860
           02 MTOTORIV  PIC X.                                          00002870
           02 MTOTORIO  PIC X(11).                                      00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MTOTGVA   PIC X.                                          00002900
           02 MTOTGVC   PIC X.                                          00002910
           02 MTOTGVP   PIC X.                                          00002920
           02 MTOTGVH   PIC X.                                          00002930
           02 MTOTGVV   PIC X.                                          00002940
           02 MTOTGVO   PIC X(11).                                      00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MCPTORIA  PIC X.                                          00002970
           02 MCPTORIC  PIC X.                                          00002980
           02 MCPTORIP  PIC X.                                          00002990
           02 MCPTORIH  PIC X.                                          00003000
           02 MCPTORIV  PIC X.                                          00003010
           02 MCPTORIO  PIC X(11).                                      00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MCPTGVA   PIC X.                                          00003040
           02 MCPTGVC   PIC X.                                          00003050
           02 MCPTGVP   PIC X.                                          00003060
           02 MCPTGVH   PIC X.                                          00003070
           02 MCPTGVV   PIC X.                                          00003080
           02 MCPTGVO   PIC X(11).                                      00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MARFORIA  PIC X.                                          00003110
           02 MARFORIC  PIC X.                                          00003120
           02 MARFORIP  PIC X.                                          00003130
           02 MARFORIH  PIC X.                                          00003140
           02 MARFORIV  PIC X.                                          00003150
           02 MARFORIO  PIC X(11).                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MARFGVA   PIC X.                                          00003180
           02 MARFGVC   PIC X.                                          00003190
           02 MARFGVP   PIC X.                                          00003200
           02 MARFGVH   PIC X.                                          00003210
           02 MARFGVV   PIC X.                                          00003220
           02 MARFGVO   PIC X(11).                                      00003230
           02 MLGVENTO OCCURS   7 TIMES .                               00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MORIA   PIC X.                                          00003260
             03 MORIC   PIC X.                                          00003270
             03 MORIP   PIC X.                                          00003280
             03 MORIH   PIC X.                                          00003290
             03 MORIV   PIC X.                                          00003300
             03 MORIO   PIC X(2).                                       00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MCODICA      PIC X.                                     00003330
             03 MCODICC PIC X.                                          00003340
             03 MCODICP PIC X.                                          00003350
             03 MCODICH PIC X.                                          00003360
             03 MCODICV PIC X.                                          00003370
             03 MCODICO      PIC X(39).                                 00003380
             03 FILLER       PIC X(2).                                  00003390
             03 MQTEA   PIC X.                                          00003400
             03 MQTEC   PIC X.                                          00003410
             03 MQTEP   PIC X.                                          00003420
             03 MQTEH   PIC X.                                          00003430
             03 MQTEV   PIC X.                                          00003440
             03 MQTEO   PIC X(3).                                       00003450
             03 FILLER       PIC X(2).                                  00003460
             03 MPVUA   PIC X.                                          00003470
             03 MPVUC   PIC X.                                          00003480
             03 MPVUP   PIC X.                                          00003490
             03 MPVUH   PIC X.                                          00003500
             03 MPVUV   PIC X.                                          00003510
             03 MPVUO   PIC X(8).                                       00003520
             03 FILLER       PIC X(2).                                  00003530
             03 MMDDELA      PIC X.                                     00003540
             03 MMDDELC PIC X.                                          00003550
             03 MMDDELP PIC X.                                          00003560
             03 MMDDELH PIC X.                                          00003570
             03 MMDDELV PIC X.                                          00003580
             03 MMDDELO      PIC X(3).                                  00003590
             03 FILLER       PIC X(2).                                  00003600
             03 MDDELA  PIC X.                                          00003610
             03 MDDELC  PIC X.                                          00003620
             03 MDDELP  PIC X.                                          00003630
             03 MDDELH  PIC X.                                          00003640
             03 MDDELV  PIC X.                                          00003650
             03 MDDELO  PIC X(10).                                      00003660
             03 FILLER       PIC X(2).                                  00003670
             03 MTOPEA  PIC X.                                          00003680
             03 MTOPEC  PIC X.                                          00003690
             03 MTOPEP  PIC X.                                          00003700
             03 MTOPEH  PIC X.                                          00003710
             03 MTOPEV  PIC X.                                          00003720
             03 MTOPEO  PIC X.                                          00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLIBERRA  PIC X.                                          00003750
           02 MLIBERRC  PIC X.                                          00003760
           02 MLIBERRP  PIC X.                                          00003770
           02 MLIBERRH  PIC X.                                          00003780
           02 MLIBERRV  PIC X.                                          00003790
           02 MLIBERRO  PIC X(78).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MCODTRAA  PIC X.                                          00003820
           02 MCODTRAC  PIC X.                                          00003830
           02 MCODTRAP  PIC X.                                          00003840
           02 MCODTRAH  PIC X.                                          00003850
           02 MCODTRAV  PIC X.                                          00003860
           02 MCODTRAO  PIC X(4).                                       00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MCICSA    PIC X.                                          00003890
           02 MCICSC    PIC X.                                          00003900
           02 MCICSP    PIC X.                                          00003910
           02 MCICSH    PIC X.                                          00003920
           02 MCICSV    PIC X.                                          00003930
           02 MCICSO    PIC X(5).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MNETNAMA  PIC X.                                          00003960
           02 MNETNAMC  PIC X.                                          00003970
           02 MNETNAMP  PIC X.                                          00003980
           02 MNETNAMH  PIC X.                                          00003990
           02 MNETNAMV  PIC X.                                          00004000
           02 MNETNAMO  PIC X(8).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MSCREENA  PIC X.                                          00004030
           02 MSCREENC  PIC X.                                          00004040
           02 MSCREENP  PIC X.                                          00004050
           02 MSCREENH  PIC X.                                          00004060
           02 MSCREENV  PIC X.                                          00004070
           02 MSCREENO  PIC X(4).                                       00004080
                                                                                
