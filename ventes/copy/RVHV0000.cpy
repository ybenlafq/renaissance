      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV0000                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0000                         
      **********************************************************                
       01  RVHV0000.                                                            
           02  HV00-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV00-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV00-CMODDEL                                                     
               PIC X(0003).                                                     
           02  HV00-DOPER                                                       
               PIC X(0006).                                                     
           02  HV00-QMVT                                                        
               PIC S9(11) COMP-3.                                               
           02  HV00-PCA                                                         
               PIC S9(13)V9(0002) COMP-3.                                       
           02  HV00-CFAM                                                        
               PIC X(0005).                                                     
           02  HV00-WSEQFAM                                                     
               PIC S9(5) COMP-3.                                                
           02  HV00-CMARKETING                                                  
               PIC X(0005).                                                     
           02  HV00-CVMARKETING                                                 
               PIC X(0005).                                                     
           02  HV00-WSEQED                                                      
               PIC X(0002).                                                     
           02  HV00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV0000                                  
      **********************************************************                
       01  RVHV0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-DOPER-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-DOPER-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-QMVT-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-QMVT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-WSEQFAM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-WSEQFAM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-CMARKETING-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-CMARKETING-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-CVMARKETING-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-CVMARKETING-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-WSEQED-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV00-WSEQED-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  HV00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
