      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ECL00   ECL00                                              00000020
      ***************************************************************** 00000030
       01   ECL00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MZONCMDI  PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCIETEI     PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPDOCL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MCTYPDOCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCTYPDOCF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCTYPDOCI      PIC X(2).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMDOCL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNUMDOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNUMDOCF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNUMDOCI  PIC X(7).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMUTATIONL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MNMUTATIONL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNMUTATIONF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNMUTATIONI    PIC X(7).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCMAGL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNSOCMAGL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNSOCMAGF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNSOCMAGI      PIC X(3).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MNMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNMAGF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNMAGI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MNVENTEI  PIC X(7).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCLIEUL     COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MNSOCLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCLIEUF     PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNSOCLIEUI     PIC X(3).                                  00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMAGASINL     COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MNMAGASINL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNMAGASINF     PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNMAGASINI     PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNJOURL   COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNJOURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNJOURF   PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNJOURI   PIC X(2).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMOISL   COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNMOISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNMOISF   PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNMOISI   PIC X(2).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNANNEEL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNANNEEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNANNEEF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNANNEEI  PIC X(4).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MLIBERRI  PIC X(78).                                      00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCODTRAI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCICSI    PIC X(5).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MNETNAMI  PIC X(8).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MSCREENI  PIC X(4).                                       00000890
      ***************************************************************** 00000900
      * SDF: ECL00   ECL00                                              00000910
      ***************************************************************** 00000920
       01   ECL00O REDEFINES ECL00I.                                    00000930
           02 FILLER    PIC X(12).                                      00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MDATJOUA  PIC X.                                          00000960
           02 MDATJOUC  PIC X.                                          00000970
           02 MDATJOUP  PIC X.                                          00000980
           02 MDATJOUH  PIC X.                                          00000990
           02 MDATJOUV  PIC X.                                          00001000
           02 MDATJOUO  PIC X(10).                                      00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MTIMJOUA  PIC X.                                          00001030
           02 MTIMJOUC  PIC X.                                          00001040
           02 MTIMJOUP  PIC X.                                          00001050
           02 MTIMJOUH  PIC X.                                          00001060
           02 MTIMJOUV  PIC X.                                          00001070
           02 MTIMJOUO  PIC X(5).                                       00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MZONCMDA  PIC X.                                          00001100
           02 MZONCMDC  PIC X.                                          00001110
           02 MZONCMDP  PIC X.                                          00001120
           02 MZONCMDH  PIC X.                                          00001130
           02 MZONCMDV  PIC X.                                          00001140
           02 MZONCMDO  PIC X(2).                                       00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNSOCIETEA     PIC X.                                     00001170
           02 MNSOCIETEC     PIC X.                                     00001180
           02 MNSOCIETEP     PIC X.                                     00001190
           02 MNSOCIETEH     PIC X.                                     00001200
           02 MNSOCIETEV     PIC X.                                     00001210
           02 MNSOCIETEO     PIC X(3).                                  00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MNLIEUA   PIC X.                                          00001240
           02 MNLIEUC   PIC X.                                          00001250
           02 MNLIEUP   PIC X.                                          00001260
           02 MNLIEUH   PIC X.                                          00001270
           02 MNLIEUV   PIC X.                                          00001280
           02 MNLIEUO   PIC X(3).                                       00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MCTYPDOCA      PIC X.                                     00001310
           02 MCTYPDOCC PIC X.                                          00001320
           02 MCTYPDOCP PIC X.                                          00001330
           02 MCTYPDOCH PIC X.                                          00001340
           02 MCTYPDOCV PIC X.                                          00001350
           02 MCTYPDOCO      PIC X(2).                                  00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MNUMDOCA  PIC X.                                          00001380
           02 MNUMDOCC  PIC X.                                          00001390
           02 MNUMDOCP  PIC X.                                          00001400
           02 MNUMDOCH  PIC X.                                          00001410
           02 MNUMDOCV  PIC X.                                          00001420
           02 MNUMDOCO  PIC X(7).                                       00001430
           02 FILLER    PIC X(2).                                       00001440
           02 MNMUTATIONA    PIC X.                                     00001450
           02 MNMUTATIONC    PIC X.                                     00001460
           02 MNMUTATIONP    PIC X.                                     00001470
           02 MNMUTATIONH    PIC X.                                     00001480
           02 MNMUTATIONV    PIC X.                                     00001490
           02 MNMUTATIONO    PIC X(7).                                  00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNSOCMAGA      PIC X.                                     00001520
           02 MNSOCMAGC PIC X.                                          00001530
           02 MNSOCMAGP PIC X.                                          00001540
           02 MNSOCMAGH PIC X.                                          00001550
           02 MNSOCMAGV PIC X.                                          00001560
           02 MNSOCMAGO      PIC X(3).                                  00001570
           02 FILLER    PIC X(2).                                       00001580
           02 MNMAGA    PIC X.                                          00001590
           02 MNMAGC    PIC X.                                          00001600
           02 MNMAGP    PIC X.                                          00001610
           02 MNMAGH    PIC X.                                          00001620
           02 MNMAGV    PIC X.                                          00001630
           02 MNMAGO    PIC X(3).                                       00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNVENTEA  PIC X.                                          00001660
           02 MNVENTEC  PIC X.                                          00001670
           02 MNVENTEP  PIC X.                                          00001680
           02 MNVENTEH  PIC X.                                          00001690
           02 MNVENTEV  PIC X.                                          00001700
           02 MNVENTEO  PIC X(7).                                       00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MNSOCLIEUA     PIC X.                                     00001730
           02 MNSOCLIEUC     PIC X.                                     00001740
           02 MNSOCLIEUP     PIC X.                                     00001750
           02 MNSOCLIEUH     PIC X.                                     00001760
           02 MNSOCLIEUV     PIC X.                                     00001770
           02 MNSOCLIEUO     PIC X(3).                                  00001780
           02 FILLER    PIC X(2).                                       00001790
           02 MNMAGASINA     PIC X.                                     00001800
           02 MNMAGASINC     PIC X.                                     00001810
           02 MNMAGASINP     PIC X.                                     00001820
           02 MNMAGASINH     PIC X.                                     00001830
           02 MNMAGASINV     PIC X.                                     00001840
           02 MNMAGASINO     PIC X(3).                                  00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MNJOURA   PIC X.                                          00001870
           02 MNJOURC   PIC X.                                          00001880
           02 MNJOURP   PIC X.                                          00001890
           02 MNJOURH   PIC X.                                          00001900
           02 MNJOURV   PIC X.                                          00001910
           02 MNJOURO   PIC X(2).                                       00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MNMOISA   PIC X.                                          00001940
           02 MNMOISC   PIC X.                                          00001950
           02 MNMOISP   PIC X.                                          00001960
           02 MNMOISH   PIC X.                                          00001970
           02 MNMOISV   PIC X.                                          00001980
           02 MNMOISO   PIC X(2).                                       00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MNANNEEA  PIC X.                                          00002010
           02 MNANNEEC  PIC X.                                          00002020
           02 MNANNEEP  PIC X.                                          00002030
           02 MNANNEEH  PIC X.                                          00002040
           02 MNANNEEV  PIC X.                                          00002050
           02 MNANNEEO  PIC X(4).                                       00002060
           02 FILLER    PIC X(2).                                       00002070
           02 MLIBERRA  PIC X.                                          00002080
           02 MLIBERRC  PIC X.                                          00002090
           02 MLIBERRP  PIC X.                                          00002100
           02 MLIBERRH  PIC X.                                          00002110
           02 MLIBERRV  PIC X.                                          00002120
           02 MLIBERRO  PIC X(78).                                      00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MCODTRAA  PIC X.                                          00002150
           02 MCODTRAC  PIC X.                                          00002160
           02 MCODTRAP  PIC X.                                          00002170
           02 MCODTRAH  PIC X.                                          00002180
           02 MCODTRAV  PIC X.                                          00002190
           02 MCODTRAO  PIC X(4).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCICSA    PIC X.                                          00002220
           02 MCICSC    PIC X.                                          00002230
           02 MCICSP    PIC X.                                          00002240
           02 MCICSH    PIC X.                                          00002250
           02 MCICSV    PIC X.                                          00002260
           02 MCICSO    PIC X(5).                                       00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MNETNAMA  PIC X.                                          00002290
           02 MNETNAMC  PIC X.                                          00002300
           02 MNETNAMP  PIC X.                                          00002310
           02 MNETNAMH  PIC X.                                          00002320
           02 MNETNAMV  PIC X.                                          00002330
           02 MNETNAMO  PIC X(8).                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MSCREENA  PIC X.                                          00002360
           02 MSCREENC  PIC X.                                          00002370
           02 MSCREENP  PIC X.                                          00002380
           02 MSCREENH  PIC X.                                          00002390
           02 MSCREENV  PIC X.                                          00002400
           02 MSCREENO  PIC X(4).                                       00002410
                                                                                
