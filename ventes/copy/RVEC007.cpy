      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:03 >
      *----------------------------------------------------------------*
      *    VUE DE LA SOUS-TABLE EC007 MAGASIN LIVRANT ECOM  MGI        *
      *----------------------------------------------------------------*
       01  RVEC007.
           05  EC007-CTABLEG2    PIC X(15).
           05  EC007-CTABLEG2-REDEF REDEFINES EC007-CTABLEG2.
               10  EC007-NSOCPLT         PIC X(03).
               10  EC007-NLIEUPLT        PIC X(03).
           05  EC007-WTABLEG     PIC X(80).
           05  EC007-WTABLEG-REDEF  REDEFINES EC007-WTABLEG.
               10  EC007-NSOCMAG         PIC X(03).
               10  EC007-NLIEUMAG        PIC X(03).
      *----------------------------------------------------------------*
      *    FLAGS DE LA VUE                                             *
      *----------------------------------------------------------------*
       01  RVEC007-FLAGS.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  EC007-CTABLEG2-F  PIC S9(4)  COMP.
      *--
           05  EC007-CTABLEG2-F  PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  EC007-WTABLEG-F   PIC S9(4)  COMP.
      *
      *--
           05  EC007-WTABLEG-F   PIC S9(4) COMP-5.
      
      *}
