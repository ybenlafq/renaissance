      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVBA2100                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA2100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA2100.                                                            
           02  BA21-CTIERSBA                                                    
               PIC X(0006).                                                     
           02  BA21-DANNEE                                                      
               PIC X(0004).                                                     
           02  BA21-PCAREEL                                                     
               PIC S9(9)V9(0002) COMP-3.                                        
           02  BA21-PCATHEO                                                     
               PIC S9(9)V9(0002) COMP-3.                                        
           02  BA21-QBA                                                         
               PIC S9(7) COMP-3.                                                
           02  BA21-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA2100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA2100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-CTIERSBA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-CTIERSBA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-DANNEE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-DANNEE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-PCAREEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-PCAREEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-PCATHEO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-PCATHEO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-QBA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-QBA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA21-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA21-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
