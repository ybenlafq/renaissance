      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV20   EAV20                                              00000020
      ***************************************************************** 00000030
       01   EAV21I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�societe saisie                                                00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n� lieu saisie                                                  00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libell� lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(20).                                 00000280
      * n�lieu                                                          00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNLIEUI   PIC X(3).                                       00000330
      * libelle lieu                                                    00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLLIEUI   PIC X(20).                                      00000380
      * n�avoir sur 6                                                   00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MNAVOIRI  PIC X(6).                                       00000430
      * code type avoir                                                 00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPAVOIRL    COMP PIC S9(4).                            00000450
      *--                                                                       
           02 MCTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTYPAVOIRF    PIC X.                                     00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCTYPAVOIRI    PIC X(5).                                  00000480
      * libelle type avoir                                              00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPAVOIRL    COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPAVOIRF    PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLTYPAVOIRI    PIC X(20).                                 00000530
      * n�societe utilisatrice                                          00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCUTILL     COMP PIC S9(4).                            00000550
      *--                                                                       
           02 MNSOCUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCUTILF     PIC X.                                     00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MNSOCUTILI     PIC X(3).                                  00000580
      * n�lieu utilisateur                                              00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUUTILL    COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MNLIEUUTILL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUUTILF    PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MNLIEUUTILI    PIC X(3).                                  00000630
      * libelle lieu utilisateur                                        00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUUTILL    COMP PIC S9(4).                            00000650
      *--                                                                       
           02 MLLIEUUTILL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLLIEUUTILF    PIC X.                                     00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLLIEUUTILI    PIC X(20).                                 00000680
      * code type d'utilisation                                         00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000700
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MCTYPUTILI     PIC X(5).                                  00000730
      * libelle type d'utilisation                                      00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000750
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MLTYPUTILI     PIC X(20).                                 00000780
      * code motif                                                      00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000800
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MCMOTIFI  PIC X(5).                                       00000830
      * libelle motif                                                   00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MLMOTIFI  PIC X(20).                                      00000880
      * montant                                                         00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTANTL     COMP PIC S9(4).                            00000900
      *--                                                                       
           02 MPMONTANTL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPMONTANTF     PIC X.                                     00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MPMONTANTI     PIC X(10).                                 00000930
      * code devise                                                     00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000950
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCDEVISEI      PIC X(3).                                  00000980
      * libelle devise                                                  00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00001000
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MLDEVISEI      PIC X(10).                                 00001030
      * date d'emission                                                 00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MDEMISI   PIC X(10).                                      00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MLIBERRI  PIC X(78).                                      00001120
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001130
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001140
           02 FILLER    PIC X(4).                                       00001150
           02 MCODTRAI  PIC X(4).                                       00001160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001170
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001180
           02 FILLER    PIC X(4).                                       00001190
           02 MCICSI    PIC X(5).                                       00001200
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001210
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001220
           02 FILLER    PIC X(4).                                       00001230
           02 MNETNAMI  PIC X(8).                                       00001240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001250
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001260
           02 FILLER    PIC X(4).                                       00001270
           02 MSCREENI  PIC X(4).                                       00001280
      ***************************************************************** 00001290
      * SDF: EAV20   EAV20                                              00001300
      ***************************************************************** 00001310
       01   EAV21O REDEFINES EAV21I.                                    00001320
           02 FILLER    PIC X(12).                                      00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MDATJOUA  PIC X.                                          00001350
           02 MDATJOUC  PIC X.                                          00001360
           02 MDATJOUP  PIC X.                                          00001370
           02 MDATJOUH  PIC X.                                          00001380
           02 MDATJOUV  PIC X.                                          00001390
           02 MDATJOUO  PIC X(10).                                      00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MTIMJOUA  PIC X.                                          00001420
           02 MTIMJOUC  PIC X.                                          00001430
           02 MTIMJOUP  PIC X.                                          00001440
           02 MTIMJOUH  PIC X.                                          00001450
           02 MTIMJOUV  PIC X.                                          00001460
           02 MTIMJOUO  PIC X(5).                                       00001470
      * n�societe saisie                                                00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNSOCSAISIEA   PIC X.                                     00001500
           02 MNSOCSAISIEC   PIC X.                                     00001510
           02 MNSOCSAISIEP   PIC X.                                     00001520
           02 MNSOCSAISIEH   PIC X.                                     00001530
           02 MNSOCSAISIEV   PIC X.                                     00001540
           02 MNSOCSAISIEO   PIC X(3).                                  00001550
      * n� lieu saisie                                                  00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNLIEUSAISIEA  PIC X.                                     00001580
           02 MNLIEUSAISIEC  PIC X.                                     00001590
           02 MNLIEUSAISIEP  PIC X.                                     00001600
           02 MNLIEUSAISIEH  PIC X.                                     00001610
           02 MNLIEUSAISIEV  PIC X.                                     00001620
           02 MNLIEUSAISIEO  PIC X(3).                                  00001630
      * libell� lieu                                                    00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MLLIEUSAISIEA  PIC X.                                     00001660
           02 MLLIEUSAISIEC  PIC X.                                     00001670
           02 MLLIEUSAISIEP  PIC X.                                     00001680
           02 MLLIEUSAISIEH  PIC X.                                     00001690
           02 MLLIEUSAISIEV  PIC X.                                     00001700
           02 MLLIEUSAISIEO  PIC X(20).                                 00001710
      * n�lieu                                                          00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNLIEUA   PIC X.                                          00001740
           02 MNLIEUC   PIC X.                                          00001750
           02 MNLIEUP   PIC X.                                          00001760
           02 MNLIEUH   PIC X.                                          00001770
           02 MNLIEUV   PIC X.                                          00001780
           02 MNLIEUO   PIC X(3).                                       00001790
      * libelle lieu                                                    00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLLIEUA   PIC X.                                          00001820
           02 MLLIEUC   PIC X.                                          00001830
           02 MLLIEUP   PIC X.                                          00001840
           02 MLLIEUH   PIC X.                                          00001850
           02 MLLIEUV   PIC X.                                          00001860
           02 MLLIEUO   PIC X(20).                                      00001870
      * n�avoir sur 6                                                   00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNAVOIRA  PIC X.                                          00001900
           02 MNAVOIRC  PIC X.                                          00001910
           02 MNAVOIRP  PIC X.                                          00001920
           02 MNAVOIRH  PIC X.                                          00001930
           02 MNAVOIRV  PIC X.                                          00001940
           02 MNAVOIRO  PIC X(6).                                       00001950
      * code type avoir                                                 00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MCTYPAVOIRA    PIC X.                                     00001980
           02 MCTYPAVOIRC    PIC X.                                     00001990
           02 MCTYPAVOIRP    PIC X.                                     00002000
           02 MCTYPAVOIRH    PIC X.                                     00002010
           02 MCTYPAVOIRV    PIC X.                                     00002020
           02 MCTYPAVOIRO    PIC X(5).                                  00002030
      * libelle type avoir                                              00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MLTYPAVOIRA    PIC X.                                     00002060
           02 MLTYPAVOIRC    PIC X.                                     00002070
           02 MLTYPAVOIRP    PIC X.                                     00002080
           02 MLTYPAVOIRH    PIC X.                                     00002090
           02 MLTYPAVOIRV    PIC X.                                     00002100
           02 MLTYPAVOIRO    PIC X(20).                                 00002110
      * n�societe utilisatrice                                          00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MNSOCUTILA     PIC X.                                     00002140
           02 MNSOCUTILC     PIC X.                                     00002150
           02 MNSOCUTILP     PIC X.                                     00002160
           02 MNSOCUTILH     PIC X.                                     00002170
           02 MNSOCUTILV     PIC X.                                     00002180
           02 MNSOCUTILO     PIC X(3).                                  00002190
      * n�lieu utilisateur                                              00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MNLIEUUTILA    PIC X.                                     00002220
           02 MNLIEUUTILC    PIC X.                                     00002230
           02 MNLIEUUTILP    PIC X.                                     00002240
           02 MNLIEUUTILH    PIC X.                                     00002250
           02 MNLIEUUTILV    PIC X.                                     00002260
           02 MNLIEUUTILO    PIC X(3).                                  00002270
      * libelle lieu utilisateur                                        00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MLLIEUUTILA    PIC X.                                     00002300
           02 MLLIEUUTILC    PIC X.                                     00002310
           02 MLLIEUUTILP    PIC X.                                     00002320
           02 MLLIEUUTILH    PIC X.                                     00002330
           02 MLLIEUUTILV    PIC X.                                     00002340
           02 MLLIEUUTILO    PIC X(20).                                 00002350
      * code type d'utilisation                                         00002360
           02 FILLER    PIC X(2).                                       00002370
           02 MCTYPUTILA     PIC X.                                     00002380
           02 MCTYPUTILC     PIC X.                                     00002390
           02 MCTYPUTILP     PIC X.                                     00002400
           02 MCTYPUTILH     PIC X.                                     00002410
           02 MCTYPUTILV     PIC X.                                     00002420
           02 MCTYPUTILO     PIC X(5).                                  00002430
      * libelle type d'utilisation                                      00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLTYPUTILA     PIC X.                                     00002460
           02 MLTYPUTILC     PIC X.                                     00002470
           02 MLTYPUTILP     PIC X.                                     00002480
           02 MLTYPUTILH     PIC X.                                     00002490
           02 MLTYPUTILV     PIC X.                                     00002500
           02 MLTYPUTILO     PIC X(20).                                 00002510
      * code motif                                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCMOTIFA  PIC X.                                          00002540
           02 MCMOTIFC  PIC X.                                          00002550
           02 MCMOTIFP  PIC X.                                          00002560
           02 MCMOTIFH  PIC X.                                          00002570
           02 MCMOTIFV  PIC X.                                          00002580
           02 MCMOTIFO  PIC X(5).                                       00002590
      * libelle motif                                                   00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLMOTIFA  PIC X.                                          00002620
           02 MLMOTIFC  PIC X.                                          00002630
           02 MLMOTIFP  PIC X.                                          00002640
           02 MLMOTIFH  PIC X.                                          00002650
           02 MLMOTIFV  PIC X.                                          00002660
           02 MLMOTIFO  PIC X(20).                                      00002670
      * montant                                                         00002680
           02 FILLER    PIC X(2).                                       00002690
           02 MPMONTANTA     PIC X.                                     00002700
           02 MPMONTANTC     PIC X.                                     00002710
           02 MPMONTANTP     PIC X.                                     00002720
           02 MPMONTANTH     PIC X.                                     00002730
           02 MPMONTANTV     PIC X.                                     00002740
           02 MPMONTANTO     PIC X(10).                                 00002750
      * code devise                                                     00002760
           02 FILLER    PIC X(2).                                       00002770
           02 MCDEVISEA      PIC X.                                     00002780
           02 MCDEVISEC PIC X.                                          00002790
           02 MCDEVISEP PIC X.                                          00002800
           02 MCDEVISEH PIC X.                                          00002810
           02 MCDEVISEV PIC X.                                          00002820
           02 MCDEVISEO      PIC X(3).                                  00002830
      * libelle devise                                                  00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MLDEVISEA      PIC X.                                     00002860
           02 MLDEVISEC PIC X.                                          00002870
           02 MLDEVISEP PIC X.                                          00002880
           02 MLDEVISEH PIC X.                                          00002890
           02 MLDEVISEV PIC X.                                          00002900
           02 MLDEVISEO      PIC X(10).                                 00002910
      * date d'emission                                                 00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MDEMISA   PIC X.                                          00002940
           02 MDEMISC   PIC X.                                          00002950
           02 MDEMISP   PIC X.                                          00002960
           02 MDEMISH   PIC X.                                          00002970
           02 MDEMISV   PIC X.                                          00002980
           02 MDEMISO   PIC X(10).                                      00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MLIBERRA  PIC X.                                          00003010
           02 MLIBERRC  PIC X.                                          00003020
           02 MLIBERRP  PIC X.                                          00003030
           02 MLIBERRH  PIC X.                                          00003040
           02 MLIBERRV  PIC X.                                          00003050
           02 MLIBERRO  PIC X(78).                                      00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCODTRAA  PIC X.                                          00003080
           02 MCODTRAC  PIC X.                                          00003090
           02 MCODTRAP  PIC X.                                          00003100
           02 MCODTRAH  PIC X.                                          00003110
           02 MCODTRAV  PIC X.                                          00003120
           02 MCODTRAO  PIC X(4).                                       00003130
           02 FILLER    PIC X(2).                                       00003140
           02 MCICSA    PIC X.                                          00003150
           02 MCICSC    PIC X.                                          00003160
           02 MCICSP    PIC X.                                          00003170
           02 MCICSH    PIC X.                                          00003180
           02 MCICSV    PIC X.                                          00003190
           02 MCICSO    PIC X(5).                                       00003200
           02 FILLER    PIC X(2).                                       00003210
           02 MNETNAMA  PIC X.                                          00003220
           02 MNETNAMC  PIC X.                                          00003230
           02 MNETNAMP  PIC X.                                          00003240
           02 MNETNAMH  PIC X.                                          00003250
           02 MNETNAMV  PIC X.                                          00003260
           02 MNETNAMO  PIC X(8).                                       00003270
           02 FILLER    PIC X(2).                                       00003280
           02 MSCREENA  PIC X.                                          00003290
           02 MSCREENC  PIC X.                                          00003300
           02 MSCREENP  PIC X.                                          00003310
           02 MSCREENH  PIC X.                                          00003320
           02 MSCREENV  PIC X.                                          00003330
           02 MSCREENO  PIC X(4).                                       00003340
                                                                                
