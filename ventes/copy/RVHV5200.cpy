      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5200                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5200                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5200.                                                            
           02  HV52-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV52-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV52-DCAISSE                                                     
               PIC X(0008).                                                     
           02  HV52-NCAISSE                                                     
               PIC X(0003).                                                     
           02  HV52-NTRANS                                                      
               PIC X(0004).                                                     
           02  HV52-NLIGNE                                                      
               PIC X(0002).                                                     
           02  HV52-NCODIC                                                      
               PIC X(0007).                                                     
           02  HV52-CRAYON                                                      
               PIC X(0002).                                                     
           02  HV52-LREF                                                        
               PIC X(0020).                                                     
           02  HV52-CPSE                                                        
               PIC X(0001).                                                     
           02  HV52-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  HV52-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV52-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV52-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV52-CFORCE                                                      
               PIC X(0001).                                                     
           02  HV52-TAUXREM                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  HV52-PRABAIS                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV52-PPRIME                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV52-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  HV52-CVENDEUR                                                    
               PIC X(0006).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5200                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5200-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-CRAYON-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-CRAYON-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-LREF-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-LREF-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-CPSE-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-CPSE-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-CFORCE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-CFORCE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-TAUXREM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-TAUXREM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-PRABAIS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-PRABAIS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-PPRIME-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-PPRIME-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV52-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV52-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
