      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5301                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5301.                                                            
           02  HV53-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV53-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV53-DCAISSE                                                     
               PIC X(0008).                                                     
           02  HV53-NCAISSE                                                     
               PIC X(0003).                                                     
           02  HV53-NTRANS                                                      
               PIC X(0004).                                                     
           02  HV53-NSEQPAIMT                                                   
               PIC X(0001).                                                     
           02  HV53-CMODPAIMT                                                   
               PIC X(0005).                                                     
           02  HV53-PREGLTVTE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV53-PREGLRENDU                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV53-NREGLTVTE                                                   
               PIC X(0010).                                                     
           02  HV53-DEVREGL                                                     
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5301                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5301-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NSEQPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NSEQPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-PREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-PREGLRENDU-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-PREGLRENDU-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-NREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV53-DEVREGL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV53-DEVREGL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
