      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *---SGR----------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDIRO INTéGRATION DES ARO              *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDIRO.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDIRO.                                                             
      *}                                                                        
           05  TDIRO-CTABLEG2    PIC X(15).                                     
           05  TDIRO-CTABLEG2-REDEF REDEFINES TDIRO-CTABLEG2.                   
               10  TDIRO-TYPDOSS         PIC X(05).                             
               10  TDIRO-WARO1           PIC X(05).                             
               10  TDIRO-WARO2           PIC X(05).                             
           05  TDIRO-WTABLEG     PIC X(80).                                     
           05  TDIRO-WTABLEG-REDEF  REDEFINES TDIRO-WTABLEG.                    
               10  TDIRO-WARO3           PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDIRO-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDIRO-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIRO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDIRO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIRO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDIRO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
