      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDIMP IMPRESSION TD00                  *        
      *----------------------------------------------------------------*        
       01  RVTDIMP.                                                             
           05  TDIMP-CTABLEG2    PIC X(15).                                     
           05  TDIMP-CTABLEG2-REDEF REDEFINES TDIMP-CTABLEG2.                   
               10  TDIMP-TYPDOSS         PIC X(05).                             
               10  TDIMP-LIEUACT         PIC X(01).                             
               10  TDIMP-NOPTION         PIC X(02).                             
           05  TDIMP-WTABLEG     PIC X(80).                                     
           05  TDIMP-WTABLEG-REDEF  REDEFINES TDIMP-WTABLEG.                    
               10  TDIMP-SELECTIO        PIC X(10).                             
               10  TDIMP-LIBSEL          PIC X(20).                             
               10  TDIMP-PARAM           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDIMP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIMP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDIMP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDIMP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDIMP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
