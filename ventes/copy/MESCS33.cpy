      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ ENVOI MISE A JOUR DE PRIX EN LOCAL                   
      *    EN MEME TEMPS QUE L'ENVOI DE L'ETIQUETTE DE PRIX                     
      *****************************************************************         
       01  TCS33-MESSAGE.                                                       
      * CETTE PARTIE EST OBLIGATOIRE POUR LE TRAITEMENT AS400                   
      *                                                 MCBMDMQ000              
           05 TCS33-ENTETE.                                                     
              10 TCS33-FICHIER                    PIC   X(10).                  
              10 TCS33-NB-OCC                     PIC   9(05).                  
              10 TCS33-MAJ                        PIC   X(01).                  
      * A PARTIR D'ICI, C'EST APPLICATIF                                        
           05 TCS33-MESS.                                                       
              10  W-MSG-PRIME.                                                  
                  15  WS-NCODIC                   PIC   X(07).                  
                  15  WS-PRIX                     PIC   X(09).                  
                  15  WS-PRIME-EX                 PIC   X(01).                  
                  15  W-ZONES-GG40.                                             
                      20  WS-GG40-NSOCIETE        PIC   X(03).                  
                      20  WS-GG40-NLIEU           PIC   X(03).                  
                      20  WS-GG40-NCODIC          PIC   X(07).                  
                      20  WS-GG40-DEFFET          PIC   X(08).                  
                      20  WS-GG40-DFEFFET         PIC   X(08).                  
                      20  WS-GG40-PRIME      PIC   S9(05)V99 COMP-3.            
                      20  WS-GG40-NCONC           PIC   X(04).                  
                  15  W-ZONES-GA56.                                             
                      20  WS-GA56-NSOCIETE        PIC   X(03).                  
                      20  WS-GA56-ZPRIX           PIC   X(02).                  
                      20  WS-GA56-NCODIC          PIC   X(07).                  
                      20  WS-GA56-DEFFET          PIC   X(08).                  
                      20  WS-GA56-PRIX            PIC   X(09).                  
                      20  WS-GA56-PRIME           PIC   X(07).                  
                      20  WS-GA56-PCOMVOL         PIC   X(07).                  
      * ON AJOUTE LES ZONES LIEU ET PRIX EXCEPTIONNEL O/N                       
      * SERVENT UNIQUEMENT AUX LIEUX CENTRALISES                                
      * LIEU = '000' SI ZPRIX = PRIX ZONE DE PRIX                               
      * ET PRIX EXCEPTIONNEL = 'N'                                              
      * SINON LIEU = MAGASIN                                                    
      * CES DEUX ZONES NE SONT PAS ENVOYEES                                     
      * SUR LES MAGASINS NON CENTRALISES                                        
              10  W-MSG-PRIME-C.                                                
                  15  WS-PRIX-EX                  PIC   X(01).                  
                  15  WS-LIEU                     PIC   X(03).                  
                  15  WS-ZPRIX                    PIC   X(02).                  
                  15  WS-DEFFET-PRIX              PIC   X(08).                  
                  15  WS-DFEFFET-PRIX             PIC   X(08).                  
                                                                                
