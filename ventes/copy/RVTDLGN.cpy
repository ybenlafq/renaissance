      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDLGN LIGNE AIDE � LA SAISIE TD        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDLGN.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDLGN.                                                             
      *}                                                                        
           05  TDLGN-CTABLEG2    PIC X(15).                                     
           05  TDLGN-CTABLEG2-REDEF REDEFINES TDLGN-CTABLEG2.                   
               10  TDLGN-TYPDOSS         PIC X(05).                             
               10  TDLGN-ECRAN           PIC X(05).                             
               10  TDLGN-NOPTION         PIC X(02).                             
           05  TDLGN-WTABLEG     PIC X(80).                                     
           05  TDLGN-WTABLEG-REDEF  REDEFINES TDLGN-WTABLEG.                    
               10  TDLGN-LIBLGN          PIC X(54).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTDLGN-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTDLGN-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLGN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDLGN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLGN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDLGN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
