      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVEC0301                      *        
      ******************************************************************        
       01  RVEC0301.                                                            
           10 EC03-NSOCIETE        PIC X(3).                                    
           10 EC03-NLIEU           PIC X(3).                                    
           10 EC03-NVENTE          PIC X(7).                                    
           10 EC03-DVENTE          PIC X(8).                                    
           10 EC03-NSEQNQ          PIC S9(5)V USAGE COMP-3.                     
           10 EC03-CCOLIS          PIC X(15).                                   
           10 EC03-DEXP            PIC X(8).                                    
           10 EC03-DSYST           PIC S9(13)V USAGE COMP-3.                    
           10 EC03-CCDDEL          PIC X(8).                                    
           10 EC03-CCHDEL          PIC X(4).                                    
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 10      *        
      ******************************************************************        
                                                                                
