      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : N M D                                            *        
      *  TITRE      : COMMAREA DU MODULE STANDARD MSL22.               *        
      *  LONGUEUR   : 65                                               *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
       01  COMM-MSL22.                                                          
           05  COMM-MSL22-NSOC              PIC X(03).                          
           05  COMM-MSL22-NLIEU             PIC X(03).                          
           05  COMM-MSL22-NVENTE            PIC X(07).                          
           05  COMM-MSL22-CODRET            PIC X(02).                          
           05  COMM-MSL22-ERREUR            PIC X(50).                          
      *                                                                 00230000
                                                                                
