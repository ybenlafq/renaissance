      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------ *          
      * ZONE DE COMMUNICATION POUR MGV81 MODULE APPEL GENERIX POUR   *          
      * EMD DACEM                                                    *          
      *                                                              *          
      *                                                              *          
      * ------------------------------------------------------------ *          
       01 COMMAREA-GV81.                                                        
      * - PROGRAMME APPELANT.                                                   
          02 COMM-GV81-CPROG           PIC X(06).                               
             88 COMM-GV81-CONTROLE                VALUE 'MGV21'.                
             88 COMM-GV81-VENTE                   VALUE 'MGV22'.                
             88 COMM-GV81-ENCAISSEMENT            VALUE 'MBS45'.                
      * - DATE DU JOUR.                                                         
          02 COMM-GV81-SSAAMMJJ        PIC X(08).                               
      * - SOC LIEU VENTE                                                        
          02 COMM-GV81-NSOCMODIF       PIC X(03).                               
          02 COMM-GV81-NLIEUMODIF      PIC X(03).                               
          02 COMM-GV81-NSOCIETE        PIC X(03).                               
          02 COMM-GV81-NLIEU           PIC X(03).                               
          02 COMM-GV81-NVENTE          PIC X(07).                               
      * - CODE FONCTION PRINCIPAL CORRESPODANT � L'APPELANT.                    
          02 COMM-GV81-CFONCTION       PIC X(01).                               
             88 COMM-GV81-INTERRO                  VALUE 'I'.                   
             88 COMM-GV81-COMMANDE                 VALUE 'C'.                   
      * -NBRE DE PRODUITS DANS APPEL                                            
          02 COMM-GV81-NB-ITEM         PIC 9(02)   VALUE 0.                     
      * - NBRE DE PRODUITS MAX POUR APPEL                                       
          02 COMM-GV81-NB-ITEM-MAX     PIC 9(02)   VALUE 40.                    
      * - RETOUR / ARRET TRAITEMENT D�S PREMI�RE ERREUR.                        
          02 COMM-GV81-OUT.                                                     
             03 COMM-GV81-IDRET        PIC 9(02).                               
             03 COMM-GV81-ERREUR       PIC X(04).                               
             03 COMM-GV81-LERREUR      PIC X(58).                               
             03 COMM-GV81-CRET         PIC X(04).                               
             03 COMM-GV81-LRET         PIC X(58).                               
          02 COMM-GV81-APPLI           PIC X(20000).                            
      * ------------------------------------------------------------- *         
      * ZONE DE COMMUNICATION INTERROGATION                           *         
      *                                                               *         
      * ------------------------------------------------------------- *         
          02 COMM-GV81-INTERROGATION REDEFINES COMM-GV81-APPLI.                 
      * - (IN) ACTION SUR LIGNE DE VENTE.                                       
              03 COMM-GV81-INT-CACTION        PIC X(02).                        
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-GV81-INT-CREATION             VALUE 'AJ' , ' '.        
      *--                                                                       
                 88 COMM-GV81-INT-CREATION             VALUE 'AJ'   ' '.        
      *}                                                                        
                 88 COMM-GV81-INT-ANNULEE              VALUE 'AN'.              
                 88 COMM-GV81-INT-MODIFICATION         VALUE 'MO'.              
      * - (IN/OUT) LIEU DEMANDEUR                                               
              03 COMM-GV81-INT-NSOCD          PIC X(03).                        
              03 COMM-GV81-INT-NLIEUD         PIC X(03).                        
      * - (OUT) DEPOT SERVEUR                                                   
              03 COMM-GV81-INT-NSOCDEP        PIC X(03).                        
              03 COMM-GV81-INT-NLIEUDEP       PIC X(03).                        
      * - (IN) CODIC                                                            
              03 COMM-GV81-INT-NCODIC         PIC X(07).                        
      * - (IN) SEQUENCE UNIQUE PRODUIT                                          
              03 COMM-GV81-INT-NSEQNQ         PIC S9(5)  COMP-3.                
      * - (IN) QTE � COMMANDER INITIALE                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GV81-INT-QVENDUE-I      PIC S9(06) COMP.                  
      *--                                                                       
              03 COMM-GV81-INT-QVENDUE-I      PIC S9(06) COMP-5.                
      *}                                                                        
      * - (IN) QTE � COMMANDER                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       03 COMM-GV81-INT-QVENDUE        PIC S9(06) COMP.                  
      *--                                                                       
              03 COMM-GV81-INT-QVENDUE        PIC S9(06) COMP-5.                
      *}                                                                        
      * - (IN) DATE DELIVRANCE INITIALE                                         
              03 COMM-GV81-INT-DDELIV-I       PIC X(08).                        
      * - (IN) DATE DELIVRANCE                                                  
              03 COMM-GV81-INT-DDELIV         PIC X(08).                        
      * - (IN) MODE DE D�LIVRANCE INITIAL                                       
              03 COMM-GV81-INT-CMODDEL-I      PIC X(03).                        
      * - (IN) MODE DE D�LIVRANCE                                               
              03 COMM-GV81-INT-CMODDEL        PIC X(03).                        
      * - (IN) SOUS TYPE MODE DE D�LIVRANCE INITIAL                             
              03 COMM-GV81-INT-MTYPE-I        PIC X(01).                        
      * - (IN) SOUS TYPE MODE DE D�LIVRANCE                                     
              03 COMM-GV81-INT-MTYPE          PIC X(01).                        
      * - (OUT) GV11-WCQERESF ETAT DE LA LIGNE (A,E,Z)                          
              03 COMM-GV81-INT-ETAT-WCQERESF  PIC X(01).                        
                 88 COMM-GV81-INT-ETAT-ATTENC        VALUE 'E'.                 
                 88 COMM-GV81-INT-ETAT-ATTMUT        VALUE 'A'.                 
                 88 COMM-GV81-INT-ETAT-Z             VALUE 'Z'.                 
                 88 COMM-GV81-INT-ETAT-INDEFINI      VALUE ' '.                 
      * - Ligne de vente consid�r�e mut�e suite � r�ception de mutation.        
              03 COMM-GV81-INT-WINTMAJ        PIC X(01).                        
                    88 COMM-GV81-INT-MUTEE                VALUE 'M'.            
      * - Ligne encaiss�e.                                                      
              03 COMM-GV81-INT-DATENC         PIC X(08).                        
                    88 COMM-GV81-INT-ENCAISSEE-NON        VALUE SPACES.         
      * - (OUT) DATE DE COMMANDE GENERIX + GVDEL                                
              03 COMM-GV81-INT-DCDE           PIC X(08).                        
      * - (OUT) INFO SI COMMANDE DACEM D�J� EXISTANTE.                          
              03 COMM-GV81-INT-INFO           PIC X(35).                        
                03 FILLER REDEFINES COMM-GV81-INT-INFO.                         
                   05 INT-INFO-GIE        PIC X(09).                            
                   05 FILLER              PIC X(01).                            
                   05 INT-INFO-CDE        PIC X(06).                            
                   05 FILLER              PIC X(01).                            
                   05 INT-INFO-NSOC       PIC X(03).                            
                   05 INT-INFO-NLIEU      PIC X(03).                            
                   05 FILLER              PIC X(12).                            
      * - (IN) NUMERO DE TRANSACTION DE PAIEMENT                                
               03 COMM-GV81-INT-NTRANS    PIC S9(8) COMP-3.                     
               03 FILLER                  PIC X(10253).                         
      * ------------------------------------------------------------- *         
      * ZONE DE COMMUNICATION COMMANDE                                *         
      *                                                               *         
      * ------------------------------------------------------------- *         
          02 COMM-GV81-T-COMMANDE      REDEFINES COMM-GV81-APPLI.               
             03 COMM-GV81-TAB-COMMANDE OCCURS 41.                               
      * - (IN) ACTION SUR LIGNE DE VENTE.                                       
                04 COMM-GV81-COM-CACTION      PIC X(02).                        
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-GV81-COM-CREATION             VALUE 'AJ' , ' '.        
      *--                                                                       
                 88 COMM-GV81-COM-CREATION             VALUE 'AJ'   ' '.        
      *}                                                                        
                 88 COMM-GV81-COM-ANNULEE              VALUE 'AN'.              
                 88 COMM-GV81-COM-MODIFICATION         VALUE 'MO'.              
      * - (IN) LIEU DEMANDEUR                                                   
                04 COMM-GV81-COM-NSOCD      PIC X(03).                          
                04 COMM-GV81-COM-NLIEUD     PIC X(03).                          
                04 COMM-GV81-COM-NSOCDEP    PIC X(03).                          
                04 COMM-GV81-COM-NLIEUDEP   PIC X(03).                          
      * - (  ) POUR INTERROGATION GENERIX NE PAS ALIMENTER                      
                04 COMM-GV81-COM-IDK        PIC 9(02).                          
      * - (IN) ID LIGNE                                                         
                04 COMM-GV81-COM-IDLIGNE    PIC 9(02).                          
      * - (IN) CODIC � COMMANDER                                                
                04 COMM-GV81-COM-NCODIC     PIC X(07).                          
      * - (IN) ID LIGNE                                                         
                04 COMM-GV81-COM-NSEQNQ     PIC S9(5)  COMP-3.                  
      * - (IN) QTE � COMMANDER INITIALE                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GV81-COM-QVENDUE-I  PIC S9(06) COMP.                    
      *--                                                                       
                04 COMM-GV81-COM-QVENDUE-I  PIC S9(06) COMP-5.                  
      *}                                                                        
      * - (IN) QTE � COMMANDER                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *         04 COMM-GV81-COM-QVENDUE    PIC S9(06) COMP.                    
      *--                                                                       
                04 COMM-GV81-COM-QVENDUE    PIC S9(06) COMP-5.                  
      *}                                                                        
      * - (IN) DATE DELIVRANCE INITIALE                                         
                04 COMM-GV81-COM-DDELIV-I   PIC X(08).                          
      * - (IN) DATE DELIVRANCE                                                  
                04 COMM-GV81-COM-DDELIV     PIC X(08).                          
      * - (IN) MODE DE D�LIVRANCE INITIAL                                       
                04 COMM-GV81-COM-CMODDEL-I  PIC X(03).                          
      * - (IN) MODE DE D�LIVRANCE                                               
                04 COMM-GV81-COM-CMODDEL    PIC X(03).                          
      * - (IN) SOUS TYPE MODE DE D�LIVRANCE INITIAL                             
                04 COMM-GV81-COM-MTYPE-I    PIC X(01).                          
      * - (IN) SOUS TYPE MODE DE D�LIVRANCE                                     
                04 COMM-GV81-COM-MTYPE      PIC X(01).                          
      * - (OUT)ETAT DE LA LIGNE (Z) FORCE RECOMMANDE                            
                04 COMM-GV81-COM-ETAT-WCQERESF   PIC X(01).                     
                    88 COMM-GV81-COM-ETAT-ATTMUT          VALUE 'A'.            
                    88 COMM-GV81-COM-ETAT-Z               VALUE 'Z' .           
                    88 COMM-GV81-COM-ETAT-ATTENC          VALUE 'E'.            
                    88 COMM-GV81-COM-ETAT-SUPPRES         VALUE 'S'.            
                    88 COMM-GV81-COM-ETAT-INDEFINI        VALUE ' '.            
                04 COMM-GV81-COM-ETAT-WCQERESF-I PIC X(01).                     
                    88 COMM-GV81-COM-ETAT-i-ATTMUT        VALUE 'A'.            
                    88 COMM-GV81-COM-ETAT-i-Z             VALUE 'Z' .           
                    88 COMM-GV81-COM-ETAT-i-ATTENC        VALUE 'E'.            
                    88 COMM-GV81-COM-ETAT-i-INDEFINI      VALUE ' '.            
      * - (IN) Ligne de vente mut�e suite � r�ception de mutation.              
                04 COMM-GV81-COM-WINTMAJ    PIC X(01).                          
                    88 COMM-GV81-COM-MUTEE                VALUE 'M'.            
      * - (IN) Ligne encaiss�e.                                                 
                04 COMM-GV81-COM-DATENC     PIC X(08).                          
                    88 COMM-GV81-COM-ENCAISSEE-NON        VALUE SPACES.         
      * - (OUT) DATE DE COMMANDE GENERIX                                        
                04 COMM-GV81-COM-DCDE     PIC X(08).                            
      * - (OUT) INFO SUR COMMANDE DACEM SI D�J� EXISTANTE.                      
                04 COMM-GV81-COM-INFO     PIC X(35).                            
                04 FILLER REDEFINES COMM-GV81-COM-INFO.                         
                   05 COM-INFO-GIE        PIC X(09).                            
                   05 FILLER              PIC X(01).                            
                   05 COM-INFO-CDE        PIC X(06).                            
                   05 FILLER              PIC X(01).                            
                   05 COM-INFO-NSOC       PIC X(03).                            
                   05 COM-INFO-NLIEU      PIC X(03).                            
                   05 FILLER              PIC X(12).                            
                04 COMM-GV81-COM-INFO-I   PIC X(35).                            
                04 FILLER REDEFINES COMM-GV81-COM-INFO-I.                       
                   05 COM-INFO-I-GIE      PIC X(09).                            
                   05 FILLER              PIC X(01).                            
                   05 COM-INFO-I-CDE      PIC X(06).                            
                   05 FILLER              PIC X(01).                            
                   05 COM-INFO-NSOC-I     PIC X(03).                            
                   05 COM-INFO-NLIEU-I    PIC X(03).                            
                   05 FILLER              PIC X(12).                            
      * - (IN) NUMERO DE TRANSACTION DE PAIEMENT                                
                04  COMM-GV81-COM-NTRANS  PIC S9(8) COMP-3.                     
             03 FILLER                    PIC X(6030).                          
                                                                                
