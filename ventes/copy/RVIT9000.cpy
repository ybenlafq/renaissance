      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVIT9000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVIT9000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT9000.                                                    00000090
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT9000.                                                            
      *}                                                                        
           02  IT90-NINVENTAIRE                                         00000100
               PIC X(0005).                                             00000110
           02  IT90-NSOCDEPOT                                           00000120
               PIC X(0003).                                             00000130
           02  IT90-NDEPOT                                              00000140
               PIC X(0003).                                             00000150
           02  IT90-NCODIC                                              00000160
               PIC X(0007).                                             00000170
           02  IT90-QSTOCKTHEO                                          00000180
               PIC S9(5) COMP-3.                                        00000190
           02  IT90-QSTOCKINVT                                          00000200
               PIC S9(5) COMP-3.                                        00000210
           02  IT90-DSYST                                               00000220
               PIC S9(13) COMP-3.                                       00000230
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00000240
      *---------------------------------------------------------        00000250
      *   LISTE DES FLAGS DE LA TABLE RVIT9000                          00000260
      *---------------------------------------------------------        00000270
      *                                                                 00000280
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVIT9000-FLAGS.                                              00000290
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVIT9000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-NINVENTAIRE-F                                       00000300
      *        PIC S9(4) COMP.                                          00000310
      *--                                                                       
           02  IT90-NINVENTAIRE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-NSOCDEPOT-F                                         00000320
      *        PIC S9(4) COMP.                                          00000330
      *--                                                                       
           02  IT90-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-NDEPOT-F                                            00000340
      *        PIC S9(4) COMP.                                          00000350
      *--                                                                       
           02  IT90-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-NCODIC-F                                            00000360
      *        PIC S9(4) COMP.                                          00000370
      *--                                                                       
           02  IT90-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-QSTOCKTHEO-F                                        00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  IT90-QSTOCKTHEO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-QSTOCKINVT-F                                        00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  IT90-QSTOCKINVT-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  IT90-DSYST-F                                             00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  IT90-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00000440
                                                                                
