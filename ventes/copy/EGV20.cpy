      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV20   EGV20                                              00000020
      ***************************************************************** 00000030
       01   EGV20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICSELL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNCODICSELL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNCODICSELF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICSELI    PIC X(7).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MCMARQI   PIC X(5).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLREFFOURNI    PIC X(20).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNENTCDESELL   COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MNENTCDESELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNENTCDESELF   PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNENTCDESELI   PIC X(5).                                  00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLENTCDESELL   COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MLENTCDESELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLENTCDESELF   PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLENTCDESELI   PIC X(20).                                 00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTPRESTSELL   COMP PIC S9(4).                            00000460
      *--                                                                       
           02 MCTPRESTSELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MCTPRESTSELF   PIC X.                                     00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCTPRESTSELI   PIC X(5).                                  00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTPRESTSELL   COMP PIC S9(4).                            00000500
      *--                                                                       
           02 MLTPRESTSELL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MLTPRESTSELF   PIC X.                                     00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLTPRESTSELI   PIC X(20).                                 00000530
           02 MTAB1I OCCURS   12 TIMES .                                00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWSELECTL    COMP PIC S9(4).                            00000550
      *--                                                                       
             03 MWSELECTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MWSELECTF    PIC X.                                     00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MWSELECTI    PIC X.                                     00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTPRESTL    COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCTPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTPRESTF    PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCTPRESTI    PIC X(5).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLTPRESTL    COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MLTPRESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLTPRESTF    PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MLTPRESTI    PIC X(20).                                 00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNENTCDEL    COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNENTCDEF    PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNENTCDEI    PIC X(5).                                  00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLENTCDEL    COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MLENTCDEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLENTCDEF    PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MLENTCDEI    PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MZONCMDI  PIC X(15).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(58).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCODTRAI  PIC X(4).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCICSI    PIC X(5).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MNETNAMI  PIC X(8).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MSCREENI  PIC X(4).                                       00000980
      ***************************************************************** 00000990
      * SDF: EGV20   EGV20                                              00001000
      ***************************************************************** 00001010
       01   EGV20O REDEFINES EGV20I.                                    00001020
           02 FILLER    PIC X(12).                                      00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MDATJOUA  PIC X.                                          00001050
           02 MDATJOUC  PIC X.                                          00001060
           02 MDATJOUP  PIC X.                                          00001070
           02 MDATJOUH  PIC X.                                          00001080
           02 MDATJOUV  PIC X.                                          00001090
           02 MDATJOUO  PIC X(10).                                      00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNUMPAGEA      PIC X.                                     00001190
           02 MNUMPAGEC PIC X.                                          00001200
           02 MNUMPAGEP PIC X.                                          00001210
           02 MNUMPAGEH PIC X.                                          00001220
           02 MNUMPAGEV PIC X.                                          00001230
           02 MNUMPAGEO      PIC X(2).                                  00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MPAGEMAXA      PIC X.                                     00001260
           02 MPAGEMAXC PIC X.                                          00001270
           02 MPAGEMAXP PIC X.                                          00001280
           02 MPAGEMAXH PIC X.                                          00001290
           02 MPAGEMAXV PIC X.                                          00001300
           02 MPAGEMAXO      PIC X(2).                                  00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MNCODICSELA    PIC X.                                     00001330
           02 MNCODICSELC    PIC X.                                     00001340
           02 MNCODICSELP    PIC X.                                     00001350
           02 MNCODICSELH    PIC X.                                     00001360
           02 MNCODICSELV    PIC X.                                     00001370
           02 MNCODICSELO    PIC X(7).                                  00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCMARQA   PIC X.                                          00001400
           02 MCMARQC   PIC X.                                          00001410
           02 MCMARQP   PIC X.                                          00001420
           02 MCMARQH   PIC X.                                          00001430
           02 MCMARQV   PIC X.                                          00001440
           02 MCMARQO   PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCFAMA    PIC X.                                          00001470
           02 MCFAMC    PIC X.                                          00001480
           02 MCFAMP    PIC X.                                          00001490
           02 MCFAMH    PIC X.                                          00001500
           02 MCFAMV    PIC X.                                          00001510
           02 MCFAMO    PIC X(5).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MLREFFOURNA    PIC X.                                     00001540
           02 MLREFFOURNC    PIC X.                                     00001550
           02 MLREFFOURNP    PIC X.                                     00001560
           02 MLREFFOURNH    PIC X.                                     00001570
           02 MLREFFOURNV    PIC X.                                     00001580
           02 MLREFFOURNO    PIC X(20).                                 00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNENTCDESELA   PIC X.                                     00001610
           02 MNENTCDESELC   PIC X.                                     00001620
           02 MNENTCDESELP   PIC X.                                     00001630
           02 MNENTCDESELH   PIC X.                                     00001640
           02 MNENTCDESELV   PIC X.                                     00001650
           02 MNENTCDESELO   PIC X(5).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLENTCDESELA   PIC X.                                     00001680
           02 MLENTCDESELC   PIC X.                                     00001690
           02 MLENTCDESELP   PIC X.                                     00001700
           02 MLENTCDESELH   PIC X.                                     00001710
           02 MLENTCDESELV   PIC X.                                     00001720
           02 MLENTCDESELO   PIC X(20).                                 00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCTPRESTSELA   PIC X.                                     00001750
           02 MCTPRESTSELC   PIC X.                                     00001760
           02 MCTPRESTSELP   PIC X.                                     00001770
           02 MCTPRESTSELH   PIC X.                                     00001780
           02 MCTPRESTSELV   PIC X.                                     00001790
           02 MCTPRESTSELO   PIC X(5).                                  00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MLTPRESTSELA   PIC X.                                     00001820
           02 MLTPRESTSELC   PIC X.                                     00001830
           02 MLTPRESTSELP   PIC X.                                     00001840
           02 MLTPRESTSELH   PIC X.                                     00001850
           02 MLTPRESTSELV   PIC X.                                     00001860
           02 MLTPRESTSELO   PIC X(20).                                 00001870
           02 MTAB1O OCCURS   12 TIMES .                                00001880
             03 FILLER       PIC X(2).                                  00001890
             03 MWSELECTA    PIC X.                                     00001900
             03 MWSELECTC    PIC X.                                     00001910
             03 MWSELECTP    PIC X.                                     00001920
             03 MWSELECTH    PIC X.                                     00001930
             03 MWSELECTV    PIC X.                                     00001940
             03 MWSELECTO    PIC X.                                     00001950
             03 FILLER       PIC X(2).                                  00001960
             03 MCTPRESTA    PIC X.                                     00001970
             03 MCTPRESTC    PIC X.                                     00001980
             03 MCTPRESTP    PIC X.                                     00001990
             03 MCTPRESTH    PIC X.                                     00002000
             03 MCTPRESTV    PIC X.                                     00002010
             03 MCTPRESTO    PIC X(5).                                  00002020
             03 FILLER       PIC X(2).                                  00002030
             03 MLTPRESTA    PIC X.                                     00002040
             03 MLTPRESTC    PIC X.                                     00002050
             03 MLTPRESTP    PIC X.                                     00002060
             03 MLTPRESTH    PIC X.                                     00002070
             03 MLTPRESTV    PIC X.                                     00002080
             03 MLTPRESTO    PIC X(20).                                 00002090
             03 FILLER       PIC X(2).                                  00002100
             03 MNENTCDEA    PIC X.                                     00002110
             03 MNENTCDEC    PIC X.                                     00002120
             03 MNENTCDEP    PIC X.                                     00002130
             03 MNENTCDEH    PIC X.                                     00002140
             03 MNENTCDEV    PIC X.                                     00002150
             03 MNENTCDEO    PIC X(5).                                  00002160
             03 FILLER       PIC X(2).                                  00002170
             03 MLENTCDEA    PIC X.                                     00002180
             03 MLENTCDEC    PIC X.                                     00002190
             03 MLENTCDEP    PIC X.                                     00002200
             03 MLENTCDEH    PIC X.                                     00002210
             03 MLENTCDEV    PIC X.                                     00002220
             03 MLENTCDEO    PIC X(20).                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MZONCMDA  PIC X.                                          00002250
           02 MZONCMDC  PIC X.                                          00002260
           02 MZONCMDP  PIC X.                                          00002270
           02 MZONCMDH  PIC X.                                          00002280
           02 MZONCMDV  PIC X.                                          00002290
           02 MZONCMDO  PIC X(15).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(58).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
