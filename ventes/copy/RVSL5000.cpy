      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL5000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL5000                         
      *   TABLE DES RESULTATS D'INVENTAIRE PAR ZONES                            
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL5000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL5000.                                                            
      *}                                                                        
           02  SL50-DINV                                                        
               PIC   X(08).                                                     
           02  SL50-NSOCIETE                                                    
               PIC   X(03).                                                     
           02  SL50-NLIEU                                                       
               PIC   X(03).                                                     
           02  SL50-CLIEUINV                                                    
               PIC   X(05).                                                     
           02  SL50-CZONE                                                       
               PIC   X(05).                                                     
           02  SL50-NBCPT                                                       
               PIC   X(01).                                                     
           02  SL50-WQTUN                                                       
               PIC   X(01).                                                     
           02  SL50-WCTRG                                                       
               PIC   X(01).                                                     
           02  SL50-WETATC                                                      
               PIC   X(01).                                                     
           02  SL50-WCB                                                         
               PIC   X(01).                                                     
           02  SL50-NBFD                                                        
               PIC   S9(07) COMP-3.                                             
           02  SL50-NBFS                                                        
               PIC   S9(07) COMP-3.                                             
           02  SL50-NBFERR                                                      
               PIC   S9(07) COMP-3.                                             
           02  SL50-QCPT1                                                       
               PIC   S9(07) COMP-3.                                             
           02  SL50-QCPT2                                                       
               PIC   S9(07) COMP-3.                                             
           02  SL50-QINV                                                        
               PIC   S9(07) COMP-3.                                             
           02  SL50-DSYST                                                       
               PIC   S9(13) COMP-3.                                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE RVSL5000                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL5000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL5000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-DINV-F                                                      
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-DINV-F                                                      
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NSOCIETE-F                                                  
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NSOCIETE-F                                                  
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NLIEU-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NLIEU-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-CLIEUINV-F                                                  
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-CLIEUINV-F                                                  
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-CZONE-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-CZONE-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NBCPT-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NBCPT-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-WQTUN-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-WQTUN-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-WCTRG-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-WCTRG-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-WETATC-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-WETATC-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-WCB-F                                                       
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-WCB-F                                                       
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NBFD-F                                                      
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NBFD-F                                                      
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NBFS-F                                                      
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NBFS-F                                                      
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-NBFERR-F                                                    
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-NBFERR-F                                                    
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-QCPT1-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-QCPT1-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-QCPT2-F                                                     
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-QCPT2-F                                                     
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-QINV-F                                                      
      *        PIC   S9(4) COMP.                                                
      *--                                                                       
           02  SL50-QINV-F                                                      
               PIC   S9(4) COMP-5.                                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  SL50-DSYST-F                                                     
      *        PIC   S9(4) COMP.                                                
      *                                                                         
      *--                                                                       
           02  SL50-DSYST-F                                                     
               PIC   S9(4) COMP-5.                                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
