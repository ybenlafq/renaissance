      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPF22   EPF22                                              00000020
      ***************************************************************** 00000030
       01   EPM20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNSOCI    PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIBELL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIBELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIBELF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIBELI  PIC X(20).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATERECL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MDATERECL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDATERECF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATERECI      PIC X(10).                                 00000290
           02 MNLIGNE1I OCCURS   14 TIMES .                             00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MNLIEUI      PIC X(3).                                  00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MLIBELI      PIC X(20).                                 00000380
           02 MNLIGNE3I OCCURS   14 TIMES .                             00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMPTL  COMP PIC S9(4).                                 00000400
      *--                                                                       
             03 MCMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCMPTF  PIC X.                                          00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MCMPTI  PIC X(10).                                      00000430
           02 MNLIGNE4I OCCURS   14 TIMES .                             00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDIFFL  COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MDIFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDIFFF  PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MDIFFI  PIC X(10).                                      00000480
           02 MNLIGNE5I OCCURS   14 TIMES .                             00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MRFACL  COMP PIC S9(4).                                 00000500
      *--                                                                       
             03 MRFACL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MRFACF  PIC X.                                          00000510
             03 FILLER  PIC X(4).                                       00000520
             03 MRFACI  PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLIBERRI  PIC X(79).                                      00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCICSI    PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNETNAMI  PIC X(8).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MSCREENI  PIC X(4).                                       00000730
      ***************************************************************** 00000740
      * SDF: EPF22   EPF22                                              00000750
      ***************************************************************** 00000760
       01   EPM20O REDEFINES EPM20I.                                    00000770
           02 FILLER    PIC X(12).                                      00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MDATJOUA  PIC X.                                          00000800
           02 MDATJOUC  PIC X.                                          00000810
           02 MDATJOUP  PIC X.                                          00000820
           02 MDATJOUH  PIC X.                                          00000830
           02 MDATJOUV  PIC X.                                          00000840
           02 MDATJOUO  PIC X(10).                                      00000850
           02 FILLER    PIC X(2).                                       00000860
           02 MTIMJOUA  PIC X.                                          00000870
           02 MTIMJOUC  PIC X.                                          00000880
           02 MTIMJOUP  PIC X.                                          00000890
           02 MTIMJOUH  PIC X.                                          00000900
           02 MTIMJOUV  PIC X.                                          00000910
           02 MTIMJOUO  PIC X(5).                                       00000920
           02 FILLER    PIC X(2).                                       00000930
           02 MPAGEA    PIC X.                                          00000940
           02 MPAGEC    PIC X.                                          00000950
           02 MPAGEP    PIC X.                                          00000960
           02 MPAGEH    PIC X.                                          00000970
           02 MPAGEV    PIC X.                                          00000980
           02 MPAGEO    PIC X(3).                                       00000990
           02 FILLER    PIC X(2).                                       00001000
           02 MNSOCA    PIC X.                                          00001010
           02 MNSOCC    PIC X.                                          00001020
           02 MNSOCP    PIC X.                                          00001030
           02 MNSOCH    PIC X.                                          00001040
           02 MNSOCV    PIC X.                                          00001050
           02 MNSOCO    PIC X(3).                                       00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MNLIBELA  PIC X.                                          00001080
           02 MNLIBELC  PIC X.                                          00001090
           02 MNLIBELP  PIC X.                                          00001100
           02 MNLIBELH  PIC X.                                          00001110
           02 MNLIBELV  PIC X.                                          00001120
           02 MNLIBELO  PIC X(20).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MDATERECA      PIC X.                                     00001150
           02 MDATERECC PIC X.                                          00001160
           02 MDATERECP PIC X.                                          00001170
           02 MDATERECH PIC X.                                          00001180
           02 MDATERECV PIC X.                                          00001190
           02 MDATERECO      PIC X(10).                                 00001200
           02 MNLIGNE1O OCCURS   14 TIMES .                             00001210
             03 FILLER       PIC X(2).                                  00001220
             03 MNLIEUA      PIC X.                                     00001230
             03 MNLIEUC PIC X.                                          00001240
             03 MNLIEUP PIC X.                                          00001250
             03 MNLIEUH PIC X.                                          00001260
             03 MNLIEUV PIC X.                                          00001270
             03 MNLIEUO      PIC X(3).                                  00001280
             03 FILLER       PIC X(2).                                  00001290
             03 MLIBELA      PIC X.                                     00001300
             03 MLIBELC PIC X.                                          00001310
             03 MLIBELP PIC X.                                          00001320
             03 MLIBELH PIC X.                                          00001330
             03 MLIBELV PIC X.                                          00001340
             03 MLIBELO      PIC X(20).                                 00001350
           02 MNLIGNE3O OCCURS   14 TIMES .                             00001360
             03 FILLER       PIC X(2).                                  00001370
             03 MCMPTA  PIC X.                                          00001380
             03 MCMPTC  PIC X.                                          00001390
             03 MCMPTP  PIC X.                                          00001400
             03 MCMPTH  PIC X.                                          00001410
             03 MCMPTV  PIC X.                                          00001420
             03 MCMPTO  PIC X(10).                                      00001430
           02 MNLIGNE4O OCCURS   14 TIMES .                             00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MDIFFA  PIC X.                                          00001460
             03 MDIFFC  PIC X.                                          00001470
             03 MDIFFP  PIC X.                                          00001480
             03 MDIFFH  PIC X.                                          00001490
             03 MDIFFV  PIC X.                                          00001500
             03 MDIFFO  PIC X(10).                                      00001510
           02 MNLIGNE5O OCCURS   14 TIMES .                             00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MRFACA  PIC X.                                          00001540
             03 MRFACC  PIC X.                                          00001550
             03 MRFACP  PIC X.                                          00001560
             03 MRFACH  PIC X.                                          00001570
             03 MRFACV  PIC X.                                          00001580
             03 MRFACO  PIC X(10).                                      00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MLIBERRA  PIC X.                                          00001610
           02 MLIBERRC  PIC X.                                          00001620
           02 MLIBERRP  PIC X.                                          00001630
           02 MLIBERRH  PIC X.                                          00001640
           02 MLIBERRV  PIC X.                                          00001650
           02 MLIBERRO  PIC X(79).                                      00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCODTRAA  PIC X.                                          00001680
           02 MCODTRAC  PIC X.                                          00001690
           02 MCODTRAP  PIC X.                                          00001700
           02 MCODTRAH  PIC X.                                          00001710
           02 MCODTRAV  PIC X.                                          00001720
           02 MCODTRAO  PIC X(4).                                       00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCICSA    PIC X.                                          00001750
           02 MCICSC    PIC X.                                          00001760
           02 MCICSP    PIC X.                                          00001770
           02 MCICSH    PIC X.                                          00001780
           02 MCICSV    PIC X.                                          00001790
           02 MCICSO    PIC X(5).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MNETNAMA  PIC X.                                          00001820
           02 MNETNAMC  PIC X.                                          00001830
           02 MNETNAMP  PIC X.                                          00001840
           02 MNETNAMH  PIC X.                                          00001850
           02 MNETNAMV  PIC X.                                          00001860
           02 MNETNAMO  PIC X(8).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSCREENA  PIC X.                                          00001890
           02 MSCREENC  PIC X.                                          00001900
           02 MSCREENP  PIC X.                                          00001910
           02 MSCREENH  PIC X.                                          00001920
           02 MSCREENV  PIC X.                                          00001930
           02 MSCREENO  PIC X(4).                                       00001940
                                                                                
