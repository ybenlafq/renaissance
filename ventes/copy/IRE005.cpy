      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE005 AU 20/02/1995  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,01,BI,A,                          *        
      *                           11,05,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,03,BI,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE005.                                                        
            05 NOMETAT-IRE005           PIC X(6) VALUE 'IRE005'.                
            05 RUPTURES-IRE005.                                                 
           10 IRE005-NSOCIETE           PIC X(03).                      007  003
           10 IRE005-TYPE-ENR           PIC X(01).                      010  001
           10 IRE005-TLM-ELA            PIC X(05).                      011  005
           10 IRE005-CGRPMAG            PIC X(02).                      016  002
           10 IRE005-NLIEU              PIC X(03).                      018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE005-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE005-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE005.                                                   
           10 IRE005-CODETAT            PIC X(01).                      023  001
           10 IRE005-LIBELLE            PIC X(20).                      024  020
           10 IRE005-LMOISENC           PIC X(09).                      044  009
           10 IRE005-LMOIS01            PIC X(09).                      053  009
           10 IRE005-LMOIS02            PIC X(09).                      062  009
           10 IRE005-LMOIS03            PIC X(09).                      071  009
           10 IRE005-LMOIS04            PIC X(09).                      080  009
           10 IRE005-LMOIS05            PIC X(09).                      089  009
           10 IRE005-LMOIS06            PIC X(09).                      098  009
           10 IRE005-LMOIS07            PIC X(09).                      107  009
           10 IRE005-LMOIS08            PIC X(09).                      116  009
           10 IRE005-LMOIS09            PIC X(09).                      125  009
           10 IRE005-LMOIS10            PIC X(09).                      134  009
           10 IRE005-LMOIS11            PIC X(09).                      143  009
           10 IRE005-LMOIS12            PIC X(09).                      152  009
           10 IRE005-CA01               PIC S9(13)V9(2) COMP-3.         161  008
           10 IRE005-CA02               PIC S9(13)V9(2) COMP-3.         169  008
           10 IRE005-CA03               PIC S9(13)V9(2) COMP-3.         177  008
           10 IRE005-CA04               PIC S9(13)V9(2) COMP-3.         185  008
           10 IRE005-CA05               PIC S9(13)V9(2) COMP-3.         193  008
           10 IRE005-CA06               PIC S9(13)V9(2) COMP-3.         201  008
           10 IRE005-CA07               PIC S9(13)V9(2) COMP-3.         209  008
           10 IRE005-CA08               PIC S9(13)V9(2) COMP-3.         217  008
           10 IRE005-CA09               PIC S9(13)V9(2) COMP-3.         225  008
           10 IRE005-CA10               PIC S9(13)V9(2) COMP-3.         233  008
           10 IRE005-CA11               PIC S9(13)V9(2) COMP-3.         241  008
           10 IRE005-CA12               PIC S9(13)V9(2) COMP-3.         249  008
           10 IRE005-REM01              PIC S9(13)V9(2) COMP-3.         257  008
           10 IRE005-REM02              PIC S9(13)V9(2) COMP-3.         265  008
           10 IRE005-REM03              PIC S9(13)V9(2) COMP-3.         273  008
           10 IRE005-REM04              PIC S9(13)V9(2) COMP-3.         281  008
           10 IRE005-REM05              PIC S9(13)V9(2) COMP-3.         289  008
           10 IRE005-REM06              PIC S9(13)V9(2) COMP-3.         297  008
           10 IRE005-REM07              PIC S9(13)V9(2) COMP-3.         305  008
           10 IRE005-REM08              PIC S9(13)V9(2) COMP-3.         313  008
           10 IRE005-REM09              PIC S9(13)V9(2) COMP-3.         321  008
           10 IRE005-REM10              PIC S9(13)V9(2) COMP-3.         329  008
           10 IRE005-REM11              PIC S9(13)V9(2) COMP-3.         337  008
           10 IRE005-REM12              PIC S9(13)V9(2) COMP-3.         345  008
            05 FILLER                      PIC X(160).                          
                                                                                
