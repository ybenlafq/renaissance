      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *   COPY MESSAGES MQ GENERATION GV11 LOCAL                                
      *                                                                         
      *                                                                         
      ******************************************************************        
      *  20011219 / DSA003 / NMD / AJOUT NOUVELLES ZONES RTGV11        *        
      *                            MODIF LG MESSAGE : 301 � 319 C      *        
      ******************************************************************        
      *  POUR MGV51 AVEC PUT                                                    
      *                                                                         
           10  WS-MESSAGE REDEFINES COMM-MQ21-MESSAGE.                          
            15  MES-ENTETE.                                                     
              20   MES-TYPE      PIC    X(3).                                   
              20   MES-NSOCMSG   PIC    X(3).                                   
              20   MES-NLIEUMSG  PIC    X(3).                                   
              20   MES-NSOCDST   PIC    X(3).                                   
              20   MES-NLIEUDST  PIC    X(3).                                   
              20   MES-NORD      PIC    9(8).                                   
              20   MES-LPROG     PIC    X(10).                                  
              20   MES-DJOUR     PIC    X(8).                                   
              20   MES-WSID      PIC    X(10).                                  
              20   MES-USER      PIC    X(10).                                  
              20   MES-CHRONO    PIC    9(7).                                   
              20   MES-NBRMSG    PIC    9(7).                                   
              20   MES-NBRENR    PIC    9(5).                                   
              20   MES-TAILLE    PIC    9(5).                                   
              20   MES-VERSION   PIC    X(2).                                   
              20   MES-FILLER    PIC    X(18).                                  
            15  MES-GV11.                                                       
  T001        20   MES-GV11-ENRGV10    PIC X(422).                              
              20   MES-GV11-PHOTO      PIC X(01).                               
              20   MES-GV11-NB-LG      PIC 9(03).                               
              20   MES-GV11-ENR OCCURS 80.                                      
  T001           25 MES-GV11-LIGNE      PIC X(0362).                            
      *---                                                                      
                                                                                
