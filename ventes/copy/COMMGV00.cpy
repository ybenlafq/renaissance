      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: GV00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION GV00                   *        
      *  LONGUEUR   : 9096                                             *        
      *                                                                *        
      *  COMMAREA MODIDIFE POUR LE NEM, CETTE VERSION SUPPRIME LES     *        
      *  COPY COMMGV01, COMMGV02, COMMGV03, COMMGV12, COMMGV15,        *        
      *       COMMGV40, COMMGV55, COMMGV60, COMMGV66, COMMHV51         *        
      *                                                                *        
      *  Z-COMMAREA, D' UNE LONGUEUR DE 9096 C, COMPRENANT :           *        
      *            LES ZONES FENETRES VENTE GV02-GV19           4000 C *        
      *            LES ZONES SAISIE MODIF/VENTE  GV01         + 4000 C *        
      *        4.LES ZONES APPLICATIVES GV01-APPLI          = 8000 C   *        
      *          LES ZONES DONNEES GENERALES VENTE           + 600 C   *        
      *      3.LES ZONES APPLICATIVES GV10-APPLI          = 8600 C     *        
      *        LES ZONES GENERALES GV00                    + 124 C     *        
      *    2.LES ZONES APPLICATIVES GV00-APPLI          = 8724 C       *        
      *      LES ZONES RESERVEES                         + 372 C       *        
      *  1.Z-COMMAREA                                 = 9096 C         *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      * MODIFICATIONS |                                                *        
      * -------------------------------------------------------------- *        
      * DATE          | LIBELLE                                        *        
      * -------------------------------------------------------------- *        
      * 01/07/2009    | AL0107 : AJOUT DE LA VALEUR 'LF' DANS LES      *        
      *               | BOOLEENS KOM-TYPE-LIGNE POUR D�FINIR UN LIEN   *        
      *               | FORT ENTRE LIGNE DE SERVICE ET PRODUIT         *        
      *               | LIEN FORT = SERVICE ASSOCI� � UN PRODUIT       *        
      *               | QUI NE PEUT SE JUSTIFIER LUI-M�ME              *        
      *               | EXEMPLE TYPE ASSURANCE.                        *        
      *               | LA COMMGV01 N'A PAS �T� TOUCH�E.               *        
      ******************************************************************        
      * -------------------------------------------------------------- *        
      * MODIFICATIONS | INNOVENTE V3. MODIFICATION VENTES B&S          *        
      * -------------------------------------------------------------- *        
      * DATE          | LIBELLE                                        *        
      * -------------------------------------------------------------- *        
      * 17/02/2010    | AL1702 : AJOUT NSEQENS DE LA RTGV10            *        
      ******************************************************************        
V34R4 * 17/01/2011    | AJOUT FLAG 'VENTE EN COURS EXPEDITION'         *        
      ******************************************************************        
      * 14/12/2011    | VENTE SIEBEL MARQUE SI2011                     *        
      *               | AJOUT 'S' DANS GV00-CFONCTION                  *        
      ******************************************************************        
      * 12/13         | EMPORTE LIVRAISON PLFT AJOUT DEBRANCHEMENT     *        
      *               | TGV32                                          *        
      ******************************************************************        
      * 13/10/2014    | CODE FONCTION M POUR MGD                       *        
      ******************************************************************        
      * 13/01/2015    | MGD AJOUT CODE MARQUE POUR TYPE DE VENTE       *        
      ******************************************************************        
      *                                                                         
       01  ICOMM                        PIC 9(02)          VALUE ZEROES.        
       01  ICOMM1                       PIC 9(02)          VALUE ZEROES.        
      *01  COM-GV00-LONG-COMMAREA       PIC S9(4) COMP     VALUE +9096.         
      *--- NOMBRE DE LIGNES MAXIMUM D'UNE VENTE                                 
       01  COMM-GV01-NLIGNE-MAX         PIC 9(02)          VALUE 80.            
      *                                                                         
       01  Z-COMMAREA.                                                          
      **** ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      **** ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *                                                                         
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(08).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS              PIC 9(02).                        
              05 COMM-DATE-SEMAA              PIC 9(02).                        
              05 COMM-DATE-SEMNU              PIC 9(02).                        
           02 COMM-DATE-FILLER          PIC X(08).                              
      *                                                                         
      **** ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV00 GENERALES ********************* 124          
           02 COMM-GV00-APPLI.                                                  
      *---    LIEU DE MODIFICATION DE LA VENTE (CACID)                          
              03 COMM-GV00-LIEU-MODIF.                                          
                 05 COMM-GV00-NSOCMODIF       PIC X(03).                        
                 05 COMM-GV00-NLIEUMODIF      PIC X(03).                        
                 05 COMM-GV00-MODIF-CTYPLIEU  PIC X(01).                        
                    88 COMM-GV00-ENTREPOT         VALUE '2'.                    
                    88 COMM-GV00-MAGASIN          VALUE '3'.                    
                 05 COMM-GV00-MODIF-CTYPSOC.                                    
                    10 COMM-GV00-MODIF-CTYPCAIS  PIC X(01).                     
                       88 COMM-GV00-MODIF-NEM        VALUE 'N'.                 
                       88 COMM-GV00-MODIF-NONNEM     VALUE 'M'.                 
                    10 COMM-GV00-MODIF-CTYPMAG   PIC X(01).                     
                       88 COMM-MAGASIN-CLASSIQUE     VALUE ' '.                 
                       88 COMM-MAGASIN-SPECIAL       VALUE 'S'.                 
                       88 COMM-MAGASIN-TRES-SPECIAL  VALUE 'T'.                 
                       88 COMM-MAGASIN-INNOVENTE     VALUE 'I'.                 
                       88 COMM-MAGASIN-MGD           VALUE 'M'.                 
                    10 COMM-GV00-MODIF-NMD       PIC X(01).                     
                       88 COMM-MAGASIN-NMD           VALUE 'D'.                 
      *{ remove-comma-in-dde 1.5                                                
      *                88 COMM-MAGASIN-NON-NMD                                  
      *                                  VALUE ' ', 'S', 'P'.                   
      *--                                                                       
                       88 COMM-MAGASIN-NON-NMD                                  
                                         VALUE ' '  'S'  'P'.                   
      *}                                                                        
      *---    IDENTIFIANT DE LA VENTE                                           
              03 COMM-GV00-LIEU-VENTE.                                          
                 05 COMM-GV00-NSOCIETE        PIC X(03).                        
                 05 COMM-GV00-NLIEU           PIC X(03).                        
                 05 COMM-GV00-VENTE-CTYPSOC.                                    
                    10 COMM-GV00-VENTE-CTYPCAIS   PIC X(01).                    
                       88 COMM-GV00-VENTE-NEM        VALUE 'N'.                 
                       88 COMM-GV00-VENTE-NONNEM     VALUE 'M'.                 
      *---    LIEU DE STOCKAGE MAGASIN                                          
              03 COMM-GV00-LIEU-STOCKMAG.                                       
                 05 COMM-GV00-NSOCLIVR        PIC X(03).                        
                 05 COMM-GV00-NDEPOT          PIC X(03).                        
              03 COMM-GV00-CFONCTION       PIC X(01).                           
                 88 COMM-SAISIE-VENTE             VALUE '1'.                    
                 88 COMM-SAISIE-PRE-RESERVATION   VALUE '2'.                    
                 88 COMM-SAISIE-INEXISTANTE       VALUE '3'.                    
      *{ remove-comma-in-dde 1.5                                                
SI2011*          88 COMM-REPRISE-PRE-RESERVATION  VALUE '4' , '5' , 'S'.        
      *--                                                                       
                 88 COMM-REPRISE-PRE-RESERVATION  VALUE '4'   '5'   'S'.        
      *}                                                                        
SI2011           88 COMM-REPRISE-PRE-SIEBEL       VALUE 'S'.                    
                 88 COMM-MODE-DEGRADE-NEM         VALUE 'O'.                    
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-MODIFICATION-VENTE                                     
      *             VALUE '6', '7', '8', '9', 'O', 'P'.                         
      *--                                                                       
                 88 COMM-MODIFICATION-VENTE                                     
                    VALUE '6'  '7'  '8'  '9'  'O'  'P'.                         
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-MODIFICATION-INEXISTANTE                               
      *             VALUE 'A', 'B', 'C', 'D'.                                   
      *--                                                                       
                 88 COMM-MODIFICATION-INEXISTANTE                               
                    VALUE 'A'  'B'  'C'  'D'.                                   
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-FONCTION-VENTE                                         
      *             VALUE '1', '4', '5', '6', '7', '8', '9', 'O', 'P'.          
      *--                                                                       
                 88 COMM-FONCTION-VENTE                                         
                    VALUE '1'  '4'  '5'  '6'  '7'  '8'  '9'  'O'  'P'.          
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-VENTE-INEXISTANTE                                      
      *             VALUE '3', 'A', 'B', 'C', 'D'.                              
      *--                                                                       
                 88 COMM-VENTE-INEXISTANTE                                      
                    VALUE '3'  'A'  'B'  'C'  'D'.                              
      *}                                                                        
                 88 COMM-CONSULTATION-VENTE       VALUE 'V'.                    
      *---    LIEU DE PAIEMENT + CODE IMPRIMANTE LOCALE                         
              03 COMM-GV00-LIEU-PAIEMENT.                                       
                 05 COMM-GV00-NSOCP           PIC X(03).                        
                 05 COMM-GV00-NLIEUP          PIC X(03).                        
              03 COMM-GV00-CIMPRIM         PIC X(10).                           
      *---    DONNEES COMMUNES A TOUS LES PROGRAMMES DE GV00                    
              03 COMM-GV00-DONNEES.                                             
                 05 COMM-GV00-NVENTE-TRAITE   PIC X(07).                        
      *-         MENU SUR LEQUEL LE PROGRAMME DOIT REVENIR (TGV00-40-45)        
                 05 COMM-GV00-CMENU           PIC X(05).                        
      *-         PROGRAMME SUR LEQUEL LA FENETRE DOIT REVENIR                   
      *-        (TGV01-TGV10-TGV14-TGV16-TGV17-TGV41)                           
                 05 COMM-GV00-CPROG           PIC X(05).                        
      *---    CRITERES DE SELECTION : OPTIONS DE VENTE                20        
              03 COMM-GV00-CRITERES-VENTES.                                     
                 05 COMM-GV00-NVENTE          PIC X(07).                        
                 05 COMM-GV00-NORDRE          PIC X(05).                        
                 05 COMM-GV00-DVENTE          PIC X(08).                        
      *---    CRITERES DE SELECTION : BONS ENLEVEMENT (OPT 10,11,12)  11        
              03 COMM-GV00-CRITERES-BE.                                         
                 05 COMM-GV00-TYPE-DE-TRAVAIL PIC X(01).                        
                    88 COMM-DEMANDE-DE-SUPPRESSION  VALUE 'S'.                  
                    88 COMM-DEMANDE-DE-CREATION     VALUE 'C'.                  
                 05 COMM-GV00-NLIEUORIG       PIC X(03).                        
                 05 COMM-GV00-NBONENLV        PIC X(07).                        
                 05 COMM-GV00-TOPESEL         PIC X(01).                        
                    88 COMM-GV80-NONTOPE          VALUE '1'.                    
      *---    CRITERES DE SELECTION : NCODIC (OPT 9 GV60)             11        
              03 COMM-GV00-NCODIC          PIC X(07).                           
              03 COMM-GV00-TYPVTE          PIC X(01).                           
              03 COMM-GV00-PERDEB          PIC X(08).                           
              03 COMM-GV00-PERFIN          PIC X(08).                           
AL2208* -> BON D'ECHANGE                                                        
AL2208        03 COMM-GV00-NBONECH         PIC S9(9)V USAGE COMP-3.             
AL2208*       03 COMM-GV00-FILLER          PIC X(11).                           
AL2208*       03 COMM-GV00-FILLER          PIC X(06).                           
AL2812        03 COMM-GV00-GESTION-B2B.                                         
AL2812           10 COMM-GV00-DEF-CLT         PIC X(01).                        
AL2812               88 COMM-GV00-CLIENT-B2B-OUI         VALUE 'O'.             
AL2812               88 COMM-GV00-CLIENT-B2B-NON         VALUE 'N'.             
AL2812           10 COMM-GV00-CLIENT-ARF      PIC X(01).                        
AL2812               88 COMM-GV00-CLIENT-ARF-OUI         VALUE 'A'.             
      *{ remove-comma-in-dde 1.5                                                
AL2812*              88 COMM-GV00-CLIENT-ARF-NON    VALUE 'N',' '.              
      *--                                                                       
                     88 COMM-GV00-CLIENT-ARF-NON    VALUE 'N' ' '.              
      *}                                                                        
AL2812           10 COMM-GV00-SAISIE-ARF      PIC X(01).                        
AL2812               88 COMM-GV00-SAISIE-ARF-OUI         VALUE 'O'.             
AL2812               88 COMM-GV00-SAISIE-ARF-NON         VALUE 'N'.             
      *INTER  03 COMM-GV00-FILLER          PIC X(03).                           
              03 COMM-GV00-INTERFIL        PIC X(01).                           
              03 COMM-GV00-FILLER          PIC X(02).                           
      *                                                                         
      **** ZONES APPLICATIVES GV00 - SAISIE/MODIF VENTES ******** 8600          
           02 COMM-GV00-REDEFINES       PIC X(8600).                            
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV10 - DONNEES GENERALES VENTE ***** 600          
           02 COMM-GV10-APPLI REDEFINES COMM-GV00-REDEFINES.                    
      *---    ZONES ENTETES DE LA VENTE A TRAITER                  400          
              03 COMM-GV10-DONNEES-VENTE.                                       
                 05 COMM-GV00-PTTVENTE        PIC S9(7)V99.                     
                 05 COMM-GV00-PVERSE          PIC S9(7)V99.                     
                 05 COMM-GV00-PCOMPT          PIC S9(7)V99.                     
                 05 COMM-GV00-PLIVR           PIC S9(7)V99.                     
                 05 COMM-GV00-PDIFFERE        PIC S9(7)V99.                     
                 05 COMM-GV00-PRFACT          PIC S9(7)V99.                     
                 05 COMM-GV00-CREMVTE         PIC X(05).                        
                 05 COMM-GV00-PREMVTE         PIC S9(7)V99.                     
                 05 COMM-GV00-PLIVRVTE        PIC S9(7)V99.                     
                 05 COMM-GV00-LCOMVTE1        PIC X(30).                        
                 05 COMM-GV00-LCOMVTE2        PIC X(30).                        
                 05 COMM-GV00-LCOMVTE3        PIC X(30).                        
                 05 COMM-GV00-LCOMVTE4        PIC X(30).                        
                 05 COMM-GV00-DMODIFVTE       PIC X(08).                        
                 05 COMM-GV00-WFLAGS.                                           
                    10 COMM-GV00-WFACTURE        PIC X(01).                     
                    10 COMM-GV00-WEXPORT         PIC X(01).                     
                    10 COMM-GV00-WDETAXEC        PIC X(01).                     
                    10 COMM-GV00-WDETAXEHC       PIC X(01).                     
                    10 COMM-GV00-WBLSP REDEFINES COMM-GV00-WDETAXEHC            
                                                 PIC X(01).                     
                 05 COMM-GV00-CORGORED        PIC X(05).                        
                 05 COMM-GV00-CMODPAIMTI      PIC X(05).                        
                 05 COMM-GV00-LDESCRIPTIF1    PIC X(30).                        
                 05 COMM-GV00-LDESCRIPTIF2    PIC X(30).                        
                 05 COMM-GV00-DLIVRBL         PIC X(08).                        
                 05 COMM-GV00-LAUTORM         PIC X(05).                        
                 05 COMM-GV00-NAUTORD         PIC X(07).                        
                 05 COMM-GV00-CEQUIPE         PIC X(05).                        
                 05 COMM-GV00-ECMODDEL        PIC X(03).                        
                 05 COMM-GV00-EDDELIV         PIC X(06).                        
                 05 COMM-GV00-ECPLAGE         PIC X(02).                        
                 05 COMM-GV00-PTTVENTE-DEBUT  PIC S9(7)V99.                     
                 05 COMM-GV00-PCOMPT-DEBUT    PIC S9(7)V99.                     
                 05 COMM-GV00-PLIVR-DEBUT     PIC S9(7)V99.                     
                 05 COMM-GV00-PDIFFERE-DEBUT  PIC S9(7)V99.                     
                 05 COMM-GV00-PRFACT-DEBUT    PIC S9(7)V99.                     
                 05 COMM-GV00-NSEQNQ          PIC S9(05) COMP-3.                
AL1702           05 COMM-GV00-NSEQENS         PIC S9(5) COMP-3.                 
SI2011           05 COMM-GV00-NAUTO           PIC X(20).                        
      *MGD14     05 COMM-GV10-FILLER1         PIC X(14).                        
                 05 COMM-GV00-MGD             PIC X(01).                        
      *          05 COMM-GV10-FILLER1         PIC X(13).                        
                 05 COMM-GV10-CMRQVTE         PIC X(05).                        
                 05 COMM-GV10-FILLER1         PIC X(08).                        
AL1702*          05 COMM-GV10-FILLER1         PIC X(34).                        
      *                                                                         
      *---    ZONES ETAT DE LA VENTE                               100          
              03 COMM-GV10-ETAT-VENTE.                                          
                 05 COMM-GV00-TOP-REMISE-PERSONNEL    PIC X(01).                
                    88 COMM-REMISE-PERSONNEL              VALUE 'P'.            
                    88 COMM-PAS-REMISE-PERSONNEL          VALUE ' '.            
                 05 COMM-GV00-TOP-ARTICLE-EMPORTE     PIC X(01).                
                    88 COMM-UNIQUEMENT-EMPORTE-MAG        VALUE 'E'.            
                    88 COMM-PAS-UNIQUE-EMPORTE-MAG        VALUE ' '.            
                 05 COMM-GV00-TOP-EN-LIVRAISON        PIC X(01).                
                    88 COMM-VENTE-EN-COURS-LIVRAISON      VALUE 'L'.            
                    88 COMM-VENTE-PAS-EN-LIVRAISON        VALUE ' '.            
                 05 COMM-GV00-TOP-CHARGEMENT-ECRAN    PIC X(01).                
                    88 COMM-CHARGEMENT-ECRAN              VALUE 'C'.            
                    88 COMM-PAS-CHARGEMENT-ECRAN          VALUE ' '.            
                 05 COMM-GV00-DEMANDE                 PIC X(01).                
                    88 COMM-AUCUNE-DEMANDE                VALUE ' '.            
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-CONTROLE-DE-DEMANDE           VALUE '1' ,           
      *                                             '2' , '3' , '4'.            
      *--                                                                       
                    88 COMM-CONTROLE-DE-DEMANDE           VALUE '1'             
                                                    '2'   '3'   '4'.            
      *}                                                                        
                    88 COMM-DEMANDE-D-ANNULATION          VALUE '1'.            
                    88 COMM-DEMANDE-DE-MODIFICATION       VALUE '2'.            
                    88 COMM-DEMANDE-D-AJOUT               VALUE '3'.            
                    88 COMM-DEMANDE-DE-CORRECTION         VALUE '4'.            
                 05 COMM-GV00-PAS-CALENDIER-MUT       PIC X(01).                
                    88 COMM-GV00-ENTREE-EFFECTUEE         VALUE 'O'.            
                    88 COMM-GV00-ENTREE-PAS-EFFECTUEE     VALUE ' '.            
                 05 COMM-GV00-MESSAGE.                                          
                    10 COMM-GV00-CODRET               PIC X(01).                
                       88 COMM-GV00-CODRET-OK            VALUE ' '.             
                       88 COMM-GV00-CODRET-ERREUR        VALUE '1'.             
                       88 COMM-GV00-CODRET-ERREUR-TIGA5  VALUE '2'.             
                    10 COMM-GV00-LIBERR               PIC X(58).                
      *-         POUR EXTRACTION DE L'ARCHIVAGE                                 
                 05 COMM-GV00-RECHERCHE-ARCHIVAGE     PIC X(01).                
                    88 COMM-GV00-PREMIER-PF5              VALUE ' '.            
                    88 COMM-GV00-DEUXIEME-PF5             VALUE '1'.            
                    88 COMM-GV00-TROISIEME-ENTER          VALUE '2'.            
                    88 COMM-GV00-AFFICHE-ONE              VALUE '3'.            
                    88 COMM-GV00-AFFICHE-TWO              VALUE '4'.            
                    88 COMM-GV00-PREMIER-PF21             VALUE 'A'.            
                    88 COMM-GV00-DEUXIEME-PF21            VALUE 'B'.            
                    88 COMM-GV00-PREMIER-PF1              VALUE 'C'.            
                    88 COMM-GV00-DEUXIEME-PF1             VALUE 'D'.            
                    88 COMM-GV00-PREMIER-PF2              VALUE 'E'.            
                    88 COMM-GV00-DEUXIEME-PF2             VALUE 'F'.            
                 05 COMM-GV00-ARCHIVAGE               PIC X(01).                
                    88 COMM-GV00-INTERDIT-ARCH            VALUE '2'.            
                    88 COMM-GV00-EXISTE-ARCH              VALUE '1'.            
                    88 COMM-GV00-EXISTE-PAS-ARCH          VALUE ' '.            
                 05 COMM-GV00-MGV42-CODRET            PIC X(01).                
                    88 COMM-GV00-GV42-OK                  VALUE ' '.            
                    88 COMM-GV00-GV42-PAS-OK              VALUE '1'.            
      *-         INDICE DE COMMAREA, NLIGNE A PASSER A MGV25                    
                 05 COMM-GV00-INDICE          PIC 9(02).                        
                 05 COMM-GV00-NLIGNE          PIC 9(02).                        
                 05 COMM-GV00-ACCES-MGV48             PIC X(01).                
                    88 COMM-GV00-PREMIER-MGV48            VALUE ' '.            
                    88 COMM-GV00-DEUXIEME-MGV48           VALUE '1'.            
                    88 COMM-GV00-VALID-PTF-1              VALUE 'A'.            
                    88 COMM-GV00-VALID-PTF-2              VALUE 'B'.            
                 05 COMM-GV00-TOP-EN-EXEPDITION       PIC X(01).                
                    88 COMM-VENTE-EN-COURS-EXPEDITION     VALUE 'X'.            
                    88 COMM-VENTE-PAS-EN-EXPEDITION       VALUE ' '.            
                 05 COMM-GV10-FILLER2         PIC X(26).                        
      *                                                                         
      *---    ZONES PERIPHERIQUES A LA VENTE                       100          
              03 COMM-GV10-PERIPHERIQUE-VENTE.                                  
                 05 COMM-GV00-DEBRANCHEMENT   PIC X(05).                        
                    88 PAS-DE-DEBRANCHEMENT               VALUE '     '.        
      *{ remove-comma-in-dde 1.5                                                
      *             88 DEMANDE-DE-DEBRANCHEMENT                                 
      *                                      VALUE 'TGV02' THRU 'TGV53',        
      *                                            'THV50' THRU 'THV55',        
      *                                            'TVT41' THRU 'TVT49'.        
      *--                                                                       
                    88 DEMANDE-DE-DEBRANCHEMENT                                 
                                             VALUE 'TGV02' THRU 'TGV53'         
                                                   'THV50' THRU 'THV55'         
                                                   'TVT41' THRU 'TVT49'.        
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *             88 DEBRANCHEMENT-FENETRES             VALUE                 
      *                              'TGV02', 'TGV03', 'TGV05', 'TGV06',        
      *                              'TGV07', 'TGV08', 'TGV19', 'TGV21',        
      *                              'TGV31' , 'TGV32'.                         
      *--                                                                       
                    88 DEBRANCHEMENT-FENETRES             VALUE                 
                                     'TGV02'  'TGV03'  'TGV05'  'TGV06'         
                                     'TGV07'  'TGV08'  'TGV19'  'TGV21'         
                                     'TGV31'   'TGV32'.                         
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *             88 DEBRANCHEMENT-ENCAISSEMENT                               
      *                                          VALUE 'TGV04', 'TGV11'.        
      *--                                                                       
                    88 DEBRANCHEMENT-ENCAISSEMENT                               
                                                 VALUE 'TGV04'  'TGV11'.        
      *}                                                                        
                    88 DEBRANCHE-ADRESSE                  VALUE 'TGV02'.        
                    88 DEBRANCHE-DATA-COMPL               VALUE 'TGV31'.        
                    88 DEBRANCHE-QUOTA                    VALUE 'TGV03'.        
                    88 DEBRANCHE-COMMUNE                  VALUE 'TGV05'.        
                    88 DEBRANCHE-STOCK                    VALUE 'TGV06'.        
                    88 DEBRANCHE-CARACTERISTIQUE          VALUE 'TGV07'.        
                    88 DEBRANCHE-COMMENTAIRE              VALUE 'TGV08'.        
                    88 DEBRANCHE-ENCAISSE-MODIF           VALUE 'TGV04'.        
                    88 DEBRANCHE-ENCAISSE-CREAT           VALUE 'TGV11'.        
                    88 DEBRANCHE-PRESTATION               VALUE 'TGV19'.        
                    88 DEBRANCHE-PRESTATION-INEXIST       VALUE 'TGV21'.        
                    88 CONSULTATION-ADRESSE               VALUE 'TVT42'.        
                    88 CONSULTATION-QUOTA                 VALUE 'TVT43'.        
                    88 CONSULTATION-ENCAISSEMENT          VALUE 'TVT44'.        
                    88 CONSULTATION-STOCK                 VALUE 'TVT46'.        
                    88 CONSULTATION-CARACTERISTIQUE       VALUE 'TVT47'.        
                    88 CONSULTATION-COMMENTAIRE           VALUE 'TVT48'.        
                    88 CONSULTATION-ADRESSE-EXPORT        VALUE 'TVT49'.        
                    88 CONSULTATION-ARCH-DOCUMENT         VALUE 'THV55'.        
                    88 CONSULTATION-ARCH-ADRESSE          VALUE 'THV53'.        
                    88 CONSULTATION-ARCH-ADR-EXPORT       VALUE 'THV54'.        
 MH                 88 CONSULTATION-ARCH-ENCAISSEMENT     VALUE 'THV50'.        
                    88 DEBRANCHE-LISTE-PLTF               VALUE 'TGV32'.        
      *-         DESCRIPTION ZONES POUR ADRESSE - TGV02 - TGV09                 
                 05 COMM-GV00-ADRESSE-TYPE-A          PIC X(01).                
                    88 PRESENCE-ADRESSE-TYPE-A            VALUE 'A'.            
                    88 ABSENCE-ADRESSE-TYPE-A             VALUE ' '.            
                 05 COMM-GV00-ADRESSE-TYPE-B          PIC X(01).                
                    88 PRESENCE-ADRESSE-TYPE-B            VALUE 'B'.            
                    88 ABSENCE-ADRESSE-TYPE-B             VALUE ' '.            
                 05 COMM-GV00-ADRESSE-TYPE-C          PIC X(01).                
                    88 PRESENCE-ADRESSE-TYPE-C            VALUE 'C'.            
                    88 ABSENCE-ADRESSE-TYPE-C             VALUE ' '.            
                 05 COMM-GV00-ADRESSE-TYPE-D          PIC X(01).                
                    88 PRESENCE-ADRESSE-TYPE-D            VALUE 'D'.            
                    88 ABSENCE-ADRESSE-TYPE-D             VALUE ' '.            
                 05 COMM-GV00-CINSEE-A        PIC X(05).                        
                 05 COMM-GV00-CINSEE-B        PIC X(05).                        
                 05 COMM-GV00-CINSEE-C        PIC X(05).                        
                 05 COMM-GV00-CPOSTAL-A       PIC X(05).                        
                 05 COMM-GV00-CPOSTAL-B       PIC X(05).                        
                 05 COMM-GV00-CPOSTAL-C       PIC X(05).                        
      *-         DESCRIPTION ZONES POUR CARACT. SPECIFIQUES - TGV07    *        
                 05 COMM-GV00-TOP-MAJ-CARACT-SPE      PIC X(01).                
                    88 MAJ-CARACT-SPE-NON-EFFECTUEE       VALUE 'C'.            
                    88 MAJ-CARACT-SPE-EFFECTUEE           VALUE ' '.            
                 05 COMM-GV00-TOP-MAJ-DATA-COMP       PIC X(01).                
                    88 MAJ-DATA-NON-EFFECTUEE             VALUE 'C'.            
                    88 MAJ-DATA-EFFECTUEE                 VALUE ' '.            
              03 COMM-GV10-FILLER          PIC X(59).                           
INNO32*       03 COMM-GV10-FILLER          PIC X(60).                           
      *                                                                         
      ****    ZONES APPLICATIVES GV55 - CONSULT DES VENTES ***38+22=60**        
              03 COMM-GV55-APPLI  REDEFINES   COMM-GV10-FILLER.                 
                 05 COMM-GV55-LNOM            PIC X(10).                        
                 05 COMM-GV55-NTEL            PIC X(10).                        
                 05 COMM-GV55-CPOSTAL         PIC X(05).                        
                 05 COMM-GV55-NOPAGE          PIC 9(03).                        
                 05 COMM-GV55-NBPAGE          PIC 9(03).                        
                 05 COMM-GV55-CODERET         PIC X(02).                        
                 05 COMM-GV55-CPROG           PIC X(05).                        
                 05 COMM-GV55-CMARQ           PIC X(05).                        
                 05 COMM-GV55-CFAM            PIC X(05).                        
                 05 COMM-GV55-TYPVTE          PIC X(01).                        
                 05 COMM-GV55-NVENTE          PIC X(07).                        
                 05 COMM-GV55-TOPGVVE         PIC X(01).                        
                 05 COMM-GV55-TOPENT          PIC X(01).                        
                 05 COMM-GV55-FILLER          PIC X(01).                        
      *                                                                         
      ******* ZONES APPLICATIVES GV10 - CORPS VENTE (LIGNES,ENCAI)- 8000        
              03 COMM-GV10-REDEFINES       PIC X(8000).                         
      *                                                                         
      *                                                                         
      ****    ZONES APPLICATIVES GV01 - LIGNES DE VENTES (GV01) *** 4100        
120110        03 COMM-INNOVENTE         REDEFINES COMM-GV10-REDEFINES.          
080110         05  CO-INNOVENTE.                                                
080110           15  CO-INNOVENTE-TRAN         PIC X(04).                       
080110           15  CO-INNOVENTE-SECURITE     PIC X(04).                       
080110             88 CO-INNOVENTE-OK                VALUE   '$**$'.            
080110           15  CO-INNOVENTE-DEMANDE      PIC X(02).                       
080110           15  CO-INNOVENTE-SOCIETE      PIC X(03).                       
080110           15  CO-INNOVENTE-LIEU         PIC X(03).                       
080110           15  CO-INNOVENTE-NVENTE       PIC X(11).                       
110110           15  CO-INNOVENTE-OCC1 OCCURS 40.                               
080110             25  CO-INNOVENTE-NCODIC       PIC X(07).                     
080110             25  CO-INNOVENTE-NLIGNE       PIC X(11).                     
080110         05  CO-INNOVENTE-FILLER         PIC X(7213).                     
      ****    ZONES APPLICATIVES GV01 - LIGNES DE VENTES (GV01) *** 4100        
              03 COMM-GV01-APPLI        REDEFINES COMM-GV10-REDEFINES.          
      *-         DONNEES GENERALES GV01 ---------------------------- 100        
                 05 KOM-CONTROLE              PIC 9(01).                        
                    88 KOM-CONTROLE-CORRECT       VALUE 1.                      
                    88 KOM-CONTROLE-INCORRECT     VALUE 2.                      
                 05 KOM-PAGE                  PIC 9(02).                        
                    88 KOM-DERNIERE-PAGE          VALUE 10.                     
                 05 KOM-DERNIER-ITEM          PIC 9(02).                        
                    88 KOM-ITEM-MAXIMUM           VALUE 80.                     
                    88 KOM-PAS-ITEM               VALUE 0.                      
      ****       SUPPRESSION DE KOM-COMPTEUR-LIGNE-ACTIVE, MODIF 12/99          
                 05 COMM-GV01-FILLER2         PIC X(02).                        
                 05 KOM-CHARGEMENT-TS         PIC X(01).                        
                    88 KOM-CHARGEMENT-TS-EFFECTUE         VALUE '1'.            
                    88 KOM-CHARGEMENT-TS-NON-EFFECTUE     VALUE ' '.            
                 05 COMM-GV00-TABLE-VENDEURS.                                   
                    10 COMM-GV00-POSTE-VENDEUR    OCCURS 4.                     
                       15 COMM-GV00-CVENDEUR         PIC X(6).                  
                       15 COMM-GV00-LVENDEUR         PIC X(15).                 
                       15 COMM-ATTR-CVENDEUR         PIC X.                     
                          88 COMM-CVENDEUR-PROT           VALUE 'N'.            
                          88 COMM-CVENDEUR-FREE           VALUE 'O'.            
                 05 KOM-DETAXE                PIC X(01).                        
                    88 KOM-DETAXE-PROT                    VALUE 'N'.            
                    88 KOM-DETAXE-FREE                    VALUE 'O'.            
      * VALEURS 'D' = DEMANDE EN SAISIE D'ADRESSE D'INFO REDEVANCE              
      * VALEURS 'O' = LES INFO REDEVANCE SONT PRESENTES                         
      * VALEURS ' ', 'N' ... = AUTRES SOLUTIONS                                 
                 05 COMM-GV01-INFO-REDEVANCE  PIC X(01).                        
                 05 COMM-GV01-MAJ-ADRESSE     PIC X(01).                        
                 05 COMM-GV01-FILLER          PIC X(01).                        
      *                                                                         
      *-         TABLE D'AFFICHAGE -80X50-------------------------- 4000        
                 05 KOM-TABLE-ECRAN.                                            
                    10 KOM-TABLE-LIGNE OCCURS 80.                               
      *-               CLE DES LIGNES                                 21        
                       15 KOM-ITEM                  PIC 9(02).                  
                       15 FILLER REDEFINES KOM-ITEM PIC X(02).                  
                          88 KOM-LIGNE-INEXISTANTE      VALUE SPACE.            
                       15 KOM-TYPE-LIGNE            PIC X(02).                  
                          88 KOM-LIGNE-VIERGE           VALUE SPACE.            
      *{ remove-comma-in-dde 1.5                                                
      *                   88 KOM-LIGNE-PRESTATION       VALUE 'PR','R1'.        
      *--                                                                       
                          88 KOM-LIGNE-PRESTATION       VALUE 'PR' 'R1'.        
      *}                                                                        
                          88 KOM-LIGNE-VENTE            VALUE 'AV'.             
      *{ remove-comma-in-dde 1.5                                                
      *                   88 KOM-LIGNE-REMISE           VALUE 'AR','R2'.        
      *--                                                                       
                          88 KOM-LIGNE-REMISE           VALUE 'AR' 'R2'.        
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *                   88 KOM-LIGNE-GARANTIE         VALUE 'AG','R3'.        
      *--                                                                       
                          88 KOM-LIGNE-GARANTIE         VALUE 'AG' 'R3'.        
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
AL0107*                   88 KOM-LIGNE-PRESTA-RATTACHEE                         
      *                   VALUE 'AP','LF','R4','R6'.                            
      *--                                                                       
                          88 KOM-LIGNE-PRESTA-RATTACHEE                         
                          VALUE 'AP' 'LF' 'R4' 'R6'.                            
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
AL0107*                   88 KOM-LIGNE-LIEN-FORT        VALUE 'LF','R6'.        
      *--                                                                       
                          88 KOM-LIGNE-LIEN-FORT        VALUE 'LF' 'R6'.        
      *}                                                                        
                          88 KOM-LIGNE-REPRISE          VALUE 'RE'.             
      *{ remove-comma-in-dde 1.5                                                
      *                   88 KOM-LIGNE-RACHAT-PSE       VALUE 'RP','R5'.        
      *--                                                                       
                          88 KOM-LIGNE-RACHAT-PSE       VALUE 'RP' 'R5'.        
      *}                                                                        
AL1603                    88 KOM-REPRI-PRESTATION       VALUE 'R1'.             
     |                    88 KOM-REPRI-REMISE           VALUE 'R2'.             
     |                    88 KOM-REPRI-GARANTIE         VALUE 'R3'.             
     |                    88 KOM-REPRI-PRESTA-RATTACHEE VALUE 'R4'.             
     |                    88 KOM-REPRI-RACHAT-PSE       VALUE 'R5'.             
     |                    88 KOM-REPRI-LIEN-FORT        VALUE 'R6'.             
      *{ remove-comma-in-dde 1.5                                                
     |*                   88 KOM-LIGNE-REPRISE-AUTRE                            
     |*                   VALUE 'R1','R2','R3','R4','R5','R6'.                  
      *--                                                                       
                          88 KOM-LIGNE-REPRISE-AUTRE                            
                          VALUE 'R1' 'R2' 'R3' 'R4' 'R5' 'R6'.                  
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *                   88 KOM-LIGNE-DEPENDANTE                               
AL0107*                   VALUE  'AR' , 'AG' , 'AP' , 'RE' , 'RP', 'LF',        
      *                          'R1','R2','R3','R4','R5','R6'.                 
      *--                                                                       
                          88 KOM-LIGNE-DEPENDANTE                               
                          VALUE  'AR'   'AG'   'AP'   'RE'   'RP'  'LF'         
                                 'R1' 'R2' 'R3' 'R4' 'R5' 'R6'.                 
      *}                                                                        
                       15 KOM-CLE-ARTICLE.                                      
                          20 KOM-ART-CTYPENREG         PIC X(01).               
                             88 KOM-ART-VENTE              VALUE '1'.           
                             88 KOM-ART-REMISE             VALUE '2'.           
                             88 KOM-ART-GARANTIE           VALUE '3'.           
                             88 KOM-ART-PRESTATION         VALUE '4'.           
                          20 KOM-ART-NCODICGRP         PIC X(7).                
                             88 KOM-ART-CODIC-GROUPE                            
                                VALUE '0000001'  THRU '9999999'.                
                             88 KOM-ART-CODIC-NORMAL       VALUE SPACES.        
                          20 KOM-PRE-CPRESTGRP   REDEFINES                      
                             KOM-ART-NCODICGRP   PIC X(7).                      
                          20 KOM-ART-NCODIC            PIC X(7).                
                             88 KOM-ART-ELEMENT                                 
                                VALUE '0000001'  THRU '9999999'.                
                          20 KOM-PRE-CPRESTATION REDEFINES                      
                             KOM-ART-NCODIC      PIC X(7).                      
                          20 KOM-ART-NSEQ              PIC X(2).                
      *-              ETAT DES LIGNES                                 10        
                       15 KOM-ETAT-LIGNE.                                       
                          20 KOM-EXISTENCE-LIGNE       PIC X(01).               
                             88 KOM-LIGNE-NON-CREEE     VALUE ' '.              
                             88 KOM-LIGNE-CREEE         VALUE 'C'.              
                          20 KOM-STATUT-LIGNE          PIC X(01).               
      *{ remove-comma-in-dde 1.5                                                
      *                      88 KOM-LIGNE-ACTIVE        VALUE ' ' , 'R'.        
      *--                                                                       
                             88 KOM-LIGNE-ACTIVE        VALUE ' '   'R'.        
      *}                                                                        
                             88 KOM-LIGNE-NORMALE       VALUE ' '.              
                             88 KOM-LIGNE-EN-REPRISE    VALUE 'R'.              
                             88 KOM-LIGNE-ANNULEE       VALUE 'A'.              
                          20 KOM-PREMIER-OU-AUTRE      PIC X(01).               
                             88 KOM-AUTRE-PASSAGE       VALUE ' '.              
      *{ remove-comma-in-dde 1.5                                                
      *                      88 KOM-PREMIER-PASSAGE     VALUE 'P' , 'C'.        
      *--                                                                       
                             88 KOM-PREMIER-PASSAGE     VALUE 'P'   'C'.        
      *}                                                                        
                             88 KOM-SAISIE-LIGNE        VALUE 'P'.              
                             88 KOM-CORRECTION-LIGNE    VALUE 'C'.              
                          20 KOM-DELIVRANCE            PIC X(01).               
                             88 KOM-EN-ATTENTE          VALUE ' '.              
                             88 KOM-DIFFERE             VALUE 'D'.              
                             88 KOM-EN-DESTOCKAGE       VALUE '*'.              
                             88 KOM-EN-LIVRAISON        VALUE 'L'.              
                             88 KOM-MUTE                VALUE '�'.              
      *{ remove-comma-in-dde 1.5                                                
      *                      88 KOM-NON-TOPE-LIVRE                              
      *                         VALUE ' ' , 'D' , '*' , 'L' , '�'.              
      *--                                                                       
                             88 KOM-NON-TOPE-LIVRE                              
                                VALUE ' '   'D'   '*'   'L'   '�'.              
      *}                                                                        
                             88 KOM-TOPE-LIVRE          VALUE 'T'.              
                          20 KOM-TYPE-DEMANDE          PIC X(02).               
                             88 KOM-AUCUNE-DEMANDE      VALUE '  '.             
                             88 KOM-DEMANDE-ANNULATION  VALUE 'AN'.             
                             88 KOM-DEMANDE-MODIFICATION VALUE 'MO'.            
                             88 KOM-DEMANDE-AJOUT       VALUE 'AJ'.             
                          20 KOM-CARACTERISTIQUE       PIC X(01).               
                             88 KOM-CARACT-NON-TRAITE   VALUE ' '.              
                             88 KOM-CARACT-NON-MAJ      VALUE '1'.              
                             88 KOM-CARACT-ABSENT       VALUE '2'.              
                             88 KOM-CARACT-MAJ          VALUE '3'.              
                             88 KOM-CARACT-MODIFIE      VALUE '4'.              
                          20 KOM-COMMUTATION           PIC X(01).               
                             88 KOM-SANS-COMMUTATION    VALUE ' '.              
                             88 KOM-COMMUTATION-COMPLEXE VALUE 'D'.             
                             88 KOM-COMMUTATION-SIMPLE  VALUE 'C'.              
                          20 KOM-LIEN-PRESTA           PIC X(01).               
                             88 KOM-LIEN-PRESTA-SANS    VALUE ' '.              
                             88 KOM-LIEN-PRESTA-NON-TRAITE VALUE '1'.           
                             88 KOM-LIEN-PRESTA-TRAITE  VALUE '2'.              
                          20 KOM-WFORCERSRP REDEFINES                           
                             KOM-LIEN-PRESTA           PIC X(01).               
                             88 KOM-WFORCERSRP-OUI      VALUE 'O'.              
                             88 KOM-WFORCERSRP-NON      VALUE 'N'.              
                          20 KOM-TYPE-REMISE           PIC X(01).               
                             88 KOM-REMISE-POURC        VALUE 'P'.              
                             88 KOM-REMISE-FRANC        VALUE 'F'.              
      *-              ZONES UTILISEES POUR LA GESTION DES ATTRIBUTS   13        
      *-              * VALEUR 'O' LA ZONE EST DEPROTEGEE                       
      *-              * VALEUR 'N' LA ZONE EST PROTEGEE                         
                      15 KOM-ZONE-LIBRE-EN-SAISIE.                              
                          20 KOM-ATTR-CODIC            PIC X(01).               
                             88 KOM-CODIC-LIBRE            VALUE 'O'.           
                             88 KOM-CODIC-BLOQUE           VALUE 'N'.           
                          20 KOM-ATTR-QTE              PIC X(01).               
                             88 KOM-QTE-LIBRE              VALUE 'O'.           
                             88 KOM-QTE-BLOQUE             VALUE 'N'.           
                          20 KOM-ATTR-TYPE             PIC X(01).               
                             88 KOM-TYPE-LIBRE             VALUE 'O'.           
                             88 KOM-TYPE-BLOQUE            VALUE 'N'.           
                          20 KOM-ATTR-PVUNIT           PIC X(01).               
                             88 KOM-PVUNIT-LIBRE           VALUE 'O'.           
                             88 KOM-PVUNIT-BLOQUE          VALUE 'N'.           
                          20 KOM-ATTR-PVTOTAL          PIC X(01).               
                             88 KOM-PVTOTAL-LIBRE          VALUE 'O'.           
                             88 KOM-PVTOTAL-BLOQUE         VALUE 'N'.           
                          20 KOM-ATTR-DEL              PIC X(01).               
                             88 KOM-DEL-LIBRE              VALUE 'O'.           
                             88 KOM-DEL-BLOQUE             VALUE 'N'.           
                          20 KOM-ATTR-RESERV           PIC X(01).               
                             88 KOM-RESERV-LIBRE           VALUE 'O'.           
                             88 KOM-RESERV-BLOQUE          VALUE 'N'.           
                          20 KOM-ATTR-DATE             PIC X(01).               
                             88 KOM-DATE-LIBRE             VALUE 'O'.           
                             88 KOM-DATE-BLOQUE            VALUE 'N'.           
                          20 KOM-ATTR-PLAGE            PIC X(01).               
                             88 KOM-PLAGE-LIBRE            VALUE 'O'.           
                             88 KOM-PLAGE-BLOQUE           VALUE 'N'.           
                          20 KOM-ATTR-VENDEUR          PIC X(01).               
                             88 KOM-VENDEUR-LIBRE          VALUE 'O'.           
                             88 KOM-VENDEUR-BLOQUE         VALUE 'N'.           
                          20 KOM-ATTR-COMMENTAIRE      PIC X(01).               
                             88 KOM-COMMENTAIRE-LIBRE      VALUE 'O'.           
                             88 KOM-COMMENTAIRE-BLOQUE     VALUE 'N'.           
                          20 KOM-ATTR-CARACTERISTIQUE  PIC X(01).               
                             88 KOM-CARACTERISTIQUE-LIBRE  VALUE 'O'.           
                             88 KOM-CARACTERISTIQUE-BLOQUE VALUE 'N'.           
                          20 KOM-ATTR-AUTORISATION     PIC X(01).               
                             88 KOM-AUTORISATION-LIBRE     VALUE 'O'.           
                             88 KOM-AUTORISATION-BLOQUE    VALUE 'N'.           
      *                RESERVE                                         6        
EMDDAC*                15 COMM-GV01-FILLER1         PIC X(06).                  
"                      15 COMM-GV01-WDACEM          PIC X(01).                  
"                      15 COMM-GV01-FILLER1         PIC X(05).                  
      *                                                                         
      ********** ZONES APPLICATIVES GV0X - FENETRES DE LA VENTE *** 3900        
                 05 COMM-GV01-REDEFINES       PIC X(3900).                      
      *                                                                         
      *                                                                         
      ****       ZONES APPLICATIVES GV02 - SAISIE DES ADRESSES **** 3900        
                 05 COMM-GV02-APPLI   REDEFINES  COMM-GV01-REDEFINES.           
      *-            DONNEES GENERALES                                337        
                    10 COMM-GV02-PROG            PIC X(8).                      
                    10 FILLER REDEFINES COMM-GV02-PROG.                         
                       15 FILLER                    PIC X.                      
                       15 COMM-GV02-TRANSA          PIC X(4).                   
                       15 FILLER                    PIC X(3).                   
                    10 COMM-GV02-DATA.                                          
                       15 COMM-GV02-GQ0600.                                     
                          20 COMM-GV02-C1POSTAL     PIC X(05).                  
                          20 COMM-GV02-LCOMUNE      PIC X(32).                  
                          20 COMM-GV02-LBUREAU      PIC X(26).                  
                          20 COMM-GV02-CINSEE       PIC X(05) OCCURS 2.         
      *-               TYPE D'ADRESSE  'A' OU 'B'                               
                       15 COMM-GV02-ADRESSE         PIC X.                      
                       15 COMM-GV02-TITRE           PIC X(20).                  
                       15 COMM-GV02-ATTR            PIC X   OCCURS 180.         
                       15 COMM-GV02-NPAGE           PIC 999.                    
                       15 COMM-GV02-NBRPAG          PIC 999.                    
      *-               TOP DE MAJA, C=CREAT ADRESSE A  M=MODIF ADRESSE A        
                       15 COMM-GV02-TOPMAJA         PIC X.                      
      *-               TOP DE MAJB  C = CREAT ADRESSE B                         
      *-                            M = MODIF ADRESSE B                         
      *-                            S = ANNUL ADRESSE B                         
                       15 COMM-GV02-TOPMAJB         PIC X.                      
                       15 COMM-GV02-ADRAOK          PIC X.                      
                       15 COMM-GV02-VOIE            PIC X   OCCURS 3.           
                       15 COMM-GV02-TEL             PIC X   OCCURS 3.           
                       15 COMM-GV02-LIBRE           PIC X   OCCURS 3.           
                       15 COMM-GV02-REMISE          PIC X.                      
                       15 COMM-GV02-LIVRE           PIC X.                      
                       15 COMM-GV02-REPRISE         PIC X.                      
      *-               ZONES POUR CONTROLE ORTHOGRAPHE DE LA VOIE ET            
      *-               DETERMINATION DU CODE ILOT POUR LOGITRANS                
                       15 COMM-GV02-CTL-CILOT       PIC X   OCCURS 3.           
                          88 COMM-GV02-CTL-EFFECTUE         VALUE 'O'.          
                          88 COMM-GV02-CTL-PAS-EFFECTUE     VALUE SPACE.        
                       15 COMM-GV02-SAISIE-FORCEE   PIC X.                      
                          88 COMM-GV02-PF12-ACTIVEE         VALUE 'O'.          
                          88 COMM-GV02-PF12-NON-ACTIVEE     VALUE SPACE.        
                       15 COMM-GV02-HISTO-PF12      PIC X   OCCURS 3.           
                          88 COMM-GV02-PF12-EFFECTUEE       VALUE 'O'.          
                          88 COMM-GV02-PF12-PAS-EFFECTUEE   VALUE SPACE.        
                       15 COMM-GV02-ERREUR-CTL-ADR  PIC X   OCCURS 3.           
                          88 COMM-GV02-ERREUR-CTL           VALUE 'O'.          
                          88 COMM-GV02-PAS-ERREUR-CTL       VALUE SPACE.        
                       15 COMM-GV02-CILOT           PIC X(08) OCCURS 3.         
      *                                                                         
      *-               ZONES FICHIER ADRESSES A,B,C ET D      395X3=1185        
                       15 COMM-GV02-FICADR  OCCURS 3.                           
                          20 COMM-GV02-FCTITRENOM      PIC X(05).               
                          20 COMM-GV02-FLNOM           PIC X(25).               
                          20 COMM-GV02-FLPRENOM        PIC X(15).               
                          20 COMM-GV02-FLBATIMENT      PIC X(03).               
                          20 COMM-GV02-FLESCALIER      PIC X(03).               
                          20 COMM-GV02-FLETAGE         PIC X(03).               
                          20 COMM-GV02-FLPORTE         PIC X(03).               
                          20 COMM-GV02-FLCMPAD1        PIC X(32).               
                          20 COMM-GV02-FLCMPAD2        PIC X(32).               
                          20 COMM-GV02-FCVOIE          PIC X(05).               
                          20 COMM-GV02-FCTVOIE         PIC X(04).               
                          20 COMM-GV02-FLNOMVOIE       PIC X(21).               
                          20 COMM-GV02-FTELDOM.                                 
                             25 COMM-GV02-FTELDOM-P1      PIC X(02).            
                             25 COMM-GV02-FTELDOM-P2      PIC X(02).            
                             25 COMM-GV02-FTELDOM-P3      PIC X(02).            
                             25 COMM-GV02-FTELDOM-P4      PIC X(02).            
                             25 COMM-GV02-FTELDOM-P5      PIC X(02).            
                          20 COMM-GV02-FLCOMUNE        PIC X(32).               
                          20 COMM-GV02-FTELBUR.                                 
                             25 COMM-GV02-FTELBUR-P1      PIC X(02).            
                             25 COMM-GV02-FTELBUR-P2      PIC X(02).            
                             25 COMM-GV02-FTELBUR-P3      PIC X(02).            
                             25 COMM-GV02-FTELBUR-P4      PIC X(02).            
                             25 COMM-GV02-FTELBUR-P5      PIC X(02).            
                          20 COMM-GV02-FCPOSTAL        PIC X(05).               
                          20 COMM-GV02-FLBUREAU        PIC X(26).               
                          20 COMM-GV02-FLPOSTEBUR      PIC X(05).               
                          20 COMM-GV02-FLCOMLIV1       PIC X(78).               
                          20 COMM-GV02-FLCOMLIV2       PIC X(78).               
HV                        20 COMM-GV02-FLIDCLIENT      PIC X(08).               
HV                        20 COMM-GV02-FLCPAYS         PIC X(03).               
HV                        20 COMM-GV02-FLNGSM          PIC X(15).               
      *                                                              155        
                       15 COMM-GV02-FICADR-ETR.                                 
                          20 COMM-GV02-FLADRETR1       PIC X(31).               
                          20 COMM-GV02-FLADRETR2       PIC X(31).               
                          20 COMM-GV02-FLADRETR3       PIC X(31).               
                          20 COMM-GV02-FLADRETR4       PIC X(31).               
                          20 COMM-GV02-FLADRETR5       PIC X(31).               
      *                                                                         
      *-               ZONES ECRAN   ADRESSES A,B,C ET D      395X3=1185        
                       15 COMM-GV02-ECRADR  OCCURS 3.                           
                          20 COMM-GV02-ECTITRENOM      PIC X(05).               
                          20 COMM-GV02-ELNOM           PIC X(25).               
                          20 COMM-GV02-ELPRENOM        PIC X(15).               
                          20 COMM-GV02-ELBATIMENT      PIC X(03).               
                          20 COMM-GV02-ELESCALIER      PIC X(03).               
                          20 COMM-GV02-ELETAGE         PIC X(03).               
                          20 COMM-GV02-ELPORTE         PIC X(03).               
                          20 COMM-GV02-ELCMPAD1        PIC X(32).               
                          20 COMM-GV02-ELCMPAD2        PIC X(32).               
                          20 COMM-GV02-ECVOIE          PIC X(05).               
                          20 COMM-GV02-ECTVOIE         PIC X(04).               
                          20 COMM-GV02-ELNOMVOIE       PIC X(21).               
                          20 COMM-GV02-ETELDOM.                                 
                             25 COMM-GV02-ETELDOM-P1      PIC X(02).            
                             25 COMM-GV02-ETELDOM-P2      PIC X(02).            
                             25 COMM-GV02-ETELDOM-P3      PIC X(02).            
                             25 COMM-GV02-ETELDOM-P4      PIC X(02).            
                             25 COMM-GV02-ETELDOM-P5      PIC X(02).            
                          20 COMM-GV02-ELCOMUNE        PIC X(32).               
                          20 COMM-GV02-ETELBUR.                                 
                             25 COMM-GV02-ETELBUR-P1      PIC X(02).            
                             25 COMM-GV02-ETELBUR-P2      PIC X(02).            
                             25 COMM-GV02-ETELBUR-P3      PIC X(02).            
                             25 COMM-GV02-ETELBUR-P4      PIC X(02).            
                             25 COMM-GV02-ETELBUR-P5      PIC X(02).            
                          20 COMM-GV02-ECPOSTAL        PIC X(05).               
                          20 COMM-GV02-ELBUREAU        PIC X(26).               
                          20 COMM-GV02-ELPOSTEBUR      PIC X(05).               
                          20 COMM-GV02-ELCOMLIV1       PIC X(78).               
                          20 COMM-GV02-ELCOMLIV2       PIC X(78).               
HV                        20 COMM-GV02-IDCLIENT        PIC X(08).               
HV                        20 COMM-GV02-CPAYS           PIC X(03).               
HV                        20 COMM-GV02-NGSM            PIC X(15).               
      *                                                              155        
                       15 COMM-GV02-ECRADR-ETR.                                 
                          20 COMM-GV02-ELADRETR1       PIC X(31).               
                          20 COMM-GV02-ELADRETR2       PIC X(31).               
                          20 COMM-GV02-ELADRETR3       PIC X(31).               
                          20 COMM-GV02-ELADRETR4       PIC X(31).               
                          20 COMM-GV02-ELADRETR5       PIC X(31).               
HV    *             10 COMM-GV02-FILLER          PIC X(83).                     
HV                  10 COMM-GV02-BC50-ACTIF      PIC X(01).                     
HV                  10 COMM-GV02-FILLER          PIC X(04).                     
      *                                                                         
HV    *             10 COMM-GV02-REDEFINES       PIC X(800).                    
HV                  10 COMM-GV02-REDEFINES       PIC X(722).                    
      *                                                                         
      *                                                                         
      ****          ZONES APPLICATIVES GV18 - SELECTION ADRESSES *** 145        
                    10 COMM-GV18-APPLI  REDEFINES COMM-GV02-REDEFINES.          
                       15 COMM-GV18-MAX-PAGE        PIC 9(2).                   
                       15 COMM-GV18-NB-PAGE         PIC 9(2).                   
                       15 COMM-GV18-NO-PAGE         PIC 9(2).                   
                       15 COMM-GV18-CINSEE          PIC X(05).                  
                       15 COMM-GV18-WCRITERE        PIC X(41).                  
      *-               NOM DE LA VOIE DEMANDE EN RECHERCHE                      
                       15 COMM-GV18-LNOMPVOIE       PIC X(41).                  
      *-               NOM ET TYPE DE LA VOIE DE LA DERNIERE RECHERCHE          
                       15 COMM-GV18-CTVOIE-DEB      PIC X(05).                  
                       15 COMM-GV18-LNOMVOIE-DEB    PIC X(21).                  
                       15 COMM-GV18-SEL-CTVOIE      PIC X(05).                  
                       15 COMM-GV18-SEL-LNOMVOIE    PIC X(21).                  
HV    *                15 COMM-GV18-FILLER          PIC X(655).                 
                       15 COMM-GV18-FILLER          PIC X(577).                 
      *                                                                         
              15 COMM-GV31-APPLI REDEFINES COMM-GV18-FILLER.                    
                 20 DATA-FONCT.                                                 
                    25 COMM-GV31-LIVRE          PIC X(01).                      
                    25 COMM-GV31-REPRIS         PIC X(01).                      
                 20 DATA-COMPLEMENTAIRES.                                       
                  25 COMM-GV31-CDATENAISS       PIC X(8).                       
                  25 COMM-GV31-CCPNAISS         PIC X(5).                       
                  25 COMM-GV31-CLIEUNAISS       PIC X(32).                      
                  25 COMM-GV31-CWBTOB           PIC X(1).                       
                  25 COMM-GV31-CWARF            PIC X(1).                       
                  25 COMM-GV31-ADRESSE          PIC X(1).                       
                 20 DATA-TABLE.                                                 
                  25 COMM-GV31-TABLE            PIC X(1).                       
M16117*           25 COMM-GV31-TWACCES          PIC X(1).                       
M16117            25 COMM-GV31-TWASCUTIL        PIC X(1).                       
                  25 COMM-GV31-TWREPRISE        PIC X(1).                       
                  25 COMM-GV31-TNBPRDREP        PIC S9(2)V USAGE COMP-3.        
                  25 COMM-GV31-TDATENAISS       PIC X(8).                       
                  25 COMM-GV31-TCPNAISS         PIC X(5).                       
                  25 COMM-GV31-TLIEUNAISS       PIC X(32).                      
                  25 COMM-GV31-TWBTOB           PIC X(1).                       
                  25 COMM-GV31-TWARF            PIC X(1).                       
M16117            25 COMM-GV31-TWASC            PIC X(1).                       
M16117            25 COMM-GV31-TWMARCHES        PIC X(1).                       
M16117            25 COMM-GV31-TWCOL            PIC X(1).                       
                 20 DATA-ECRAN.                                                 
M16117*           25 COMM-GV31-MWACCES          PIC X(1).                       
M16117            25 COMM-GV31-MWASCUTIL        PIC X(1).                       
                  25 COMM-GV31-MWREPRISE        PIC X(1).                       
                  25 COMM-GV31-MNBPRDREP        PIC S9(2)V USAGE COMP-3.        
M16117            25 COMM-GV31-MWASC            PIC X(1).                       
M16117            25 COMM-GV31-MWMARCHES        PIC X(1).                       
M16117            25 COMM-GV31-MWCOL            PIC X(1).                       
                 20 DATA-COMMENTAIRES.                                          
                  25 COMM-GV31-MCOMMENL1        PIC X(78).                      
                  25 COMM-GV31-MCOMMENL2        PIC X(78).                      
                  25 COMM-GV31-MCOMMENR1        PIC X(78).                      
                  25 COMM-GV31-MCOMMENR2        PIC X(78).                      
M16117***      20 COMM-GV31-FILLER           PIC X(159).                        
M16117         20 COMM-GV31-FILLER           PIC X(153).                        
      ****          ZONES APPLICATIVES GV40 - ARCHIVAGE ************ 800        
---                 10 COMM-GV40-APPLI  REDEFINES COMM-GV02-REDEFINES.          
B&S   *-               DONNEES GV40                                  631        
---   *              12 COMM-GV40-DONNEES.                                      
B&S                    15 COMM-GV40-FLAG-ARCHIVAGE  PIC X.                      
B&S                       88 COMM-GV40-ACCES-KO             VALUE ' '.          
B&S                       88 COMM-GV40-ACCES-OK1            VALUE '1'.          
B&S                       88 COMM-GV40-ACCES-OK2            VALUE '2'.          
B&S                       88 COMM-GV40-ACCES-OK3            VALUE '3'.          
B&S                       88 COMM-GV40-ACCES-OK12           VALUE '4'.          
B&S                       88 COMM-GV40-ACCES-OK13           VALUE '5'.          
B&S                       88 COMM-GV40-ACCES-OK23           VALUE '6'.          
B&S                       88 COMM-GV40-ACCES-OK123          VALUE '7'.          
B&S                    15 COMM-GV40-PERIODE-MAX     PIC S9(03) COMP-3.          
B&S                       88 COMM-GV40-PERIODE-MAX-DEF      VALUE 5.            
B&S                    15 COMM-GV40-NBMAX-VALIDATION  PIC S9(03) COMP-3.        
B&S                       88 COMM-GV40-NBMAX-VALIDATION-DEF VALUE 22.           
B&S                    15 COMM-GV40-NBMAX-REJET     PIC S9(03) COMP-3.          
B&S                       88 COMM-GV40-NBMAX-REJET-DEF      VALUE 55.           
B&S                    15 COMM-HV45-APPLI.                                      
B&S                       20 COMM-HV45-NBPAGE          PIC 9(03).               
B&S                       20 COMM-HV45-NOPAGE          PIC 9(03).               
B&S                       20 COMM-HV45-CODERET         PIC X(02).               
B&S   *-               DONNEES COMMUNES AUX PROGRAMMES                          
B&S   *-               (THV41,THV42),(TEM46,TEM47,TEM48,TEM49,MEM49)            
B&S   *-               REPORT� MODIFICATIONS DANS LA COMMAREA COMMHV49          
B&S                    15 COMM-GV40-DONNEES-MODULE.                             
B&S                       20 COMM-GV40-NOPAGE          PIC 9(03).               
B&S                       20 COMM-GV40-NBPROPO         PIC 9(03).               
B&S                       20 COMM-GV40-TAB-SELECTION.                           
B&S   *-                     TABLEAU INDICES PROPOSITIONS S�LECTIONN�ES.        
B&S                          25 COMM-GV40-SELECTION    PIC S9(3) COMP-3         
HV                                                     OCCURS 250.              
HV    *                                                OCCURS 300.              
B&S   *-                     NOMBRE DE PROPOSITIONS S�LECTIONN�ES.              
B&S                          25 COMM-GV40-MAX-SELECT   PIC S9(3) COMP-3.        
B&S   *-                     INDICE DE LA DERNI�RE S�LECTION AFFICH�E           
B&S                          25 COMM-GV40-DERN-SELECT  PIC S9(3) COMP-3.        
B&S                          88 COMM-GV40-PAS-EN-SELECTION     VALUE 0.         
B&S                          88 COMM-GV40-EN-SELECTION VALUE 1 THRU 999.        
B&S   *-                  N� DE LA PROPOSITION AFFICH�E                         
B&S                       20 COMM-GV40-NOPROPO    PIC S9(3) COMP-3.             
B&S   *-                  INDICE PROPOSITION AFFICH�E DANS LA PAGE TS           
B&S                       20 COMM-GV40-NOINDICE   PIC S9(3) COMP-3.             
B&S   *-                  NOMBRE DE PROPOSITIONS D�J� AFFICH�ES                 
B&S                      20 COMM-GV40-NBPROPO-AFFICHES PIC S9(3) COMP-3.        
B&S   *****            15 COMM-GV40-ZONE-FILLER    PIC X(631).                  
MH-                    15 COMM-GV40-MAX-PERDF      PIC S9(03) COMP-3.           
HV    *                15 COMM-GV40-FILLER         PIC X(167).                  
HV-                    15 COMM-GV40-FILLER         PIC X(189).                  
      *                                                                         
      *-               DONNEES HV41                            158              
                       15 COMM-HV41-APPLI  REDEFINES COMM-GV40-FILLER.          
                          20 COMM-HV41-CRITERES.                                
                             25 COMM-HV41-LNOM           PIC X(24).             
                             25 COMM-HV41-DVENTE         PIC X(08).             
                             25 COMM-HV41-NVENTE         PIC X(07).             
                             25 COMM-HV41-NLIEU          PIC X(03).             
                             25 COMM-HV41-CMARQ          PIC X(05).             
                             25 COMM-HV41-CFAM           PIC X(05).             
                             25 COMM-HV41-NTEL           PIC X(10).             
                             25 COMM-HV41-NCODIC         PIC X(07).             
                             25 COMM-HV41-NTYPVTE        PIC X(03).             
                          20 COMM-HV41-NBPAGE         PIC 9(03).                
                          20 COMM-HV41-CODERET        PIC X(02).                
                          20 COMM-HV41-TYPE-RECHERCHE PIC X.                    
                             88 COMM-HV41-PAS-RECH            VALUE ' '.        
                             88 COMM-HV41-RECH-NVENTE         VALUE '1'.        
                             88 COMM-HV41-RECH-DVENTE         VALUE '2'.        
                             88 COMM-HV41-RECH-NOM-MARQUE     VALUE '3'.        
                             88 COMM-HV41-RECH-NOM-FAMILLE    VALUE '4'.        
                             88 COMM-HV41-RECH-NOM-MARQ-FAM   VALUE '5'.        
                             88 COMM-HV41-RECH-NOM    VALUE '3' '4' '5'.        
                             88 COMM-HV41-RECH-NTEL           VALUE '6'.        
                             88 COMM-HV41-RECH-NCODIC         VALUE '7'.        
                             88 COMM-HV41-RECH-LIEU-FAM       VALUE '8'.        
                             88 COMM-HV41-RECH-LIEU-MARQ      VALUE '9'.        
                             88 COMM-HV41-RECH-LIEU-PER       VALUE 'A'.        
                             88 COMM-HV41-RECH-LIEU-PER-TYP   VALUE 'B'.        
                             88 COMM-HV41-RECH-LIEU-TYP       VALUE 'C'.        
LY                           88 COMM-HV41-RECH-IDCLIENT       VALUE 'D'.        
                          20 COMM-HV41-TAB-ECRAN-SELECTION.                     
                             25 COMM-HV41-ECRAN-SELECT OCCURS 11 PIC X.         
      * AJOUT NFLAG                                                             
                          20 COMM-HV41-NFLAG          PIC X(001).               
                          20 COMM-HV41-NCLUSTERP      PIC X(013).               
                          20 COMM-HV41-NCLUSTER       PIC X(013).               
                          20 COMM-HV42-APPLI.                                   
                             25 COMM-HV42-TSHV42-NBPAGE  PIC 9(03).             
                             25 COMM-HV42-TSHV42-NOPAGE  PIC 9(03).             
                             25 COMM-HV42-TSHV43-NBPAGE  PIC 9(03).             
                             25 COMM-HV42-TSHV43-NOPAGE  PIC 9(03).             
                             25 COMM-HV42-TSHV44-NBPAGE  PIC 9(03).             
                             25 COMM-HV42-TSHV44-NOPAGE  PIC 9(03).             
                             25 COMM-HV42-CODERET        PIC X(02).             
                             25 COMMHV42-MONTANTS.                              
                                30 COMM-HV42-PTOTAL PIC S9(7)V99 COMP-3.        
                                30 COMM-HV42-PCOMPT PIC S9(7)V99 COMP-3.        
                                30 COMM-HV42-PLIVR  PIC S9(7)V99 COMP-3.        
                              30 COMM-HV42-PDIFFERE PIC S9(7)V99 COMP-3.        
                                30 COMM-HV42-PRFACT PIC S9(7)V99 COMP-3.        
                             25 COMM-HV42-LCRED          PIC X(07).             
                          20 COMM-HV41-FILLER         PIC X(21).                
---   *                   20 COMM-HV41-FILLER         PIC X(9).                 
      *                                                                         
      *                DONNEES EM46                                  166        
                       15 COMM-EM46-APPLI  REDEFINES COMM-GV40-FILLER.          
                          20 COMM-EM46-CRITERES.                                
                             25 COMM-EM46-NLIEU          PIC X(03).             
                             25 COMM-EM46-DDEBUT         PIC X(08).             
                             25 COMM-EM46-DFIN           PIC X(08).             
                             25 COMM-EM46-NCAISSE        PIC X(03).             
B&S                          25 COMM-EM46-NTRANS         PIC X(08).             
B&S                          25 COMM-EM46-MONTANT   PIC S9(7)V99 COMP-3.        
MODP  *                      25 COMM-EM46-TPAIE          PIC X(01).             
MODP                         25 COMM-EM46-TPAIE          PIC X(05).             
B&S                          25 COMM-EM46-TTRANS         PIC X(03).             
                             25 COMM-EM46-CDEVISE        PIC X(01).             
B&S   *                      25 COMM-EM46-MTREGLEM  PIC S9(7)V99 COMP-3.        
                             25 COMM-EM46-NCODIC         PIC X(07).             
B&S   * LE N� DE VENTE EST RENSEIGNE SOIT PAR N�VENTE, SOIT N� EMPORT           
B&S                          25 COMM-EM46-NVENTE         PIC X(08).             
                          20 COMM-EM46-NBPAGE         PIC 9(03).                
                          20 COMM-EM46-CODERET        PIC X(02).                
                          20 COMM-EM46-TYPE-RECHERCHE PIC X(01).                
                            88 COMM-EM46-PAS-RECH             VALUE '0'.        
B&S                         88 COMM-EM46-RECH-CODIC           VALUE '1'.        
B&S                         88 COMM-EM46-RECH-CODIC-DEL       VALUE '2'.        
B&S                         88 COMM-EM46-RECH-CODIC-DEL-NCAI  VALUE '3'.        
B&S                         88 COMM-EM46-RECH-CODIC-NCAI      VALUE '4'.        
B&S                         88 COMM-EM46-RECH-DEL             VALUE '5'.        
B&S                         88 COMM-EM46-RECH-DEL-DEV         VALUE '6'.        
B&S                         88 COMM-EM46-RECH-DEL-DEV-NCAI    VALUE '7'.        
B&S                         88 COMM-EM46-RECH-DEL-NCAI        VALUE '8'.        
                            88 COMM-EM46-RECH-DEL-NCAI-PMONT  VALUE '9'.        
                            88 COMM-EM46-RECH-DEL-NCAI-PVTE   VALUE 'A'.        
B&S                         88 COMM-EM46-RECH-DEV             VALUE 'B'.        
B&S                         88 COMM-EM46-RECH-DEV-MOD-PVTE    VALUE 'C'.        
B&S                         88 COMM-EM46-RECH-DEV-MOD-VTE     VALUE 'D'.        
B&S                         88 COMM-EM46-RECH-DEV-MOD-NC-PVTE VALUE 'E'.        
B&S                         88 COMM-EM46-RECH-DEV-NCAI        VALUE 'F'.        
B&S                         88 COMM-EM46-RECH-DEV-NCAI-VTEMOD VALUE 'G'.        
B&S                         88 COMM-EM46-RECH-DFIN            VALUE 'H'.        
B&S                         88 COMM-EM46-RECH-NCAI            VALUE 'I'.        
B&S                         88 COMM-EM46-RECH-NCAI-NTRANS     VALUE 'J'.        
B&S                         88 COMM-EM46-RECH-NCAI-NTRANS-PMO VALUE 'K'.        
B&S                         88 COMM-EM46-RECH-NCAI-NVTE       VALUE 'L'.        
B&S                         88 COMM-EM46-RECH-NCAI-PMONT      VALUE 'M'.        
B&S                         88 COMM-EM46-RECH-NCAI-PVTE       VALUE 'N'.        
B&S                         88 COMM-EM46-RECH-NTRANS          VALUE 'O'.        
B&S                         88 COMM-EM46-RECH-NTRANS-PMONT    VALUE 'P'.        
B&S                         88 COMM-EM46-RECH-NVTE            VALUE 'Q'.        
B&S                         88 COMM-EM46-RECH-PMONT           VALUE 'R'.        
B&S                         88 COMM-EM46-RECH-PVTE            VALUE 'S'.        
B&S                         88 COMM-EM46-RECH-PAS-CRITERE     VALUE 'Z'.        
                          20 COMM-EM46-TAB-ECRAN-SELECTION.                     
                             25 COMM-EM46-ECRAN-SELECT OCCURS 12 PIC X.         
                          20 COMM-EM47-APPLI.                                   
                             25 COMM-EM47-TSEM47-NBPAGE  PIC 9(03).             
                             25 COMM-EM47-TSEM47-NOPAGE  PIC 9(03).             
                             25 COMM-EM47-CODERET        PIC X(02).             
                             25 COMM-EM47-NPOSITION      PIC 9(02).             
                          20 COMM-EM48-APPLI.                                   
                             25 COMM-EM48-NBPAGE        PIC 9(03).              
                             25 COMM-EM48-NOPAGE        PIC 9(03).              
                          20 COMM-HV51-DONNEES.                                 
                             25 COMM-HV51-NB-POSTES  PIC S9(09) COMP-3.         
                             25 COMM-HV51-CODRET        PIC  9(03).             
                                88 COMM-HV51-CODRET-OK       VALUE 000.         
                                88 COMM-HV51-CODRET-KO       VALUE 001.         
                                88 COMM-HV51-CODRET-DK       VALUE 002.         
                             25 COMM-HV51-LIBERR        PIC X(25).              
                             25 COMM-HV51-CRITERES.                             
                                30 COMM-HV51-NSOCIETE      PIC X(03).           
                                30 COMM-HV51-NLIEU         PIC X(03).           
                                30 COMM-HV51-NVENTE        PIC X(07).           
                                30 COMM-HV51-LNOM          PIC X(22).           
DC01  *                   20 COMM-EM46-FILLER        PIC X(14).                 
DC01                      20 COMM-EM46-FILLER.                                  
DC01  *  <== PIC S9(04)V99 VAUT 4 CARACTERES ==>                                
DC01                       25 COMM-EM47-PREGLRENDU PIC S9(04)V99 COMP-3.        
B&S                        25 COMM-EM46-MODDEL         PIC X(3).                
B&S                        25 COMM-EM46-APVF           PIC X(1).                
B&S                        25 COMM-EM46-TYPE-MONTANT   PIC X(1).                
B&S   * <== MONTANT DE LA VENTE ==>                                             
B&S                  88 COMM-EM46-MONTANT-VENTE         VALUE 'V'.              
B&S   * <== MONTANT DE LA TRANSACTION ==>                                       
B&S                  88 COMM-EM46-MONTANT-TRANS         VALUE 'T'.              
B&S   * <== MONTANT DU PAIEMENT (COMM-EM46-MTREGLEM) ==>                        
B&S                  88 COMM-EM46-MONTANT-PAIEMENT      VALUE 'P'.              
                             25 FILLER                   PIC X(1).              
      *                                                                         
      *                                                                         
      ****       ZONES APPLICATIVES GV03 - CONSULTATION QUOTAS **** 1533        
                 05 COMM-GV03-APPLI    REDEFINES  COMM-GV01-REDEFINES.          
                    10 COMM-GV03-DONNEES.                                       
                       15 COMM-GV03-ADR.                                        
                          20 COMM-GV03-ADRESSE          OCCURS 3.               
                             25 COMM-GV03-WTYPEADR         PIC X(01).           
                             25 COMM-GV03-CPOSTAL          PIC X(05).           
                             25 COMM-GV03-CINSEE           PIC X(05).           
                       15 COMM-GV03-ETAT-FETCH       PIC 9(01).                 
                          88 COMM-GV03-F-F              VALUE 1.                
                       15 COMM-GV03-TABLE-PACK.                                 
                          20 COMM-GV03-TAB-PASSE        OCCURS 9.               
                             25 COMM-GV03-T-CLE.                                
                                30 COMM-GV03-T-CMODDEL        PIC X(03).        
                                30 COMM-GV03-T-CINSEE         PIC X(05).        
                                30 COMM-GV03-T-CPOSTAL        PIC X(05).        
                             25 COMM-GV03-T-DATA.                               
                                30 COMM-GV03-TDATA            OCCURS 27.        
                                   35 COMM-GV03-T-CFAM        PIC X(05).        
                       15 COMM-GV03-TABLE-PASSAGE-TRI.                          
                          20 COMM-GV03-T-CLE-T.                                 
                             25 COMM-GV03-T-CMODDEL-T      PIC X(03).           
                             25 COMM-GV03-T-CINSEE-T       PIC X(05).           
                             25 COMM-GV03-T-CPOSTAL-T      PIC X(05).           
                          20 COMM-GV03-T-DATA-T.                                
                             25 COMM-GV03-T-CFAM-T         PIC X(05).           
                       15 COMM-GV03-FLAG-PAGINATION PIC X(1).                   
                       15 COMM-GV03-TRI             PIC X(148).                 
                       15 COMM-GV03-USER  REDEFINES COMM-GV03-TRI.              
                          20 COMM-GV03-ITEM            PIC S9(5) COMP-3.        
                          20 COMM-GV03-DELIVR-MAX      PIC S9(5) COMP-3.        
                          20 COMM-GV03-ITEM-PAGE       PIC S9(5) COMP-3.        
                          20 COMM-GV03-ITEM-OCCURS     PIC S9(5) COMP-3.        
                          20 COMM-GV03-OCCURS-MAX      PIC S9(5) COMP-3.        
                          20 COMM-GV03-T-FONCTION      PIC X(04).               
                          20 COMM-GV03-NPAGE           PIC 9(03).               
                          20 COMM-IT-PLAGES            PIC 9(02).               
                          20 COMM-IT-PLAGES-MAX        PIC 9(02).               
                          20 FILLER                    PIC X(122).              
      *-            RESERVE                                         2367        
                    10 COMM-GV03-FILLER          PIC X(2367).                   
      *                                                               *         
      *                                                               *         
      ****       ZONES APPLICATIVES GV07 - CARACT SPECIFIQUES ****** 464        
                 05 COMM-GV07-APPLI  REDEFINES   COMM-GV01-REDEFINES.           
                    10 COMM-GV07-LIB             PIC X(20).                     
                    10 COMM-GV07-CONFIRME        PIC X.                         
                    10 COMM-GV07-WNB-FLAG-D      PIC 999.                       
                    10 COMM-GV07-DONNEES         OCCURS 8.                      
                       15 COMM-GV07-CAR5            OCCURS 5.                   
                          20 COMM-GV07-CARSP        PIC X(5).                   
                          20 COMM-GV07-CARSV        PIC X(5).                   
                          20 COMM-GV07-FLAG         PIC X(1).                   
      *-            RESERVE                                         3436        
                    10 COMM-GV07-FILLER          PIC X(3436).                   
      *                                                                         
      *                                                                         
      ****       ZONES APPLICATIVES GV19 - SELECT PRESTATIONS ****** 227        
                 05 COMM-GV19-APPLI  REDEFINES   COMM-GV01-REDEFINES.           
                    10 COMM-GV19-PRESTA-TYPE     PIC X(01).                     
                       88 COMM-GV19-PRESTA-RATTACHEE VALUE 'R'.                 
                       88 COMM-GV19-PRESTA-SEULE     VALUE 'S'.                 
                    10 COMM-GV19-ICOMM           PIC 9(02).                     
                    10 COMM-GV19-NCODICSEL       PIC X(07).                     
                    10 COMM-GV19-MODE-PRESENTATION  PIC X(01).                  
                       88 COMM-GV19-SANS-MODE           VALUE ' '.              
                       88 COMM-GV19-LIEN-CODIC          VALUE 'C'.              
                       88 COMM-GV19-SELECTION           VALUE 'L'.              
                    10 COMM-GV19-COUPLESEL.                                     
                       15 COMM-GV19-CTPRESTSEL   PIC X(05).                     
                       15 COMM-GV19-NENTCDESEL   PIC X(05).                     
                    10 COMM-GV19-NCODICSEL-ATTR  PIC X(01).                     
                       88 COMM-GV19-NCODICSEL-LIBRE     VALUE ' '.              
                       88 COMM-GV19-NCODICSEL-PROTEGE   VALUE 'P'.              
                    10 COMM-GV19-NENTCDESEL-ATTR PIC X(01).                     
                       88 COMM-GV19-NENTCDESEL-LIBRE    VALUE ' '.              
                       88 COMM-GV19-NENTCDESEL-PROTEGE  VALUE 'P'.              
                    10 COMM-GV19-CTPRESTSEL-ATTR PIC X(01).                     
                       88 COMM-GV19-CTPRESTSEL-LIBRE    VALUE ' '.              
                       88 COMM-GV19-CTPRESTSEL-PROTEGE  VALUE 'P'.              
                    10 COMM-GV19-PAGIN.                                         
                       15 COMM-GV19-PAGE            PIC S9(03) COMP-3.          
                       15 COMM-GV19-NBP             PIC S9(03) COMP-3.          
                       15 COMM-GV19-NBPRESTA        PIC 9(03).                  
                    10 COMM-GV19-TABLE-COUPLE.                                  
                       15 COMM-GV19-COUPLE          OCCURS 10.                  
                          20 COMM-GV19-CTPREST         PIC X(05).               
                          20 COMM-GV19-NENTCDE         PIC X(05).               
                    10 COMM-GV19-ETAT-POINTAGE.                                 
                       15 COMM-GV19-TOP-POINTAGE    PIC X(01) OCCURS 80.        
                          88 COMM-GV19-POINTAGE-VALIDE        VALUE ' '.        
                          88 COMM-GV19-POINTAGE-NON-VALIDE    VALUE 'N'.        
                    10 COMM-GV19-COUPLE-SAVE     PIC X(10).                     
                    10 COMM-GV20-PAGIN.                                         
                       15 COMM-GV20-PAGE            PIC S9(03) COMP-3.          
                       15 COMM-GV20-NBP             PIC S9(03) COMP-3.          
                       15 COMM-GV20-NBL             PIC S9(03) COMP-3.          
                    10 COMM-GV19-FILLER          PIC X(3673).                   
      *                                                                         
      *                                                                         
      ****       ZONES APPLICATIVES GV04 - MODIFICATION ENCAISS. ** 304         
                 05 COMM-GV04-APPLI  REDEFINES   COMM-GV01-REDEFINES.           
                    10 COMM-GV04-ADRESSE-OK      PIC X(1).                      
                    10 COMM-GV04-WMTVTE          PIC X(1).                      
                    10 COMM-GV04-WMONEYVTE       PIC X(1).                      
                    10 COMM-GV04-WECHELONT       PIC X(1).                      
                    10 COMM-GV04-WAVOIR          PIC X(1).                      
                    10 COMM-GV04-ITEM            PIC 9(2).                      
                    10 COMM-GV04-POS-MAX         PIC 9(2).                      
                    10 COMM-GV04-MAX-NSEQ-X      PIC X(2).                      
                    10 COMM-GV04-MAX-NSEQ REDEFINES COMM-GV04-MAX-NSEQ-X        
                                                 PIC 9(2).                      
      *-            POSTE 1: LIGNE INACTIVE (COMPTABILISEE)                     
      *-            POSTE 2: DERNIERE LIGNE (COMPTABILISEE OU NON)              
                    10 COMM-GV04-LIGNE-NON-VISIBLE-D OCCURS 2.                  
                       15 COMM-GV04-D-DSAISIE       PIC X(08).                  
                       15 COMM-GV04-D-NSEQ          PIC X(02).                  
                       15 COMM-GV04-D-CMODPAIMT     PIC X(05).                  
                       15 COMM-GV04-D-PREGLTVTE     PIC S9(7)V99 COMP-3.        
                       15 COMM-GV04-D-NREGLTVTE     PIC X(07).                  
                       15 COMM-GV04-D-DREMISE       PIC X(07).                  
                       15 COMM-GV04-D-WREGLTVTE     PIC X(01).                  
                       15 COMM-GV04-D-DCOMPTA       PIC X(08).                  
                    10 COMM-GV04-LIGNE-NON-VISIBLE-R OCCURS 2.                  
                       15 COMM-GV04-R-DSAISIE       PIC X(08).                  
                       15 COMM-GV04-R-NSEQ          PIC X(02).                  
                       15 COMM-GV04-R-CMODPAIMT     PIC X(05).                  
                       15 COMM-GV04-R-PREGLTVTE     PIC S9(7)V99 COMP-3.        
                       15 COMM-GV04-R-NREGLTVTE     PIC X(07).                  
                       15 COMM-GV04-R-DREMISE       PIC X(07).                  
                       15 COMM-GV04-R-WREGLTVTE     PIC X(01).                  
                       15 COMM-GV04-R-DCOMPTA       PIC X(08).                  
                    10 COMM-SWAP-ATTR-GV04       PIC X(1) OCCURS 105.           
                    10 COMM-GV04-FILLER          PIC X(3596).                   
      *                                                                         
      *                                                                         
      ****    ZONES APPLICATIVES GV11 - SAISIE ENCAISSEMENT ******** 221        
              03 COMM-GV11-APPLI  REDEFINES   COMM-GV10-REDEFINES.              
                 05 COMM-GV11-ITEM            PIC 9(02).                        
                 05 COMM-GV11-POS-MAX         PIC 9(02).                        
                 05 COMM-GV11-PHASE           PIC X(01).                        
                 05 COMM-GV11-ART-SENSIBLE    PIC X(01).                        
                 05 COMM-GV11-RENDU-MONNAIE   PIC S9(07)V99.                    
                 05 COMM-GV11-WMTVTE          PIC X(01).                        
                 05 COMM-GV11-WMONEYVTE       PIC X(01).                        
                 05 COMM-GV11-WECHELONT       PIC X(01).                        
                 05 COMM-GV11-WESCROC         PIC X(01).                        
                 05 COMM-GV11-WAVOIR          PIC X(01).                        
                 05 COMM-GV11-PRESTATION      PIC X(01).                        
                 05 COMM-GV11-WEXPORT         PIC X(01).                        
                 05 COMM-GV11-WDETAXEC        PIC X(01).                        
                 05 COMM-GV11-WDETAXEHC       PIC X(01).                        
                 05 COMM-GV11-DVENTE          PIC X(08).                        
                 05 COMM-GV11-PTOTAL          PIC X(09).                        
                 05 COMM-GV11-ADRESSE-OK      PIC X(01).                        
                 05 COMM-GV11-CUMUL-TVA       PIC S9(07)V99.                    
                 05 COMM-GV11-MTTOTAL         PIC S9(07)V99.                    
                 05 FILLER  REDEFINES  COMM-GV11-MTTOTAL.                       
                    10 COMM-GV11-MTTOTAL-E       PIC S9(07).                    
                    10 COMM-GV11-MTTOTAL-D       PIC S9(02).                    
                 05 COMM-GV11-TABLE.                                            
                    10 COMM-GV11-PRIX-TVA OCCURS 3.                             
                       15 COMM-GV11-TAUXTVA   PIC S9(03)V9 COMP-3.              
                       15 COMM-GV11-CUMULTVA  PIC S9(07)V99.                    
                    10 COMM-GV11-NLIEU        PIC X(03) OCCURS 5.               
                 05 COMM-SWAP-ATTR-GV11       PIC X(01) OCCURS 110.             
                 05 COMM-GV11-FILLER          PIC X(7779).                      
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV12 - VENTES D'UNE JOURNEE ************ 4        
           02 COMM-GV12-APPLI  REDEFINES   COMM-GV00-REDEFINES.                 
              05 COMM-GV12-MAX-PAGE        PIC 9(3) COMP-3.                     
              05 COMM-GV12-NO-PAGE         PIC 9(3) COMP-3.                     
              05 COMM-GV12-FILLER          PIC X(8596).                         
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV15 - INT PRE-RESERVATIONS *********** 49        
           02 COMM-GV15-APPLI  REDEFINES   COMM-GV00-REDEFINES.                 
              05 COMM-GV15-PAGE            PIC 9(02).                           
              05 COMM-GV15-DVENTE          PIC X(08).                           
              05 COMM-GV15-DHVENTE         PIC X(02).                           
              05 COMM-GV15-DMVENTE         PIC X(02).                           
              05 COMM-GV15-DVENTE-SAUV     PIC X(08).                           
              05 COMM-GV15-DHVENTE-SAUV    PIC X(02).                           
              05 COMM-GV15-DMVENTE-SAUV    PIC X(02).                           
              05 COMM-GV15-NLIEU           PIC X(03).                           
              05 COMM-GV15-LLIEU           PIC X(20).                           
              05 COMM-GV15-NORDRE-SAUV     PIC X(05).                           
INNO33        05 COMM-GV15-AFFICHAGE       PIC 9(1).                            
              05 COMM-GV15-FILLER          PIC X(8545).                         
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV60 - LISTE DES CDES FNR ************* 40        
           02 COMM-GV60-APPLI  REDEFINES   COMM-GV00-REDEFINES.                 
              05 COMM-GV60-PAGIN.                                               
                 10 COMM-GV60-PAGE            PIC S9(03) COMP-3.                
                 10 COMM-GV60-NBP             PIC S9(03) COMP-3.                
                 10 COMM-GV60-NBL             PIC S9(03) COMP-3.                
              05 COMM-GV60-CTYPLIEU        PIC X(01).                           
              05 COMM-GV60-CTYPSOC         PIC X(03).                           
              05 COMM-GV60-LREFFOURN       PIC X(20).                           
              05 COMM-GV60-CFAM            PIC X(05).                           
              05 COMM-GV60-CMARQ           PIC X(05).                           
              05 COMM-GV60-FILLER          PIC X(8560).                         
      *                                                                         
      *                                                                         
      **** ZONES APPLICATIVES GV66 - CONSULTATION DES B.E. ********** 38        
           02 COMM-GV66-APPLI  REDEFINES   COMM-GV00-REDEFINES.                 
              05 COMM-GV66-PAGE-ENC        PIC 9(02).                           
              05 COMM-GV66-PAGE-MAX        PIC 9(02).                           
              05 COMM-GV66-NLIEU-SEL       PIC X(03).                           
              05 COMM-GV66-LLIEU-SEL       PIC X(20).                           
              05 COMM-GV66-NLIEU-LU        PIC X(03).                           
              05 COMM-GV66-NBONENLV-LU     PIC X(07).                           
              05 COMM-GV66-CRITERE         PIC X.                               
                 88 COMM-GV66-UN-BE            VALUE 'B'.                       
                 88 COMM-GV66-TOUS-BE          VALUE 'L'.                       
                 88 COMM-GV66-UN-MAG           VALUE 'M'.                       
                 88 COMM-GV66-TOUS-MAG         VALUE 'T'.                       
              05 COMM-GV66-FILLER          PIC X(8562).                         
      **** ZONES APPLICATIVES GV66 - CONSULTATION DES B.E. ********** 38        
           02 COMM-GV25-APPLI  REDEFINES   COMM-GV00-REDEFINES.                 
            03 COMM-GV25.                                                       
              05 COMM-GV25-MSIN            PIC X(30).                           
              05 COMM-GV25-MNOM            PIC X(30).                           
              05 COMM-GV25-MCPOSTAL        PIC X(05).                           
              05 COMM-GV25-MTEL1           PIC X(02).                           
              05 COMM-GV25-MTEL2           PIC X(02).                           
              05 COMM-GV25-MTEL3           PIC X(02).                           
              05 COMM-GV25-MTEL4           PIC X(02).                           
              05 COMM-GV25-MTEL5           PIC X(02).                           
              05 COMM-GV25-MSOC            PIC X(03).                           
              05 COMM-GV25-MLIEU           PIC X(03).                           
              05 COMM-GV25-MVENTE          PIC X(07).                           
              05 COMM-GV25-MDANNULJ        PIC X(02).                           
              05 COMM-GV25-MDANNULM        PIC X(02).                           
              05 COMM-GV25-MDANNULA        PIC X(04).                           
              05 COMM-GV25-CHOIX           PIC 9(2).                            
              05 COMM-GV25-LIGNE OCCURS 10.                                     
                07 COMM-GV25-REFCMDEXT      PIC X(30).                          
                07 COMM-GV25-TYPE           PIC X(10).                          
                07 COMM-GV25-RAISON         PIC X(100).                         
                07 COMM-GV25-NOM            PIC X(50).                          
                07 COMM-GV25-ADR            PIC X(30).                          
                07 COMM-GV25-CPOSTAL        PIC X(05).                          
                07 COMM-GV25-LCOMMUNE       PIC X(50).                          
                07 COMM-GV25-ETAT           PIC X(05).                          
            03 COMM-GV26.                                                       
              05 COMM-GV26-PAGE            PIC 9(3).                            
              05 COMM-GV26-PAGEMAX         PIC 9(3).                            
              05 COMM-GV26-DCREATION       PIC X(8).                            
              05 COMM-GV26-NSOCIETE        PIC X(3).                            
              05 COMM-GV26-NLIEU           PIC X(3).                            
              05 COMM-GV26-NVENTE          PIC X(7).                            
              05 COMM-GV26-NORDRE          PIC X(5).                            
              05 COMM-GV26-PVTOTAL         PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-GVPVTOTAL       PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-COMPTANT        PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-GVCOMPTANT      PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-MNTARF          PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-GVMNTARF        PIC S9(7)V9(2) USAGE COMP-3.         
              05 COMM-GV26-MSERVICE        PIC X(30).                           
              05 COMM-GV26-DEPOSE          PIC X(30).                           
              05 COMM-GV26-REPRISEANC      PIC X(30).                           
              05 COMM-GV26-DTWEB           PIC X(8).                            
      *                                                                         
              05 COMM-GV25-FILLER          PIC X(5542).                         
      *                                                                         
      *                                                                         
                                                                                
