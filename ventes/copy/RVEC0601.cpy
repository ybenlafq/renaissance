      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE DSA000.RTEC06                      *        
      ******************************************************************        
      *01  RVEC0600.                                                            
      *    10 EC06-CPAYS           PIC X(2).                                    
      *    10 EC06-NSOC            PIC X(3).                                    
      *    10 EC06-CELEMEN         PIC X(5).                                    
      *    10 EC06-NSOCLIV         PIC X(3).                                    
      *    10 EC06-NLIEULIV        PIC X(3).                                    
       01  RVEC0601.                                                            
           10 EC06-CPAYS           PIC X(2).                                    
           10 EC06-NSOC            PIC X(3).                                    
           10 EC06-CSERVICE        PIC X(5).                                    
           10 EC06-CMODDEL         PIC X(3).                                    
           10 EC06-CELEMEN         PIC X(5).                                    
           10 EC06-NSOCLIV         PIC X(3).                                    
           10 EC06-NLIEULIV        PIC X(3).                                    
                                                                                
