      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV5101                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV5101                         
      **********************************************************                
      * POUR PASSAGE A L'EURO AJOUT DE 2 DECIMALES AUX MONTANTS*                
      * DATE : 07/01    AUTEUR:DENIS COIFFIER  MARQUE:DC01     *                
      **********************************************************                
       01  RVGV5101.                                                            
           02  GV51-NSOCMERE                                                    
               PIC X(0003).                                                     
           02  GV51-NMAGMGI                                                     
               PIC X(0003).                                                     
           02  GV51-CFAMMGI                                                     
               PIC X(0005).                                                     
           02  GV51-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV51-DMOISVENTE                                                  
               PIC X(0006).                                                     
           02  GV51-QVENDUE                                                     
               PIC S9(7) COMP-3.                                                
DC01  *    02  GV51-PCAHT                                                       
DC01  *        PIC S9(9) COMP-3.                                                
DC01  *    02  GV51-PMBHTMGI                                                    
DC01  *        PIC S9(7) COMP-3.                                                
DC01  *    02  GV51-PMBHTTOT                                                    
DC01  *        PIC S9(7) COMP-3.                                                
DC01       02  GV51-PCAHT                                                       
DC01           PIC S9(9)V99 COMP-3.                                             
DC01       02  GV51-PMBHTMGI                                                    
DC01           PIC S9(7)V99 COMP-3.                                             
DC01       02  GV51-PMBHTTOT                                                    
DC01           PIC S9(7)V99 COMP-3.                                             
           02  GV51-DSYST                                                       
               PIC S9(13) COMP-3.                                               
DC01  *    02  GV51-PCAHTSR                                                     
DC01  *        PIC S9(9) COMP-3.                                                
DC01       02  GV51-PCAHTSR                                                     
DC01           PIC S9(9)V99 COMP-3.                                             
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV5101                                  
      **********************************************************                
       01  RVGV5101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-NSOCMERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-NSOCMERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-NMAGMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-NMAGMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-CFAMMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-CFAMMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-DMOISVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-DMOISVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-PCAHT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-PCAHT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-PMBHTMGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-PMBHTMGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-PMBHTTOT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-PMBHTTOT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV51-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV51-PCAHTSR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GV51-PCAHTSR-F                                                   
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
