      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV210 AU 31/10/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,20,BI,A,                          *        
      *                           30,03,PD,A,                          *        
      *                           33,10,BI,A,                          *        
      *                           43,05,BI,A,                          *        
      *                           48,05,BI,A,                          *        
      *                           53,04,PD,A,                          *        
      *                           57,05,BI,A,                          *        
      *                           62,07,BI,A,                          *        
      *                           69,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV210.                                                        
            05 NOMETAT-IPV210           PIC X(6) VALUE 'IPV210'.                
            05 RUPTURES-IPV210.                                                 
           10 IPV210-NSOCIETE           PIC X(03).                      007  003
           10 IPV210-LVPARAM            PIC X(20).                      010  020
           10 IPV210-WSEQFAM            PIC S9(05)      COMP-3.         030  003
           10 IPV210-CODPROF            PIC X(10).                      033  010
           10 IPV210-CMARKETING         PIC X(05).                      043  005
           10 IPV210-CVMARKETING        PIC X(05).                      048  005
           10 IPV210-PSTDTTC            PIC S9(07)      COMP-3.         053  004
           10 IPV210-CMARQ              PIC X(05).                      057  005
           10 IPV210-NCODIC-NORMAL      PIC X(07).                      062  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV210-SEQUENCE           PIC S9(04) COMP.                069  002
      *--                                                                       
           10 IPV210-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV210.                                                   
           10 IPV210-CFAM               PIC X(05).                      071  005
           10 IPV210-INTER              PIC X(03).                      076  003
           10 IPV210-LIBPROFIL          PIC X(30).                      079  030
           10 IPV210-LMARKETING         PIC X(20).                      109  020
           10 IPV210-LREFFOURN          PIC X(20).                      129  020
           10 IPV210-LSTATCOMP          PIC X(03).                      149  003
           10 IPV210-LVMARKETING        PIC X(20).                      152  020
           10 IPV210-NCODIC             PIC X(09).                      172  009
           10 IPV210-VAR                PIC X(01).                      181  001
           10 IPV210-MONTINTER          PIC S9(04)V9(1) COMP-3.         182  003
           10 IPV210-QSTOCKDEP          PIC S9(08)      COMP-3.         185  005
           10 IPV210-QSTOCKMAG          PIC S9(08)      COMP-3.         190  005
           10 IPV210-PERDEB             PIC X(08).                      195  008
           10 IPV210-PERFIN             PIC X(08).                      203  008
            05 FILLER                      PIC X(302).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV210-LONG           PIC S9(4)   COMP  VALUE +210.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV210-LONG           PIC S9(4) COMP-5  VALUE +210.           
                                                                                
      *}                                                                        
