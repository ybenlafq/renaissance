      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT ISM090      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,07,BI,A,                          *        
      *                           20,03,PD,A,                          *        
      *                           23,05,BI,A,                          *        
      *                           28,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-ISM090.                                                        
            05 NOMETAT-ISM090           PIC X(6) VALUE 'ISM090'.                
            05 RUPTURES-ISM090.                                                 
           10 ISM090-TLM-ELA            PIC X(03).                      007  003
           10 ISM090-RAYON              PIC X(03).                      010  003
           10 ISM090-GROUPE             PIC X(07).                      013  007
           10 ISM090-WSEQFAM            PIC S9(05)      COMP-3.         020  003
           10 ISM090-CFAM               PIC X(05).                      023  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ISM090-SEQUENCE           PIC S9(04) COMP.                028  002
      *--                                                                       
           10 ISM090-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-ISM090.                                                   
           10 ISM090-LFAM               PIC X(20).                      030  020
           10 ISM090-PRMP               PIC S9(07)V9(6) COMP-3.         050  007
           10 ISM090-QSTOCK-TOTAL-E     PIC S9(05)      COMP-3.         057  003
           10 ISM090-QSTOCK-TOTAL-M     PIC S9(05)      COMP-3.         060  003
           10 ISM090-QSTOCKRES-E        PIC S9(05)      COMP-3.         063  003
           10 ISM090-QSTOCKRES-M        PIC S9(05)      COMP-3.         066  003
            05 FILLER                      PIC X(444).                          
                                                                                
