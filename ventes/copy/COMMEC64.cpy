      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : VENTES EXPEDIEES                                 *        
      *  TRANSACTION: GV  EC  ...                                      *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MEC64         *        
      *  LONGUEUR   : XXX                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MEC64-APPLI.                                            00260000
         02 COMM-MEC64-ENTREE.                                          00260000
           05 COMM-MEC64-CPROG          PIC X(05).                              
           05 COMM-MEC64-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MEC64-NLIEU          PIC X(03).                      00330000
           05 COMM-MEC64-NVENTE         PIC X(07).                      00340000
           05 COMM-MEC64-NORDRE         PIC X(05).                      00340000
           05 COMM-MEC64-DJOUR          PIC X(08).                      00340000
           05 COMM-MEC64-FILIERE        PIC X(02).                      00340000
           05 COMM-MEC64-NCDEWC         PIC S9(15) PACKED-DECIMAL.              
           05 COMM-MEC64-INTERNET       PIC X(1).                               
           05 COMM-MEC64-MAJ-STOCK      PIC X(01).                              
           05 COMM-MEC64-MBS45-ETAT     PIC X(01).                              
              88 COMM-MEC64-CREATION         VALUE 'C'.                         
              88 COMM-MEC64-MODIF            VALUE 'M'.                         
           05 FILLER                    PIC X(50).                      00340000
      * DDELIV ENVOYE AU MEC68                                                  
           05 COMM-MEC64-DDELIV         PIC X(08).                      00340000
      * DDELIV ENVOYE PAR LE TEC02 LORS DE LA MAJ-DDELIV                        
           05 COMM-MEC64-DATE-MAJ       PIC X(08).                      00340000
           05 COMM-MEC64-ACTION         PIC X(02) VALUE '00'.           00340000
             88 COMM-MEC64-ACTION-CONTROLE      VALUE '00'.             00340000
             88 COMM-MEC64-ACTION-VALID-DATE    VALUE '01'.             00340000
             88 COMM-MEC64-ACTION-MAJ-DDELIV    VALUE '02'.             00340000
             88 COMM-MEC64-ACTION-SORTIE        VALUE '03'.             00340000
      *{ remove-comma-in-dde 1.5                                                
      *      88 COMM-MEC64-ACTION-TRT-CODIC                             00340000
      *         VALUE '04', '05', '08', '09', '10'.                             
      *--                                                                       
             88 COMM-MEC64-ACTION-TRT-CODIC                                     
                VALUE '04'  '05'  '08'  '09'  '10'.                             
      *}                                                                        
             88 COMM-MEC64-ACTION-VALID-CODIC   VALUE '04'.             00340000
             88 COMM-MEC64-ACTION-VALID-EC06    VALUE '05'.             00340000
             88 COMM-MEC64-ACTION-RESERVATION   VALUE '06'.             00340000
             88 COMM-MEC64-ACTION-ENCAISSEMENT  VALUE '07'.             00340000
             88 COMM-MEC64-ACTION-DEL-DDELIV    VALUE '08'.             00340000
             88 COMM-MEC64-ACTION-CONTROLE-GV   VALUE '09'.             00340000
             88 COMM-MEC64-ACTION-VALIDE-GV     VALUE '10'.             00340000
      * RETOUR                                                                  
         02 COMM-MEC64-SORTIE.                                          00260000
           05 COMM-MEC64-TYPE-NB        PIC X(01) VALUE '0'.            00340000
             88 COMM-MEC64-TYPE-NB-MONO      VALUE '0'.                 00340000
             88 COMM-MEC64-TYPE-NB-MULTI     VALUE '1'.                 00340000
             88 COMM-MEC64-TYPE-ANNUL        VALUE '9'.                 00340000
           05 COMM-EC64-ETAT-LIGNES     PIC 9(01) VALUE 0.                      
              88 COMM-EC64-LIGNE-A-TRAITER        VALUE 1.                      
           05 COMM-MEC64-STATUT         PIC 9(01).                              
              88 EC64-STATUT-COMPLETE        VALUE 0.                           
      *{ remove-comma-in-dde 1.5                                                
      *       88 EC64-STATUT-INCOMPLETE      VALUE 1, 2 , 3 , 4 , 5 , 6.        
      *--                                                                       
              88 EC64-STATUT-INCOMPLETE      VALUE 1  2   3   4   5   6.        
      *}                                                                        
              88 EC64-STATUT-ATTENTE-FOURN   VALUE 1.                           
              88 EC64-STATUT-PB-DISPO        VALUE 2.                           
              88 EC64-STATUT-ANO             VALUE 3.                           
              88 EC64-STATUT-NON-TRAITE      VALUE 4.                           
              88 EC64-STATUT-CQE-DACEM       VALUE 5.                           
              88 EC64-STATUT-DACEM-FERME     VALUE 6.                           
              88 EC64-STATUT-ERREUR-GRAVE    VALUE 9.                           
           05 COMM-MEC64-STATUT-PREC    PIC 9(01).                              
      *{ remove-comma-in-dde 1.5                                                
      *       88 EC64-STATUT-PREC-COMPLETE      VALUE 1, 3.                     
      *--                                                                       
              88 EC64-STATUT-PREC-COMPLETE      VALUE 1  3.                     
      *}                                                                        
              88 EC64-STATUT-PREC-INCOMPLETE    VALUE 0.                        
              88 EC64-STATUT-PREC-NON-DEFINI    VALUE 2.                        
              88 EC64-STATUT-PREC-OK            VALUE 3.                        
           05 COMM-MEC64-STATUT-CQE-PREC PIC 9(01).                             
              88 EC64-VTE-AVEC-CQE-PREC         VALUE 1.                        
              88 EC64-VTE-SANS-CQE-PREC         VALUE 0.                        
           05 COMM-MEC64-STATUT-CQE-NEW  PIC 9(01).                             
              88 EC64-VTE-AVEC-CQE-NEW          VALUE 1.                        
              88 EC64-VTE-SANS-CQE-NEW          VALUE 0.                        
      * CONTIENT LA DDELIV MAX DES ARTICLES DE LA VENTE QUI DEVIENDRA           
      * LA DDELIV COMMUNE LORS DE LA MISE EN INCOMPLETE                         
           05 COMM-MEC64-DDELIV-COM     PIC X(8).                               
           05 EC64-NB-DACEM-TRT         PIC 9(05).                      00340000
           05 EC64-NB-DARTY90-TRT       PIC 9(05).                      00340000
           05 EC64-NB-DARTY95-TRT       PIC 9(05).                      00340000
           05 EC64-NB-INDISPO           PIC 9(02).                      00340000
           05 EC64-MULTIDA              PIC X(01).                      00340000
           05 EC64-VENTE-CQE            PIC X(01).                      00340000
           05 COMM-MEC64-CODE-RETOUR    PIC X(04).                      00340000
           05 COMM-MEC64-MESSAGE        PIC X(58).                              
           05 COMM-MEC64-TABLEAU-NLIGNE.                                        
               06 COMM-MEC64-TAB-NLIGNE OCCURS 80.                              
                  10 COMM-MEC64-TAB-NCODIC         PIC X(7).                    
                  10 COMM-MEC64-TAB-NCODICGRP      PIC X(7).                    
                  10 COMM-MEC64-TAB-CTYPENREG      PIC X(1).                    
                  10 COMM-MEC64-TAB-CFAM           PIC X(5).                    
                  10 COMM-MEC64-TAB-NSEQ           PIC 9(2).                    
                  10 COMM-MEC64-TAB-NSEQNQ         PIC S9(5)  COMP-3.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-MEC64-TAB-QVENDUE        PIC S9(06) COMP.             
      *--                                                                       
                  10 COMM-MEC64-TAB-QVENDUE        PIC S9(06) COMP-5.           
      *}                                                                        
      * DEPOTS MFL05                                                            
                  10 COMM-MEC64-FL05.                                           
                    15 COMM-MEC64-FL05-DEPOT     OCCURS 5.                      
                      20 COMM-MEC64-FL05-NSOCDEPOT       PIC X(03).             
                      20 COMM-MEC64-FL05-NDEPOT          PIC X(03).             
      * GV11-DDELIV                                                             
                  10 COMM-MEC64-TAB-DDELIVO PIC X(8).                           
                  10 COMM-MEC64-TAB-LCOMMENT PIC X(35).                         
                  10 COMM-MEC64-TAB-TYPECDE  PIC X(5).                          
                   88 COMM-MEC64-CDEDARTY                 VALUE 'DARTY'.        
                   88 COMM-MEC64-CDEDACEM                 VALUE 'DACEM'.        
                  10 COMM-MEC64-TAB-WCQERESF PIC X(1).                          
                     88 EC64-ARTICLE-EN-CF           VALUE 'F'.                 
                     88 EC64-ARTICLE-PAS-EN-CF       VALUE ' '.                 
                  10 COMM-MEC64-TAB-WEMPORTE PIC X(1).                          
                  10 COMM-MEC64-TAB-NMUT     PIC X(7).                          
                  10 COMM-MEC64-TAB-NMUTTEMP PIC X(7).                          
                  10 COMM-MEC64-TAB-CSTATUT  PIC X(1).                          
      *{ remove-comma-in-dde 1.5                                                
      *              88 EC64-ARTICLE-EN-ATTENTE      VALUE 'A','F'.             
      *--                                                                       
                     88 EC64-ARTICLE-EN-ATTENTE      VALUE 'A' 'F'.             
      *}                                                                        
                     88 EC64-ARTICLE-RESERVE         VALUE 'R'.                 
      * -> ETAT COMMANDE FOURNISSEUR AVANT ENCAISSEMENT                         
                     88 EC64-ARTICLE-CDF-AVENC       VALUE 'F'.                 
                     88 EC64-ARTICLE-MUTE            VALUE 'M'.                 
                     88 EC64-CSTATUT-A-BLANC         VALUE ' '.                 
                  10 COMM-MEC64-TAB-CEQUIPE      PIC X(5).                      
                  10 COMM-MEC64-TAB-ETAT-RESA    PIC 9(01).                     
                     88 EC64-AUCUN-STOCK-DISPO       VALUE 0.                   
                     88 EC64-STOCK-DISPONIBLE        VALUE 1.                   
                     88 EC64-STOCK-DACEM             VALUE 2.                   
                     88 EC64-STOCK-ATT-FOURN         VALUE 3.                   
                     88 EC64-STOCK-CQE-DACEM         VALUE 5.                   
                     88 EC64-DACEM-FERME             VALUE 6.                   
                     88 EC64-DEJA-MUTE               VALUE 7.                   
                     88 EC64-STOCK-ERREUR            VALUE 9.                   
                  10 COMM-MEC64-TAB-ETAT-FOURN   PIC 9(01).                     
                     88 EC64-STOCK-FOURN-NON         VALUE 0.                   
                     88 EC64-STOCK-FOURN-OUI         VALUE 1.                   
                  10 COMM-MEC64-TAB-NSOCLIVR     PIC X(03).                     
                  10 COMM-MEC64-TAB-NDEPOT       PIC X(03).                     
                  10 COMM-MEC64-TAB-TOPE         PIC X(01).                     
           05 COMM-MEC64-ZONE-GV-MAJ.                                           
              10 COMM-MEC64-CRE-GB05-TEMPO   PIC X(01).                         
              10 COMM-MEC64-SUP-MUT-GB05     PIC X(01).                         
              10 COMM-MEC64-SUP-MUT-GB15     PIC X(01).                         
              10 COMM-MEC64-ETAT-PREC-VTE    PIC X(01).                         
      * DONNEES RESERVEES POUR LES VENTES DACEM                                 
              10 COMM-MEC64-ACT-DACEM        PIC X(01).                         
              10 COMM-MEC64-DMUT             PIC X(08).                         
              10 COMM-MEC64-FILIERE-IN       PIC X(02).                         
              10 COMM-MEC64-DATE-DISPO       PIC X(08).                         
         02 FILLER                    PIC X(141).                       00340000
                                                                                
