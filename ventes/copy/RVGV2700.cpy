      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV2700                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV2700                         
      **********************************************************                
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGV2700.                                                            
           02  GV27-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV27-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV27-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV27-DAUTORD                                                     
               PIC X(0008).                                                     
           02  GV27-DHAUTORD                                                    
               PIC X(0002).                                                     
           02  GV27-DMAUTORD                                                    
               PIC X(0002).                                                     
           02  GV27-DHAUTORDUTI                                                 
               PIC X(0002).                                                     
           02  GV27-DMAUTORDUTI                                                 
               PIC X(0002).                                                     
           02  GV27-NAUTORD                                                     
               PIC X(0007).                                                     
           02  GV27-CAUTORD                                                     
               PIC X(0005).                                                     
           02  GV27-DVENTE                                                      
               PIC X(0008).                                                     
           02  GV27-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV2700                                  
      **********************************************************                
       01  RVGV2700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DHAUTORD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DHAUTORD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DMAUTORD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DMAUTORD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DHAUTORDUTI-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DHAUTORDUTI-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DMAUTORDUTI-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DMAUTORDUTI-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-NAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-NAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-CAUTORD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-CAUTORD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV27-DVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV27-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV27-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
