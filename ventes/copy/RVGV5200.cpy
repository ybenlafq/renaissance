      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV5200                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV5200                         
      **********************************************************                
      * POUR PASSAGE A L'EURO AJOUT DE 2 DECIMALES AUX MONTANTS*                
      * DATE : 07/01    AUTEUR:DENIS COIFFIER  MARQUE:DC01     *                
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV5200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV5200.                                                            
      *}                                                                        
           02  GV52-NSOCMERE                                                    
               PIC X(0003).                                                     
           02  GV52-NMAGMGI                                                     
               PIC X(0003).                                                     
           02  GV52-CFAMMGI                                                     
               PIC X(0005).                                                     
           02  GV52-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV52-DMOISVENTE                                                  
               PIC X(0006).                                                     
           02  GV52-QVENDUE                                                     
               PIC S9(7) COMP-3.                                                
DC01  *    02  GV52-PCAHT                                                       
DC01  *        PIC S9(9) COMP-3.                                                
DC01  *    02  GV52-PMBHTMGI                                                    
DC01  *        PIC S9(7) COMP-3.                                                
DC01  *    02  GV52-PMBHTTOT                                                    
DC01  *        PIC S9(7) COMP-3.                                                
DC01       02  GV52-PCAHT                                                       
DC01           PIC S9(9)V99 COMP-3.                                             
DC01       02  GV52-PMBHTMGI                                                    
DC01           PIC S9(7)V99 COMP-3.                                             
DC01       02  GV52-PMBHTTOT                                                    
DC01           PIC S9(7)V99 COMP-3.                                             
           02  GV52-DSYST                                                       
               PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV5200                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV5200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV5200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-NSOCMERE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-NSOCMERE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-NMAGMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-NMAGMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-CFAMMGI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-CFAMMGI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-DMOISVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-DMOISVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-PCAHT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-PCAHT-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-PMBHTMGI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-PMBHTMGI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-PMBHTTOT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV52-PMBHTTOT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV52-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  GV52-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
