      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EC009 CORR CIVILITE   DARTY.COM->NCG   *        
      *----------------------------------------------------------------*        
       01  RVEC009.                                                             
           05  EC009-CTABLEG2    PIC X(15).                                     
           05  EC009-CTABLEG2-REDEF REDEFINES EC009-CTABLEG2.                   
               10  EC009-ECOM            PIC X(15).                             
           05  EC009-WTABLEG     PIC X(80).                                     
           05  EC009-WTABLEG-REDEF  REDEFINES EC009-WTABLEG.                    
               10  EC009-NCG             PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEC009-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC009-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EC009-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EC009-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EC009-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
