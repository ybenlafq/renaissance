      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
ECOM   TRT-ECOM                   SECTION.                                      
ECOM  *------------------------------------*                                    
ECOM  * ALIMENTATION SPECIFIQUE DE LA ZONE DE COM SELON LE TYPE D'APPEL         
ECOM       PERFORM    ALIM-ZONE-MEC04C .                                        
ECOM  * APPEL DU MODULE MAJ PRIX INTERNET                                       
ECOM       PERFORM    LINK-MEC04 .                                              
ECOM                                                                            
ECOM   FIN-TRT-ECOM.  EXIT.                                                     
ECOM                                                                            
ECOM   ALIM-ZONE-MEC04C           SECTION.                                      
ECOM  *------------------------------------*                                    
ECOM       INITIALIZE  MEC04C-COMMAREA .                                        
ECOM       MOVE '0' TO MEC04C-CODE-RETOUR .                                     
ECOM  * ALIMENTATION SPECIFIQUE DE LA ZONE DE COM SELON LE TYPE D'APPEL         
ECOM       EVALUATE NUM-APPEL                                                   
ECOM        WHEN '01'                                                           
ECOM               MOVE 'RTGG20'             TO MEC04C-TABLE                    
ECOM               MOVE 'INSERT'             TO MEC04C-ORDRE-DB2                
ECOM               MOVE GG20-NSOCIETE        TO MEC04C-SOCIETE                  
ECOM               MOVE GG20-NLIEU           TO MEC04C-LIEU                     
ECOM               MOVE GG20-NCODIC          TO MEC04C-CODIC                    
ECOM               MOVE GG20-DEFFET          TO MEC04C-DATE-EFFET               
ECOM               MOVE GG20-PEXPTTC         TO MEC04C-PRIX                     
ECOM        WHEN '02'                                                           
ECOM               MOVE 'RTGA59'             TO MEC04C-TABLE                    
ECOM               MOVE 'INSERT'             TO MEC04C-ORDRE-DB2                
ECOM               MOVE GA59-NZONPRIX        TO MEC04C-ZONE-PRIX                
ECOM               MOVE GA59-NCODIC          TO MEC04C-CODIC                    
ECOM               MOVE GA59-PSTDTTC         TO MEC04C-PRIX                     
ECOM               MOVE GA59-DEFFET          TO MEC04C-DATE-EFFET               
ECOM       END-EVALUATE .                                                       
ECOM  * ALIMENTATION COMMUNE AUX TYPES D'APPEL                                  
ECOM       MOVE COMM-DATE-SSAAMMJJ   TO MEC04C-DATE-JOUR .                      
T     *    STRING '---ALIM MEC04 APPEL ' NUM-APPEL ' '                          
T     *    MEC04C-DATA-ENTREE(1:43)                                             
T     *    DELIMITED BY SIZE    INTO W-MESSAGE.                                 
T     *    PERFORM DIS-MES .                                                    
ECOM                                                                            
ECOM   FIN-ALIM.  EXIT.                                                         
ECOM                                                                            
ECOM   LINK-MEC04        SECTION.                                               
ECOM  *----------------------------*                                            
ECOM       MOVE LENGTH OF MEC04C-COMMAREA    TO LONG-COMMAREA-LINK              
ECOM       MOVE 'MEC04'                      TO NOM-PROG-LINK                   
ECOM       EXEC CICS LINK  PROGRAM  (NOM-PROG-LINK)                             
ECOM                 COMMAREA (MEC04C-COMMAREA)                                 
ECOM                 LENGTH   (LONG-COMMAREA-LINK)                              
ECOM                 NOHANDLE                                                   
ECOM       END-EXEC.                                                            
ECOM                                                                    00002000
ECOM       MOVE EIBRCODE TO EIB-RCODE.                                  00002100
ECOM       IF   NOT EIB-NORMAL                                          00002200
ECOM            GO TO ABANDON-CICS                                      00002300
ECOM       END-IF.                                                              
ECOM                                                                            
ECOM       IF ( MEC04C-CODE-RETOUR > 0 )                                        
ECOM          MOVE SPACES TO MESS                                               
ECOM          STRING 'MEC04-CDRET:'   MEC04C-CODE-RETOUR                        
ECOM          ' SQL:' MEC04C-CODE-DB2-DISPLAY ' MES:'                           
ECOM           MEC04C-MESSAGE(1:450)                                            
ECOM          DELIMITED BY SIZE INTO MESS                                       
ECOM          GO TO ABANDON-TACHE                                               
T     *    ELSE                                                                 
T     *       MOVE SPACES TO W-MESSAGE                                          
T     *       STRING '---RETOUR MEC04: '   MEC04C-DATA-SORTIE(1:23)             
T     *       DELIMITED BY SIZE    INTO W-MESSAGE                               
T     *       PERFORM DIS-MES                                                   
ECOM       END-IF.                                                              
ECOM   FIN-LINK.  EXIT.                                                         
T     *================================                                         
T     *DIS-MES                  SECTION.                                        
T     *=======================                                                  
T     *                                                                         
T     *    EXEC CICS                                                            
T     *         WRITE OPERATOR TEXT (W-MESSAGE) NOHANDLE                        
T     *    END-EXEC.                                                            
T     *    MOVE SPACES TO W-MESSAGE .                                           
T     *                                                                         
T     *FIN-DIS.  EXIT.                                                          
                                                                                
