      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV82   EGV82                                              00000020
      ***************************************************************** 00000030
       01   EGV82I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * PAGE ECRAN                                                      00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWPAGEI   PIC X(3).                                       00000200
      * MESSAGE DE CONFI                                                00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMESS-CONFL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MMESS-CONFL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MMESS-CONFF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MMESS-CONFI    PIC X(63).                                 00000250
      * MAGASIN TRAITE                                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000270
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNLIEUORIGI    PIC X(3).                                  00000300
      * POINTAGE GLOBAL                                                 00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZPGLOBALL     COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MZPGLOBALL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MZPGLOBALF     PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MZPGLOBALI     PIC X.                                     00000350
           02 MBESI OCCURS   15 TIMES .                                 00000360
      * DETAIL DE BE                                                    00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-BEL   COMP PIC S9(4).                            00000380
      *--                                                                       
             03 MLIGNE-BEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIGNE-BEF   PIC X.                                     00000390
             03 FILLER  PIC X(4).                                       00000400
             03 MLIGNE-BEI   PIC X(76).                                 00000410
      * POINTAGE LIGNE                                                  00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MZPLIGNEL    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MZPLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MZPLIGNEF    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MZPLIGNEI    PIC X.                                     00000460
      * ZONE CMD AIDA                                                   00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MZONCMDI  PIC X(15).                                      00000510
      * MESSAGE ERREUR                                                  00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(58).                                      00000560
      * CODE TRANSACTION                                                00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCODTRAI  PIC X(4).                                       00000610
      * CICS DE TRAVAIL                                                 00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000630
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MCICSI    PIC X(5).                                       00000660
      * NETNAME                                                         00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      * CODE TERMINAL                                                   00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MSCREENI  PIC X(5).                                       00000760
      ***************************************************************** 00000770
      * SDF: EGV82   EGV82                                              00000780
      ***************************************************************** 00000790
       01   EGV82O REDEFINES EGV82I.                                    00000800
           02 FILLER    PIC X(12).                                      00000810
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
      * HEURE                                                           00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MTIMJOUA  PIC X.                                          00000920
           02 MTIMJOUC  PIC X.                                          00000930
           02 MTIMJOUP  PIC X.                                          00000940
           02 MTIMJOUH  PIC X.                                          00000950
           02 MTIMJOUV  PIC X.                                          00000960
           02 MTIMJOUO  PIC X(5).                                       00000970
      * PAGE ECRAN                                                      00000980
           02 FILLER    PIC X(2).                                       00000990
           02 MWPAGEA   PIC X.                                          00001000
           02 MWPAGEC   PIC X.                                          00001010
           02 MWPAGEP   PIC X.                                          00001020
           02 MWPAGEH   PIC X.                                          00001030
           02 MWPAGEV   PIC X.                                          00001040
           02 MWPAGEO   PIC ZZ9.                                        00001050
      * MESSAGE DE CONFI                                                00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MMESS-CONFA    PIC X.                                     00001080
           02 MMESS-CONFC    PIC X.                                     00001090
           02 MMESS-CONFP    PIC X.                                     00001100
           02 MMESS-CONFH    PIC X.                                     00001110
           02 MMESS-CONFV    PIC X.                                     00001120
           02 MMESS-CONFO    PIC X(63).                                 00001130
      * MAGASIN TRAITE                                                  00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MNLIEUORIGA    PIC X.                                     00001160
           02 MNLIEUORIGC    PIC X.                                     00001170
           02 MNLIEUORIGP    PIC X.                                     00001180
           02 MNLIEUORIGH    PIC X.                                     00001190
           02 MNLIEUORIGV    PIC X.                                     00001200
           02 MNLIEUORIGO    PIC X(3).                                  00001210
      * POINTAGE GLOBAL                                                 00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MZPGLOBALA     PIC X.                                     00001240
           02 MZPGLOBALC     PIC X.                                     00001250
           02 MZPGLOBALP     PIC X.                                     00001260
           02 MZPGLOBALH     PIC X.                                     00001270
           02 MZPGLOBALV     PIC X.                                     00001280
           02 MZPGLOBALO     PIC X.                                     00001290
           02 MBESO OCCURS   15 TIMES .                                 00001300
      * DETAIL DE BE                                                    00001310
             03 FILLER       PIC X(2).                                  00001320
             03 MLIGNE-BEA   PIC X.                                     00001330
             03 MLIGNE-BEC   PIC X.                                     00001340
             03 MLIGNE-BEP   PIC X.                                     00001350
             03 MLIGNE-BEH   PIC X.                                     00001360
             03 MLIGNE-BEV   PIC X.                                     00001370
             03 MLIGNE-BEO   PIC X(76).                                 00001380
      * POINTAGE LIGNE                                                  00001390
             03 FILLER       PIC X(2).                                  00001400
             03 MZPLIGNEA    PIC X.                                     00001410
             03 MZPLIGNEC    PIC X.                                     00001420
             03 MZPLIGNEP    PIC X.                                     00001430
             03 MZPLIGNEH    PIC X.                                     00001440
             03 MZPLIGNEV    PIC X.                                     00001450
             03 MZPLIGNEO    PIC X.                                     00001460
      * ZONE CMD AIDA                                                   00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MZONCMDA  PIC X.                                          00001490
           02 MZONCMDC  PIC X.                                          00001500
           02 MZONCMDP  PIC X.                                          00001510
           02 MZONCMDH  PIC X.                                          00001520
           02 MZONCMDV  PIC X.                                          00001530
           02 MZONCMDO  PIC X(15).                                      00001540
      * MESSAGE ERREUR                                                  00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRP  PIC X.                                          00001590
           02 MLIBERRH  PIC X.                                          00001600
           02 MLIBERRV  PIC X.                                          00001610
           02 MLIBERRO  PIC X(58).                                      00001620
      * CODE TRANSACTION                                                00001630
           02 FILLER    PIC X(2).                                       00001640
           02 MCODTRAA  PIC X.                                          00001650
           02 MCODTRAC  PIC X.                                          00001660
           02 MCODTRAP  PIC X.                                          00001670
           02 MCODTRAH  PIC X.                                          00001680
           02 MCODTRAV  PIC X.                                          00001690
           02 MCODTRAO  PIC X(4).                                       00001700
      * CICS DE TRAVAIL                                                 00001710
           02 FILLER    PIC X(2).                                       00001720
           02 MCICSA    PIC X.                                          00001730
           02 MCICSC    PIC X.                                          00001740
           02 MCICSP    PIC X.                                          00001750
           02 MCICSH    PIC X.                                          00001760
           02 MCICSV    PIC X.                                          00001770
           02 MCICSO    PIC X(5).                                       00001780
      * NETNAME                                                         00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNETNAMA  PIC X.                                          00001810
           02 MNETNAMC  PIC X.                                          00001820
           02 MNETNAMP  PIC X.                                          00001830
           02 MNETNAMH  PIC X.                                          00001840
           02 MNETNAMV  PIC X.                                          00001850
           02 MNETNAMO  PIC X(8).                                       00001860
      * CODE TERMINAL                                                   00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MSCREENA  PIC X.                                          00001890
           02 MSCREENC  PIC X.                                          00001900
           02 MSCREENP  PIC X.                                          00001910
           02 MSCREENH  PIC X.                                          00001920
           02 MSCREENV  PIC X.                                          00001930
           02 MSCREENO  PIC X(5).                                       00001940
                                                                                
