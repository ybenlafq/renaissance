      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESL54   ESL54                                              00000020
      ***************************************************************** 00000030
       01   ESL54I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODACTL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MCODACTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODACTF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MCODACTI  PIC X.                                          00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MDATDEBI  PIC X(6).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATFINI  PIC X(6).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MENVOIL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MENVOIL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MENVOIF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MENVOII   PIC X.                                          00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPENVOIL     COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MTYPENVOIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MTYPENVOIF     PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTYPENVOII     PIC X(15).                                 00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSTATUTL      COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MCSTATUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCSTATUTF      PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCSTATUTI      PIC X.                                     00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSOCL    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MCSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCSOCF    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MCSOCI    PIC X(3).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLIEUL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLIEUF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCLIEUI   PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNVENTEI  PIC X(7).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MZONCMDI  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MLIBERRI  PIC X(78).                                      00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCODTRAI  PIC X(4).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCICSI    PIC X(5).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MNETNAMI  PIC X(8).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MSCREENI  PIC X(4).                                       00000770
      ***************************************************************** 00000780
      * SDF: ESL54   ESL54                                              00000790
      ***************************************************************** 00000800
       01   ESL54O REDEFINES ESL54I.                                    00000810
           02 FILLER    PIC X(12).                                      00000820
           02 FILLER    PIC X(2).                                       00000830
           02 MDATJOUA  PIC X.                                          00000840
           02 MDATJOUC  PIC X.                                          00000850
           02 MDATJOUP  PIC X.                                          00000860
           02 MDATJOUH  PIC X.                                          00000870
           02 MDATJOUV  PIC X.                                          00000880
           02 MDATJOUO  PIC X(10).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MTIMJOUA  PIC X.                                          00000910
           02 MTIMJOUC  PIC X.                                          00000920
           02 MTIMJOUP  PIC X.                                          00000930
           02 MTIMJOUH  PIC X.                                          00000940
           02 MTIMJOUV  PIC X.                                          00000950
           02 MTIMJOUO  PIC X(5).                                       00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MNCODICA  PIC X.                                          00000980
           02 MNCODICC  PIC X.                                          00000990
           02 MNCODICP  PIC X.                                          00001000
           02 MNCODICH  PIC X.                                          00001010
           02 MNCODICV  PIC X.                                          00001020
           02 MNCODICO  PIC X(7).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MCODACTA  PIC X.                                          00001050
           02 MCODACTC  PIC X.                                          00001060
           02 MCODACTP  PIC X.                                          00001070
           02 MCODACTH  PIC X.                                          00001080
           02 MCODACTV  PIC X.                                          00001090
           02 MCODACTO  PIC X.                                          00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MDATDEBA  PIC X.                                          00001120
           02 MDATDEBC  PIC X.                                          00001130
           02 MDATDEBP  PIC X.                                          00001140
           02 MDATDEBH  PIC X.                                          00001150
           02 MDATDEBV  PIC X.                                          00001160
           02 MDATDEBO  PIC X(6).                                       00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MDATFINA  PIC X.                                          00001190
           02 MDATFINC  PIC X.                                          00001200
           02 MDATFINP  PIC X.                                          00001210
           02 MDATFINH  PIC X.                                          00001220
           02 MDATFINV  PIC X.                                          00001230
           02 MDATFINO  PIC X(6).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MENVOIA   PIC X.                                          00001260
           02 MENVOIC   PIC X.                                          00001270
           02 MENVOIP   PIC X.                                          00001280
           02 MENVOIH   PIC X.                                          00001290
           02 MENVOIV   PIC X.                                          00001300
           02 MENVOIO   PIC X.                                          00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MTYPENVOIA     PIC X.                                     00001330
           02 MTYPENVOIC     PIC X.                                     00001340
           02 MTYPENVOIP     PIC X.                                     00001350
           02 MTYPENVOIH     PIC X.                                     00001360
           02 MTYPENVOIV     PIC X.                                     00001370
           02 MTYPENVOIO     PIC X(15).                                 00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCSTATUTA      PIC X.                                     00001400
           02 MCSTATUTC PIC X.                                          00001410
           02 MCSTATUTP PIC X.                                          00001420
           02 MCSTATUTH PIC X.                                          00001430
           02 MCSTATUTV PIC X.                                          00001440
           02 MCSTATUTO      PIC X.                                     00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MCSOCA    PIC X.                                          00001470
           02 MCSOCC    PIC X.                                          00001480
           02 MCSOCP    PIC X.                                          00001490
           02 MCSOCH    PIC X.                                          00001500
           02 MCSOCV    PIC X.                                          00001510
           02 MCSOCO    PIC X(3).                                       00001520
           02 FILLER    PIC X(2).                                       00001530
           02 MCLIEUA   PIC X.                                          00001540
           02 MCLIEUC   PIC X.                                          00001550
           02 MCLIEUP   PIC X.                                          00001560
           02 MCLIEUH   PIC X.                                          00001570
           02 MCLIEUV   PIC X.                                          00001580
           02 MCLIEUO   PIC X(3).                                       00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MNVENTEA  PIC X.                                          00001610
           02 MNVENTEC  PIC X.                                          00001620
           02 MNVENTEP  PIC X.                                          00001630
           02 MNVENTEH  PIC X.                                          00001640
           02 MNVENTEV  PIC X.                                          00001650
           02 MNVENTEO  PIC X(7).                                       00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MZONCMDA  PIC X.                                          00001680
           02 MZONCMDC  PIC X.                                          00001690
           02 MZONCMDP  PIC X.                                          00001700
           02 MZONCMDH  PIC X.                                          00001710
           02 MZONCMDV  PIC X.                                          00001720
           02 MZONCMDO  PIC X.                                          00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MLIBERRA  PIC X.                                          00001750
           02 MLIBERRC  PIC X.                                          00001760
           02 MLIBERRP  PIC X.                                          00001770
           02 MLIBERRH  PIC X.                                          00001780
           02 MLIBERRV  PIC X.                                          00001790
           02 MLIBERRO  PIC X(78).                                      00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCODTRAA  PIC X.                                          00001820
           02 MCODTRAC  PIC X.                                          00001830
           02 MCODTRAP  PIC X.                                          00001840
           02 MCODTRAH  PIC X.                                          00001850
           02 MCODTRAV  PIC X.                                          00001860
           02 MCODTRAO  PIC X(4).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MCICSA    PIC X.                                          00001890
           02 MCICSC    PIC X.                                          00001900
           02 MCICSP    PIC X.                                          00001910
           02 MCICSH    PIC X.                                          00001920
           02 MCICSV    PIC X.                                          00001930
           02 MCICSO    PIC X(5).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MNETNAMA  PIC X.                                          00001960
           02 MNETNAMC  PIC X.                                          00001970
           02 MNETNAMP  PIC X.                                          00001980
           02 MNETNAMH  PIC X.                                          00001990
           02 MNETNAMV  PIC X.                                          00002000
           02 MNETNAMO  PIC X(8).                                       00002010
           02 FILLER    PIC X(2).                                       00002020
           02 MSCREENA  PIC X.                                          00002030
           02 MSCREENC  PIC X.                                          00002040
           02 MSCREENP  PIC X.                                          00002050
           02 MSCREENH  PIC X.                                          00002060
           02 MSCREENV  PIC X.                                          00002070
           02 MSCREENO  PIC X(4).                                       00002080
                                                                                
