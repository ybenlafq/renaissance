      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EVT47   EVT47                                              00000020
      ***************************************************************** 00000030
       01   EVT47I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MTITREI   PIC X(25).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MWPAGEI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMAGL     COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MMAGL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MMAGF     PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MMAGI     PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNVENTEI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVENTEL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MDVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVENTEF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MDVENTEI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNORDREI  PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMODIFL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDMODIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDMODIFF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDMODIFI  PIC X(8).                                       00000410
           02 MTABLEI OCCURS   8 TIMES .                                00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMQEL   COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MMQEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MMQEF   PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MMQEI   PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MREFMQEL     COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MREFMQEL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MREFMQEF     PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MREFMQEI     PIC X(20).                                 00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNCODICL     COMP PIC S9(4).                            00000510
      *--                                                                       
             03 MNCODICL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNCODICF     PIC X.                                     00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNCODICI     PIC X(7).                                  00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MQTEL   COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MQTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MQTEF   PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MQTEI   PIC X(3).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAR1L  COMP PIC S9(4).                                 00000590
      *--                                                                       
             03 MCAR1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAR1F  PIC X.                                          00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCAR1I  PIC X(5).                                       00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAR2L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MCAR2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAR2F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MCAR2I  PIC X(5).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAR3L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MCAR3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAR3F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MCAR3I  PIC X(5).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAR4L  COMP PIC S9(4).                                 00000710
      *--                                                                       
             03 MCAR4L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAR4F  PIC X.                                          00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MCAR4I  PIC X(5).                                       00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAR5L  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MCAR5L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MCAR5F  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCAR5I  PIC X(5).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVEND1L  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MCVEND1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVEND1F  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MCVEND1I  PIC X(6).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVEND1L  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLVEND1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVEND1F  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLVEND1I  PIC X(15).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMM1L  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLCOMM1L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMM1F  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLCOMM1I  PIC X(30).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDELL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MEDELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MEDELF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MEDELI    PIC X(3).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVEND2L  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCVEND2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVEND2F  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCVEND2I  PIC X(6).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVEND2L  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MLVEND2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVEND2F  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MLVEND2I  PIC X(15).                                      00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMM2L  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MLCOMM2L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMM2F  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MLCOMM2I  PIC X(30).                                      00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDATEL   COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MEDATEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDATEF   PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MEDATEI   PIC X(6).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVEND3L  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MCVEND3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVEND3F  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MCVEND3I  PIC X(6).                                       00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVEND3L  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MLVEND3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVEND3F  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MLVEND3I  PIC X(15).                                      00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMM3L  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MLCOMM3L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMM3F  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MLCOMM3I  PIC X(30).                                      00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEPLL     COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MEPLL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MEPLF     PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MEPLI     PIC X(2).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVEND4L  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MCVEND4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVEND4F  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MCVEND4I  PIC X(6).                                       00001300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLVEND4L  COMP PIC S9(4).                                 00001310
      *--                                                                       
           02 MLVEND4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLVEND4F  PIC X.                                          00001320
           02 FILLER    PIC X(4).                                       00001330
           02 MLVEND4I  PIC X(15).                                      00001340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMM4L  COMP PIC S9(4).                                 00001350
      *--                                                                       
           02 MLCOMM4L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLCOMM4F  PIC X.                                          00001360
           02 FILLER    PIC X(4).                                       00001370
           02 MLCOMM4I  PIC X(30).                                      00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAUTORML      COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MLAUTORML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLAUTORMF      PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MLAUTORMI      PIC X(5).                                  00001420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUTORDL      COMP PIC S9(4).                            00001430
      *--                                                                       
           02 MNAUTORDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAUTORDF      PIC X.                                     00001440
           02 FILLER    PIC X(4).                                       00001450
           02 MNAUTORDI      PIC X(7).                                  00001460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQVENTEL  COMP PIC S9(4).                                 00001470
      *--                                                                       
           02 MQVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQVENTEF  PIC X.                                          00001480
           02 FILLER    PIC X(4).                                       00001490
           02 MQVENTEI  PIC X(9).                                       00001500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQCOMPTL  COMP PIC S9(4).                                 00001510
      *--                                                                       
           02 MQCOMPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MQCOMPTF  PIC X.                                          00001520
           02 FILLER    PIC X(4).                                       00001530
           02 MQCOMPTI  PIC X(9).                                       00001540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQLIVRL   COMP PIC S9(4).                                 00001550
      *--                                                                       
           02 MQLIVRL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQLIVRF   PIC X.                                          00001560
           02 FILLER    PIC X(4).                                       00001570
           02 MQLIVRI   PIC X(9).                                       00001580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQDIFFL   COMP PIC S9(4).                                 00001590
      *--                                                                       
           02 MQDIFFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQDIFFF   PIC X.                                          00001600
           02 FILLER    PIC X(4).                                       00001610
           02 MQDIFFI   PIC X(9).                                       00001620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQRECFL   COMP PIC S9(4).                                 00001630
      *--                                                                       
           02 MQRECFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MQRECFF   PIC X.                                          00001640
           02 FILLER    PIC X(4).                                       00001650
           02 MQRECFI   PIC X(9).                                       00001660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFACL    COMP PIC S9(4).                                 00001670
      *--                                                                       
           02 MCFACL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFACF    PIC X.                                          00001680
           02 FILLER    PIC X(4).                                       00001690
           02 MCFACI    PIC X.                                          00001700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCCEEL    COMP PIC S9(4).                                 00001710
      *--                                                                       
           02 MCCEEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCCEEF    PIC X.                                          00001720
           02 FILLER    PIC X(4).                                       00001730
           02 MCCEEI    PIC X.                                          00001740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEXPL    COMP PIC S9(4).                                 00001750
      *--                                                                       
           02 MCEXPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCEXPF    PIC X.                                          00001760
           02 FILLER    PIC X(4).                                       00001770
           02 MCEXPI    PIC X.                                          00001780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCBLSPL   COMP PIC S9(4).                                 00001790
      *--                                                                       
           02 MCBLSPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCBLSPF   PIC X.                                          00001800
           02 FILLER    PIC X(4).                                       00001810
           02 MCBLSPI   PIC X.                                          00001820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001830
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001840
           02 FILLER    PIC X(4).                                       00001850
           02 MZONCMDI  PIC X(15).                                      00001860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001880
           02 FILLER    PIC X(4).                                       00001890
           02 MLIBERRI  PIC X(58).                                      00001900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001920
           02 FILLER    PIC X(4).                                       00001930
           02 MCODTRAI  PIC X(4).                                       00001940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001960
           02 FILLER    PIC X(4).                                       00001970
           02 MCICSI    PIC X(5).                                       00001980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00002000
           02 FILLER    PIC X(4).                                       00002010
           02 MNETNAMI  PIC X(8).                                       00002020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00002030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00002040
           02 FILLER    PIC X(4).                                       00002050
           02 MSCREENI  PIC X(4).                                       00002060
      ***************************************************************** 00002070
      * SDF: EVT47   EVT47                                              00002080
      ***************************************************************** 00002090
       01   EVT47O REDEFINES EVT47I.                                    00002100
           02 FILLER    PIC X(12).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MDATJOUA  PIC X.                                          00002130
           02 MDATJOUC  PIC X.                                          00002140
           02 MDATJOUP  PIC X.                                          00002150
           02 MDATJOUH  PIC X.                                          00002160
           02 MDATJOUV  PIC X.                                          00002170
           02 MDATJOUO  PIC X(10).                                      00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MTIMJOUA  PIC X.                                          00002200
           02 MTIMJOUC  PIC X.                                          00002210
           02 MTIMJOUP  PIC X.                                          00002220
           02 MTIMJOUH  PIC X.                                          00002230
           02 MTIMJOUV  PIC X.                                          00002240
           02 MTIMJOUO  PIC X(5).                                       00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MTITREA   PIC X.                                          00002270
           02 MTITREC   PIC X.                                          00002280
           02 MTITREP   PIC X.                                          00002290
           02 MTITREH   PIC X.                                          00002300
           02 MTITREV   PIC X.                                          00002310
           02 MTITREO   PIC X(25).                                      00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MWPAGEA   PIC X.                                          00002340
           02 MWPAGEC   PIC X.                                          00002350
           02 MWPAGEP   PIC X.                                          00002360
           02 MWPAGEH   PIC X.                                          00002370
           02 MWPAGEV   PIC X.                                          00002380
           02 MWPAGEO   PIC X(3).                                       00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MMAGA     PIC X.                                          00002410
           02 MMAGC     PIC X.                                          00002420
           02 MMAGP     PIC X.                                          00002430
           02 MMAGH     PIC X.                                          00002440
           02 MMAGV     PIC X.                                          00002450
           02 MMAGO     PIC X(3).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MNVENTEA  PIC X.                                          00002480
           02 MNVENTEC  PIC X.                                          00002490
           02 MNVENTEP  PIC X.                                          00002500
           02 MNVENTEH  PIC X.                                          00002510
           02 MNVENTEV  PIC X.                                          00002520
           02 MNVENTEO  PIC X(7).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MDVENTEA  PIC X.                                          00002550
           02 MDVENTEC  PIC X.                                          00002560
           02 MDVENTEP  PIC X.                                          00002570
           02 MDVENTEH  PIC X.                                          00002580
           02 MDVENTEV  PIC X.                                          00002590
           02 MDVENTEO  PIC X(8).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MNORDREA  PIC X.                                          00002620
           02 MNORDREC  PIC X.                                          00002630
           02 MNORDREP  PIC X.                                          00002640
           02 MNORDREH  PIC X.                                          00002650
           02 MNORDREV  PIC X.                                          00002660
           02 MNORDREO  PIC X(5).                                       00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MDMODIFA  PIC X.                                          00002690
           02 MDMODIFC  PIC X.                                          00002700
           02 MDMODIFP  PIC X.                                          00002710
           02 MDMODIFH  PIC X.                                          00002720
           02 MDMODIFV  PIC X.                                          00002730
           02 MDMODIFO  PIC X(8).                                       00002740
           02 MTABLEO OCCURS   8 TIMES .                                00002750
             03 FILLER       PIC X(2).                                  00002760
             03 MMQEA   PIC X.                                          00002770
             03 MMQEC   PIC X.                                          00002780
             03 MMQEP   PIC X.                                          00002790
             03 MMQEH   PIC X.                                          00002800
             03 MMQEV   PIC X.                                          00002810
             03 MMQEO   PIC X(5).                                       00002820
             03 FILLER       PIC X(2).                                  00002830
             03 MREFMQEA     PIC X.                                     00002840
             03 MREFMQEC     PIC X.                                     00002850
             03 MREFMQEP     PIC X.                                     00002860
             03 MREFMQEH     PIC X.                                     00002870
             03 MREFMQEV     PIC X.                                     00002880
             03 MREFMQEO     PIC X(20).                                 00002890
             03 FILLER       PIC X(2).                                  00002900
             03 MNCODICA     PIC X.                                     00002910
             03 MNCODICC     PIC X.                                     00002920
             03 MNCODICP     PIC X.                                     00002930
             03 MNCODICH     PIC X.                                     00002940
             03 MNCODICV     PIC X.                                     00002950
             03 MNCODICO     PIC X(7).                                  00002960
             03 FILLER       PIC X(2).                                  00002970
             03 MQTEA   PIC X.                                          00002980
             03 MQTEC   PIC X.                                          00002990
             03 MQTEP   PIC X.                                          00003000
             03 MQTEH   PIC X.                                          00003010
             03 MQTEV   PIC X.                                          00003020
             03 MQTEO   PIC X(3).                                       00003030
             03 FILLER       PIC X(2).                                  00003040
             03 MCAR1A  PIC X.                                          00003050
             03 MCAR1C  PIC X.                                          00003060
             03 MCAR1P  PIC X.                                          00003070
             03 MCAR1H  PIC X.                                          00003080
             03 MCAR1V  PIC X.                                          00003090
             03 MCAR1O  PIC X(5).                                       00003100
             03 FILLER       PIC X(2).                                  00003110
             03 MCAR2A  PIC X.                                          00003120
             03 MCAR2C  PIC X.                                          00003130
             03 MCAR2P  PIC X.                                          00003140
             03 MCAR2H  PIC X.                                          00003150
             03 MCAR2V  PIC X.                                          00003160
             03 MCAR2O  PIC X(5).                                       00003170
             03 FILLER       PIC X(2).                                  00003180
             03 MCAR3A  PIC X.                                          00003190
             03 MCAR3C  PIC X.                                          00003200
             03 MCAR3P  PIC X.                                          00003210
             03 MCAR3H  PIC X.                                          00003220
             03 MCAR3V  PIC X.                                          00003230
             03 MCAR3O  PIC X(5).                                       00003240
             03 FILLER       PIC X(2).                                  00003250
             03 MCAR4A  PIC X.                                          00003260
             03 MCAR4C  PIC X.                                          00003270
             03 MCAR4P  PIC X.                                          00003280
             03 MCAR4H  PIC X.                                          00003290
             03 MCAR4V  PIC X.                                          00003300
             03 MCAR4O  PIC X(5).                                       00003310
             03 FILLER       PIC X(2).                                  00003320
             03 MCAR5A  PIC X.                                          00003330
             03 MCAR5C  PIC X.                                          00003340
             03 MCAR5P  PIC X.                                          00003350
             03 MCAR5H  PIC X.                                          00003360
             03 MCAR5V  PIC X.                                          00003370
             03 MCAR5O  PIC X(5).                                       00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MCVEND1A  PIC X.                                          00003400
           02 MCVEND1C  PIC X.                                          00003410
           02 MCVEND1P  PIC X.                                          00003420
           02 MCVEND1H  PIC X.                                          00003430
           02 MCVEND1V  PIC X.                                          00003440
           02 MCVEND1O  PIC X(6).                                       00003450
           02 FILLER    PIC X(2).                                       00003460
           02 MLVEND1A  PIC X.                                          00003470
           02 MLVEND1C  PIC X.                                          00003480
           02 MLVEND1P  PIC X.                                          00003490
           02 MLVEND1H  PIC X.                                          00003500
           02 MLVEND1V  PIC X.                                          00003510
           02 MLVEND1O  PIC X(15).                                      00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MLCOMM1A  PIC X.                                          00003540
           02 MLCOMM1C  PIC X.                                          00003550
           02 MLCOMM1P  PIC X.                                          00003560
           02 MLCOMM1H  PIC X.                                          00003570
           02 MLCOMM1V  PIC X.                                          00003580
           02 MLCOMM1O  PIC X(30).                                      00003590
           02 FILLER    PIC X(2).                                       00003600
           02 MEDELA    PIC X.                                          00003610
           02 MEDELC    PIC X.                                          00003620
           02 MEDELP    PIC X.                                          00003630
           02 MEDELH    PIC X.                                          00003640
           02 MEDELV    PIC X.                                          00003650
           02 MEDELO    PIC X(3).                                       00003660
           02 FILLER    PIC X(2).                                       00003670
           02 MCVEND2A  PIC X.                                          00003680
           02 MCVEND2C  PIC X.                                          00003690
           02 MCVEND2P  PIC X.                                          00003700
           02 MCVEND2H  PIC X.                                          00003710
           02 MCVEND2V  PIC X.                                          00003720
           02 MCVEND2O  PIC X(6).                                       00003730
           02 FILLER    PIC X(2).                                       00003740
           02 MLVEND2A  PIC X.                                          00003750
           02 MLVEND2C  PIC X.                                          00003760
           02 MLVEND2P  PIC X.                                          00003770
           02 MLVEND2H  PIC X.                                          00003780
           02 MLVEND2V  PIC X.                                          00003790
           02 MLVEND2O  PIC X(15).                                      00003800
           02 FILLER    PIC X(2).                                       00003810
           02 MLCOMM2A  PIC X.                                          00003820
           02 MLCOMM2C  PIC X.                                          00003830
           02 MLCOMM2P  PIC X.                                          00003840
           02 MLCOMM2H  PIC X.                                          00003850
           02 MLCOMM2V  PIC X.                                          00003860
           02 MLCOMM2O  PIC X(30).                                      00003870
           02 FILLER    PIC X(2).                                       00003880
           02 MEDATEA   PIC X.                                          00003890
           02 MEDATEC   PIC X.                                          00003900
           02 MEDATEP   PIC X.                                          00003910
           02 MEDATEH   PIC X.                                          00003920
           02 MEDATEV   PIC X.                                          00003930
           02 MEDATEO   PIC X(6).                                       00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MCVEND3A  PIC X.                                          00003960
           02 MCVEND3C  PIC X.                                          00003970
           02 MCVEND3P  PIC X.                                          00003980
           02 MCVEND3H  PIC X.                                          00003990
           02 MCVEND3V  PIC X.                                          00004000
           02 MCVEND3O  PIC X(6).                                       00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MLVEND3A  PIC X.                                          00004030
           02 MLVEND3C  PIC X.                                          00004040
           02 MLVEND3P  PIC X.                                          00004050
           02 MLVEND3H  PIC X.                                          00004060
           02 MLVEND3V  PIC X.                                          00004070
           02 MLVEND3O  PIC X(15).                                      00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MLCOMM3A  PIC X.                                          00004100
           02 MLCOMM3C  PIC X.                                          00004110
           02 MLCOMM3P  PIC X.                                          00004120
           02 MLCOMM3H  PIC X.                                          00004130
           02 MLCOMM3V  PIC X.                                          00004140
           02 MLCOMM3O  PIC X(30).                                      00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MEPLA     PIC X.                                          00004170
           02 MEPLC     PIC X.                                          00004180
           02 MEPLP     PIC X.                                          00004190
           02 MEPLH     PIC X.                                          00004200
           02 MEPLV     PIC X.                                          00004210
           02 MEPLO     PIC X(2).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MCVEND4A  PIC X.                                          00004240
           02 MCVEND4C  PIC X.                                          00004250
           02 MCVEND4P  PIC X.                                          00004260
           02 MCVEND4H  PIC X.                                          00004270
           02 MCVEND4V  PIC X.                                          00004280
           02 MCVEND4O  PIC X(6).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MLVEND4A  PIC X.                                          00004310
           02 MLVEND4C  PIC X.                                          00004320
           02 MLVEND4P  PIC X.                                          00004330
           02 MLVEND4H  PIC X.                                          00004340
           02 MLVEND4V  PIC X.                                          00004350
           02 MLVEND4O  PIC X(15).                                      00004360
           02 FILLER    PIC X(2).                                       00004370
           02 MLCOMM4A  PIC X.                                          00004380
           02 MLCOMM4C  PIC X.                                          00004390
           02 MLCOMM4P  PIC X.                                          00004400
           02 MLCOMM4H  PIC X.                                          00004410
           02 MLCOMM4V  PIC X.                                          00004420
           02 MLCOMM4O  PIC X(30).                                      00004430
           02 FILLER    PIC X(2).                                       00004440
           02 MLAUTORMA      PIC X.                                     00004450
           02 MLAUTORMC PIC X.                                          00004460
           02 MLAUTORMP PIC X.                                          00004470
           02 MLAUTORMH PIC X.                                          00004480
           02 MLAUTORMV PIC X.                                          00004490
           02 MLAUTORMO      PIC X(5).                                  00004500
           02 FILLER    PIC X(2).                                       00004510
           02 MNAUTORDA      PIC X.                                     00004520
           02 MNAUTORDC PIC X.                                          00004530
           02 MNAUTORDP PIC X.                                          00004540
           02 MNAUTORDH PIC X.                                          00004550
           02 MNAUTORDV PIC X.                                          00004560
           02 MNAUTORDO      PIC X(7).                                  00004570
           02 FILLER    PIC X(2).                                       00004580
           02 MQVENTEA  PIC X.                                          00004590
           02 MQVENTEC  PIC X.                                          00004600
           02 MQVENTEP  PIC X.                                          00004610
           02 MQVENTEH  PIC X.                                          00004620
           02 MQVENTEV  PIC X.                                          00004630
           02 MQVENTEO  PIC X(9).                                       00004640
           02 FILLER    PIC X(2).                                       00004650
           02 MQCOMPTA  PIC X.                                          00004660
           02 MQCOMPTC  PIC X.                                          00004670
           02 MQCOMPTP  PIC X.                                          00004680
           02 MQCOMPTH  PIC X.                                          00004690
           02 MQCOMPTV  PIC X.                                          00004700
           02 MQCOMPTO  PIC X(9).                                       00004710
           02 FILLER    PIC X(2).                                       00004720
           02 MQLIVRA   PIC X.                                          00004730
           02 MQLIVRC   PIC X.                                          00004740
           02 MQLIVRP   PIC X.                                          00004750
           02 MQLIVRH   PIC X.                                          00004760
           02 MQLIVRV   PIC X.                                          00004770
           02 MQLIVRO   PIC X(9).                                       00004780
           02 FILLER    PIC X(2).                                       00004790
           02 MQDIFFA   PIC X.                                          00004800
           02 MQDIFFC   PIC X.                                          00004810
           02 MQDIFFP   PIC X.                                          00004820
           02 MQDIFFH   PIC X.                                          00004830
           02 MQDIFFV   PIC X.                                          00004840
           02 MQDIFFO   PIC X(9).                                       00004850
           02 FILLER    PIC X(2).                                       00004860
           02 MQRECFA   PIC X.                                          00004870
           02 MQRECFC   PIC X.                                          00004880
           02 MQRECFP   PIC X.                                          00004890
           02 MQRECFH   PIC X.                                          00004900
           02 MQRECFV   PIC X.                                          00004910
           02 MQRECFO   PIC X(9).                                       00004920
           02 FILLER    PIC X(2).                                       00004930
           02 MCFACA    PIC X.                                          00004940
           02 MCFACC    PIC X.                                          00004950
           02 MCFACP    PIC X.                                          00004960
           02 MCFACH    PIC X.                                          00004970
           02 MCFACV    PIC X.                                          00004980
           02 MCFACO    PIC X.                                          00004990
           02 FILLER    PIC X(2).                                       00005000
           02 MCCEEA    PIC X.                                          00005010
           02 MCCEEC    PIC X.                                          00005020
           02 MCCEEP    PIC X.                                          00005030
           02 MCCEEH    PIC X.                                          00005040
           02 MCCEEV    PIC X.                                          00005050
           02 MCCEEO    PIC X.                                          00005060
           02 FILLER    PIC X(2).                                       00005070
           02 MCEXPA    PIC X.                                          00005080
           02 MCEXPC    PIC X.                                          00005090
           02 MCEXPP    PIC X.                                          00005100
           02 MCEXPH    PIC X.                                          00005110
           02 MCEXPV    PIC X.                                          00005120
           02 MCEXPO    PIC X.                                          00005130
           02 FILLER    PIC X(2).                                       00005140
           02 MCBLSPA   PIC X.                                          00005150
           02 MCBLSPC   PIC X.                                          00005160
           02 MCBLSPP   PIC X.                                          00005170
           02 MCBLSPH   PIC X.                                          00005180
           02 MCBLSPV   PIC X.                                          00005190
           02 MCBLSPO   PIC X.                                          00005200
           02 FILLER    PIC X(2).                                       00005210
           02 MZONCMDA  PIC X.                                          00005220
           02 MZONCMDC  PIC X.                                          00005230
           02 MZONCMDP  PIC X.                                          00005240
           02 MZONCMDH  PIC X.                                          00005250
           02 MZONCMDV  PIC X.                                          00005260
           02 MZONCMDO  PIC X(15).                                      00005270
           02 FILLER    PIC X(2).                                       00005280
           02 MLIBERRA  PIC X.                                          00005290
           02 MLIBERRC  PIC X.                                          00005300
           02 MLIBERRP  PIC X.                                          00005310
           02 MLIBERRH  PIC X.                                          00005320
           02 MLIBERRV  PIC X.                                          00005330
           02 MLIBERRO  PIC X(58).                                      00005340
           02 FILLER    PIC X(2).                                       00005350
           02 MCODTRAA  PIC X.                                          00005360
           02 MCODTRAC  PIC X.                                          00005370
           02 MCODTRAP  PIC X.                                          00005380
           02 MCODTRAH  PIC X.                                          00005390
           02 MCODTRAV  PIC X.                                          00005400
           02 MCODTRAO  PIC X(4).                                       00005410
           02 FILLER    PIC X(2).                                       00005420
           02 MCICSA    PIC X.                                          00005430
           02 MCICSC    PIC X.                                          00005440
           02 MCICSP    PIC X.                                          00005450
           02 MCICSH    PIC X.                                          00005460
           02 MCICSV    PIC X.                                          00005470
           02 MCICSO    PIC X(5).                                       00005480
           02 FILLER    PIC X(2).                                       00005490
           02 MNETNAMA  PIC X.                                          00005500
           02 MNETNAMC  PIC X.                                          00005510
           02 MNETNAMP  PIC X.                                          00005520
           02 MNETNAMH  PIC X.                                          00005530
           02 MNETNAMV  PIC X.                                          00005540
           02 MNETNAMO  PIC X(8).                                       00005550
           02 FILLER    PIC X(2).                                       00005560
           02 MSCREENA  PIC X.                                          00005570
           02 MSCREENC  PIC X.                                          00005580
           02 MSCREENP  PIC X.                                          00005590
           02 MSCREENH  PIC X.                                          00005600
           02 MSCREENV  PIC X.                                          00005610
           02 MSCREENO  PIC X(4).                                       00005620
                                                                                
