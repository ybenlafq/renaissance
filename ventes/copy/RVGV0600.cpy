      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVGV0600                                     00020005
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV0600                 00060005
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0600.                                                    00090005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0600.                                                            
      *}                                                                        
           02  GV06-NSOCIETE                                            00100004
               PIC X(0003).                                             00110004
           02  GV06-NLIEU                                               00120004
               PIC X(0003).                                             00130004
           02  GV06-NVENTE                                              00140004
               PIC X(0007).                                             00150004
           02  GV06-NDOSSIER                                            00160004
               PIC S9(3) COMP-3.                                        00170006
           02  GV06-NSEQNQ                                              00180004
               PIC S9(5) COMP-3.                                        00190001
           02  GV06-NSEQ                                                00200004
               PIC S9(3) COMP-3.                                        00210001
           02  GV06-DSYST                                               00220004
               PIC S9(13) COMP-3.                                       00230004
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00240000
      *---------------------------------------------------------        00250000
      *   LISTE DES FLAGS DE LA TABLE RVGV0600                          00260005
      *---------------------------------------------------------        00270000
      *                                                                 00280000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0600-FLAGS.                                              00290005
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0600-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NSOCIETE-F                                          00300004
      *        PIC S9(4) COMP.                                          00310000
      *--                                                                       
           02  GV06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NLIEU-F                                             00320004
      *        PIC S9(4) COMP.                                          00330000
      *--                                                                       
           02  GV06-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NVENTE-F                                            00340004
      *        PIC S9(4) COMP.                                          00350000
      *--                                                                       
           02  GV06-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NDOSSIER-F                                          00360004
      *        PIC S9(4) COMP.                                          00370000
      *--                                                                       
           02  GV06-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NSEQNQ-F                                            00380004
      *        PIC S9(4) COMP.                                          00390000
      *--                                                                       
           02  GV06-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-NSEQ-F                                              00400004
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  GV06-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV06-DSYST-F                                             00420004
      *        PIC S9(4) COMP.                                          00430004
      *--                                                                       
           02  GV06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00440000
                                                                                
