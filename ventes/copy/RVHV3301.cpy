      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVHV3301                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV3301                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV3301.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV3301.                                                            
      *}                                                                        
           02  HV33-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV33-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV33-NSOCMAG                                                     
               PIC X(0003).                                                     
           02  HV33-NMAG                                                        
               PIC X(0003).                                                     
           02  HV33-CRAYONFAM                                                   
               PIC X(0005).                                                     
           02  HV33-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  HV33-DMVENTE                                                     
               PIC X(0006).                                                     
           02  HV33-NAGREGATED                                                  
               PIC S9(5) COMP-3.                                                
           02  HV33-LAGREGATED                                                  
               PIC X(0020).                                                     
           02  HV33-QVENDD2                                                     
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV33-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTCOMM1                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTCOMM2                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-QNBPSE                                                      
               PIC S9(5) COMP-3.                                                
           02  HV33-PCAPSE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTPRIMPSE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV33-QNBPSAB                                                     
               PIC S9(5) COMP-3.                                                
           02  HV33-QVENDD2EP                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV33-QVENDD2SV                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV33-QVENDD2CM                                                   
               PIC S9(5)V9(0002) COMP-3.                                        
           02  HV33-PCAEP                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PCASV                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PCACM                                                       
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTACHEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTACHSV                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTACHCM                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTREM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTREMEP                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-PMTCOMMEP                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-MTVA                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-MTVAEP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-MTVASV                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV33-MTVACM                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV3301                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVHV3301-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVHV3301-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-NSOCMAG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-NSOCMAG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-NMAG-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-NMAG-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-CRAYONFAM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-CRAYONFAM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-NAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-NAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-LAGREGATED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-LAGREGATED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QVENDD2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QVENDD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTCOMM1-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTCOMM1-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTCOMM2-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTCOMM2-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QNBPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QNBPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PCAPSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PCAPSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTPRIMPSE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTPRIMPSE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QNBPSAB-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QNBPSAB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QVENDD2EP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QVENDD2EP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QVENDD2SV-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QVENDD2SV-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-QVENDD2CM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-QVENDD2CM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PCAEP-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PCAEP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PCASV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PCASV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PCACM-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PCACM-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTACHEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTACHEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTACHSV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTACHSV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTACHCM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTACHCM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTREM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTREM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTREMEP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTREMEP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-PMTCOMMEP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-PMTCOMMEP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-MTVA-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-MTVA-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-MTVAEP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-MTVAEP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-MTVASV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-MTVASV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV33-MTVACM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV33-MTVACM-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
