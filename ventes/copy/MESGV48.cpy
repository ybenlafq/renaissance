      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION STOCK LOCAL                            
      *         B-VERSION                                                       
      *****************************************************************         
      * 10/12/2001 P FAYOLLE (PF1) : PB QUAND ON PASSE UN EMD EN EMR.           
      * 27/12/2013 M ZAIDI(1213): AJOUT PARTIE R�SERVATION STK                  
      *                           PROJET EMPORT� SUR STK PTF                    
      *****************************************************************         
      *  POUR MGV48 AVEC GET ET PUT EXTERNES                                    
      *                                                                         
      *01 WS-MESSAGE.                                                           
           10  WS-MESSAGE REDEFINES COMM-MQ20-MESSAGE.                          
      *---                                                                      
           15  MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
PF1        15  TAB-MES-GV11.                                                    
               20  MES-GV11 OCCURS 40.                                          
                   25   MES-GV11-NCODIC          PIC X(07).                     
                   25   MES-GV11-QVENDUE         PIC S9(5).                     
                   25   MES-GV11-NLIGNE          PIC X(02).                     
PF1                25   MES-GV11-NSOCIETE        PIC X(03).                     
PF1                25   MES-GV11-NLIEU           PIC X(03).                     
PF1                25   MES-GV11-NVENTE          PIC X(07).                     
 "                 25   MES-GV11-NSEQNQ          PIC S9(3).                     
  1213*    TYPE DEMANDE (M/P) MAG/PTF                                           
  1213             25   MES-GV11-TYPDEM          PIC X(01).                     
      *                                                                         
  1213* MESSAGE DE R�SERVATION DE STOCK:PARTIE QUESTION                         
      *                                                                         
           10  WS-MESS-Q  REDEFINES COMM-MQ20-MESSAGE.                          
      *---                                                                      
   "       15  MESS-ENTETE.                                                     
   "           20   MESS-TYPE     PIC    X(3).                                  
   "           20   MESS-NSOCMSG  PIC    X(3).                                  
   "           20   MESS-NLIEUMSG PIC    X(3).                                  
   "           20   MESS-NSOCDST  PIC    X(3).                                  
   "           20   MESS-NLIEUDST PIC    X(3).                                  
   "           20   MESS-NORD     PIC    9(8).                                  
   "           20   MESS-LPROG    PIC    X(10).                                 
   "           20   MESS-DJOUR    PIC    X(8).                                  
   "           20   MESS-WSID     PIC    X(10).                                 
   "           20   MESS-USER     PIC    X(10).                                 
   "           20   MESS-CHRONO   PIC    9(7).                                  
   "           20   MESS-NBRMSG   PIC    9(7).                                  
   "           20   MESS-FILLER   PIC    X(30).                                 
   "       15  MESS-DETAILLE.                                                   
   "           20 MESS-TYPE-TRT        PIC 9(01).                               
   "              88 MDQRPF-CRE-RESA-VENTE-PF  VALUE 1.                         
   "              88 MDQRPF-SUPP-RESA-VENTE-PF VALUE 2.                         
   "              88 MDQRPF-CHG-RESA-VENTE-PF  VALUE 3.                         
   "              88 MDQRPF-DEC-RESA-VENTE-PF  VALUE 4.                         
   "  *      SOCI�T� DE VENTE                                                   
   "           20 MESS-Q-NSOCVTE    PIC X(03).                                  
   "  *      LIEU DE VENTE                                                      
   "           20 MESS-Q-NLIEUVTE   PIC X(03).                                  
   "  *      N� DE R�SERVATION (SOIT N� ORDRE SOIT N� DE VENTE)                 
   "           20 MESS-Q-NUMRES     PIC X(07).                                  
   "  *      N� DE VENTE                                                        
   "           20 MESS-Q-NVENTE     PIC X(07).                                  
   "  * =========   CORPS DU MESSAGE:  ===========                              
   "           20 TAB-MES-RESA.                                                 
   "              22 TABL-Q OCCURS 40.                                          
   "  *              CODIC                                                      
   "                 25 MESS-Q-NCODIC      PIC X(7).                            
   "  *              N� DE S�QUENCE UNIQUE ORIGINE                              
   "                 25 MESS-Q-NSEQNQ-ORIG PIC 9(05).                           
   "  *              N� DE S�QUENCE UNIQUE                                      
   "                 25 MESS-Q-NSEQNQ      PIC 9(05).                           
   "  *              QUANTIT�                                                   
   "                 25 MESS-Q-QTE         PIC S9(5).                           
   "  *                                                                         
      *                                                                         
PF1   *121310   LONGUEUR-MESSAGE         PIC S9(04) VALUE +1065.                
 "    *    10   LONGUEUR-MESSAGE         PIC S9(03) VALUE +665.                 
1213   01   LONGUEUR-MESSAGE         PIC S9(04) VALUE +1065.                    
1213   01   LONGUEUR-MES-RESA        PIC S9(04) VALUE +0721.                    
                                                                                
