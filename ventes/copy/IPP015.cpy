      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPP015 AU 28/06/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,01,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,15,BI,A,                          *        
      *                           34,06,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPP015.                                                        
            05 NOMETAT-IPP015           PIC X(6) VALUE 'IPP015'.                
            05 RUPTURES-IPP015.                                                 
           10 IPP015-NSOCTITRE          PIC X(03).                      007  003
           10 IPP015-CGRPMAG            PIC X(02).                      010  002
           10 IPP015-NMAGTITRE          PIC X(03).                      012  003
           10 IPP015-A-D                PIC X(01).                      015  001
           10 IPP015-NMAGTRI            PIC X(03).                      016  003
           10 IPP015-LVENDEUR           PIC X(15).                      019  015
           10 IPP015-CVENDEUR           PIC X(06).                      034  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPP015-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IPP015-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPP015.                                                   
           10 IPP015-LIB1-A-D           PIC X(17).                      042  017
           10 IPP015-LIB2-A-D           PIC X(17).                      059  017
           10 IPP015-LLIEU              PIC X(20).                      076  020
           10 IPP015-MMSSAA             PIC X(07).                      096  007
           10 IPP015-NMAGRAT            PIC X(03).                      103  003
           10 IPP015-NMAGVTE            PIC X(03).                      106  003
           10 IPP015-GEN-CA             PIC S9(07)      COMP-3.         109  004
           10 IPP015-GEN-TOT            PIC S9(07)      COMP-3.         113  004
           10 IPP015-GEN-VOLU           PIC S9(07)      COMP-3.         117  004
           10 IPP015-PAY-CA             PIC S9(07)      COMP-3.         121  004
           10 IPP015-PAY-TOT            PIC S9(07)      COMP-3.         125  004
           10 IPP015-PAY-VOLU           PIC S9(07)      COMP-3.         129  004
           10 IPP015-PMTCA              PIC S9(11)      COMP-3.         133  006
           10 IPP015-PPAMM1             PIC S9(07)      COMP-3.         139  004
           10 IPP015-PPAMM1-SIG         PIC S9(07)      COMP-3.         143  004
           10 IPP015-PPAMM2             PIC S9(07)      COMP-3.         147  004
           10 IPP015-PPAMM2-SIG         PIC S9(07)      COMP-3.         151  004
           10 IPP015-PPAMM3             PIC S9(07)      COMP-3.         155  004
           10 IPP015-PPAMM3-SIG         PIC S9(07)      COMP-3.         159  004
           10 IPP015-QPIECEVOL          PIC S9(07)      COMP-3.         163  004
           10 IPP015-QPOURCCA           PIC S9(03)V9(3) COMP-3.         167  004
           10 IPP015-QPOURCVOL          PIC S9(03)V9(3) COMP-3.         171  004
            05 FILLER                      PIC X(338).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPP015-LONG           PIC S9(4)   COMP  VALUE +174.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPP015-LONG           PIC S9(4) COMP-5  VALUE +174.           
                                                                                
      *}                                                                        
