      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY ENTETE DES MESSAGES MQ RECUPERES PAR LE HOST                     
      *****************************************************************         
      *                                                                         
       01  WS-MQ10.                                                             
      * ZONES D'ECHANGE AVEC MQHI (IDENTIFIANT MSG / CODE RET TRT)              
         02 WS-QUEUE.                                                           
               10   MQ10-MSGID       PIC  X(24).                                
               10   MQ10-CORRELID    PIC  X(24).                                
         02 WS-CODRET                PIC  XX.                                   
      * DESCRIPTION DU MSG                                                      
         02 MESSL.                                                              
      *    ENTETE STANDARD                                       LG=105C        
           05  MESSL-ENTETE.                                                    
               10   MESSL-TYPE       PIC  X(03).                                
               10   MESSL-NSOCMSG    PIC  X(03).                                
               10   MESSL-NLIEUMSG   PIC  X(03).                                
               10   MESSL-NSOCDST    PIC  X(03).                                
               10   MESSL-NLIEUDST   PIC  X(03).                                
               10   MESSL-NORD       PIC  9(08).                                
               10   MESSL-LPROG      PIC  X(10).                                
               10   MESSL-DJOUR      PIC  X(08).                                
               10   MESSL-WSID       PIC  X(10).                                
               10   MESSL-USER       PIC  X(10).                                
               10   MESSL-CHRONO     PIC  9(07).                                
               10   MESSL-NBRMSG     PIC  9(07).                                
               10   MESSL-NBRENR     PIC  9(05).                                
               10   MESSL-TAILLE     PIC  9(05).                                
               10   MESSL-VERSION    PIC  X(02).                                
               10   MESSL-DSYST      PIC S9(13).                                
               10   MESSL-FILLER     PIC  X(05).                                
      *                                                                         
      *---- FIN COPIE                                                           
                                                                                
