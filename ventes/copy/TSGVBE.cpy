      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS POUR LA GESTION DES BES COMMUNS AUX TGV8X             * 00000030
      *      NOM: 'GVBE' + SOCIETE                                    * 00000090
      *      LG :  15  C                                              * 00000091
      *                                                               * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GVBE-IDENTIFICATEUR.                                              
           05  TS-GVBE-NOM          PIC    X(4)  VALUE  'GVBE'.                 
           05  TS-GVBE-NSOC         PIC    X(3).                                
      *                                                               * 00000094
       01  TS-GVBE.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
           05 TS-GVBE-LONG               PIC S9(5) COMP-3 VALUE +15.    00000097
      ********************************** DONNEES                        00000098
           05 TS-GVBE-DONNEES.                                          00000099
              10 TS-GVBE-DATCREAT        PIC X(8).                      00000100
              10 TS-GVBE-MAXNDOC         PIC 9(7).                      00000100
                                                                                
