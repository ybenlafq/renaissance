      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : STATISTIQUES NMD                                 *        
      *  TRANSACTION: GV??                                             *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MGV51         *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MGV51-APPLI.                                            00260000
         02 COMM-MGV51-ENTREE.                                          00260000
      * IDENTIFIANT VENTE                                                       
           05 COMM-MGV51-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MGV51-NLIEU          PIC X(03).                      00330000
           05 COMM-MGV51-NVENTE         PIC X(07).                      00340000
           05 COMM-MGV51-DJOUR          PIC X(08).                      00340000
           05 COMM-MGV51-PHOTO          PIC X(01).                      00340000
         02 COMM-MGV51-SORTIE.                                          00260000
           05 COMM-MGV51-CODE-RETOUR    PIC X(01).                      00340000
           05 COMM-MGV51-MESSAGE.                                               
                10 COMM-MGV51-CODRET         PIC X.                             
                   88  COMM-MGV51-OK         VALUE ' '.                         
                   88  COMM-MGV51-ERR        VALUE '1'.                         
                10 COMM-MGV51-LIBERR         PIC X(58).                         
         02 COMM-MGV51-CCEVENT        PIC X(004).                       00340000
         02 FILLER                    PIC X(001).                       00340000
                                                                                
