      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTEC01                        *        
      ******************************************************************        
       01  RVEC0100.                                                            
           10 EC01-NCODIC          PIC X(7).                                    
           10 EC01-NSOCIETE        PIC X(3).                                    
           10 EC01-NLIEU           PIC X(3).                                    
           10 EC01-QSTOCK          PIC S9(5)V USAGE COMP-3.                     
           10 EC01-DDISPO          PIC X(8).                                    
           10 EC01-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
