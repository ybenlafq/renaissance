      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPP010 AU 28/06/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,01,BI,A,                          *        
      *                           16,03,BI,A,                          *        
      *                           19,15,BI,A,                          *        
      *                           34,06,BI,A,                          *        
      *                           40,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPP010.                                                        
            05 NOMETAT-IPP010           PIC X(6) VALUE 'IPP010'.                
            05 RUPTURES-IPP010.                                                 
           10 IPP010-NSOCTITRE          PIC X(03).                      007  003
           10 IPP010-CGRPMAG            PIC X(02).                      010  002
           10 IPP010-NMAGTITRE          PIC X(03).                      012  003
           10 IPP010-A-D                PIC X(01).                      015  001
           10 IPP010-NMAGTRI            PIC X(03).                      016  003
           10 IPP010-LVENDEUR           PIC X(15).                      019  015
           10 IPP010-CVENDEUR           PIC X(06).                      034  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPP010-SEQUENCE           PIC S9(04) COMP.                040  002
      *--                                                                       
           10 IPP010-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPP010.                                                   
           10 IPP010-LIB1-A-D           PIC X(17).                      042  017
           10 IPP010-LIB2-A-D           PIC X(17).                      059  017
           10 IPP010-LLIEU              PIC X(20).                      076  020
           10 IPP010-MMSSAA             PIC X(07).                      096  007
           10 IPP010-NMAGRAT            PIC X(03).                      103  003
           10 IPP010-NMAGVTE            PIC X(03).                      106  003
           10 IPP010-ARTI               PIC S9(07)      COMP-3.         109  004
           10 IPP010-ARTI-SIG           PIC S9(07)      COMP-3.         113  004
           10 IPP010-CONT               PIC S9(07)      COMP-3.         117  004
           10 IPP010-CONT-SIG           PIC S9(07)      COMP-3.         121  004
           10 IPP010-GEN-ARTI           PIC S9(07)      COMP-3.         125  004
           10 IPP010-GEN-CONT           PIC S9(07)      COMP-3.         129  004
           10 IPP010-GEN-PAMM           PIC S9(07)      COMP-3.         133  004
           10 IPP010-GEN-PRES           PIC S9(07)      COMP-3.         137  004
           10 IPP010-PAMM               PIC S9(07)      COMP-3.         141  004
           10 IPP010-PAMM-SIG           PIC S9(07)      COMP-3.         145  004
           10 IPP010-PAY-ARTI           PIC S9(07)      COMP-3.         149  004
           10 IPP010-PAY-CONT           PIC S9(07)      COMP-3.         153  004
           10 IPP010-PAY-PAMM           PIC S9(07)      COMP-3.         157  004
           10 IPP010-PAY-PRES           PIC S9(07)      COMP-3.         161  004
           10 IPP010-PRES               PIC S9(07)      COMP-3.         165  004
           10 IPP010-PRES-SIG           PIC S9(07)      COMP-3.         169  004
            05 FILLER                      PIC X(340).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPP010-LONG           PIC S9(4)   COMP  VALUE +172.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPP010-LONG           PIC S9(4) COMP-5  VALUE +172.           
                                                                                
      *}                                                                        
