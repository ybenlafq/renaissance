      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVGV1160                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV1160                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1160.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1160.                                                            
      *}                                                                        
           02  GV11-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV11-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV11-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV11-CTYPENREG                                                   
               PIC X(0001).                                                     
           02  GV11-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV11-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV11-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV11-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV11-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV11-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV11-CENREG                                                      
               PIC X(0005).                                                     
           02  GV11-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV11-PVUNIT                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV11-PVUNITF                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV11-PVTOTAL                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV11-CEQUIPE                                                     
               PIC X(0005).                                                     
           02  GV11-NLIGNE                                                      
               PIC X(0002).                                                     
           02  GV11-TAUXTVA                                                     
               PIC S9(3)V9(0002) COMP-3.                                        
           02  GV11-QCONDT                                                      
               PIC S9(5) COMP-3.                                                
           02  GV11-PRMP                                                        
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV11-WEMPORTE                                                    
               PIC X(0001).                                                     
           02  GV11-CPLAGE                                                      
               PIC X(0002).                                                     
           02  GV11-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  GV11-CADRTOUR                                                    
               PIC X(0001).                                                     
           02  GV11-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  GV11-LCOMMENT                                                    
               PIC X(0035).                                                     
           02  GV11-CVENDEUR                                                    
               PIC X(0006).                                                     
           02  GV11-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV11-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV11-NAUTORM                                                     
               PIC X(0005).                                                     
           02  GV11-WARTINEX                                                    
               PIC X(0001).                                                     
           02  GV11-WEDITBL                                                     
               PIC X(0001).                                                     
           02  GV11-WACOMMUTER                                                  
               PIC X(0001).                                                     
           02  GV11-WCQERESF                                                    
               PIC X(0001).                                                     
           02  GV11-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GV11-CTOURNEE                                                    
               PIC X(0008).                                                     
           02  GV11-WTOPELIVRE                                                  
               PIC X(0001).                                                     
           02  GV11-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  GV11-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV11-HCREATION                                                   
               PIC X(0004).                                                     
           02  GV11-DANNULATION                                                 
               PIC X(0008).                                                     
           02  GV11-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GV11-WINTMAJ                                                     
               PIC X(0001).                                                     
           02  GV11-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV11-DSTAT                                                       
               PIC X(0004).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV1160                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1160-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1160-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CTYPENREG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CTYPENREG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-PVUNIT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-PVUNITF-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-PVTOTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CEQUIPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CEQUIPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-TAUXTVA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-QCONDT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-PRMP-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-PRMP-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WEMPORTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WEMPORTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CPLAGE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CPLAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CADRTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CADRTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WARTINEX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WARTINEX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WEDITBL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WEDITBL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WACOMMUTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WACOMMUTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-CTOURNEE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-CTOURNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WTOPELIVRE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WTOPELIVRE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-HCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-HCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-WINTMAJ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-WINTMAJ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV11-DSTAT-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV11-DSTAT-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
