      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : EXPLOITATION MQ SERIES                           *        
      *  TRANSACTION: SL10                                             *        
      *  TITRE      : COMMAREA DE LA TRANSACTION SL10                  *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      * Z-COMMAREA, D' UNE LONGUEUR DE 4096 C, COMPRENANT :            *        
      * 1 - LES ZONES RESERVEES A AIDA                           100 C *        
      * 2 - LES ZONES RESERVEES EN PROVENANCE DE CICS             20 C *        
      * 3 - LES ZONES RESERVEES A LA DATE DE TRAITEMENT          100 C *        
      * 4 - LES ZONES RESERVEES TRAITEMENT DU SWAP               152 C *        
      * 5 - LES ZONES RESERVEES APPLICATIVES                    3724 C *        
      *                                                        -----   *        
      * COM-SL10-LONG-COMMAREA A UNE VALUE DE                   4096 C *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-SL10-LONG-COMMAREA       PIC S9(4) COMP   VALUE +4096.           
      *--                                                                       
       01  COM-SL10-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +4096.         
      *}                                                                        
      *                                                                         
       01  Z-COMMAREA.                                                          
      *--- ZONES RESERVEES A AIDA -------------------------------- 100          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *--- ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 COMM-CICS-APPLID          PIC X(08).                              
           02 COMM-CICS-NETNAM          PIC X(08).                              
           02 COMM-CICS-TRANSA          PIC X(04).                              
      *--- ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 COMM-DATE-SIECLE          PIC X(02).                              
           02 COMM-DATE-ANNEE           PIC X(02).                              
           02 COMM-DATE-MOIS            PIC X(02).                              
           02 COMM-DATE-JOUR            PIC 9(02).                              
           02 COMM-DATE-QNTA            PIC 9(03).                              
           02 COMM-DATE-QNT0            PIC 9(05).                              
           02 COMM-DATE-BISX            PIC 9(01).                              
           02 COMM-DATE-JSM             PIC 9(01).                              
           02 COMM-DATE-JSM-LC          PIC X(03).                              
           02 COMM-DATE-JSM-LL          PIC X(08).                              
           02 COMM-DATE-MOIS-LC         PIC X(03).                              
           02 COMM-DATE-MOIS-LL         PIC X(09).                              
           02 COMM-DATE-SSAAMMJJ        PIC X(08).                              
           02 COMM-DATE-AAMMJJ          PIC X(06).                              
           02 COMM-DATE-JJMMSSAA        PIC X(08).                              
           02 COMM-DATE-JJMMAA          PIC X(06).                              
           02 COMM-DATE-JJ-MM-AA        PIC X(08).                              
           02 COMM-DATE-JJ-MM-SSAA      PIC X(10).                              
           02 COMM-DATE-WEEK.                                                   
              05 COMM-DATE-SEMSS           PIC 9(02).                           
              05 COMM-DATE-SEMAA           PIC 9(02).                           
              05 COMM-DATE-SEMNU           PIC 9(02).                           
           02 COMM-DATE-FILLER          PIC X(07).                              
      *--- ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-CURS            PIC S9(4) COMP VALUE -1.                
      *--                                                                       
           02 COMM-SWAP-CURS            PIC S9(4) COMP-5 VALUE -1.              
      *}                                                                        
           02 COMM-SWAP-ATTR            OCCURS 150 PIC X(1).                    
      *                                                                         
      ******************************************************************        
      *--- ZONES RESERVEES APPLICATIVES DE LA TRANSACTION SL10 - 3724 C*        
      ******************************************************************        
      *                                                                         
           02 COMM-SL10-APPLI.                                                  
      *      ZONES SL11                       -----------------   92 C*         
             03 COMM-SL11-DATA.                                                 
               05 COMM-SL11-VALIDE-TRT     PIC X(1).                            
               05 COMM-SL11-VALIDE-SUP     PIC 9(1).                            
               05 COMM-SL11-PAGE             PIC 9(3).                          
               05 COMM-SL11-NBPAGES          PIC 9(3).                          
      *        FLAG INDIQUANT SI LONGUEUR-TS < 500                              
               05 COMM-SL11-LONGUEUR-TS      PIC 9 .                            
                    88 LONGUEUR-TS-OK               VALUE 0.                    
                    88 LONGUEUR-TS-KO               VALUE 1.                    
      *        NB ENREG AFFICHES PAR PAGE (DEPEND DU NIVEAU DE DETAIL)          
               05 COMM-SL11-NBENR-PAGE       PIC 9(2)  COMP-3 .                 
      *        NBRE D'ENREG MIS DANS LA TS                                      
               05 COMM-SL11-CPT              PIC S9(4)  COMP-3 .                
      *        CRITERES DE SELECTION                                            
               05 COMM-SL11-NUMDOC           PIC X(7).                          
               05 COMM-SL11-SOCCRE           PIC X(3).                          
               05 COMM-SL11-LIEUCCRE         PIC X(3).                          
               05 COMM-SL11-TYPDOC           PIC X(2).                          
               05 COMM-SL11-CFAM             PIC X(5).                          
               05 COMM-SL11-CMARQ            PIC X(5).                          
               05 COMM-SL11-NCODIC           PIC X(7).                          
               05 COMM-SL11-NSOCDO           PIC X(3).                          
               05 COMM-SL11-NSOCFO           PIC X(3).                          
               05 COMM-SL11-NLIEUDO          PIC X(3).                          
               05 COMM-SL11-NLIEUFO          PIC X(3).                          
               05 COMM-SL11-NSOCDD           PIC X(3).                          
               05 COMM-SL11-NSOCFD           PIC X(3).                          
               05 COMM-SL11-NLIEUDD          PIC X(3).                          
               05 COMM-SL11-NLIEUFD          PIC X(3).                          
               05 COMM-SL11-DATERD           PIC X(8).                          
               05 COMM-SL11-DATERF           PIC X(8).                          
               05 COMM-SL11-DATEED           PIC X(8).                          
               05 COMM-SL11-DATEEF           PIC X(8).                          
               05 COMM-SL11-WARB             PIC X(1).                          
               05 COMM-SL11-COPAR            PIC X(5).                          
               05 COMM-SL11-DETAIL           PIC 9(1).                          
               05 COMM-SL11-TRI              PIC 9(1).                          
      *        MODE DE CONSULTATION UTILISE.                                    
               05  COMM-SL11-MODE-CONSULT       PIC 9(1).                       
                   88 MODE-NON-RENSEIGNE       VALUE 0.                         
                   88 MODE-DOCUMENT            VALUE 1.                         
                   88 MODE-CODIC               VALUE 2.                         
                   88 MODE-CODE-OPERATION      VALUE 3.                         
                   88 MODE-FAMILLE             VALUE 4.                         
                   88 MODE-MARQUE              VALUE 5.                         
                   88 MODE-LIEU-ORIG           VALUE 6.                         
                   88 MODE-LIEU-DEST           VALUE 7.                         
      *        NBRE D'UTILISATIONS DE PF9.                                      
               05  COMM-SL11-CPT-PF9         PIC 9.                             
                   88 ZERO-PF9                 VALUE 0.                         
                   88 UN-PF9                   VALUE 1.                         
                   88 DEUX-PF9                 VALUE 2.                         
               05  COMM-SL11-TEMOIN-LIGNE    PIC S9(4) COMP-3.                  
      *        INDICATEUR D'EGALITE ORIGINE-DESTINATAIRE.                       
               05  COMM-SL11-ORIG-DEST         PIC 9.                           
                   88 INEGALITE-ORIG-DEST      VALUE 0.                         
                   88 EGALITE-ORIG-DEST        VALUE 1.                         
               05  COMM-SL11-MENU              PIC X(4).                        
      *      ZONES SL12.                      -----------------   91 C*         
             03 COMM-SL12-DATA.                                                 
               05 COMM-SL12-VALIDE-TRT       PIC X(1).                          
               05 COMM-SL12-VALIDE-SUP       PIC 9(1).                          
               05 COMM-SL12-PAGE             PIC 9(3).                          
               05 COMM-SL12-NBPAGES          PIC 9(3).                          
      *        FLAG INDIQUANT SI LONGUEUR-TS < 500                              
               05 COMM-SL12-LONGUEUR-TS      PIC 9 .                            
                    88 LONGUEUR-TS-OK2              VALUE 0.                    
                    88 LONGUEUR-TS-KO2              VALUE 1.                    
      *        NBRE D'ENREG AFFICHES PAR PAGE.                                  
               05 COMM-SL12-NBENR-PAGE       PIC 9(2)  COMP-3 .                 
      *        NBRE D'ENREG MIS DANS LA TS                                      
               05 COMM-SL12-CPT              PIC S9(4)  COMP-3 .                
      *        CRITERES DE SELECTION.                                           
               05 COMM-SL12-NUMDOC           PIC X(7).                          
               05 COMM-SL12-SOCCRE           PIC X(3).                          
               05 COMM-SL12-LIEUCCRE         PIC X(3).                          
               05 COMM-SL12-TYPDOC           PIC X(2).                          
               05 COMM-SL12-CFAM             PIC X(5).                          
               05 COMM-SL12-CMARQ            PIC X(5).                          
               05 COMM-SL12-NCODIC           PIC X(7).                          
               05 COMM-SL12-NSOCO            PIC X(3).                          
               05 COMM-SL12-NLIEUO           PIC X(3).                          
               05 COMM-SL12-NSOCD            PIC X(3).                          
               05 COMM-SL12-NLIEUD           PIC X(3).                          
               05 COMM-SL12-DATER            PIC X(8).                          
               05 COMM-SL12-DATEE            PIC X(8).                          
               05 COMM-SL12-WARB             PIC X(1).                          
               05 COMM-SL12-COPAR            PIC X(5).                          
               05 COMM-SL12-DETAIL           PIC 9(1).                          
      *        MODE DE CONSULTATION UTILISE.                                    
               05 COMM-SL12-MODE-CONSULT       PIC 9(1).                        
                  88 MODE-NON-RENSEIGNE2      VALUE 0.                          
                  88 MODE-DOCUMENT2           VALUE 1.                          
                  88 MODE-CODIC2              VALUE 2.                          
                  88 MODE-CODE-OPERATION2     VALUE 3.                          
                  88 MODE-FAMILLE2            VALUE 4.                          
                  88 MODE-MARQUE2             VALUE 5.                          
                  88 MODE-LIEU-ORIG2          VALUE 6.                          
                  88 MODE-LIEU-DEST2          VALUE 7.                          
      *        NBRE D'UTILISATIONS DE PF9.                                      
               05 COMM-SL12-CPT-PF9      PIC 9.                                 
                  88 ZERO-PF92                VALUE 0.                          
                  88 UN-PF92                  VALUE 1.                          
                  88 DEUX-PF92                VALUE 2.                          
               05 COMM-SL12-TEMOIN-LIGNE      PIC S9(4) COMP-3.                 
      *        INDICATEUR D'EGALITE ORIGINE-DESTINATAIRE.                       
               05 COMM-SL12-ORIG-DEST         PIC 9.                            
                  88 INEGALITE-ORIG-DEST2     VALUE 0.                          
                  88 EGALITE-ORIG-DEST2       VALUE 1.                          
               05 COMM-SL12-MENU              PIC X(4).                         
      *      ZONES SL13.                      -----------------  138 C*         
             03 COMM-SL13-DATA.                                                 
               05 COMM-SL13-PAGE            PIC 9(3) VALUE 0.                   
               05 COMM-SL13-NBPAGES         PIC 9(3) VALUE 0.                   
               05 COMM-SL13-NSOCCRE         PIC X(3) VALUE SPACES.              
               05 COMM-SL13-NLIEUCRE        PIC X(3) VALUE SPACES.              
               05 COMM-SL13-CTYPDOC         PIC X(2) VALUE SPACES.              
               05 COMM-SL13-NUMDOC          PIC S9(7) COMP-3 VALUE 0.           
               05 COMM-SL13-DATEDOC         PIC X(8) VALUE SPACES.              
               05 COMM-SL13-LLIEUO          PIC X(20) VALUE SPACES.             
               05 COMM-SL13-LLIEUD          PIC X(20) VALUE SPACES.             
??             05 COMM-SL13-TAB-NLDOC OCCURS 14.                                
??               10 COMM-SL13-NLDOC       PIC S9(05).                           
               05 COMM-SL13-TOPMODIF        PIC X VALUE '1'.                    
                 88 COMM-SL13-TELQUEL       VALUE '1'.                          
                 88 COMM-SL13-MODIFICATION  VALUE '2'.                          
               05 COMM-SL13-TOPMAJ          PIC X VALUE '2'.                    
                 88 COMM-SL13-MAJ           VALUE '1'.                          
                 88 COMM-SL13-RIEN          VALUE '2'.                          
      *      ZONES SL14.                      -----------------  191 C*         
             03 COMM-SL14-DATA.                                                 
               05 COMM-SL14-TAB-ARB.                                            
                 10 COMM-SL14-TAB-NLDOC OCCURS 14.                              
                   15 COMM-SL14-NLDOC       PIC S9(05).                         
                   15 COMM-SL14-WARB        PIC X(01).                          
               05 COMM-SL14-PAGE            PIC 9(3) VALUE 0.                   
               05 COMM-SL14-NBPAGES         PIC 9(3) VALUE 1.                   
               05 COMM-SL14-POSPAGE         PIC 9(2) VALUE 0.                   
               05 COMM-SL14-QEXPTOT         PIC 9(5) VALUE 0.                   
               05 COMM-SL14-QRECTOT         PIC 9(5) VALUE 0.                   
               05 COMM-SL14-QARBTOT         PIC 9(5) VALUE 0.                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        05 COMM-SL14-RANGTS          PIC S9(4) COMP VALUE 0.             
      *--                                                                       
               05 COMM-SL14-RANGTS          PIC S9(4) COMP-5 VALUE 0.           
      *}                                                                        
               05 COMM-SL14-NBDOC           PIC 9(2) VALUE 0.                   
               05 COMM-SL14-DOC-1.                                              
                 10 COMM-SL14-NSOCCRE-1   PIC X(03).                            
                 10 COMM-SL14-NLIEUCRE-1  PIC X(03).                            
                 10 COMM-SL14-CTYPDOC-1   PIC X(02).                            
                 10 COMM-SL14-NUMDOC-1    PIC S9(7) COMP-3.                     
                 10 COMM-SL14-NCODIC-1    PIC X(07).                            
               05 COMM-SL14-DOC-2.                                              
                 10 COMM-SL14-NSOCCRE-2   PIC X(03).                            
                 10 COMM-SL14-NLIEUCRE-2  PIC X(03).                            
                 10 COMM-SL14-CTYPDOC-2   PIC X(02).                            
                 10 COMM-SL14-NUMDOC-2    PIC S9(7) COMP-3.                     
                 10 COMM-SL14-NCODIC-2    PIC X(07).                            
               05 COMM-SL14-SELARTICLE.                                         
                 10 COMM-SL14-NSOCO       PIC X(03).                            
                 10 COMM-SL14-NLIEUO      PIC X(03).                            
                 10 COMM-SL14-CFAM        PIC X(05).                            
                 10 COMM-SL14-CMARQ       PIC X(05).                            
                 10 COMM-SL14-NCODIC      PIC X(07).                            
                 10 COMM-SL14-DDEBUT      PIC X(08).                            
                 10 COMM-SL14-DFIN        PIC X(08).                            
               05 COMM-SL14-TOPMODIF        PIC X VALUE '1'.                    
                 88 COMM-SL14-TELQUEL       VALUE '1'.                          
                 88 COMM-SL14-MODIFICATION  VALUE '2'.                          
               05 COMM-SL14-SELECTION       PIC X VALUE '1'.                    
                 88 COMM-SL14-DOCUMENT      VALUE '1'.                          
                 88 COMM-SL14-ARTICLE       VALUE '2'.                          
               05 COMM-SL14-TOPMAJ          PIC X VALUE '2'.                    
                 88 COMM-SL14-MAJ           VALUE '1'.                          
                 88 COMM-SL14-RIEN          VALUE '2'.                          
      *      ZONES SL17.                      -----------------  146 C*         
             03 COMM-SL17-DATA.                                                 
               05 COMM-SL17-VALIDE-TRT     PIC X(1).                            
               05 COMM-SL17-VALIDE-SUP     PIC 9(1).                            
               05 COMM-SL17-PAGE             PIC 9(3).                          
               05 COMM-SL17-NBPAGES          PIC 9(3).                          
               05 COMM-SL17-DE-TSL11.                                           
                  10 COMM-SL17-NCODIC        PIC X(7).                          
                  10 COMM-SL17-NSOCCRE       PIC X(3).                          
                  10 COMM-SL17-NLIEUCRE      PIC X(3).                          
                  10 COMM-SL17-CTYPDOC       PIC X(2).                          
                  10 COMM-SL17-NUMDOC        PIC X(7).                          
                  10 COMM-SL17-CFAM          PIC X(5).                          
                  10 COMM-SL17-CMARQ         PIC X(5).                          
                  10 COMM-SL17-REFFOURN      PIC X(20).                         
                  10 COMM-SL17-SOCEXP        PIC X(3).                          
                  10 COMM-SL17-LIEUEXP       PIC X(3).                          
                  10 COMM-SL17-SOCREC        PIC X(3).                          
                  10 COMM-SL17-LIEUREC       PIC X(3).                          
                  10 COMM-SL17-QEXP          PIC X(5).                          
                  10 COMM-SL17-QREC          PIC X(5).                          
                  10 COMM-SL17-QARB          PIC X(5).                          
                  10 COMM-SL17-DEXP          PIC X(5).                          
                  10 COMM-SL17-NLDOC         PIC S9(5) COMP-3.                  
                  10 COMM-SL17-LLIEUEXP      PIC X(20).                         
                  10 COMM-SL17-LLIEUREC      PIC X(20).                         
               05 COMM-SL17-FLAG-PASSAGE     PIC 9 VALUE ZERO.                  
                  88 PREMIER-PASSAGE17       VALUE 0.                           
                  88 AUTRE-PASSAGE17         VALUE 1.                           
               05 COMM-SL17-CPT-PF3     PIC 9 VALUE ZERO.                       
                  88 ZERO-PF3                VALUE 0.                           
                  88 UN-PF3                  VALUE 1.                           
               05 COMM-SL17-CPT-TSINIT     PIC 9(4) VALUE ZERO.                 
               05 COMM-SL17-TOP-MODIF   PIC 9 VALUE ZERO.                       
                  88 PAS-MODIFICATION17            VALUE 0.                     
                  88 MODIFICATION17                VALUE 1.                     
               05 COMM-SL17-TRNPRE      PIC X(4).                               
      *      FILLER.                                                            
MULTI *      03 COMM-SL10-FILLER          PIC X(3066).                          
MULTI        03 COMM-SL10-FILLER          PIC X(3038).                          
      ******************************************************************        
      *       ZONES DISPONIBLES ------------------------------- 3724 C*         
      *       - 92  SL11        ------------------------------- 3632 C*         
      *       - 91  SL12        ------------------------------- 3541 C*         
      *       - 138 SL13        ------------------------------- 3403 C*         
      *       - 191 SL14        ------------------------------- 3212 C*         
      *       - 146 SL17        ------------------------------- 3066 C*         
      ******************************************************************        
                                                                                
