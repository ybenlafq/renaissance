      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE - CONDITION DE STOCKAGE        *
      * NOM FICHIER.: EASSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 079                                             *
      *****************************************************************
      *
       01  EASSPC6.
      *
      * TYPE ENREGISTREMENT : RECEPTION ARTICLE SITE STANDARD
           05      EASSPC6-TYP-ENREG      PIC  X(0006).
      * CODE SOCIETE
           05      EASSPC6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EASSPC6-CARTICLE       PIC  X(0018).
      * CONDITIONNEMENT STOCKAGE
           05      EASSPC6-COND-STOCKAGE  PIC  X(0005).
      * DATE DERNIERE MODIF
           05      EASSPC6-DMODIF         PIC  X(0008).
      * HEURE DERNIERE MODIF
           05      EASSPC6-HMODIF         PIC  X(0006).
      * FILLER  30
           05      EASSPC6-FILLER         PIC  X(0030).
      * FIN
           05      EASSPC6-FIN            PIC  X(0001).
      
