      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPM36   EPM36                                              00000020
      ***************************************************************** 00000030
       01   EPM36I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNPAGEF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNPAGEI   PIC X(3).                                       00000170
           02 LIGNEI OCCURS   14 TIMES .                                00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELL   COMP PIC S9(4).                                 00000190
      *--                                                                       
             03 MSELL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSELF   PIC X.                                          00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MSELI   PIC X.                                          00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCNOMLGL     COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCNOMLGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCNOMLGF     PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCNOMLGI     PIC X(15).                                 00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOMLGL     COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MLNOMLGL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MLNOMLGF     PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MLNOMLGI     PIC X(40).                                 00000300
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCQALIAL     COMP PIC S9(4).                            00000310
      *--                                                                       
             03 MCQALIAL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCQALIAF     PIC X.                                     00000320
             03 FILLER  PIC X(4).                                       00000330
             03 MCQALIAI     PIC X(15).                                 00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000350
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MLIBERRI  PIC X(62).                                      00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000390
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCODTRAI  PIC X(4).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCICSI    PIC X(5).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000470
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MNETNAMI  PIC X(8).                                       00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000510
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MSCREENI  PIC X(4).                                       00000540
      ***************************************************************** 00000550
      * SDF: EPM36   EPM36                                              00000560
      ***************************************************************** 00000570
       01   EPM36O REDEFINES EPM36I.                                    00000580
           02 FILLER    PIC X(12).                                      00000590
           02 FILLER    PIC X(2).                                       00000600
           02 MDATJOUA  PIC X.                                          00000610
           02 MDATJOUC  PIC X.                                          00000620
           02 MDATJOUP  PIC X.                                          00000630
           02 MDATJOUH  PIC X.                                          00000640
           02 MDATJOUV  PIC X.                                          00000650
           02 MDATJOUO  PIC X(10).                                      00000660
           02 FILLER    PIC X(2).                                       00000670
           02 MTIMJOUA  PIC X.                                          00000680
           02 MTIMJOUC  PIC X.                                          00000690
           02 MTIMJOUP  PIC X.                                          00000700
           02 MTIMJOUH  PIC X.                                          00000710
           02 MTIMJOUV  PIC X.                                          00000720
           02 MTIMJOUO  PIC X(5).                                       00000730
           02 FILLER    PIC X(2).                                       00000740
           02 MNPAGEA   PIC X.                                          00000750
           02 MNPAGEC   PIC X.                                          00000760
           02 MNPAGEP   PIC X.                                          00000770
           02 MNPAGEH   PIC X.                                          00000780
           02 MNPAGEV   PIC X.                                          00000790
           02 MNPAGEO   PIC 999.                                        00000800
           02 LIGNEO OCCURS   14 TIMES .                                00000810
             03 FILLER       PIC X(2).                                  00000820
             03 MSELA   PIC X.                                          00000830
             03 MSELC   PIC X.                                          00000840
             03 MSELP   PIC X.                                          00000850
             03 MSELH   PIC X.                                          00000860
             03 MSELV   PIC X.                                          00000870
             03 MSELO   PIC X.                                          00000880
             03 FILLER       PIC X(2).                                  00000890
             03 MCNOMLGA     PIC X.                                     00000900
             03 MCNOMLGC     PIC X.                                     00000910
             03 MCNOMLGP     PIC X.                                     00000920
             03 MCNOMLGH     PIC X.                                     00000930
             03 MCNOMLGV     PIC X.                                     00000940
             03 MCNOMLGO     PIC X(15).                                 00000950
             03 FILLER       PIC X(2).                                  00000960
             03 MLNOMLGA     PIC X.                                     00000970
             03 MLNOMLGC     PIC X.                                     00000980
             03 MLNOMLGP     PIC X.                                     00000990
             03 MLNOMLGH     PIC X.                                     00001000
             03 MLNOMLGV     PIC X.                                     00001010
             03 MLNOMLGO     PIC X(40).                                 00001020
             03 FILLER       PIC X(2).                                  00001030
             03 MCQALIAA     PIC X.                                     00001040
             03 MCQALIAC     PIC X.                                     00001050
             03 MCQALIAP     PIC X.                                     00001060
             03 MCQALIAH     PIC X.                                     00001070
             03 MCQALIAV     PIC X.                                     00001080
             03 MCQALIAO     PIC X(15).                                 00001090
           02 FILLER    PIC X(2).                                       00001100
           02 MLIBERRA  PIC X.                                          00001110
           02 MLIBERRC  PIC X.                                          00001120
           02 MLIBERRP  PIC X.                                          00001130
           02 MLIBERRH  PIC X.                                          00001140
           02 MLIBERRV  PIC X.                                          00001150
           02 MLIBERRO  PIC X(62).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MCODTRAA  PIC X.                                          00001180
           02 MCODTRAC  PIC X.                                          00001190
           02 MCODTRAP  PIC X.                                          00001200
           02 MCODTRAH  PIC X.                                          00001210
           02 MCODTRAV  PIC X.                                          00001220
           02 MCODTRAO  PIC X(4).                                       00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MCICSA    PIC X.                                          00001250
           02 MCICSC    PIC X.                                          00001260
           02 MCICSP    PIC X.                                          00001270
           02 MCICSH    PIC X.                                          00001280
           02 MCICSV    PIC X.                                          00001290
           02 MCICSO    PIC X(5).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MNETNAMA  PIC X.                                          00001320
           02 MNETNAMC  PIC X.                                          00001330
           02 MNETNAMP  PIC X.                                          00001340
           02 MNETNAMH  PIC X.                                          00001350
           02 MNETNAMV  PIC X.                                          00001360
           02 MNETNAMO  PIC X(8).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MSCREENA  PIC X.                                          00001390
           02 MSCREENC  PIC X.                                          00001400
           02 MSCREENP  PIC X.                                          00001410
           02 MSCREENH  PIC X.                                          00001420
           02 MSCREENV  PIC X.                                          00001430
           02 MSCREENO  PIC X(4).                                       00001440
                                                                                
