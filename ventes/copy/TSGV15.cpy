      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00000010
      *                                                               * 00000020
      *      TS SPECIFIQUE DU PROGRAMME TGV15                         * 00000030
      *      1 ITEM PAR PAGE                                          * 00000050
      *      12 LIGNES PAR ITEM CONTENANT LES PRE-RESERVATIONS        * 00000051
      *                                                               * 00000052
      *      TR : GV00  GESTION DES VENTES                            * 00000060
      *      PG : TGV15 CONSULTATION DES PRE-RESERVATIONS             * 00000070
      *                                                               * 00000080
      *      NOM: 'GV15' + EIBTRMID                                   * 00000090
      *      LG : 948 C             948  + (12 * 11) = 1080 C         * 00000091
      *                                                               * 00000092
      * INNO33 : 21/10/2010 AJOUT ORDERID                             * 00000092
      ***************************************************************** 00000093
      *                                                               * 00000094
       01  TS-GV15.                                                     00000095
      ********************************** LONGUEUR TS                    00000096
      *    05 TS-GV15-LONG               PIC S9(5) COMP-3 VALUE +948.   00000097
      *    05 TS-GV15-LONG               PIC S9(5) COMP-3 VALUE +959.   00000097
JB1711     05 TS-GV15-LONG              PIC S9(5) COMP-3 VALUE +1080.   00000097
      ********************************** DONNEES                        00000098
           05 TS-GV15-DONNEES.                                          00000099
              10 TS-GV15-LIGNE           OCCURS 12.                     00000100
                 15 TS-GV15-CTYPE        PIC    X(01).                  00000110
                    88 TS-GV15-ENTETE-DE-VENTE  VALUE 'E'.              00000111
                    88 TS-GV15-LIGNE-DE-VENTE   VALUE 'L'.              00000112
                 15 TS-GV15-NLIGNE       PIC    X(78).                  00000120
INNO33           15 TS-GV15-ORDERID      PIC    X(11).                  00000120
                                                                                
