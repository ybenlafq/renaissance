      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * TD00 : STATISTIQUE                                              00000020
      ***************************************************************** 00000030
       01   ETD05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEAI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNSOCIETEI     PIC X(3).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLSOCIETEI     PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPEL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCTYPEF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCTYPEI   PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPEL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLTYPEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLTYPEF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTYPEI   PIC X(20).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUI   PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MLLIEUI   PIC X(20).                                      00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MCMARQI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMARQL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLMARQF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMARQI   PIC X(20).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBDOSSL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNBDOSSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBDOSSF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNBDOSSI  PIC X(4).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDOSSIERL     COMP PIC S9(4).                            00000580
      *--                                                                       
           02 MCDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCDOSSIERF     PIC X.                                     00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MCDOSSIERI     PIC X(5).                                  00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDOSSIERL     COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MLDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLDOSSIERF     PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MLDOSSIERI     PIC X(20).                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATDEBL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDATDEBL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATDEBF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDATDEBI  PIC X(10).                                      00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MAUL      COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MAUL COMP-5 PIC S9(4).                                            
      *}                                                                        
           02 MAUF      PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MAUI      PIC X(2).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATFINL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDATFINL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATFINF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDATFINI  PIC X(10).                                      00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLOPTIONL      COMP PIC S9(4).                            00000780
      *--                                                                       
           02 MLOPTIONL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLOPTIONF      PIC X.                                     00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MLOPTIONI      PIC X(20).                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOLONNEL     COMP PIC S9(4).                            00000820
      *--                                                                       
           02 MLCOLONNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOLONNEF     PIC X.                                     00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLCOLONNEI     PIC X(78).                                 00000850
           02 MTABI OCCURS   11 TIMES .                                 00000860
             03 MLIGNED OCCURS   1 TIMES .                              00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLIGNEL    COMP PIC S9(4).                            00000880
      *--                                                                       
               04 MLIGNEL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MLIGNEF    PIC X.                                     00000890
               04 FILLER     PIC X(4).                                  00000900
               04 MLIGNEI    PIC X(78).                                 00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000920
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000930
           02 FILLER    PIC X(4).                                       00000940
           02 MLIBERRI  PIC X(78).                                      00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCODTRAI  PIC X(4).                                       00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCICSI    PIC X(5).                                       00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MNETNAMI  PIC X(8).                                       00001070
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001080
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001090
           02 FILLER    PIC X(4).                                       00001100
           02 MSCREENI  PIC X(4).                                       00001110
      ***************************************************************** 00001120
      * TD00 : STATISTIQUE                                              00001130
      ***************************************************************** 00001140
       01   ETD05O REDEFINES ETD05I.                                    00001150
           02 FILLER    PIC X(12).                                      00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MDATJOUA  PIC X.                                          00001180
           02 MDATJOUC  PIC X.                                          00001190
           02 MDATJOUP  PIC X.                                          00001200
           02 MDATJOUH  PIC X.                                          00001210
           02 MDATJOUV  PIC X.                                          00001220
           02 MDATJOUO  PIC X(10).                                      00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MTIMJOUA  PIC X.                                          00001250
           02 MTIMJOUC  PIC X.                                          00001260
           02 MTIMJOUP  PIC X.                                          00001270
           02 MTIMJOUH  PIC X.                                          00001280
           02 MTIMJOUV  PIC X.                                          00001290
           02 MTIMJOUO  PIC X(5).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MPAGEAA   PIC X.                                          00001320
           02 MPAGEAC   PIC X.                                          00001330
           02 MPAGEAP   PIC X.                                          00001340
           02 MPAGEAH   PIC X.                                          00001350
           02 MPAGEAV   PIC X.                                          00001360
           02 MPAGEAO   PIC Z9.                                         00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNBPA     PIC X.                                          00001390
           02 MNBPC     PIC X.                                          00001400
           02 MNBPP     PIC X.                                          00001410
           02 MNBPH     PIC X.                                          00001420
           02 MNBPV     PIC X.                                          00001430
           02 MNBPO     PIC Z9.                                         00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNSOCIETEA     PIC X.                                     00001460
           02 MNSOCIETEC     PIC X.                                     00001470
           02 MNSOCIETEP     PIC X.                                     00001480
           02 MNSOCIETEH     PIC X.                                     00001490
           02 MNSOCIETEV     PIC X.                                     00001500
           02 MNSOCIETEO     PIC X(3).                                  00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLSOCIETEA     PIC X.                                     00001530
           02 MLSOCIETEC     PIC X.                                     00001540
           02 MLSOCIETEP     PIC X.                                     00001550
           02 MLSOCIETEH     PIC X.                                     00001560
           02 MLSOCIETEV     PIC X.                                     00001570
           02 MLSOCIETEO     PIC X(20).                                 00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MCTYPEA   PIC X.                                          00001600
           02 MCTYPEC   PIC X.                                          00001610
           02 MCTYPEP   PIC X.                                          00001620
           02 MCTYPEH   PIC X.                                          00001630
           02 MCTYPEV   PIC X.                                          00001640
           02 MCTYPEO   PIC X(5).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLTYPEA   PIC X.                                          00001670
           02 MLTYPEC   PIC X.                                          00001680
           02 MLTYPEP   PIC X.                                          00001690
           02 MLTYPEH   PIC X.                                          00001700
           02 MLTYPEV   PIC X.                                          00001710
           02 MLTYPEO   PIC X(20).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MNLIEUA   PIC X.                                          00001740
           02 MNLIEUC   PIC X.                                          00001750
           02 MNLIEUP   PIC X.                                          00001760
           02 MNLIEUH   PIC X.                                          00001770
           02 MNLIEUV   PIC X.                                          00001780
           02 MNLIEUO   PIC X(3).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLLIEUA   PIC X.                                          00001810
           02 MLLIEUC   PIC X.                                          00001820
           02 MLLIEUP   PIC X.                                          00001830
           02 MLLIEUH   PIC X.                                          00001840
           02 MLLIEUV   PIC X.                                          00001850
           02 MLLIEUO   PIC X(20).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MCMARQA   PIC X.                                          00001880
           02 MCMARQC   PIC X.                                          00001890
           02 MCMARQP   PIC X.                                          00001900
           02 MCMARQH   PIC X.                                          00001910
           02 MCMARQV   PIC X.                                          00001920
           02 MCMARQO   PIC X(5).                                       00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MLMARQA   PIC X.                                          00001950
           02 MLMARQC   PIC X.                                          00001960
           02 MLMARQP   PIC X.                                          00001970
           02 MLMARQH   PIC X.                                          00001980
           02 MLMARQV   PIC X.                                          00001990
           02 MLMARQO   PIC X(20).                                      00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MNBDOSSA  PIC X.                                          00002020
           02 MNBDOSSC  PIC X.                                          00002030
           02 MNBDOSSP  PIC X.                                          00002040
           02 MNBDOSSH  PIC X.                                          00002050
           02 MNBDOSSV  PIC X.                                          00002060
           02 MNBDOSSO  PIC X(4).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCDOSSIERA     PIC X.                                     00002090
           02 MCDOSSIERC     PIC X.                                     00002100
           02 MCDOSSIERP     PIC X.                                     00002110
           02 MCDOSSIERH     PIC X.                                     00002120
           02 MCDOSSIERV     PIC X.                                     00002130
           02 MCDOSSIERO     PIC X(5).                                  00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MLDOSSIERA     PIC X.                                     00002160
           02 MLDOSSIERC     PIC X.                                     00002170
           02 MLDOSSIERP     PIC X.                                     00002180
           02 MLDOSSIERH     PIC X.                                     00002190
           02 MLDOSSIERV     PIC X.                                     00002200
           02 MLDOSSIERO     PIC X(20).                                 00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MDATDEBA  PIC X.                                          00002230
           02 MDATDEBC  PIC X.                                          00002240
           02 MDATDEBP  PIC X.                                          00002250
           02 MDATDEBH  PIC X.                                          00002260
           02 MDATDEBV  PIC X.                                          00002270
           02 MDATDEBO  PIC X(10).                                      00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MAUA      PIC X.                                          00002300
           02 MAUC      PIC X.                                          00002310
           02 MAUP      PIC X.                                          00002320
           02 MAUH      PIC X.                                          00002330
           02 MAUV      PIC X.                                          00002340
           02 MAUO      PIC X(2).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MDATFINA  PIC X.                                          00002370
           02 MDATFINC  PIC X.                                          00002380
           02 MDATFINP  PIC X.                                          00002390
           02 MDATFINH  PIC X.                                          00002400
           02 MDATFINV  PIC X.                                          00002410
           02 MDATFINO  PIC X(10).                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MLOPTIONA      PIC X.                                     00002440
           02 MLOPTIONC PIC X.                                          00002450
           02 MLOPTIONP PIC X.                                          00002460
           02 MLOPTIONH PIC X.                                          00002470
           02 MLOPTIONV PIC X.                                          00002480
           02 MLOPTIONO      PIC X(20).                                 00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MLCOLONNEA     PIC X.                                     00002510
           02 MLCOLONNEC     PIC X.                                     00002520
           02 MLCOLONNEP     PIC X.                                     00002530
           02 MLCOLONNEH     PIC X.                                     00002540
           02 MLCOLONNEV     PIC X.                                     00002550
           02 MLCOLONNEO     PIC X(78).                                 00002560
           02 MTABO OCCURS   11 TIMES .                                 00002570
             03 DFHMS1 OCCURS   1 TIMES .                               00002580
               04 FILLER     PIC X(2).                                  00002590
               04 MLIGNEA    PIC X.                                     00002600
               04 MLIGNEC    PIC X.                                     00002610
               04 MLIGNEP    PIC X.                                     00002620
               04 MLIGNEH    PIC X.                                     00002630
               04 MLIGNEV    PIC X.                                     00002640
               04 MLIGNEO    PIC X(78).                                 00002650
           02 FILLER    PIC X(2).                                       00002660
           02 MLIBERRA  PIC X.                                          00002670
           02 MLIBERRC  PIC X.                                          00002680
           02 MLIBERRP  PIC X.                                          00002690
           02 MLIBERRH  PIC X.                                          00002700
           02 MLIBERRV  PIC X.                                          00002710
           02 MLIBERRO  PIC X(78).                                      00002720
           02 FILLER    PIC X(2).                                       00002730
           02 MCODTRAA  PIC X.                                          00002740
           02 MCODTRAC  PIC X.                                          00002750
           02 MCODTRAP  PIC X.                                          00002760
           02 MCODTRAH  PIC X.                                          00002770
           02 MCODTRAV  PIC X.                                          00002780
           02 MCODTRAO  PIC X(4).                                       00002790
           02 FILLER    PIC X(2).                                       00002800
           02 MCICSA    PIC X.                                          00002810
           02 MCICSC    PIC X.                                          00002820
           02 MCICSP    PIC X.                                          00002830
           02 MCICSH    PIC X.                                          00002840
           02 MCICSV    PIC X.                                          00002850
           02 MCICSO    PIC X(5).                                       00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNETNAMA  PIC X.                                          00002880
           02 MNETNAMC  PIC X.                                          00002890
           02 MNETNAMP  PIC X.                                          00002900
           02 MNETNAMH  PIC X.                                          00002910
           02 MNETNAMV  PIC X.                                          00002920
           02 MNETNAMO  PIC X(8).                                       00002930
           02 FILLER    PIC X(2).                                       00002940
           02 MSCREENA  PIC X.                                          00002950
           02 MSCREENC  PIC X.                                          00002960
           02 MSCREENP  PIC X.                                          00002970
           02 MSCREENH  PIC X.                                          00002980
           02 MSCREENV  PIC X.                                          00002990
           02 MSCREENO  PIC X(4).                                       00003000
                                                                                
