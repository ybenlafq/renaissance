      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBA51   EBA51                                              00000020
      ***************************************************************** 00000030
       01   EBA51I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFONCTL  COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MCFONCTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCFONCTF  PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MCFONCTI  PIC X(3).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCGLL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNSOCGLL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNSOCGLF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNSOCGLI  PIC X(5).                                       00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAUXL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MNAUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNAUXF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MNAUXI    PIC X(3).                                       00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLAUXL    COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MLAUXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLAUXF    PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MLAUXI    PIC X(40).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTIERSL  COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MNTIERSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTIERSF  PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNTIERSI  PIC X(10).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRSOCIALEL    COMP PIC S9(4).                            00000360
      *--                                                                       
           02 MLRSOCIALEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLRSOCIALEF    PIC X.                                     00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MLRSOCIALEI    PIC X(32).                                 00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTIERSBAL     COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MCTIERSBAL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTIERSBAF     PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MCTIERSBAI     PIC X(6).                                  00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEXTFUSIOL    COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MTEXTFUSIOL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MTEXTFUSIOF    PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MTEXTFUSIOI    PIC X(31).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANCTIERSL     COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MANCTIERSL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MANCTIERSF     PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MANCTIERSI     PIC X(6).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWELIBELLEL    COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MWELIBELLEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWELIBELLEF    PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MWELIBELLEI    PIC X.                                     00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEFACTUREL    COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MWEFACTUREL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWEFACTUREF    PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MWEFACTUREI    PIC X.                                     00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWVTEMINIL     COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MWVTEMINIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MWVTEMINIF     PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MWVTEMINII     PIC X.                                     00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPVTEMINIL     COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MPVTEMINIL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MPVTEMINIF     PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MPVTEMINII     PIC X(5).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEMONTANTL    COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MWEMONTANTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWEMONTANTF    PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MWEMONTANTI    PIC X.                                     00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWEMISEUROL    COMP PIC S9(4).                            00000720
      *--                                                                       
           02 MWEMISEUROL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MWEMISEUROF    PIC X.                                     00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MWEMISEUROI    PIC X.                                     00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MQTAUXREML     COMP PIC S9(4).                            00000760
      *--                                                                       
           02 MQTAUXREML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MQTAUXREMF     PIC X.                                     00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MQTAUXREMI     PIC X(6).                                  00000790
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWTYPREML      COMP PIC S9(4).                            00000800
      *--                                                                       
           02 MWTYPREML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWTYPREMF      PIC X.                                     00000810
           02 FILLER    PIC X(4).                                       00000820
           02 MWTYPREMI      PIC X.                                     00000830
      * ZONE CMD AIDA                                                   00000840
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000850
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000860
           02 FILLER    PIC X(4).                                       00000870
           02 MZONCMDI  PIC X(15).                                      00000880
      * MESSAGE ERREUR                                                  00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MLIBERRI  PIC X(58).                                      00000930
      * CODE TRANSACTION                                                00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCODTRAI  PIC X(4).                                       00000980
      * CICS DE TRAVAIL                                                 00000990
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001000
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001010
           02 FILLER    PIC X(4).                                       00001020
           02 MCICSI    PIC X(5).                                       00001030
      * NETNAME                                                         00001040
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001050
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001060
           02 FILLER    PIC X(4).                                       00001070
           02 MNETNAMI  PIC X(8).                                       00001080
      * CODE TERMINAL                                                   00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MSCREENI  PIC X(5).                                       00001130
      ***************************************************************** 00001140
      * SDF: EBA51   EBA51                                              00001150
      ***************************************************************** 00001160
       01   EBA51O REDEFINES EBA51I.                                    00001170
           02 FILLER    PIC X(12).                                      00001180
      * DATE DU JOUR(JJ/MM/AAAA)                                        00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
      * HEURE                                                           00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MTIMJOUA  PIC X.                                          00001290
           02 MTIMJOUC  PIC X.                                          00001300
           02 MTIMJOUP  PIC X.                                          00001310
           02 MTIMJOUH  PIC X.                                          00001320
           02 MTIMJOUV  PIC X.                                          00001330
           02 MTIMJOUO  PIC X(5).                                       00001340
           02 FILLER    PIC X(2).                                       00001350
           02 MCFONCTA  PIC X.                                          00001360
           02 MCFONCTC  PIC X.                                          00001370
           02 MCFONCTP  PIC X.                                          00001380
           02 MCFONCTH  PIC X.                                          00001390
           02 MCFONCTV  PIC X.                                          00001400
           02 MCFONCTO  PIC X(3).                                       00001410
           02 FILLER    PIC X(2).                                       00001420
           02 MNSOCGLA  PIC X.                                          00001430
           02 MNSOCGLC  PIC X.                                          00001440
           02 MNSOCGLP  PIC X.                                          00001450
           02 MNSOCGLH  PIC X.                                          00001460
           02 MNSOCGLV  PIC X.                                          00001470
           02 MNSOCGLO  PIC X(5).                                       00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNAUXA    PIC X.                                          00001500
           02 MNAUXC    PIC X.                                          00001510
           02 MNAUXP    PIC X.                                          00001520
           02 MNAUXH    PIC X.                                          00001530
           02 MNAUXV    PIC X.                                          00001540
           02 MNAUXO    PIC X(3).                                       00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLAUXA    PIC X.                                          00001570
           02 MLAUXC    PIC X.                                          00001580
           02 MLAUXP    PIC X.                                          00001590
           02 MLAUXH    PIC X.                                          00001600
           02 MLAUXV    PIC X.                                          00001610
           02 MLAUXO    PIC X(40).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MNTIERSA  PIC X.                                          00001640
           02 MNTIERSC  PIC X.                                          00001650
           02 MNTIERSP  PIC X.                                          00001660
           02 MNTIERSH  PIC X.                                          00001670
           02 MNTIERSV  PIC X.                                          00001680
           02 MNTIERSO  PIC X(10).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MLRSOCIALEA    PIC X.                                     00001710
           02 MLRSOCIALEC    PIC X.                                     00001720
           02 MLRSOCIALEP    PIC X.                                     00001730
           02 MLRSOCIALEH    PIC X.                                     00001740
           02 MLRSOCIALEV    PIC X.                                     00001750
           02 MLRSOCIALEO    PIC X(32).                                 00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MCTIERSBAA     PIC X.                                     00001780
           02 MCTIERSBAC     PIC X.                                     00001790
           02 MCTIERSBAP     PIC X.                                     00001800
           02 MCTIERSBAH     PIC X.                                     00001810
           02 MCTIERSBAV     PIC X.                                     00001820
           02 MCTIERSBAO     PIC X(6).                                  00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MTEXTFUSIOA    PIC X.                                     00001850
           02 MTEXTFUSIOC    PIC X.                                     00001860
           02 MTEXTFUSIOP    PIC X.                                     00001870
           02 MTEXTFUSIOH    PIC X.                                     00001880
           02 MTEXTFUSIOV    PIC X.                                     00001890
           02 MTEXTFUSIOO    PIC X(31).                                 00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MANCTIERSA     PIC X.                                     00001920
           02 MANCTIERSC     PIC X.                                     00001930
           02 MANCTIERSP     PIC X.                                     00001940
           02 MANCTIERSH     PIC X.                                     00001950
           02 MANCTIERSV     PIC X.                                     00001960
           02 MANCTIERSO     PIC X(6).                                  00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MWELIBELLEA    PIC X.                                     00001990
           02 MWELIBELLEC    PIC X.                                     00002000
           02 MWELIBELLEP    PIC X.                                     00002010
           02 MWELIBELLEH    PIC X.                                     00002020
           02 MWELIBELLEV    PIC X.                                     00002030
           02 MWELIBELLEO    PIC X.                                     00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MWEFACTUREA    PIC X.                                     00002060
           02 MWEFACTUREC    PIC X.                                     00002070
           02 MWEFACTUREP    PIC X.                                     00002080
           02 MWEFACTUREH    PIC X.                                     00002090
           02 MWEFACTUREV    PIC X.                                     00002100
           02 MWEFACTUREO    PIC X.                                     00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MWVTEMINIA     PIC X.                                     00002130
           02 MWVTEMINIC     PIC X.                                     00002140
           02 MWVTEMINIP     PIC X.                                     00002150
           02 MWVTEMINIH     PIC X.                                     00002160
           02 MWVTEMINIV     PIC X.                                     00002170
           02 MWVTEMINIO     PIC X.                                     00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MPVTEMINIA     PIC X.                                     00002200
           02 MPVTEMINIC     PIC X.                                     00002210
           02 MPVTEMINIP     PIC X.                                     00002220
           02 MPVTEMINIH     PIC X.                                     00002230
           02 MPVTEMINIV     PIC X.                                     00002240
           02 MPVTEMINIO     PIC X(5).                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MWEMONTANTA    PIC X.                                     00002270
           02 MWEMONTANTC    PIC X.                                     00002280
           02 MWEMONTANTP    PIC X.                                     00002290
           02 MWEMONTANTH    PIC X.                                     00002300
           02 MWEMONTANTV    PIC X.                                     00002310
           02 MWEMONTANTO    PIC X.                                     00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MWEMISEUROA    PIC X.                                     00002340
           02 MWEMISEUROC    PIC X.                                     00002350
           02 MWEMISEUROP    PIC X.                                     00002360
           02 MWEMISEUROH    PIC X.                                     00002370
           02 MWEMISEUROV    PIC X.                                     00002380
           02 MWEMISEUROO    PIC X.                                     00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MQTAUXREMA     PIC X.                                     00002410
           02 MQTAUXREMC     PIC X.                                     00002420
           02 MQTAUXREMP     PIC X.                                     00002430
           02 MQTAUXREMH     PIC X.                                     00002440
           02 MQTAUXREMV     PIC X.                                     00002450
           02 MQTAUXREMO     PIC X(6).                                  00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MWTYPREMA      PIC X.                                     00002480
           02 MWTYPREMC PIC X.                                          00002490
           02 MWTYPREMP PIC X.                                          00002500
           02 MWTYPREMH PIC X.                                          00002510
           02 MWTYPREMV PIC X.                                          00002520
           02 MWTYPREMO      PIC X.                                     00002530
      * ZONE CMD AIDA                                                   00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MZONCMDA  PIC X.                                          00002560
           02 MZONCMDC  PIC X.                                          00002570
           02 MZONCMDP  PIC X.                                          00002580
           02 MZONCMDH  PIC X.                                          00002590
           02 MZONCMDV  PIC X.                                          00002600
           02 MZONCMDO  PIC X(15).                                      00002610
      * MESSAGE ERREUR                                                  00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MLIBERRA  PIC X.                                          00002640
           02 MLIBERRC  PIC X.                                          00002650
           02 MLIBERRP  PIC X.                                          00002660
           02 MLIBERRH  PIC X.                                          00002670
           02 MLIBERRV  PIC X.                                          00002680
           02 MLIBERRO  PIC X(58).                                      00002690
      * CODE TRANSACTION                                                00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCODTRAA  PIC X.                                          00002720
           02 MCODTRAC  PIC X.                                          00002730
           02 MCODTRAP  PIC X.                                          00002740
           02 MCODTRAH  PIC X.                                          00002750
           02 MCODTRAV  PIC X.                                          00002760
           02 MCODTRAO  PIC X(4).                                       00002770
      * CICS DE TRAVAIL                                                 00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCICSA    PIC X.                                          00002800
           02 MCICSC    PIC X.                                          00002810
           02 MCICSP    PIC X.                                          00002820
           02 MCICSH    PIC X.                                          00002830
           02 MCICSV    PIC X.                                          00002840
           02 MCICSO    PIC X(5).                                       00002850
      * NETNAME                                                         00002860
           02 FILLER    PIC X(2).                                       00002870
           02 MNETNAMA  PIC X.                                          00002880
           02 MNETNAMC  PIC X.                                          00002890
           02 MNETNAMP  PIC X.                                          00002900
           02 MNETNAMH  PIC X.                                          00002910
           02 MNETNAMV  PIC X.                                          00002920
           02 MNETNAMO  PIC X(8).                                       00002930
      * CODE TERMINAL                                                   00002940
           02 FILLER    PIC X(2).                                       00002950
           02 MSCREENA  PIC X.                                          00002960
           02 MSCREENC  PIC X.                                          00002970
           02 MSCREENP  PIC X.                                          00002980
           02 MSCREENH  PIC X.                                          00002990
           02 MSCREENV  PIC X.                                          00003000
           02 MSCREENO  PIC X(5).                                       00003010
                                                                                
