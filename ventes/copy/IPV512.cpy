      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV512 AU 20/01/2005  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,03,PD,A,                          *        
      *                           16,02,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV512.                                                        
            05 NOMETAT-IPV512           PIC X(6) VALUE 'IPV512'.                
            05 RUPTURES-IPV512.                                                 
           10 IPV512-NSOCIETE           PIC X(03).                      007  003
           10 IPV512-NLIEU              PIC X(03).                      010  003
           10 IPV512-WSEQED             PIC S9(05)      COMP-3.         013  003
           10 IPV512-NAGREGATED         PIC X(02).                      016  002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV512-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IPV512-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV512.                                                   
           10 IPV512-RUBRIQUE           PIC X(20).                      020  020
           10 IPV512-COMM               PIC S9(07)V9(2) COMP-3.         040  005
           10 IPV512-MBU                PIC S9(07)V9(2) COMP-3.         045  005
           10 IPV512-MBUGRP             PIC S9(07)V9(2) COMP-3.         050  005
           10 IPV512-PCA                PIC S9(07)V9(2) COMP-3.         055  005
           10 IPV512-PCAGRP             PIC S9(07)V9(2) COMP-3.         060  005
           10 IPV512-PMT                PIC S9(07)V9(2) COMP-3.         065  005
           10 IPV512-PMTADD             PIC S9(07)V9(2) COMP-3.         070  005
           10 IPV512-PMTEP              PIC S9(07)V9(2) COMP-3.         075  005
           10 IPV512-PMTPSE             PIC S9(07)V9(2) COMP-3.         080  005
           10 IPV512-PMTTOT             PIC S9(07)V9(2) COMP-3.         085  005
           10 IPV512-PMTVOL             PIC S9(07)V9(2) COMP-3.         090  005
           10 IPV512-PRIMECLI           PIC S9(07)V9(2) COMP-3.         095  005
           10 IPV512-QVENDUE            PIC S9(05)      COMP-3.         100  003
           10 IPV512-QVENDUEGRP         PIC S9(05)      COMP-3.         103  003
           10 IPV512-RECETTE            PIC S9(07)V9(2) COMP-3.         106  005
           10 IPV512-DEBPERIODE         PIC X(08).                      111  008
           10 IPV512-FINPERIODE         PIC X(08).                      119  008
            05 FILLER                      PIC X(386).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV512-LONG           PIC S9(4)   COMP  VALUE +126.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV512-LONG           PIC S9(4) COMP-5  VALUE +126.           
                                                                                
      *}                                                                        
