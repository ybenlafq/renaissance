      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  TRANSACTION: VI00                                             *        
      *  TITRE      : COMMAREA DE L'APPLICATION VI00                   *        
      *  LONGUEUR   : 9096                                             *        
      *                                                                *        
      *                                                                         
       01  ICOMM                        PIC 9(02)          VALUE ZEROES.        
       01  ICOMM1                       PIC 9(02)          VALUE ZEROES.        
       01  C-GV01-NLIGNE-MAX            PIC 9(02)          VALUE 80.            
      * MAX LISTE DES COMMUNES                                                  
       01  COM-TAB-MAX                  PIC 9(05)          VALUE 350.           
      * MAX LISTE DES MAGASINS                                                  
       01  MAG-TAB-MAX                  PIC 9(05)          VALUE 500.           
      * MAX TABLE DES VENTES                                                    
       01  TAB-MAX                      PIC 9(05)          VALUE 020.           
      *                                                                         
       01  Z-COMMAREA.                                                          
           02 FILLER-COM-AIDA           PIC X(100).                             
      *                                                                         
      **** ZONES RESERVEES EN PROVENANCE DE CICS ----------------- 020          
           02 C-CICS-APPLID             PIC X(08).                              
           02 C-CICS-NETNAM             PIC X(08).                              
           02 C-CICS-TRANSA             PIC X(04).                              
      *                                                                         
      **** ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION --- 100          
           02 C-DATE-SIECLE             PIC X(02).                              
           02 C-DATE-ANNEE              PIC X(02).                              
           02 C-DATE-MOIS               PIC X(02).                              
           02 C-DATE-JOUR               PIC 9(02).                              
           02 C-DATE-QNTA               PIC 9(03).                              
           02 C-DATE-QNT0               PIC 9(05).                              
           02 C-DATE-BISX               PIC 9(01).                              
           02 C-DATE-JSM                PIC 9(01).                              
           02 C-DATE-JSM-LC             PIC X(03).                              
           02 C-DATE-JSM-LL             PIC X(08).                              
           02 C-DATE-MOIS-LC            PIC X(03).                              
           02 C-DATE-MOIS-LL            PIC X(08).                              
           02 C-DATE-SSAAMMJJ           PIC X(08).                              
           02 C-DATE-AAMMJJ             PIC X(06).                              
           02 C-DATE-JJMMSSAA           PIC X(08).                              
           02 C-DATE-JJMMAA             PIC X(06).                              
           02 C-DATE-JJ-MM-AA           PIC X(08).                              
           02 C-DATE-JJ-MM-SSAA         PIC X(10).                              
           02 C-DATE-WEEK.                                                      
              05 C-DATE-SEMSS                 PIC 9(02).                        
              05 C-DATE-SEMAA                 PIC 9(02).                        
              05 C-DATE-SEMNU                 PIC 9(02).                        
           02 C-DATE-FILLER             PIC X(08).                              
      *                                                                         
      **** ZONES RESERVEES TRAITEMENT DU SWAP -------------------- 152          
      *    02 C-SWAP-CURS               PIC S9(4) COMP VALUE -1.                
      *    02 C-SWAP-ATTR               OCCURS 150 PIC X(1).                    
      *                                                                         
      **** ZONES APPLICATIVES VI00 GENERALES ********************* 124          
           02 W-SIGNATURE                PIC 9(1) VALUE 0.                      
              88 USER-CORRECT                     VALUE 0.                      
              88 USER-INCORRECT                   VALUE 1.                      
           02 W-DISTANCE-DEPASSE         PIC 9(1) VALUE 0.                      
              88 DISTANCE-NON-DEPAS               VALUE 0.                      
              88 DISTANCE-DEPAS                   VALUE 1.                      
           02 C-PAGE-COURANTE            PIC 9(02)       VALUE ZEROES.          
           02 COMM-I                     PIC 9(05)      VALUE ZEROES.           
           02 W-MPOSTALI                 PIC X(05).                             
           02 W-CPOSTAL                  PIC X(05).                             
           02 W-COMMUNE                  PIC X(20).                             
           02 WS-TYPE                    PIC X(03).                             
           02 W-TYPE                     PIC X(03).                             
           02 W-MCUIS                    PIC X(03).                             
           02 C-VI00-CPOSTAL             PIC X(05).                             
           02 C-VI00-COMMUNE             PIC X(32).                             
           02 C-VI00-NSOC                PIC X(03).                             
           02 C-VI00-NSOCORIG            PIC X(03).                             
           02 C-VI00-NLIEUORIG           PIC X(03).                             
           02 C-VI00-LLIEUORIG           PIC X(20).                             
           02 C-VI00-VENDEUR             PIC X(06).                             
           02 C-VI00-APPLI.                                                     
              05 C-POSITION-AFFICHAGE    PIC 9(01).                             
                 88 C-LISTE-VENTES       VALUE 0.                               
                 88 C-LISTE-COMMUNES     VALUE 1.                               
                 88 C-LISTE-MAGASINS     VALUE 2.                               
      *       05 C-AFFICHAGE-PRECEDENT   PIC X(14).                             
      * TABLEAU DES MAGASINS                                                    
           02 C-MAGASIN-TAB.                                                    
             03 CMAG-TAB.                                                       
                05 C-MAG-NBP                   PIC 9(05) VALUE 0.               
                05 C-MAG-POSTE       OCCURS  500.                               
                   10  C-MAG-NSOC              PIC X(03).                       
                   10  C-MAG-NLIEU             PIC X(03).                       
                   10  C-MAG-LLIEU             PIC X(20).                       
                   10  C-MAG-COMMUNE           PIC X(20).                       
                   10  C-MAG-DISTANCE          PIC S9(04).                      
                   10  C-MAG-CPOSTAL           PIC X(05).                       
      * TABLEAU DES COMMUNES PAR CODE POSTAL                                    
           02 C-COMMUNE-TAB.                                                    
    ***      03 C-COM-TAB.                                                      
                05 C-COM-NBP                   PIC 9(05) VALUE 0.               
    ***         05 C-COM-POSTE       OCCURS  350.                               
    ***            10  C-COM-NSOC              PIC X(03).                       
    ***            10  C-COM-LIBELLE           PIC X(32).                       
    ***            10  C-COM-CPOSTAL           PIC X(05).                       
    ***            10  C-COM-WCONTRA           PIC X(01).                       
      * TABLEAU DES VENTES                                                      
           02 C-TAB-VENTES.                                                     
             05 C-TAB-NBP                   PIC 9(05) VALUE 0.                  
             05 C-TAB-POSTE       OCCURS  020.                                  
               10 C-TAB-LMAG              PIC X(20).                            
               10 C-TAB-NSOCV             PIC X(03).                            
               10 C-TAB-NLIEUV            PIC X(03).                            
               10 C-TAB-NVENTE            PIC X(07).                            
               10 C-TAB-DVENTE            PIC X(08).                            
               10 C-TAB-CVENDEURO         PIC X(06).                            
      **       10 C-TAB-CVENDEURG         PIC X(06).                            
      **       10 C-TAB-CTITRENOM         PIC X(05).                            
               10 C-TAB-LNOM              PIC X(25).                            
      **       10 C-TAB-LPRENOM           PIC X(15).                            
      **       10 C-TAB-CPOSTAL           PIC X(05).                            
               10 C-TAB-CTOTAL            PIC S9(7)V9(3).                       
                                                                                
