      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV2101                            *                
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV2101        *                
      **********************************************************                
       01  RVGV2101.                                                            
           02  GV21-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV21-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV21-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV21-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV21-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV21-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV21-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV21-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV21-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GV21-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV21-QRESERV                                                     
               PIC S9(5) COMP-3.                                                
           02  GV21-WCQERESF                                                    
               PIC X(0001).                                                     
           02  GV21-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV21-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV21-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV21-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV21-WACOMMUTER                                                  
               PIC X(0001).                                                     
           02  GV21-LCOMMENT                                                    
               PIC X(0035).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV2101                 *                
      **********************************************************                
       01  RVGV2101-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-QRESERV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-QRESERV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-WACOMMUTER-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV21-WACOMMUTER-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV21-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV21-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
