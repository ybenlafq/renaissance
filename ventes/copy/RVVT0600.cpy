      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVVT0600                                     00020002
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0600                 00060002
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVVT0600.                                                    00090002
           02  VT06-NFLAG                                               00100002
               PIC X(0001).                                             00110000
           02  VT06-NCLUSTER                                            00120002
               PIC X(0013).                                             00130000
           02  VT06-NSOCIETE                                            00140002
               PIC X(0003).                                             00150000
           02  VT06-NDOSSIER                                            00160002
               PIC S9(3) COMP-3.                                        00170003
           02  VT06-NSEQNQ                                              00180002
               PIC S9(5) COMP-3.                                        00190001
           02  VT06-NSEQ                                                00200002
               PIC S9(3) COMP-3.                                        00210001
      *                                                                 00220000
      *---------------------------------------------------------        00230000
      *   LISTE DES FLAGS DE LA TABLE RVVT0600                          00240002
      *---------------------------------------------------------        00250000
      *                                                                 00260000
       01  RVVT0600-FLAGS.                                              00270002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NFLAG-F                                             00280002
      *        PIC S9(4) COMP.                                          00290000
      *--                                                                       
           02  VT06-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NCLUSTER-F                                          00300002
      *        PIC S9(4) COMP.                                          00310000
      *--                                                                       
           02  VT06-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NSOCIETE-F                                          00320002
      *        PIC S9(4) COMP.                                          00330000
      *--                                                                       
           02  VT06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NDOSSIER-F                                          00340002
      *        PIC S9(4) COMP.                                          00350000
      *--                                                                       
           02  VT06-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NSEQNQ-F                                            00360002
      *        PIC S9(4) COMP.                                          00370000
      *--                                                                       
           02  VT06-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT06-NSEQ-F                                              00380002
      *        PIC S9(4) COMP.                                          00390000
      *--                                                                       
           02  VT06-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00400000
                                                                                
