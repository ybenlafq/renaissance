      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IGV750 AU 05/09/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,26,BI,A,                          *        
      *                           36,08,BI,A,                          *        
      *                           44,03,BI,A,                          *        
      *                           47,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IGV750.                                                        
            05 NOMETAT-IGV750           PIC X(6) VALUE 'IGV750'.                
            05 RUPTURES-IGV750.                                                 
           10 IGV750-NSOCIETE           PIC X(03).                      007  003
           10 IGV750-LPAYS              PIC X(26).                      010  026
           10 IGV750-DLIVR              PIC X(08).                      036  008
           10 IGV750-NMAG               PIC X(03).                      044  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IGV750-SEQUENCE           PIC S9(04) COMP.                047  002
      *--                                                                       
           10 IGV750-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IGV750.                                                   
           10 IGV750-LNOM               PIC X(25).                      049  025
           10 IGV750-NCODIC             PIC X(07).                      074  007
           10 IGV750-NVENTE             PIC X(07).                      081  007
           10 IGV750-PCAHT              PIC S9(07)V9(2) COMP-3.         088  005
           10 IGV750-PCUMULM-1          PIC S9(08)V9(2) COMP-3.         093  006
            05 FILLER                      PIC X(414).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IGV750-LONG           PIC S9(4)   COMP  VALUE +098.           
      *                                                                         
      *--                                                                       
        01  DSECT-IGV750-LONG           PIC S9(4) COMP-5  VALUE +098.           
                                                                                
      *}                                                                        
