      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CEFOR FORMATS éTIQUETTES QUICK PRESS   *        
      *----------------------------------------------------------------*        
       01  RVCEFOR.                                                             
           05  CEFOR-CTABLEG2    PIC X(15).                                     
           05  CEFOR-CTABLEG2-REDEF REDEFINES CEFOR-CTABLEG2.                   
               10  CEFOR-CFORMAT         PIC X(05).                             
           05  CEFOR-WTABLEG     PIC X(80).                                     
           05  CEFOR-WTABLEG-REDEF  REDEFINES CEFOR-WTABLEG.                    
               10  CEFOR-WACTIF          PIC X(01).                             
               10  CEFOR-LIBELLE         PIC X(20).                             
               10  CEFOR-WDATA           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCEFOR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEFOR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CEFOR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CEFOR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CEFOR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
