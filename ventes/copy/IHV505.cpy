      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IHV505 AU 21/01/1994  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,05,BI,A,                          *        
      *                           20,02,BI,A,                          *        
      *                           22,03,BI,A,                          *        
      *                           25,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IHV505.                                                        
            05 NOMETAT-IHV505           PIC X(6) VALUE 'IHV505'.                
            05 RUPTURES-IHV505.                                                 
           10 IHV505-NSOCIETE           PIC X(03).                      007  003
           10 IHV505-CRAYON1            PIC X(05).                      010  005
           10 IHV505-CRAYON2            PIC X(05).                      015  005
           10 IHV505-CGRPMAG            PIC X(02).                      020  002
           10 IHV505-NMAG               PIC X(03).                      022  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IHV505-SEQUENCE           PIC S9(04) COMP.                025  002
      *--                                                                       
           10 IHV505-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IHV505.                                                   
           10 IHV505-PCA                PIC S9(11)V9(2) COMP-3.         027  007
           10 IHV505-PCA-PM             PIC S9(11)V9(2) COMP-3.         034  007
           10 IHV505-PMARGE             PIC S9(11)V9(2) COMP-3.         041  007
           10 IHV505-PMARGE-PM          PIC S9(11)V9(2) COMP-3.         048  007
           10 IHV505-QPIECES            PIC S9(09)      COMP-3.         055  005
           10 IHV505-QPIECES-PM         PIC S9(09)      COMP-3.         060  005
            05 FILLER                      PIC X(448).                          
                                                                                
