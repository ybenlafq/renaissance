      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGA10   EGA10                                              00000020
      ***************************************************************** 00000030
       01   EPM10I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBSOCL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MLIBSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBSOCF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MLIBSOCI  PIC X(20).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBLIEUL      COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLIBLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIBLIEUF      PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLIBLIEUI      PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMAGEDL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLMAGEDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMAGEDF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLMAGEDI  PIC X(15).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLORDREL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MLORDREL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLORDREF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLORDREI  PIC X(15).                                      00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMAGPL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MWMAGPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWMAGPF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MWMAGPI   PIC X.                                          00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNOBCDL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MWNOBCDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWNOBCDF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWNOBCDI  PIC X.                                          00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWNOPSEL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MWNOPSEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWNOPSEF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MWNOPSEI  PIC X.                                          00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWDEROGL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MWDEROGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWDEROGF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MWDEROGI  PIC X.                                          00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWMDGRDL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MWMDGRDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWMDGRDF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MWMDGRDI  PIC X.                                          00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWAUTMGL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MWAUTMGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWAUTMGF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MWAUTMGI  PIC X.                                          00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENDFL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MCVENDFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVENDFF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MCVENDFI  PIC X(7).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCOPERFL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MCOPERFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCOPERFF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MCOPERFI  PIC X(7).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWSTOCKL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MWSTOCKL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MWSTOCKF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MWSTOCKI  PIC X.                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVENLSL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MCVENLSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVENLSF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MCVENLSI  PIC X(7).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCVSECRL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCVSECRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCVSECRF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCVSECRI  PIC X(7).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTOMAGL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MCTOMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCTOMAGF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MCTOMAGI  PIC X.                                          00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSSAVL   COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCSSAVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCSSAVF   PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCSSAVI   PIC X(3).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLSAVL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCLSAVL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCLSAVF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCLSAVI   PIC X(3).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCSDACML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MCSDACML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCSDACMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MCSDACMI  PIC X(3).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCLDACML  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MCLDACML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCLDACMF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MCLDACMI  PIC X(3).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDSEM01L  COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MDSEM01L COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDSEM01F  PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MDSEM01I  PIC X(8).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(78).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCICSI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(5).                                       00001250
      ***************************************************************** 00001260
      * SDF: EGA10   EGA10                                              00001270
      ***************************************************************** 00001280
       01   EPM10O REDEFINES EPM10I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MDATJOUA  PIC X.                                          00001320
           02 MDATJOUC  PIC X.                                          00001330
           02 MDATJOUP  PIC X.                                          00001340
           02 MDATJOUH  PIC X.                                          00001350
           02 MDATJOUV  PIC X.                                          00001360
           02 MDATJOUO  PIC X(10).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTIMJOUA  PIC X.                                          00001390
           02 MTIMJOUC  PIC X.                                          00001400
           02 MTIMJOUP  PIC X.                                          00001410
           02 MTIMJOUH  PIC X.                                          00001420
           02 MTIMJOUV  PIC X.                                          00001430
           02 MTIMJOUO  PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MNSOCA    PIC X.                                          00001460
           02 MNSOCC    PIC X.                                          00001470
           02 MNSOCP    PIC X.                                          00001480
           02 MNSOCH    PIC X.                                          00001490
           02 MNSOCV    PIC X.                                          00001500
           02 MNSOCO    PIC X(3).                                       00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MLIBSOCA  PIC X.                                          00001530
           02 MLIBSOCC  PIC X.                                          00001540
           02 MLIBSOCP  PIC X.                                          00001550
           02 MLIBSOCH  PIC X.                                          00001560
           02 MLIBSOCV  PIC X.                                          00001570
           02 MLIBSOCO  PIC X(20).                                      00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNLIEUA   PIC X.                                          00001600
           02 MNLIEUC   PIC X.                                          00001610
           02 MNLIEUP   PIC X.                                          00001620
           02 MNLIEUH   PIC X.                                          00001630
           02 MNLIEUV   PIC X.                                          00001640
           02 MNLIEUO   PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MLIBLIEUA      PIC X.                                     00001670
           02 MLIBLIEUC PIC X.                                          00001680
           02 MLIBLIEUP PIC X.                                          00001690
           02 MLIBLIEUH PIC X.                                          00001700
           02 MLIBLIEUV PIC X.                                          00001710
           02 MLIBLIEUO      PIC X(20).                                 00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MLMAGEDA  PIC X.                                          00001740
           02 MLMAGEDC  PIC X.                                          00001750
           02 MLMAGEDP  PIC X.                                          00001760
           02 MLMAGEDH  PIC X.                                          00001770
           02 MLMAGEDV  PIC X.                                          00001780
           02 MLMAGEDO  PIC X(15).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MLORDREA  PIC X.                                          00001810
           02 MLORDREC  PIC X.                                          00001820
           02 MLORDREP  PIC X.                                          00001830
           02 MLORDREH  PIC X.                                          00001840
           02 MLORDREV  PIC X.                                          00001850
           02 MLORDREO  PIC X(15).                                      00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MWMAGPA   PIC X.                                          00001880
           02 MWMAGPC   PIC X.                                          00001890
           02 MWMAGPP   PIC X.                                          00001900
           02 MWMAGPH   PIC X.                                          00001910
           02 MWMAGPV   PIC X.                                          00001920
           02 MWMAGPO   PIC X.                                          00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MWNOBCDA  PIC X.                                          00001950
           02 MWNOBCDC  PIC X.                                          00001960
           02 MWNOBCDP  PIC X.                                          00001970
           02 MWNOBCDH  PIC X.                                          00001980
           02 MWNOBCDV  PIC X.                                          00001990
           02 MWNOBCDO  PIC X.                                          00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MWNOPSEA  PIC X.                                          00002020
           02 MWNOPSEC  PIC X.                                          00002030
           02 MWNOPSEP  PIC X.                                          00002040
           02 MWNOPSEH  PIC X.                                          00002050
           02 MWNOPSEV  PIC X.                                          00002060
           02 MWNOPSEO  PIC X.                                          00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MWDEROGA  PIC X.                                          00002090
           02 MWDEROGC  PIC X.                                          00002100
           02 MWDEROGP  PIC X.                                          00002110
           02 MWDEROGH  PIC X.                                          00002120
           02 MWDEROGV  PIC X.                                          00002130
           02 MWDEROGO  PIC X.                                          00002140
           02 FILLER    PIC X(2).                                       00002150
           02 MWMDGRDA  PIC X.                                          00002160
           02 MWMDGRDC  PIC X.                                          00002170
           02 MWMDGRDP  PIC X.                                          00002180
           02 MWMDGRDH  PIC X.                                          00002190
           02 MWMDGRDV  PIC X.                                          00002200
           02 MWMDGRDO  PIC X.                                          00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MWAUTMGA  PIC X.                                          00002230
           02 MWAUTMGC  PIC X.                                          00002240
           02 MWAUTMGP  PIC X.                                          00002250
           02 MWAUTMGH  PIC X.                                          00002260
           02 MWAUTMGV  PIC X.                                          00002270
           02 MWAUTMGO  PIC X.                                          00002280
           02 FILLER    PIC X(2).                                       00002290
           02 MCVENDFA  PIC X.                                          00002300
           02 MCVENDFC  PIC X.                                          00002310
           02 MCVENDFP  PIC X.                                          00002320
           02 MCVENDFH  PIC X.                                          00002330
           02 MCVENDFV  PIC X.                                          00002340
           02 MCVENDFO  PIC X(7).                                       00002350
           02 FILLER    PIC X(2).                                       00002360
           02 MCOPERFA  PIC X.                                          00002370
           02 MCOPERFC  PIC X.                                          00002380
           02 MCOPERFP  PIC X.                                          00002390
           02 MCOPERFH  PIC X.                                          00002400
           02 MCOPERFV  PIC X.                                          00002410
           02 MCOPERFO  PIC X(7).                                       00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MWSTOCKA  PIC X.                                          00002440
           02 MWSTOCKC  PIC X.                                          00002450
           02 MWSTOCKP  PIC X.                                          00002460
           02 MWSTOCKH  PIC X.                                          00002470
           02 MWSTOCKV  PIC X.                                          00002480
           02 MWSTOCKO  PIC X.                                          00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MCVENLSA  PIC X.                                          00002510
           02 MCVENLSC  PIC X.                                          00002520
           02 MCVENLSP  PIC X.                                          00002530
           02 MCVENLSH  PIC X.                                          00002540
           02 MCVENLSV  PIC X.                                          00002550
           02 MCVENLSO  PIC X(7).                                       00002560
           02 FILLER    PIC X(2).                                       00002570
           02 MCVSECRA  PIC X.                                          00002580
           02 MCVSECRC  PIC X.                                          00002590
           02 MCVSECRP  PIC X.                                          00002600
           02 MCVSECRH  PIC X.                                          00002610
           02 MCVSECRV  PIC X.                                          00002620
           02 MCVSECRO  PIC X(7).                                       00002630
           02 FILLER    PIC X(2).                                       00002640
           02 MCTOMAGA  PIC X.                                          00002650
           02 MCTOMAGC  PIC X.                                          00002660
           02 MCTOMAGP  PIC X.                                          00002670
           02 MCTOMAGH  PIC X.                                          00002680
           02 MCTOMAGV  PIC X.                                          00002690
           02 MCTOMAGO  PIC X.                                          00002700
           02 FILLER    PIC X(2).                                       00002710
           02 MCSSAVA   PIC X.                                          00002720
           02 MCSSAVC   PIC X.                                          00002730
           02 MCSSAVP   PIC X.                                          00002740
           02 MCSSAVH   PIC X.                                          00002750
           02 MCSSAVV   PIC X.                                          00002760
           02 MCSSAVO   PIC X(3).                                       00002770
           02 FILLER    PIC X(2).                                       00002780
           02 MCLSAVA   PIC X.                                          00002790
           02 MCLSAVC   PIC X.                                          00002800
           02 MCLSAVP   PIC X.                                          00002810
           02 MCLSAVH   PIC X.                                          00002820
           02 MCLSAVV   PIC X.                                          00002830
           02 MCLSAVO   PIC X(3).                                       00002840
           02 FILLER    PIC X(2).                                       00002850
           02 MCSDACMA  PIC X.                                          00002860
           02 MCSDACMC  PIC X.                                          00002870
           02 MCSDACMP  PIC X.                                          00002880
           02 MCSDACMH  PIC X.                                          00002890
           02 MCSDACMV  PIC X.                                          00002900
           02 MCSDACMO  PIC X(3).                                       00002910
           02 FILLER    PIC X(2).                                       00002920
           02 MCLDACMA  PIC X.                                          00002930
           02 MCLDACMC  PIC X.                                          00002940
           02 MCLDACMP  PIC X.                                          00002950
           02 MCLDACMH  PIC X.                                          00002960
           02 MCLDACMV  PIC X.                                          00002970
           02 MCLDACMO  PIC X(3).                                       00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MDSEM01A  PIC X.                                          00003000
           02 MDSEM01C  PIC X.                                          00003010
           02 MDSEM01P  PIC X.                                          00003020
           02 MDSEM01H  PIC X.                                          00003030
           02 MDSEM01V  PIC X.                                          00003040
           02 MDSEM01O  PIC X(8).                                       00003050
           02 FILLER    PIC X(2).                                       00003060
           02 MLIBERRA  PIC X.                                          00003070
           02 MLIBERRC  PIC X.                                          00003080
           02 MLIBERRP  PIC X.                                          00003090
           02 MLIBERRH  PIC X.                                          00003100
           02 MLIBERRV  PIC X.                                          00003110
           02 MLIBERRO  PIC X(78).                                      00003120
           02 FILLER    PIC X(2).                                       00003130
           02 MCODTRAA  PIC X.                                          00003140
           02 MCODTRAC  PIC X.                                          00003150
           02 MCODTRAP  PIC X.                                          00003160
           02 MCODTRAH  PIC X.                                          00003170
           02 MCODTRAV  PIC X.                                          00003180
           02 MCODTRAO  PIC X(4).                                       00003190
           02 FILLER    PIC X(2).                                       00003200
           02 MCICSA    PIC X.                                          00003210
           02 MCICSC    PIC X.                                          00003220
           02 MCICSP    PIC X.                                          00003230
           02 MCICSH    PIC X.                                          00003240
           02 MCICSV    PIC X.                                          00003250
           02 MCICSO    PIC X(5).                                       00003260
           02 FILLER    PIC X(2).                                       00003270
           02 MNETNAMA  PIC X.                                          00003280
           02 MNETNAMC  PIC X.                                          00003290
           02 MNETNAMP  PIC X.                                          00003300
           02 MNETNAMH  PIC X.                                          00003310
           02 MNETNAMV  PIC X.                                          00003320
           02 MNETNAMO  PIC X(8).                                       00003330
           02 FILLER    PIC X(2).                                       00003340
           02 MSCREENA  PIC X.                                          00003350
           02 MSCREENC  PIC X.                                          00003360
           02 MSCREENP  PIC X.                                          00003370
           02 MSCREENH  PIC X.                                          00003380
           02 MSCREENV  PIC X.                                          00003390
           02 MSCREENO  PIC X(5).                                       00003400
                                                                                
