      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBA100 AU 14/03/2008  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,06,BI,A,                          *        
      *                           16,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IBA100.                                                        
            05 NOMETAT-IBA100           PIC X(6) VALUE 'IBA100'.                
            05 RUPTURES-IBA100.                                                 
           10 IBA100-NSOC               PIC X(03).                      007  003
           10 IBA100-NBA                PIC X(06).                      010  006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IBA100-SEQUENCE           PIC S9(04) COMP.                016  002
      *--                                                                       
           10 IBA100-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IBA100.                                                   
           10 IBA100-CTIERSBA           PIC X(06).                      018  006
           10 IBA100-NFACTBA            PIC X(10).                      024  010
           10 IBA100-NFACTREM           PIC X(10).                      034  010
           10 IBA100-NTIERSSAP          PIC X(10).                      044  010
           10 IBA100-PBA                PIC S9(05)V9(2) COMP-3.         054  004
           10 IBA100-DEMIS              PIC X(08).                      058  008
           10 IBA100-DVALIDITE          PIC X(08).                      066  008
            05 FILLER                      PIC X(439).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IBA100-LONG           PIC S9(4)   COMP  VALUE +073.           
      *                                                                         
      *--                                                                       
        01  DSECT-IBA100-LONG           PIC S9(4) COMP-5  VALUE +073.           
                                                                                
      *}                                                                        
