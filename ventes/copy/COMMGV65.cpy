      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TRANSACTION: GV00                                              *        
      * TITRE      : COMMAREA D'APPEL MGV65, MODULE INTERROGATION      *        
      *              DES STOCKS DARTY ET DACEM POUR L'EXPEDIION (EMX)  *        
      * LONGUEUR   : ????                                              *        
      *                                                                *        
L20410*              COMMANDE FOURNISSEUR EXPEDIE                      *        
LD0610*              CHRONOPOST                                        *        
V34   * COMMANDES INCOMPLETES : MODE CONTROLE SANS MESSAGES            *        
      * -------------------------------------------------------------- *        
      * GESTION DES CONTREMARQUES DACEM                                *        
      * -------------------------------------------------------------- *        
        01 COMM-GV65-APPLI.                                                     
           05 COMM-GV65-ZIN.                                                    
               10 COMM-GV65-CAPPEL           PIC X(01).                         
      *{ remove-comma-in-dde 1.5                                                
      *           88 COMM-GV65-CONTROLE      VALUE 'D', 'M', '4', 'C'.          
      *--                                                                       
                  88 COMM-GV65-CONTROLE      VALUE 'D'  'M'  '4'  'C'.          
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *           88 COMM-GV65-RESERVATION   VALUE 'R', 'I' , 'X' , '3'.        
      *--                                                                       
                  88 COMM-GV65-RESERVATION   VALUE 'R'  'I'   'X'   '3'.        
      *}                                                                        
                  88 COMM-GV65-ANNULATION    VALUE 'A'.                         
      *{ remove-comma-in-dde 1.5                                                
V34   *           88 COMM-GV65-VALIDATION    VALUE 'E', 'F', '2'.               
      *--                                                                       
                  88 COMM-GV65-VALIDATION    VALUE 'E'  'F'  '2'.               
      *}                                                                        
                  88 COMM-GV65-CTRL-SPE      VALUE 'C'.                         
                  88 COMM-GV65-VTE-INTERNET  VALUE 'I'.                         
                  88 COMM-GV65-VTE-REDBOX    VALUE 'X'.                         
V34               88 COMM-GV65-CTRL-MGV64    VALUE '4'.                         
V34               88 COMM-GV65-SUPPRESSION   VALUE 'S'.                         
V34               88 COMM-GV65-MAJ-DACEM     VALUE 'B'.                         
V34               88 COMM-GV65-RESA-DACEM    VALUE 'G'.                         
V34               88 COMM-GV65-RESA-GR32     VALUE '3'.                         
V34               88 COMM-GV65-VALID-GR32    VALUE '2'.                         
V34               88 COMM-GV65-VALID-EC02    VALUE 'F'.                         
                  88 COMM-GV65-NEW-APPEL     VALUE ' '.                         
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-GV65-NSOCIETE         PIC X(03).                         
               10 COMM-GV65-NLIEU            PIC X(03).                         
               10 COMM-GV65-NVENTE           PIC X(07).                         
               10 COMM-GV65-NCODIC           PIC X(07).                         
               10 COMM-GV65-NSEQNQ           PIC S9(5) COMP-3.                  
               10 COMM-GV65-WCQERESF         PIC X(01).                         
                  88 COMM-GV65-CMD-FNR                 VALUE 'F'.               
               10 COMM-GV65-DDELIV           PIC X(08).                         
               10 COMM-GV65-SSAAMMJJ         PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-GV65-QTE              PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-GV65-QTE              PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
      * -> RESERVATION DACEM OU DARTY                                           
               10 COMM-GV65-TYPCDE           PIC X(05).                         
                  88 COMM-GV65-CDEDARTY                 VALUE 'DARTY'.          
                  88 COMM-GV65-CDEDACEM                 VALUE 'DACEM'.          
      * -> PARAM�TRES SP�CIAUX                                                  
HV             10 COMM-GV65-TYPMUT          PIC X(01).                          
090306            88 COMM-GV65-MUT-UNIQUE               VALUE 'U'.              
090306            88 COMM-GV65-MUT-MULTI                VALUE 'M'.              
LD0610*        10 FILLER                    PIC X(100).                         
LD0610         10 COMM-GV65-WEMPORTE        PIC X(01).                          
               10 COMM-GV65-ETAT-PREC-VTE   PIC X(01).                          
                  88 GV65-STATUT-PREC-COMPLETE      VALUE 'N'.                  
                  88 GV65-STATUT-PREC-INCOMPLETE    VALUE 'O'.                  
                  88 GV65-STATUT-PREC-NON-DEFINI    VALUE ' '.                  
               10 COMM-GV65-NEW-MODE.                                           
                  15 COMM-GV65-ACT-DACEM      PIC X(01).                        
                     88 COMM-GV65-CREATION-DACEM   VALUE 'C'.                   
                     88 COMM-GV65-MODIF-DACEM      VALUE 'M'.                   
                     88 COMM-GV65-SUP-DACEM        VALUE 'S'.                   
                  15 COMM-GV65-MAJ-STOCK      PIC X(01).                        
                  15 COMM-GV65-SUP-MUT-GB05   PIC X(01).                        
                  15 COMM-GV65-SUP-MUT-GB15   PIC X(01).                        
                  15 COMM-GV65-CRE-GB05-TEMPO PIC X(01).                        
                  15 COMM-GV65-CRE-GB05-FINAL PIC X(01).                        
                  15 COMM-GV65-CRE-GB15-FINAL PIC X(01).                        
                  15 COMM-GV65-INTERNET       PIC X(01).                        
               10 COMM-GV65-AUTORISATION    PIC X(01).                          
               10 COMM-GV65-RESA-A-J        PIC X(01).                          
               10 COMM-GV65-NORDRE          PIC X(05).                          
               10 COMM-GV65-MULTIDA         PIC X(01).                          
               10 COMM-GV65-DMUT            PIC X(08).                          
               10 COMM-GV65-HMUT            PIC X(04).                          
               10 COMM-GV65-NMUT            PIC X(07).                          
               10 COMM-GV65-TENTATIVES      PIC 9(02).                          
               10 COMM-GV65-FILIERE-IN      PIC X(02).                          
      *        10 COMM-GV65-090-OK          PIC X(01).                          
      *        10 COMM-GV65-095-OK          PIC X(01).                          
      *        10 COMM-GV65-QTE-CUM-095     PIC 9(03).                          
      *        10 COMM-GV65-QTE-CUM-090     PIC 9(03).                          
      *        10 COMM-GV65-NUM-MUT-090     PIC 9(07).                          
      *        10 COMM-GV65-NUM-MUT-095     PIC 9(07).                          
      *        10 COMM-GV65-DATE-GB07       PIC X(08).                          
               10 COMM-GV65-SUP-FILIERE     PIC X(02).                          
               10 COMM-GV65-MUT-SPE         PIC X(01).                          
               10 COMM-GV65-NCDEWC          PIC S9(15) PACKED-DECIMAL.          
               10 COMM-GA00-WDACEM          PIC X(01).                          
                 88 GV65-PRODUIT-DACEM           VALUE 'O'.                     
                 88 GV65-PRODUIT-DARTY           VALUE 'N'.                     
               10 COMM-GA00-CFAM            PIC X(05).                          
               10 COMM-GA00-CMARQ           PIC X(05).                          
               10 COMM-GA00-CAPPRO          PIC X(05).                          
               10 COMM-GA00-QHAUTEUR        PIC S9(3) COMP-3.                   
               10 COMM-GA00-QPROFONDEUR     PIC S9(3) COMP-3.                   
               10 COMM-GA00-QLARGEUR        PIC S9(3) COMP-3.                   
               10 COMM-GA00-QPOIDS          PIC S9(7) COMP-3.                   
               10 FILLER                    PIC X(22).                          
      *        10 FILLER                    PIC X(48).                          
      *        10 FILLER                    PIC X(88).                          
LD0610*        10 FILLER                    PIC X(99).                          
      * ->                                                                      
           05 COMM-GV65-ZOUT.                                                   
               10 COMM-GV65-CODRET           PIC X(04).                         
               10 COMM-GV65-LMESSAGE         PIC X(53).                         
               10 COMM-GV65-NMUTATION        PIC X(07).                         
               10 COMM-GV65-NMUTTEMP         PIC X(07).                         
               10 COMM-GV65-LIEU-STOCK.                                         
                  15 COMM-GV65-NSOCDEPOT     PIC X(03).                         
                  15 COMM-GV65-NDEPOT        PIC X(03).                         
               10 COMM-GV65-NCODIC-REMPL     PIC X(07).                         
               10 COMM-GV25-WDACEM           PIC 9(01) VALUE 0.                 
                  88 COMM-GV65-DACEM-NON               VALUE 0.                 
                  88 COMM-GV65-DACEM-OUI               VALUE 1.                 
               10 COMM-GV65-ETAT-RESERVATION PIC 9(01).                         
                  88 COMM-GV65-AUCUN-STOCK-DISPO       VALUE 0.                 
                  88 COMM-GV65-STOCK-DISPONIBLE        VALUE 1.                 
                  88 COMM-GV65-STOCK-DACEM             VALUE 2.                 
L20410         10 COMM-GV65-ETAT-FOURNISSEUR PIC 9(01).                         
L20410            88 COMM-GV65-STOCK-FOURN-NON         VALUE 0.                 
L20410            88 COMM-GV65-STOCK-FOURN-OUI         VALUE 1.                 
               10 COMM-GV65-CSTATUT          PIC X(01).                         
               10 COMM-GV65-ARTICLE-CQE      PIC X(01).                         
               10 COMM-GV65-DATE-DISPO       PIC X(08).                         
               10 COMM-GV65-PRECO            PIC 9(01).                         
                  88 GV65-PAS-COMMANDE-PRECO      VALUE 0.                      
                  88 GV65-COMMANDE-PRECO          VALUE 1.                      
               10 COMM-GV65-DMUTATION        PIC X(08).                         
               10 COMM-GV65-HMUTATION        PIC X(04).                         
               10 COMM-GV65-FILIERE          PIC X(04).                         
               10 FILLER                     PIC X(073).                        
      *        10 FILLER                     PIC X(099).                        
                                                                                
