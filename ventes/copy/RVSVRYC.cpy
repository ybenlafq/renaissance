      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SVRYC SVRYC RAYON CIBLE                *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVRYC.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVRYC.                                                             
      *}                                                                        
           05  SVRYC-CTABLEG2    PIC X(15).                                     
           05  SVRYC-CTABLEG2-REDEF REDEFINES SVRYC-CTABLEG2.                   
               10  SVRYC-APPLICAT        PIC X(08).                             
               10  SVRYC-CRAYCIB         PIC X(05).                             
           05  SVRYC-WTABLEG     PIC X(80).                                     
           05  SVRYC-WTABLEG-REDEF  REDEFINES SVRYC-WTABLEG.                    
               10  SVRYC-LRAYCIB         PIC X(30).                             
               10  SVRYC-CTRAYCIB        PIC X(01).                             
               10  SVRYC-COMMENT         PIC X(10).                             
               10  SVRYC-COMMENT1        PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSVRYC-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSVRYC-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVRYC-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SVRYC-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SVRYC-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SVRYC-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
