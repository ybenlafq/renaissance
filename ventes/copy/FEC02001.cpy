      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * DARTY.COM : EXTRACTION DES LIGNES D'ENCAISSEMENT DES VENTES EN  00000020
      *             ECART DE CAISSE                                             
      ****************************************************************  00000030
      *                                                                 00000030
      * 12/10/10 : CREATION                                             00000030
      *                                                                 00000030
      ****************************************************************  00000030
       01     FEC02001-FIC-EXTRACT.                                             
 A         05 FEC02001-CODEACTION  PIC X(12).                                   
           05 W-SEP1               PIC X(01) VALUE ';'.                         
 A         05 FEC02001-TYPECLE     PIC X(08).                                   
           05 W-SEP2               PIC X(01) VALUE ';'.                         
 A         05 FEC02001-VALCLE      PIC X(13).                                   
           05 W-SEP3               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-CODIC1      PIC X(07).                                   
           05 W-SEP4               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-DADIAG      PIC X(10).                                   
           05 W-SEP5               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-DARESO      PIC X(10).                                   
           05 W-SEP6               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-DARECA      PIC X(10).                                   
           05 W-SEP7               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-DADTOUV     PIC X(10).                                   
           05 W-SEP8               PIC X(01) VALUE ';'.                         
      *    05 FEC02001-DADTFER     PIC X(10).                                   
           05 W-SEP9               PIC X(01) VALUE ';'.                         
 A         05 FEC02001-VALLIB1     PIC X(15).                                   
           05 W-SEP10              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-VALLIB2     PIC X(15).                                   
           05 W-SEP11              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-VALLIB3     PIC X(15).                                   
           05 W-SEP12              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-VALLIB4     PIC X(15).                                   
           05 W-SEP13              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-VALLIB5     PIC X(15).                                   
           05 W-SEP14              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDESC     PIC X(20).                                   
           05 W-SEP15              PIC X(01) VALUE ';'.                         
 A         05 FEC02001-ACTCOMM1    PIC X(25).                                   
           05 W-SEP16              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTDEB1   PIC X(12).                                   
           05 W-SEP17              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTFIN1   PIC X(12).                                   
           05 W-SEP18              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTTYPPJ1   PIC X(15).                                   
           05 W-SEP19              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALPJ1   PIC X(15).                                   
           05 W-SEP20              PIC X(01) VALUE ';'.                         
 A         05 FEC02001-ACTVALL11   PIC X(07).                                   
           05 W-SEP21              PIC X(01) VALUE ';'.                         
 A         05 FEC02001-ACTVALL21   PIC X(13).                                   
           05 W-SEP22              PIC X(01) VALUE ';'.                         
 A         05 FEC02001-ACTVALL31   PIC X(10).                                   
           05 W-SEP23              PIC X(01) VALUE ';'.                         
 A         05 FEC02001-ACTVALL41   PIC X(08).                                   
           05 W-SEP24              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL51   PIC X(16).                                   
           05 W-SEP25              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDESC2    PIC X(15).                                   
           05 W-SEP26              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTCOMM2    PIC X(15).                                   
           05 W-SEP27              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTDEB2   PIC X(12).                                   
           05 W-SEP28              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTFIN2   PIC X(12).                                   
           05 W-SEP29              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTTYPPJ2   PIC X(15).                                   
           05 W-SEP30              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALPJ2   PIC X(15).                                   
           05 W-SEP31              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL12   PIC X(16).                                   
           05 W-SEP32              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL22   PIC X(16).                                   
           05 W-SEP33              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL32   PIC X(16).                                   
           05 W-SEP34              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL42   PIC X(16).                                   
           05 W-SEP35              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL52   PIC X(16).                                   
           05 W-SEP36              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDESC3    PIC X(15).                                   
           05 W-SEP37              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTCOMM3    PIC X(15).                                   
           05 W-SEP38              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTDEB3   PIC X(12).                                   
           05 W-SEP39              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTFIN3   PIC X(12).                                   
           05 W-SEP40              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTTYPPJ3   PIC X(15).                                   
           05 W-SEP41              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALPJ3   PIC X(15).                                   
           05 W-SEP42              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL13   PIC X(16).                                   
           05 W-SEP43              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL23   PIC X(16).                                   
           05 W-SEP44              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL33   PIC X(16).                                   
           05 W-SEP45              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL43   PIC X(16).                                   
           05 W-SEP46              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL53   PIC X(16).                                   
           05 W-SEP47              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDESC4    PIC X(15).                                   
           05 W-SEP48              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTCOMM4    PIC X(15).                                   
           05 W-SEP49              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTDEB4   PIC X(12).                                   
           05 W-SEP50              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTFIN4   PIC X(12).                                   
           05 W-SEP51              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTTYPPJ4   PIC X(15).                                   
           05 W-SEP52              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALPJ4   PIC X(15).                                   
           05 W-SEP53              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL14   PIC X(16).                                   
           05 W-SEP54              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL24   PIC X(16).                                   
           05 W-SEP56              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL34   PIC X(16).                                   
           05 W-SEP57              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL44   PIC X(16).                                   
           05 W-SEP58              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL54   PIC X(16).                                   
           05 W-SEP59              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDESC5    PIC X(15).                                   
           05 W-SEP60              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTCOMM5    PIC X(15).                                   
           05 W-SEP61              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTDEB5   PIC X(12).                                   
           05 W-SEP62              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTDTFIN5   PIC X(12).                                   
           05 W-SEP63              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTTYPPJ5   PIC X(15).                                   
           05 W-SEP64              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALPJ5   PIC X(15).                                   
           05 W-SEP65              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL15   PIC X(16).                                   
           05 W-SEP66              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL25   PIC X(16).                                   
           05 W-SEP67              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL35   PIC X(16).                                   
           05 W-SEP68              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL45   PIC X(16).                                   
           05 W-SEP69              PIC X(01) VALUE ';'.                         
      *    05 FEC02001-ACTVALL55   PIC X(16).                                   
           05 W-SEP70              PIC X(01) VALUE ';'.                         
           05 W-FILLER             PIC X(778) VALUE SPACES.                     
                                                                                
