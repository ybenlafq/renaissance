      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDAMA ANNULATION ANOMALIE              *        
      *----------------------------------------------------------------*        
       01  RVTDAMA.                                                             
           05  TDAMA-CTABLEG2    PIC X(15).                                     
           05  TDAMA-CTABLEG2-REDEF REDEFINES TDAMA-CTABLEG2.                   
               10  TDAMA-CODANO          PIC X(05).                             
               10  TDAMA-PROV            PIC X(05).                             
           05  TDAMA-WTABLEG     PIC X(80).                                     
           05  TDAMA-WTABLEG-REDEF  REDEFINES TDAMA-WTABLEG.                    
               10  TDAMA-DATDEB          PIC X(08).                             
               10  TDAMA-DATDEB-N       REDEFINES TDAMA-DATDEB                  
                                         PIC 9(08).                             
               10  TDAMA-DATFIN          PIC X(08).                             
               10  TDAMA-DATFIN-N       REDEFINES TDAMA-DATFIN                  
                                         PIC 9(08).                             
               10  TDAMA-ACTIF           PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDAMA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDAMA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDAMA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDAMA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDAMA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
