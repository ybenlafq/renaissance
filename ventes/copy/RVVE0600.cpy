      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVVE0600                                     00020006
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVE0600                 00060006
      *---------------------------------------------------------        00070000
      *                                                                 00080000
       01  RVVE0600.                                                    00090006
           02  VE06-NSOCIETE                                            00100005
               PIC X(0003).                                             00110004
           02  VE06-NLIEU                                               00120005
               PIC X(0003).                                             00130004
           02  VE06-NVENTE                                              00140005
               PIC X(0007).                                             00150004
           02  VE06-NDOSSIER                                            00160005
               PIC S9(3) COMP-3.                                        00170007
           02  VE06-NSEQNQ                                              00180005
               PIC S9(5) COMP-3.                                        00190001
           02  VE06-NSEQ                                                00200005
               PIC S9(3) COMP-3.                                        00210001
           02  VE06-DSYST                                               00220005
               PIC S9(13) COMP-3.                                       00230004
      *                                                                 00240000
      *---------------------------------------------------------        00250000
      *   LISTE DES FLAGS DE LA TABLE RVVE0600                          00260006
      *---------------------------------------------------------        00270000
      *                                                                 00280000
       01  RVVE0600-FLAGS.                                              00290006
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NSOCIETE-F                                          00300005
      *        PIC S9(4) COMP.                                          00310000
      *--                                                                       
           02  VE06-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NLIEU-F                                             00320005
      *        PIC S9(4) COMP.                                          00330000
      *--                                                                       
           02  VE06-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NVENTE-F                                            00340005
      *        PIC S9(4) COMP.                                          00350000
      *--                                                                       
           02  VE06-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NDOSSIER-F                                          00360005
      *        PIC S9(4) COMP.                                          00370000
      *--                                                                       
           02  VE06-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NSEQNQ-F                                            00380005
      *        PIC S9(4) COMP.                                          00390000
      *--                                                                       
           02  VE06-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-NSEQ-F                                              00400005
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  VE06-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE06-DSYST-F                                             00420005
      *        PIC S9(4) COMP.                                          00430004
      *--                                                                       
           02  VE06-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00440000
                                                                                
