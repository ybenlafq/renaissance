      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGV3500                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV3500                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVGV3500.                                                            
           02  GV35-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV35-NLIEUORIG                                                   
               PIC X(0003).                                                     
           02  GV35-NSSLIEUORIG                                                 
               PIC X(0003).                                                     
           02  GV35-NLIEUVENTE                                                  
               PIC X(0003).                                                     
           02  GV35-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV35-NBONENLV                                                    
               PIC X(0007).                                                     
           02  GV35-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV35-NLIEUDEST                                                   
               PIC X(0003).                                                     
           02  GV35-NSSLIEUDEST                                                 
               PIC X(0003).                                                     
           02  GV35-LNOM                                                        
               PIC X(0025).                                                     
           02  GV35-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV35-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV35-DDLIV                                                       
               PIC X(0008).                                                     
           02  GV35-DANNUL                                                      
               PIC X(0008).                                                     
           02  GV35-DRECEPT                                                     
               PIC X(0008).                                                     
           02  GV35-DTOPE                                                       
               PIC X(0008).                                                     
           02  GV35-DREGUL                                                      
               PIC X(0008).                                                     
           02  GV35-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV35-DCREATVENTE                                                 
               PIC X(0008).                                                     
           02  GV35-DANNULVENTE                                                 
               PIC X(0008).                                                     
           02  GV35-NDOCENLV                                                    
               PIC X(0007).                                                     
           02  GV35-WEDITE                                                      
               PIC X(0001).                                                     
           02  GV35-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV3500                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGV3500-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NLIEUORIG-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NLIEUORIG-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NSSLIEUORIG-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NSSLIEUORIG-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NLIEUVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NLIEUVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NBONENLV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NBONENLV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NLIEUDEST-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NLIEUDEST-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NSSLIEUDEST-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NSSLIEUDEST-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DDLIV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DDLIV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DRECEPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DRECEPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DREGUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DREGUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DCREATVENTE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DCREATVENTE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DANNULVENTE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DANNULVENTE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-NDOCENLV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-NDOCENLV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-WEDITE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-WEDITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV35-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV35-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
