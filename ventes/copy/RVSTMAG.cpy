      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE STMAG STMAG                            *        
      *----------------------------------------------------------------*        
       01  RVSTMAG.                                                             
           05  STMAG-CTABLEG2    PIC X(15).                                     
           05  STMAG-CTABLEG2-REDEF REDEFINES STMAG-CTABLEG2.                   
               10  STMAG-SOC             PIC X(03).                             
               10  STMAG-MAG             PIC X(03).                             
           05  STMAG-WTABLEG     PIC X(80).                                     
           05  STMAG-WTABLEG-REDEF  REDEFINES STMAG-WTABLEG.                    
               10  STMAG-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSTMAG-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  STMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  STMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  STMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  STMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
