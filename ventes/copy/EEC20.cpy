      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Contr�le Commandes WebSph�re 1/2                                00000020
      ***************************************************************** 00000030
       01   EEC20I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNCODICI  PIC X(7).                                       00000170
           02 MCEXPOD OCCURS   18 TIMES .                               00000180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCEXPOL      COMP PIC S9(4).                            00000190
      *--                                                                       
             03 MCEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCEXPOF      PIC X.                                     00000200
             03 FILLER  PIC X(4).                                       00000210
             03 MCEXPOI      PIC X.                                     00000220
           02 MWCCD OCCURS   18 TIMES .                                 00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWCCL   COMP PIC S9(4).                                 00000240
      *--                                                                       
             03 MWCCL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MWCCF   PIC X.                                          00000250
             03 FILLER  PIC X(4).                                       00000260
             03 MWCCI   PIC X.                                          00000270
           02 MLEXPOD OCCURS   18 TIMES .                               00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLEXPOL      COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MLEXPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLEXPOF      PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MLEXPOI      PIC X(10).                                 00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MCFAMI    PIC X(5).                                       00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCRAYONL  COMP PIC S9(4).                                 00000370
      *--                                                                       
           02 MCRAYONL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCRAYONF  PIC X.                                          00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MCRAYONI  PIC X(5).                                       00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSARTL    COMP PIC S9(4).                                 00000410
      *--                                                                       
           02 MSARTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSARTF    PIC X.                                          00000420
           02 FILLER    PIC X(4).                                       00000430
           02 MSARTI    PIC X(3).                                       00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSMAGL    COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MSMAGL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MSMAGF    PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MSMAGI    PIC X(3).                                       00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSDEPOTL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MSDEPOTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSDEPOTF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MSDEPOTI  PIC X(3).                                       00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MLIBERRI  PIC X(79).                                      00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000570
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000580
           02 FILLER    PIC X(4).                                       00000590
           02 MCODTRAI  PIC X(4).                                       00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MZONCMDI  PIC X(15).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MCICSI    PIC X(5).                                       00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MNETNAMI  PIC X(8).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MSCREENI  PIC X(4).                                       00000760
      ***************************************************************** 00000770
      * Contr�le Commandes WebSph�re 1/2                                00000780
      ***************************************************************** 00000790
       01   EEC20O REDEFINES EEC20I.                                    00000800
           02 FILLER    PIC X(12).                                      00000810
           02 FILLER    PIC X(2).                                       00000820
           02 MDATJOUA  PIC X.                                          00000830
           02 MDATJOUC  PIC X.                                          00000840
           02 MDATJOUP  PIC X.                                          00000850
           02 MDATJOUH  PIC X.                                          00000860
           02 MDATJOUV  PIC X.                                          00000870
           02 MDATJOUO  PIC X(10).                                      00000880
           02 FILLER    PIC X(2).                                       00000890
           02 MTIMJOUA  PIC X.                                          00000900
           02 MTIMJOUC  PIC X.                                          00000910
           02 MTIMJOUP  PIC X.                                          00000920
           02 MTIMJOUH  PIC X.                                          00000930
           02 MTIMJOUV  PIC X.                                          00000940
           02 MTIMJOUO  PIC X(5).                                       00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MNCODICA  PIC X.                                          00000970
           02 MNCODICC  PIC X.                                          00000980
           02 MNCODICP  PIC X.                                          00000990
           02 MNCODICH  PIC X.                                          00001000
           02 MNCODICV  PIC X.                                          00001010
           02 MNCODICO  PIC X(7).                                       00001020
           02 DFHMS1 OCCURS   18 TIMES .                                00001030
             03 FILLER       PIC X(2).                                  00001040
             03 MCEXPOA      PIC X.                                     00001050
             03 MCEXPOC PIC X.                                          00001060
             03 MCEXPOP PIC X.                                          00001070
             03 MCEXPOH PIC X.                                          00001080
             03 MCEXPOV PIC X.                                          00001090
             03 MCEXPOO      PIC X.                                     00001100
           02 DFHMS2 OCCURS   18 TIMES .                                00001110
             03 FILLER       PIC X(2).                                  00001120
             03 MWCCA   PIC X.                                          00001130
             03 MWCCC   PIC X.                                          00001140
             03 MWCCP   PIC X.                                          00001150
             03 MWCCH   PIC X.                                          00001160
             03 MWCCV   PIC X.                                          00001170
             03 MWCCO   PIC X.                                          00001180
           02 DFHMS3 OCCURS   18 TIMES .                                00001190
             03 FILLER       PIC X(2).                                  00001200
             03 MLEXPOA      PIC X.                                     00001210
             03 MLEXPOC PIC X.                                          00001220
             03 MLEXPOP PIC X.                                          00001230
             03 MLEXPOH PIC X.                                          00001240
             03 MLEXPOV PIC X.                                          00001250
             03 MLEXPOO      PIC X(10).                                 00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MCFAMA    PIC X.                                          00001280
           02 MCFAMC    PIC X.                                          00001290
           02 MCFAMP    PIC X.                                          00001300
           02 MCFAMH    PIC X.                                          00001310
           02 MCFAMV    PIC X.                                          00001320
           02 MCFAMO    PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MCRAYONA  PIC X.                                          00001350
           02 MCRAYONC  PIC X.                                          00001360
           02 MCRAYONP  PIC X.                                          00001370
           02 MCRAYONH  PIC X.                                          00001380
           02 MCRAYONV  PIC X.                                          00001390
           02 MCRAYONO  PIC X(5).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MSARTA    PIC X.                                          00001420
           02 MSARTC    PIC X.                                          00001430
           02 MSARTP    PIC X.                                          00001440
           02 MSARTH    PIC X.                                          00001450
           02 MSARTV    PIC X.                                          00001460
           02 MSARTO    PIC --9.                                        00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MSMAGA    PIC X.                                          00001490
           02 MSMAGC    PIC X.                                          00001500
           02 MSMAGP    PIC X.                                          00001510
           02 MSMAGH    PIC X.                                          00001520
           02 MSMAGV    PIC X.                                          00001530
           02 MSMAGO    PIC --9.                                        00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MSDEPOTA  PIC X.                                          00001560
           02 MSDEPOTC  PIC X.                                          00001570
           02 MSDEPOTP  PIC X.                                          00001580
           02 MSDEPOTH  PIC X.                                          00001590
           02 MSDEPOTV  PIC X.                                          00001600
           02 MSDEPOTO  PIC --9.                                        00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLIBERRA  PIC X.                                          00001630
           02 MLIBERRC  PIC X.                                          00001640
           02 MLIBERRP  PIC X.                                          00001650
           02 MLIBERRH  PIC X.                                          00001660
           02 MLIBERRV  PIC X.                                          00001670
           02 MLIBERRO  PIC X(79).                                      00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MCODTRAA  PIC X.                                          00001700
           02 MCODTRAC  PIC X.                                          00001710
           02 MCODTRAP  PIC X.                                          00001720
           02 MCODTRAH  PIC X.                                          00001730
           02 MCODTRAV  PIC X.                                          00001740
           02 MCODTRAO  PIC X(4).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MZONCMDA  PIC X.                                          00001770
           02 MZONCMDC  PIC X.                                          00001780
           02 MZONCMDP  PIC X.                                          00001790
           02 MZONCMDH  PIC X.                                          00001800
           02 MZONCMDV  PIC X.                                          00001810
           02 MZONCMDO  PIC X(15).                                      00001820
           02 FILLER    PIC X(2).                                       00001830
           02 MCICSA    PIC X.                                          00001840
           02 MCICSC    PIC X.                                          00001850
           02 MCICSP    PIC X.                                          00001860
           02 MCICSH    PIC X.                                          00001870
           02 MCICSV    PIC X.                                          00001880
           02 MCICSO    PIC X(5).                                       00001890
           02 FILLER    PIC X(2).                                       00001900
           02 MNETNAMA  PIC X.                                          00001910
           02 MNETNAMC  PIC X.                                          00001920
           02 MNETNAMP  PIC X.                                          00001930
           02 MNETNAMH  PIC X.                                          00001940
           02 MNETNAMV  PIC X.                                          00001950
           02 MNETNAMO  PIC X(8).                                       00001960
           02 FILLER    PIC X(2).                                       00001970
           02 MSCREENA  PIC X.                                          00001980
           02 MSCREENC  PIC X.                                          00001990
           02 MSCREENP  PIC X.                                          00002000
           02 MSCREENH  PIC X.                                          00002010
           02 MSCREENV  PIC X.                                          00002020
           02 MSCREENO  PIC X(4).                                       00002030
                                                                                
