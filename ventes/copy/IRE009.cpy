      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE009      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE009.                                                        
            05 NOMETAT-IRE009           PIC X(6) VALUE 'IRE009'.                
            05 RUPTURES-IRE009.                                                 
           10 IRE009-NSOCIETE           PIC X(03).                      007  003
           10 IRE009-BLANC-BRUN         PIC X(05).                      010  005
           10 IRE009-TLM-ELA            PIC X(03).                      015  003
           10 IRE009-WSEQED             PIC S9(05)      COMP-3.         018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE009-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE009-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE009.                                                   
           10 IRE009-LMOISENC           PIC X(09).                      023  009
           10 IRE009-NRAYON             PIC X(15).                      032  015
           10 IRE009-CAA                PIC S9(13)V9(2) COMP-3.         047  008
           10 IRE009-CAM                PIC S9(13)V9(2) COMP-3.         055  008
           10 IRE009-FORCA              PIC S9(13)V9(2) COMP-3.         063  008
           10 IRE009-FORCM              PIC S9(13)V9(2) COMP-3.         071  008
           10 IRE009-PRMPA              PIC S9(13)V9(2) COMP-3.         079  008
           10 IRE009-PRMPM              PIC S9(13)V9(2) COMP-3.         087  008
           10 IRE009-REMA               PIC S9(13)V9(2) COMP-3.         095  008
           10 IRE009-REMM               PIC S9(13)V9(2) COMP-3.         103  008
            05 FILLER                      PIC X(402).                          
                                                                                
