      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVHV0800                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0800                         
      **********************************************************                
       01  RVHV0800.                                                            
           02  HV08-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV08-CFAM                                                        
               PIC X(0005).                                                     
           02  HV08-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  HV08-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV08-QPIECESCOMM                                                 
               PIC S9(5) COMP-3.                                                
           02  HV08-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PCACOMM                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATSCOMM                                               
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVHV0800                                  
      **********************************************************                
       01  RVHV0800-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESCOMM-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECESCOMM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCACOMM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCACOMM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSCOMM-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATSCOMM-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *                                                                         
      *--                                                                       
           02  HV08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
EMOD                                                                            
      *}                                                                        
