      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVAV0201                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVAV0201                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0201.                                                            
           02  AV02-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  AV02-NLIEU                                                       
               PIC X(0003).                                                     
           02  AV02-NAVOIR                                                      
               PIC X(0007).                                                     
           02  AV02-CTYPAVOIR                                                   
               PIC X(0005).                                                     
           02  AV02-CTITRENOM                                                   
               PIC X(0005).                                                     
           02  AV02-LNOM                                                        
               PIC X(0025).                                                     
           02  AV02-LPRENOM                                                     
               PIC X(0015).                                                     
           02  AV02-CVOIE                                                       
               PIC X(0005).                                                     
           02  AV02-CTVOIE                                                      
               PIC X(0004).                                                     
           02  AV02-LNOMVOIE                                                    
               PIC X(0021).                                                     
           02  AV02-CPOSTAL                                                     
               PIC X(0005).                                                     
           02  AV02-LCOMMUNE                                                    
               PIC X(0032).                                                     
           02  AV02-LBUREAU                                                     
               PIC X(0026).                                                     
           02  AV02-DSYST                                                       
               PIC S9(13) COMP-3.                                               
LA2811     02  AV02-LBATIMENT                                                   
LA2811         PIC X(0003).                                                     
LA2811     02  AV02-LESCALIER                                                   
LA2811         PIC X(0003).                                                     
LA2811     02  AV02-LETAGE                                                      
LA2811         PIC X(0003).                                                     
LA2811     02  AV02-LPORTE                                                      
LA2811         PIC X(0003).                                                     
LA2811     02  AV02-LCMPAD1                                                     
LA2811         PIC X(0032).                                                     
LA2811     02  AV02-LCMPAD2                                                     
LA2811         PIC X(0032).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVAV0201                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVAV0201-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-NAVOIR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-NAVOIR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-CTYPAVOIR-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-CTYPAVOIR-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-CTITRENOM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-LNOM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-LPRENOM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-CVOIE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-CTVOIE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-LNOMVOIE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-LCOMMUNE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-LBUREAU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  AV02-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LBATIMENT-F                                                 
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LESCALIER-F                                                 
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LETAGE-F                                                    
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LPORTE-F                                                    
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LCMPAD1-F                                                   
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LCMPAD1-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
LA2811*    02  AV02-LCMPAD2-F                                                   
LA2811*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AV02-LCMPAD2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
