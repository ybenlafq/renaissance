      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV25   EGV25                                              00000020
      ***************************************************************** 00000030
       01   EGV25I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTVENTEL  COMP PIC S9(4).                                         
      *--                                                                       
           02 MTVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTVENTEF  PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MTVENTEI  PIC X(8).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLRECHL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MLRECHL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLRECHF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MLRECHI   PIC X(11).                                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMML   COMP PIC S9(4).                                         
      *--                                                                       
           02 MLCOMML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLCOMMF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MLCOMMI   PIC X(8).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSINL     COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MSINL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSINF     PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MSINI     PIC X(30).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOML     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNOML COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNOMF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNOMI     PIC X(30).                                      00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MCPOSTALI      PIC X(5).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEL1L    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTEL1L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTEL1F    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTEL1I    PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEL2L    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MTEL2L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTEL2F    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MTEL2I    PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEL3L    COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MTEL3L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTEL3F    PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MTEL3I    PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEL4L    COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MTEL4L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTEL4F    PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MTEL4I    PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTEL5L    COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MTEL5L COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTEL5F    PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MTEL5I    PIC X(2).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSOCL     COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MSOCL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSOCF     PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MSOCI     PIC X(3).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLIEUL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLIEUF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLIEUI    PIC X(3).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MVENTEL   COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MVENTEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MVENTEF   PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MVENTEI   PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDANNULL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MLDANNULL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDANNULF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MLDANNULI      PIC X(32).                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULJL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MDANNULJL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNULJF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MDANNULJI      PIC X(2).                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULML      COMP PIC S9(4).                                    
      *--                                                                       
           02 MDANNULML COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNULMF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MDANNULMI      PIC X(2).                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULAL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MDANNULAL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDANNULAF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MDANNULAI      PIC X(4).                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFACTL   COMP PIC S9(4).                                         
      *--                                                                       
           02 MLFACTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLFACTF   PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MLFACTI   PIC X(8).                                               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLADRL    COMP PIC S9(4).                                         
      *--                                                                       
           02 MLADRL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLADRF    PIC X.                                                  
           02 FILLER    PIC X(4).                                               
           02 MLADRI    PIC X(6).                                               
           02 MLSINI OCCURS   10 TIMES .                                00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCHOIXL      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MCHOIXL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCHOIXF      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MCHOIXI      PIC X.                                     00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MASSURL      COMP PIC S9(4).                            00000630
      *--                                                                       
             03 MASSURL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MASSURF      PIC X.                                     00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MASSURI      PIC X(9).                                  00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOMCLIL     COMP PIC S9(4).                            00000670
      *--                                                                       
             03 MNOMCLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNOMCLIF     PIC X.                                     00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MNOMCLII     PIC X(15).                                 00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MADRCLIL     COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MADRCLIL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MADRCLIF     PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MADRCLII     PIC X(20).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCPCLIL      COMP PIC S9(4).                            00000750
      *--                                                                       
             03 MCPCLIL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCPCLIF      PIC X.                                     00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MCPCLII      PIC X(5).                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVILLEL      COMP PIC S9(4).                            00000790
      *--                                                                       
             03 MVILLEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MVILLEF      PIC X.                                     00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MVILLEI      PIC X(15).                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 METATL  COMP PIC S9(4).                                 00000830
      *--                                                                       
             03 METATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 METATF  PIC X.                                          00000840
             03 FILLER  PIC X(4).                                       00000850
             03 METATI  PIC X(5).                                       00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDETAILL      COMP PIC S9(4).                                    
      *--                                                                       
           02 MLDETAILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDETAILF      PIC X.                                             
           02 FILLER    PIC X(4).                                               
           02 MLDETAILI      PIC X(11).                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MLIBERRI  PIC X(78).                                      00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCODTRAI  PIC X(4).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * SDF: EGV25   EGV25                                              00001080
      ***************************************************************** 00001090
       01   EGV25O REDEFINES EGV25I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MTVENTEA  PIC X.                                                  
           02 MTVENTEC  PIC X.                                                  
           02 MTVENTEP  PIC X.                                                  
           02 MTVENTEH  PIC X.                                                  
           02 MTVENTEV  PIC X.                                                  
           02 MTVENTEO  PIC X(8).                                               
           02 FILLER    PIC X(2).                                               
           02 MLRECHA   PIC X.                                                  
           02 MLRECHC   PIC X.                                                  
           02 MLRECHP   PIC X.                                                  
           02 MLRECHH   PIC X.                                                  
           02 MLRECHV   PIC X.                                                  
           02 MLRECHO   PIC X(11).                                              
           02 FILLER    PIC X(2).                                               
           02 MLCOMMA   PIC X.                                                  
           02 MLCOMMC   PIC X.                                                  
           02 MLCOMMP   PIC X.                                                  
           02 MLCOMMH   PIC X.                                                  
           02 MLCOMMV   PIC X.                                                  
           02 MLCOMMO   PIC X(8).                                               
           02 FILLER    PIC X(2).                                               
           02 MSINA     PIC X.                                          00001270
           02 MSINC     PIC X.                                          00001280
           02 MSINP     PIC X.                                          00001290
           02 MSINH     PIC X.                                          00001300
           02 MSINV     PIC X.                                          00001310
           02 MSINO     PIC X(30).                                      00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNOMA     PIC X.                                          00001340
           02 MNOMC     PIC X.                                          00001350
           02 MNOMP     PIC X.                                          00001360
           02 MNOMH     PIC X.                                          00001370
           02 MNOMV     PIC X.                                          00001380
           02 MNOMO     PIC X(30).                                      00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MCPOSTALA      PIC X.                                     00001410
           02 MCPOSTALC PIC X.                                          00001420
           02 MCPOSTALP PIC X.                                          00001430
           02 MCPOSTALH PIC X.                                          00001440
           02 MCPOSTALV PIC X.                                          00001450
           02 MCPOSTALO      PIC X(5).                                  00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTEL1A    PIC X.                                          00001480
           02 MTEL1C    PIC X.                                          00001490
           02 MTEL1P    PIC X.                                          00001500
           02 MTEL1H    PIC X.                                          00001510
           02 MTEL1V    PIC X.                                          00001520
           02 MTEL1O    PIC X(2).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MTEL2A    PIC X.                                          00001550
           02 MTEL2C    PIC X.                                          00001560
           02 MTEL2P    PIC X.                                          00001570
           02 MTEL2H    PIC X.                                          00001580
           02 MTEL2V    PIC X.                                          00001590
           02 MTEL2O    PIC X(2).                                       00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MTEL3A    PIC X.                                          00001620
           02 MTEL3C    PIC X.                                          00001630
           02 MTEL3P    PIC X.                                          00001640
           02 MTEL3H    PIC X.                                          00001650
           02 MTEL3V    PIC X.                                          00001660
           02 MTEL3O    PIC X(2).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MTEL4A    PIC X.                                          00001690
           02 MTEL4C    PIC X.                                          00001700
           02 MTEL4P    PIC X.                                          00001710
           02 MTEL4H    PIC X.                                          00001720
           02 MTEL4V    PIC X.                                          00001730
           02 MTEL4O    PIC X(2).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MTEL5A    PIC X.                                          00001760
           02 MTEL5C    PIC X.                                          00001770
           02 MTEL5P    PIC X.                                          00001780
           02 MTEL5H    PIC X.                                          00001790
           02 MTEL5V    PIC X.                                          00001800
           02 MTEL5O    PIC X(2).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MSOCA     PIC X.                                          00001830
           02 MSOCC     PIC X.                                          00001840
           02 MSOCP     PIC X.                                          00001850
           02 MSOCH     PIC X.                                          00001860
           02 MSOCV     PIC X.                                          00001870
           02 MSOCO     PIC X(3).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MLIEUA    PIC X.                                          00001900
           02 MLIEUC    PIC X.                                          00001910
           02 MLIEUP    PIC X.                                          00001920
           02 MLIEUH    PIC X.                                          00001930
           02 MLIEUV    PIC X.                                          00001940
           02 MLIEUO    PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MVENTEA   PIC X.                                          00001970
           02 MVENTEC   PIC X.                                          00001980
           02 MVENTEP   PIC X.                                          00001990
           02 MVENTEH   PIC X.                                          00002000
           02 MVENTEV   PIC X.                                          00002010
           02 MVENTEO   PIC X(7).                                       00002020
           02 FILLER    PIC X(2).                                               
           02 MLDANNULA      PIC X.                                             
           02 MLDANNULC PIC X.                                                  
           02 MLDANNULP PIC X.                                                  
           02 MLDANNULH PIC X.                                                  
           02 MLDANNULV PIC X.                                                  
           02 MLDANNULO      PIC X(32).                                         
           02 FILLER    PIC X(2).                                               
           02 MDANNULJA      PIC X.                                             
           02 MDANNULJC PIC X.                                                  
           02 MDANNULJP PIC X.                                                  
           02 MDANNULJH PIC X.                                                  
           02 MDANNULJV PIC X.                                                  
           02 MDANNULJO      PIC X(2).                                          
           02 FILLER    PIC X(2).                                               
           02 MDANNULMA      PIC X.                                             
           02 MDANNULMC PIC X.                                                  
           02 MDANNULMP PIC X.                                                  
           02 MDANNULMH PIC X.                                                  
           02 MDANNULMV PIC X.                                                  
           02 MDANNULMO      PIC X(2).                                          
           02 FILLER    PIC X(2).                                               
           02 MDANNULAA      PIC X.                                             
           02 MDANNULAC PIC X.                                                  
           02 MDANNULAP PIC X.                                                  
           02 MDANNULAH PIC X.                                                  
           02 MDANNULAV PIC X.                                                  
           02 MDANNULAO      PIC X(4).                                          
           02 FILLER    PIC X(2).                                               
           02 MLFACTA   PIC X.                                                  
           02 MLFACTC   PIC X.                                                  
           02 MLFACTP   PIC X.                                                  
           02 MLFACTH   PIC X.                                                  
           02 MLFACTV   PIC X.                                                  
           02 MLFACTO   PIC X(8).                                               
           02 FILLER    PIC X(2).                                               
           02 MLADRA    PIC X.                                                  
           02 MLADRC    PIC X.                                                  
           02 MLADRP    PIC X.                                                  
           02 MLADRH    PIC X.                                                  
           02 MLADRV    PIC X.                                                  
           02 MLADRO    PIC X(6).                                               
           02 MLSINO OCCURS   10 TIMES .                                00002030
             03 FILLER       PIC X(2).                                  00002040
             03 MCHOIXA      PIC X.                                     00002050
             03 MCHOIXC PIC X.                                          00002060
             03 MCHOIXP PIC X.                                          00002070
             03 MCHOIXH PIC X.                                          00002080
             03 MCHOIXV PIC X.                                          00002090
             03 MCHOIXO      PIC X.                                     00002100
             03 FILLER       PIC X(2).                                  00002110
             03 MASSURA      PIC X.                                     00002120
             03 MASSURC PIC X.                                          00002130
             03 MASSURP PIC X.                                          00002140
             03 MASSURH PIC X.                                          00002150
             03 MASSURV PIC X.                                          00002160
             03 MASSURO      PIC X(9).                                  00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MNOMCLIA     PIC X.                                     00002190
             03 MNOMCLIC     PIC X.                                     00002200
             03 MNOMCLIP     PIC X.                                     00002210
             03 MNOMCLIH     PIC X.                                     00002220
             03 MNOMCLIV     PIC X.                                     00002230
             03 MNOMCLIO     PIC X(15).                                 00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MADRCLIA     PIC X.                                     00002260
             03 MADRCLIC     PIC X.                                     00002270
             03 MADRCLIP     PIC X.                                     00002280
             03 MADRCLIH     PIC X.                                     00002290
             03 MADRCLIV     PIC X.                                     00002300
             03 MADRCLIO     PIC X(20).                                 00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MCPCLIA      PIC X.                                     00002330
             03 MCPCLIC PIC X.                                          00002340
             03 MCPCLIP PIC X.                                          00002350
             03 MCPCLIH PIC X.                                          00002360
             03 MCPCLIV PIC X.                                          00002370
             03 MCPCLIO      PIC X(5).                                  00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MVILLEA      PIC X.                                     00002400
             03 MVILLEC PIC X.                                          00002410
             03 MVILLEP PIC X.                                          00002420
             03 MVILLEH PIC X.                                          00002430
             03 MVILLEV PIC X.                                          00002440
             03 MVILLEO      PIC X(15).                                 00002450
             03 FILLER       PIC X(2).                                  00002460
             03 METATA  PIC X.                                          00002470
             03 METATC  PIC X.                                          00002480
             03 METATP  PIC X.                                          00002490
             03 METATH  PIC X.                                          00002500
             03 METATV  PIC X.                                          00002510
             03 METATO  PIC X(5).                                       00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MLDETAILA      PIC X.                                             
           02 MLDETAILC PIC X.                                                  
           02 MLDETAILP PIC X.                                                  
           02 MLDETAILH PIC X.                                                  
           02 MLDETAILV PIC X.                                                  
           02 MLDETAILO      PIC X(11).                                         
           02 FILLER    PIC X(2).                                               
           02 MLIBERRA  PIC X.                                          00002540
           02 MLIBERRC  PIC X.                                          00002550
           02 MLIBERRP  PIC X.                                          00002560
           02 MLIBERRH  PIC X.                                          00002570
           02 MLIBERRV  PIC X.                                          00002580
           02 MLIBERRO  PIC X(78).                                      00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MCODTRAA  PIC X.                                          00002610
           02 MCODTRAC  PIC X.                                          00002620
           02 MCODTRAP  PIC X.                                          00002630
           02 MCODTRAH  PIC X.                                          00002640
           02 MCODTRAV  PIC X.                                          00002650
           02 MCODTRAO  PIC X(4).                                       00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
