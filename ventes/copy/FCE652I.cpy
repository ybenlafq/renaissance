      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------- *              
      * FICHIER ENTREE DU BCE652                                 *              
      * CONTIENT L'EXTRACTION DE LA RTCE52                       *              
      * -------------------------------------------------------- *              
       01 FCE652I-ENREG.                                                        
          05 FCE652I-NCODIC1                 PIC X(07).                         
          05 FCE652I-PRIX                    PIC X(01).                         
          05 FCE652I-COMPATIBLE              PIC X(05).                         
          05 FCE652I-COBJET                  PIC X(05).                         
          05 FCE652I-VERSION                 PIC X(01).                         
          05 FCE652I-NCODIC2                 PIC X(07).                         
          05 FCE652I-NCODICS                 PIC X(07).                         
          05 FCE652I-PRIX2                   PIC X(01).                         
                                                                                
