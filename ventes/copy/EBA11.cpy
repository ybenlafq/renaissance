      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBA11   EBA11                                              00000020
      ***************************************************************** 00000030
       01   EBA11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUI   PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDRECEPL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDRECEPL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDRECEPF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDRECEPI  PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPECARTL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MPECARTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPECARTF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MPECARTI  PIC X(11).                                      00000330
           02 MLIGNESI OCCURS   12 TIMES .                              00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFIL1L      COMP PIC S9(4).                            00000350
      *--                                                                       
             03 MNFIL1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNFIL1F      PIC X.                                     00000360
             03 FILLER  PIC X(4).                                       00000370
             03 MNFIL1I      PIC X(6).                                  00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBA1L  COMP PIC S9(4).                                 00000390
      *--                                                                       
             03 MNBA1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBA1F  PIC X.                                          00000400
             03 FILLER  PIC X(4).                                       00000410
             03 MNBA1I  PIC X(6).                                       00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPBA1L  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MPBA1L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPBA1F  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MPBA1I  PIC X(8).                                       00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFIL2L      COMP PIC S9(4).                            00000470
      *--                                                                       
             03 MNFIL2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNFIL2F      PIC X.                                     00000480
             03 FILLER  PIC X(4).                                       00000490
             03 MNFIL2I      PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBA2L  COMP PIC S9(4).                                 00000510
      *--                                                                       
             03 MNBA2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBA2F  PIC X.                                          00000520
             03 FILLER  PIC X(4).                                       00000530
             03 MNBA2I  PIC X(6).                                       00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPBA2L  COMP PIC S9(4).                                 00000550
      *--                                                                       
             03 MPBA2L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPBA2F  PIC X.                                          00000560
             03 FILLER  PIC X(4).                                       00000570
             03 MPBA2I  PIC X(8).                                       00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNFIL3L      COMP PIC S9(4).                            00000590
      *--                                                                       
             03 MNFIL3L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNFIL3F      PIC X.                                     00000600
             03 FILLER  PIC X(4).                                       00000610
             03 MNFIL3I      PIC X(6).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNBA3L  COMP PIC S9(4).                                 00000630
      *--                                                                       
             03 MNBA3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNBA3F  PIC X.                                          00000640
             03 FILLER  PIC X(4).                                       00000650
             03 MNBA3I  PIC X(6).                                       00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPBA3L  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MPBA3L COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPBA3F  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MPBA3I  PIC X(8).                                       00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCEDITL   COMP PIC S9(4).                                 00000710
      *--                                                                       
           02 MCEDITL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCEDITF   PIC X.                                          00000720
           02 FILLER    PIC X(4).                                       00000730
           02 MCEDITI   PIC X.                                          00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSUMPBAL  COMP PIC S9(4).                                 00000750
      *--                                                                       
           02 MSUMPBAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSUMPBAF  PIC X.                                          00000760
           02 FILLER    PIC X(4).                                       00000770
           02 MSUMPBAI  PIC X(10).                                      00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MZONCMDI  PIC X(15).                                      00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(58).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MCICSI    PIC X(5).                                       00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MNETNAMI  PIC X(8).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * SDF: EBA11   EBA11                                              00001040
      ***************************************************************** 00001050
       01   EBA11O REDEFINES EBA11I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
           02 FILLER    PIC X(2).                                       00001080
           02 MDATJOUA  PIC X.                                          00001090
           02 MDATJOUC  PIC X.                                          00001100
           02 MDATJOUP  PIC X.                                          00001110
           02 MDATJOUH  PIC X.                                          00001120
           02 MDATJOUV  PIC X.                                          00001130
           02 MDATJOUO  PIC X(10).                                      00001140
           02 FILLER    PIC X(2).                                       00001150
           02 MTIMJOUA  PIC X.                                          00001160
           02 MTIMJOUC  PIC X.                                          00001170
           02 MTIMJOUP  PIC X.                                          00001180
           02 MTIMJOUH  PIC X.                                          00001190
           02 MTIMJOUV  PIC X.                                          00001200
           02 MTIMJOUO  PIC X(5).                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNUMPAGEA      PIC X.                                     00001230
           02 MNUMPAGEC PIC X.                                          00001240
           02 MNUMPAGEP PIC X.                                          00001250
           02 MNUMPAGEH PIC X.                                          00001260
           02 MNUMPAGEV PIC X.                                          00001270
           02 MNUMPAGEO      PIC X(2).                                  00001280
           02 FILLER    PIC X(2).                                       00001290
           02 MPAGEMAXA      PIC X.                                     00001300
           02 MPAGEMAXC PIC X.                                          00001310
           02 MPAGEMAXP PIC X.                                          00001320
           02 MPAGEMAXH PIC X.                                          00001330
           02 MPAGEMAXV PIC X.                                          00001340
           02 MPAGEMAXO      PIC X(2).                                  00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MNLIEUA   PIC X.                                          00001370
           02 MNLIEUC   PIC X.                                          00001380
           02 MNLIEUP   PIC X.                                          00001390
           02 MNLIEUH   PIC X.                                          00001400
           02 MNLIEUV   PIC X.                                          00001410
           02 MNLIEUO   PIC X(3).                                       00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MDRECEPA  PIC X.                                          00001440
           02 MDRECEPC  PIC X.                                          00001450
           02 MDRECEPP  PIC X.                                          00001460
           02 MDRECEPH  PIC X.                                          00001470
           02 MDRECEPV  PIC X.                                          00001480
           02 MDRECEPO  PIC X(10).                                      00001490
           02 FILLER    PIC X(2).                                       00001500
           02 MPECARTA  PIC X.                                          00001510
           02 MPECARTC  PIC X.                                          00001520
           02 MPECARTP  PIC X.                                          00001530
           02 MPECARTH  PIC X.                                          00001540
           02 MPECARTV  PIC X.                                          00001550
           02 MPECARTO  PIC X(11).                                      00001560
           02 MLIGNESO OCCURS   12 TIMES .                              00001570
             03 FILLER       PIC X(2).                                  00001580
             03 MNFIL1A      PIC X.                                     00001590
             03 MNFIL1C PIC X.                                          00001600
             03 MNFIL1P PIC X.                                          00001610
             03 MNFIL1H PIC X.                                          00001620
             03 MNFIL1V PIC X.                                          00001630
             03 MNFIL1O      PIC X(6).                                  00001640
             03 FILLER       PIC X(2).                                  00001650
             03 MNBA1A  PIC X.                                          00001660
             03 MNBA1C  PIC X.                                          00001670
             03 MNBA1P  PIC X.                                          00001680
             03 MNBA1H  PIC X.                                          00001690
             03 MNBA1V  PIC X.                                          00001700
             03 MNBA1O  PIC X(6).                                       00001710
             03 FILLER       PIC X(2).                                  00001720
             03 MPBA1A  PIC X.                                          00001730
             03 MPBA1C  PIC X.                                          00001740
             03 MPBA1P  PIC X.                                          00001750
             03 MPBA1H  PIC X.                                          00001760
             03 MPBA1V  PIC X.                                          00001770
             03 MPBA1O  PIC X(8).                                       00001780
             03 FILLER       PIC X(2).                                  00001790
             03 MNFIL2A      PIC X.                                     00001800
             03 MNFIL2C PIC X.                                          00001810
             03 MNFIL2P PIC X.                                          00001820
             03 MNFIL2H PIC X.                                          00001830
             03 MNFIL2V PIC X.                                          00001840
             03 MNFIL2O      PIC X(6).                                  00001850
             03 FILLER       PIC X(2).                                  00001860
             03 MNBA2A  PIC X.                                          00001870
             03 MNBA2C  PIC X.                                          00001880
             03 MNBA2P  PIC X.                                          00001890
             03 MNBA2H  PIC X.                                          00001900
             03 MNBA2V  PIC X.                                          00001910
             03 MNBA2O  PIC X(6).                                       00001920
             03 FILLER       PIC X(2).                                  00001930
             03 MPBA2A  PIC X.                                          00001940
             03 MPBA2C  PIC X.                                          00001950
             03 MPBA2P  PIC X.                                          00001960
             03 MPBA2H  PIC X.                                          00001970
             03 MPBA2V  PIC X.                                          00001980
             03 MPBA2O  PIC X(8).                                       00001990
             03 FILLER       PIC X(2).                                  00002000
             03 MNFIL3A      PIC X.                                     00002010
             03 MNFIL3C PIC X.                                          00002020
             03 MNFIL3P PIC X.                                          00002030
             03 MNFIL3H PIC X.                                          00002040
             03 MNFIL3V PIC X.                                          00002050
             03 MNFIL3O      PIC X(6).                                  00002060
             03 FILLER       PIC X(2).                                  00002070
             03 MNBA3A  PIC X.                                          00002080
             03 MNBA3C  PIC X.                                          00002090
             03 MNBA3P  PIC X.                                          00002100
             03 MNBA3H  PIC X.                                          00002110
             03 MNBA3V  PIC X.                                          00002120
             03 MNBA3O  PIC X(6).                                       00002130
             03 FILLER       PIC X(2).                                  00002140
             03 MPBA3A  PIC X.                                          00002150
             03 MPBA3C  PIC X.                                          00002160
             03 MPBA3P  PIC X.                                          00002170
             03 MPBA3H  PIC X.                                          00002180
             03 MPBA3V  PIC X.                                          00002190
             03 MPBA3O  PIC X(8).                                       00002200
           02 FILLER    PIC X(2).                                       00002210
           02 MCEDITA   PIC X.                                          00002220
           02 MCEDITC   PIC X.                                          00002230
           02 MCEDITP   PIC X.                                          00002240
           02 MCEDITH   PIC X.                                          00002250
           02 MCEDITV   PIC X.                                          00002260
           02 MCEDITO   PIC X.                                          00002270
           02 FILLER    PIC X(2).                                       00002280
           02 MSUMPBAA  PIC X.                                          00002290
           02 MSUMPBAC  PIC X.                                          00002300
           02 MSUMPBAP  PIC X.                                          00002310
           02 MSUMPBAH  PIC X.                                          00002320
           02 MSUMPBAV  PIC X.                                          00002330
           02 MSUMPBAO  PIC X(10).                                      00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MZONCMDA  PIC X.                                          00002360
           02 MZONCMDC  PIC X.                                          00002370
           02 MZONCMDP  PIC X.                                          00002380
           02 MZONCMDH  PIC X.                                          00002390
           02 MZONCMDV  PIC X.                                          00002400
           02 MZONCMDO  PIC X(15).                                      00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MLIBERRA  PIC X.                                          00002430
           02 MLIBERRC  PIC X.                                          00002440
           02 MLIBERRP  PIC X.                                          00002450
           02 MLIBERRH  PIC X.                                          00002460
           02 MLIBERRV  PIC X.                                          00002470
           02 MLIBERRO  PIC X(58).                                      00002480
           02 FILLER    PIC X(2).                                       00002490
           02 MCODTRAA  PIC X.                                          00002500
           02 MCODTRAC  PIC X.                                          00002510
           02 MCODTRAP  PIC X.                                          00002520
           02 MCODTRAH  PIC X.                                          00002530
           02 MCODTRAV  PIC X.                                          00002540
           02 MCODTRAO  PIC X(4).                                       00002550
           02 FILLER    PIC X(2).                                       00002560
           02 MCICSA    PIC X.                                          00002570
           02 MCICSC    PIC X.                                          00002580
           02 MCICSP    PIC X.                                          00002590
           02 MCICSH    PIC X.                                          00002600
           02 MCICSV    PIC X.                                          00002610
           02 MCICSO    PIC X(5).                                       00002620
           02 FILLER    PIC X(2).                                       00002630
           02 MNETNAMA  PIC X.                                          00002640
           02 MNETNAMC  PIC X.                                          00002650
           02 MNETNAMP  PIC X.                                          00002660
           02 MNETNAMH  PIC X.                                          00002670
           02 MNETNAMV  PIC X.                                          00002680
           02 MNETNAMO  PIC X(8).                                       00002690
           02 FILLER    PIC X(2).                                       00002700
           02 MSCREENA  PIC X.                                          00002710
           02 MSCREENC  PIC X.                                          00002720
           02 MSCREENP  PIC X.                                          00002730
           02 MSCREENH  PIC X.                                          00002740
           02 MSCREENV  PIC X.                                          00002750
           02 MSCREENO  PIC X(4).                                       00002760
                                                                                
