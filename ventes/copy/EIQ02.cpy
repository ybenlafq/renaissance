      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: eiq02   eiq02                                              00000020
      ***************************************************************** 00000030
       01   EIQ02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLFAML    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MLFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLFAMF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MLFAMI    PIC X(44).                                      00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEDL   COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MPAGEDL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEDF   PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEDI   PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMDL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MPAGEMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAGEMDF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEMDI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MDATEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDATEF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MDATEI    PIC X(10).                                      00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MANNEEL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MANNEEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MANNEEF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MANNEEI   PIC X(4).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCPOSTALL      COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MCPOSTALL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCPOSTALF      PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCPOSTALI      PIC X(5).                                  00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLCOMMUNEL     COMP PIC S9(4).                            00000380
      *--                                                                       
           02 MLCOMMUNEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLCOMMUNEF     PIC X.                                     00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLCOMMUNEI     PIC X(32).                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWATPLTFL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MWATPLTFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MWATPLTFF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MWATPLTFI      PIC X.                                     00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPPLTFL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MPPLTFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPPLTFF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MPPLTFI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEPLTFL   COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MEPLTFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEPLTFF   PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MEPLTFI   PIC X(5).                                       00000530
           02 MTABPLAGEI OCCURS   6 TIMES .                             00000540
             03 MAPLAGED OCCURS   2 TIMES .                             00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MAPLAGEL   COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MAPLAGEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MAPLAGEF   PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MAPLAGEI   PIC X.                                     00000590
             03 MCPLAGED OCCURS   2 TIMES .                             00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCPLAGEL   COMP PIC S9(4).                            00000610
      *--                                                                       
               04 MCPLAGEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCPLAGEF   PIC X.                                     00000620
               04 FILLER     PIC X(4).                                  00000630
               04 MCPLAGEI   PIC X(2).                                  00000640
             03 MLPLAGED OCCURS   2 TIMES .                             00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLPLAGEL   COMP PIC S9(4).                            00000660
      *--                                                                       
               04 MLPLAGEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MLPLAGEF   PIC X.                                     00000670
               04 FILLER     PIC X(4).                                  00000680
               04 MLPLAGEI   PIC X(11).                                 00000690
           02 MTABSERVI OCCURS   6 TIMES .                              00000700
             03 MASRVCED OCCURS   2 TIMES .                             00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MASRVCEL   COMP PIC S9(4).                            00000720
      *--                                                                       
               04 MASRVCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MASRVCEF   PIC X.                                     00000730
               04 FILLER     PIC X(4).                                  00000740
               04 MASRVCEI   PIC X.                                     00000750
             03 MCSRVCED OCCURS   2 TIMES .                             00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCSRVCEL   COMP PIC S9(4).                            00000770
      *--                                                                       
               04 MCSRVCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCSRVCEF   PIC X.                                     00000780
               04 FILLER     PIC X(4).                                  00000790
               04 MCSRVCEI   PIC X(5).                                  00000800
             03 MPSRVCED OCCURS   2 TIMES .                             00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MPSRVCEL   COMP PIC S9(4).                            00000820
      *--                                                                       
               04 MPSRVCEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MPSRVCEF   PIC X.                                     00000830
               04 FILLER     PIC X(4).                                  00000840
               04 MPSRVCEI   PIC X(6).                                  00000850
           02 MLDATED OCCURS   9 TIMES .                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLDATEL      COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MLDATEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLDATEF      PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MLDATEI      PIC X(5).                                  00000900
           02 MTAB2I OCCURS   12 TIMES .                                00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCODEPL      COMP PIC S9(4).                            00000920
      *--                                                                       
             03 MCODEPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCODEPF      PIC X.                                     00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MCODEPI      PIC X.                                     00000950
             03 MCODESD OCCURS   9 TIMES .                              00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCODESL    COMP PIC S9(4).                            00000970
      *--                                                                       
               04 MCODESL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MCODESF    PIC X.                                     00000980
               04 FILLER     PIC X(4).                                  00000990
               04 MCODESI    PIC X.                                     00001000
             03 MQUOTAD OCCURS   9 TIMES .                              00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQUOTAL    COMP PIC S9(4).                            00001020
      *--                                                                       
               04 MQUOTAL COMP-5 PIC S9(4).                                     
      *}                                                                        
               04 MQUOTAF    PIC X.                                     00001030
               04 FILLER     PIC X(4).                                  00001040
               04 MQUOTAI    PIC X(5).                                  00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MLIBERRI  PIC X(78).                                      00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MCODTRAI  PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MCICSI    PIC X(5).                                       00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNETNAMI  PIC X(8).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MSCREENI  PIC X(5).                                       00001250
      ***************************************************************** 00001260
      * SDF: eiq02   eiq02                                              00001270
      ***************************************************************** 00001280
       01   EIQ02O REDEFINES EIQ02I.                                    00001290
           02 FILLER    PIC X(12).                                      00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MDATJOUA  PIC X.                                          00001320
           02 MDATJOUC  PIC X.                                          00001330
           02 MDATJOUP  PIC X.                                          00001340
           02 MDATJOUH  PIC X.                                          00001350
           02 MDATJOUV  PIC X.                                          00001360
           02 MDATJOUO  PIC X(10).                                      00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MTIMJOUA  PIC X.                                          00001390
           02 MTIMJOUC  PIC X.                                          00001400
           02 MTIMJOUP  PIC X.                                          00001410
           02 MTIMJOUH  PIC X.                                          00001420
           02 MTIMJOUV  PIC X.                                          00001430
           02 MTIMJOUO  PIC X(5).                                       00001440
           02 FILLER    PIC X(2).                                       00001450
           02 MLFAMA    PIC X.                                          00001460
           02 MLFAMC    PIC X.                                          00001470
           02 MLFAMP    PIC X.                                          00001480
           02 MLFAMH    PIC X.                                          00001490
           02 MLFAMV    PIC X.                                          00001500
           02 MLFAMO    PIC X(44).                                      00001510
           02 FILLER    PIC X(2).                                       00001520
           02 MPAGEDA   PIC X.                                          00001530
           02 MPAGEDC   PIC X.                                          00001540
           02 MPAGEDP   PIC X.                                          00001550
           02 MPAGEDH   PIC X.                                          00001560
           02 MPAGEDV   PIC X.                                          00001570
           02 MPAGEDO   PIC X(3).                                       00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MPAGEMDA  PIC X.                                          00001600
           02 MPAGEMDC  PIC X.                                          00001610
           02 MPAGEMDP  PIC X.                                          00001620
           02 MPAGEMDH  PIC X.                                          00001630
           02 MPAGEMDV  PIC X.                                          00001640
           02 MPAGEMDO  PIC X(3).                                       00001650
           02 FILLER    PIC X(2).                                       00001660
           02 MDATEA    PIC X.                                          00001670
           02 MDATEC    PIC X.                                          00001680
           02 MDATEP    PIC X.                                          00001690
           02 MDATEH    PIC X.                                          00001700
           02 MDATEV    PIC X.                                          00001710
           02 MDATEO    PIC X(10).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MANNEEA   PIC X.                                          00001740
           02 MANNEEC   PIC X.                                          00001750
           02 MANNEEP   PIC X.                                          00001760
           02 MANNEEH   PIC X.                                          00001770
           02 MANNEEV   PIC X.                                          00001780
           02 MANNEEO   PIC X(4).                                       00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MCPOSTALA      PIC X.                                     00001810
           02 MCPOSTALC PIC X.                                          00001820
           02 MCPOSTALP PIC X.                                          00001830
           02 MCPOSTALH PIC X.                                          00001840
           02 MCPOSTALV PIC X.                                          00001850
           02 MCPOSTALO      PIC X(5).                                  00001860
           02 FILLER    PIC X(2).                                       00001870
           02 MLCOMMUNEA     PIC X.                                     00001880
           02 MLCOMMUNEC     PIC X.                                     00001890
           02 MLCOMMUNEP     PIC X.                                     00001900
           02 MLCOMMUNEH     PIC X.                                     00001910
           02 MLCOMMUNEV     PIC X.                                     00001920
           02 MLCOMMUNEO     PIC X(32).                                 00001930
           02 FILLER    PIC X(2).                                       00001940
           02 MWATPLTFA      PIC X.                                     00001950
           02 MWATPLTFC PIC X.                                          00001960
           02 MWATPLTFP PIC X.                                          00001970
           02 MWATPLTFH PIC X.                                          00001980
           02 MWATPLTFV PIC X.                                          00001990
           02 MWATPLTFO      PIC X.                                     00002000
           02 FILLER    PIC X(2).                                       00002010
           02 MPPLTFA   PIC X.                                          00002020
           02 MPPLTFC   PIC X.                                          00002030
           02 MPPLTFP   PIC X.                                          00002040
           02 MPPLTFH   PIC X.                                          00002050
           02 MPPLTFV   PIC X.                                          00002060
           02 MPPLTFO   PIC X(5).                                       00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MEPLTFA   PIC X.                                          00002090
           02 MEPLTFC   PIC X.                                          00002100
           02 MEPLTFP   PIC X.                                          00002110
           02 MEPLTFH   PIC X.                                          00002120
           02 MEPLTFV   PIC X.                                          00002130
           02 MEPLTFO   PIC X(5).                                       00002140
           02 MTABPLAGEO OCCURS   6 TIMES .                             00002150
             03 MS1 OCCURS   2 TIMES .                                  00002160
               04 FILLER     PIC X(2).                                  00002170
               04 MAPLAGEA   PIC X.                                     00002180
               04 MAPLAGEC   PIC X.                                     00002190
               04 MAPLAGEP   PIC X.                                     00002200
               04 MAPLAGEH   PIC X.                                     00002210
               04 MAPLAGEV   PIC X.                                     00002220
               04 MAPLAGEO   PIC X.                                     00002230
             03 MS2 OCCURS   2 TIMES .                                  00002240
               04 FILLER     PIC X(2).                                  00002250
               04 MCPLAGEA   PIC X.                                     00002260
               04 MCPLAGEC   PIC X.                                     00002270
               04 MCPLAGEP   PIC X.                                     00002280
               04 MCPLAGEH   PIC X.                                     00002290
               04 MCPLAGEV   PIC X.                                     00002300
               04 MCPLAGEO   PIC X(2).                                  00002310
             03 MS3 OCCURS   2 TIMES .                                  00002320
               04 FILLER     PIC X(2).                                  00002330
               04 MLPLAGEA   PIC X.                                     00002340
               04 MLPLAGEC   PIC X.                                     00002350
               04 MLPLAGEP   PIC X.                                     00002360
               04 MLPLAGEH   PIC X.                                     00002370
               04 MLPLAGEV   PIC X.                                     00002380
               04 MLPLAGEO   PIC X(11).                                 00002390
           02 MTABSERVO OCCURS   6 TIMES .                              00002400
             03 MS4 OCCURS   2 TIMES .                                  00002410
               04 FILLER     PIC X(2).                                  00002420
               04 MASRVCEA   PIC X.                                     00002430
               04 MASRVCEC   PIC X.                                     00002440
               04 MASRVCEP   PIC X.                                     00002450
               04 MASRVCEH   PIC X.                                     00002460
               04 MASRVCEV   PIC X.                                     00002470
               04 MASRVCEO   PIC X.                                     00002480
             03 MS5 OCCURS   2 TIMES .                                  00002490
               04 FILLER     PIC X(2).                                  00002500
               04 MCSRVCEA   PIC X.                                     00002510
               04 MCSRVCEC   PIC X.                                     00002520
               04 MCSRVCEP   PIC X.                                     00002530
               04 MCSRVCEH   PIC X.                                     00002540
               04 MCSRVCEV   PIC X.                                     00002550
               04 MCSRVCEO   PIC X(5).                                  00002560
             03 MS6 OCCURS   2 TIMES .                                  00002570
               04 FILLER     PIC X(2).                                  00002580
               04 MPSRVCEA   PIC X.                                     00002590
               04 MPSRVCEC   PIC X.                                     00002600
               04 MPSRVCEP   PIC X.                                     00002610
               04 MPSRVCEH   PIC X.                                     00002620
               04 MPSRVCEV   PIC X.                                     00002630
               04 MPSRVCEO   PIC X(6).                                  00002640
           02 MS7 OCCURS   9 TIMES .                                    00002650
             03 FILLER       PIC X(2).                                  00002660
             03 MLDATEA      PIC X.                                     00002670
             03 MLDATEC PIC X.                                          00002680
             03 MLDATEP PIC X.                                          00002690
             03 MLDATEH PIC X.                                          00002700
             03 MLDATEV PIC X.                                          00002710
             03 MLDATEO      PIC X(5).                                  00002720
           02 MTAB2O OCCURS   12 TIMES .                                00002730
             03 FILLER       PIC X(2).                                  00002740
             03 MCODEPA      PIC X.                                     00002750
             03 MCODEPC PIC X.                                          00002760
             03 MCODEPP PIC X.                                          00002770
             03 MCODEPH PIC X.                                          00002780
             03 MCODEPV PIC X.                                          00002790
             03 MCODEPO      PIC X.                                     00002800
             03 MS8 OCCURS   9 TIMES .                                  00002810
               04 FILLER     PIC X(2).                                  00002820
               04 MCODESA    PIC X.                                     00002830
               04 MCODESC    PIC X.                                     00002840
               04 MCODESP    PIC X.                                     00002850
               04 MCODESH    PIC X.                                     00002860
               04 MCODESV    PIC X.                                     00002870
               04 MCODESO    PIC X.                                     00002880
             03 MS9 OCCURS   9 TIMES .                                  00002890
               04 FILLER     PIC X(2).                                  00002900
               04 MQUOTAA    PIC X.                                     00002910
               04 MQUOTAC    PIC X.                                     00002920
               04 MQUOTAP    PIC X.                                     00002930
               04 MQUOTAH    PIC X.                                     00002940
               04 MQUOTAV    PIC X.                                     00002950
               04 MQUOTAO    PIC X(5).                                  00002960
           02 FILLER    PIC X(2).                                       00002970
           02 MLIBERRA  PIC X.                                          00002980
           02 MLIBERRC  PIC X.                                          00002990
           02 MLIBERRP  PIC X.                                          00003000
           02 MLIBERRH  PIC X.                                          00003010
           02 MLIBERRV  PIC X.                                          00003020
           02 MLIBERRO  PIC X(78).                                      00003030
           02 FILLER    PIC X(2).                                       00003040
           02 MCODTRAA  PIC X.                                          00003050
           02 MCODTRAC  PIC X.                                          00003060
           02 MCODTRAP  PIC X.                                          00003070
           02 MCODTRAH  PIC X.                                          00003080
           02 MCODTRAV  PIC X.                                          00003090
           02 MCODTRAO  PIC X(4).                                       00003100
           02 FILLER    PIC X(2).                                       00003110
           02 MCICSA    PIC X.                                          00003120
           02 MCICSC    PIC X.                                          00003130
           02 MCICSP    PIC X.                                          00003140
           02 MCICSH    PIC X.                                          00003150
           02 MCICSV    PIC X.                                          00003160
           02 MCICSO    PIC X(5).                                       00003170
           02 FILLER    PIC X(2).                                       00003180
           02 MNETNAMA  PIC X.                                          00003190
           02 MNETNAMC  PIC X.                                          00003200
           02 MNETNAMP  PIC X.                                          00003210
           02 MNETNAMH  PIC X.                                          00003220
           02 MNETNAMV  PIC X.                                          00003230
           02 MNETNAMO  PIC X(8).                                       00003240
           02 FILLER    PIC X(2).                                       00003250
           02 MSCREENA  PIC X.                                          00003260
           02 MSCREENC  PIC X.                                          00003270
           02 MSCREENP  PIC X.                                          00003280
           02 MSCREENH  PIC X.                                          00003290
           02 MSCREENV  PIC X.                                          00003300
           02 MSCREENO  PIC X(5).                                       00003310
                                                                                
