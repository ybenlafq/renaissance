      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV81   EGV81                                              00000020
      ***************************************************************** 00000030
       01   EGV81I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE                                                  00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWPAGEL   COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MWPAGEL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWPAGEF   PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MWPAGEI   PIC X(3).                                       00000200
           02 MBESI OCCURS   15 TIMES .                                 00000210
      * DETAIL DE BE                                                    00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIGNE-BEL   COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MLIGNE-BEL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MLIGNE-BEF   PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MLIGNE-BEI   PIC X(78).                                 00000260
      * ZONE CMD AIDA                                                   00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MZONCMDI  PIC X(15).                                      00000310
      * MESSAGE ERREUR                                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000330
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000340
           02 FILLER    PIC X(4).                                       00000350
           02 MLIBERRI  PIC X(58).                                      00000360
      * CODE TRANSACTION                                                00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MCODTRAI  PIC X(4).                                       00000410
      * CICS DE TRAVAIL                                                 00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000430
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000440
           02 FILLER    PIC X(4).                                       00000450
           02 MCICSI    PIC X(5).                                       00000460
      * NETNAME                                                         00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MNETNAMI  PIC X(8).                                       00000510
      * CODE TERMINAL                                                   00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000530
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000540
           02 FILLER    PIC X(4).                                       00000550
           02 MSCREENI  PIC X(5).                                       00000560
      ***************************************************************** 00000570
      * SDF: EGV81   EGV81                                              00000580
      ***************************************************************** 00000590
       01   EGV81O REDEFINES EGV81I.                                    00000600
           02 FILLER    PIC X(12).                                      00000610
      * DATE DU JOUR(JJ/MM/AAAA)                                        00000620
           02 FILLER    PIC X(2).                                       00000630
           02 MDATJOUA  PIC X.                                          00000640
           02 MDATJOUC  PIC X.                                          00000650
           02 MDATJOUP  PIC X.                                          00000660
           02 MDATJOUH  PIC X.                                          00000670
           02 MDATJOUV  PIC X.                                          00000680
           02 MDATJOUO  PIC X(10).                                      00000690
      * HEURE                                                           00000700
           02 FILLER    PIC X(2).                                       00000710
           02 MTIMJOUA  PIC X.                                          00000720
           02 MTIMJOUC  PIC X.                                          00000730
           02 MTIMJOUP  PIC X.                                          00000740
           02 MTIMJOUH  PIC X.                                          00000750
           02 MTIMJOUV  PIC X.                                          00000760
           02 MTIMJOUO  PIC X(5).                                       00000770
      * NUMERO DE PAGE                                                  00000780
           02 FILLER    PIC X(2).                                       00000790
           02 MWPAGEA   PIC X.                                          00000800
           02 MWPAGEC   PIC X.                                          00000810
           02 MWPAGEP   PIC X.                                          00000820
           02 MWPAGEH   PIC X.                                          00000830
           02 MWPAGEV   PIC X.                                          00000840
           02 MWPAGEO   PIC ZZ9.                                        00000850
           02 MBESO OCCURS   15 TIMES .                                 00000860
      * DETAIL DE BE                                                    00000870
             03 FILLER       PIC X(2).                                  00000880
             03 MLIGNE-BEA   PIC X.                                     00000890
             03 MLIGNE-BEC   PIC X.                                     00000900
             03 MLIGNE-BEP   PIC X.                                     00000910
             03 MLIGNE-BEH   PIC X.                                     00000920
             03 MLIGNE-BEV   PIC X.                                     00000930
             03 MLIGNE-BEO   PIC X(78).                                 00000940
      * ZONE CMD AIDA                                                   00000950
           02 FILLER    PIC X(2).                                       00000960
           02 MZONCMDA  PIC X.                                          00000970
           02 MZONCMDC  PIC X.                                          00000980
           02 MZONCMDP  PIC X.                                          00000990
           02 MZONCMDH  PIC X.                                          00001000
           02 MZONCMDV  PIC X.                                          00001010
           02 MZONCMDO  PIC X(15).                                      00001020
      * MESSAGE ERREUR                                                  00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MLIBERRA  PIC X.                                          00001050
           02 MLIBERRC  PIC X.                                          00001060
           02 MLIBERRP  PIC X.                                          00001070
           02 MLIBERRH  PIC X.                                          00001080
           02 MLIBERRV  PIC X.                                          00001090
           02 MLIBERRO  PIC X(58).                                      00001100
      * CODE TRANSACTION                                                00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MCODTRAA  PIC X.                                          00001130
           02 MCODTRAC  PIC X.                                          00001140
           02 MCODTRAP  PIC X.                                          00001150
           02 MCODTRAH  PIC X.                                          00001160
           02 MCODTRAV  PIC X.                                          00001170
           02 MCODTRAO  PIC X(4).                                       00001180
      * CICS DE TRAVAIL                                                 00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MCICSA    PIC X.                                          00001210
           02 MCICSC    PIC X.                                          00001220
           02 MCICSP    PIC X.                                          00001230
           02 MCICSH    PIC X.                                          00001240
           02 MCICSV    PIC X.                                          00001250
           02 MCICSO    PIC X(5).                                       00001260
      * NETNAME                                                         00001270
           02 FILLER    PIC X(2).                                       00001280
           02 MNETNAMA  PIC X.                                          00001290
           02 MNETNAMC  PIC X.                                          00001300
           02 MNETNAMP  PIC X.                                          00001310
           02 MNETNAMH  PIC X.                                          00001320
           02 MNETNAMV  PIC X.                                          00001330
           02 MNETNAMO  PIC X(8).                                       00001340
      * CODE TERMINAL                                                   00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MSCREENA  PIC X.                                          00001370
           02 MSCREENC  PIC X.                                          00001380
           02 MSCREENP  PIC X.                                          00001390
           02 MSCREENH  PIC X.                                          00001400
           02 MSCREENV  PIC X.                                          00001410
           02 MSCREENO  PIC X(5).                                       00001420
                                                                                
