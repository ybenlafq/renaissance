      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAC13   EAC13                                              00000020
      ***************************************************************** 00000030
       01   EAC13I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTITREL   COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MTITREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MTITREF   PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MTITREI   PIC X(56).                                      00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPAFFL  COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MTYPAFFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTYPAFFF  PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MTYPAFFI  PIC X(17).                                      00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBUTL  COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 MDDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDDEBUTF  PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 MDDEBUTI  PIC X(10).                                      00000270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJDEBUTL  COMP PIC S9(4).                                 00000280
      *--                                                                       
           02 MJDEBUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MJDEBUTF  PIC X.                                          00000290
           02 FILLER    PIC X(4).                                       00000300
           02 MJDEBUTI  PIC X(10).                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINL    COMP PIC S9(4).                                 00000320
      *--                                                                       
           02 MDFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MDFINF    PIC X.                                          00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MDFINI    PIC X(10).                                      00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MJFINL    COMP PIC S9(4).                                 00000360
      *--                                                                       
           02 MJFINL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MJFINF    PIC X.                                          00000370
           02 FILLER    PIC X(4).                                       00000380
           02 MJFINI    PIC X(10).                                      00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE1L     COMP PIC S9(4).                                 00000400
      *--                                                                       
           02 MTE1L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE1F     PIC X.                                          00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MTE1I     PIC X(28).                                      00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDDEBUTL      COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MEDDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEDDEBUTF      PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MEDDEBUTI      PIC X(10).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJDEBUTL      COMP PIC S9(4).                            00000480
      *--                                                                       
           02 MEJDEBUTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MEJDEBUTF      PIC X.                                     00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MEJDEBUTI      PIC X(10).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTE2L     COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MTE2L COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MTE2F     PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MTE2I     PIC X(4).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEDFINL   COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MEDFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEDFINF   PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MEDFINI   PIC X(10).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MEJFINL   COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MEJFINL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MEJFINF   PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MEJFINI   PIC X(10).                                      00000630
           02 MLJOURD OCCURS   7 TIMES .                                00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLJOURL      COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MLJOURL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLJOURF      PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MLJOURI      PIC X(7).                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDIMCAL   COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MDIMCAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDIMCAF   PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MDIMCAI   PIC X(22).                                      00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDIMCAPDL      COMP PIC S9(4).                            00000730
      *--                                                                       
           02 MDIMCAPDL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDIMCAPDF      PIC X.                                     00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MDIMCAPDI      PIC X(4).                                  00000760
           02 MLIGNECAI OCCURS   14 TIMES .                             00000770
             03 MCAD OCCURS   7 TIMES .                                 00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCAL  COMP PIC S9(4).                                 00000790
      *--                                                                       
               04 MCAL COMP-5 PIC S9(4).                                        
      *}                                                                        
               04 MCAF  PIC X.                                          00000800
               04 FILLER     PIC X(4).                                  00000810
               04 MCAI  PIC X(7).                                       00000820
           02 MLIGNCATI OCCURS   14 TIMES .                             00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCACL   COMP PIC S9(4).                                 00000840
      *--                                                                       
             03 MCACL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCACF   PIC X.                                          00000850
             03 FILLER  PIC X(4).                                       00000860
             03 MCACI   PIC X(7).                                       00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPDTL      COMP PIC S9(4).                            00000880
      *--                                                                       
             03 MCAPDTL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAPDTF      PIC X.                                     00000890
             03 FILLER  PIC X(4).                                       00000900
             03 MCAPDTI      PIC X(4).                                  00000910
           02 MCATD OCCURS   7 TIMES .                                  00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCATL   COMP PIC S9(4).                                 00000930
      *--                                                                       
             03 MCATL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCATF   PIC X.                                          00000940
             03 FILLER  PIC X(4).                                       00000950
             03 MCATI   PIC X(7).                                       00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCACTL    COMP PIC S9(4).                                 00000970
      *--                                                                       
           02 MCACTL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCACTF    PIC X.                                          00000980
           02 FILLER    PIC X(4).                                       00000990
           02 MCACTI    PIC X(7).                                       00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPDTTL  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MCAPDTTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPDTTF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MCAPDTTI  PIC X(4).                                       00001040
           02 MCAPDJD OCCURS   7 TIMES .                                00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCAPDJL      COMP PIC S9(4).                            00001060
      *--                                                                       
             03 MCAPDJL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MCAPDJF      PIC X.                                     00001070
             03 FILLER  PIC X(4).                                       00001080
             03 MCAPDJI      PIC X(4).                                  00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MLIBERRI  PIC X(79).                                      00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBPFL   COMP PIC S9(4).                                 00001140
      *--                                                                       
           02 MLIBPFL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLIBPFF   PIC X.                                          00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MLIBPFI   PIC X(79).                                      00001170
      * CODE TRANSACTION                                                00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCODTRAI  PIC X(4).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MZONCMDI  PIC X(15).                                      00001260
      * NOM DU CICS                                                     00001270
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001280
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001290
           02 FILLER    PIC X(4).                                       00001300
           02 MCICSI    PIC X(5).                                       00001310
      * NETNAME                                                         00001320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001330
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001340
           02 FILLER    PIC X(4).                                       00001350
           02 MNETNAMI  PIC X(8).                                       00001360
      * CODE TERMINAL                                                   00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MSCREENI  PIC X(5).                                       00001410
      ***************************************************************** 00001420
      * SDF: EAC13   EAC13                                              00001430
      ***************************************************************** 00001440
       01   EAC13O REDEFINES EAC13I.                                    00001450
           02 FILLER    PIC X(12).                                      00001460
      * DATE DU JOUR                                                    00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MDATJOUA  PIC X.                                          00001490
           02 MDATJOUC  PIC X.                                          00001500
           02 MDATJOUP  PIC X.                                          00001510
           02 MDATJOUH  PIC X.                                          00001520
           02 MDATJOUV  PIC X.                                          00001530
           02 MDATJOUO  PIC X(10).                                      00001540
      * HEURE                                                           00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MTIMJOUA  PIC X.                                          00001570
           02 MTIMJOUC  PIC X.                                          00001580
           02 MTIMJOUP  PIC X.                                          00001590
           02 MTIMJOUH  PIC X.                                          00001600
           02 MTIMJOUV  PIC X.                                          00001610
           02 MTIMJOUO  PIC X(5).                                       00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MTITREA   PIC X.                                          00001640
           02 MTITREC   PIC X.                                          00001650
           02 MTITREP   PIC X.                                          00001660
           02 MTITREH   PIC X.                                          00001670
           02 MTITREV   PIC X.                                          00001680
           02 MTITREO   PIC X(56).                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MTYPAFFA  PIC X.                                          00001710
           02 MTYPAFFC  PIC X.                                          00001720
           02 MTYPAFFP  PIC X.                                          00001730
           02 MTYPAFFH  PIC X.                                          00001740
           02 MTYPAFFV  PIC X.                                          00001750
           02 MTYPAFFO  PIC X(17).                                      00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MDDEBUTA  PIC X.                                          00001780
           02 MDDEBUTC  PIC X.                                          00001790
           02 MDDEBUTP  PIC X.                                          00001800
           02 MDDEBUTH  PIC X.                                          00001810
           02 MDDEBUTV  PIC X.                                          00001820
           02 MDDEBUTO  PIC X(10).                                      00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MJDEBUTA  PIC X.                                          00001850
           02 MJDEBUTC  PIC X.                                          00001860
           02 MJDEBUTP  PIC X.                                          00001870
           02 MJDEBUTH  PIC X.                                          00001880
           02 MJDEBUTV  PIC X.                                          00001890
           02 MJDEBUTO  PIC X(10).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDFINA    PIC X.                                          00001920
           02 MDFINC    PIC X.                                          00001930
           02 MDFINP    PIC X.                                          00001940
           02 MDFINH    PIC X.                                          00001950
           02 MDFINV    PIC X.                                          00001960
           02 MDFINO    PIC X(10).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MJFINA    PIC X.                                          00001990
           02 MJFINC    PIC X.                                          00002000
           02 MJFINP    PIC X.                                          00002010
           02 MJFINH    PIC X.                                          00002020
           02 MJFINV    PIC X.                                          00002030
           02 MJFINO    PIC X(10).                                      00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MTE1A     PIC X.                                          00002060
           02 MTE1C     PIC X.                                          00002070
           02 MTE1P     PIC X.                                          00002080
           02 MTE1H     PIC X.                                          00002090
           02 MTE1V     PIC X.                                          00002100
           02 MTE1O     PIC X(28).                                      00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MEDDEBUTA      PIC X.                                     00002130
           02 MEDDEBUTC PIC X.                                          00002140
           02 MEDDEBUTP PIC X.                                          00002150
           02 MEDDEBUTH PIC X.                                          00002160
           02 MEDDEBUTV PIC X.                                          00002170
           02 MEDDEBUTO      PIC X(10).                                 00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MEJDEBUTA      PIC X.                                     00002200
           02 MEJDEBUTC PIC X.                                          00002210
           02 MEJDEBUTP PIC X.                                          00002220
           02 MEJDEBUTH PIC X.                                          00002230
           02 MEJDEBUTV PIC X.                                          00002240
           02 MEJDEBUTO      PIC X(10).                                 00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MTE2A     PIC X.                                          00002270
           02 MTE2C     PIC X.                                          00002280
           02 MTE2P     PIC X.                                          00002290
           02 MTE2H     PIC X.                                          00002300
           02 MTE2V     PIC X.                                          00002310
           02 MTE2O     PIC X(4).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MEDFINA   PIC X.                                          00002340
           02 MEDFINC   PIC X.                                          00002350
           02 MEDFINP   PIC X.                                          00002360
           02 MEDFINH   PIC X.                                          00002370
           02 MEDFINV   PIC X.                                          00002380
           02 MEDFINO   PIC X(10).                                      00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MEJFINA   PIC X.                                          00002410
           02 MEJFINC   PIC X.                                          00002420
           02 MEJFINP   PIC X.                                          00002430
           02 MEJFINH   PIC X.                                          00002440
           02 MEJFINV   PIC X.                                          00002450
           02 MEJFINO   PIC X(10).                                      00002460
           02 DFHMS1 OCCURS   7 TIMES .                                 00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MLJOURA      PIC X.                                     00002490
             03 MLJOURC PIC X.                                          00002500
             03 MLJOURP PIC X.                                          00002510
             03 MLJOURH PIC X.                                          00002520
             03 MLJOURV PIC X.                                          00002530
             03 MLJOURO      PIC X(7).                                  00002540
           02 FILLER    PIC X(2).                                       00002550
           02 MDIMCAA   PIC X.                                          00002560
           02 MDIMCAC   PIC X.                                          00002570
           02 MDIMCAP   PIC X.                                          00002580
           02 MDIMCAH   PIC X.                                          00002590
           02 MDIMCAV   PIC X.                                          00002600
           02 MDIMCAO   PIC X(22).                                      00002610
           02 FILLER    PIC X(2).                                       00002620
           02 MDIMCAPDA      PIC X.                                     00002630
           02 MDIMCAPDC PIC X.                                          00002640
           02 MDIMCAPDP PIC X.                                          00002650
           02 MDIMCAPDH PIC X.                                          00002660
           02 MDIMCAPDV PIC X.                                          00002670
           02 MDIMCAPDO      PIC X(4).                                  00002680
           02 MLIGNECAO OCCURS   14 TIMES .                             00002690
             03 DFHMS2 OCCURS   7 TIMES .                               00002700
               04 FILLER     PIC X(2).                                  00002710
               04 MCAA  PIC X.                                          00002720
               04 MCAC  PIC X.                                          00002730
               04 MCAP  PIC X.                                          00002740
               04 MCAH  PIC X.                                          00002750
               04 MCAV  PIC X.                                          00002760
               04 MCAO  PIC ------9.                                    00002770
           02 MLIGNCATO OCCURS   14 TIMES .                             00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MCACA   PIC X.                                          00002800
             03 MCACC   PIC X.                                          00002810
             03 MCACP   PIC X.                                          00002820
             03 MCACH   PIC X.                                          00002830
             03 MCACV   PIC X.                                          00002840
             03 MCACO   PIC ------9.                                    00002850
             03 FILLER       PIC X(2).                                  00002860
             03 MCAPDTA      PIC X.                                     00002870
             03 MCAPDTC PIC X.                                          00002880
             03 MCAPDTP PIC X.                                          00002890
             03 MCAPDTH PIC X.                                          00002900
             03 MCAPDTV PIC X.                                          00002910
             03 MCAPDTO      PIC Z9,9.                                  00002920
           02 DFHMS3 OCCURS   7 TIMES .                                 00002930
             03 FILLER       PIC X(2).                                  00002940
             03 MCATA   PIC X.                                          00002950
             03 MCATC   PIC X.                                          00002960
             03 MCATP   PIC X.                                          00002970
             03 MCATH   PIC X.                                          00002980
             03 MCATV   PIC X.                                          00002990
             03 MCATO   PIC ------9.                                    00003000
           02 FILLER    PIC X(2).                                       00003010
           02 MCACTA    PIC X.                                          00003020
           02 MCACTC    PIC X.                                          00003030
           02 MCACTP    PIC X.                                          00003040
           02 MCACTH    PIC X.                                          00003050
           02 MCACTV    PIC X.                                          00003060
           02 MCACTO    PIC ------9.                                    00003070
           02 FILLER    PIC X(2).                                       00003080
           02 MCAPDTTA  PIC X.                                          00003090
           02 MCAPDTTC  PIC X.                                          00003100
           02 MCAPDTTP  PIC X.                                          00003110
           02 MCAPDTTH  PIC X.                                          00003120
           02 MCAPDTTV  PIC X.                                          00003130
           02 MCAPDTTO  PIC ---9.                                       00003140
           02 DFHMS4 OCCURS   7 TIMES .                                 00003150
             03 FILLER       PIC X(2).                                  00003160
             03 MCAPDJA      PIC X.                                     00003170
             03 MCAPDJC PIC X.                                          00003180
             03 MCAPDJP PIC X.                                          00003190
             03 MCAPDJH PIC X.                                          00003200
             03 MCAPDJV PIC X.                                          00003210
             03 MCAPDJO      PIC Z9,9.                                  00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MLIBERRA  PIC X.                                          00003240
           02 MLIBERRC  PIC X.                                          00003250
           02 MLIBERRP  PIC X.                                          00003260
           02 MLIBERRH  PIC X.                                          00003270
           02 MLIBERRV  PIC X.                                          00003280
           02 MLIBERRO  PIC X(79).                                      00003290
           02 FILLER    PIC X(2).                                       00003300
           02 MLIBPFA   PIC X.                                          00003310
           02 MLIBPFC   PIC X.                                          00003320
           02 MLIBPFP   PIC X.                                          00003330
           02 MLIBPFH   PIC X.                                          00003340
           02 MLIBPFV   PIC X.                                          00003350
           02 MLIBPFO   PIC X(79).                                      00003360
      * CODE TRANSACTION                                                00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MCODTRAA  PIC X.                                          00003390
           02 MCODTRAC  PIC X.                                          00003400
           02 MCODTRAP  PIC X.                                          00003410
           02 MCODTRAH  PIC X.                                          00003420
           02 MCODTRAV  PIC X.                                          00003430
           02 MCODTRAO  PIC X(4).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MZONCMDA  PIC X.                                          00003460
           02 MZONCMDC  PIC X.                                          00003470
           02 MZONCMDP  PIC X.                                          00003480
           02 MZONCMDH  PIC X.                                          00003490
           02 MZONCMDV  PIC X.                                          00003500
           02 MZONCMDO  PIC X(15).                                      00003510
      * NOM DU CICS                                                     00003520
           02 FILLER    PIC X(2).                                       00003530
           02 MCICSA    PIC X.                                          00003540
           02 MCICSC    PIC X.                                          00003550
           02 MCICSP    PIC X.                                          00003560
           02 MCICSH    PIC X.                                          00003570
           02 MCICSV    PIC X.                                          00003580
           02 MCICSO    PIC X(5).                                       00003590
      * NETNAME                                                         00003600
           02 FILLER    PIC X(2).                                       00003610
           02 MNETNAMA  PIC X.                                          00003620
           02 MNETNAMC  PIC X.                                          00003630
           02 MNETNAMP  PIC X.                                          00003640
           02 MNETNAMH  PIC X.                                          00003650
           02 MNETNAMV  PIC X.                                          00003660
           02 MNETNAMO  PIC X(8).                                       00003670
      * CODE TERMINAL                                                   00003680
           02 FILLER    PIC X(2).                                       00003690
           02 MSCREENA  PIC X.                                          00003700
           02 MSCREENC  PIC X.                                          00003710
           02 MSCREENP  PIC X.                                          00003720
           02 MSCREENH  PIC X.                                          00003730
           02 MSCREENV  PIC X.                                          00003740
           02 MSCREENO  PIC X(5).                                       00003750
                                                                                
