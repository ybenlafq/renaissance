      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: ESE00   ESE00                                              00000020
      ***************************************************************** 00000030
       01   ESL30I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNCODICI  PIC X(7).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFFOURNL    COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MLREFFOURNL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLREFFOURNF    PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MLREFFOURNI    PIC X(20).                                 00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCFAML    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MCFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCFAMF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MCFAMI    PIC X(5).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMARQL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MCMARQL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MCMARQF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MCMARQI   PIC X(5).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTATUTL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MSTATUTL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSTATUTF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MSTATUTI  PIC X(3).                                       00000410
           02 MMSGENTD OCCURS   6 TIMES .                               00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MMSGENTL     COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MMSGENTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MMSGENTF     PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MMSGENTI     PIC X(7).                                  00000460
           02 MLIGNEI OCCURS   10 TIMES .                               00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MNLIEUI      PIC X(3).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLLIEUL      COMP PIC S9(4).                            00000520
      *--                                                                       
             03 MLLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLLIEUF      PIC X.                                     00000530
             03 FILLER  PIC X(4).                                       00000540
             03 MLLIEUI      PIC X(20).                                 00000550
             03 MQUANTITED OCCURS   6 TIMES .                           00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MQUANTITEL      COMP PIC S9(4).                       00000570
      *--                                                                       
               04 MQUANTITEL COMP-5 PIC S9(4).                                  
      *}                                                                        
               04 MQUANTITEF      PIC X.                                00000580
               04 FILLER     PIC X(4).                                  00000590
               04 MQUANTITEI      PIC X(7).                             00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MZONCMDI  PIC X(15).                                      00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000650
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000660
           02 FILLER    PIC X(4).                                       00000670
           02 MLIBERRI  PIC X(58).                                      00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000690
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MCODTRAI  PIC X(4).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MCICSI    PIC X(5).                                       00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000770
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000780
           02 FILLER    PIC X(4).                                       00000790
           02 MNETNAMI  PIC X(8).                                       00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000810
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000820
           02 FILLER    PIC X(4).                                       00000830
           02 MSCREENI  PIC X(4).                                       00000840
      ***************************************************************** 00000850
      * SDF: ESE00   ESE00                                              00000860
      ***************************************************************** 00000870
       01   ESL30O REDEFINES ESL30I.                                    00000880
           02 FILLER    PIC X(12).                                      00000890
           02 FILLER    PIC X(2).                                       00000900
           02 MDATJOUA  PIC X.                                          00000910
           02 MDATJOUC  PIC X.                                          00000920
           02 MDATJOUP  PIC X.                                          00000930
           02 MDATJOUH  PIC X.                                          00000940
           02 MDATJOUV  PIC X.                                          00000950
           02 MDATJOUO  PIC X(10).                                      00000960
           02 FILLER    PIC X(2).                                       00000970
           02 MTIMJOUA  PIC X.                                          00000980
           02 MTIMJOUC  PIC X.                                          00000990
           02 MTIMJOUP  PIC X.                                          00001000
           02 MTIMJOUH  PIC X.                                          00001010
           02 MTIMJOUV  PIC X.                                          00001020
           02 MTIMJOUO  PIC X(5).                                       00001030
           02 FILLER    PIC X(2).                                       00001040
           02 MPAGEA    PIC X.                                          00001050
           02 MPAGEC    PIC X.                                          00001060
           02 MPAGEP    PIC X.                                          00001070
           02 MPAGEH    PIC X.                                          00001080
           02 MPAGEV    PIC X.                                          00001090
           02 MPAGEO    PIC X(3).                                       00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MPAGEMAXA      PIC X.                                     00001120
           02 MPAGEMAXC PIC X.                                          00001130
           02 MPAGEMAXP PIC X.                                          00001140
           02 MPAGEMAXH PIC X.                                          00001150
           02 MPAGEMAXV PIC X.                                          00001160
           02 MPAGEMAXO      PIC X(3).                                  00001170
           02 FILLER    PIC X(2).                                       00001180
           02 MNCODICA  PIC X.                                          00001190
           02 MNCODICC  PIC X.                                          00001200
           02 MNCODICP  PIC X.                                          00001210
           02 MNCODICH  PIC X.                                          00001220
           02 MNCODICV  PIC X.                                          00001230
           02 MNCODICO  PIC X(7).                                       00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MLREFFOURNA    PIC X.                                     00001260
           02 MLREFFOURNC    PIC X.                                     00001270
           02 MLREFFOURNP    PIC X.                                     00001280
           02 MLREFFOURNH    PIC X.                                     00001290
           02 MLREFFOURNV    PIC X.                                     00001300
           02 MLREFFOURNO    PIC X(20).                                 00001310
           02 FILLER    PIC X(2).                                       00001320
           02 MCFAMA    PIC X.                                          00001330
           02 MCFAMC    PIC X.                                          00001340
           02 MCFAMP    PIC X.                                          00001350
           02 MCFAMH    PIC X.                                          00001360
           02 MCFAMV    PIC X.                                          00001370
           02 MCFAMO    PIC X(5).                                       00001380
           02 FILLER    PIC X(2).                                       00001390
           02 MCMARQA   PIC X.                                          00001400
           02 MCMARQC   PIC X.                                          00001410
           02 MCMARQP   PIC X.                                          00001420
           02 MCMARQH   PIC X.                                          00001430
           02 MCMARQV   PIC X.                                          00001440
           02 MCMARQO   PIC X(5).                                       00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MSTATUTA  PIC X.                                          00001470
           02 MSTATUTC  PIC X.                                          00001480
           02 MSTATUTP  PIC X.                                          00001490
           02 MSTATUTH  PIC X.                                          00001500
           02 MSTATUTV  PIC X.                                          00001510
           02 MSTATUTO  PIC X(3).                                       00001520
           02 DFHMS1 OCCURS   6 TIMES .                                 00001530
             03 FILLER       PIC X(2).                                  00001540
             03 MMSGENTA     PIC X.                                     00001550
             03 MMSGENTC     PIC X.                                     00001560
             03 MMSGENTP     PIC X.                                     00001570
             03 MMSGENTH     PIC X.                                     00001580
             03 MMSGENTV     PIC X.                                     00001590
             03 MMSGENTO     PIC X(7).                                  00001600
           02 MLIGNEO OCCURS   10 TIMES .                               00001610
             03 FILLER       PIC X(2).                                  00001620
             03 MNLIEUA      PIC X.                                     00001630
             03 MNLIEUC PIC X.                                          00001640
             03 MNLIEUP PIC X.                                          00001650
             03 MNLIEUH PIC X.                                          00001660
             03 MNLIEUV PIC X.                                          00001670
             03 MNLIEUO      PIC X(3).                                  00001680
             03 FILLER       PIC X(2).                                  00001690
             03 MLLIEUA      PIC X.                                     00001700
             03 MLLIEUC PIC X.                                          00001710
             03 MLLIEUP PIC X.                                          00001720
             03 MLLIEUH PIC X.                                          00001730
             03 MLLIEUV PIC X.                                          00001740
             03 MLLIEUO      PIC X(20).                                 00001750
             03 DFHMS2 OCCURS   6 TIMES .                               00001760
               04 FILLER     PIC X(2).                                  00001770
               04 MQUANTITEA      PIC X.                                00001780
               04 MQUANTITEC PIC X.                                     00001790
               04 MQUANTITEP PIC X.                                     00001800
               04 MQUANTITEH PIC X.                                     00001810
               04 MQUANTITEV PIC X.                                     00001820
               04 MQUANTITEO      PIC X(7).                             00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MZONCMDA  PIC X.                                          00001850
           02 MZONCMDC  PIC X.                                          00001860
           02 MZONCMDP  PIC X.                                          00001870
           02 MZONCMDH  PIC X.                                          00001880
           02 MZONCMDV  PIC X.                                          00001890
           02 MZONCMDO  PIC X(15).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MLIBERRA  PIC X.                                          00001920
           02 MLIBERRC  PIC X.                                          00001930
           02 MLIBERRP  PIC X.                                          00001940
           02 MLIBERRH  PIC X.                                          00001950
           02 MLIBERRV  PIC X.                                          00001960
           02 MLIBERRO  PIC X(58).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MCODTRAA  PIC X.                                          00001990
           02 MCODTRAC  PIC X.                                          00002000
           02 MCODTRAP  PIC X.                                          00002010
           02 MCODTRAH  PIC X.                                          00002020
           02 MCODTRAV  PIC X.                                          00002030
           02 MCODTRAO  PIC X(4).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MCICSA    PIC X.                                          00002060
           02 MCICSC    PIC X.                                          00002070
           02 MCICSP    PIC X.                                          00002080
           02 MCICSH    PIC X.                                          00002090
           02 MCICSV    PIC X.                                          00002100
           02 MCICSO    PIC X(5).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNETNAMA  PIC X.                                          00002130
           02 MNETNAMC  PIC X.                                          00002140
           02 MNETNAMP  PIC X.                                          00002150
           02 MNETNAMH  PIC X.                                          00002160
           02 MNETNAMV  PIC X.                                          00002170
           02 MNETNAMO  PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MSCREENA  PIC X.                                          00002200
           02 MSCREENC  PIC X.                                          00002210
           02 MSCREENP  PIC X.                                          00002220
           02 MSCREENH  PIC X.                                          00002230
           02 MSCREENV  PIC X.                                          00002240
           02 MSCREENO  PIC X(4).                                       00002250
                                                                                
