      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010002
      *   COPY DE LA TABLE RVVT1100                                     00020005
      **********************************************************        00030002
      *                                                                 00040002
      *---------------------------------------------------------        00050002
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1100                 00060005
      *---------------------------------------------------------        00070002
      *                                                                 00080002
       01  RVVT1100.                                                    00090005
           02  VT11-NFLAG                                               00100005
               PIC X(0001).                                             00110005
           02  VT11-NCLUSTER                                            00120005
               PIC X(0013).                                             00130005
           02  VT11-NSOCIETE                                            00140005
               PIC X(0003).                                             00150005
           02  VT11-NLIGNE                                              00160005
               PIC X(0003).                                             00170002
           02  VT11-NCODIC                                              00180005
               PIC X(0007).                                             00190002
           02  VT11-CMODDEL                                             00200005
               PIC X(0003).                                             00210002
           02  VT11-DDELIV                                              00220005
               PIC X(0008).                                             00230002
           02  VT11-QVENDUE                                             00240005
               PIC S9(5) COMP-3.                                        00250002
           02  VT11-PVUNIT                                              00260005
               PIC S9(7)V9(0002) COMP-3.                                00270002
           02  VT11-PVUNITF                                             00280005
               PIC S9(7)V9(0002) COMP-3.                                00290002
           02  VT11-TAUXTVA                                             00300005
               PIC S9(3)V9(0002) COMP-3.                                00310002
           02  VT11-QCONDT                                              00320005
               PIC S9(5) COMP-3.                                        00330002
           02  VT11-CANNULREP                                           00340005
               PIC X(0001).                                             00350002
           02  VT11-CVENDEUR                                            00360005
               PIC X(0007).                                             00370002
           02  VT11-NAUTORM                                             00370106
               PIC X(0005).                                             00370206
           02  VT11-NSEQNQ                                              00371006
               PIC S9(5) COMP-3.                                        00372006
           02  VT11-NSEQREF                                             00373006
               PIC S9(5) COMP-3.                                        00374006
           02  VT11-NCODICGRP                                           00380005
               PIC X(0007).                                             00390002
           02  VT11-CTYPENT                                             00391007
               PIC X(0002).                                             00392007
           02  VT11-NLIEN                                               00393006
               PIC S9(5) COMP-3.                                        00394006
           02  VT11-NACTVTE                                             00395006
               PIC S9(5) COMP-3.                                        00396006
           02  VT11-QVENDUEGRP                                          00400005
               PIC S9(5) COMP-3.                                        00410003
           02  VT11-PVUNITGRP                                           00420005
               PIC S9(7)V9(0002) COMP-3.                                00430002
           02  VT11-NSEQENS                                             00431006
               PIC S9(5) COMP-3.                                        00432006
           02  VT11-MPRIMECLI                                           00440006
               PIC S9(7)V9(0002) COMP-3.                                00450002
      *                                                                 00460002
      *---------------------------------------------------------        00470002
      *   LISTE DES FLAGS DE LA TABLE RVVT1100                          00480005
      *---------------------------------------------------------        00490002
      *                                                                 00500002
       01  RVVT1100-FLAGS.                                              00510005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NFLAG-F                                             00520005
      *        PIC S9(4) COMP.                                          00530002
      *--                                                                       
           02  VT11-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NCLUSTER-F                                          00540005
      *        PIC S9(4) COMP.                                          00550002
      *--                                                                       
           02  VT11-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NSOCIETE-F                                          00560005
      *        PIC S9(4) COMP.                                          00570005
      *--                                                                       
           02  VT11-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NLIGNE-F                                            00580005
      *        PIC S9(4) COMP.                                          00590002
      *--                                                                       
           02  VT11-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NCODIC-F                                            00600005
      *        PIC S9(4) COMP.                                          00610002
      *--                                                                       
           02  VT11-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-CMODDEL-F                                           00620005
      *        PIC S9(4) COMP.                                          00630002
      *--                                                                       
           02  VT11-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-DDELIV-F                                            00640005
      *        PIC S9(4) COMP.                                          00650002
      *--                                                                       
           02  VT11-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-QVENDUE-F                                           00660005
      *        PIC S9(4) COMP.                                          00670002
      *--                                                                       
           02  VT11-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-PVUNIT-F                                            00680005
      *        PIC S9(4) COMP.                                          00690002
      *--                                                                       
           02  VT11-PVUNIT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-PVUNITF-F                                           00700005
      *        PIC S9(4) COMP.                                          00710002
      *--                                                                       
           02  VT11-PVUNITF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-TAUXTVA-F                                           00720005
      *        PIC S9(4) COMP.                                          00730002
      *--                                                                       
           02  VT11-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-QCONDT-F                                            00740005
      *        PIC S9(4) COMP.                                          00750002
      *--                                                                       
           02  VT11-QCONDT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-CANNULREP-F                                         00760005
      *        PIC S9(4) COMP.                                          00770002
      *--                                                                       
           02  VT11-CANNULREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-CVENDEUR-F                                          00780005
      *        PIC S9(4) COMP.                                          00790002
      *--                                                                       
           02  VT11-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NAUTORM-F                                           00791006
      *        PIC S9(4) COMP.                                          00792006
      *--                                                                       
           02  VT11-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NSEQNQ-F                                            00793006
      *        PIC S9(4) COMP.                                          00794006
      *--                                                                       
           02  VT11-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NSEQREF-F                                           00795006
      *        PIC S9(4) COMP.                                          00796006
      *--                                                                       
           02  VT11-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NCODICGRP-F                                         00800005
      *        PIC S9(4) COMP.                                          00810002
      *--                                                                       
           02  VT11-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-CTYPENT-F                                           00811007
      *        PIC S9(4) COMP.                                          00812006
      *--                                                                       
           02  VT11-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NLIEN-F                                             00813006
      *        PIC S9(4) COMP.                                          00814006
      *--                                                                       
           02  VT11-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NACTVTE-F                                           00815006
      *        PIC S9(4) COMP.                                          00816006
      *--                                                                       
           02  VT11-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-QVENDUEGRP-F                                        00820005
      *        PIC S9(4) COMP.                                          00830003
      *--                                                                       
           02  VT11-QVENDUEGRP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-PVUNITGRP-F                                         00840005
      *        PIC S9(4) COMP.                                          00850002
      *--                                                                       
           02  VT11-PVUNITGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-NSEQENS-F                                           00860006
      *        PIC S9(4) COMP.                                          00870002
      *--                                                                       
           02  VT11-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT11-MPRIMECLI-F                                         00871006
      *        PIC S9(4) COMP.                                          00872006
      *--                                                                       
           02  VT11-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00880002
                                                                                
