      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : VENTES EXPEDIEES                                 *        
      *  TRANSACTION: GV  EC  ...                                      *        
      *  TITRE      : COMMAREA DE PASSAGE VERS LE MODULE MGV64         *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00530000
      *                                                                 00230000
       01  COMM-MGV64-APPLI.                                            00260000
         02 COMM-MGV64-ENTREE.                                          00260000
           05 COMM-MGV64-CPROG          PIC X(05).                              
           05 COMM-MGV64-NSOCIETE       PIC X(03).                      00320000
           05 COMM-MGV64-NLIEU          PIC X(03).                      00330000
           05 COMM-MGV64-NVENTE         PIC X(07).                      00340000
           05 COMM-MGV64-DJOUR          PIC X(08).                      00340000
           05 COMM-MGV64-NCODIC         PIC X(07).                      00340000
      *    05 COMM-MGV64-NCODICGRP      PIC X(07).                      00340000
           05 COMM-MGV64-NSEQNQ         PIC 9(05).                      00340000
      *    05 COMM-MGV64-CTYPENREG      PIC X(01).                      00340000
           05 COMM-MGV64-QTE            PIC 9(05).                      00340000
           05 COMM-MGV64-FILIERE        PIC X(02).                      00340000
           05 COMM-MGV64-DATE-DACEM     PIC X(08).                      00340000
           05 COMM-MGV64-NUM-DACEM      PIC X(07).                      00340000
           05 FILLER                    PIC X(33).                      00340000
      * DDELIV ENVOYE AU MGV65                                                  
           05 COMM-MGV64-DDELIV         PIC X(08).                      00340000
      * DDELIV ENVOYE PAR LE TEC02 LORS DE LA MAJ-DDELIV                        
V43R0      05 COMM-MGV64-DATE-MAJ       PIC X(08).                      00340000
           05 COMM-MGV64-ACTION         PIC X(01) VALUE '0'.            00340000
             88 COMM-MGV64-ACTION-CONTROLE      VALUE '0'.              00340000
             88 COMM-MGV64-ACTION-VALIDATION    VALUE '1'.              00340000
             88 COMM-MGV64-ACTION-VALID-DATE    VALUE '2'.              00340000
             88 COMM-MGV64-ACTION-MAJ-DDELIV    VALUE '3'.              00340000
             88 COMM-MGV64-ACTION-CONTROLE-MAJ  VALUE '4'.              00340000
             88 COMM-MGV64-ACTION-SET-INCOM     VALUE '5'.              00340000
             88 COMM-MGV64-ACTION-SORTIE        VALUE '6'.              00340000
V52R0        88 COMM-MGV64-ACTION-VALID-CODIC   VALUE '7'.              00340000
             88 COMM-MGV64-ACTION-VALID-EC06    VALUE '8'.                      
             88 COMM-MGV64-ACTION-VALID-CF      VALUE '9'.                      
      *    05 COMM-MGV64-MUT-SPE        PIC X(01).                      00340000
      *    05 FILLER                    PIC X(099).                     00340000
      * RETOUR                                                                  
         02 COMM-MGV64-SORTIE.                                          00260000
           05 COMM-MGV64-TYPE-NB        PIC X(01) VALUE '0'.            00340000
             88 COMM-MGV64-TYPE-NB-MONO      VALUE '0'.                 00340000
             88 COMM-MGV64-TYPE-NB-MULTI     VALUE '1'.                 00340000
             88 COMM-MGV64-TYPE-ANNUL        VALUE '9'.                         
           05 COMM-MGV64-CODE-RETOUR    PIC X(04).                      00340000
           05 COMM-MGV64-MESSAGE        PIC X(58).                              
           05 COMM-GV64-ETAT-LIGNES     PIC 9(01) VALUE 0.                      
              88 COMM-GV64-LIGNE-A-TRAITER        VALUE 1.                      
           05 COMM-MGV64-STATUT         PIC 9(01).                              
              88 GV64-STATUT-COMPLETE           VALUE 0.                        
      *{ remove-comma-in-dde 1.5                                                
      *       88 GV64-STATUT-INCOMPLETE         VALUE 1, 2 , 3 , 4              
      *                                         5 , 6.                          
      *--                                                                       
              88 GV64-STATUT-INCOMPLETE         VALUE 1  2   3   4              
                                                5   6.                          
      *}                                                                        
              88 GV64-STATUT-ATTENTE-FOURN      VALUE 1.                        
              88 GV64-STATUT-PB-DISPO           VALUE 2.                        
              88 GV64-STATUT-ANO                VALUE 3.                        
              88 GV64-STATUT-NON-TRAITE         VALUE 4.                        
V43R0         88 GV64-STATUT-CQE-DACEM          VALUE 5.                        
V43R0         88 GV64-STATUT-DACEM-FERME        VALUE 6.                        
              88 GV64-STATUT-ERREUR-GRAVE       VALUE 9.                        
           05 COMM-MGV64-STATUT-PREC    PIC 9(01).                              
              88 GV64-STATUT-PREC-COMPLETE      VALUE 1.                        
              88 GV64-STATUT-PREC-INCOMPLETE    VALUE 0.                        
              88 GV64-STATUT-PREC-NON-DEFINI    VALUE 2.                        
V43R0      05 COMM-MGV64-STATUT-CQE-PREC PIC 9(01).                             
V43R0         88 GV64-VTE-AVEC-CQE-PREC         VALUE 1.                        
V43R0         88 GV64-VTE-SANS-CQE-PREC         VALUE 0.                        
V43R0      05 COMM-MGV64-STATUT-CQE-NEW  PIC 9(01).                             
V43R0         88 GV64-VTE-AVEC-CQE-NEW          VALUE 1.                        
V43R0         88 GV64-VTE-SANS-CQE-NEW          VALUE 0.                        
      * CONTIENT LA DDELIV MAX DES ARTICLES DE LA VENTE QUI DEVIENDRA           
      * LA DDELIV COMMUNE LORS DE LA MISE EN INCOMPLETE                         
           05 COMM-MGV64-DDELIV-COM     PIC X(8).                               
           05 COMM-MGV64-TABLEAU-NLIGNE.                                        
               06 COMM-MGV64-TAB-NLIGNE OCCURS 80.                              
                  10 COMM-MGV64-TAB-NCODIC         PIC X(7).                    
                  10 COMM-MGV64-TAB-NCODICGRP      PIC X(7).                    
                  10 COMM-MGV64-TAB-CTYPENREG      PIC X(1).                    
                  10 COMM-MGV64-TAB-NSEQ           PIC 9(2).                    
                  10 COMM-MGV64-TAB-NSEQNQ         PIC S9(5)  COMP-3.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *           10 COMM-MGV64-TAB-QVENDUE        PIC S9(06) COMP.             
      *--                                                                       
                  10 COMM-MGV64-TAB-QVENDUE        PIC S9(06) COMP-5.           
      *}                                                                        
      * GV11-DDELIV                                                             
                  10 COMM-MGV64-TAB-DDELIVO PIC X(8).                           
V43R0 *           10 COMM-MGV64-TAB-DDELIVN PIC X(8).                           
                  10 COMM-MGV64-TAB-LCOMMENT PIC X(35).                         
                  10 COMM-MGV64-TAB-WCQERESF PIC X(1).                          
                     88 GV64-ARTICLE-EN-CF           VALUE 'F'.                 
                     88 GV64-ARTICLE-PAS-EN-CF       VALUE ' '.                 
                  10 COMM-MGV64-TAB-WEMPORTE PIC X(1).                          
                  10 COMM-MGV64-TAB-NMUT     PIC X(7).                          
                  10 COMM-MGV64-TAB-NMUTTEMP PIC X(7).                          
                  10 COMM-MGV64-TAB-CSTATUT  PIC X(1).                          
      *{ remove-comma-in-dde 1.5                                                
      *              88 GV64-ARTICLE-EN-ATTENTE      VALUE 'A','F'.             
      *--                                                                       
                     88 GV64-ARTICLE-EN-ATTENTE      VALUE 'A' 'F'.             
      *}                                                                        
                     88 GV64-ARTICLE-RESERVE         VALUE 'R'.                 
      * -> ETAT COMMANDE FOURNISSEUR AVANT ENCAISSEMENT                         
                     88 GV64-ARTICLE-CDF-AVENC       VALUE 'F'.                 
                     88 GV64-ARTICLE-MUTE            VALUE 'M'.                 
                     88 GV64-CSTATUT-A-BLANC         VALUE ' '.                 
                  10 COMM-MGV64-TAB-CEQUIP       PIC X(5).                      
                  10 COMM-MGV64-TAB-ETAT-RESA    PIC 9(01).                     
                     88 GV64-AUCUN-STOCK-DISPO       VALUE 0.                   
                     88 GV64-STOCK-DISPONIBLE        VALUE 1.                   
                     88 GV64-STOCK-DACEM             VALUE 2.                   
                     88 GV64-STOCK-ATT-FOURN         VALUE 3.                   
V43R0                88 GV64-STOCK-CQE-DACEM         VALUE 5.                   
V43R0                88 GV64-DACEM-FERME             VALUE 6.                   
V43R0                88 GV64-DEJA-MUTE               VALUE 7.                   
                     88 GV64-STOCK-ERREUR            VALUE 9.                   
                  10 COMM-MGV64-TAB-ETAT-FOURN PIC 9(01).                       
                     88 GV64-STOCK-FOURN-NON         VALUE 0.                   
                     88 GV64-STOCK-FOURN-OUI         VALUE 1.                   
                  10 COMM-MGV64-TAB-NDEPOT        PIC X(03).                    
           05 COMM-MGV64-NORDRE         PIC X(05).                      00340000
V50R0      05 GV64-NB-DACEM-TRT         PIC 9(05).                      00340000
V50R0      05 GV64-NB-DARTY90-TRT       PIC 9(05).                      00340000
V50R0      05 GV64-NB-DARTY95-TRT       PIC 9(05).                      00340000
V50R1      05 GV64-NB-INDISPO           PIC 9(02).                      00340000
V52R0      05 GV64-MULTIDA              PIC X(01).                      00340000
           05 GV64-VENTE-CQE            PIC X(01).                              
V52R0 *  02 FILLER                    PIC X(513).                       00340000
         02 FILLER                    PIC X(322).                               
      *  02 FILLER                    PIC X(005).                       00340000
                                                                                
