      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *********************************************************                 
      *   COPY DE LA TABLE RVBA0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVBA0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA0000.                                                            
           02  BA00-NFACTBA                                                     
               PIC X(0010).                                                     
           02  BA00-WTYPFACT                                                    
               PIC X(0001).                                                     
           02  BA00-WANNUL                                                      
               PIC X(0001).                                                     
           02  BA00-NFACTREM                                                    
               PIC X(0010).                                                     
           02  BA00-WELIBELLE                                                   
               PIC X(0001).                                                     
           02  BA00-NSIGNAT                                                     
               PIC X(0001).                                                     
           02  BA00-PFACTURE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  BA00-PREMISE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  BA00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVBA0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-NFACTBA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-NFACTBA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-WTYPFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-WTYPFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-WANNUL-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-WANNUL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-NFACTREM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-NFACTREM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-WELIBELLE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-WELIBELLE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-NSIGNAT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-NSIGNAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-PFACTURE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-PFACTURE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-PREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-PREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
