      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * INIT DES BR (LORS DU DEMARRAGE)                                 00000020
      ***************************************************************** 00000030
       01   ECL01I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEL    COMP PIC S9(4).                                 00000160
      *--                                                                       
           02 MPAGEL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MPAGEF    PIC X.                                          00000170
           02 FILLER    PIC X(4).                                       00000180
           02 MPAGEI    PIC X(2).                                       00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGESL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNBPAGESL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNBPAGESF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNBPAGESI      PIC X(2).                                  00000230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 CHOIXL    COMP PIC S9(4).                                 00000240
      *--                                                                       
           02 CHOIXL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 CHOIXF    PIC X.                                          00000250
           02 FILLER    PIC X(4).                                       00000260
           02 CHOIXI    PIC X(5).                                       00000270
           02 MLIGNESI OCCURS   14 TIMES .                              00000280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSSLIEUL     COMP PIC S9(4).                            00000290
      *--                                                                       
             03 MSSLIEUL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSSLIEUF     PIC X.                                     00000300
             03 FILLER  PIC X(4).                                       00000310
             03 MSSLIEUI     PIC X(5).                                  00000320
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBELL      COMP PIC S9(4).                            00000330
      *--                                                                       
             03 MLIBELL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLIBELF      PIC X.                                     00000340
             03 FILLER  PIC X(4).                                       00000350
             03 MLIBELI      PIC X(19).                                 00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALOL  COMP PIC S9(4).                                 00000370
      *--                                                                       
             03 MVALOL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MVALOF  PIC X.                                          00000380
             03 FILLER  PIC X(4).                                       00000390
             03 MVALOI  PIC X.                                          00000400
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRESL  COMP PIC S9(4).                                 00000410
      *--                                                                       
             03 MPRESL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRESF  PIC X.                                          00000420
             03 FILLER  PIC X(4).                                       00000430
             03 MPRESI  PIC X.                                          00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVNDL   COMP PIC S9(4).                                 00000450
      *--                                                                       
             03 MVNDL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MVNDF   PIC X.                                          00000460
             03 FILLER  PIC X(4).                                       00000470
             03 MVNDI   PIC X.                                          00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHSL    COMP PIC S9(4).                                 00000490
      *--                                                                       
             03 MHSL COMP-5 PIC S9(4).                                          
      *}                                                                        
             03 MHSF    PIC X.                                          00000500
             03 FILLER  PIC X(4).                                       00000510
             03 MHSI    PIC X.                                          00000520
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPRETL  COMP PIC S9(4).                                 00000530
      *--                                                                       
             03 MPRETL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MPRETF  PIC X.                                          00000540
             03 FILLER  PIC X(4).                                       00000550
             03 MPRETI  PIC X.                                          00000560
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTRL   COMP PIC S9(4).                                 00000570
      *--                                                                       
             03 MCTRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MCTRF   PIC X.                                          00000580
             03 FILLER  PIC X(4).                                       00000590
             03 MCTRI   PIC X.                                          00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MATRL   COMP PIC S9(4).                                 00000610
      *--                                                                       
             03 MATRL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MATRF   PIC X.                                          00000620
             03 FILLER  PIC X(4).                                       00000630
             03 MATRI   PIC X.                                          00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MAUXL   COMP PIC S9(4).                                 00000650
      *--                                                                       
             03 MAUXL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MAUXF   PIC X.                                          00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MAUXI   PIC X.                                          00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTRANSL      COMP PIC S9(4).                            00000690
      *--                                                                       
             03 MTRANSL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTRANSF      PIC X.                                     00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MTRANSI      PIC X.                                     00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDISPOL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MDISPOL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDISPOF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MDISPOI      PIC X.                                     00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSEQL   COMP PIC S9(4).                                 00000770
      *--                                                                       
             03 MSEQL COMP-5 PIC S9(4).                                         
      *}                                                                        
             03 MSEQF   PIC X.                                          00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MSEQI   PIC X(3).                                       00000800
      * MESSAGE ERREUR                                                  00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(79).                                      00000850
      * CODE TRANSACTION                                                00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      * CICS DE TRAVAIL                                                 00000950
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000960
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000970
           02 FILLER    PIC X(4).                                       00000980
           02 MCICSI    PIC X(5).                                       00000990
      * NETNAME                                                         00001000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001010
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001020
           02 FILLER    PIC X(4).                                       00001030
           02 MNETNAMI  PIC X(8).                                       00001040
      * CODE TERMINAL                                                   00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MSCREENI  PIC X(4).                                       00001090
      ***************************************************************** 00001100
      * INIT DES BR (LORS DU DEMARRAGE)                                 00001110
      ***************************************************************** 00001120
       01   ECL01O REDEFINES ECL01I.                                    00001130
           02 FILLER    PIC X(12).                                      00001140
      * DATE DU JOUR                                                    00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MDATJOUA  PIC X.                                          00001170
           02 MDATJOUC  PIC X.                                          00001180
           02 MDATJOUP  PIC X.                                          00001190
           02 MDATJOUH  PIC X.                                          00001200
           02 MDATJOUV  PIC X.                                          00001210
           02 MDATJOUO  PIC X(10).                                      00001220
      * HEURE                                                           00001230
           02 FILLER    PIC X(2).                                       00001240
           02 MTIMJOUA  PIC X.                                          00001250
           02 MTIMJOUC  PIC X.                                          00001260
           02 MTIMJOUP  PIC X.                                          00001270
           02 MTIMJOUH  PIC X.                                          00001280
           02 MTIMJOUV  PIC X.                                          00001290
           02 MTIMJOUO  PIC X(5).                                       00001300
           02 FILLER    PIC X(2).                                       00001310
           02 MPAGEA    PIC X.                                          00001320
           02 MPAGEC    PIC X.                                          00001330
           02 MPAGEP    PIC X.                                          00001340
           02 MPAGEH    PIC X.                                          00001350
           02 MPAGEV    PIC X.                                          00001360
           02 MPAGEO    PIC X(2).                                       00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MNBPAGESA      PIC X.                                     00001390
           02 MNBPAGESC PIC X.                                          00001400
           02 MNBPAGESP PIC X.                                          00001410
           02 MNBPAGESH PIC X.                                          00001420
           02 MNBPAGESV PIC X.                                          00001430
           02 MNBPAGESO      PIC X(2).                                  00001440
           02 FILLER    PIC X(2).                                       00001450
           02 CHOIXA    PIC X.                                          00001460
           02 CHOIXC    PIC X.                                          00001470
           02 CHOIXP    PIC X.                                          00001480
           02 CHOIXH    PIC X.                                          00001490
           02 CHOIXV    PIC X.                                          00001500
           02 CHOIXO    PIC X(5).                                       00001510
           02 MLIGNESO OCCURS   14 TIMES .                              00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MSSLIEUA     PIC X.                                     00001540
             03 MSSLIEUC     PIC X.                                     00001550
             03 MSSLIEUP     PIC X.                                     00001560
             03 MSSLIEUH     PIC X.                                     00001570
             03 MSSLIEUV     PIC X.                                     00001580
             03 MSSLIEUO     PIC X(5).                                  00001590
             03 FILLER       PIC X(2).                                  00001600
             03 MLIBELA      PIC X.                                     00001610
             03 MLIBELC PIC X.                                          00001620
             03 MLIBELP PIC X.                                          00001630
             03 MLIBELH PIC X.                                          00001640
             03 MLIBELV PIC X.                                          00001650
             03 MLIBELO      PIC X(19).                                 00001660
             03 FILLER       PIC X(2).                                  00001670
             03 MVALOA  PIC X.                                          00001680
             03 MVALOC  PIC X.                                          00001690
             03 MVALOP  PIC X.                                          00001700
             03 MVALOH  PIC X.                                          00001710
             03 MVALOV  PIC X.                                          00001720
             03 MVALOO  PIC X.                                          00001730
             03 FILLER       PIC X(2).                                  00001740
             03 MPRESA  PIC X.                                          00001750
             03 MPRESC  PIC X.                                          00001760
             03 MPRESP  PIC X.                                          00001770
             03 MPRESH  PIC X.                                          00001780
             03 MPRESV  PIC X.                                          00001790
             03 MPRESO  PIC X.                                          00001800
             03 FILLER       PIC X(2).                                  00001810
             03 MVNDA   PIC X.                                          00001820
             03 MVNDC   PIC X.                                          00001830
             03 MVNDP   PIC X.                                          00001840
             03 MVNDH   PIC X.                                          00001850
             03 MVNDV   PIC X.                                          00001860
             03 MVNDO   PIC X.                                          00001870
             03 FILLER       PIC X(2).                                  00001880
             03 MHSA    PIC X.                                          00001890
             03 MHSC    PIC X.                                          00001900
             03 MHSP    PIC X.                                          00001910
             03 MHSH    PIC X.                                          00001920
             03 MHSV    PIC X.                                          00001930
             03 MHSO    PIC X.                                          00001940
             03 FILLER       PIC X(2).                                  00001950
             03 MPRETA  PIC X.                                          00001960
             03 MPRETC  PIC X.                                          00001970
             03 MPRETP  PIC X.                                          00001980
             03 MPRETH  PIC X.                                          00001990
             03 MPRETV  PIC X.                                          00002000
             03 MPRETO  PIC X.                                          00002010
             03 FILLER       PIC X(2).                                  00002020
             03 MCTRA   PIC X.                                          00002030
             03 MCTRC   PIC X.                                          00002040
             03 MCTRP   PIC X.                                          00002050
             03 MCTRH   PIC X.                                          00002060
             03 MCTRV   PIC X.                                          00002070
             03 MCTRO   PIC X.                                          00002080
             03 FILLER       PIC X(2).                                  00002090
             03 MATRA   PIC X.                                          00002100
             03 MATRC   PIC X.                                          00002110
             03 MATRP   PIC X.                                          00002120
             03 MATRH   PIC X.                                          00002130
             03 MATRV   PIC X.                                          00002140
             03 MATRO   PIC X.                                          00002150
             03 FILLER       PIC X(2).                                  00002160
             03 MAUXA   PIC X.                                          00002170
             03 MAUXC   PIC X.                                          00002180
             03 MAUXP   PIC X.                                          00002190
             03 MAUXH   PIC X.                                          00002200
             03 MAUXV   PIC X.                                          00002210
             03 MAUXO   PIC X.                                          00002220
             03 FILLER       PIC X(2).                                  00002230
             03 MTRANSA      PIC X.                                     00002240
             03 MTRANSC PIC X.                                          00002250
             03 MTRANSP PIC X.                                          00002260
             03 MTRANSH PIC X.                                          00002270
             03 MTRANSV PIC X.                                          00002280
             03 MTRANSO      PIC X.                                     00002290
             03 FILLER       PIC X(2).                                  00002300
             03 MDISPOA      PIC X.                                     00002310
             03 MDISPOC PIC X.                                          00002320
             03 MDISPOP PIC X.                                          00002330
             03 MDISPOH PIC X.                                          00002340
             03 MDISPOV PIC X.                                          00002350
             03 MDISPOO      PIC X.                                     00002360
             03 FILLER       PIC X(2).                                  00002370
             03 MSEQA   PIC X.                                          00002380
             03 MSEQC   PIC X.                                          00002390
             03 MSEQP   PIC X.                                          00002400
             03 MSEQH   PIC X.                                          00002410
             03 MSEQV   PIC X.                                          00002420
             03 MSEQO   PIC X(3).                                       00002430
      * MESSAGE ERREUR                                                  00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MLIBERRA  PIC X.                                          00002460
           02 MLIBERRC  PIC X.                                          00002470
           02 MLIBERRP  PIC X.                                          00002480
           02 MLIBERRH  PIC X.                                          00002490
           02 MLIBERRV  PIC X.                                          00002500
           02 MLIBERRO  PIC X(79).                                      00002510
      * CODE TRANSACTION                                                00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCODTRAA  PIC X.                                          00002540
           02 MCODTRAC  PIC X.                                          00002550
           02 MCODTRAP  PIC X.                                          00002560
           02 MCODTRAH  PIC X.                                          00002570
           02 MCODTRAV  PIC X.                                          00002580
           02 MCODTRAO  PIC X(4).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MZONCMDA  PIC X.                                          00002610
           02 MZONCMDC  PIC X.                                          00002620
           02 MZONCMDP  PIC X.                                          00002630
           02 MZONCMDH  PIC X.                                          00002640
           02 MZONCMDV  PIC X.                                          00002650
           02 MZONCMDO  PIC X(15).                                      00002660
      * CICS DE TRAVAIL                                                 00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MCICSA    PIC X.                                          00002690
           02 MCICSC    PIC X.                                          00002700
           02 MCICSP    PIC X.                                          00002710
           02 MCICSH    PIC X.                                          00002720
           02 MCICSV    PIC X.                                          00002730
           02 MCICSO    PIC X(5).                                       00002740
      * NETNAME                                                         00002750
           02 FILLER    PIC X(2).                                       00002760
           02 MNETNAMA  PIC X.                                          00002770
           02 MNETNAMC  PIC X.                                          00002780
           02 MNETNAMP  PIC X.                                          00002790
           02 MNETNAMH  PIC X.                                          00002800
           02 MNETNAMV  PIC X.                                          00002810
           02 MNETNAMO  PIC X(8).                                       00002820
      * CODE TERMINAL                                                   00002830
           02 FILLER    PIC X(2).                                       00002840
           02 MSCREENA  PIC X.                                          00002850
           02 MSCREENC  PIC X.                                          00002860
           02 MSCREENP  PIC X.                                          00002870
           02 MSCREENH  PIC X.                                          00002880
           02 MSCREENV  PIC X.                                          00002890
           02 MSCREENO  PIC X(4).                                       00002900
                                                                                
