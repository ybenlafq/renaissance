      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            DSECT DU FICHIER D'EXTRACTION DE L'ETAT IRE008      *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,BI,A,                          *        
      *                           18,03,PD,A,                          *        
      *                           21,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IRE008.                                                        
            05 NOMETAT-IRE008           PIC X(6) VALUE 'IRE008'.                
            05 RUPTURES-IRE008.                                                 
           10 IRE008-NSOCIETE           PIC X(03).                      007  003
           10 IRE008-BLANC-BRUN         PIC X(05).                      010  005
           10 IRE008-TLM-ELA            PIC X(03).                      015  003
           10 IRE008-WSEQED             PIC S9(05)      COMP-3.         018  003
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IRE008-SEQUENCE           PIC S9(04) COMP.                021  002
      *--                                                                       
           10 IRE008-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IRE008.                                                   
           10 IRE008-LMOISENC           PIC X(09).                      023  009
           10 IRE008-NRAYON             PIC X(15).                      032  015
           10 IRE008-CAA                PIC S9(13)V9(2) COMP-3.         047  008
           10 IRE008-CAM                PIC S9(13)V9(2) COMP-3.         055  008
           10 IRE008-FORCA              PIC S9(13)V9(2) COMP-3.         063  008
           10 IRE008-FORCM              PIC S9(13)V9(2) COMP-3.         071  008
           10 IRE008-PRMPA              PIC S9(13)V9(2) COMP-3.         079  008
           10 IRE008-PRMPM              PIC S9(13)V9(2) COMP-3.         087  008
           10 IRE008-REMA               PIC S9(13)V9(2) COMP-3.         095  008
           10 IRE008-REMM               PIC S9(13)V9(2) COMP-3.         103  008
            05 FILLER                      PIC X(402).                          
                                                                                
