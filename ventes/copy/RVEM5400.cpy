      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEM5400                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEM5400                         
      *   CLE = NSOCIETE A NLIGNE   LG = 88                                     
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5400.                                                            
      *}                                                                        
1          02  EM54-NSOCIETE                                                    
               PIC X(0003).                                                     
4          02  EM54-NLIEU                                                       
               PIC X(0003).                                                     
7          02  EM54-DCAISSE                                                     
               PIC X(0008).                                                     
15         02  EM54-NCAISSE                                                     
               PIC X(0003).                                                     
18         02  EM54-NTRANS                                                      
               PIC X(0008).                                                     
26         02  EM54-NLIGNE                                                      
               PIC X(0003).                                                     
29         02  EM54-DHVENTE                                                     
               PIC X(0002).                                                     
31         02  EM54-DMVENTE                                                     
               PIC X(0002).                                                     
33         02  EM54-NTYPETRANS                                                  
               PIC X(0003).                                                     
36         02  EM54-CANNULATION                                                 
               PIC X(0001).                                                     
37         02  EM54-NTRANANUL                                                   
               PIC X(0008).                                                     
45         02  EM54-NOPERATEUR                                                  
               PIC X(0007).                                                     
52         02  EM54-CMODPAIMT                                                   
               PIC X(0005).                                                     
57         02  EM54-NCAISC                                                      
               PIC X(0003).                                                     
60         02  EM54-NTRCPT                                                      
               PIC X(0008).                                                     
68         02  EM54-NENVLP                                                      
               PIC X(0010).                                                     
78         02  EM54-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
83         02  EM54-PMTENV                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
88         02  EM54-DEVISOPE                                                    
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEM5401                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEM5401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEM5401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NLIGNE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NTYPETRANS-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NTYPETRANS-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-CANNULATION-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-CANNULATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NTRANANUL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NTRANANUL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NCAISC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NCAISC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NTRCPT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NTRCPT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-NENVLP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-NENVLP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-PMTENV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-PMTENV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EM54-DEVISOPE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EM54-DEVISOPE-F                                                  
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
