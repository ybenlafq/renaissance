      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVSL0700                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVSL0700                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL0700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL0700.                                                            
      *}                                                                        
           10 SL07-NSOCIETE        PIC X(3).                                    
           10 SL07-NLIEU           PIC X(3).                                    
           10 SL07-CEMPL           PIC X(5).                                    
           10 SL07-CAUX            PIC X(6).                                    
           10 SL07-CTIERS          PIC X(6).                                    
           10 SL07-NCODIC          PIC X(7).                                    
           10 SL07-CETAT           PIC X(2).                                    
           10 SL07-QSTK            PIC S9(7) COMP-3.                            
           10 SL07-QSTR            PIC S9(7) COMP-3.                            
           10 SL07-IMAJ            PIC S9(7) COMP-3.                            
           10 SL07-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSL0700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSL0700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-NSOCIETE-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-NSOCIETE-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-CEMPL-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-CEMPL-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-CAUX-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-CAUX-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-CTIERS-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-CTIERS-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-NCODIC-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-NCODIC-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-CETAT-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-CETAT-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-QSTK-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-QSTK-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-QSTR-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-QSTR-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-IMAJ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 SL07-IMAJ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL07-DSYST-F         PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 SL07-DSYST-F         PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
