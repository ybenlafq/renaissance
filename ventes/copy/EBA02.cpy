      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EBA02   EBA02                                              00000020
      ***************************************************************** 00000030
       01   EBA02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNUMPAGEL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNUMPAGEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNUMPAGEF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNUMPAGEI      PIC X(2).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEMAXL      COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MPAGEMAXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPAGEMAXF      PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MPAGEMAXI      PIC X(2).                                  00000210
           02 MLIGNESI OCCURS   15 TIMES .                              00000220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTIERS1L    COMP PIC S9(4).                            00000230
      *--                                                                       
             03 MCTIERS1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTIERS1F    PIC X.                                     00000240
             03 FILLER  PIC X(4).                                       00000250
             03 MCTIERS1I    PIC X(6).                                  00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOM1L      COMP PIC S9(4).                            00000270
      *--                                                                       
             03 MLNOM1L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLNOM1F      PIC X.                                     00000280
             03 FILLER  PIC X(4).                                       00000290
             03 MLNOM1I      PIC X(20).                                 00000300
      * XXXXXXXXXX                                                      00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERS1L    COMP PIC S9(4).                            00000320
      *--                                                                       
             03 MNTIERS1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNTIERS1F    PIC X.                                     00000330
             03 FILLER  PIC X(4).                                       00000340
             03 MNTIERS1I    PIC X(10).                                 00000350
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCTIERS2L    COMP PIC S9(4).                            00000360
      *--                                                                       
             03 MCTIERS2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MCTIERS2F    PIC X.                                     00000370
             03 FILLER  PIC X(4).                                       00000380
             03 MCTIERS2I    PIC X(6).                                  00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOM2L      COMP PIC S9(4).                            00000400
      *--                                                                       
             03 MLNOM2L COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLNOM2F      PIC X.                                     00000410
             03 FILLER  PIC X(4).                                       00000420
             03 MLNOM2I      PIC X(20).                                 00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNTIERS2L    COMP PIC S9(4).                            00000440
      *--                                                                       
             03 MNTIERS2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MNTIERS2F    PIC X.                                     00000450
             03 FILLER  PIC X(4).                                       00000460
             03 MNTIERS2I    PIC X(10).                                 00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000480
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000490
           02 FILLER    PIC X(4).                                       00000500
           02 MZONCMDI  PIC X(15).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MLIBERRI  PIC X(58).                                      00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MCODTRAI  PIC X(4).                                       00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCICSI    PIC X(5).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MNETNAMI  PIC X(8).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MSCREENI  PIC X(4).                                       00000710
      ***************************************************************** 00000720
      * SDF: EBA02   EBA02                                              00000730
      ***************************************************************** 00000740
       01   EBA02O REDEFINES EBA02I.                                    00000750
           02 FILLER    PIC X(12).                                      00000760
           02 FILLER    PIC X(2).                                       00000770
           02 MDATJOUA  PIC X.                                          00000780
           02 MDATJOUC  PIC X.                                          00000790
           02 MDATJOUP  PIC X.                                          00000800
           02 MDATJOUH  PIC X.                                          00000810
           02 MDATJOUV  PIC X.                                          00000820
           02 MDATJOUO  PIC X(10).                                      00000830
           02 FILLER    PIC X(2).                                       00000840
           02 MTIMJOUA  PIC X.                                          00000850
           02 MTIMJOUC  PIC X.                                          00000860
           02 MTIMJOUP  PIC X.                                          00000870
           02 MTIMJOUH  PIC X.                                          00000880
           02 MTIMJOUV  PIC X.                                          00000890
           02 MTIMJOUO  PIC X(5).                                       00000900
           02 FILLER    PIC X(2).                                       00000910
           02 MNUMPAGEA      PIC X.                                     00000920
           02 MNUMPAGEC PIC X.                                          00000930
           02 MNUMPAGEP PIC X.                                          00000940
           02 MNUMPAGEH PIC X.                                          00000950
           02 MNUMPAGEV PIC X.                                          00000960
           02 MNUMPAGEO      PIC X(2).                                  00000970
           02 FILLER    PIC X(2).                                       00000980
           02 MPAGEMAXA      PIC X.                                     00000990
           02 MPAGEMAXC PIC X.                                          00001000
           02 MPAGEMAXP PIC X.                                          00001010
           02 MPAGEMAXH PIC X.                                          00001020
           02 MPAGEMAXV PIC X.                                          00001030
           02 MPAGEMAXO      PIC X(2).                                  00001040
           02 MLIGNESO OCCURS   15 TIMES .                              00001050
             03 FILLER       PIC X(2).                                  00001060
             03 MCTIERS1A    PIC X.                                     00001070
             03 MCTIERS1C    PIC X.                                     00001080
             03 MCTIERS1P    PIC X.                                     00001090
             03 MCTIERS1H    PIC X.                                     00001100
             03 MCTIERS1V    PIC X.                                     00001110
             03 MCTIERS1O    PIC X(6).                                  00001120
             03 FILLER       PIC X(2).                                  00001130
             03 MLNOM1A      PIC X.                                     00001140
             03 MLNOM1C PIC X.                                          00001150
             03 MLNOM1P PIC X.                                          00001160
             03 MLNOM1H PIC X.                                          00001170
             03 MLNOM1V PIC X.                                          00001180
             03 MLNOM1O      PIC X(20).                                 00001190
      * XXXXXXXXXX                                                      00001200
             03 FILLER       PIC X(2).                                  00001210
             03 MNTIERS1A    PIC X.                                     00001220
             03 MNTIERS1C    PIC X.                                     00001230
             03 MNTIERS1P    PIC X.                                     00001240
             03 MNTIERS1H    PIC X.                                     00001250
             03 MNTIERS1V    PIC X.                                     00001260
             03 MNTIERS1O    PIC X(10).                                 00001270
             03 FILLER       PIC X(2).                                  00001280
             03 MCTIERS2A    PIC X.                                     00001290
             03 MCTIERS2C    PIC X.                                     00001300
             03 MCTIERS2P    PIC X.                                     00001310
             03 MCTIERS2H    PIC X.                                     00001320
             03 MCTIERS2V    PIC X.                                     00001330
             03 MCTIERS2O    PIC X(6).                                  00001340
             03 FILLER       PIC X(2).                                  00001350
             03 MLNOM2A      PIC X.                                     00001360
             03 MLNOM2C PIC X.                                          00001370
             03 MLNOM2P PIC X.                                          00001380
             03 MLNOM2H PIC X.                                          00001390
             03 MLNOM2V PIC X.                                          00001400
             03 MLNOM2O      PIC X(20).                                 00001410
             03 FILLER       PIC X(2).                                  00001420
             03 MNTIERS2A    PIC X.                                     00001430
             03 MNTIERS2C    PIC X.                                     00001440
             03 MNTIERS2P    PIC X.                                     00001450
             03 MNTIERS2H    PIC X.                                     00001460
             03 MNTIERS2V    PIC X.                                     00001470
             03 MNTIERS2O    PIC X(10).                                 00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MZONCMDA  PIC X.                                          00001500
           02 MZONCMDC  PIC X.                                          00001510
           02 MZONCMDP  PIC X.                                          00001520
           02 MZONCMDH  PIC X.                                          00001530
           02 MZONCMDV  PIC X.                                          00001540
           02 MZONCMDO  PIC X(15).                                      00001550
           02 FILLER    PIC X(2).                                       00001560
           02 MLIBERRA  PIC X.                                          00001570
           02 MLIBERRC  PIC X.                                          00001580
           02 MLIBERRP  PIC X.                                          00001590
           02 MLIBERRH  PIC X.                                          00001600
           02 MLIBERRV  PIC X.                                          00001610
           02 MLIBERRO  PIC X(58).                                      00001620
           02 FILLER    PIC X(2).                                       00001630
           02 MCODTRAA  PIC X.                                          00001640
           02 MCODTRAC  PIC X.                                          00001650
           02 MCODTRAP  PIC X.                                          00001660
           02 MCODTRAH  PIC X.                                          00001670
           02 MCODTRAV  PIC X.                                          00001680
           02 MCODTRAO  PIC X(4).                                       00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCICSA    PIC X.                                          00001710
           02 MCICSC    PIC X.                                          00001720
           02 MCICSP    PIC X.                                          00001730
           02 MCICSH    PIC X.                                          00001740
           02 MCICSV    PIC X.                                          00001750
           02 MCICSO    PIC X(5).                                       00001760
           02 FILLER    PIC X(2).                                       00001770
           02 MNETNAMA  PIC X.                                          00001780
           02 MNETNAMC  PIC X.                                          00001790
           02 MNETNAMP  PIC X.                                          00001800
           02 MNETNAMH  PIC X.                                          00001810
           02 MNETNAMV  PIC X.                                          00001820
           02 MNETNAMO  PIC X(8).                                       00001830
           02 FILLER    PIC X(2).                                       00001840
           02 MSCREENA  PIC X.                                          00001850
           02 MSCREENC  PIC X.                                          00001860
           02 MSCREENP  PIC X.                                          00001870
           02 MSCREENH  PIC X.                                          00001880
           02 MSCREENV  PIC X.                                          00001890
           02 MSCREENO  PIC X(4).                                       00001900
                                                                                
