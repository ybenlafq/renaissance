      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....: DECLARATION PRODUIT                             *
      * FICHIER.....: EMISSION COLIS LIGNE SP�CIFIQUE (MUTATION)      *
      * NOM FICHIER.: ECLSPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 109                                             *
      *****************************************************************
      *
       01  ECLSPC6.
      * TYPE ENREGISTREMENT
           05      ECLSPC6-TYP-ENREG       PIC  X(0006).
      * CODE SOCIETE
           05      ECLSPC6-CSOCIETE        PIC  X(0005).
      * NUMERO D OP
           05      ECLSPC6-NOP             PIC  X(0015).
      * NUMERO DE COLIS
           05      ECLSPC6-NCOLIS          PIC  X(0008).
      * NUMERO DE LA LIGNE
           05      ECLSPC6-NLIGNE          PIC  9(0005).
      * NUMERO DE LA SOUS-LIGNE
           05      ECLSPC6-NSSLIGNE        PIC  9(0005).
      * POIDS DE LA LIGNE
           05      ECLSPC6-POIDS           PIC  9(0009).
      * NOM PR�PARATEUR
           05      ECLSPC6-NOM-PREPARATEUR PIC  X(0025).
      * FILLER 30
           05      ECLSPC6-FILLER          PIC  X(0030).
      * FIN
           05      ECLSPC6-FIN             PIC  X(0001).
      
