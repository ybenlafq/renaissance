      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : EDITION DE FICHES                                *        
      *  PROGRAMME  : TCS55                                            *        
      *  TITRE      : COMMAREA DU MODULE D'EDITION DE FICHES           *        
      *  LONGUEUR   : 100                                              *        
      *                                                                *        
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COM-CS55-LONG-COMMAREA       PIC S9(4) COMP   VALUE +100.            
      *--                                                                       
       01  COM-CS55-LONG-COMMAREA       PIC S9(4) COMP-5   VALUE +100.          
      *}                                                                        
      *--- ZONES APPLICATIVES DU MODULE TCS55 ---------------------- 100        
       01  COMM-CS55-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 24        
           05 COMM-CS55-DONNEES-ENTREE.                                         
              10 COMM-CS55-NOMETAT      PIC X(08).                              
              10 COMM-CS55-CIMPRI       PIC X(04).                              
              10 COMM-CS55-AAMMJJ       PIC X(06).                              
              10 COMM-CS55-NOMTS        PIC X(08).                              
      *------ CODE RETOUR + MESSAGE --------------------------------- 59        
           05 COMM-CS55-MESSAGE.                                                
              10 COMM-CS55-CODRET       PIC X(01).                              
                 88 COMM-CS55-CODRET-OK                  VALUE ' '.             
                 88 COMM-CS55-CODRET-ERREUR              VALUE '1'.             
              10 COMM-CS55-LIBERR       PIC X(58).                              
      *--- ZONES DISPONIBLES ---------------------------------------- 15        
           05 COMM-CS55-FILLER          PIC X(15).                              
      *                                                                         
                                                                                
