      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ INTERROGATION EXISTENCE RESERVATION                  
      *   ET RECEPTION REPONSE                                                  
      *****************************************************************         
      * 21/12/2010 AJOUT ORDERID POUR VENTE INNOVENTE                 *         
      *                                                               *         
      *****************************************************************         
      *  POUR BGV516                                                            
      *  TAILLE 124                                                             
INNO  *  TAILLE 135                                                             
       01 WS-MESSAGE.                                                           
      *---                                                                      
           10  MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
           10  MES-INTER-EXISTE.                                                
               20   MES-NSOCIETE             PIC X(3).                          
               20   MES-NLIEU                PIC X(3).                          
               20   MES-DVENTE               PIC X(8).                          
               20   MES-NORDRE               PIC X(5).                          
INNO           20   MES-ORDERID              PIC X(11).                         
          05   LONGUEUR-MESSAGE         PIC S9(03) VALUE +135.                  
      *   05   LONGUEUR-MESSAGE         PIC S9(03) VALUE +138.                  
      *  POUR BGV516                                                            
      *  TAILLE 124                                                             
       01 WS-MESSAGER.                                                          
      *---                                                                      
           10  MESR-ENTETE.                                                     
               20   MESR-TYPE      PIC    X(3).                                 
               20   MESR-NSOCMSG   PIC    X(3).                                 
               20   MESR-NLIEUMSG  PIC    X(3).                                 
               20   MESR-NSOCDST   PIC    X(3).                                 
               20   MESR-NLIEUDST  PIC    X(3).                                 
               20   MESR-NORD      PIC    9(8).                                 
               20   MESR-LPROG     PIC    X(10).                                
               20   MESR-DJOUR     PIC    X(8).                                 
               20   MESR-WSID      PIC    X(10).                                
               20   MESR-USER      PIC    X(10).                                
               20   MESR-CHRONO    PIC    9(7).                                 
               20   MESR-NBRMSG    PIC    9(7).                                 
               20   MESR-NBROCC    PIC    9(5).                                 
               20   MESR-LNGOCC    PIC    9(5).                                 
               20   MESR-FILLER    PIC    X(20).                                
           10  MESR-REPONSE OCCURS 1000.                                        
               20   MESR-NSOCIETE             PIC X(3).                         
               20   MESR-NLIEU                PIC X(3).                         
               20   MESR-DVENTE               PIC X(8).                         
               20   MESR-NORDRE               PIC X(5).                         
          05   LONGUEUR-MESSAGER         PIC S9(03) VALUE +124.                 
                                                                                
