      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : N M D                                            *        
      *  TRANSACTION: MQ10 OU AUTRE                                    *        
      *  TITRE      : COMMAREA DU MODULE STANDARD MSL13 (STOCKS)       *        
      *  LONGUEUR   : 500                                              *        
      *                                                                *        
      ******************************************************************        
      *                                                                 00230000
      *                                                                 00230000
       01  COMM-MSL13-APPLI.                                            00260000
           02 COMM-MSL13-ENTETE.                                                
              05 COMM-MSL13-NSOCCRE        PIC X(03).                   00320000
              05 COMM-MSL13-NLIEUCRE       PIC X(03).                   00330000
              05 COMM-MSL13-CTYPDOC        PIC X(02).                   00330000
              05 COMM-MSL13-NUMDOC         PIC S9(7) COMP-3.            00330000
           02 COMM-MSL13-SL10           PIC X(255).                     00320000
           02 COMM-MSL13-CODRET         PIC X(2).                       00330000
           02 COMM-MSL13-ERREUR         PIC X(52).                      00330000
           02 FILLER                    PIC X(179).                     00340000
                                                                                
