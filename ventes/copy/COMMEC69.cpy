      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TRANSACTION: GV00                                              *        
      * TITRE      : COMMAREA D'APPEL MEC69, MODULE GESTION            *        
      *              DES MUTATIONS DARTY POUR L'EXPEDIION (EMX)        *        
      * LONGUEUR   : ????                                              *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
        01 COMM-EC69-APPLI.                                                     
           05 COMM-EC69-ZIN.                                                    
               10 COMM-EC69-CAPPEL           PIC X(01).                         
      *{ remove-comma-in-dde 1.5                                                
      *           88 COMM-EC69-CONTROLE      VALUE 'D', 'M', '4', 'C'.          
      *--                                                                       
                  88 COMM-EC69-CONTROLE      VALUE 'D'  'M'  '4'  'C'.          
      *}                                                                        
                  88 COMM-EC69-CTRL-MGV64    VALUE '4'.                         
THEMIS            88 COMM-EC69-MUCEN         VALUE 'T'.                         
                  88 COMM-EC69-NEW-APPEL     VALUE ' '.                         
                  88 COMM-EC69-CTRL-GV       VALUE 'G'.                         
               10 COMM-EC69-NEW-MODE.                                           
                  15 COMM-EC69-SUP-MUT-GB05   PIC X(01).                        
                  15 COMM-EC69-SUP-MUT-GB15   PIC X(01).                        
                  15 COMM-EC69-CRE-GB05-TEMPO PIC X(01).                        
                  15 COMM-EC69-CRE-GB05-FINAL PIC X(01).                        
                  15 COMM-EC69-CRE-GB15-FINAL PIC X(01).                        
                  15 COMM-EC69-INTERNET       PIC X(01).                        
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-EC69-SUFFIX           PIC X(01).                         
               10 COMM-EC69-NSOCIETE         PIC X(03).                         
               10 COMM-EC69-NLIEU            PIC X(03).                         
               10 COMM-EC69-MDLIV-NSOC       PIC X(03).                         
               10 COMM-EC69-MDLIV-NLIEU      PIC X(03).                         
               10 COMM-EC69-NVENTE           PIC X(07).                         
               10 COMM-EC69-NCODIC           PIC X(07).                         
               10 COMM-EC69-NSEQNQ           PIC S9(5) COMP-3.                  
               10 COMM-EC69-WCQERESF         PIC X(01).                         
                  88 COMM-EC69-CMD-FNR                 VALUE 'F'.               
               10 COMM-EC69-DDELIV           PIC X(08).                         
               10 COMM-EC69-NSOCLIVR         PIC X(03).                         
               10 COMM-EC69-NDEPOT           PIC X(03).                         
               10 COMM-EC69-LCOMMENT         PIC X(35).                         
               10 COMM-EC69-SSAAMMJJ         PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-EC69-QTE              PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-EC69-QTE              PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
THEMIS*        10 COMM-EC69-QTE2             PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-EC69-QTE2             PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
               10 COMM-EC69-FL05.                                               
                   15 COMM-EC69-FL05-DEPOT     OCCURS 5.                        
                      20 COMM-EC69-FL05-NSOCDEPOT           PIC X(03).          
                      20 COMM-EC69-FL05-NDEPOT              PIC X(03).          
      * -> PARAM�TRES SP�CIAUX                                                  
               10 COMM-EC69-TYPMUT          PIC X(01).                          
                  88 COMM-EC69-MUT-UNIQUE               VALUE 'U'.              
                  88 COMM-EC69-MUT-MULTI                VALUE 'M'.              
               10 COMM-EC69-WEMPORTE        PIC X(01).                          
               10 COMM-EC69-ETAT-PREC-VTE   PIC X(01).                          
                  88 EC69-STATUT-PREC-COMPLETE      VALUE 'N'.                  
                  88 EC69-STATUT-PREC-INCOMPLETE    VALUE 'O'.                  
                  88 EC69-STATUT-PREC-NON-DEFINI    VALUE ' '.                  
               10 COMM-EC69-RESA-A-J        PIC X(01).                          
               10 COMM-EC69-NORDRE          PIC X(05).                          
               10 COMM-EC69-NMUT            PIC X(07).                          
               10 COMM-EC69-MUT-SL300       PIC X(01).                          
               10 COMM-EC69-NCDEWC          PIC S9(15) PACKED-DECIMAL.          
               10 COMM-EC69-GA00-CFAM            PIC X(05).                     
               10 COMM-EC69-GA00-CMARQ           PIC X(05).                     
               10 COMM-EC69-GA00-CAPPRO          PIC X(05).                     
               10 COMM-EC69-GA00-QHAUTEUR        PIC S9(3) COMP-3.              
               10 COMM-EC69-GA00-QPROFONDEUR     PIC S9(3) COMP-3.              
               10 COMM-EC69-GA00-QLARGEUR        PIC S9(3) COMP-3.              
               10 COMM-EC69-GA00-QPOIDS          PIC S9(7) COMP-3.              
               10 FILLER                    PIC X(41).                          
      * ->                                                                      
           05 COMM-EC69-ZOUT.                                                   
               10 COMM-EC69-CODRET           PIC X(04).                         
               10 COMM-EC69-LMESSAGE         PIC X(53).                         
               10 COMM-EC69-NMUTATION        PIC X(07).                         
THEMIS         10 COMM-EC69-NMUTATION2       PIC X(07).                         
               10 COMM-EC69-NMUTTEMP         PIC X(07).                         
               10 COMM-EC69-LIEU-STOCK.                                         
                  15 COMM-EC69-NSOCDEPOTO    PIC X(03).                         
                  15 COMM-EC69-NDEPOTO       PIC X(03).                         
               10 COMM-EC69-ETAT-RESERVATION PIC 9(01).                         
                  88 COMM-EC69-AUCUN-STOCK-DISPO       VALUE 0.                 
                  88 COMM-EC69-STOCK-DISPONIBLE        VALUE 1.                 
                  88 COMM-EC69-STOCK-DACEM             VALUE 2.                 
               10 COMM-EC69-ETAT-FOURNISSEUR PIC 9(01).                         
                  88 COMM-EC69-STOCK-FOURN-NON         VALUE 0.                 
                  88 COMM-EC69-STOCK-FOURN-OUI         VALUE 1.                 
               10 COMM-EC69-DATE-DISPO       PIC X(08).                         
               10 COMM-EC69-PRECO            PIC 9(01).                         
                  88 EC69-PAS-COMMANDE-PRECO      VALUE 0.                      
                  88 EC69-COMMANDE-PRECO          VALUE 1.                      
               10 COMM-EC69-DMUTATION        PIC X(08).                         
               10 FILLER                     PIC X(084).                        
                                                                                
