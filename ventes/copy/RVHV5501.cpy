      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV5501                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV5501                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5501.                                                            
           02  HV55-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV55-NLIEU                                                       
               PIC X(0003).                                                     
           02  HV55-DCAISSE                                                     
               PIC X(0008).                                                     
           02  HV55-NCAISSE                                                     
               PIC X(0003).                                                     
           02  HV55-NTRANS                                                      
               PIC X(0004).                                                     
           02  HV55-DHVENTE                                                     
               PIC X(0002).                                                     
           02  HV55-DMVENTE                                                     
               PIC X(0002).                                                     
           02  HV55-NSEQREGL                                                    
               PIC X(0003).                                                     
           02  HV55-NTYPEREGL                                                   
               PIC X(0001).                                                     
           02  HV55-NOPERATEUR                                                  
               PIC X(0004).                                                     
           02  HV55-PMONTANT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV55-CMODREG                                                     
               PIC X(0005).                                                     
           02  HV55-LMODREG                                                     
               PIC X(0015).                                                     
           02  HV55-DEVCUMUL                                                    
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV5501                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV5501-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-DCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-DCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NCAISSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NCAISSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NTRANS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NTRANS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-DHVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-DHVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-DMVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-DMVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NSEQREGL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NSEQREGL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NTYPEREGL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NTYPEREGL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-NOPERATEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-NOPERATEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-PMONTANT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-PMONTANT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-CMODREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-CMODREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-LMODREG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-LMODREG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV55-DEVCUMUL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV55-DEVCUMUL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
