      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      ******************************************************************        
      * RTEC10 - Suivi de Commandes WebSph�re - Ent�te                          
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEC1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEC1000.                                                            
      *}                                                                        
           10 EC10-NCDEWC          PIC S9(15)V USAGE COMP-3.                    
           10 EC10-NSOCIETE        PIC X(3).                                    
           10 EC10-NLIEU           PIC X(3).                                    
           10 EC10-NVENTE          PIC X(7).                                    
           10 EC10-STAT            PIC X(1).                                    
           10 EC10-OPER            PIC X(4).                                    
           10 EC10-NOM             PIC X(25).                                   
           10 EC10-DCRE            PIC X(8).                                    
           10 EC10-HCRE            PIC X(6).                                    
           10 EC10-DSYST           PIC S9(13)V USAGE COMP-3.                    
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
