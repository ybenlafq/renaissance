      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT1500                                     00020002
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT1500                 00060002
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT1500.                                                    00090002
           02  VT15-NFLAG                                               00100002
               PIC X(0001).                                             00110002
           02  VT15-NCLUSTER                                            00120002
               PIC X(0013).                                             00130002
           02  VT15-NSOCIETE                                            00140002
               PIC X(0003).                                             00150002
           02  VT15-NLIGNE                                              00160002
               PIC X(0003).                                             00170001
           02  VT15-CPRESTATION                                         00180002
               PIC X(0005).                                             00190001
           02  VT15-NLREP                                               00200002
               PIC X(0003).                                             00210001
           02  VT15-DDELIV                                              00220002
               PIC X(0008).                                             00230001
           02  VT15-PVTOTAL                                             00240002
               PIC S9(7)V9(0002) COMP-3.                                00250001
           02  VT15-TAUXTVA                                             00260002
               PIC S9(3)V9(0002) COMP-3.                                00270001
           02  VT15-NCONTRAT                                            00280002
               PIC X(0010).                                             00290001
           02  VT15-CANNULREP                                           00300002
               PIC X(0001).                                             00310001
           02  VT15-CVENDEUR                                            00320002
               PIC X(0007).                                             00330001
           02  VT15-NAUTORM                                             00331003
               PIC X(0005).                                             00332003
           02  VT15-NSEQNQ                                              00333003
               PIC S9(005)  COMP-3.                                     00334003
           02  VT15-NSEQREF                                             00335003
               PIC S9(005)  COMP-3.                                     00336003
           02  VT15-CTYPENT                                             00337004
               PIC X(002).                                              00338004
           02  VT15-NLIEN                                               00339003
               PIC S9(005)  COMP-3.                                     00339103
           02  VT15-NACTVTE                                             00339203
               PIC S9(005)  COMP-3.                                     00339303
           02  VT15-NSEQENS                                             00339403
               PIC S9(005)  COMP-3.                                     00339503
           02  VT15-MPRIMECLI                                           00339603
               PIC S9(7)V9(0002) COMP-3.                                00339703
      *                                                                 00340001
      *---------------------------------------------------------        00350001
      *   LISTE DES FLAGS DE LA TABLE RVVT1500                          00360002
      *---------------------------------------------------------        00370001
      *                                                                 00380001
       01  RVVT1500-FLAGS.                                              00390002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NFLAG-F                                             00400002
      *        PIC S9(4) COMP.                                          00410001
      *--                                                                       
           02  VT15-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NCLUSTER-F                                          00420002
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  VT15-NCLUSTER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NSOCIETE-F                                          00440002
      *        PIC S9(4) COMP.                                          00450002
      *--                                                                       
           02  VT15-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NLIGNE-F                                            00460002
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT15-NLIGNE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-CPRESTATION-F                                       00480002
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT15-CPRESTATION-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NLREP-F                                             00500002
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT15-NLREP-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-DDELIV-F                                            00520002
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT15-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-PVTOTAL-F                                           00540002
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT15-PVTOTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-TAUXTVA-F                                           00560002
      *        PIC S9(4) COMP.                                          00570001
      *--                                                                       
           02  VT15-TAUXTVA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NCONTRAT-F                                          00580002
      *        PIC S9(4) COMP.                                          00590001
      *--                                                                       
           02  VT15-NCONTRAT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-CANNULREP-F                                         00600002
      *        PIC S9(4) COMP.                                          00610001
      *--                                                                       
           02  VT15-CANNULREP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-CVENDEUR-F                                          00620002
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  VT15-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NAUTORM-F                                           00631003
      *        PIC S9(4) COMP.                                          00632003
      *--                                                                       
           02  VT15-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NSEQNQ-F                                            00633003
      *        PIC S9(4) COMP.                                          00634003
      *--                                                                       
           02  VT15-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NSEQREF-F                                           00635003
      *        PIC S9(4) COMP.                                          00636003
      *--                                                                       
           02  VT15-NSEQREF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-CTYPENT-F                                           00637004
      *        PIC S9(4) COMP.                                          00638003
      *--                                                                       
           02  VT15-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NLIEN-F                                             00639003
      *        PIC S9(4) COMP.                                          00639103
      *--                                                                       
           02  VT15-NLIEN-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NACTVTE-F                                           00639203
      *        PIC S9(4) COMP.                                          00639303
      *--                                                                       
           02  VT15-NACTVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-NSEQENS-F                                           00639403
      *        PIC S9(4) COMP.                                          00639503
      *--                                                                       
           02  VT15-NSEQENS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT15-MPRIMECLI-F                                         00639603
      *        PIC S9(4) COMP.                                          00639703
      *--                                                                       
           02  VT15-MPRIMECLI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00640001
                                                                                
