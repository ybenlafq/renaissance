      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EGV55   EGV55                                              00000020
      ***************************************************************** 00000030
       01   ESL56I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * NUMERO DE PAGE COURANTE                                         00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNOPAGEL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MNOPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNOPAGEF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MNOPAGEI  PIC X(3).                                       00000200
      * NOMBRE TOTAL DE PAGES                                           00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPAGEL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MNBPAGEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNBPAGEF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNBPAGEI  PIC X(3).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNCODICI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLREFL    COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLREFL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLREFF    PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLREFI    PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSTAL     COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MSTAL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MSTAF     PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MSTAI     PIC X(3).                                       00000370
           02 MLIGNEI OCCURS   10 TIMES .                               00000380
      * ZONE DE SELECTION                                               00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MSELECTL   COMP PIC S9(4).                            00000400
      *--                                                                       
               04 MSELECTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MSELECTF   PIC X.                                     00000410
               04 FILLER     PIC X(4).                                  00000420
               04 MSELECTI   PIC X.                                     00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNMAGL     COMP PIC S9(4).                            00000440
      *--                                                                       
               04 MNMAGL COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MNMAGF     PIC X.                                     00000450
               04 FILLER     PIC X(4).                                  00000460
               04 MNMAGI     PIC X(6).                                  00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MNVENTEL   COMP PIC S9(4).                            00000480
      *--                                                                       
               04 MNVENTEL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MNVENTEF   PIC X.                                     00000490
               04 FILLER     PIC X(4).                                  00000500
               04 MNVENTEI   PIC X(7).                                  00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLNOML     COMP PIC S9(4).                            00000520
      *--                                                                       
               04 MLNOML COMP-5 PIC S9(4).                                      
      *}                                                                        
               04 MLNOMF     PIC X.                                     00000530
               04 FILLER     PIC X(4).                                  00000540
               04 MLNOMI     PIC X(25).                                 00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MLPRENOML  COMP PIC S9(4).                            00000560
      *--                                                                       
               04 MLPRENOML COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MLPRENOMF  PIC X.                                     00000570
               04 FILLER     PIC X(4).                                  00000580
               04 MLPRENOMI  PIC X(15).                                 00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MDDELIVL   COMP PIC S9(4).                            00000600
      *--                                                                       
               04 MDDELIVL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MDDELIVF   PIC X.                                     00000610
               04 FILLER     PIC X(4).                                  00000620
               04 MDDELIVI   PIC X(5).                                  00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCODACTL   COMP PIC S9(4).                            00000640
      *--                                                                       
               04 MCODACTL COMP-5 PIC S9(4).                                    
      *}                                                                        
               04 MCODACTF   PIC X.                                     00000650
               04 FILLER     PIC X(4).                                  00000660
               04 MCODACTI   PIC X.                                     00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        04 MCSTATUTL  COMP PIC S9(4).                            00000680
      *--                                                                       
               04 MCSTATUTL COMP-5 PIC S9(4).                                   
      *}                                                                        
               04 MCSTATUTF  PIC X.                                     00000690
               04 FILLER     PIC X(4).                                  00000700
               04 MCSTATUTI  PIC X.                                     00000710
      * MESSAGE ERREUR                                                  00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLIBERRI  PIC X(78).                                      00000760
      * CODE TRANSACTION                                                00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MCODTRAI  PIC X(4).                                       00000810
      * CICS DE TRAVAIL                                                 00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MCICSI    PIC X(5).                                       00000860
      * NETNAME                                                         00000870
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000880
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000890
           02 FILLER    PIC X(4).                                       00000900
           02 MNETNAMI  PIC X(8).                                       00000910
      * CODE TERMINAL                                                   00000920
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000930
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000940
           02 FILLER    PIC X(4).                                       00000950
           02 MSCREENI  PIC X(5).                                       00000960
      ***************************************************************** 00000970
      * SDF: EGV55   EGV55                                              00000980
      ***************************************************************** 00000990
       01   ESL56O REDEFINES ESL56I.                                    00001000
           02 FILLER    PIC X(12).                                      00001010
      * DATE DU JOUR                                                    00001020
           02 FILLER    PIC X(2).                                       00001030
           02 MDATJOUA  PIC X.                                          00001040
           02 MDATJOUC  PIC X.                                          00001050
           02 MDATJOUP  PIC X.                                          00001060
           02 MDATJOUH  PIC X.                                          00001070
           02 MDATJOUV  PIC X.                                          00001080
           02 MDATJOUO  PIC X(10).                                      00001090
      * HEURE                                                           00001100
           02 FILLER    PIC X(2).                                       00001110
           02 MTIMJOUA  PIC X.                                          00001120
           02 MTIMJOUC  PIC X.                                          00001130
           02 MTIMJOUP  PIC X.                                          00001140
           02 MTIMJOUH  PIC X.                                          00001150
           02 MTIMJOUV  PIC X.                                          00001160
           02 MTIMJOUO  PIC X(5).                                       00001170
      * NUMERO DE PAGE COURANTE                                         00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MNOPAGEA  PIC X.                                          00001200
           02 MNOPAGEC  PIC X.                                          00001210
           02 MNOPAGEP  PIC X.                                          00001220
           02 MNOPAGEH  PIC X.                                          00001230
           02 MNOPAGEV  PIC X.                                          00001240
           02 MNOPAGEO  PIC X(3).                                       00001250
      * NOMBRE TOTAL DE PAGES                                           00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MNBPAGEA  PIC X.                                          00001280
           02 MNBPAGEC  PIC X.                                          00001290
           02 MNBPAGEP  PIC X.                                          00001300
           02 MNBPAGEH  PIC X.                                          00001310
           02 MNBPAGEV  PIC X.                                          00001320
           02 MNBPAGEO  PIC X(3).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MNCODICA  PIC X.                                          00001350
           02 MNCODICC  PIC X.                                          00001360
           02 MNCODICP  PIC X.                                          00001370
           02 MNCODICH  PIC X.                                          00001380
           02 MNCODICV  PIC X.                                          00001390
           02 MNCODICO  PIC X(7).                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MLREFA    PIC X.                                          00001420
           02 MLREFC    PIC X.                                          00001430
           02 MLREFP    PIC X.                                          00001440
           02 MLREFH    PIC X.                                          00001450
           02 MLREFV    PIC X.                                          00001460
           02 MLREFO    PIC X(20).                                      00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MSTAA     PIC X.                                          00001490
           02 MSTAC     PIC X.                                          00001500
           02 MSTAP     PIC X.                                          00001510
           02 MSTAH     PIC X.                                          00001520
           02 MSTAV     PIC X.                                          00001530
           02 MSTAO     PIC X(3).                                       00001540
           02 MLIGNEO OCCURS   10 TIMES .                               00001550
      * ZONE DE SELECTION                                               00001560
               04 FILLER     PIC X(2).                                  00001570
               04 MSELECTA   PIC X.                                     00001580
               04 MSELECTC   PIC X.                                     00001590
               04 MSELECTP   PIC X.                                     00001600
               04 MSELECTH   PIC X.                                     00001610
               04 MSELECTV   PIC X.                                     00001620
               04 MSELECTO   PIC X.                                     00001630
               04 FILLER     PIC X(2).                                  00001640
               04 MNMAGA     PIC X.                                     00001650
               04 MNMAGC     PIC X.                                     00001660
               04 MNMAGP     PIC X.                                     00001670
               04 MNMAGH     PIC X.                                     00001680
               04 MNMAGV     PIC X.                                     00001690
               04 MNMAGO     PIC X(6).                                  00001700
               04 FILLER     PIC X(2).                                  00001710
               04 MNVENTEA   PIC X.                                     00001720
               04 MNVENTEC   PIC X.                                     00001730
               04 MNVENTEP   PIC X.                                     00001740
               04 MNVENTEH   PIC X.                                     00001750
               04 MNVENTEV   PIC X.                                     00001760
               04 MNVENTEO   PIC X(7).                                  00001770
               04 FILLER     PIC X(2).                                  00001780
               04 MLNOMA     PIC X.                                     00001790
               04 MLNOMC     PIC X.                                     00001800
               04 MLNOMP     PIC X.                                     00001810
               04 MLNOMH     PIC X.                                     00001820
               04 MLNOMV     PIC X.                                     00001830
               04 MLNOMO     PIC X(25).                                 00001840
               04 FILLER     PIC X(2).                                  00001850
               04 MLPRENOMA  PIC X.                                     00001860
               04 MLPRENOMC  PIC X.                                     00001870
               04 MLPRENOMP  PIC X.                                     00001880
               04 MLPRENOMH  PIC X.                                     00001890
               04 MLPRENOMV  PIC X.                                     00001900
               04 MLPRENOMO  PIC X(15).                                 00001910
               04 FILLER     PIC X(2).                                  00001920
               04 MDDELIVA   PIC X.                                     00001930
               04 MDDELIVC   PIC X.                                     00001940
               04 MDDELIVP   PIC X.                                     00001950
               04 MDDELIVH   PIC X.                                     00001960
               04 MDDELIVV   PIC X.                                     00001970
               04 MDDELIVO   PIC X(5).                                  00001980
               04 FILLER     PIC X(2).                                  00001990
               04 MCODACTA   PIC X.                                     00002000
               04 MCODACTC   PIC X.                                     00002010
               04 MCODACTP   PIC X.                                     00002020
               04 MCODACTH   PIC X.                                     00002030
               04 MCODACTV   PIC X.                                     00002040
               04 MCODACTO   PIC X.                                     00002050
               04 FILLER     PIC X(2).                                  00002060
               04 MCSTATUTA  PIC X.                                     00002070
               04 MCSTATUTC  PIC X.                                     00002080
               04 MCSTATUTP  PIC X.                                     00002090
               04 MCSTATUTH  PIC X.                                     00002100
               04 MCSTATUTV  PIC X.                                     00002110
               04 MCSTATUTO  PIC X.                                     00002120
      * MESSAGE ERREUR                                                  00002130
           02 FILLER    PIC X(2).                                       00002140
           02 MLIBERRA  PIC X.                                          00002150
           02 MLIBERRC  PIC X.                                          00002160
           02 MLIBERRP  PIC X.                                          00002170
           02 MLIBERRH  PIC X.                                          00002180
           02 MLIBERRV  PIC X.                                          00002190
           02 MLIBERRO  PIC X(78).                                      00002200
      * CODE TRANSACTION                                                00002210
           02 FILLER    PIC X(2).                                       00002220
           02 MCODTRAA  PIC X.                                          00002230
           02 MCODTRAC  PIC X.                                          00002240
           02 MCODTRAP  PIC X.                                          00002250
           02 MCODTRAH  PIC X.                                          00002260
           02 MCODTRAV  PIC X.                                          00002270
           02 MCODTRAO  PIC X(4).                                       00002280
      * CICS DE TRAVAIL                                                 00002290
           02 FILLER    PIC X(2).                                       00002300
           02 MCICSA    PIC X.                                          00002310
           02 MCICSC    PIC X.                                          00002320
           02 MCICSP    PIC X.                                          00002330
           02 MCICSH    PIC X.                                          00002340
           02 MCICSV    PIC X.                                          00002350
           02 MCICSO    PIC X(5).                                       00002360
      * NETNAME                                                         00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MNETNAMA  PIC X.                                          00002390
           02 MNETNAMC  PIC X.                                          00002400
           02 MNETNAMP  PIC X.                                          00002410
           02 MNETNAMH  PIC X.                                          00002420
           02 MNETNAMV  PIC X.                                          00002430
           02 MNETNAMO  PIC X(8).                                       00002440
      * CODE TERMINAL                                                   00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MSCREENA  PIC X.                                          00002470
           02 MSCREENC  PIC X.                                          00002480
           02 MSCREENP  PIC X.                                          00002490
           02 MSCREENH  PIC X.                                          00002500
           02 MSCREENV  PIC X.                                          00002510
           02 MSCREENO  PIC X(5).                                       00002520
                                                                                
