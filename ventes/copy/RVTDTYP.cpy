      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDTYP CODE TYPE                        *        
      *----------------------------------------------------------------*        
       01  RVTDTYP.                                                             
           05  TDTYP-CTABLEG2    PIC X(15).                                     
           05  TDTYP-CTABLEG2-REDEF REDEFINES TDTYP-CTABLEG2.                   
               10  TDTYP-TYPDOSS         PIC X(05).                             
               10  TDTYP-CTYPE           PIC X(05).                             
           05  TDTYP-WTABLEG     PIC X(80).                                     
           05  TDTYP-WTABLEG-REDEF  REDEFINES TDTYP-WTABLEG.                    
               10  TDTYP-LTYPE           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDTYP-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDTYP-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDTYP-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDTYP-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDTYP-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
