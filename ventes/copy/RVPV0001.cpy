      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RTPV00                        *        
      ******************************************************************        
       01  RVPV0001.                                                            
           10 PV00-NSOCIETE             PIC X(3).                               
           10 PV00-NLIEU                PIC X(3).                               
           10 PV00-NVENTE               PIC X(7).                               
           10 PV00-NSEQNQ               PIC S9(5)V USAGE COMP-3.                
           10 PV00-CVENDEUR             PIC X(6).                               
           10 PV00-DTOPE                PIC X(8).                               
           10 PV00-DCREATION            PIC X(8).                               
           10 PV00-WOFFRE               PIC X(1).                               
           10 PV00-NCODICGRP            PIC X(7).                               
           10 PV00-CTYPENT              PIC X(2).                               
           10 PV00-WREMISE              PIC X(1).                               
           10 PV00-NCODIC               PIC X(7).                               
           10 PV00-CENREG               PIC X(5).                               
           10 PV00-WEP                  PIC X(1).                               
           10 PV00-WOA                  PIC X(1).                               
           10 PV00-WPSAB                PIC X(1).                               
           10 PV00-WREMVTE              PIC X(1).                               
           10 PV00-NSEQENT              PIC S9(5)V USAGE COMP-3.                
           10 PV00-QTVEND               PIC S9(5)V USAGE COMP-3.                
           10 PV00-QTCODIG              PIC S9(5)V USAGE COMP-3.                
           10 PV00-MTVTEHT              PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTVTETVA             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTFORHT              PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTFORTVA             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTDIFFHT             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTDIFFTVA            PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTPRMP               PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTPRIME              PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTPRIMEO             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTPRIMEV             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-MTCOMMOP             PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-TYPVTE               PIC X(1).                               
           10 PV00-WTABLE               PIC X(2).                               
           10 PV00-NZONPRIX             PIC X(2).                               
           10 PV00-MTREFINOP            PIC S9(7)V9(2) USAGE COMP-3.            
           10 PV00-DDELIV               PIC X(08).                              
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 35      *        
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPV0001                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPV0001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-CVENDEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-CVENDEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-DTOPE-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-DTOPE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WOFFRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WOFFRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-CTYPENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-CTYPENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WREMISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WREMISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-CENREG-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-CENREG-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WEP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WEP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WOA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WOA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WPSAB-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WPSAB-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WREMVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WREMVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-QSEQENT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-QSEQENT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-QTVEND-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-QTVEND-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-QTCODIG-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-QTCODIG-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTVTEHT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTVTEHT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTVTETVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTVTETVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTFORHT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTFORHT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTFORTVA-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTFORTVA-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTDIFFHT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTDIFFHT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTDIFFTVA-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTDIFFTVA-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTPRMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTPRMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTPRIME-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTPRIME-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTPRIMEO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTPRIMEO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTPRIMEV-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTPRIMEV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTCOMMOP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTCOMMOP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-TYPVTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-TYPVTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-WTABLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-WTABLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-NZONPRIX-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-NZONPRIX-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-MTREFINOP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-MTREFINOP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PV00-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PV00-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
