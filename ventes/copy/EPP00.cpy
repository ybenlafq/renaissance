      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EPP00   EPP00                                              00000020
      ***************************************************************** 00000030
       01   EPP00I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCIETEI     PIC X(3).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000180
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNLIEUORIGI    PIC X(3).                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MZONCMDI  PIC X(15).                                      00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNLIEUI   PIC X(3).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLLIEUI   PIC X(20).                                      00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDMOISL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDMOISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDMOISF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDMOISI   PIC X(6).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTURL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MOPTURL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MOPTURF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MOPTURI   PIC X(2).                                       00000410
           02 MLIBOPT1D OCCURS   4 TIMES .                              00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBOPT1L    COMP PIC S9(4).                            00000430
      *--                                                                       
             03 MLIBOPT1L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBOPT1F    PIC X.                                     00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MLIBOPT1I    PIC X(25).                                 00000460
           02 MLIBOPT2D OCCURS   8 TIMES .                              00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLIBOPT2L    COMP PIC S9(4).                            00000480
      *--                                                                       
             03 MLIBOPT2L COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MLIBOPT2F    PIC X.                                     00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MLIBOPT2I    PIC X(25).                                 00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MOPTPARAML     COMP PIC S9(4).                            00000520
      *--                                                                       
           02 MOPTPARAML COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MOPTPARAMF     PIC X.                                     00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MOPTPARAMI     PIC X(2).                                  00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLIBERRI  PIC X(78).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000600
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MCODTRAI  PIC X(4).                                       00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000640
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCICSI    PIC X(5).                                       00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000680
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MNETNAMI  PIC X(8).                                       00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MSCREENI  PIC X(4).                                       00000750
      ***************************************************************** 00000760
      * SDF: EPP00   EPP00                                              00000770
      ***************************************************************** 00000780
       01   EPP00O REDEFINES EPP00I.                                    00000790
           02 FILLER    PIC X(12).                                      00000800
           02 FILLER    PIC X(2).                                       00000810
           02 MDATJOUA  PIC X.                                          00000820
           02 MDATJOUC  PIC X.                                          00000830
           02 MDATJOUP  PIC X.                                          00000840
           02 MDATJOUH  PIC X.                                          00000850
           02 MDATJOUV  PIC X.                                          00000860
           02 MDATJOUO  PIC X(10).                                      00000870
           02 FILLER    PIC X(2).                                       00000880
           02 MTIMJOUA  PIC X.                                          00000890
           02 MTIMJOUC  PIC X.                                          00000900
           02 MTIMJOUP  PIC X.                                          00000910
           02 MTIMJOUH  PIC X.                                          00000920
           02 MTIMJOUV  PIC X.                                          00000930
           02 MTIMJOUO  PIC X(5).                                       00000940
           02 FILLER    PIC X(2).                                       00000950
           02 MNSOCIETEA     PIC X.                                     00000960
           02 MNSOCIETEC     PIC X.                                     00000970
           02 MNSOCIETEP     PIC X.                                     00000980
           02 MNSOCIETEH     PIC X.                                     00000990
           02 MNSOCIETEV     PIC X.                                     00001000
           02 MNSOCIETEO     PIC X(3).                                  00001010
           02 FILLER    PIC X(2).                                       00001020
           02 MNLIEUORIGA    PIC X.                                     00001030
           02 MNLIEUORIGC    PIC X.                                     00001040
           02 MNLIEUORIGP    PIC X.                                     00001050
           02 MNLIEUORIGH    PIC X.                                     00001060
           02 MNLIEUORIGV    PIC X.                                     00001070
           02 MNLIEUORIGO    PIC X(3).                                  00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MZONCMDA  PIC X.                                          00001100
           02 MZONCMDC  PIC X.                                          00001110
           02 MZONCMDP  PIC X.                                          00001120
           02 MZONCMDH  PIC X.                                          00001130
           02 MZONCMDV  PIC X.                                          00001140
           02 MZONCMDO  PIC X(15).                                      00001150
           02 FILLER    PIC X(2).                                       00001160
           02 MNLIEUA   PIC X.                                          00001170
           02 MNLIEUC   PIC X.                                          00001180
           02 MNLIEUP   PIC X.                                          00001190
           02 MNLIEUH   PIC X.                                          00001200
           02 MNLIEUV   PIC X.                                          00001210
           02 MNLIEUO   PIC X(3).                                       00001220
           02 FILLER    PIC X(2).                                       00001230
           02 MLLIEUA   PIC X.                                          00001240
           02 MLLIEUC   PIC X.                                          00001250
           02 MLLIEUP   PIC X.                                          00001260
           02 MLLIEUH   PIC X.                                          00001270
           02 MLLIEUV   PIC X.                                          00001280
           02 MLLIEUO   PIC X(20).                                      00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MDMOISA   PIC X.                                          00001310
           02 MDMOISC   PIC X.                                          00001320
           02 MDMOISP   PIC X.                                          00001330
           02 MDMOISH   PIC X.                                          00001340
           02 MDMOISV   PIC X.                                          00001350
           02 MDMOISO   PIC X(6).                                       00001360
           02 FILLER    PIC X(2).                                       00001370
           02 MOPTURA   PIC X.                                          00001380
           02 MOPTURC   PIC X.                                          00001390
           02 MOPTURP   PIC X.                                          00001400
           02 MOPTURH   PIC X.                                          00001410
           02 MOPTURV   PIC X.                                          00001420
           02 MOPTURO   PIC X(2).                                       00001430
           02 DFHMS1 OCCURS   4 TIMES .                                 00001440
             03 FILLER       PIC X(2).                                  00001450
             03 MLIBOPT1A    PIC X.                                     00001460
             03 MLIBOPT1C    PIC X.                                     00001470
             03 MLIBOPT1P    PIC X.                                     00001480
             03 MLIBOPT1H    PIC X.                                     00001490
             03 MLIBOPT1V    PIC X.                                     00001500
             03 MLIBOPT1O    PIC X(25).                                 00001510
           02 DFHMS2 OCCURS   8 TIMES .                                 00001520
             03 FILLER       PIC X(2).                                  00001530
             03 MLIBOPT2A    PIC X.                                     00001540
             03 MLIBOPT2C    PIC X.                                     00001550
             03 MLIBOPT2P    PIC X.                                     00001560
             03 MLIBOPT2H    PIC X.                                     00001570
             03 MLIBOPT2V    PIC X.                                     00001580
             03 MLIBOPT2O    PIC X(25).                                 00001590
           02 FILLER    PIC X(2).                                       00001600
           02 MOPTPARAMA     PIC X.                                     00001610
           02 MOPTPARAMC     PIC X.                                     00001620
           02 MOPTPARAMP     PIC X.                                     00001630
           02 MOPTPARAMH     PIC X.                                     00001640
           02 MOPTPARAMV     PIC X.                                     00001650
           02 MOPTPARAMO     PIC X(2).                                  00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MLIBERRA  PIC X.                                          00001680
           02 MLIBERRC  PIC X.                                          00001690
           02 MLIBERRP  PIC X.                                          00001700
           02 MLIBERRH  PIC X.                                          00001710
           02 MLIBERRV  PIC X.                                          00001720
           02 MLIBERRO  PIC X(78).                                      00001730
           02 FILLER    PIC X(2).                                       00001740
           02 MCODTRAA  PIC X.                                          00001750
           02 MCODTRAC  PIC X.                                          00001760
           02 MCODTRAP  PIC X.                                          00001770
           02 MCODTRAH  PIC X.                                          00001780
           02 MCODTRAV  PIC X.                                          00001790
           02 MCODTRAO  PIC X(4).                                       00001800
           02 FILLER    PIC X(2).                                       00001810
           02 MCICSA    PIC X.                                          00001820
           02 MCICSC    PIC X.                                          00001830
           02 MCICSP    PIC X.                                          00001840
           02 MCICSH    PIC X.                                          00001850
           02 MCICSV    PIC X.                                          00001860
           02 MCICSO    PIC X(5).                                       00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNETNAMA  PIC X.                                          00001890
           02 MNETNAMC  PIC X.                                          00001900
           02 MNETNAMP  PIC X.                                          00001910
           02 MNETNAMH  PIC X.                                          00001920
           02 MNETNAMV  PIC X.                                          00001930
           02 MNETNAMO  PIC X(8).                                       00001940
           02 FILLER    PIC X(2).                                       00001950
           02 MSCREENA  PIC X.                                          00001960
           02 MSCREENC  PIC X.                                          00001970
           02 MSCREENP  PIC X.                                          00001980
           02 MSCREENH  PIC X.                                          00001990
           02 MSCREENV  PIC X.                                          00002000
           02 MSCREENO  PIC X(4).                                       00002010
                                                                                
