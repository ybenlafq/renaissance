      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV05   EAV05                                              00000020
      ***************************************************************** 00000030
       01   EAV05I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�societe                                                       00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCSAISIEL   COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCSAISIEL COMP-5 PIC S9(4).                                    
      *}                                                                        
           02 MNSOCSAISIEF   PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCSAISIEI   PIC X(3).                                  00000180
      * n�lieu                                                          00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUSAISIEL  COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUSAISIEF  PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUSAISIEI  PIC X(3).                                  00000230
      * libelle lieu                                                    00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUSAISIEL  COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MLLIEUSAISIEL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MLLIEUSAISIEF  PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUSAISIEI  PIC X(21).                                 00000280
      * n�avoir                                                         00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNAVOIRI  PIC X(8).                                       00000330
      * code type utilisation                                           00000340
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000350
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000360
           02 FILLER    PIC X(4).                                       00000370
           02 MCTYPUTILI     PIC X(5).                                  00000380
      * libelle type utilisation                                        00000390
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000400
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000410
           02 FILLER    PIC X(4).                                       00000420
           02 MLTYPUTILI     PIC X(20).                                 00000430
      * code motif                                                      00000440
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000450
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000460
           02 FILLER    PIC X(4).                                       00000470
           02 MCMOTIFI  PIC X(5).                                       00000480
      * libelle motif                                                   00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MLMOTIFI  PIC X(20).                                      00000530
      * montant avoir                                                   00000540
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAVOIRL  COMP PIC S9(4).                                 00000550
      *--                                                                       
           02 MPAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAVOIRF  PIC X.                                          00000560
           02 FILLER    PIC X(4).                                       00000570
           02 MPAVOIRI  PIC X(10).                                      00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000590
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MCDEVISEI      PIC X(3).                                  00000620
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000630
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000640
           02 FILLER    PIC X(4).                                       00000650
           02 MLDEVISEI      PIC X(5).                                  00000660
      * titre du nom                                                    00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTITRENOML    COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MCTITRENOML COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MCTITRENOMF    PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCTITRENOMI    PIC X(5).                                  00000710
      * nom du client                                                   00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000730
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000740
           02 FILLER    PIC X(4).                                       00000750
           02 MLNOMI    PIC X(25).                                      00000760
      * date d'emission                                                 00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDEMISI   PIC X(10).                                      00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MLIBERRI  PIC X(78).                                      00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MCODTRAI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MCICSI    PIC X(5).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MSCREENI  PIC X(4).                                       00001010
      ***************************************************************** 00001020
      * SDF: EAV05   EAV05                                              00001030
      ***************************************************************** 00001040
       01   EAV05O REDEFINES EAV05I.                                    00001050
           02 FILLER    PIC X(12).                                      00001060
           02 FILLER    PIC X(2).                                       00001070
           02 MDATJOUA  PIC X.                                          00001080
           02 MDATJOUC  PIC X.                                          00001090
           02 MDATJOUP  PIC X.                                          00001100
           02 MDATJOUH  PIC X.                                          00001110
           02 MDATJOUV  PIC X.                                          00001120
           02 MDATJOUO  PIC X(10).                                      00001130
           02 FILLER    PIC X(2).                                       00001140
           02 MTIMJOUA  PIC X.                                          00001150
           02 MTIMJOUC  PIC X.                                          00001160
           02 MTIMJOUP  PIC X.                                          00001170
           02 MTIMJOUH  PIC X.                                          00001180
           02 MTIMJOUV  PIC X.                                          00001190
           02 MTIMJOUO  PIC X(5).                                       00001200
      * n�societe                                                       00001210
           02 FILLER    PIC X(2).                                       00001220
           02 MNSOCSAISIEA   PIC X.                                     00001230
           02 MNSOCSAISIEC   PIC X.                                     00001240
           02 MNSOCSAISIEP   PIC X.                                     00001250
           02 MNSOCSAISIEH   PIC X.                                     00001260
           02 MNSOCSAISIEV   PIC X.                                     00001270
           02 MNSOCSAISIEO   PIC X(3).                                  00001280
      * n�lieu                                                          00001290
           02 FILLER    PIC X(2).                                       00001300
           02 MNLIEUSAISIEA  PIC X.                                     00001310
           02 MNLIEUSAISIEC  PIC X.                                     00001320
           02 MNLIEUSAISIEP  PIC X.                                     00001330
           02 MNLIEUSAISIEH  PIC X.                                     00001340
           02 MNLIEUSAISIEV  PIC X.                                     00001350
           02 MNLIEUSAISIEO  PIC X(3).                                  00001360
      * libelle lieu                                                    00001370
           02 FILLER    PIC X(2).                                       00001380
           02 MLLIEUSAISIEA  PIC X.                                     00001390
           02 MLLIEUSAISIEC  PIC X.                                     00001400
           02 MLLIEUSAISIEP  PIC X.                                     00001410
           02 MLLIEUSAISIEH  PIC X.                                     00001420
           02 MLLIEUSAISIEV  PIC X.                                     00001430
           02 MLLIEUSAISIEO  PIC X(21).                                 00001440
      * n�avoir                                                         00001450
           02 FILLER    PIC X(2).                                       00001460
           02 MNAVOIRA  PIC X.                                          00001470
           02 MNAVOIRC  PIC X.                                          00001480
           02 MNAVOIRP  PIC X.                                          00001490
           02 MNAVOIRH  PIC X.                                          00001500
           02 MNAVOIRV  PIC X.                                          00001510
           02 MNAVOIRO  PIC X(8).                                       00001520
      * code type utilisation                                           00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MCTYPUTILA     PIC X.                                     00001550
           02 MCTYPUTILC     PIC X.                                     00001560
           02 MCTYPUTILP     PIC X.                                     00001570
           02 MCTYPUTILH     PIC X.                                     00001580
           02 MCTYPUTILV     PIC X.                                     00001590
           02 MCTYPUTILO     PIC X(5).                                  00001600
      * libelle type utilisation                                        00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLTYPUTILA     PIC X.                                     00001630
           02 MLTYPUTILC     PIC X.                                     00001640
           02 MLTYPUTILP     PIC X.                                     00001650
           02 MLTYPUTILH     PIC X.                                     00001660
           02 MLTYPUTILV     PIC X.                                     00001670
           02 MLTYPUTILO     PIC X(20).                                 00001680
      * code motif                                                      00001690
           02 FILLER    PIC X(2).                                       00001700
           02 MCMOTIFA  PIC X.                                          00001710
           02 MCMOTIFC  PIC X.                                          00001720
           02 MCMOTIFP  PIC X.                                          00001730
           02 MCMOTIFH  PIC X.                                          00001740
           02 MCMOTIFV  PIC X.                                          00001750
           02 MCMOTIFO  PIC X(5).                                       00001760
      * libelle motif                                                   00001770
           02 FILLER    PIC X(2).                                       00001780
           02 MLMOTIFA  PIC X.                                          00001790
           02 MLMOTIFC  PIC X.                                          00001800
           02 MLMOTIFP  PIC X.                                          00001810
           02 MLMOTIFH  PIC X.                                          00001820
           02 MLMOTIFV  PIC X.                                          00001830
           02 MLMOTIFO  PIC X(20).                                      00001840
      * montant avoir                                                   00001850
           02 FILLER    PIC X(2).                                       00001860
           02 MPAVOIRA  PIC X.                                          00001870
           02 MPAVOIRC  PIC X.                                          00001880
           02 MPAVOIRP  PIC X.                                          00001890
           02 MPAVOIRH  PIC X.                                          00001900
           02 MPAVOIRV  PIC X.                                          00001910
           02 MPAVOIRO  PIC X(10).                                      00001920
           02 FILLER    PIC X(2).                                       00001930
           02 MCDEVISEA      PIC X.                                     00001940
           02 MCDEVISEC PIC X.                                          00001950
           02 MCDEVISEP PIC X.                                          00001960
           02 MCDEVISEH PIC X.                                          00001970
           02 MCDEVISEV PIC X.                                          00001980
           02 MCDEVISEO      PIC X(3).                                  00001990
           02 FILLER    PIC X(2).                                       00002000
           02 MLDEVISEA      PIC X.                                     00002010
           02 MLDEVISEC PIC X.                                          00002020
           02 MLDEVISEP PIC X.                                          00002030
           02 MLDEVISEH PIC X.                                          00002040
           02 MLDEVISEV PIC X.                                          00002050
           02 MLDEVISEO      PIC X(5).                                  00002060
      * titre du nom                                                    00002070
           02 FILLER    PIC X(2).                                       00002080
           02 MCTITRENOMA    PIC X.                                     00002090
           02 MCTITRENOMC    PIC X.                                     00002100
           02 MCTITRENOMP    PIC X.                                     00002110
           02 MCTITRENOMH    PIC X.                                     00002120
           02 MCTITRENOMV    PIC X.                                     00002130
           02 MCTITRENOMO    PIC X(5).                                  00002140
      * nom du client                                                   00002150
           02 FILLER    PIC X(2).                                       00002160
           02 MLNOMA    PIC X.                                          00002170
           02 MLNOMC    PIC X.                                          00002180
           02 MLNOMP    PIC X.                                          00002190
           02 MLNOMH    PIC X.                                          00002200
           02 MLNOMV    PIC X.                                          00002210
           02 MLNOMO    PIC X(25).                                      00002220
      * date d'emission                                                 00002230
           02 FILLER    PIC X(2).                                       00002240
           02 MDEMISA   PIC X.                                          00002250
           02 MDEMISC   PIC X.                                          00002260
           02 MDEMISP   PIC X.                                          00002270
           02 MDEMISH   PIC X.                                          00002280
           02 MDEMISV   PIC X.                                          00002290
           02 MDEMISO   PIC X(10).                                      00002300
           02 FILLER    PIC X(2).                                       00002310
           02 MLIBERRA  PIC X.                                          00002320
           02 MLIBERRC  PIC X.                                          00002330
           02 MLIBERRP  PIC X.                                          00002340
           02 MLIBERRH  PIC X.                                          00002350
           02 MLIBERRV  PIC X.                                          00002360
           02 MLIBERRO  PIC X(78).                                      00002370
           02 FILLER    PIC X(2).                                       00002380
           02 MCODTRAA  PIC X.                                          00002390
           02 MCODTRAC  PIC X.                                          00002400
           02 MCODTRAP  PIC X.                                          00002410
           02 MCODTRAH  PIC X.                                          00002420
           02 MCODTRAV  PIC X.                                          00002430
           02 MCODTRAO  PIC X(4).                                       00002440
           02 FILLER    PIC X(2).                                       00002450
           02 MCICSA    PIC X.                                          00002460
           02 MCICSC    PIC X.                                          00002470
           02 MCICSP    PIC X.                                          00002480
           02 MCICSH    PIC X.                                          00002490
           02 MCICSV    PIC X.                                          00002500
           02 MCICSO    PIC X(5).                                       00002510
           02 FILLER    PIC X(2).                                       00002520
           02 MNETNAMA  PIC X.                                          00002530
           02 MNETNAMC  PIC X.                                          00002540
           02 MNETNAMP  PIC X.                                          00002550
           02 MNETNAMH  PIC X.                                          00002560
           02 MNETNAMV  PIC X.                                          00002570
           02 MNETNAMO  PIC X(8).                                       00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MSCREENA  PIC X.                                          00002600
           02 MSCREENC  PIC X.                                          00002610
           02 MSCREENP  PIC X.                                          00002620
           02 MSCREENH  PIC X.                                          00002630
           02 MSCREENV  PIC X.                                          00002640
           02 MSCREENO  PIC X(4).                                       00002650
                                                                                
