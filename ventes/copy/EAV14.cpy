      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV14   EAV14                                              00000020
      ***************************************************************** 00000030
       01   EAV14I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n� de page                                                      00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGEXL  COMP PIC S9(4).                                 00000150
      *--                                                                       
           02 MNPAGEXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNPAGEXF  PIC X.                                          00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNPAGEXI  PIC X(3).                                       00000180
      * nombre page total                                               00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNPAGTOTL      COMP PIC S9(4).                            00000200
      *--                                                                       
           02 MNPAGTOTL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNPAGTOTF      PIC X.                                     00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNPAGTOTI      PIC X(3).                                  00000230
      * code devise                                                     00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000250
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MCDEVISEI      PIC X(3).                                  00000280
      * libelle devise                                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLDEVISEI      PIC X(5).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCXL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNSOCXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNSOCXF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNSOCXI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUXL  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MNLIEUXL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNLIEUXF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MNLIEUXI  PIC X(3).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRXL      COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNAVOIRXL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNAVOIRXF      PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNAVOIRXI      PIC X(8).                                  00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEJJL  COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MDATEJJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEJJF  PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MDATEJJI  PIC X(2).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEMML  COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MDATEMML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEMMF  PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MDATEMMI  PIC X(2).                                       00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATEAAL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MDATEAAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATEAAF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MDATEAAI  PIC X(4).                                       00000570
      * nom � selectionner                                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOMXL   COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MLNOMXL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOMXF   PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MLNOMXI   PIC X(20).                                      00000620
           02 MLIGNEI OCCURS   13 TIMES .                               00000630
      * case de selection                                               00000640
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSELECTL     COMP PIC S9(4).                            00000650
      *--                                                                       
             03 MSELECTL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MSELECTF     PIC X.                                     00000660
             03 FILLER  PIC X(4).                                       00000670
             03 MSELECTI     PIC X.                                     00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNSOCL  COMP PIC S9(4).                                 00000690
      *--                                                                       
             03 MNSOCL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNSOCF  PIC X.                                          00000700
             03 FILLER  PIC X(4).                                       00000710
             03 MNSOCI  PIC X(3).                                       00000720
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNLIEUL      COMP PIC S9(4).                            00000730
      *--                                                                       
             03 MNLIEUL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MNLIEUF      PIC X.                                     00000740
             03 FILLER  PIC X(4).                                       00000750
             03 MNLIEUI      PIC X(3).                                  00000760
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNAVOIRL     COMP PIC S9(4).                            00000770
      *--                                                                       
             03 MNAVOIRL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MNAVOIRF     PIC X.                                     00000780
             03 FILLER  PIC X(4).                                       00000790
             03 MNAVOIRI     PIC X(8).                                  00000800
      * date d'�mission                                                 00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEMISL      COMP PIC S9(4).                            00000820
      *--                                                                       
             03 MDEMISL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEMISF      PIC X.                                     00000830
             03 FILLER  PIC X(4).                                       00000840
             03 MDEMISI      PIC X(10).                                 00000850
      * code motif                                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCMOTIFAVL   COMP PIC S9(4).                            00000870
      *--                                                                       
             03 MCMOTIFAVL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MCMOTIFAVF   PIC X.                                     00000880
             03 FILLER  PIC X(4).                                       00000890
             03 MCMOTIFAVI   PIC X(5).                                  00000900
      * nom                                                             00000910
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLNOML  COMP PIC S9(4).                                 00000920
      *--                                                                       
             03 MLNOML COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLNOMF  PIC X.                                          00000930
             03 FILLER  PIC X(4).                                       00000940
             03 MLNOMI  PIC X(20).                                      00000950
      * montant                                                         00000960
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MPMONTANTL   COMP PIC S9(4).                            00000970
      *--                                                                       
             03 MPMONTANTL COMP-5 PIC S9(4).                                    
      *}                                                                        
             03 MPMONTANTF   PIC X.                                     00000980
             03 FILLER  PIC X(4).                                       00000990
             03 MPMONTANTI   PIC X(10).                                 00001000
      * flag annulation                                                 00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MCANNULL     COMP PIC S9(4).                            00001020
      *--                                                                       
             03 MCANNULL COMP-5 PIC S9(4).                                      
      *}                                                                        
             03 MCANNULF     PIC X.                                     00001030
             03 FILLER  PIC X(4).                                       00001040
             03 MCANNULI     PIC X.                                     00001050
      * flag statut utilisation                                         00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MWUTILL      COMP PIC S9(4).                            00001070
      *--                                                                       
             03 MWUTILL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MWUTILF      PIC X.                                     00001080
             03 FILLER  PIC X(4).                                       00001090
             03 MWUTILI      PIC X.                                     00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MLIBERRI  PIC X(78).                                      00001140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001150
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001160
           02 FILLER    PIC X(4).                                       00001170
           02 MCODTRAI  PIC X(4).                                       00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001190
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MCICSI    PIC X(5).                                       00001220
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001230
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001240
           02 FILLER    PIC X(4).                                       00001250
           02 MNETNAMI  PIC X(8).                                       00001260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001270
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001280
           02 FILLER    PIC X(4).                                       00001290
           02 MSCREENI  PIC X(4).                                       00001300
      ***************************************************************** 00001310
      * SDF: EAV14   EAV14                                              00001320
      ***************************************************************** 00001330
       01   EAV14O REDEFINES EAV14I.                                    00001340
           02 FILLER    PIC X(12).                                      00001350
           02 FILLER    PIC X(2).                                       00001360
           02 MDATJOUA  PIC X.                                          00001370
           02 MDATJOUC  PIC X.                                          00001380
           02 MDATJOUP  PIC X.                                          00001390
           02 MDATJOUH  PIC X.                                          00001400
           02 MDATJOUV  PIC X.                                          00001410
           02 MDATJOUO  PIC X(10).                                      00001420
           02 FILLER    PIC X(2).                                       00001430
           02 MTIMJOUA  PIC X.                                          00001440
           02 MTIMJOUC  PIC X.                                          00001450
           02 MTIMJOUP  PIC X.                                          00001460
           02 MTIMJOUH  PIC X.                                          00001470
           02 MTIMJOUV  PIC X.                                          00001480
           02 MTIMJOUO  PIC X(5).                                       00001490
      * n� de page                                                      00001500
           02 FILLER    PIC X(2).                                       00001510
           02 MNPAGEXA  PIC X.                                          00001520
           02 MNPAGEXC  PIC X.                                          00001530
           02 MNPAGEXP  PIC X.                                          00001540
           02 MNPAGEXH  PIC X.                                          00001550
           02 MNPAGEXV  PIC X.                                          00001560
           02 MNPAGEXO  PIC X(3).                                       00001570
      * nombre page total                                               00001580
           02 FILLER    PIC X(2).                                       00001590
           02 MNPAGTOTA      PIC X.                                     00001600
           02 MNPAGTOTC PIC X.                                          00001610
           02 MNPAGTOTP PIC X.                                          00001620
           02 MNPAGTOTH PIC X.                                          00001630
           02 MNPAGTOTV PIC X.                                          00001640
           02 MNPAGTOTO      PIC X(3).                                  00001650
      * code devise                                                     00001660
           02 FILLER    PIC X(2).                                       00001670
           02 MCDEVISEA      PIC X.                                     00001680
           02 MCDEVISEC PIC X.                                          00001690
           02 MCDEVISEP PIC X.                                          00001700
           02 MCDEVISEH PIC X.                                          00001710
           02 MCDEVISEV PIC X.                                          00001720
           02 MCDEVISEO      PIC X(3).                                  00001730
      * libelle devise                                                  00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MLDEVISEA      PIC X.                                     00001760
           02 MLDEVISEC PIC X.                                          00001770
           02 MLDEVISEP PIC X.                                          00001780
           02 MLDEVISEH PIC X.                                          00001790
           02 MLDEVISEV PIC X.                                          00001800
           02 MLDEVISEO      PIC X(5).                                  00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MNSOCXA   PIC X.                                          00001830
           02 MNSOCXC   PIC X.                                          00001840
           02 MNSOCXP   PIC X.                                          00001850
           02 MNSOCXH   PIC X.                                          00001860
           02 MNSOCXV   PIC X.                                          00001870
           02 MNSOCXO   PIC X(3).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MNLIEUXA  PIC X.                                          00001900
           02 MNLIEUXC  PIC X.                                          00001910
           02 MNLIEUXP  PIC X.                                          00001920
           02 MNLIEUXH  PIC X.                                          00001930
           02 MNLIEUXV  PIC X.                                          00001940
           02 MNLIEUXO  PIC X(3).                                       00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNAVOIRXA      PIC X.                                     00001970
           02 MNAVOIRXC PIC X.                                          00001980
           02 MNAVOIRXP PIC X.                                          00001990
           02 MNAVOIRXH PIC X.                                          00002000
           02 MNAVOIRXV PIC X.                                          00002010
           02 MNAVOIRXO      PIC X(8).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MDATEJJA  PIC X.                                          00002040
           02 MDATEJJC  PIC X.                                          00002050
           02 MDATEJJP  PIC X.                                          00002060
           02 MDATEJJH  PIC X.                                          00002070
           02 MDATEJJV  PIC X.                                          00002080
           02 MDATEJJO  PIC X(2).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MDATEMMA  PIC X.                                          00002110
           02 MDATEMMC  PIC X.                                          00002120
           02 MDATEMMP  PIC X.                                          00002130
           02 MDATEMMH  PIC X.                                          00002140
           02 MDATEMMV  PIC X.                                          00002150
           02 MDATEMMO  PIC X(2).                                       00002160
           02 FILLER    PIC X(2).                                       00002170
           02 MDATEAAA  PIC X.                                          00002180
           02 MDATEAAC  PIC X.                                          00002190
           02 MDATEAAP  PIC X.                                          00002200
           02 MDATEAAH  PIC X.                                          00002210
           02 MDATEAAV  PIC X.                                          00002220
           02 MDATEAAO  PIC X(4).                                       00002230
      * nom � selectionner                                              00002240
           02 FILLER    PIC X(2).                                       00002250
           02 MLNOMXA   PIC X.                                          00002260
           02 MLNOMXC   PIC X.                                          00002270
           02 MLNOMXP   PIC X.                                          00002280
           02 MLNOMXH   PIC X.                                          00002290
           02 MLNOMXV   PIC X.                                          00002300
           02 MLNOMXO   PIC X(20).                                      00002310
           02 MLIGNEO OCCURS   13 TIMES .                               00002320
      * case de selection                                               00002330
             03 FILLER       PIC X(2).                                  00002340
             03 MSELECTA     PIC X.                                     00002350
             03 MSELECTC     PIC X.                                     00002360
             03 MSELECTP     PIC X.                                     00002370
             03 MSELECTH     PIC X.                                     00002380
             03 MSELECTV     PIC X.                                     00002390
             03 MSELECTO     PIC X.                                     00002400
             03 FILLER       PIC X(2).                                  00002410
             03 MNSOCA  PIC X.                                          00002420
             03 MNSOCC  PIC X.                                          00002430
             03 MNSOCP  PIC X.                                          00002440
             03 MNSOCH  PIC X.                                          00002450
             03 MNSOCV  PIC X.                                          00002460
             03 MNSOCO  PIC X(3).                                       00002470
             03 FILLER       PIC X(2).                                  00002480
             03 MNLIEUA      PIC X.                                     00002490
             03 MNLIEUC PIC X.                                          00002500
             03 MNLIEUP PIC X.                                          00002510
             03 MNLIEUH PIC X.                                          00002520
             03 MNLIEUV PIC X.                                          00002530
             03 MNLIEUO      PIC X(3).                                  00002540
             03 FILLER       PIC X(2).                                  00002550
             03 MNAVOIRA     PIC X.                                     00002560
             03 MNAVOIRC     PIC X.                                     00002570
             03 MNAVOIRP     PIC X.                                     00002580
             03 MNAVOIRH     PIC X.                                     00002590
             03 MNAVOIRV     PIC X.                                     00002600
             03 MNAVOIRO     PIC X(8).                                  00002610
      * date d'�mission                                                 00002620
             03 FILLER       PIC X(2).                                  00002630
             03 MDEMISA      PIC X.                                     00002640
             03 MDEMISC PIC X.                                          00002650
             03 MDEMISP PIC X.                                          00002660
             03 MDEMISH PIC X.                                          00002670
             03 MDEMISV PIC X.                                          00002680
             03 MDEMISO      PIC X(10).                                 00002690
      * code motif                                                      00002700
             03 FILLER       PIC X(2).                                  00002710
             03 MCMOTIFAVA   PIC X.                                     00002720
             03 MCMOTIFAVC   PIC X.                                     00002730
             03 MCMOTIFAVP   PIC X.                                     00002740
             03 MCMOTIFAVH   PIC X.                                     00002750
             03 MCMOTIFAVV   PIC X.                                     00002760
             03 MCMOTIFAVO   PIC X(5).                                  00002770
      * nom                                                             00002780
             03 FILLER       PIC X(2).                                  00002790
             03 MLNOMA  PIC X.                                          00002800
             03 MLNOMC  PIC X.                                          00002810
             03 MLNOMP  PIC X.                                          00002820
             03 MLNOMH  PIC X.                                          00002830
             03 MLNOMV  PIC X.                                          00002840
             03 MLNOMO  PIC X(20).                                      00002850
      * montant                                                         00002860
             03 FILLER       PIC X(2).                                  00002870
             03 MPMONTANTA   PIC X.                                     00002880
             03 MPMONTANTC   PIC X.                                     00002890
             03 MPMONTANTP   PIC X.                                     00002900
             03 MPMONTANTH   PIC X.                                     00002910
             03 MPMONTANTV   PIC X.                                     00002920
             03 MPMONTANTO   PIC X(10).                                 00002930
      * flag annulation                                                 00002940
             03 FILLER       PIC X(2).                                  00002950
             03 MCANNULA     PIC X.                                     00002960
             03 MCANNULC     PIC X.                                     00002970
             03 MCANNULP     PIC X.                                     00002980
             03 MCANNULH     PIC X.                                     00002990
             03 MCANNULV     PIC X.                                     00003000
             03 MCANNULO     PIC X.                                     00003010
      * flag statut utilisation                                         00003020
             03 FILLER       PIC X(2).                                  00003030
             03 MWUTILA      PIC X.                                     00003040
             03 MWUTILC PIC X.                                          00003050
             03 MWUTILP PIC X.                                          00003060
             03 MWUTILH PIC X.                                          00003070
             03 MWUTILV PIC X.                                          00003080
             03 MWUTILO      PIC X.                                     00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MLIBERRA  PIC X.                                          00003110
           02 MLIBERRC  PIC X.                                          00003120
           02 MLIBERRP  PIC X.                                          00003130
           02 MLIBERRH  PIC X.                                          00003140
           02 MLIBERRV  PIC X.                                          00003150
           02 MLIBERRO  PIC X(78).                                      00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MCODTRAA  PIC X.                                          00003180
           02 MCODTRAC  PIC X.                                          00003190
           02 MCODTRAP  PIC X.                                          00003200
           02 MCODTRAH  PIC X.                                          00003210
           02 MCODTRAV  PIC X.                                          00003220
           02 MCODTRAO  PIC X(4).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MCICSA    PIC X.                                          00003250
           02 MCICSC    PIC X.                                          00003260
           02 MCICSP    PIC X.                                          00003270
           02 MCICSH    PIC X.                                          00003280
           02 MCICSV    PIC X.                                          00003290
           02 MCICSO    PIC X(5).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MNETNAMA  PIC X.                                          00003320
           02 MNETNAMC  PIC X.                                          00003330
           02 MNETNAMP  PIC X.                                          00003340
           02 MNETNAMH  PIC X.                                          00003350
           02 MNETNAMV  PIC X.                                          00003360
           02 MNETNAMO  PIC X(8).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MSCREENA  PIC X.                                          00003390
           02 MSCREENC  PIC X.                                          00003400
           02 MSCREENP  PIC X.                                          00003410
           02 MSCREENH  PIC X.                                          00003420
           02 MSCREENV  PIC X.                                          00003430
           02 MSCREENO  PIC X(4).                                       00003440
                                                                                
