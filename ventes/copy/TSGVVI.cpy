      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * --------------------------------------------------------------  00010000
      * VENTE INTERFILIALE                                              00020000
      * --------------------------------------------------------------- 00030000
       01 TSGVVI-DONNEES.                                               00040000
             10 TSGVVI-NSOCO                PIC X(03).                  00080000
             10 TSGVVI-NLIEUO               PIC X(03).                  00081000
             10 TSGVVI-NSOCV                PIC X(03).                  00082000
             10 TSGVVI-NLIEUV               PIC X(03).                  00083000
             10 TSGVVI-NVENTE               PIC X(07).                  00084000
             10 TSGVVI-DVENTE               PIC X(08).                  00085000
             10 TSGVVI-DOSSIER              PIC X(22).                  00086012
             10 TSGVVI-CVENDEURO            PIC X(06).                  00090000
             10 TSGVVI-CVENDEURG            PIC X(06).                  00100000
             10 TSGVVI-LVENDEURG            PIC X(15).                  00101010
                                                                                
