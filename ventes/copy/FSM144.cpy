      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * COPIE UTILISE DANS LE BSM144 + BSM145                                   
       01 DSECT-FSM144.                                                         
          02 FSM144-DONNEES.                                                    
             05 FSM144-NCODIC          PIC X(07).                               
             05 FSM144-CFAM            PIC X(05).                               
             05 FSM144-LFAM            PIC X(20).                               
             05 FSM144-LVMARKET1       PIC X(20).                               
             05 FSM144-LVMARKET2       PIC X(20).                               
             05 FSM144-LVMARKET3       PIC X(20).                               
             05 FSM144-WSEQFAM         PIC S9(5) COMP-3.                        
             05 FSM144-CHEFPROD        PIC X(05).                               
             05 FSM144-CODICREMPLACANT PIC X(07).                               
             05 FSM144-CODICREMPLACE   PIC X(07).                               
             05 FSM144-PREFTTC         PIC S9(05)V99 COMP-3.                    
             05 FSM144-WOA             PIC X(01).                               
             05 FSM144-CMARQ           PIC X(05).                               
             05 FSM144-LREFFOURN       PIC X(20).                               
             05 FSM144-LSTATCOMP       PIC X(03).                               
             05 FSM144-WDACEM          PIC X(01).                               
             05 FSM144-CEXPO           PIC X(01).                               
             05 FSM144-NSOCIETE        PIC X(03).                               
             05 FSM144-NSOCDEPOT       PIC X(03).                               
             05 FSM144-NDEPOT          PIC X(03).                               
             05 FSM144-CAPPRO          PIC X(03).                               
             05 FSM144-WSENSAPPRO      PIC X(01).                               
             05 FSM144-DEFFET-EP       PIC X(08).                               
             05 FSM144-DCDE            PIC X(08).                               
             05 FSM144-QCDE            PIC S9(05) COMP-3.                       
             05 FSM144-ETOILE          PIC X(01).                               
             05 FSM144-QSTOCKDEP       PIC S9(05) COMP-3.                       
             05 FSM144-A-RETESTER      PIC X(01).                               
             05 FSM144-QPIECES         PIC S9(05) COMP-3.                       
             05 FSM144-QSTOCKMAG       PIC S9(05) COMP-3.                       
             05 FSM144-NLIEU           PIC X(03).                               
             05 FSM144-GRP             PIC X(02).                               
             05 FSM144-PSTDTTC         PIC S9(7)V9(2) COMP-3.                   
             05 FSM144-PCOMMART        PIC S9(5)V9(2) COMP-3.                   
             05 FSM144-GAMME           PIC X(5).                                
             05 FSM144-NSOCZON         PIC X(5).                                
             05 FSM144-NSEQ            PIC S9(3)      COMP-3.                   
                                                                                
