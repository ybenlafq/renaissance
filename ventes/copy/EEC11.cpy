      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Contr�le Commandes WebSph�re                                    00000020
      ***************************************************************** 00000030
       01   EEC11I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNCDEWCL      COMP PIC S9(4).                            00000140
      *--                                                                       
           02 MRNCDEWCL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRNCDEWCF      PIC X.                                     00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MRNCDEWCI      PIC X(8).                                  00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTPAGEAL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MTPAGEAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTPAGEAF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MTPAGEAI  PIC X(4).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MPAGEAI   PIC X(2).                                       00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTNBPL    COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MTNBPL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MTNBPF    PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MTNBPI    PIC X(2).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNBPI     PIC X(2).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRSTATL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MRSTATL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRSTATF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MRSTATI   PIC X(7).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MROPERL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MROPERL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MROPERF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MROPERI   PIC X(4).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRDCREL   COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MRDCREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRDCREF   PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MRDCREI   PIC X(8).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRHCREL   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MRHCREL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MRHCREF   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MRHCREI   PIC X(5).                                       00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNOML    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MRNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MRNOMF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MRNOMI    PIC X(23).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNSOCIETEL    COMP PIC S9(4).                            00000540
      *--                                                                       
           02 MRNSOCIETEL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MRNSOCIETEF    PIC X.                                     00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MRNSOCIETEI    PIC X(3).                                  00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNLIEUL  COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MRNLIEUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MRNLIEUF  PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MRNLIEUI  PIC X(3).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MRNVENTEL      COMP PIC S9(4).                            00000620
      *--                                                                       
           02 MRNVENTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MRNVENTEF      PIC X.                                     00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MRNVENTEI      PIC X(7).                                  00000650
           02 MLIGNESI OCCURS   13 TIMES .                              00000660
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MSTATL  COMP PIC S9(4).                                 00000670
      *--                                                                       
             03 MSTATL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MSTATF  PIC X.                                          00000680
             03 FILLER  PIC X(4).                                       00000690
             03 MSTATI  PIC X.                                          00000700
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MTEXTEL      COMP PIC S9(4).                            00000710
      *--                                                                       
             03 MTEXTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MTEXTEF      PIC X.                                     00000720
             03 FILLER  PIC X(4).                                       00000730
             03 MTEXTEI      PIC X(62).                                 00000740
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDCREL  COMP PIC S9(4).                                 00000750
      *--                                                                       
             03 MDCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MDCREF  PIC X.                                          00000760
             03 FILLER  PIC X(4).                                       00000770
             03 MDCREI  PIC X(8).                                       00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MHCREL  COMP PIC S9(4).                                 00000790
      *--                                                                       
             03 MHCREL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MHCREF  PIC X.                                          00000800
             03 FILLER  PIC X(4).                                       00000810
             03 MHCREI  PIC X(5).                                       00000820
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000830
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000840
           02 FILLER    PIC X(4).                                       00000850
           02 MLIBERRI  PIC X(79).                                      00000860
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000870
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000880
           02 FILLER    PIC X(4).                                       00000890
           02 MCODTRAI  PIC X(4).                                       00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000910
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000920
           02 FILLER    PIC X(4).                                       00000930
           02 MZONCMDI  PIC X(15).                                      00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MCICSI    PIC X(5).                                       00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MNETNAMI  PIC X(8).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MSCREENI  PIC X(4).                                       00001060
      ***************************************************************** 00001070
      * Contr�le Commandes WebSph�re                                    00001080
      ***************************************************************** 00001090
       01   EEC11O REDEFINES EEC11I.                                    00001100
           02 FILLER    PIC X(12).                                      00001110
           02 FILLER    PIC X(2).                                       00001120
           02 MDATJOUA  PIC X.                                          00001130
           02 MDATJOUC  PIC X.                                          00001140
           02 MDATJOUP  PIC X.                                          00001150
           02 MDATJOUH  PIC X.                                          00001160
           02 MDATJOUV  PIC X.                                          00001170
           02 MDATJOUO  PIC X(10).                                      00001180
           02 FILLER    PIC X(2).                                       00001190
           02 MTIMJOUA  PIC X.                                          00001200
           02 MTIMJOUC  PIC X.                                          00001210
           02 MTIMJOUP  PIC X.                                          00001220
           02 MTIMJOUH  PIC X.                                          00001230
           02 MTIMJOUV  PIC X.                                          00001240
           02 MTIMJOUO  PIC X(5).                                       00001250
           02 FILLER    PIC X(2).                                       00001260
           02 MRNCDEWCA      PIC X.                                     00001270
           02 MRNCDEWCC PIC X.                                          00001280
           02 MRNCDEWCP PIC X.                                          00001290
           02 MRNCDEWCH PIC X.                                          00001300
           02 MRNCDEWCV PIC X.                                          00001310
           02 MRNCDEWCO      PIC Z(7)9.                                 00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MTPAGEAA  PIC X.                                          00001340
           02 MTPAGEAC  PIC X.                                          00001350
           02 MTPAGEAP  PIC X.                                          00001360
           02 MTPAGEAH  PIC X.                                          00001370
           02 MTPAGEAV  PIC X.                                          00001380
           02 MTPAGEAO  PIC X(4).                                       00001390
           02 FILLER    PIC X(2).                                       00001400
           02 MPAGEAA   PIC X.                                          00001410
           02 MPAGEAC   PIC X.                                          00001420
           02 MPAGEAP   PIC X.                                          00001430
           02 MPAGEAH   PIC X.                                          00001440
           02 MPAGEAV   PIC X.                                          00001450
           02 MPAGEAO   PIC Z9.                                         00001460
           02 FILLER    PIC X(2).                                       00001470
           02 MTNBPA    PIC X.                                          00001480
           02 MTNBPC    PIC X.                                          00001490
           02 MTNBPP    PIC X.                                          00001500
           02 MTNBPH    PIC X.                                          00001510
           02 MTNBPV    PIC X.                                          00001520
           02 MTNBPO    PIC X(2).                                       00001530
           02 FILLER    PIC X(2).                                       00001540
           02 MNBPA     PIC X.                                          00001550
           02 MNBPC     PIC X.                                          00001560
           02 MNBPP     PIC X.                                          00001570
           02 MNBPH     PIC X.                                          00001580
           02 MNBPV     PIC X.                                          00001590
           02 MNBPO     PIC Z9.                                         00001600
           02 FILLER    PIC X(2).                                       00001610
           02 MRSTATA   PIC X.                                          00001620
           02 MRSTATC   PIC X.                                          00001630
           02 MRSTATP   PIC X.                                          00001640
           02 MRSTATH   PIC X.                                          00001650
           02 MRSTATV   PIC X.                                          00001660
           02 MRSTATO   PIC X(7).                                       00001670
           02 FILLER    PIC X(2).                                       00001680
           02 MROPERA   PIC X.                                          00001690
           02 MROPERC   PIC X.                                          00001700
           02 MROPERP   PIC X.                                          00001710
           02 MROPERH   PIC X.                                          00001720
           02 MROPERV   PIC X.                                          00001730
           02 MROPERO   PIC X(4).                                       00001740
           02 FILLER    PIC X(2).                                       00001750
           02 MRDCREA   PIC X.                                          00001760
           02 MRDCREC   PIC X.                                          00001770
           02 MRDCREP   PIC X.                                          00001780
           02 MRDCREH   PIC X.                                          00001790
           02 MRDCREV   PIC X.                                          00001800
           02 MRDCREO   PIC X(8).                                       00001810
           02 FILLER    PIC X(2).                                       00001820
           02 MRHCREA   PIC X.                                          00001830
           02 MRHCREC   PIC X.                                          00001840
           02 MRHCREP   PIC X.                                          00001850
           02 MRHCREH   PIC X.                                          00001860
           02 MRHCREV   PIC X.                                          00001870
           02 MRHCREO   PIC X(5).                                       00001880
           02 FILLER    PIC X(2).                                       00001890
           02 MRNOMA    PIC X.                                          00001900
           02 MRNOMC    PIC X.                                          00001910
           02 MRNOMP    PIC X.                                          00001920
           02 MRNOMH    PIC X.                                          00001930
           02 MRNOMV    PIC X.                                          00001940
           02 MRNOMO    PIC X(23).                                      00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MRNSOCIETEA    PIC X.                                     00001970
           02 MRNSOCIETEC    PIC X.                                     00001980
           02 MRNSOCIETEP    PIC X.                                     00001990
           02 MRNSOCIETEH    PIC X.                                     00002000
           02 MRNSOCIETEV    PIC X.                                     00002010
           02 MRNSOCIETEO    PIC X(3).                                  00002020
           02 FILLER    PIC X(2).                                       00002030
           02 MRNLIEUA  PIC X.                                          00002040
           02 MRNLIEUC  PIC X.                                          00002050
           02 MRNLIEUP  PIC X.                                          00002060
           02 MRNLIEUH  PIC X.                                          00002070
           02 MRNLIEUV  PIC X.                                          00002080
           02 MRNLIEUO  PIC X(3).                                       00002090
           02 FILLER    PIC X(2).                                       00002100
           02 MRNVENTEA      PIC X.                                     00002110
           02 MRNVENTEC PIC X.                                          00002120
           02 MRNVENTEP PIC X.                                          00002130
           02 MRNVENTEH PIC X.                                          00002140
           02 MRNVENTEV PIC X.                                          00002150
           02 MRNVENTEO      PIC X(7).                                  00002160
           02 MLIGNESO OCCURS   13 TIMES .                              00002170
             03 FILLER       PIC X(2).                                  00002180
             03 MSTATA  PIC X.                                          00002190
             03 MSTATC  PIC X.                                          00002200
             03 MSTATP  PIC X.                                          00002210
             03 MSTATH  PIC X.                                          00002220
             03 MSTATV  PIC X.                                          00002230
             03 MSTATO  PIC X.                                          00002240
             03 FILLER       PIC X(2).                                  00002250
             03 MTEXTEA      PIC X.                                     00002260
             03 MTEXTEC PIC X.                                          00002270
             03 MTEXTEP PIC X.                                          00002280
             03 MTEXTEH PIC X.                                          00002290
             03 MTEXTEV PIC X.                                          00002300
             03 MTEXTEO      PIC X(62).                                 00002310
             03 FILLER       PIC X(2).                                  00002320
             03 MDCREA  PIC X.                                          00002330
             03 MDCREC  PIC X.                                          00002340
             03 MDCREP  PIC X.                                          00002350
             03 MDCREH  PIC X.                                          00002360
             03 MDCREV  PIC X.                                          00002370
             03 MDCREO  PIC X(8).                                       00002380
             03 FILLER       PIC X(2).                                  00002390
             03 MHCREA  PIC X.                                          00002400
             03 MHCREC  PIC X.                                          00002410
             03 MHCREP  PIC X.                                          00002420
             03 MHCREH  PIC X.                                          00002430
             03 MHCREV  PIC X.                                          00002440
             03 MHCREO  PIC X(5).                                       00002450
           02 FILLER    PIC X(2).                                       00002460
           02 MLIBERRA  PIC X.                                          00002470
           02 MLIBERRC  PIC X.                                          00002480
           02 MLIBERRP  PIC X.                                          00002490
           02 MLIBERRH  PIC X.                                          00002500
           02 MLIBERRV  PIC X.                                          00002510
           02 MLIBERRO  PIC X(79).                                      00002520
           02 FILLER    PIC X(2).                                       00002530
           02 MCODTRAA  PIC X.                                          00002540
           02 MCODTRAC  PIC X.                                          00002550
           02 MCODTRAP  PIC X.                                          00002560
           02 MCODTRAH  PIC X.                                          00002570
           02 MCODTRAV  PIC X.                                          00002580
           02 MCODTRAO  PIC X(4).                                       00002590
           02 FILLER    PIC X(2).                                       00002600
           02 MZONCMDA  PIC X.                                          00002610
           02 MZONCMDC  PIC X.                                          00002620
           02 MZONCMDP  PIC X.                                          00002630
           02 MZONCMDH  PIC X.                                          00002640
           02 MZONCMDV  PIC X.                                          00002650
           02 MZONCMDO  PIC X(15).                                      00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCICSA    PIC X.                                          00002680
           02 MCICSC    PIC X.                                          00002690
           02 MCICSP    PIC X.                                          00002700
           02 MCICSH    PIC X.                                          00002710
           02 MCICSV    PIC X.                                          00002720
           02 MCICSO    PIC X(5).                                       00002730
           02 FILLER    PIC X(2).                                       00002740
           02 MNETNAMA  PIC X.                                          00002750
           02 MNETNAMC  PIC X.                                          00002760
           02 MNETNAMP  PIC X.                                          00002770
           02 MNETNAMH  PIC X.                                          00002780
           02 MNETNAMV  PIC X.                                          00002790
           02 MNETNAMO  PIC X(8).                                       00002800
           02 FILLER    PIC X(2).                                       00002810
           02 MSCREENA  PIC X.                                          00002820
           02 MSCREENC  PIC X.                                          00002830
           02 MSCREENP  PIC X.                                          00002840
           02 MSCREENH  PIC X.                                          00002850
           02 MSCREENV  PIC X.                                          00002860
           02 MSCREENO  PIC X(4).                                       00002870
                                                                                
