      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSBA03 = TS D'EDITION DES BA                               *         
      *                                                               *         
      *    CETTE TS CONTIENT LES SEQUENCES DE BA A EDITER             *         
      *    1 SEQUENCE = QUANTITE, MONTANT, CODIC                      *         
      *                                                               *         
      *    LONGUEUR TOTALE = 5701                                     *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  ITS03                      PIC S9(05) COMP-3 VALUE  +0.              
       01  ITS03-MAX                  PIC S9(05) COMP-3 VALUE +39.              
      *                                                               *         
       01  TS03-IDENT.                                                          
           05 FILLER                  PIC X(04)       VALUE 'BA03'.             
           05 TS03-TERM               PIC X(04)       VALUE SPACES.             
      *                                                               *         
      *01  TS03-LONG                  PIC S9(04) COMP VALUE +4290.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  TS03-LONG                  PIC S9(04) COMP VALUE +5701.              
      *--                                                                       
       01  TS03-LONG                  PIC S9(04) COMP-5 VALUE +5701.            
      *}                                                                        
      *                                                               *         
       01  TS03-DONNEES.                                                        
           05 TS03-NPOSTE             OCCURS 39.                                
              10 TS03-QBA             PIC 9(04).                                
              10 TS03-PBA             PIC 9(05)V99.                             
              10 TS03-NCODIC          PIC X(07).                                
              10 TS03-NCODIC-9        REDEFINES TS03-NCODIC PIC 9(07).          
              10 TS03-NBADEB          PIC 9(06).                                
              10 TS03-NBAFIN          PIC 9(06).                                
              10 TS03-NCTL            PIC X(04).                                
              10 TS03-LVALEUR         PIC X(27).                                
              10 TS03-ARTICLE.                                                  
                 15 TS03-LFAM         PIC X(20).                                
                 15 TS03-CMARQ        PIC X(05).                                
                 15 TS03-LREF         PIC X(20).                                
              10 TS03-PBA2            PIC 9(05)V99.                             
              10 TS03-NCTL2           PIC X(04).                                
              10 TS03-LVALEUR2        PIC X(29).                                
           05 TS03-WEMISEURO          PIC X(01).                                
           05 TS03-PTAUX              PIC S9(05)V9(05) COMP-3.                  
                                                                                
