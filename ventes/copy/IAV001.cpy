      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IAV001 AU 30/08/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,03,BI,A,                          *        
      *                           13,05,BI,A,                          *        
      *                           18,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IAV001.                                                        
            05 NOMETAT-IAV001           PIC X(6) VALUE 'IAV001'.                
            05 RUPTURES-IAV001.                                                 
           10 IAV001-NSOCIETE           PIC X(03).                      007  003
           10 IAV001-NLIEU              PIC X(03).                      010  003
           10 IAV001-CMOTIF             PIC X(05).                      013  005
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IAV001-SEQUENCE           PIC S9(04) COMP.                018  002
      *--                                                                       
           10 IAV001-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IAV001.                                                   
           10 IAV001-CANNULATION        PIC X(01).                      020  001
           10 IAV001-CDEVISE            PIC X(03).                      021  003
           10 IAV001-CTYPAVOIR          PIC X(01).                      024  001
           10 IAV001-FMOIS              PIC X(07).                      025  007
           10 IAV001-LDEVISE            PIC X(05).                      032  005
           10 IAV001-LLIEU              PIC X(20).                      037  020
           10 IAV001-TOT-CMOTIF         PIC X(05).                      057  005
           10 IAV001-TOT-LMOTIF         PIC X(20).                      062  020
           10 IAV001-WUTIL              PIC X(01).                      082  001
           10 IAV001-NAVOIR             PIC 9(07)     .                 083  007
           10 IAV001-NBR-AVOIR-MOTIF    PIC S9(05)      COMP-3.         090  003
           10 IAV001-DEMIS              PIC X(08).                      093  008
           10 IAV001-PMONTANT           PIC S9(08)V9(2).                101  010
           10 IAV001-PMONTANT1          PIC S9(08)V9(2).                111  010
            05 FILLER                      PIC X(392).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IAV001-LONG           PIC S9(4)   COMP  VALUE +120.           
      *                                                                         
      *--                                                                       
        01  DSECT-IAV001-LONG           PIC S9(4) COMP-5  VALUE +120.           
                                                                                
      *}                                                                        
