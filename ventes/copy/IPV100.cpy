      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPV100 AU 11/12/2001  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,05,BI,A,                          *        
      *                           15,03,PD,A,                          *        
      *                           18,05,PD,A,                          *        
      *                           23,07,BI,A,                          *        
      *                           30,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPV100.                                                        
            05 NOMETAT-IPV100           PIC X(6) VALUE 'IPV100'.                
            05 RUPTURES-IPV100.                                                 
           10 IPV100-NSOCIETE           PIC X(03).                      007  003
           10 IPV100-CHEFPROD           PIC X(05).                      010  005
           10 IPV100-WSEQFAM            PIC S9(05)      COMP-3.         015  003
           10 IPV100-PVTEFRF            PIC S9(07)V9(2) COMP-3.         018  005
           10 IPV100-NCODIC             PIC X(07).                      023  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPV100-SEQUENCE           PIC S9(04) COMP.                030  002
      *--                                                                       
           10 IPV100-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPV100.                                                   
           10 IPV100-CFAM               PIC X(05).                      032  005
           10 IPV100-CMARQ              PIC X(05).                      037  005
           10 IPV100-FLAG               PIC X(01).                      042  001
           10 IPV100-LCHEFPROD          PIC X(20).                      043  020
           10 IPV100-LFAM               PIC X(20).                      063  020
           10 IPV100-LREFFOURN          PIC X(20).                      083  020
           10 IPV100-CENT               PIC 9(03)     .                 103  003
           10 IPV100-DIXMILLE           PIC 9(05)     .                 106  005
           10 IPV100-PCOMMARTF          PIC S9(05)V9(2) COMP-3.         111  004
           10 IPV100-PCOMMVOLF          PIC S9(05)V9(2) COMP-3.         115  004
           10 IPV100-PVTE4S             PIC S9(09)      COMP-3.         119  005
           10 IPV100-TXEURO             PIC S9(01)V9(5) COMP-3.         124  004
           10 IPV100-TXFRF              PIC S9(01)V9(5) COMP-3.         128  004
            05 FILLER                      PIC X(381).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPV100-LONG           PIC S9(4)   COMP  VALUE +131.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPV100-LONG           PIC S9(4) COMP-5  VALUE +131.           
                                                                                
      *}                                                                        
