      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00010001
      *   COPY DE LA TABLE RVVT0000                                     00020002
      **********************************************************        00030001
      *                                                                 00040001
      *---------------------------------------------------------        00050001
      *   LISTE DES HOST VARIABLES DE LA TABLE RVVT0000                 00060002
      *---------------------------------------------------------        00070001
      *                                                                 00080001
       01  RVVT0000.                                                    00090002
           02  VT00-NOMTABLE                                            00100002
               PIC X(0006).                                             00110002
           02  VT00-NFLAG                                               00120002
               PIC X(0001).                                             00130002
           02  VT00-NPART                                               00140002
               PIC X(0002).                                             00150002
           02  VT00-NCHRONO                                             00160002
               PIC S9(11) COMP-3.                                       00170002
           02  VT00-STKANNEE                                            00180002
               PIC X(0004).                                             00190002
           02  VT00-STKTRIMESTRE                                        00200002
               PIC X(0001).                                             00210002
           02  VT00-STKDATEDEB                                          00220002
               PIC X(0008).                                             00230002
           02  VT00-STKDATEFIN                                          00240002
               PIC X(0008).                                             00250002
           02  VT00-CLECOMMIT                                           00260002
               PIC X(0021).                                             00270002
           02  VT00-WACTIF                                              00280002
               PIC X(0001).                                             00290002
      *                                                                 00300001
      *---------------------------------------------------------        00310001
      *   LISTE DES FLAGS DE LA TABLE RVVT0000                          00320002
      *---------------------------------------------------------        00330001
      *                                                                 00340001
       01  RVVT0000-FLAGS.                                              00350002
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-NOMTABLE-F                                          00360002
      *        PIC S9(4) COMP.                                          00370001
      *--                                                                       
           02  VT00-NOMTABLE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-NFLAG-F                                             00380002
      *        PIC S9(4) COMP.                                          00390002
      *--                                                                       
           02  VT00-NFLAG-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-NPART-F                                             00400002
      *        PIC S9(4) COMP.                                          00410001
      *--                                                                       
           02  VT00-NPART-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-NCHRONO-F                                           00420002
      *        PIC S9(4) COMP.                                          00430001
      *--                                                                       
           02  VT00-NCHRONO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-STKANNEE-F                                          00440002
      *        PIC S9(4) COMP.                                          00450001
      *--                                                                       
           02  VT00-STKANNEE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-STKTRIMESTRE-F                                      00460002
      *        PIC S9(4) COMP.                                          00470001
      *--                                                                       
           02  VT00-STKTRIMESTRE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-STKDATEDEB-F                                        00480002
      *        PIC S9(4) COMP.                                          00490001
      *--                                                                       
           02  VT00-STKDATEDEB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-STKDATEFIN-F                                        00500002
      *        PIC S9(4) COMP.                                          00510001
      *--                                                                       
           02  VT00-STKDATEFIN-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-CLECOMMIT-F                                         00520002
      *        PIC S9(4) COMP.                                          00530001
      *--                                                                       
           02  VT00-CLECOMMIT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VT00-WACTIF-F                                            00540002
      *        PIC S9(4) COMP.                                          00550001
      *--                                                                       
           02  VT00-WACTIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00560001
                                                                                
