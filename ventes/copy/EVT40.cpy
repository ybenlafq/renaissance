      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EVT40   EVT40                                              00000020
      ***************************************************************** 00000030
       01   EVT40I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCL    COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MNSOCL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNSOCF    PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MNSOCI    PIC X(3).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MREFMAGL  COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MREFMAGL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MREFMAGF  PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MREFMAGI  PIC X(3).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000220
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MZONCMDI  PIC X.                                          00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTVL  COMP PIC S9(4).                                 00000260
      *--                                                                       
           02 MNVENTVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTVF  PIC X.                                          00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNVENTVI  PIC X(7).                                       00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTYPVTEL      COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MNTYPVTEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNTYPVTEF      PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNTYPVTEI      PIC X(3).                                  00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVTEJJL  COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MDVTEJJL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVTEJJF  PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MDVTEJJI  PIC X(2).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVTEMML  COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MDVTEMML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVTEMMF  PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MDVTEMMI  PIC X(2).                                       00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDVTEAAL  COMP PIC S9(4).                                 00000420
      *--                                                                       
           02 MDVTEAAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDVTEAAF  PIC X.                                          00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MDVTEAAI  PIC X(4).                                       00000450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOM1L   COMP PIC S9(4).                                 00000460
      *--                                                                       
           02 MLNOM1L COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLNOM1F   PIC X.                                          00000470
           02 FILLER    PIC X(4).                                       00000480
           02 MLNOM1I   PIC X(22).                                      00000490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTELL    COMP PIC S9(4).                                 00000500
      *--                                                                       
           02 MNTELL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNTELF    PIC X.                                          00000510
           02 FILLER    PIC X(4).                                       00000520
           02 MNTELI    PIC X(10).                                      00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODOFL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MNCODOFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODOFF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MNCODOFI  PIC X(7).                                       00000570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNFAML    COMP PIC S9(4).                                 00000580
      *--                                                                       
           02 MNFAML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MNFAMF    PIC X.                                          00000590
           02 FILLER    PIC X(4).                                       00000600
           02 MNFAMI    PIC X(5).                                       00000610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNMQFNSL  COMP PIC S9(4).                                 00000620
      *--                                                                       
           02 MNMQFNSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNMQFNSF  PIC X.                                          00000630
           02 FILLER    PIC X(4).                                       00000640
           02 MNMQFNSI  PIC X(5).                                       00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERJDL  COMP PIC S9(4).                                 00000660
      *--                                                                       
           02 MDPERJDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERJDF  PIC X.                                          00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MDPERJDI  PIC X(2).                                       00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERMDL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDPERMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERMDF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDPERMDI  PIC X(2).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERADL  COMP PIC S9(4).                                 00000740
      *--                                                                       
           02 MDPERADL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERADF  PIC X.                                          00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MDPERADI  PIC X(4).                                       00000770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERJFL  COMP PIC S9(4).                                 00000780
      *--                                                                       
           02 MDPERJFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERJFF  PIC X.                                          00000790
           02 FILLER    PIC X(4).                                       00000800
           02 MDPERJFI  PIC X(2).                                       00000810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERMFL  COMP PIC S9(4).                                 00000820
      *--                                                                       
           02 MDPERMFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERMFF  PIC X.                                          00000830
           02 FILLER    PIC X(4).                                       00000840
           02 MDPERMFI  PIC X(2).                                       00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDPERAFL  COMP PIC S9(4).                                 00000860
      *--                                                                       
           02 MDPERAFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDPERAFF  PIC X.                                          00000870
           02 FILLER    PIC X(4).                                       00000880
           02 MDPERAFI  PIC X(4).                                       00000890
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBJL   COMP PIC S9(4).                                 00000900
      *--                                                                       
           02 MDDEBJL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEBJF   PIC X.                                          00000910
           02 FILLER    PIC X(4).                                       00000920
           02 MDDEBJI   PIC X(2).                                       00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBML   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MDDEBML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEBMF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDDEBMI   PIC X(2).                                       00000970
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDEBAL   COMP PIC S9(4).                                 00000980
      *--                                                                       
           02 MDDEBAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDDEBAF   PIC X.                                          00000990
           02 FILLER    PIC X(4).                                       00001000
           02 MDDEBAI   PIC X(4).                                       00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINJL   COMP PIC S9(4).                                 00001020
      *--                                                                       
           02 MDFINJL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFINJF   PIC X.                                          00001030
           02 FILLER    PIC X(4).                                       00001040
           02 MDFINJI   PIC X(2).                                       00001050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINML   COMP PIC S9(4).                                 00001060
      *--                                                                       
           02 MDFINML COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFINMF   PIC X.                                          00001070
           02 FILLER    PIC X(4).                                       00001080
           02 MDFINMI   PIC X(2).                                       00001090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDFINAL   COMP PIC S9(4).                                 00001100
      *--                                                                       
           02 MDFINAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDFINAF   PIC X.                                          00001110
           02 FILLER    PIC X(4).                                       00001120
           02 MDFINAI   PIC X(4).                                       00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPERIODEL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MPERIODEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPERIODEF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MPERIODEI      PIC X(3).                                  00001170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCAISL   COMP PIC S9(4).                                 00001180
      *--                                                                       
           02 MNCAISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNCAISF   PIC X.                                          00001190
           02 FILLER    PIC X(4).                                       00001200
           02 MNCAISI   PIC X(3).                                       00001210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTTRANSL  COMP PIC S9(4).                                 00001220
      *--                                                                       
           02 MTTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTTRANSF  PIC X.                                          00001230
           02 FILLER    PIC X(4).                                       00001240
           02 MTTRANSI  PIC X(3).                                       00001250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNTRANSL  COMP PIC S9(4).                                 00001260
      *--                                                                       
           02 MNTRANSL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNTRANSF  PIC X.                                          00001270
           02 FILLER    PIC X(4).                                       00001280
           02 MNTRANSI  PIC X(8).                                       00001290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODDELL  COMP PIC S9(4).                                 00001300
      *--                                                                       
           02 MMODDELL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODDELF  PIC X.                                          00001310
           02 FILLER    PIC X(4).                                       00001320
           02 MMODDELI  PIC X(3).                                       00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPMONTL   COMP PIC S9(4).                                 00001340
      *--                                                                       
           02 MPMONTL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPMONTF   PIC X.                                          00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MPMONTI   PIC X(10).                                      00001370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODICL  COMP PIC S9(4).                                 00001380
      *--                                                                       
           02 MNCODICL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCODICF  PIC X.                                          00001390
           02 FILLER    PIC X(4).                                       00001400
           02 MNCODICI  PIC X(7).                                       00001410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00001420
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00001430
           02 FILLER    PIC X(4).                                       00001440
           02 MNVENTEI  PIC X(8).                                       00001450
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPVENTEL  COMP PIC S9(4).                                 00001460
      *--                                                                       
           02 MPVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPVENTEF  PIC X.                                          00001470
           02 FILLER    PIC X(4).                                       00001480
           02 MPVENTEI  PIC X(10).                                      00001490
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPVTEPAIL      COMP PIC S9(4).                            00001500
      *--                                                                       
           02 MPVTEPAIL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MPVTEPAIF      PIC X.                                     00001510
           02 FILLER    PIC X(4).                                       00001520
           02 MPVTEPAII      PIC X(10).                                 00001530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MMODPAIL  COMP PIC S9(4).                                 00001540
      *--                                                                       
           02 MMODPAIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MMODPAIF  PIC X.                                          00001550
           02 FILLER    PIC X(4).                                       00001560
           02 MMODPAII  PIC X(5).                                       00001570
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODDEVL  COMP PIC S9(4).                                 00001580
      *--                                                                       
           02 MCODDEVL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODDEVF  PIC X.                                          00001590
           02 FILLER    PIC X(4).                                       00001600
           02 MCODDEVI  PIC X.                                          00001610
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCARTEL  COMP PIC S9(4).                                 00001620
      *--                                                                       
           02 MNCARTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNCARTEF  PIC X.                                          00001630
           02 FILLER    PIC X(4).                                       00001640
           02 MNCARTEI  PIC X(9).                                       00001650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001660
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001670
           02 FILLER    PIC X(4).                                       00001680
           02 MLIBERRI  PIC X(78).                                      00001690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001700
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001710
           02 FILLER    PIC X(4).                                       00001720
           02 MCODTRAI  PIC X(4).                                       00001730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001740
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001750
           02 FILLER    PIC X(4).                                       00001760
           02 MCICSI    PIC X(5).                                       00001770
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001780
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001790
           02 FILLER    PIC X(4).                                       00001800
           02 MNETNAMI  PIC X(8).                                       00001810
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001820
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001830
           02 FILLER    PIC X(4).                                       00001840
           02 MSCREENI  PIC X(4).                                       00001850
      ***************************************************************** 00001860
      * SDF: EVT40   EVT40                                              00001870
      ***************************************************************** 00001880
       01   EVT40O REDEFINES EVT40I.                                    00001890
           02 FILLER    PIC X(12).                                      00001900
           02 FILLER    PIC X(2).                                       00001910
           02 MDATJOUA  PIC X.                                          00001920
           02 MDATJOUC  PIC X.                                          00001930
           02 MDATJOUP  PIC X.                                          00001940
           02 MDATJOUH  PIC X.                                          00001950
           02 MDATJOUV  PIC X.                                          00001960
           02 MDATJOUO  PIC X(10).                                      00001970
           02 FILLER    PIC X(2).                                       00001980
           02 MTIMJOUA  PIC X.                                          00001990
           02 MTIMJOUC  PIC X.                                          00002000
           02 MTIMJOUP  PIC X.                                          00002010
           02 MTIMJOUH  PIC X.                                          00002020
           02 MTIMJOUV  PIC X.                                          00002030
           02 MTIMJOUO  PIC X(5).                                       00002040
           02 FILLER    PIC X(2).                                       00002050
           02 MNSOCA    PIC X.                                          00002060
           02 MNSOCC    PIC X.                                          00002070
           02 MNSOCP    PIC X.                                          00002080
           02 MNSOCH    PIC X.                                          00002090
           02 MNSOCV    PIC X.                                          00002100
           02 MNSOCO    PIC X(3).                                       00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MREFMAGA  PIC X.                                          00002130
           02 MREFMAGC  PIC X.                                          00002140
           02 MREFMAGP  PIC X.                                          00002150
           02 MREFMAGH  PIC X.                                          00002160
           02 MREFMAGV  PIC X.                                          00002170
           02 MREFMAGO  PIC X(3).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MZONCMDA  PIC X.                                          00002200
           02 MZONCMDC  PIC X.                                          00002210
           02 MZONCMDP  PIC X.                                          00002220
           02 MZONCMDH  PIC X.                                          00002230
           02 MZONCMDV  PIC X.                                          00002240
           02 MZONCMDO  PIC X.                                          00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MNVENTVA  PIC X.                                          00002270
           02 MNVENTVC  PIC X.                                          00002280
           02 MNVENTVP  PIC X.                                          00002290
           02 MNVENTVH  PIC X.                                          00002300
           02 MNVENTVV  PIC X.                                          00002310
           02 MNVENTVO  PIC X(7).                                       00002320
           02 FILLER    PIC X(2).                                       00002330
           02 MNTYPVTEA      PIC X.                                     00002340
           02 MNTYPVTEC PIC X.                                          00002350
           02 MNTYPVTEP PIC X.                                          00002360
           02 MNTYPVTEH PIC X.                                          00002370
           02 MNTYPVTEV PIC X.                                          00002380
           02 MNTYPVTEO      PIC X(3).                                  00002390
           02 FILLER    PIC X(2).                                       00002400
           02 MDVTEJJA  PIC X.                                          00002410
           02 MDVTEJJC  PIC X.                                          00002420
           02 MDVTEJJP  PIC X.                                          00002430
           02 MDVTEJJH  PIC X.                                          00002440
           02 MDVTEJJV  PIC X.                                          00002450
           02 MDVTEJJO  PIC X(2).                                       00002460
           02 FILLER    PIC X(2).                                       00002470
           02 MDVTEMMA  PIC X.                                          00002480
           02 MDVTEMMC  PIC X.                                          00002490
           02 MDVTEMMP  PIC X.                                          00002500
           02 MDVTEMMH  PIC X.                                          00002510
           02 MDVTEMMV  PIC X.                                          00002520
           02 MDVTEMMO  PIC X(2).                                       00002530
           02 FILLER    PIC X(2).                                       00002540
           02 MDVTEAAA  PIC X.                                          00002550
           02 MDVTEAAC  PIC X.                                          00002560
           02 MDVTEAAP  PIC X.                                          00002570
           02 MDVTEAAH  PIC X.                                          00002580
           02 MDVTEAAV  PIC X.                                          00002590
           02 MDVTEAAO  PIC X(4).                                       00002600
           02 FILLER    PIC X(2).                                       00002610
           02 MLNOM1A   PIC X.                                          00002620
           02 MLNOM1C   PIC X.                                          00002630
           02 MLNOM1P   PIC X.                                          00002640
           02 MLNOM1H   PIC X.                                          00002650
           02 MLNOM1V   PIC X.                                          00002660
           02 MLNOM1O   PIC X(22).                                      00002670
           02 FILLER    PIC X(2).                                       00002680
           02 MNTELA    PIC X.                                          00002690
           02 MNTELC    PIC X.                                          00002700
           02 MNTELP    PIC X.                                          00002710
           02 MNTELH    PIC X.                                          00002720
           02 MNTELV    PIC X.                                          00002730
           02 MNTELO    PIC X(10).                                      00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MNCODOFA  PIC X.                                          00002760
           02 MNCODOFC  PIC X.                                          00002770
           02 MNCODOFP  PIC X.                                          00002780
           02 MNCODOFH  PIC X.                                          00002790
           02 MNCODOFV  PIC X.                                          00002800
           02 MNCODOFO  PIC X(7).                                       00002810
           02 FILLER    PIC X(2).                                       00002820
           02 MNFAMA    PIC X.                                          00002830
           02 MNFAMC    PIC X.                                          00002840
           02 MNFAMP    PIC X.                                          00002850
           02 MNFAMH    PIC X.                                          00002860
           02 MNFAMV    PIC X.                                          00002870
           02 MNFAMO    PIC X(5).                                       00002880
           02 FILLER    PIC X(2).                                       00002890
           02 MNMQFNSA  PIC X.                                          00002900
           02 MNMQFNSC  PIC X.                                          00002910
           02 MNMQFNSP  PIC X.                                          00002920
           02 MNMQFNSH  PIC X.                                          00002930
           02 MNMQFNSV  PIC X.                                          00002940
           02 MNMQFNSO  PIC X(5).                                       00002950
           02 FILLER    PIC X(2).                                       00002960
           02 MDPERJDA  PIC X.                                          00002970
           02 MDPERJDC  PIC X.                                          00002980
           02 MDPERJDP  PIC X.                                          00002990
           02 MDPERJDH  PIC X.                                          00003000
           02 MDPERJDV  PIC X.                                          00003010
           02 MDPERJDO  PIC X(2).                                       00003020
           02 FILLER    PIC X(2).                                       00003030
           02 MDPERMDA  PIC X.                                          00003040
           02 MDPERMDC  PIC X.                                          00003050
           02 MDPERMDP  PIC X.                                          00003060
           02 MDPERMDH  PIC X.                                          00003070
           02 MDPERMDV  PIC X.                                          00003080
           02 MDPERMDO  PIC X(2).                                       00003090
           02 FILLER    PIC X(2).                                       00003100
           02 MDPERADA  PIC X.                                          00003110
           02 MDPERADC  PIC X.                                          00003120
           02 MDPERADP  PIC X.                                          00003130
           02 MDPERADH  PIC X.                                          00003140
           02 MDPERADV  PIC X.                                          00003150
           02 MDPERADO  PIC X(4).                                       00003160
           02 FILLER    PIC X(2).                                       00003170
           02 MDPERJFA  PIC X.                                          00003180
           02 MDPERJFC  PIC X.                                          00003190
           02 MDPERJFP  PIC X.                                          00003200
           02 MDPERJFH  PIC X.                                          00003210
           02 MDPERJFV  PIC X.                                          00003220
           02 MDPERJFO  PIC X(2).                                       00003230
           02 FILLER    PIC X(2).                                       00003240
           02 MDPERMFA  PIC X.                                          00003250
           02 MDPERMFC  PIC X.                                          00003260
           02 MDPERMFP  PIC X.                                          00003270
           02 MDPERMFH  PIC X.                                          00003280
           02 MDPERMFV  PIC X.                                          00003290
           02 MDPERMFO  PIC X(2).                                       00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MDPERAFA  PIC X.                                          00003320
           02 MDPERAFC  PIC X.                                          00003330
           02 MDPERAFP  PIC X.                                          00003340
           02 MDPERAFH  PIC X.                                          00003350
           02 MDPERAFV  PIC X.                                          00003360
           02 MDPERAFO  PIC X(4).                                       00003370
           02 FILLER    PIC X(2).                                       00003380
           02 MDDEBJA   PIC X.                                          00003390
           02 MDDEBJC   PIC X.                                          00003400
           02 MDDEBJP   PIC X.                                          00003410
           02 MDDEBJH   PIC X.                                          00003420
           02 MDDEBJV   PIC X.                                          00003430
           02 MDDEBJO   PIC X(2).                                       00003440
           02 FILLER    PIC X(2).                                       00003450
           02 MDDEBMA   PIC X.                                          00003460
           02 MDDEBMC   PIC X.                                          00003470
           02 MDDEBMP   PIC X.                                          00003480
           02 MDDEBMH   PIC X.                                          00003490
           02 MDDEBMV   PIC X.                                          00003500
           02 MDDEBMO   PIC X(2).                                       00003510
           02 FILLER    PIC X(2).                                       00003520
           02 MDDEBAA   PIC X.                                          00003530
           02 MDDEBAC   PIC X.                                          00003540
           02 MDDEBAP   PIC X.                                          00003550
           02 MDDEBAH   PIC X.                                          00003560
           02 MDDEBAV   PIC X.                                          00003570
           02 MDDEBAO   PIC X(4).                                       00003580
           02 FILLER    PIC X(2).                                       00003590
           02 MDFINJA   PIC X.                                          00003600
           02 MDFINJC   PIC X.                                          00003610
           02 MDFINJP   PIC X.                                          00003620
           02 MDFINJH   PIC X.                                          00003630
           02 MDFINJV   PIC X.                                          00003640
           02 MDFINJO   PIC X(2).                                       00003650
           02 FILLER    PIC X(2).                                       00003660
           02 MDFINMA   PIC X.                                          00003670
           02 MDFINMC   PIC X.                                          00003680
           02 MDFINMP   PIC X.                                          00003690
           02 MDFINMH   PIC X.                                          00003700
           02 MDFINMV   PIC X.                                          00003710
           02 MDFINMO   PIC X(2).                                       00003720
           02 FILLER    PIC X(2).                                       00003730
           02 MDFINAA   PIC X.                                          00003740
           02 MDFINAC   PIC X.                                          00003750
           02 MDFINAP   PIC X.                                          00003760
           02 MDFINAH   PIC X.                                          00003770
           02 MDFINAV   PIC X.                                          00003780
           02 MDFINAO   PIC X(4).                                       00003790
           02 FILLER    PIC X(2).                                       00003800
           02 MPERIODEA      PIC X.                                     00003810
           02 MPERIODEC PIC X.                                          00003820
           02 MPERIODEP PIC X.                                          00003830
           02 MPERIODEH PIC X.                                          00003840
           02 MPERIODEV PIC X.                                          00003850
           02 MPERIODEO      PIC X(3).                                  00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MNCAISA   PIC X.                                          00003880
           02 MNCAISC   PIC X.                                          00003890
           02 MNCAISP   PIC X.                                          00003900
           02 MNCAISH   PIC X.                                          00003910
           02 MNCAISV   PIC X.                                          00003920
           02 MNCAISO   PIC X(3).                                       00003930
           02 FILLER    PIC X(2).                                       00003940
           02 MTTRANSA  PIC X.                                          00003950
           02 MTTRANSC  PIC X.                                          00003960
           02 MTTRANSP  PIC X.                                          00003970
           02 MTTRANSH  PIC X.                                          00003980
           02 MTTRANSV  PIC X.                                          00003990
           02 MTTRANSO  PIC X(3).                                       00004000
           02 FILLER    PIC X(2).                                       00004010
           02 MNTRANSA  PIC X.                                          00004020
           02 MNTRANSC  PIC X.                                          00004030
           02 MNTRANSP  PIC X.                                          00004040
           02 MNTRANSH  PIC X.                                          00004050
           02 MNTRANSV  PIC X.                                          00004060
           02 MNTRANSO  PIC X(8).                                       00004070
           02 FILLER    PIC X(2).                                       00004080
           02 MMODDELA  PIC X.                                          00004090
           02 MMODDELC  PIC X.                                          00004100
           02 MMODDELP  PIC X.                                          00004110
           02 MMODDELH  PIC X.                                          00004120
           02 MMODDELV  PIC X.                                          00004130
           02 MMODDELO  PIC X(3).                                       00004140
           02 FILLER    PIC X(2).                                       00004150
           02 MPMONTA   PIC X.                                          00004160
           02 MPMONTC   PIC X.                                          00004170
           02 MPMONTP   PIC X.                                          00004180
           02 MPMONTH   PIC X.                                          00004190
           02 MPMONTV   PIC X.                                          00004200
           02 MPMONTO   PIC X(10).                                      00004210
           02 FILLER    PIC X(2).                                       00004220
           02 MNCODICA  PIC X.                                          00004230
           02 MNCODICC  PIC X.                                          00004240
           02 MNCODICP  PIC X.                                          00004250
           02 MNCODICH  PIC X.                                          00004260
           02 MNCODICV  PIC X.                                          00004270
           02 MNCODICO  PIC X(7).                                       00004280
           02 FILLER    PIC X(2).                                       00004290
           02 MNVENTEA  PIC X.                                          00004300
           02 MNVENTEC  PIC X.                                          00004310
           02 MNVENTEP  PIC X.                                          00004320
           02 MNVENTEH  PIC X.                                          00004330
           02 MNVENTEV  PIC X.                                          00004340
           02 MNVENTEO  PIC X(8).                                       00004350
           02 FILLER    PIC X(2).                                       00004360
           02 MPVENTEA  PIC X.                                          00004370
           02 MPVENTEC  PIC X.                                          00004380
           02 MPVENTEP  PIC X.                                          00004390
           02 MPVENTEH  PIC X.                                          00004400
           02 MPVENTEV  PIC X.                                          00004410
           02 MPVENTEO  PIC X(10).                                      00004420
           02 FILLER    PIC X(2).                                       00004430
           02 MPVTEPAIA      PIC X.                                     00004440
           02 MPVTEPAIC PIC X.                                          00004450
           02 MPVTEPAIP PIC X.                                          00004460
           02 MPVTEPAIH PIC X.                                          00004470
           02 MPVTEPAIV PIC X.                                          00004480
           02 MPVTEPAIO      PIC X(10).                                 00004490
           02 FILLER    PIC X(2).                                       00004500
           02 MMODPAIA  PIC X.                                          00004510
           02 MMODPAIC  PIC X.                                          00004520
           02 MMODPAIP  PIC X.                                          00004530
           02 MMODPAIH  PIC X.                                          00004540
           02 MMODPAIV  PIC X.                                          00004550
           02 MMODPAIO  PIC X(5).                                       00004560
           02 FILLER    PIC X(2).                                       00004570
           02 MCODDEVA  PIC X.                                          00004580
           02 MCODDEVC  PIC X.                                          00004590
           02 MCODDEVP  PIC X.                                          00004600
           02 MCODDEVH  PIC X.                                          00004610
           02 MCODDEVV  PIC X.                                          00004620
           02 MCODDEVO  PIC X.                                          00004630
           02 FILLER    PIC X(2).                                       00004640
           02 MNCARTEA  PIC X.                                          00004650
           02 MNCARTEC  PIC X.                                          00004660
           02 MNCARTEP  PIC X.                                          00004670
           02 MNCARTEH  PIC X.                                          00004680
           02 MNCARTEV  PIC X.                                          00004690
           02 MNCARTEO  PIC X(9).                                       00004700
           02 FILLER    PIC X(2).                                       00004710
           02 MLIBERRA  PIC X.                                          00004720
           02 MLIBERRC  PIC X.                                          00004730
           02 MLIBERRP  PIC X.                                          00004740
           02 MLIBERRH  PIC X.                                          00004750
           02 MLIBERRV  PIC X.                                          00004760
           02 MLIBERRO  PIC X(78).                                      00004770
           02 FILLER    PIC X(2).                                       00004780
           02 MCODTRAA  PIC X.                                          00004790
           02 MCODTRAC  PIC X.                                          00004800
           02 MCODTRAP  PIC X.                                          00004810
           02 MCODTRAH  PIC X.                                          00004820
           02 MCODTRAV  PIC X.                                          00004830
           02 MCODTRAO  PIC X(4).                                       00004840
           02 FILLER    PIC X(2).                                       00004850
           02 MCICSA    PIC X.                                          00004860
           02 MCICSC    PIC X.                                          00004870
           02 MCICSP    PIC X.                                          00004880
           02 MCICSH    PIC X.                                          00004890
           02 MCICSV    PIC X.                                          00004900
           02 MCICSO    PIC X(5).                                       00004910
           02 FILLER    PIC X(2).                                       00004920
           02 MNETNAMA  PIC X.                                          00004930
           02 MNETNAMC  PIC X.                                          00004940
           02 MNETNAMP  PIC X.                                          00004950
           02 MNETNAMH  PIC X.                                          00004960
           02 MNETNAMV  PIC X.                                          00004970
           02 MNETNAMO  PIC X(8).                                       00004980
           02 FILLER    PIC X(2).                                       00004990
           02 MSCREENA  PIC X.                                          00005000
           02 MSCREENC  PIC X.                                          00005010
           02 MSCREENP  PIC X.                                          00005020
           02 MSCREENH  PIC X.                                          00005030
           02 MSCREENV  PIC X.                                          00005040
           02 MSCREENO  PIC X(4).                                       00005050
                                                                                
