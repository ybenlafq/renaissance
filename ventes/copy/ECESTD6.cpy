      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....: DECLARATION PRODUIT                             *
      * FICHIER.....: EMISSION COLIS ENT�TE STANDARD (MUTATION)       *
      * NOM FICHIER.: ECESTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 098                                             *
      *****************************************************************
      *
       01  ECESTD6.
      * CODE IDENTIFICATEUR
           05      ECESTD6-TYP-ENREG      PIC  X(0006).
      * CODE SOCI�T�
           05      ECESTD6-CSOCIETE       PIC  X(0005).
      * NUM�RO D'OP
           05      ECESTD6-NOP            PIC  X(0015).
      * NUM�RO DE COLIS
           05      ECESTD6-NCOLIS         PIC  X(0008).
      * ETAT COLIS
           05      ECESTD6-ETAT-COLIS     PIC  X(0001).
      * NUM�RO SUPPORT D'EXP�DITION
           05      ECESTD6-NSUPPORT       PIC  X(0010).
      * POIDS R�EL BRUT
           05      ECESTD6-POIDS          PIC  9(0009).
      * CODE EMBALLAGE
           05      ECESTD6-CEMBALLAGE     PIC  X(0010).
      * ALGORITHME DE COLISAGE
           05      ECESTD6-ALGO-COLISAGE  PIC  9(0003).
      * FILLER 30
           05      ECESTD6-FILLER         PIC  X(0030).
      * FIN
           05      ECESTD6-FIN            PIC  X(0001).
      
