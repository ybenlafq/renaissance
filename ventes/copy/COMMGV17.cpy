      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGV17                                            *        
      *  TITRE      : COMMAREA DU MODULE MGV17                         *        
      *               GESTION DES AUTORISATIONS ENTREPOT (A.S)         *        
      *  LONGUEUR   : 150 C                                            *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MGV17-APPLI.                                                    
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 39        
           05 COMM-GV17-DONNEES-ENTREE.                                         
              10 COMM-MGV17-NSOCIETE       PIC X(03).                           
              10 COMM-MGV17-NLIEU          PIC X(03).                           
              10 COMM-MGV17-NORDRE         PIC X(05).                           
              10 COMM-MGV17-DAUTORD        PIC X(08).                           
              10 COMM-MGV17-NAUTORD        PIC X(07).                           
              10 COMM-MGV17-CAUTORD        PIC X(05).                           
              10 COMM-MGV17-DVENTE         PIC X(08).                           
      *--- DONNEES EN SORTIE DU MODULE ------------------------------ 60        
           05 COMM-MGV17-DONNEES-SORTIE.                                        
              10 COMM-MGV17-MESSAGE.                                            
                 15 COMM-MGV17-CODRET          PIC X(01).                       
                    88 COMM-MGV17-CODRET-OK                 VALUE ' '.          
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-MGV17-CODRET-ERREUR   VALUE '1', '2', '3'.          
      *--                                                                       
                    88 COMM-MGV17-CODRET-ERREUR   VALUE '1'  '2'  '3'.          
      *}                                                                        
                    88 COMM-MGV17-ERREUR-ACCORD             VALUE '1'.          
                    88 COMM-MGV17-PAS-ACCORD                VALUE '2'.          
                    88 COMM-MGV17-CODRET-ERR-BLQ            VALUE '3'.          
                 15 COMM-MGV17-LIBERR.                                          
                    20 COMM-MGV17-NSEQERR         PIC X(04).                    
                    20 COMM-MGV17-ERRFIL          PIC X(01).                    
                    20 COMM-MGV17-LMESSAGE        PIC X(53).                    
              10 COMM-MGV17-ACTION          PIC X(01).                          
                 88 COMM-MGV17-INACTION                     VALUE ' '.          
                 88 COMM-MGV17-CREATION                     VALUE '1'.          
      *--- RESERVE -------------------------------------------------- 51        
           05 COMM-MGV17-FILLER          PIC X(51).                             
                                                                                
