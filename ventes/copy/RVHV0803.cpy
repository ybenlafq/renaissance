      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHV0803                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHV0803                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV0803.                                                            
           02  HV08-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HV08-CFAM                                                        
               PIC X(0005).                                                     
           02  HV08-DVENTECIALE                                                 
               PIC X(0008).                                                     
           02  HV08-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HV08-QPIECESCOMM                                                 
               PIC S9(5) COMP-3.                                                
           02  HV08-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PCACOMM                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATSCOMM                                               
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTPRIMES                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  HV08-QPIECESEMP                                                  
               PIC S9(5) COMP-3.                                                
           02  HV08-PCAEMP                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATSEMP                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTPRIMVOL                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-QPIECESEXC                                                  
               PIC S9(5) COMP-3.                                                
           02  HV08-PCAEXC                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATSEXC                                                
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-QPIECESEXE                                                  
               PIC S9(5) COMP-3.                                                
           02  HV08-PCAEXE                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HV08-PMTACHATSEXE                                                
               PIC S9(7)V9(0002) COMP-3.                                        
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHV0803                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHV0803-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DVENTECIALE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-DVENTECIALE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESCOMM-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECESCOMM-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCACOMM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCACOMM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSCOMM-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATSCOMM-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTPRIMES-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTPRIMES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESEMP-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECESEMP-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCAEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCAEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSEMP-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATSEMP-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTPRIMVOL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTPRIMVOL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESEXC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECESEXC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCAEXC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCAEXC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSEXC-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATSEXC-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-QPIECESEXE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-QPIECESEXE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PCAEXE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PCAEXE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HV08-PMTACHATSEXE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HV08-PMTACHATSEXE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
