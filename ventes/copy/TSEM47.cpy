      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00010001
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSEM47 <<<<<<<<<<<<<<<<<< * 00020006
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00030001
      *                                                               * 00040001
       01  TSEM47-IDENTIFICATEUR.                                       00050006
           05  TSEM47-TRANSID               PIC X(04) VALUE 'EM47'.     00060007
           05  TSEM47-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00070006
      *                                                               * 00080001
       01  TSEM47-ITEM.                                                 00090006
B&S        05  TSEM47-LONGUEUR              PIC S9(4) VALUE +0919.      00100006
           05  TSEM47-DATAS.                                            00110006
B&S            10  TSEM47-LIGNE-DETAIL      OCCURS  8.                  00120006
      *         DESCRIPTION DETAIL VENTE 057 OCTETS PAR OCCURENCE     * 00130000
                       20 TSEM47-CMARQ             PIC X(05).           00140006
                       20 TSEM47-CFAM              PIC X(05).           00150006
                       20 TSEM47-LREF              PIC X(20).           00160006
                       20 TSEM47-NCODIC            PIC X(07).           00170006
                       20 TSEM47-CRAYON            PIC X(02).           00180006
                       20 TSEM47-MODDEL            PIC X(03).           00190006
                       20 TSEM47-QTE               PIC S9(03) COMP-3.   00200006
B&S   *                20 TSEM47-TREMISE           PIC X(02).           00210006
B&S                    20 TSEM47-CODEREM           PIC X(05).           00220006
                       20 TSEM47-PVUNIT            PIC S9(06)V99 COMP-3.00230006
                       20 TSEM47-PREMISE-REDUCT    PIC S9(05)V99 COMP-3.00240006
                       20 TSEM47-PVTOTAL           PIC S9(06)V99 COMP-3.00250006
               10  TSEM47-LIGNE-REGLEMENT   OCCURS 06.                  00260006
      *     DESCRIPTION REGLEMENTS VENTE 032 OCTETS PAR OCCURENCE     * 00270000
                       25 TSEM47-CMODPAIE           PIC X(01).          00280006
JD                     25 TSEM47-DMODPAIE           PIC X(01).          00290006
                       25 TSEM47-LPAIE              PIC X(15).          00300006
B&S                    25 TSEM47-PREGLTVTE        PIC S9(07)V99 COMP-3. 00310006
B&S                    25 TSEM47-PREGLRENDU       PIC S9(07)V99 COMP-3. 00320006
B&S                    25 TSEM47-PVENTL           PIC S9(07)V99 COMP-3. 00330006
DC01                   25 TSEM47-DENCAIS            PIC X(14).          00340006
DC01  *        10  TSEM47-PREGLRENDU              PIC S9(04)V99 COMP-3. 00350006
      *                                                               * 00360000
      *                                                               * 00370000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00380001
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00390001
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00400001
                                                                                
