      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGES MQ   local --> HOST (MODULE MSL16)            *         
      *                                                               *         
      *   <<ATTENTION>> MAINTENIR CETTE COPIE EN PHASE AVEC CELLE     *         
      *                 DE L'AS/400 (CCBMDSL034)                      *         
      *****************************************************************         
      *                                                                         
      * Entete queue (correlid)                                lg=  50 c        
       01  WS-MQ10.                                                             
           05 WS-QUEUE.                                                         
              10 MQ10-MSGID             PIC X(24).                              
              10 MQ10-CORRELID          PIC X(24).                              
           05 WS-CODRET                 PIC X(02).                              
      *                                                                         
      * Message                                                                 
           05 WS-MESSAGE.                                                       
      * Entete standard darty                                  lg= 105 c        
              10 MES-ENTETE.                                                    
                 15 MES-TYPE            PIC X(03).                              
                 15 MES-NSOCMSG         PIC X(03).                              
                 15 MES-NLIEUMSG        PIC X(03).                              
                 15 MES-NSOCDST         PIC X(03).                              
                 15 MES-NLIEUDST        PIC X(03).                              
                 15 MES-NORD            PIC 9(08).                              
                 15 MES-LPROG           PIC X(10).                              
                 15 MES-DJOUR           PIC X(08).                              
                 15 MES-WSID            PIC X(10).                              
                 15 MES-USER            PIC X(10).                              
                 15 MES-CHRONO          PIC 9(07).                              
                 15 MES-NBRMSG          PIC 9(07).                              
                 15 MES-NBRENR          PIC 9(05).                              
                 15 MES-TAILLE          PIC 9(05).                              
                 15 MES-FILLER          PIC X(20).                              
      *                                                                         
      * Donnees applicatives                                   lg= 100 c        
              10 MES-DATA.                                                      
      * Parametres pour envoi d'une demande de modif de vente                   
      *          Num�ro de ligne de vente                                       
                 15 MESSL16-NSOCVTE        PIC X(03).                           
                 15 MESSL16-NLIEUVTE       PIC X(03).                           
                 15 MESSL16-NVENTE         PIC X(07).                           
                 15 MESSL16-NSEQNQ         PIC 9(05).                           
      *          Code fonction                                                  
                 15 MESSL16-CFONCT         PIC X(01).                           
                    88 MESSL16-PASSAGE-EN-Z    VALUE '1'.                       
                    88 MESSL16-CHG-LD2-LDM     VALUE '2'.                       
                    88 MESSL16-CHG-EMD-EMR     VALUE '3'.                       
      *          Code retour                                                    
                 15 MESSL16-CRETOUR        PIC X(02).                           
                    88 MESSL16-CRETOUR-OK      VALUE '00'.                      
                    88 MESSL16-CRETOUR-KO      VALUE '01' THRU '99'.            
                    88 MESSL16-VENTE-BLOQ      VALUE '01'.                      
                    88 MESSL16-VENTE-SUPPR     VALUE '02'.                      
                    88 MESSL16-VENTE-TOPEE     VALUE '03'.                      
      *                                                                         
                    88 MESSL16-PARAM-ERREUR    VALUE '90'.                      
                    88 MESSL16-FIN-ANORMALE    VALUE '99'.                      
                 15 MESSL16-ERREUR         PIC X(52).                           
                 15 MESSL16-NDEPOT         PIC X(03).                           
                 15 MESSL16-FILLER         PIC X(24).                           
                                                                                
