      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGV8001                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV8001                         
      **********************************************************                
       01  RVGV8001.                                                            
           02  GV80-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV80-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV80-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV80-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV80-NSEQNQ                                                      
               PIC S9(5) COMP-3.                                                
           02  GV80-CODACT                                                      
               PIC X(0001).                                                     
           02  GV80-LCOMMENT1                                                   
               PIC X(0050).                                                     
           02  GV80-LCOMMENT2                                                   
               PIC X(0050).                                                     
           02  GV80-STATUT                                                      
               PIC X(0003).                                                     
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV8000                                  
      **********************************************************                
       01  RVGV8001-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-NSEQNQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-NSEQNQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-CODACT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-CODACT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-LCOMMENT1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-LCOMMENT1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-LCOMMENT2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV80-LCOMMENT2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV80-STATUT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV80-STATUT-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
      *}                                                                        
