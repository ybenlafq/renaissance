      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>>>>> DESCRIPTION DE LA TSHV43 <<<<<<<<<<<<<<<<<< * 00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
       01  TSHV43-IDENTIFICATEUR.                                       00000050
           05  TSHV43-TRANSID               PIC X(04) VALUE 'HV43'.     00000060
           05  TSHV43-NUMERO-DE-TERMINAL    PIC X(04) VALUE SPACES.     00000070
      *                                                               * 00000080
       01  TSHV43-ITEM.                                                 00000090
           05  TSHV43-LONGUEUR              PIC S9(4) VALUE +0246.      00000100
           05  TSHV43-DATAS.                                            00000110
               10  TSHV43-LIGNE             OCCURS 06.                  00000120
      *     DESCRIPTION REGLEMENTS VENTE 055 OCTETS PAR OCCURENCE     * 00000200
                       25 TSHV43-DSAISIE            PIC X(08).             00000
                       25 TSHV43-CMODPAIE           PIC X(03).             00000
                       25 TSHV43-LPAIE              PIC X(15).             00000
                       25 TSHV43-DENCAIS            PIC X(10).             00000
                       25 TSHV43-PPAIE            PIC S9(06)V99 COMP-3.    00000
      *                                                               * 00000200
      *                                                               * 00000200
      *                                                               * 00000200
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000210
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< * 00000220
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000230
                                                                                
