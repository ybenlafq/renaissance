      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE - CONDITION DE STOCKAGE        *
      * NOM FICHIER.: EASSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 205                                             *
      *****************************************************************
      *
       01  EASSTD6.
      * TYPE ENREGISTREMENT : RECEPTION ARTICLE - CONDITION DE STOCKAGE
           05      EASSTD6-TYP-ENREG      PIC  X(0006).
      * CODE ETAT
           05      EASSTD6-CETAT          PIC  X(0001).
      * CODE SOCIETE
           05      EASSTD6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EASSTD6-CARTICLE       PIC  X(0018).
      * CONDITION DE STOCKAGE
           05      EASSTD6-COND-STOCKAGE  PIC  X(0005).
      * LIBELLE CONDITIONNEMENT STOCKAGE
           05      EASSTD6-LCOND-STOCKAGE PIC  X(0035).
      * QUANTITE
           05      EASSTD6-QTE            PIC  9(0009).
      * NB COUCHE / SUPPORT
           05      EASSTD6-NB-COUCHES-SUP PIC  9(0009).
      * NB TRANCHE / COUCHE
           05      EASSTD6-NB-TRANCH-COUCHE PIC 9(009).
      * NB COLIS / TRANCHE
           05      EASSTD6-NBCOLIS-TRANCHE PIC 9(0009).
      * NB UNITES / COLIS
           05      EASSTD6-NB-UNITES-COLIS PIC 9(0009).
      * LONGUEUR
           05      EASSTD6-LONGUEUR       PIC  9(0009).
      * LARGEUR
           05      EASSTD6-LARGEUR        PIC  9(0009).
      * HAUTEUR
           05      EASSTD6-HAUTEUR        PIC  9(0009).
      * POIDS
           05      EASSTD6-POIDS          PIC  9(0009).
      * CODE FOURNISSEUR
           05      EASSTD6-NENTCDE        PIC  X(0015).
      * ROTATION A/B/C/SANS
           05      EASSTD6-ROTATION       PIC  X(0001).
      * PROFIL DE RANGEMENT
           05      EASSTD6-RANGEMENT      PIC  9(0003).
      * NUMERO TYPE SUPPORT
           05      EASSTD6-NTYPE-SUPPORT  PIC  9(0002).
      * AUTORIS COMPACT REC
           05      EASSTD6-AUTOR-COMPACT   PIC 9(0001).
      * CONDITIONNEMENT/DFT
           05      EASSTD6-CONDITIONNEMENT PIC 9(0001).
      * FILLER 30
           05      EASSTD6-FILLER         PIC  X(0030).
      * FIN
           05      EASSTD6-FIN            PIC  X(0001).
      
