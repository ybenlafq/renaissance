      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TDLCT LIEN CONTROLE / FAM DE CONTROL   *        
      *----------------------------------------------------------------*        
       01  RVTDLCT.                                                             
           05  TDLCT-CTABLEG2    PIC X(15).                                     
           05  TDLCT-CTABLEG2-REDEF REDEFINES TDLCT-CTABLEG2.                   
               10  TDLCT-CCTRL           PIC X(05).                             
           05  TDLCT-WTABLEG     PIC X(80).                                     
           05  TDLCT-WTABLEG-REDEF  REDEFINES TDLCT-WTABLEG.                    
               10  TDLCT-FAMCTR          PIC X(05).                             
               10  TDLCT-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTDLCT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLCT-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TDLCT-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TDLCT-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TDLCT-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
