      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET     : LOGISTIQUE GROUPE                                *        
      *  PROGRAMME  : MFL05                                            *        
      *  TITRE      : TS DU MODULE TP MFL05                            *        
      *               DETERMINATION DES ENTREPOTS DE STOCKAGE          *        
      *               D'UN CODIC OU D'UNE FAMILLE                      *        
      *  LONGUEUR   : 100 C                                            *        
      *                                                                *        
      *  CONTENU    : CETTE TS CONTIENT LES DEMANDES CODIC OU FAMILLE  *        
      *               UNE DEMANDE PAR ITEM                             *        
      *  REMARQUE   : LA ZONE TS-MFL05-CFAM EST OBLIGATOIRE            *        
      *                                                                *        
      ******************************************************************        
       01  TS-MFL05-DONNEES.                                                    
           05 TS-MFL05-DEMANDE-ENTREE.                                          
              10 TS-MFL05-TYPE-DEMANDE   PIC X(01).                             
                 88 TS-MFL05-PAS-DEMANDE                VALUE ' '.              
                 88 TS-MFL05-DEMANDE-CODIC              VALUE 'C'.              
                 88 TS-MFL05-DEMANDE-FAMILLE            VALUE 'F'.              
              10 TS-MFL05-NCODIC         PIC X(07).                             
              10 TS-MFL05-CFAM           PIC X(05).                             
           05 TS-MFL05-DEMANDE-SORTIE.                                          
              15 TS-MFL05-KONTROL        PIC X(01).                             
                 88 TS-MFL05-OK                         VALUE ' '.              
                 88 TS-MFL05-ERREUR                     VALUE '1'.              
              15 TS-MFL05-DEPOT          OCCURS 5.                              
                 20 TS-MFL05-NSOC        PIC X(03).                             
                 20 TS-MFL05-NDEPOT      PIC X(03).                             
           05 TS-MFL05-FILLER            PIC X(56).                             
                                                                                
