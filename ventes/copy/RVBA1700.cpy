      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   TABLE RTBA017                                                         
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVBA1700                           
      *---------------------------------------------------------                
      *                                                                         
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVBA1700.                                                            
           02  BA17-NTIERSSAP                                                   
               PIC X(0010).                                                     
           02  BA17-LRSOCIALE                                                   
               PIC X(0032).                                                     
           02  BA17-LADRESSE1                                                   
               PIC X(0032).                                                     
           02  BA17-LADRESSE2                                                   
               PIC X(0032).                                                     
           02  BA17-CPOSTAL                                                     
               PIC X(0008).                                                     
           02  BA17-LVILLE                                                      
               PIC X(0032).                                                     
           02  BA17-DCREB2B                                                     
               PIC X(0008).                                                     
           02  BA17-DCREAT                                                      
               PIC X(0008).                                                     
           02  BA17-DMAJ                                                        
               PIC X(0008).                                                     
           02  BA17-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE RVBA1700                           
      *---------------------------------------------------------                
      *                                                                         
       01  RVBA1700-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-NTIERSSAP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-NTIERSSAP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-LRSOCIALE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-LRSOCIALE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-LADRESSE1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-LADRESSE1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-LADRESSE2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-LADRESSE2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-CPOSTAL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-LVILLE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-LVILLE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-DCREB2B-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-DCREB2B-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-DCREAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-DCREAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  BA17-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  BA17-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  BA17-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
