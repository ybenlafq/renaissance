      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      *                                                                *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.RVSL4100                      *        
      ******************************************************************        
       01  RVSL4100.                                                            
           10 SL41-NSOCIETE        PIC X(3).                                    
           10 SL41-CLIEUINV        PIC X(5).                                    
           10 SL41-NREGL           PIC S9(3)V USAGE COMP-3.                     
           10 SL41-LREGL           PIC X(20).                                   
           10 SL41-NBCPT           PIC X(1).                                    
           10 SL41-WQTUN           PIC X(1).                                    
           10 SL41-WCTRG           PIC X(1).                                    
           10 SL41-WETATC          PIC X(1).                                    
           10 SL41-WCB             PIC X(1).                                    
           10 SL41-WCTHEO          PIC X(1).                                    
           10 SL41-DSYST           PIC S9(13)V USAGE COMP-3.                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVSL4100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 INDSTRUC           PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 INDSTRUC           PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-NSOCIETE-F    PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-NSOCIETE-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-CLIEUINV-F    PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-CLIEUINV-F    PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-NREGL-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-NREGL-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-LREGL-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-LREGL-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-NBCPT-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-NBCPT-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-WQTUN-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-WQTUN-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-WCTRG-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-WCTRG-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-WETATC-F      PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-WETATC-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-WCB-F         PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-WCB-F         PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-WCTHEO-F      PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-WCTHEO-F      PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL41-DSYST-F       PIC S9(4) USAGE COMP.                          
      *--                                                                       
           10 SL41-DSYST-F       PIC S9(4) COMP-5.                              
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 11      *        
      ******************************************************************        
                                                                                
