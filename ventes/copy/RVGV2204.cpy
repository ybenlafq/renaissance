      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVGV2204                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV2204                         
      *---------------------------------------------------------                
      *                                                                         
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVGV2204.                                                            
           02  GV22-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV22-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV22-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV22-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV22-NCODICGRP                                                   
               PIC X(0007).                                                     
           02  GV22-NCODIC                                                      
               PIC X(0007).                                                     
           02  GV22-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV22-NMUTATION                                                   
               PIC X(0007).                                                     
           02  GV22-QVENDUE                                                     
               PIC S9(5) COMP-3.                                                
           02  GV22-QRESERV                                                     
               PIC S9(5) COMP-3.                                                
           02  GV22-NAUTORM                                                     
               PIC X(0005).                                                     
           02  GV22-WEMPORTE                                                    
               PIC X(0001).                                                     
           02  GV22-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV22-NSOCLIVR                                                    
               PIC X(0003).                                                     
           02  GV22-NDEPOT                                                      
               PIC X(0003).                                                     
           02  GV22-DDELIV                                                      
               PIC X(0008).                                                     
           02  GV22-CMODDEL                                                     
               PIC X(0003).                                                     
           02  GV22-CEQUIPE                                                     
               PIC X(0005).                                                     
           02  GV22-WCQERESF                                                    
               PIC X(0001).                                                     
           02  GV22-DCREATION                                                   
               PIC X(0008).                                                     
           02  GV22-CSTATUT                                                     
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVGV2203                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVGV2203-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NCODICGRP-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NCODICGRP-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NMUTATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NMUTATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-QVENDUE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-QVENDUE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-QRESERV-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-QRESERV-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NAUTORM-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NAUTORM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-WEMPORTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-WEMPORTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NSOCLIVR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NSOCLIVR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-DDELIV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-DDELIV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-CMODDEL-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-CMODDEL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-CEQUIPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-CEQUIPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-WCQERESF-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-WCQERESF-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-DCREATION-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-DCREATION-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV22-CSTATUT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV22-CSTATUT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
           EXEC SQL END DECLARE SECTION END-EXEC.      
       EJECT                                                                    
                                                                                
