      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00000010
      * Gestion des bons d'enlevement                                   00000020
      ***************************************************************** 00000030
       01   EGV80I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      * DATE DU JOUR                                                    00000060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000070
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000080
           02 FILLER    PIC X(4).                                       00000090
           02 MDATJOUI  PIC X(10).                                      00000100
      * HEURE                                                           00000110
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000120
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000130
           02 FILLER    PIC X(4).                                       00000140
           02 MTIMJOUI  PIC X(5).                                       00000150
      * OPTION                                                          00000160
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000170
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000180
           02 FILLER    PIC X(4).                                       00000190
           02 MZONCMDI  PIC X(2).                                       00000200
      * MAGASIN CEDANT                                                  00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUORIGL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MNLIEUORIGL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUORIGF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MNLIEUORIGI    PIC X(3).                                  00000250
      * NUMERO BE                                                       00000260
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBONENLVL     COMP PIC S9(4).                            00000270
      *--                                                                       
           02 MNBONENLVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNBONENLVF     PIC X.                                     00000280
           02 FILLER    PIC X(4).                                       00000290
           02 MNBONENLVI     PIC X(7).                                  00000300
      * NUMERO DOC ENLEV                                                00000310
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNDOCENLVL     COMP PIC S9(4).                            00000320
      *--                                                                       
           02 MNDOCENLVL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNDOCENLVF     PIC X.                                     00000330
           02 FILLER    PIC X(4).                                       00000340
           02 MNDOCENLVI     PIC X(7).                                  00000350
      * LIEU STOCKAGE FI                                                00000360
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDESTL    COMP PIC S9(4).                            00000370
      *--                                                                       
           02 MNLIEUDESTL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUDESTF    PIC X.                                     00000380
           02 FILLER    PIC X(4).                                       00000390
           02 MNLIEUDESTI    PIC X(3).                                  00000400
      * SSLIEU STOCKAGE                                                 00000410
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSSLIEUDESTL  COMP PIC S9(4).                            00000420
      *--                                                                       
           02 MNSSLIEUDESTL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSSLIEUDESTF  PIC X.                                     00000430
           02 FILLER    PIC X(4).                                       00000440
           02 MNSSLIEUDESTI  PIC X(3).                                  00000450
      * DATE DE DELIVRAN                                                00000460
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDDLIVMAXL     COMP PIC S9(4).                            00000470
      *--                                                                       
           02 MDDLIVMAXL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MDDLIVMAXF     PIC X.                                     00000480
           02 FILLER    PIC X(4).                                       00000490
           02 MDDLIVMAXI     PIC X(6).                                  00000500
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUDEPLIVL  COMP PIC S9(4).                            00000510
      *--                                                                       
           02 MNLIEUDEPLIVL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNLIEUDEPLIVF  PIC X.                                     00000520
           02 FILLER    PIC X(4).                                       00000530
           02 MNLIEUDEPLIVI  PIC X(3).                                  00000540
      * LIEU ENLEV.                                                     00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUENLVL    COMP PIC S9(4).                            00000560
      *--                                                                       
           02 MNLIEUENLVL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUENLVF    PIC X.                                     00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MNLIEUENLVI    PIC X(3).                                  00000590
      * NB D'EXEMPLAIRES                                                00000600
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBIMPL   COMP PIC S9(4).                                 00000610
      *--                                                                       
           02 MNBIMPL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNBIMPF   PIC X.                                          00000620
           02 FILLER    PIC X(4).                                       00000630
           02 MNBIMPI   PIC X(3).                                       00000640
      * CODE IMPRIMANTE                                                 00000650
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNCODIMPL      COMP PIC S9(4).                            00000660
      *--                                                                       
           02 MNCODIMPL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MNCODIMPF      PIC X.                                     00000670
           02 FILLER    PIC X(4).                                       00000680
           02 MNCODIMPI      PIC X(4).                                  00000690
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULL  COMP PIC S9(4).                                 00000700
      *--                                                                       
           02 MDANNULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDANNULF  PIC X.                                          00000710
           02 FILLER    PIC X(4).                                       00000720
           02 MDANNULI  PIC X(6).                                       00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIEUPTFL      COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MLIEUPTFL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLIEUPTFF      PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MLIEUPTFI      PIC X(3).                                  00000770
      * MESSAGE ERREUR                                                  00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000790
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MLIBERRI  PIC X(78).                                      00000820
      * CODE TRANSACTION                                                00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000840
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MCODTRAI  PIC X(4).                                       00000870
      * CICS DE TRAVAIL                                                 00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCICSI    PIC X(5).                                       00000920
      * NETNAME                                                         00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MNETNAMI  PIC X(8).                                       00000970
      * CODE TERMINAL                                                   00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MSCREENI  PIC X(4).                                       00001020
      ***************************************************************** 00001030
      * Gestion des bons d'enlevement                                   00001040
      ***************************************************************** 00001050
       01   EGV80O REDEFINES EGV80I.                                    00001060
           02 FILLER    PIC X(12).                                      00001070
      * DATE DU JOUR                                                    00001080
           02 FILLER    PIC X(2).                                       00001090
           02 MDATJOUA  PIC X.                                          00001100
           02 MDATJOUC  PIC X.                                          00001110
           02 MDATJOUP  PIC X.                                          00001120
           02 MDATJOUH  PIC X.                                          00001130
           02 MDATJOUV  PIC X.                                          00001140
           02 MDATJOUO  PIC X(10).                                      00001150
      * HEURE                                                           00001160
           02 FILLER    PIC X(2).                                       00001170
           02 MTIMJOUA  PIC X.                                          00001180
           02 MTIMJOUC  PIC X.                                          00001190
           02 MTIMJOUP  PIC X.                                          00001200
           02 MTIMJOUH  PIC X.                                          00001210
           02 MTIMJOUV  PIC X.                                          00001220
           02 MTIMJOUO  PIC X(5).                                       00001230
      * OPTION                                                          00001240
           02 FILLER    PIC X(2).                                       00001250
           02 MZONCMDA  PIC X.                                          00001260
           02 MZONCMDC  PIC X.                                          00001270
           02 MZONCMDP  PIC X.                                          00001280
           02 MZONCMDH  PIC X.                                          00001290
           02 MZONCMDV  PIC X.                                          00001300
           02 MZONCMDO  PIC X(2).                                       00001310
      * MAGASIN CEDANT                                                  00001320
           02 FILLER    PIC X(2).                                       00001330
           02 MNLIEUORIGA    PIC X.                                     00001340
           02 MNLIEUORIGC    PIC X.                                     00001350
           02 MNLIEUORIGP    PIC X.                                     00001360
           02 MNLIEUORIGH    PIC X.                                     00001370
           02 MNLIEUORIGV    PIC X.                                     00001380
           02 MNLIEUORIGO    PIC X(3).                                  00001390
      * NUMERO BE                                                       00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNBONENLVA     PIC X.                                     00001420
           02 MNBONENLVC     PIC X.                                     00001430
           02 MNBONENLVP     PIC X.                                     00001440
           02 MNBONENLVH     PIC X.                                     00001450
           02 MNBONENLVV     PIC X.                                     00001460
           02 MNBONENLVO     PIC X(7).                                  00001470
      * NUMERO DOC ENLEV                                                00001480
           02 FILLER    PIC X(2).                                       00001490
           02 MNDOCENLVA     PIC X.                                     00001500
           02 MNDOCENLVC     PIC X.                                     00001510
           02 MNDOCENLVP     PIC X.                                     00001520
           02 MNDOCENLVH     PIC X.                                     00001530
           02 MNDOCENLVV     PIC X.                                     00001540
           02 MNDOCENLVO     PIC X(7).                                  00001550
      * LIEU STOCKAGE FI                                                00001560
           02 FILLER    PIC X(2).                                       00001570
           02 MNLIEUDESTA    PIC X.                                     00001580
           02 MNLIEUDESTC    PIC X.                                     00001590
           02 MNLIEUDESTP    PIC X.                                     00001600
           02 MNLIEUDESTH    PIC X.                                     00001610
           02 MNLIEUDESTV    PIC X.                                     00001620
           02 MNLIEUDESTO    PIC X(3).                                  00001630
      * SSLIEU STOCKAGE                                                 00001640
           02 FILLER    PIC X(2).                                       00001650
           02 MNSSLIEUDESTA  PIC X.                                     00001660
           02 MNSSLIEUDESTC  PIC X.                                     00001670
           02 MNSSLIEUDESTP  PIC X.                                     00001680
           02 MNSSLIEUDESTH  PIC X.                                     00001690
           02 MNSSLIEUDESTV  PIC X.                                     00001700
           02 MNSSLIEUDESTO  PIC X(3).                                  00001710
      * DATE DE DELIVRAN                                                00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MDDLIVMAXA     PIC X.                                     00001740
           02 MDDLIVMAXC     PIC X.                                     00001750
           02 MDDLIVMAXP     PIC X.                                     00001760
           02 MDDLIVMAXH     PIC X.                                     00001770
           02 MDDLIVMAXV     PIC X.                                     00001780
           02 MDDLIVMAXO     PIC X(6).                                  00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MNLIEUDEPLIVA  PIC X.                                     00001810
           02 MNLIEUDEPLIVC  PIC X.                                     00001820
           02 MNLIEUDEPLIVP  PIC X.                                     00001830
           02 MNLIEUDEPLIVH  PIC X.                                     00001840
           02 MNLIEUDEPLIVV  PIC X.                                     00001850
           02 MNLIEUDEPLIVO  PIC X(3).                                  00001860
      * LIEU ENLEV.                                                     00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNLIEUENLVA    PIC X.                                     00001890
           02 MNLIEUENLVC    PIC X.                                     00001900
           02 MNLIEUENLVP    PIC X.                                     00001910
           02 MNLIEUENLVH    PIC X.                                     00001920
           02 MNLIEUENLVV    PIC X.                                     00001930
           02 MNLIEUENLVO    PIC X(3).                                  00001940
      * NB D'EXEMPLAIRES                                                00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNBIMPA   PIC X.                                          00001970
           02 MNBIMPC   PIC X.                                          00001980
           02 MNBIMPP   PIC X.                                          00001990
           02 MNBIMPH   PIC X.                                          00002000
           02 MNBIMPV   PIC X.                                          00002010
           02 MNBIMPO   PIC ZZZ.                                        00002020
      * CODE IMPRIMANTE                                                 00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MNCODIMPA      PIC X.                                     00002050
           02 MNCODIMPC PIC X.                                          00002060
           02 MNCODIMPP PIC X.                                          00002070
           02 MNCODIMPH PIC X.                                          00002080
           02 MNCODIMPV PIC X.                                          00002090
           02 MNCODIMPO      PIC X(4).                                  00002100
           02 FILLER    PIC X(2).                                       00002110
           02 MDANNULA  PIC X.                                          00002120
           02 MDANNULC  PIC X.                                          00002130
           02 MDANNULP  PIC X.                                          00002140
           02 MDANNULH  PIC X.                                          00002150
           02 MDANNULV  PIC X.                                          00002160
           02 MDANNULO  PIC X(6).                                       00002170
           02 FILLER    PIC X(2).                                       00002180
           02 MLIEUPTFA      PIC X.                                     00002190
           02 MLIEUPTFC PIC X.                                          00002200
           02 MLIEUPTFP PIC X.                                          00002210
           02 MLIEUPTFH PIC X.                                          00002220
           02 MLIEUPTFV PIC X.                                          00002230
           02 MLIEUPTFO      PIC X(3).                                  00002240
      * MESSAGE ERREUR                                                  00002250
           02 FILLER    PIC X(2).                                       00002260
           02 MLIBERRA  PIC X.                                          00002270
           02 MLIBERRC  PIC X.                                          00002280
           02 MLIBERRP  PIC X.                                          00002290
           02 MLIBERRH  PIC X.                                          00002300
           02 MLIBERRV  PIC X.                                          00002310
           02 MLIBERRO  PIC X(78).                                      00002320
      * CODE TRANSACTION                                                00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MCODTRAA  PIC X.                                          00002350
           02 MCODTRAC  PIC X.                                          00002360
           02 MCODTRAP  PIC X.                                          00002370
           02 MCODTRAH  PIC X.                                          00002380
           02 MCODTRAV  PIC X.                                          00002390
           02 MCODTRAO  PIC X(4).                                       00002400
      * CICS DE TRAVAIL                                                 00002410
           02 FILLER    PIC X(2).                                       00002420
           02 MCICSA    PIC X.                                          00002430
           02 MCICSC    PIC X.                                          00002440
           02 MCICSP    PIC X.                                          00002450
           02 MCICSH    PIC X.                                          00002460
           02 MCICSV    PIC X.                                          00002470
           02 MCICSO    PIC X(5).                                       00002480
      * NETNAME                                                         00002490
           02 FILLER    PIC X(2).                                       00002500
           02 MNETNAMA  PIC X.                                          00002510
           02 MNETNAMC  PIC X.                                          00002520
           02 MNETNAMP  PIC X.                                          00002530
           02 MNETNAMH  PIC X.                                          00002540
           02 MNETNAMV  PIC X.                                          00002550
           02 MNETNAMO  PIC X(8).                                       00002560
      * CODE TERMINAL                                                   00002570
           02 FILLER    PIC X(2).                                       00002580
           02 MSCREENA  PIC X.                                          00002590
           02 MSCREENC  PIC X.                                          00002600
           02 MSCREENP  PIC X.                                          00002610
           02 MSCREENH  PIC X.                                          00002620
           02 MSCREENV  PIC X.                                          00002630
           02 MSCREENO  PIC X(4).                                       00002640
                                                                                
