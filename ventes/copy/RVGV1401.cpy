      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
           EJECT                                                                
      **********************************************************                
      *   COPY DE LA TABLE RVGV1400                                             
      **********************************************************                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV1400                         
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1400.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1400.                                                            
      *}                                                                        
           02  GV14-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  GV14-NLIEU                                                       
               PIC X(0003).                                                     
           02  GV14-NORDRE                                                      
               PIC X(0005).                                                     
           02  GV14-NVENTE                                                      
               PIC X(0007).                                                     
           02  GV14-DSAISIE                                                     
               PIC X(0008).                                                     
           02  GV14-NSEQ                                                        
               PIC X(0002).                                                     
           02  GV14-CMODPAIMT                                                   
               PIC X(0005).                                                     
           02  GV14-PREGLTVTE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV14-NREGLTVTE                                                   
               PIC X(0007).                                                     
           02  GV14-DREGLTVTE                                                   
               PIC X(0008).                                                     
           02  GV14-WREGLTVTE                                                   
               PIC X(0001).                                                     
           02  GV14-DCOMPTA                                                     
               PIC X(0008).                                                     
           02  GV14-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  GV14-CPROTOUR                                                    
               PIC X(0005).                                                     
           02  GV14-CTOURNEE                                                    
               PIC X(0003).                                                     
           02  GV14-CDEVISE                                                     
               PIC X(0003).                                                     
           02  GV14-MDEVISE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  GV14-MECART                                                      
               PIC S9V9(0002) COMP-3.                                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      **********************************************************                
      *   LISTE DES FLAGS DE LA TABLE RVGV1400                                  
      **********************************************************                
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV1400-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV1400-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NORDRE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NORDRE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NVENTE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSAISIE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NSEQ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CMODPAIMT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CMODPAIMT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-PREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-PREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-NREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-NREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-WREGLTVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-WREGLTVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DCOMPTA-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CPROTOUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CPROTOUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-CDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-CDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MDEVISE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  GV14-MDEVISE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV14-MECART-F                                                    
      *        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  GV14-MECART-F                                                    
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
