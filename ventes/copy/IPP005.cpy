      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IPP005 AU 03/07/1996  *        
      *                                                                *        
      *          CRITERES DE TRI  07,03,BI,A,                          *        
      *                           10,02,BI,A,                          *        
      *                           12,03,BI,A,                          *        
      *                           15,01,BI,A,                          *        
      *                           16,15,BI,A,                          *        
      *                           31,07,BI,A,                          *        
      *                           38,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IPP005.                                                        
            05 NOMETAT-IPP005           PIC X(6) VALUE 'IPP005'.                
            05 RUPTURES-IPP005.                                                 
           10 IPP005-NSOCIETE           PIC X(03).                      007  003
           10 IPP005-CGRPMAG            PIC X(02).                      010  002
           10 IPP005-NLIEU              PIC X(03).                      012  003
           10 IPP005-WPRIME             PIC X(01).                      015  001
           10 IPP005-LVENDEUR           PIC X(15).                      016  015
           10 IPP005-CVENDEUR           PIC X(07).                      031  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IPP005-SEQUENCE           PIC S9(04) COMP.                038  002
      *--                                                                       
           10 IPP005-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IPP005.                                                   
           10 IPP005-LLIBELLE           PIC X(17).                      040  017
           10 IPP005-LLIEU              PIC X(20).                      057  020
           10 IPP005-NMAGRAT            PIC X(03).                      077  003
           10 IPP005-NRAT               PIC X(03).                      080  003
           10 IPP005-PMTCA              PIC S9(11)V9(2) COMP-3.         083  007
           10 IPP005-PMTCAMAG           PIC S9(11)V9(2) COMP-3.         090  007
           10 IPP005-PPAMM              PIC S9(07)      COMP-3.         097  004
           10 IPP005-PPAMM-AFFECTE-CA   PIC S9(07)      COMP-3.         101  004
           10 IPP005-PPAMM-AFFECTE-TOT  PIC S9(07)      COMP-3.         105  004
           10 IPP005-PPAMM-AFFECTE-VOL  PIC S9(07)      COMP-3.         109  004
           10 IPP005-PPAMM-ECART-CA     PIC S9(07)      COMP-3.         113  004
           10 IPP005-PPAMM-ECART-TOT    PIC S9(07)      COMP-3.         117  004
           10 IPP005-PPAMM-ECART-VOL    PIC S9(07)      COMP-3.         121  004
           10 IPP005-PPAMM-NONAFF-CA    PIC S9(07)      COMP-3.         125  004
           10 IPP005-PPAMM-NONAFF-TOT   PIC S9(07)      COMP-3.         129  004
           10 IPP005-PPAMM-NONAFF-VOL   PIC S9(07)      COMP-3.         133  004
           10 IPP005-PPAMM-OBJECTIF-CA  PIC S9(07)      COMP-3.         137  004
           10 IPP005-PPAMM-OBJECTIF-TOT PIC S9(07)      COMP-3.         141  004
           10 IPP005-PPAMM-OBJECTIF-VOL PIC S9(07)      COMP-3.         145  004
           10 IPP005-PPAMMCA            PIC S9(07)      COMP-3.         149  004
           10 IPP005-PPAMMVOL           PIC S9(07)      COMP-3.         153  004
           10 IPP005-QPIECEMAG          PIC S9(07)      COMP-3.         157  004
           10 IPP005-QPIECEVOL          PIC S9(07)      COMP-3.         161  004
           10 IPP005-QPOURCCA           PIC S9(03)V9(3) COMP-3.         165  004
           10 IPP005-QPOURCVOL          PIC S9(03)V9(3) COMP-3.         169  004
           10 IPP005-DMOISPAYE          PIC X(08).                      173  008
            05 FILLER                      PIC X(332).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IPP005-LONG           PIC S9(4)   COMP  VALUE +180.           
      *                                                                         
      *--                                                                       
        01  DSECT-IPP005-LONG           PIC S9(4) COMP-5  VALUE +180.           
                                                                                
      *}                                                                        
