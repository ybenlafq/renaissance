      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************        00010000
      *   COPY DE LA TABLE RVGV0899                                     00020001
      **********************************************************        00030000
      *                                                                 00040000
      *---------------------------------------------------------        00050000
      *   LISTE DES HOST VARIABLES DE LA TABLE RVGV0899                 00060001
      *---------------------------------------------------------        00070000
      *                                                                 00080000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0899.                                                    00090001
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0899.                                                            
      *}                                                                        
           02  GV08-NSOCIETE                                            00100001
               PIC X(0003).                                             00110001
           02  GV08-NLIEU                                               00120001
               PIC X(0003).                                             00130001
           02  GV08-NVENTE                                              00140001
               PIC X(0007).                                             00150001
           02  GV08-NDOSSIER                                            00160001
               PIC S9(3) COMP-3.                                        00170001
           02  GV08-NSEQ                                                00180001
               PIC S9(3) COMP-3.                                        00190001
           02  GV08-CCTRL                                               00200001
               PIC X(0005).                                             00210000
           02  GV08-LGCTRL                                              00220001
               PIC S9(3) COMP-3.                                        00230001
           02  GV08-VALCTRL                                             00240001
               PIC X(0050).                                             00250001
           02  GV08-CAUTOR                                              00260001
               PIC X(0005).                                             00270001
           02  GV08-CJUSTIF                                             00280001
               PIC X(0005).                                             00290001
           02  GV08-DMODIF                                              00300001
               PIC X(0008).                                             00310000
           02  GV08-DSYST                                               00320001
               PIC S9(13) COMP-3.                                       00330001
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                 00340000
      *---------------------------------------------------------        00350000
      *   LISTE DES FLAGS DE LA TABLE RVGV0800                          00360001
      *---------------------------------------------------------        00370000
      *                                                                 00380000
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVGV0899-FLAGS.                                              00390002
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVGV0899-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NSOCIETE-F                                          00400001
      *        PIC S9(4) COMP.                                          00410000
      *--                                                                       
           02  GV08-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NLIEU-F                                             00420001
      *        PIC S9(4) COMP.                                          00430000
      *--                                                                       
           02  GV08-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NVENTE-F                                            00440001
      *        PIC S9(4) COMP.                                          00450000
      *--                                                                       
           02  GV08-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NDOSSIER-F                                          00460001
      *        PIC S9(4) COMP.                                          00470000
      *--                                                                       
           02  GV08-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-NSEQ-F                                              00480001
      *        PIC S9(4) COMP.                                          00490000
      *--                                                                       
           02  GV08-NSEQ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CCTRL-F                                             00500001
      *        PIC S9(4) COMP.                                          00510000
      *--                                                                       
           02  GV08-CCTRL-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-LGCTRL-F                                            00520001
      *        PIC S9(4) COMP.                                          00530000
      *--                                                                       
           02  GV08-LGCTRL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-VALCTRL-F                                           00540001
      *        PIC S9(4) COMP.                                          00550000
      *--                                                                       
           02  GV08-VALCTRL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CAUTOR-F                                            00560001
      *        PIC S9(4) COMP.                                          00570000
      *--                                                                       
           02  GV08-CAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-CJUSTIF-F                                           00580001
      *        PIC S9(4) COMP.                                          00590000
      *--                                                                       
           02  GV08-CJUSTIF-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-DMODIF-F                                            00600001
      *        PIC S9(4) COMP.                                          00610000
      *--                                                                       
           02  GV08-DMODIF-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  GV08-DSYST-F                                             00620001
      *        PIC S9(4) COMP.                                          00630001
      *--                                                                       
           02  GV08-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                            00640000
                                                                                
