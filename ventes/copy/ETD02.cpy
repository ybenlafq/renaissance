      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * TD00 : CONSULTATION/MODIFICATION                                00000020
      ***************************************************************** 00000030
       01   ETD02I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAGEAL   COMP PIC S9(4).                                 00000140
      *--                                                                       
           02 MPAGEAL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MPAGEAF   PIC X.                                          00000150
           02 FILLER    PIC X(4).                                       00000160
           02 MPAGEAI   PIC X(2).                                       00000170
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNBPL     COMP PIC S9(4).                                 00000180
      *--                                                                       
           02 MNBPL COMP-5 PIC S9(4).                                           
      *}                                                                        
           02 MNBPF     PIC X.                                          00000190
           02 FILLER    PIC X(4).                                       00000200
           02 MNBPI     PIC X(2).                                       00000210
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MINITIALESL    COMP PIC S9(4).                            00000220
      *--                                                                       
           02 MINITIALESL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MINITIALESF    PIC X.                                     00000230
           02 FILLER    PIC X(4).                                       00000240
           02 MINITIALESI    PIC X(2).                                  00000250
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000260
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000270
           02 FILLER    PIC X(4).                                       00000280
           02 MNSOCIETEI     PIC X(3).                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLSOCIETEL     COMP PIC S9(4).                            00000300
      *--                                                                       
           02 MLSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLSOCIETEF     PIC X.                                     00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MLSOCIETEI     PIC X(20).                                 00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000340
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MNLIEUI   PIC X(3).                                       00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000380
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000390
           02 FILLER    PIC X(4).                                       00000400
           02 MLLIEUI   PIC X(20).                                      00000410
           02 MNOPTIONI OCCURS   4 TIMES .                              00000420
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MNOPTL  COMP PIC S9(4).                                 00000430
      *--                                                                       
             03 MNOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MNOPTF  PIC X.                                          00000440
             03 FILLER  PIC X(4).                                       00000450
             03 MNOPTI  PIC X(2).                                       00000460
           02 MLOPTIONI OCCURS   4 TIMES .                              00000470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLOPTL  COMP PIC S9(4).                                 00000480
      *--                                                                       
             03 MLOPTL COMP-5 PIC S9(4).                                        
      *}                                                                        
             03 MLOPTF  PIC X.                                          00000490
             03 FILLER  PIC X(4).                                       00000500
             03 MLOPTI  PIC X(20).                                      00000510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNVENTEL  COMP PIC S9(4).                                 00000520
      *--                                                                       
           02 MNVENTEL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNVENTEF  PIC X.                                          00000530
           02 FILLER    PIC X(4).                                       00000540
           02 MNVENTEI  PIC X(7).                                       00000550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLNOML    COMP PIC S9(4).                                 00000560
      *--                                                                       
           02 MLNOML COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MLNOMF    PIC X.                                          00000570
           02 FILLER    PIC X(4).                                       00000580
           02 MLNOMI    PIC X(26).                                      00000590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDOSSIERL     COMP PIC S9(4).                            00000600
      *--                                                                       
           02 MLDOSSIERL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLDOSSIERF     PIC X.                                     00000610
           02 FILLER    PIC X(4).                                       00000620
           02 MLDOSSIERI     PIC X(20).                                 00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTYPDOSSL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MTYPDOSSL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MTYPDOSSF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MTYPDOSSI      PIC X(7).                                  00000670
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCREATIONL     COMP PIC S9(4).                            00000680
      *--                                                                       
           02 MCREATIONL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCREATIONF     PIC X.                                     00000690
           02 FILLER    PIC X(4).                                       00000700
           02 MCREATIONI     PIC X(10).                                 00000710
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULL  COMP PIC S9(4).                                 00000720
      *--                                                                       
           02 MDANNULL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDANNULF  PIC X.                                          00000730
           02 FILLER    PIC X(4).                                       00000740
           02 MDANNULI  PIC X(20).                                      00000750
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MZONCMDL  COMP PIC S9(4).                                 00000760
      *--                                                                       
           02 MZONCMDL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MZONCMDF  PIC X.                                          00000770
           02 FILLER    PIC X(4).                                       00000780
           02 MZONCMDI  PIC X(2).                                       00000790
           02 MLIBCTRLI OCCURS   8 TIMES .                              00000800
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MLCTRLL      COMP PIC S9(4).                            00000810
      *--                                                                       
             03 MLCTRLL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MLCTRLF      PIC X.                                     00000820
             03 FILLER  PIC X(4).                                       00000830
             03 MLCTRLI      PIC X(20).                                 00000840
           02 MDEUXPOINTSI OCCURS   8 TIMES .                           00000850
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MDEUXPL      COMP PIC S9(4).                            00000860
      *--                                                                       
             03 MDEUXPL COMP-5 PIC S9(4).                                       
      *}                                                                        
             03 MDEUXPF      PIC X.                                     00000870
             03 FILLER  PIC X(4).                                       00000880
             03 MDEUXPI      PIC X.                                     00000890
           02 MVALEURCTRLI OCCURS   8 TIMES .                           00000900
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *      03 MVALCTRLL    COMP PIC S9(4).                            00000910
      *--                                                                       
             03 MVALCTRLL COMP-5 PIC S9(4).                                     
      *}                                                                        
             03 MVALCTRLF    PIC X.                                     00000920
             03 FILLER  PIC X(4).                                       00000930
             03 MVALCTRLI    PIC X(50).                                 00000940
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00000950
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00000960
           02 FILLER    PIC X(4).                                       00000970
           02 MLIBERRI  PIC X(78).                                      00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00000990
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MCODTRAI  PIC X(4).                                       00001020
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001030
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001040
           02 FILLER    PIC X(4).                                       00001050
           02 MCICSI    PIC X(5).                                       00001060
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001070
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001080
           02 FILLER    PIC X(4).                                       00001090
           02 MNETNAMI  PIC X(8).                                       00001100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001110
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001120
           02 FILLER    PIC X(4).                                       00001130
           02 MSCREENI  PIC X(4).                                       00001140
      ***************************************************************** 00001150
      * TD00 : CONSULTATION/MODIFICATION                                00001160
      ***************************************************************** 00001170
       01   ETD02O REDEFINES ETD02I.                                    00001180
           02 FILLER    PIC X(12).                                      00001190
           02 FILLER    PIC X(2).                                       00001200
           02 MDATJOUA  PIC X.                                          00001210
           02 MDATJOUC  PIC X.                                          00001220
           02 MDATJOUP  PIC X.                                          00001230
           02 MDATJOUH  PIC X.                                          00001240
           02 MDATJOUV  PIC X.                                          00001250
           02 MDATJOUO  PIC X(10).                                      00001260
           02 FILLER    PIC X(2).                                       00001270
           02 MTIMJOUA  PIC X.                                          00001280
           02 MTIMJOUC  PIC X.                                          00001290
           02 MTIMJOUP  PIC X.                                          00001300
           02 MTIMJOUH  PIC X.                                          00001310
           02 MTIMJOUV  PIC X.                                          00001320
           02 MTIMJOUO  PIC X(5).                                       00001330
           02 FILLER    PIC X(2).                                       00001340
           02 MPAGEAA   PIC X.                                          00001350
           02 MPAGEAC   PIC X.                                          00001360
           02 MPAGEAP   PIC X.                                          00001370
           02 MPAGEAH   PIC X.                                          00001380
           02 MPAGEAV   PIC X.                                          00001390
           02 MPAGEAO   PIC Z9.                                         00001400
           02 FILLER    PIC X(2).                                       00001410
           02 MNBPA     PIC X.                                          00001420
           02 MNBPC     PIC X.                                          00001430
           02 MNBPP     PIC X.                                          00001440
           02 MNBPH     PIC X.                                          00001450
           02 MNBPV     PIC X.                                          00001460
           02 MNBPO     PIC Z9.                                         00001470
           02 FILLER    PIC X(2).                                       00001480
           02 MINITIALESA    PIC X.                                     00001490
           02 MINITIALESC    PIC X.                                     00001500
           02 MINITIALESP    PIC X.                                     00001510
           02 MINITIALESH    PIC X.                                     00001520
           02 MINITIALESV    PIC X.                                     00001530
           02 MINITIALESO    PIC X(2).                                  00001540
           02 FILLER    PIC X(2).                                       00001550
           02 MNSOCIETEA     PIC X.                                     00001560
           02 MNSOCIETEC     PIC X.                                     00001570
           02 MNSOCIETEP     PIC X.                                     00001580
           02 MNSOCIETEH     PIC X.                                     00001590
           02 MNSOCIETEV     PIC X.                                     00001600
           02 MNSOCIETEO     PIC X(3).                                  00001610
           02 FILLER    PIC X(2).                                       00001620
           02 MLSOCIETEA     PIC X.                                     00001630
           02 MLSOCIETEC     PIC X.                                     00001640
           02 MLSOCIETEP     PIC X.                                     00001650
           02 MLSOCIETEH     PIC X.                                     00001660
           02 MLSOCIETEV     PIC X.                                     00001670
           02 MLSOCIETEO     PIC X(20).                                 00001680
           02 FILLER    PIC X(2).                                       00001690
           02 MNLIEUA   PIC X.                                          00001700
           02 MNLIEUC   PIC X.                                          00001710
           02 MNLIEUP   PIC X.                                          00001720
           02 MNLIEUH   PIC X.                                          00001730
           02 MNLIEUV   PIC X.                                          00001740
           02 MNLIEUO   PIC X(3).                                       00001750
           02 FILLER    PIC X(2).                                       00001760
           02 MLLIEUA   PIC X.                                          00001770
           02 MLLIEUC   PIC X.                                          00001780
           02 MLLIEUP   PIC X.                                          00001790
           02 MLLIEUH   PIC X.                                          00001800
           02 MLLIEUV   PIC X.                                          00001810
           02 MLLIEUO   PIC X(20).                                      00001820
           02 MNOPTIONO OCCURS   4 TIMES .                              00001830
             03 FILLER       PIC X(2).                                  00001840
             03 MNOPTA  PIC X.                                          00001850
             03 MNOPTC  PIC X.                                          00001860
             03 MNOPTP  PIC X.                                          00001870
             03 MNOPTH  PIC X.                                          00001880
             03 MNOPTV  PIC X.                                          00001890
             03 MNOPTO  PIC X(2).                                       00001900
           02 MLOPTIONO OCCURS   4 TIMES .                              00001910
             03 FILLER       PIC X(2).                                  00001920
             03 MLOPTA  PIC X.                                          00001930
             03 MLOPTC  PIC X.                                          00001940
             03 MLOPTP  PIC X.                                          00001950
             03 MLOPTH  PIC X.                                          00001960
             03 MLOPTV  PIC X.                                          00001970
             03 MLOPTO  PIC X(20).                                      00001980
           02 FILLER    PIC X(2).                                       00001990
           02 MNVENTEA  PIC X.                                          00002000
           02 MNVENTEC  PIC X.                                          00002010
           02 MNVENTEP  PIC X.                                          00002020
           02 MNVENTEH  PIC X.                                          00002030
           02 MNVENTEV  PIC X.                                          00002040
           02 MNVENTEO  PIC X(7).                                       00002050
           02 FILLER    PIC X(2).                                       00002060
           02 MLNOMA    PIC X.                                          00002070
           02 MLNOMC    PIC X.                                          00002080
           02 MLNOMP    PIC X.                                          00002090
           02 MLNOMH    PIC X.                                          00002100
           02 MLNOMV    PIC X.                                          00002110
           02 MLNOMO    PIC X(26).                                      00002120
           02 FILLER    PIC X(2).                                       00002130
           02 MLDOSSIERA     PIC X.                                     00002140
           02 MLDOSSIERC     PIC X.                                     00002150
           02 MLDOSSIERP     PIC X.                                     00002160
           02 MLDOSSIERH     PIC X.                                     00002170
           02 MLDOSSIERV     PIC X.                                     00002180
           02 MLDOSSIERO     PIC X(20).                                 00002190
           02 FILLER    PIC X(2).                                       00002200
           02 MTYPDOSSA      PIC X.                                     00002210
           02 MTYPDOSSC PIC X.                                          00002220
           02 MTYPDOSSP PIC X.                                          00002230
           02 MTYPDOSSH PIC X.                                          00002240
           02 MTYPDOSSV PIC X.                                          00002250
           02 MTYPDOSSO      PIC X(7).                                  00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCREATIONA     PIC X.                                     00002280
           02 MCREATIONC     PIC X.                                     00002290
           02 MCREATIONP     PIC X.                                     00002300
           02 MCREATIONH     PIC X.                                     00002310
           02 MCREATIONV     PIC X.                                     00002320
           02 MCREATIONO     PIC X(10).                                 00002330
           02 FILLER    PIC X(2).                                       00002340
           02 MDANNULA  PIC X.                                          00002350
           02 MDANNULC  PIC X.                                          00002360
           02 MDANNULP  PIC X.                                          00002370
           02 MDANNULH  PIC X.                                          00002380
           02 MDANNULV  PIC X.                                          00002390
           02 MDANNULO  PIC X(20).                                      00002400
           02 FILLER    PIC X(2).                                       00002410
           02 MZONCMDA  PIC X.                                          00002420
           02 MZONCMDC  PIC X.                                          00002430
           02 MZONCMDP  PIC X.                                          00002440
           02 MZONCMDH  PIC X.                                          00002450
           02 MZONCMDV  PIC X.                                          00002460
           02 MZONCMDO  PIC X(2).                                       00002470
           02 MLIBCTRLO OCCURS   8 TIMES .                              00002480
             03 FILLER       PIC X(2).                                  00002490
             03 MLCTRLA      PIC X.                                     00002500
             03 MLCTRLC PIC X.                                          00002510
             03 MLCTRLP PIC X.                                          00002520
             03 MLCTRLH PIC X.                                          00002530
             03 MLCTRLV PIC X.                                          00002540
             03 MLCTRLO      PIC X(20).                                 00002550
           02 MDEUXPOINTSO OCCURS   8 TIMES .                           00002560
             03 FILLER       PIC X(2).                                  00002570
             03 MDEUXPA      PIC X.                                     00002580
             03 MDEUXPC PIC X.                                          00002590
             03 MDEUXPP PIC X.                                          00002600
             03 MDEUXPH PIC X.                                          00002610
             03 MDEUXPV PIC X.                                          00002620
             03 MDEUXPO      PIC X.                                     00002630
           02 MVALEURCTRLO OCCURS   8 TIMES .                           00002640
             03 FILLER       PIC X(2).                                  00002650
             03 MVALCTRLA    PIC X.                                     00002660
             03 MVALCTRLC    PIC X.                                     00002670
             03 MVALCTRLP    PIC X.                                     00002680
             03 MVALCTRLH    PIC X.                                     00002690
             03 MVALCTRLV    PIC X.                                     00002700
             03 MVALCTRLO    PIC X(50).                                 00002710
           02 FILLER    PIC X(2).                                       00002720
           02 MLIBERRA  PIC X.                                          00002730
           02 MLIBERRC  PIC X.                                          00002740
           02 MLIBERRP  PIC X.                                          00002750
           02 MLIBERRH  PIC X.                                          00002760
           02 MLIBERRV  PIC X.                                          00002770
           02 MLIBERRO  PIC X(78).                                      00002780
           02 FILLER    PIC X(2).                                       00002790
           02 MCODTRAA  PIC X.                                          00002800
           02 MCODTRAC  PIC X.                                          00002810
           02 MCODTRAP  PIC X.                                          00002820
           02 MCODTRAH  PIC X.                                          00002830
           02 MCODTRAV  PIC X.                                          00002840
           02 MCODTRAO  PIC X(4).                                       00002850
           02 FILLER    PIC X(2).                                       00002860
           02 MCICSA    PIC X.                                          00002870
           02 MCICSC    PIC X.                                          00002880
           02 MCICSP    PIC X.                                          00002890
           02 MCICSH    PIC X.                                          00002900
           02 MCICSV    PIC X.                                          00002910
           02 MCICSO    PIC X(5).                                       00002920
           02 FILLER    PIC X(2).                                       00002930
           02 MNETNAMA  PIC X.                                          00002940
           02 MNETNAMC  PIC X.                                          00002950
           02 MNETNAMP  PIC X.                                          00002960
           02 MNETNAMH  PIC X.                                          00002970
           02 MNETNAMV  PIC X.                                          00002980
           02 MNETNAMO  PIC X(8).                                       00002990
           02 FILLER    PIC X(2).                                       00003000
           02 MSCREENA  PIC X.                                          00003010
           02 MSCREENC  PIC X.                                          00003020
           02 MSCREENP  PIC X.                                          00003030
           02 MSCREENH  PIC X.                                          00003040
           02 MSCREENV  PIC X.                                          00003050
           02 MSCREENO  PIC X(4).                                       00003060
                                                                                
