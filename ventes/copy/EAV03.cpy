      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00000010
      * SDF: EAV03   EAV03                                              00000020
      ***************************************************************** 00000030
       01   EAV03I.                                                     00000040
           02 FILLER    PIC X(12).                                      00000050
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDATJOUL  COMP PIC S9(4).                                 00000060
      *--                                                                       
           02 MDATJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MDATJOUF  PIC X.                                          00000070
           02 FILLER    PIC X(4).                                       00000080
           02 MDATJOUI  PIC X(10).                                      00000090
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MTIMJOUL  COMP PIC S9(4).                                 00000100
      *--                                                                       
           02 MTIMJOUL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MTIMJOUF  PIC X.                                          00000110
           02 FILLER    PIC X(4).                                       00000120
           02 MTIMJOUI  PIC X(5).                                       00000130
      * n�societe (ici)                                                 00000140
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEL     COMP PIC S9(4).                            00000150
      *--                                                                       
           02 MNSOCIETEL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCIETEF     PIC X.                                     00000160
           02 FILLER    PIC X(4).                                       00000170
           02 MNSOCIETEI     PIC X(3).                                  00000180
      * n�magasin   "                                                   00000190
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUL   COMP PIC S9(4).                                 00000200
      *--                                                                       
           02 MNLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MNLIEUF   PIC X.                                          00000210
           02 FILLER    PIC X(4).                                       00000220
           02 MNLIEUI   PIC X(3).                                       00000230
      * libelle du lieu                                                 00000240
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUL   COMP PIC S9(4).                                 00000250
      *--                                                                       
           02 MLLIEUL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MLLIEUF   PIC X.                                          00000260
           02 FILLER    PIC X(4).                                       00000270
           02 MLLIEUI   PIC X(20).                                      00000280
      * numero d'avoir                                                  00000290
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNAVOIRL  COMP PIC S9(4).                                 00000300
      *--                                                                       
           02 MNAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNAVOIRF  PIC X.                                          00000310
           02 FILLER    PIC X(4).                                       00000320
           02 MNAVOIRI  PIC X(8).                                       00000330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPAVOIRL    COMP PIC S9(4).                            00000340
      *--                                                                       
           02 MLTYPAVOIRL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLTYPAVOIRF    PIC X.                                     00000350
           02 FILLER    PIC X(4).                                       00000360
           02 MLTYPAVOIRI    PIC X(20).                                 00000370
      * code type utilisation                                           00000380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCTYPUTILL     COMP PIC S9(4).                            00000390
      *--                                                                       
           02 MCTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MCTYPUTILF     PIC X.                                     00000400
           02 FILLER    PIC X(4).                                       00000410
           02 MCTYPUTILI     PIC X(5).                                  00000420
      * libelle  type utilisation                                       00000430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLTYPUTILL     COMP PIC S9(4).                            00000440
      *--                                                                       
           02 MLTYPUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLTYPUTILF     PIC X.                                     00000450
           02 FILLER    PIC X(4).                                       00000460
           02 MLTYPUTILI     PIC X(20).                                 00000470
      * code motif                                                      00000480
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCMOTIFL  COMP PIC S9(4).                                 00000490
      *--                                                                       
           02 MCMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCMOTIFF  PIC X.                                          00000500
           02 FILLER    PIC X(4).                                       00000510
           02 MCMOTIFI  PIC X(5).                                       00000520
      * libelle motif                                                   00000530
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLMOTIFL  COMP PIC S9(4).                                 00000540
      *--                                                                       
           02 MLMOTIFL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLMOTIFF  PIC X.                                          00000550
           02 FILLER    PIC X(4).                                       00000560
           02 MLMOTIFI  PIC X(20).                                      00000570
      * montant de l'avoir                                              00000580
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MPAVOIRL  COMP PIC S9(4).                                 00000590
      *--                                                                       
           02 MPAVOIRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MPAVOIRF  PIC X.                                          00000600
           02 FILLER    PIC X(4).                                       00000610
           02 MPAVOIRI  PIC X(10).                                      00000620
      * code devise                                                     00000630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCDEVISEL      COMP PIC S9(4).                            00000640
      *--                                                                       
           02 MCDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MCDEVISEF      PIC X.                                     00000650
           02 FILLER    PIC X(4).                                       00000660
           02 MCDEVISEI      PIC X(3).                                  00000670
      * libelle devise                                                  00000680
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLDEVISEL      COMP PIC S9(4).                            00000690
      *--                                                                       
           02 MLDEVISEL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MLDEVISEF      PIC X.                                     00000700
           02 FILLER    PIC X(4).                                       00000710
           02 MLDEVISEI      PIC X(5).                                  00000720
      * n�societe d'imputation                                          00000730
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCIETEAFFL  COMP PIC S9(4).                            00000740
      *--                                                                       
           02 MNSOCIETEAFFL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MNSOCIETEAFFF  PIC X.                                     00000750
           02 FILLER    PIC X(4).                                       00000760
           02 MNSOCIETEAFFI  PIC X(3).                                  00000770
      * n�lieu d'imputation                                             00000780
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUAFFL     COMP PIC S9(4).                            00000790
      *--                                                                       
           02 MNLIEUAFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNLIEUAFFF     PIC X.                                     00000800
           02 FILLER    PIC X(4).                                       00000810
           02 MNLIEUAFFI     PIC X(3).                                  00000820
      * libelle qui va bien                                             00000830
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUAFFL     COMP PIC S9(4).                            00000840
      *--                                                                       
           02 MLLIEUAFFL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MLLIEUAFFF     PIC X.                                     00000850
           02 FILLER    PIC X(4).                                       00000860
           02 MLLIEUAFFI     PIC X(20).                                 00000870
      * code origine                                                    00000880
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCAPPLIL  COMP PIC S9(4).                                 00000890
      *--                                                                       
           02 MCAPPLIL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCAPPLIF  PIC X.                                          00000900
           02 FILLER    PIC X(4).                                       00000910
           02 MCAPPLII  PIC X(3).                                       00000920
      * date d'emission                                                 00000930
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDEMISL   COMP PIC S9(4).                                 00000940
      *--                                                                       
           02 MDEMISL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDEMISF   PIC X.                                          00000950
           02 FILLER    PIC X(4).                                       00000960
           02 MDEMISI   PIC X(10).                                      00000970
      * date IC d'�mission                                              00000980
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICEMISL      COMP PIC S9(4).                            00000990
      *--                                                                       
           02 MDICEMISL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDICEMISF      PIC X.                                     00001000
           02 FILLER    PIC X(4).                                       00001010
           02 MDICEMISI      PIC X(10).                                 00001020
      * statut d'utilisation                                            00001030
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWUTILL   COMP PIC S9(4).                                 00001040
      *--                                                                       
           02 MWUTILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MWUTILF   PIC X.                                          00001050
           02 FILLER    PIC X(4).                                       00001060
           02 MWUTILI   PIC X.                                          00001070
      * date d'utilisation                                              00001080
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDUTILL   COMP PIC S9(4).                                 00001090
      *--                                                                       
           02 MDUTILL COMP-5 PIC S9(4).                                         
      *}                                                                        
           02 MDUTILF   PIC X.                                          00001100
           02 FILLER    PIC X(4).                                       00001110
           02 MDUTILI   PIC X(10).                                      00001120
      * date IC d'utilisation                                           00001130
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICUTILL      COMP PIC S9(4).                            00001140
      *--                                                                       
           02 MDICUTILL COMP-5 PIC S9(4).                                       
      *}                                                                        
           02 MDICUTILF      PIC X.                                     00001150
           02 FILLER    PIC X(4).                                       00001160
           02 MDICUTILI      PIC X(10).                                 00001170
      * statut d'annulation                                             00001180
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MWANNULATIONL  COMP PIC S9(4).                            00001190
      *--                                                                       
           02 MWANNULATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MWANNULATIONF  PIC X.                                     00001200
           02 FILLER    PIC X(4).                                       00001210
           02 MWANNULATIONI  PIC X.                                     00001220
      * date d'annulation                                               00001230
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDANNULATIONL  COMP PIC S9(4).                            00001240
      *--                                                                       
           02 MDANNULATIONL COMP-5 PIC S9(4).                                   
      *}                                                                        
           02 MDANNULATIONF  PIC X.                                     00001250
           02 FILLER    PIC X(4).                                       00001260
           02 MDANNULATIONI  PIC X(10).                                 00001270
      * date IC d'annulation                                            00001280
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MDICANNULATIONL     COMP PIC S9(4).                       00001290
      *--                                                                       
           02 MDICANNULATIONL COMP-5 PIC S9(4).                                 
      *}                                                                        
           02 MDICANNULATIONF     PIC X.                                00001300
           02 FILLER    PIC X(4).                                       00001310
           02 MDICANNULATIONI     PIC X(10).                            00001320
      * societe utilisation                                             00001330
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNSOCUTILL     COMP PIC S9(4).                            00001340
      *--                                                                       
           02 MNSOCUTILL COMP-5 PIC S9(4).                                      
      *}                                                                        
           02 MNSOCUTILF     PIC X.                                     00001350
           02 FILLER    PIC X(4).                                       00001360
           02 MNSOCUTILI     PIC X(3).                                  00001370
      * magasin utilisation                                             00001380
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNLIEUUTILL    COMP PIC S9(4).                            00001390
      *--                                                                       
           02 MNLIEUUTILL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MNLIEUUTILF    PIC X.                                     00001400
           02 FILLER    PIC X(4).                                       00001410
           02 MNLIEUUTILI    PIC X(3).                                  00001420
      * libelle utilisation                                             00001430
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLLIEUUTILL    COMP PIC S9(4).                            00001440
      *--                                                                       
           02 MLLIEUUTILL COMP-5 PIC S9(4).                                     
      *}                                                                        
           02 MLLIEUUTILF    PIC X.                                     00001450
           02 FILLER    PIC X(4).                                       00001460
           02 MLLIEUUTILI    PIC X(20).                                 00001470
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MLIBERRL  COMP PIC S9(4).                                 00001480
      *--                                                                       
           02 MLIBERRL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MLIBERRF  PIC X.                                          00001490
           02 FILLER    PIC X(4).                                       00001500
           02 MLIBERRI  PIC X(78).                                      00001510
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCODTRAL  COMP PIC S9(4).                                 00001520
      *--                                                                       
           02 MCODTRAL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MCODTRAF  PIC X.                                          00001530
           02 FILLER    PIC X(4).                                       00001540
           02 MCODTRAI  PIC X(4).                                       00001550
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MCICSL    COMP PIC S9(4).                                 00001560
      *--                                                                       
           02 MCICSL COMP-5 PIC S9(4).                                          
      *}                                                                        
           02 MCICSF    PIC X.                                          00001570
           02 FILLER    PIC X(4).                                       00001580
           02 MCICSI    PIC X(5).                                       00001590
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MNETNAML  COMP PIC S9(4).                                 00001600
      *--                                                                       
           02 MNETNAML COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MNETNAMF  PIC X.                                          00001610
           02 FILLER    PIC X(4).                                       00001620
           02 MNETNAMI  PIC X(8).                                       00001630
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSCREENL  COMP PIC S9(4).                                 00001640
      *--                                                                       
           02 MSCREENL COMP-5 PIC S9(4).                                        
      *}                                                                        
           02 MSCREENF  PIC X.                                          00001650
           02 FILLER    PIC X(4).                                       00001660
           02 MSCREENI  PIC X(4).                                       00001670
      ***************************************************************** 00001680
      * SDF: EAV03   EAV03                                              00001690
      ***************************************************************** 00001700
       01   EAV03O REDEFINES EAV03I.                                    00001710
           02 FILLER    PIC X(12).                                      00001720
           02 FILLER    PIC X(2).                                       00001730
           02 MDATJOUA  PIC X.                                          00001740
           02 MDATJOUC  PIC X.                                          00001750
           02 MDATJOUP  PIC X.                                          00001760
           02 MDATJOUH  PIC X.                                          00001770
           02 MDATJOUV  PIC X.                                          00001780
           02 MDATJOUO  PIC X(10).                                      00001790
           02 FILLER    PIC X(2).                                       00001800
           02 MTIMJOUA  PIC X.                                          00001810
           02 MTIMJOUC  PIC X.                                          00001820
           02 MTIMJOUP  PIC X.                                          00001830
           02 MTIMJOUH  PIC X.                                          00001840
           02 MTIMJOUV  PIC X.                                          00001850
           02 MTIMJOUO  PIC X(5).                                       00001860
      * n�societe (ici)                                                 00001870
           02 FILLER    PIC X(2).                                       00001880
           02 MNSOCIETEA     PIC X.                                     00001890
           02 MNSOCIETEC     PIC X.                                     00001900
           02 MNSOCIETEP     PIC X.                                     00001910
           02 MNSOCIETEH     PIC X.                                     00001920
           02 MNSOCIETEV     PIC X.                                     00001930
           02 MNSOCIETEO     PIC X(3).                                  00001940
      * n�magasin   "                                                   00001950
           02 FILLER    PIC X(2).                                       00001960
           02 MNLIEUA   PIC X.                                          00001970
           02 MNLIEUC   PIC X.                                          00001980
           02 MNLIEUP   PIC X.                                          00001990
           02 MNLIEUH   PIC X.                                          00002000
           02 MNLIEUV   PIC X.                                          00002010
           02 MNLIEUO   PIC X(3).                                       00002020
      * libelle du lieu                                                 00002030
           02 FILLER    PIC X(2).                                       00002040
           02 MLLIEUA   PIC X.                                          00002050
           02 MLLIEUC   PIC X.                                          00002060
           02 MLLIEUP   PIC X.                                          00002070
           02 MLLIEUH   PIC X.                                          00002080
           02 MLLIEUV   PIC X.                                          00002090
           02 MLLIEUO   PIC X(20).                                      00002100
      * numero d'avoir                                                  00002110
           02 FILLER    PIC X(2).                                       00002120
           02 MNAVOIRA  PIC X.                                          00002130
           02 MNAVOIRC  PIC X.                                          00002140
           02 MNAVOIRP  PIC X.                                          00002150
           02 MNAVOIRH  PIC X.                                          00002160
           02 MNAVOIRV  PIC X.                                          00002170
           02 MNAVOIRO  PIC X(8).                                       00002180
           02 FILLER    PIC X(2).                                       00002190
           02 MLTYPAVOIRA    PIC X.                                     00002200
           02 MLTYPAVOIRC    PIC X.                                     00002210
           02 MLTYPAVOIRP    PIC X.                                     00002220
           02 MLTYPAVOIRH    PIC X.                                     00002230
           02 MLTYPAVOIRV    PIC X.                                     00002240
           02 MLTYPAVOIRO    PIC X(20).                                 00002250
      * code type utilisation                                           00002260
           02 FILLER    PIC X(2).                                       00002270
           02 MCTYPUTILA     PIC X.                                     00002280
           02 MCTYPUTILC     PIC X.                                     00002290
           02 MCTYPUTILP     PIC X.                                     00002300
           02 MCTYPUTILH     PIC X.                                     00002310
           02 MCTYPUTILV     PIC X.                                     00002320
           02 MCTYPUTILO     PIC X(5).                                  00002330
      * libelle  type utilisation                                       00002340
           02 FILLER    PIC X(2).                                       00002350
           02 MLTYPUTILA     PIC X.                                     00002360
           02 MLTYPUTILC     PIC X.                                     00002370
           02 MLTYPUTILP     PIC X.                                     00002380
           02 MLTYPUTILH     PIC X.                                     00002390
           02 MLTYPUTILV     PIC X.                                     00002400
           02 MLTYPUTILO     PIC X(20).                                 00002410
      * code motif                                                      00002420
           02 FILLER    PIC X(2).                                       00002430
           02 MCMOTIFA  PIC X.                                          00002440
           02 MCMOTIFC  PIC X.                                          00002450
           02 MCMOTIFP  PIC X.                                          00002460
           02 MCMOTIFH  PIC X.                                          00002470
           02 MCMOTIFV  PIC X.                                          00002480
           02 MCMOTIFO  PIC X(5).                                       00002490
      * libelle motif                                                   00002500
           02 FILLER    PIC X(2).                                       00002510
           02 MLMOTIFA  PIC X.                                          00002520
           02 MLMOTIFC  PIC X.                                          00002530
           02 MLMOTIFP  PIC X.                                          00002540
           02 MLMOTIFH  PIC X.                                          00002550
           02 MLMOTIFV  PIC X.                                          00002560
           02 MLMOTIFO  PIC X(20).                                      00002570
      * montant de l'avoir                                              00002580
           02 FILLER    PIC X(2).                                       00002590
           02 MPAVOIRA  PIC X.                                          00002600
           02 MPAVOIRC  PIC X.                                          00002610
           02 MPAVOIRP  PIC X.                                          00002620
           02 MPAVOIRH  PIC X.                                          00002630
           02 MPAVOIRV  PIC X.                                          00002640
           02 MPAVOIRO  PIC X(10).                                      00002650
      * code devise                                                     00002660
           02 FILLER    PIC X(2).                                       00002670
           02 MCDEVISEA      PIC X.                                     00002680
           02 MCDEVISEC PIC X.                                          00002690
           02 MCDEVISEP PIC X.                                          00002700
           02 MCDEVISEH PIC X.                                          00002710
           02 MCDEVISEV PIC X.                                          00002720
           02 MCDEVISEO      PIC X(3).                                  00002730
      * libelle devise                                                  00002740
           02 FILLER    PIC X(2).                                       00002750
           02 MLDEVISEA      PIC X.                                     00002760
           02 MLDEVISEC PIC X.                                          00002770
           02 MLDEVISEP PIC X.                                          00002780
           02 MLDEVISEH PIC X.                                          00002790
           02 MLDEVISEV PIC X.                                          00002800
           02 MLDEVISEO      PIC X(5).                                  00002810
      * n�societe d'imputation                                          00002820
           02 FILLER    PIC X(2).                                       00002830
           02 MNSOCIETEAFFA  PIC X.                                     00002840
           02 MNSOCIETEAFFC  PIC X.                                     00002850
           02 MNSOCIETEAFFP  PIC X.                                     00002860
           02 MNSOCIETEAFFH  PIC X.                                     00002870
           02 MNSOCIETEAFFV  PIC X.                                     00002880
           02 MNSOCIETEAFFO  PIC X(3).                                  00002890
      * n�lieu d'imputation                                             00002900
           02 FILLER    PIC X(2).                                       00002910
           02 MNLIEUAFFA     PIC X.                                     00002920
           02 MNLIEUAFFC     PIC X.                                     00002930
           02 MNLIEUAFFP     PIC X.                                     00002940
           02 MNLIEUAFFH     PIC X.                                     00002950
           02 MNLIEUAFFV     PIC X.                                     00002960
           02 MNLIEUAFFO     PIC X(3).                                  00002970
      * libelle qui va bien                                             00002980
           02 FILLER    PIC X(2).                                       00002990
           02 MLLIEUAFFA     PIC X.                                     00003000
           02 MLLIEUAFFC     PIC X.                                     00003010
           02 MLLIEUAFFP     PIC X.                                     00003020
           02 MLLIEUAFFH     PIC X.                                     00003030
           02 MLLIEUAFFV     PIC X.                                     00003040
           02 MLLIEUAFFO     PIC X(20).                                 00003050
      * code origine                                                    00003060
           02 FILLER    PIC X(2).                                       00003070
           02 MCAPPLIA  PIC X.                                          00003080
           02 MCAPPLIC  PIC X.                                          00003090
           02 MCAPPLIP  PIC X.                                          00003100
           02 MCAPPLIH  PIC X.                                          00003110
           02 MCAPPLIV  PIC X.                                          00003120
           02 MCAPPLIO  PIC X(3).                                       00003130
      * date d'emission                                                 00003140
           02 FILLER    PIC X(2).                                       00003150
           02 MDEMISA   PIC X.                                          00003160
           02 MDEMISC   PIC X.                                          00003170
           02 MDEMISP   PIC X.                                          00003180
           02 MDEMISH   PIC X.                                          00003190
           02 MDEMISV   PIC X.                                          00003200
           02 MDEMISO   PIC X(10).                                      00003210
      * date IC d'�mission                                              00003220
           02 FILLER    PIC X(2).                                       00003230
           02 MDICEMISA      PIC X.                                     00003240
           02 MDICEMISC PIC X.                                          00003250
           02 MDICEMISP PIC X.                                          00003260
           02 MDICEMISH PIC X.                                          00003270
           02 MDICEMISV PIC X.                                          00003280
           02 MDICEMISO      PIC X(10).                                 00003290
      * statut d'utilisation                                            00003300
           02 FILLER    PIC X(2).                                       00003310
           02 MWUTILA   PIC X.                                          00003320
           02 MWUTILC   PIC X.                                          00003330
           02 MWUTILP   PIC X.                                          00003340
           02 MWUTILH   PIC X.                                          00003350
           02 MWUTILV   PIC X.                                          00003360
           02 MWUTILO   PIC X.                                          00003370
      * date d'utilisation                                              00003380
           02 FILLER    PIC X(2).                                       00003390
           02 MDUTILA   PIC X.                                          00003400
           02 MDUTILC   PIC X.                                          00003410
           02 MDUTILP   PIC X.                                          00003420
           02 MDUTILH   PIC X.                                          00003430
           02 MDUTILV   PIC X.                                          00003440
           02 MDUTILO   PIC X(10).                                      00003450
      * date IC d'utilisation                                           00003460
           02 FILLER    PIC X(2).                                       00003470
           02 MDICUTILA      PIC X.                                     00003480
           02 MDICUTILC PIC X.                                          00003490
           02 MDICUTILP PIC X.                                          00003500
           02 MDICUTILH PIC X.                                          00003510
           02 MDICUTILV PIC X.                                          00003520
           02 MDICUTILO      PIC X(10).                                 00003530
      * statut d'annulation                                             00003540
           02 FILLER    PIC X(2).                                       00003550
           02 MWANNULATIONA  PIC X.                                     00003560
           02 MWANNULATIONC  PIC X.                                     00003570
           02 MWANNULATIONP  PIC X.                                     00003580
           02 MWANNULATIONH  PIC X.                                     00003590
           02 MWANNULATIONV  PIC X.                                     00003600
           02 MWANNULATIONO  PIC X.                                     00003610
      * date d'annulation                                               00003620
           02 FILLER    PIC X(2).                                       00003630
           02 MDANNULATIONA  PIC X.                                     00003640
           02 MDANNULATIONC  PIC X.                                     00003650
           02 MDANNULATIONP  PIC X.                                     00003660
           02 MDANNULATIONH  PIC X.                                     00003670
           02 MDANNULATIONV  PIC X.                                     00003680
           02 MDANNULATIONO  PIC X(10).                                 00003690
      * date IC d'annulation                                            00003700
           02 FILLER    PIC X(2).                                       00003710
           02 MDICANNULATIONA     PIC X.                                00003720
           02 MDICANNULATIONC     PIC X.                                00003730
           02 MDICANNULATIONP     PIC X.                                00003740
           02 MDICANNULATIONH     PIC X.                                00003750
           02 MDICANNULATIONV     PIC X.                                00003760
           02 MDICANNULATIONO     PIC X(10).                            00003770
      * societe utilisation                                             00003780
           02 FILLER    PIC X(2).                                       00003790
           02 MNSOCUTILA     PIC X.                                     00003800
           02 MNSOCUTILC     PIC X.                                     00003810
           02 MNSOCUTILP     PIC X.                                     00003820
           02 MNSOCUTILH     PIC X.                                     00003830
           02 MNSOCUTILV     PIC X.                                     00003840
           02 MNSOCUTILO     PIC X(3).                                  00003850
      * magasin utilisation                                             00003860
           02 FILLER    PIC X(2).                                       00003870
           02 MNLIEUUTILA    PIC X.                                     00003880
           02 MNLIEUUTILC    PIC X.                                     00003890
           02 MNLIEUUTILP    PIC X.                                     00003900
           02 MNLIEUUTILH    PIC X.                                     00003910
           02 MNLIEUUTILV    PIC X.                                     00003920
           02 MNLIEUUTILO    PIC X(3).                                  00003930
      * libelle utilisation                                             00003940
           02 FILLER    PIC X(2).                                       00003950
           02 MLLIEUUTILA    PIC X.                                     00003960
           02 MLLIEUUTILC    PIC X.                                     00003970
           02 MLLIEUUTILP    PIC X.                                     00003980
           02 MLLIEUUTILH    PIC X.                                     00003990
           02 MLLIEUUTILV    PIC X.                                     00004000
           02 MLLIEUUTILO    PIC X(20).                                 00004010
           02 FILLER    PIC X(2).                                       00004020
           02 MLIBERRA  PIC X.                                          00004030
           02 MLIBERRC  PIC X.                                          00004040
           02 MLIBERRP  PIC X.                                          00004050
           02 MLIBERRH  PIC X.                                          00004060
           02 MLIBERRV  PIC X.                                          00004070
           02 MLIBERRO  PIC X(78).                                      00004080
           02 FILLER    PIC X(2).                                       00004090
           02 MCODTRAA  PIC X.                                          00004100
           02 MCODTRAC  PIC X.                                          00004110
           02 MCODTRAP  PIC X.                                          00004120
           02 MCODTRAH  PIC X.                                          00004130
           02 MCODTRAV  PIC X.                                          00004140
           02 MCODTRAO  PIC X(4).                                       00004150
           02 FILLER    PIC X(2).                                       00004160
           02 MCICSA    PIC X.                                          00004170
           02 MCICSC    PIC X.                                          00004180
           02 MCICSP    PIC X.                                          00004190
           02 MCICSH    PIC X.                                          00004200
           02 MCICSV    PIC X.                                          00004210
           02 MCICSO    PIC X(5).                                       00004220
           02 FILLER    PIC X(2).                                       00004230
           02 MNETNAMA  PIC X.                                          00004240
           02 MNETNAMC  PIC X.                                          00004250
           02 MNETNAMP  PIC X.                                          00004260
           02 MNETNAMH  PIC X.                                          00004270
           02 MNETNAMV  PIC X.                                          00004280
           02 MNETNAMO  PIC X(8).                                       00004290
           02 FILLER    PIC X(2).                                       00004300
           02 MSCREENA  PIC X.                                          00004310
           02 MSCREENC  PIC X.                                          00004320
           02 MSCREENP  PIC X.                                          00004330
           02 MSCREENH  PIC X.                                          00004340
           02 MSCREENV  PIC X.                                          00004350
           02 MSCREENO  PIC X(4).                                       00004360
                                                                                
